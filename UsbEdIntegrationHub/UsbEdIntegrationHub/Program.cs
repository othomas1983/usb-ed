﻿using Quartz;
using StellenboschUniversity.ShortCoursesDivision.IntegrationServer.Jobs;
using Topshelf;
using Topshelf.Quartz;

namespace StellenboschUniversity.UsbEd.Integration
{
    class Program
    {
        static void Main(string[] args)
        {
            HostFactory.Run(x =>
            {
                x.Service<IntegrationHubService>(s =>
                {
                    s.ConstructUsing(name => new IntegrationHubService());
                    s.WhenStarted(tc => tc.Start());
                    s.WhenStopped(tc => tc.Stop());

                    // message polling
                    s.ScheduleQuartzJob(q =>
                        q.WithJob(() =>
                        JobBuilder.Create<MessagePollJob>()
                        .WithIdentity(MessagePollJob.Name)
                        .Build())
                            .AddTrigger(() =>
                                TriggerBuilder.Create()
                                .WithSimpleSchedule(builder => builder
                                .WithIntervalInSeconds(1)
                                .RepeatForever())
                            .Build())
                    );

                    // case creation
                    //s.ScheduleQuartzJob(q =>
                    //    q.WithJob(() =>
                    //    JobBuilder.Create<CaseCreationJob>()
                    //    .WithIdentity(CaseCreationJob.Name)
                    //    .Build())
                    //        .AddTrigger(() =>
                    //            TriggerBuilder.Create()
                    //            .WithSimpleSchedule(builder => builder
                    //            .WithIntervalInSeconds(60)
                    //            .RepeatForever())
                    //            .StartNow()
                    //        .Build())
                    //);

                    // course expiry
                    //s.ScheduleQuartzJob(q =>
                    //    q.WithJob(() =>
                    //    JobBuilder.Create<CourseExpiryJob>()
                    //    .WithIdentity(CourseExpiryJob.Name)
                    //    .Build())
                            
                    //        //.AddTrigger(() =>
                    //        //        TriggerBuilder.Create()
                    //        //        .WithSimpleSchedule(builder => builder
                    //        //        .WithIntervalInSeconds(1)
                    //        //        .RepeatForever())
                    //        //    .Build())

                    //        .AddTrigger(() =>
                    //            TriggerBuilder.Create()
                    //            .WithCronSchedule("59 59 23 L * ?")     // 23:59:59 last day of every month
                    //        .Build())
                    //);
                });

                x.RunAsLocalSystem();
                x.SetDescription("USB-ED Integration Hub");
                x.SetDisplayName("USB-ED Integration Hub");
                x.SetServiceName("integration-usbed-integrationhub");
            });
        }
    }
}
