﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Client;
using NLog;
using StellenboschUniversity.UsbEd.Integration.Crm;
using StellenboschUniversity.UsbEd.Integration.Crm.Utilities;
using StellenboschUniversity.Utilities.NLogGelfExtensions;

namespace StellenboschUniversity.UsbEd.Integration
{
    public class IntegrationHubService
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        
        public void Start()
        {
            CrmServer.CrmConnection = CrmConnection.Parse(Properties.Settings.Default.CrmConnectionString);
            
            //TestDirector.TestCrmToOic();
            //TestDirector.TestPerson();

            logger.Warn("USB-ED Integration Hub started.");
        }

        public void Stop()
        {

        }
    }
}
