﻿using System;
using Mercator;
using StellenboschUniversity.Messaging.Oic.Dto;
using StellenboschUniversity.UsbEd.Integration.Crm;
using StellenboschUniversity.UsbEd.Integration.Maps.MessageTypes;
using StellenboschUniversity.Messaging.Oic.Dto.Person;
using StellenboschUniversity.Messaging.Oic.Dto.Address;

namespace StellenboschUniversity.UsbEd.Integration.Maps
{
    public static class Mappers
    {
        #region Person

        public static Mapper<PERSOON_NAT_UPD_V4, Contact> PersonNatural
        {
            get
            {
                return PersonMaps.Natural;
            }
        }

        public static Mapper<PERSOON_HRMS_UPD_V3, Contact> PersonHR
        {
            get
            {
                return PersonMaps.HR;
            }
        }

        public static Mapper<PERSOON_DNR_UPD_V4, Contact> PersonDonor
        {
            get
            {
                return PersonMaps.Donor;
            }
        }

        public static Mapper<PERSOON_USBCRM_UPD_V5, Contact> PersonUsb
        {
            get
            {
                return PersonMaps.Usb;
            }
        }

        #endregion

        #region Address

        public static Mapper<U_ADRES_NAT_UPD_V3, Contact> AddressNatural
        {
            get
            {
                return AddressMaps.Natural;
            }
        }

        public static Mapper<U_ADRES_HRMS_UPD_V3, Contact> AddressHR
        {
            get
            {
                return AddressMaps.HR;
            }
        }

        public static Mapper<U_ADRES_DNR_UPD_V2, Contact> AddressDonor
        {
            get
            {
                return AddressMaps.Donor;
            }
        }

        public static Mapper<U_ADRES_USBCRM_UPD_V2, Contact> AddressUsb
        {
            get
            {
                return AddressMaps.Usb;
            }
        }

        #endregion

        #region Telephone

        public static Mapper<PERSOON_RTAD_WTEL_UPD_V2, Contact> TelephoneRtad
        {
            get
            {
                return TelephoneMaps.Rtad;
            }
        }

        public static Mapper<PERSOON_TELEFOON_HRMS_UPD_V2, Contact> TelephoneHR
        {
            get
            {
                return TelephoneMaps.HR;
            }
        }

        #endregion

        #region Email

        public static Mapper<PERSOON_RTAD_EMAIL_UPD_V2, Contact> EmailRtad
        {
            get
            {
                return EmailMaps.Rtad;
            }
        }

        #endregion
    }
}
