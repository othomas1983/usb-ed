﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mercator;
using StellenboschUniversity.Messaging.Oic.Dto;
using StellenboschUniversity.UsbEd.Integration.Crm;

namespace StellenboschUniversity.UsbEd.Integration.Maps.MessageTypes
{
    public static class TelephoneMaps
    {
        private static Mapper<PERSOON_RTAD_WTEL_UPD_V2, Contact> rtadTelephoneMap;
        private static Mapper<PERSOON_TELEFOON_HRMS_UPD_V2, Contact> hrTelephoneMap;

        static TelephoneMaps()
        {

        }

        private static void InitializeRtadMap()
        {
            rtadTelephoneMap = new Mapper<PERSOON_RTAD_WTEL_UPD_V2, Contact>();

            rtadTelephoneMap.AddRule(oicMessage =>
                {
                    return oicMessage.US_SKAKEL_WERK + oicMessage.US_TELEFOON_WERK;
                }
                , "telephone3");
        }

        private static void InitializeHrMap()
        {
            hrTelephoneMap = new Mapper<PERSOON_TELEFOON_HRMS_UPD_V2, Contact>();
            hrTelephoneMap.AddRule("TELEFOON_NOMMER_VOL", "telephone3");
        }

        public static Mapper<PERSOON_RTAD_WTEL_UPD_V2, Contact> Rtad
        {
            get
            {
                return rtadTelephoneMap;
            }
        }

        public static Mapper<PERSOON_TELEFOON_HRMS_UPD_V2, Contact> HR
        {
            get
            {
                return hrTelephoneMap;
            }
        }
    }
}
