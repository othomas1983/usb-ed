﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Microsoft.Xrm.Client;
using Mercator;
using StellenboschUniversity.Messaging.Oic.Dto;
using StellenboschUniversity.UsbEd.Integration.Crm;
using StellenboschUniversity.Messaging.Oic.Dto.Person;

namespace StellenboschUniversity.UsbEd.Integration.Maps.MessageTypes
{
    public static class PersonMaps
    {
        private static Mapper<PERSOON_NAT_UPD_V4, Contact> naturalPersonMap;
        private static Mapper<PERSOON_HRMS_UPD_V3, Contact> hrPersonMap;
        private static Mapper<PERSOON_DNR_UPD_V4, Contact> donorPersonMap;
        private static Mapper<PERSOON_USBCRM_UPD_V5, Contact> usbPersonMap;

        static PersonMaps()
        {
            InitializeNaturalMap();
            InitializeHRMap();
            InitializeDonorMap();
            InitializeUsbMap();
        }

        private static void InitializeNaturalMap()
        {
            naturalPersonMap = new Mapper<PERSOON_NAT_UPD_V4, Contact>();

            naturalPersonMap.AddTypeConverter<string, DateTime?>(MappingTypeConverters.ConvertSisDateToDateTime);

            naturalPersonMap.AddRule("US_ID_NOMMER", "governmentid");
            naturalPersonMap.AddRule("US_VOORLETTERS", "stb_initials");
            naturalPersonMap.AddRule("US_VAN", "lastname");
            naturalPersonMap.AddRule("US_GEB_DATUM", "birthdate");
            naturalPersonMap.AddRule("US_PASPOORT_NR", "stb_passportnumber");
            naturalPersonMap.AddRule("US_PASPOORT_VERVALDAT", "stb_passportexpirydate");
            naturalPersonMap.AddRule("US_HUWELIKSTAAT_DATUM", "anniversary");
            naturalPersonMap.AddRule("US_NOOIENSVAN", "stb_maidenname");
            naturalPersonMap.AddRule("US_EMAIL_ADRES", "emailaddress2");
            naturalPersonMap.AddRule("US_1E_INSKR_JAAR", "stb_firstyearofregistration");
            naturalPersonMap.AddRule("US_NOEMNAAM", "stb_givenname");
            naturalPersonMap.AddRule("US_SELFOON_NR", "mobilephone");

            // lookups

            naturalPersonMap.AddRule(message =>
            {
                return MappingTypeConverters.ConvertSisCodeToCrmEntityReference(stb_gender.EntityLogicalName, message.US_GESLAGKODE);
            },
            "stb_genderlookup"
            );

            naturalPersonMap.AddRule(message =>
            {
                return MappingTypeConverters.ConvertSisCodeToCrmEntityReference(stb_title.EntityLogicalName, message.US_TITELKODE);
            },
            "stb_titlelookup"
            );

            naturalPersonMap.AddRule(message =>
            {
                return MappingTypeConverters.ConvertSisCodeToCrmEntityReference(stb_ethnicity.EntityLogicalName, message.US_RASKODE);
            },
            "stb_ethnicitylookup"
            );

            naturalPersonMap.AddRule(message =>
            {
                return MappingTypeConverters.ConvertSisCodeToCrmEntityReference(stb_maritalstatus.EntityLogicalName, message.US_HUWELIKSTAATKODE);
            },
            "stb_maritalstatuslookup"
            );

            naturalPersonMap.AddRule(message =>
            {
                return MappingTypeConverters.ConvertSisCodeToCrmEntityReference(stb_language.EntityLogicalName, message.US_TAALKODE);
            },
            "stb_homelanguage_languagelookup"
            );

            naturalPersonMap.AddRule(message =>
            {
                return MappingTypeConverters.ConvertSisCodeToCrmEntityReference(stb_nationality.EntityLogicalName, message.US_NASIONALITEITKODE);
            },
            "stb_nationalitylookup"
            );

            naturalPersonMap.AddRule(message =>
            {
                return MappingTypeConverters.ConvertSisCodeToCrmEntityReference(stb_language.EntityLogicalName, message.US_KORR_TAAL);
            },
            "stb_correspondencelanguagelookup"
            );

            // composites

            naturalPersonMap.AddRule(message =>
            {
                return message.US_SKAKEL_NA_URE + message.US_TELEFOON_NA_URE;
            }
            , "telephone2"
            );

            naturalPersonMap.AddRule(message =>
            {
                return message.US_SKAKEL_WERK + message.US_TELEFOON_WERK;
            }
            , "telephone3"
            );

            naturalPersonMap.AddRule(message =>
            {
                return message.US_FAKS_SKAKEL + message.US_FAKS_NR;
            }
            , "fax"
            );

            naturalPersonMap.AddRule(message =>
            {
                if (String.IsNullOrEmpty(message.GENERIC_2))
                {
                    return message.US_VOORNAME.Trim();
                }
                else
                {
                    return message.GENERIC_2.Trim() + " " + message.GENERIC_3.Trim();
                }
            }
            , "firstname");
        }

        private static void InitializeHRMap()
        {
            hrPersonMap = new Mapper<PERSOON_HRMS_UPD_V3, Contact>();

            hrPersonMap.AddTypeConverter<string, DateTime?>(MappingTypeConverters.ConvertSisDateToDateTime);

            hrPersonMap.AddRule("US_ID_NOMMER", "governmentid");
            hrPersonMap.AddRule("US_VOORLETTERS", "stb_initials");
            hrPersonMap.AddRule("US_VAN", "lastname");
            hrPersonMap.AddRule("US_GEB_DATUM", "birthdate");
            hrPersonMap.AddRule("US_PASPOORT_NR", "stb_passportnumber");
            hrPersonMap.AddRule("US_PASPOORT_VERVALDAT", "stb_passportexpirydate");
            hrPersonMap.AddRule("US_EMAIL_ADRES", "emailaddress2");
            hrPersonMap.AddRule("US_HUWELIKSTAAT_DATUM", "anniversary");

            // lookups

            hrPersonMap.AddRule(message =>
            {
                return MappingTypeConverters.ConvertSisCodeToCrmEntityReference(stb_gender.EntityLogicalName, message.US_GESLAGKODE);
            },
            "stb_genderlookup"
            );

            hrPersonMap.AddRule(message =>
            {
                return MappingTypeConverters.ConvertSisCodeToCrmEntityReference(stb_title.EntityLogicalName, message.US_TITELKODE);
            },
            "stb_titlelookup"
            );

            hrPersonMap.AddRule(message =>
            {
                return MappingTypeConverters.ConvertSisCodeToCrmEntityReference(stb_ethnicity.EntityLogicalName, message.US_RASKODE);
            },
            "stb_ethnicitylookup"
            );

            hrPersonMap.AddRule(message =>
            {
                return MappingTypeConverters.ConvertSisCodeToCrmEntityReference(stb_maritalstatus.EntityLogicalName, message.US_HUWELIKSTAATKODE);
            },
            "stb_maritalstatuslookup"
            );

            hrPersonMap.AddRule(message =>
            {
                return MappingTypeConverters.ConvertSisCodeToCrmEntityReference(stb_language.EntityLogicalName, message.US_TAALKODE);
            },
            "stb_homelanguage_languagelookup"
            );

            hrPersonMap.AddRule(message =>
            {
                return MappingTypeConverters.ConvertSisCodeToCrmEntityReference(stb_nationality.EntityLogicalName, message.US_NASIONALITEITKODE);
            },
            "stb_nationalitylookup"
            );

            hrPersonMap.AddRule(message =>
            {
                return MappingTypeConverters.ConvertSisCodeToCrmEntityReference(stb_language.EntityLogicalName, message.US_KORR_TAAL);
            },
            "stb_correspondencelanguagelookup"
            );

            // composites

            hrPersonMap.AddRule(message =>
            {
                if (String.IsNullOrEmpty(message.GENERIC_2))
                {
                    return message.US_VOORNAME.Trim();
                }
                else
                {
                    return message.GENERIC_2.Trim() + " " + message.GENERIC_3.Trim();
                }
            }
            , "firstname");
        }

        private static void InitializeDonorMap()
        {
            donorPersonMap = new Mapper<PERSOON_DNR_UPD_V4, Contact>();

            donorPersonMap.AddTypeConverter<string, DateTime?>(MappingTypeConverters.ConvertSisDateToDateTime);

            donorPersonMap.AddRule("US_ID_NOMMER", "governmentid");
            donorPersonMap.AddRule("US_VOORLETTERS", "stb_initials");
            donorPersonMap.AddRule("US_VAN", "lastname");
            donorPersonMap.AddRule("US_GEB_DATUM", "birthdate");
            donorPersonMap.AddRule("US_EMAIL_ADRES", "emailaddress2");
            donorPersonMap.AddRule("US_FAKS_NR", "fax");
            donorPersonMap.AddRule("US_SELFOON_NR", "mobilephone");
            donorPersonMap.AddRule("US_NOEMNAAM", "stb_givenname");
            donorPersonMap.AddRule("US_HUWELIKSTAAT_DATUM", "anniversary");
            donorPersonMap.AddRule("US_NOOIENSVAN", "stb_maidenname");
            donorPersonMap.AddRule("US_TELEFOON_NA_URE", "telephone2");
            donorPersonMap.AddRule("US_PASPOORT_NR", "stb_passportnumber");
            donorPersonMap.AddRule("US_PASPOORT_VERVALDAT", "stb_passportexpirydate");

            // lookups

            donorPersonMap.AddRule(message =>
            {
                return MappingTypeConverters.ConvertSisCodeToCrmEntityReference(stb_gender.EntityLogicalName, message.US_GESLAGKODE);
            },
            "stb_genderlookup"
            );

            donorPersonMap.AddRule(message =>
            {
                return MappingTypeConverters.ConvertSisCodeToCrmEntityReference(stb_title.EntityLogicalName, message.US_TITELKODE);
            },
            "stb_titlelookup"
            );

            donorPersonMap.AddRule(message =>
            {
                return MappingTypeConverters.ConvertSisCodeToCrmEntityReference(stb_ethnicity.EntityLogicalName, message.US_RASKODE);
            },
            "stb_ethnicitylookup"
            );

            donorPersonMap.AddRule(message =>
            {
                return MappingTypeConverters.ConvertSisCodeToCrmEntityReference(stb_maritalstatus.EntityLogicalName, message.US_HUWELIKSTAATKODE);
            },
            "stb_maritalstatuslookup"
            );

            donorPersonMap.AddRule(message =>
            {
                return MappingTypeConverters.ConvertSisCodeToCrmEntityReference(stb_language.EntityLogicalName, message.US_TAALKODE);
            },
            "stb_homelanguage_languagelookup"
            );

            donorPersonMap.AddRule(message =>
            {
                return MappingTypeConverters.ConvertSisCodeToCrmEntityReference(stb_nationality.EntityLogicalName, message.US_NASIONALITEITKODE);
            },
            "stb_nationalitylookup"
            );

            donorPersonMap.AddRule(message =>
            {
                return MappingTypeConverters.ConvertSisCodeToCrmEntityReference(stb_language.EntityLogicalName, message.US_KORR_TAAL);
            },
            "stb_correspondencelanguagelookup"
            );

            // composites

            donorPersonMap.AddRule(message =>
            {
                if (String.IsNullOrEmpty(message.GENERIC_2))
                {
                    return message.US_VOORNAME.Trim();
                }
                else
                {
                    return message.GENERIC_2.Trim() + " " + message.GENERIC_3.Trim();
                }
            }
            , "firstname");
        }

        private static void InitializeUsbMap()
        {
            usbPersonMap = new Mapper<PERSOON_USBCRM_UPD_V5, Contact>();

            usbPersonMap.AddTypeConverter<string, DateTime?>(MappingTypeConverters.ConvertSisDateToDateTime);

            usbPersonMap.AddRule("US_ID_NOMMER", "governmentid");
            usbPersonMap.AddRule("US_VOORLETTERS", "stb_initials");
            usbPersonMap.AddRule("US_VAN", "lastname");
	        usbPersonMap.AddRule("US_GEB_DATUM", "birthdate");
            usbPersonMap.AddRule("US_EMAIL_ADRES", "emailaddress2");
            usbPersonMap.AddRule("US_FAKS_NR", "fax");
            usbPersonMap.AddRule("US_TELEFOON_NA_URE", "telephone2");
            usbPersonMap.AddRule("US_SELFOON_NR", "mobilephone");
            usbPersonMap.AddRule("US_NOEMNAAM", "stb_givenname");
            usbPersonMap.AddRule("US_TELEFOON_WERK", "telephone3");
            usbPersonMap.AddRule("US_HUWELIKSTAAT_DATUM", "anniversary");
            usbPersonMap.AddRule("US_PASPOORT_NR", "stb_passportnumber");
            usbPersonMap.AddRule("US_PASPOORT_VERVALDAT", "stb_passportexpirydate");
            usbPersonMap.AddRule("US_1E_INSKR_JAAR", "stb_firstyearofregistration");

            // lookups

            usbPersonMap.AddRule(message =>
            {
                return MappingTypeConverters.ConvertSisCodeToCrmEntityReference(stb_gender.EntityLogicalName, message.US_GESLAGKODE);
            },
            "stb_genderlookup"
            );

            usbPersonMap.AddRule(message =>
            {
                return MappingTypeConverters.ConvertSisCodeToCrmEntityReference(stb_title.EntityLogicalName, message.US_TITELKODE);
            },
            "stb_titlelookup"
            );

            usbPersonMap.AddRule(message =>
            {
                return MappingTypeConverters.ConvertSisCodeToCrmEntityReference(stb_ethnicity.EntityLogicalName, message.US_RASKODE);
            },
            "stb_ethnicitylookup"
            );

            usbPersonMap.AddRule(message =>
            {
                return MappingTypeConverters.ConvertSisCodeToCrmEntityReference(stb_maritalstatus.EntityLogicalName, message.US_HUWELIKSTAATKODE);
            },
            "stb_maritalstatuslookup"
            );

            usbPersonMap.AddRule(message =>
            {
                return MappingTypeConverters.ConvertSisCodeToCrmEntityReference(stb_language.EntityLogicalName, message.US_TAALKODE);
            },
            "stb_homelanguage_languagelookup"
            );

            usbPersonMap.AddRule(message =>
            {
                return MappingTypeConverters.ConvertSisCodeToCrmEntityReference(stb_nationality.EntityLogicalName, message.US_NASIONALITEITKODE);
            },
            "stb_nationalitylookup"
            );

            usbPersonMap.AddRule(message =>
            {
                return MappingTypeConverters.ConvertSisCodeToCrmEntityReference(stb_language.EntityLogicalName, message.US_KORR_TAAL);
            },
            "stb_correspondencelanguagelookup"
            );

            // composites

            usbPersonMap.AddRule(message =>
            {
                if (String.IsNullOrEmpty(message.GENERIC_2))
                {
                    return message.US_VOORNAME.Trim();
                }
                else
                {
                    return message.GENERIC_2.Trim() + " " + message.GENERIC_3.Trim();
                }
            }
            , "firstname");
        }

        public static Mapper<PERSOON_NAT_UPD_V4, Contact> Natural
        {
            get
            {
                return naturalPersonMap;
            }
        }

        public static Mapper<PERSOON_HRMS_UPD_V3, Contact> HR
        {
            get
            {
                return hrPersonMap;
            }
        }

        public static Mapper<PERSOON_DNR_UPD_V4, Contact> Donor
        {
            get
            {
                return donorPersonMap;
            }
        }

        public static Mapper<PERSOON_USBCRM_UPD_V5, Contact> Usb
        {
            get
            {
                return usbPersonMap;
            }
        }
    }
}
