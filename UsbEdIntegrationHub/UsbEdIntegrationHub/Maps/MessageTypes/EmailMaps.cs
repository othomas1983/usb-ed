﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mercator;
using StellenboschUniversity.Messaging.Oic.Dto;
using StellenboschUniversity.UsbEd.Integration.Crm;

namespace StellenboschUniversity.UsbEd.Integration.Maps.MessageTypes
{
    public static class EmailMaps
    {
        private static Mapper<PERSOON_RTAD_EMAIL_UPD_V2, Contact> rtadEmailMap;
                
        static EmailMaps()
        {
            InitializeRtadMap();
        }

        private static void InitializeRtadMap()
        {
            rtadEmailMap = new Mapper<PERSOON_RTAD_EMAIL_UPD_V2, Contact>();

            rtadEmailMap.AddRule("US_AMPTELIKE_EMAIL_ADRES", "emailaddress2");
        }

        public static Mapper<PERSOON_RTAD_EMAIL_UPD_V2, Contact> Rtad
        {
            get
            {
                return rtadEmailMap;
            }
        }
    }
}
