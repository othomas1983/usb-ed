﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk.Messages;
using NLog;
using Newtonsoft.Json;
using StellenboschUniversity.Integration.Sis.Extensions;
using StellenboschUniversity.UsbEd.Integration.Crm;
using StellenboschUniversity.UsbEd.Integration.Crm.Utilities;
using StellenboschUniversity.Infrastructure.Rtad;
using StellenboschUniversity.Utilities.NLogGelfExtensions;
using StellenboschUniversity.Messaging.Oic.Dto;


namespace StellenboschUniversity.UsbEd.Integration.Maps.MessageTypes
{
    public static class CreateShortCourseMessage
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private static string courseLeaderRoleName = "Base User Role (Read Only Role for all Users & Teams)";
        private static string courseAdminRoleName = "Course Administrator (Additive role for Course Admin functions)";
        private static string currencyName = "Rand";
        private static string businessUnitName = "USB-ED";
        private static int emailDeliveryMethod = 2;
        private static int cutoffYear = 2013;
        private static string ouCode = Properties.Settings.Default.UsbEdOUCode;

        private static CrmConnection crmConnection = new CrmConnection(Properties.Settings.Default.CrmConnectionString);
        private static IOrganizationService _orgService = new OrganizationService(crmConnection);


        public static void Process(SC_GENERIC message)
        {
            if (ShouldProcessMessage(message) == false)
            {
                return;
            }

            try
            {
                string shortCourseNumber = message.SC_NUMBER;
                Guid adminUserId;
                Guid leaderUserId;

                CreateUsers(message, out adminUserId, out leaderUserId);

                Guid shortCourseId;

                if (ShortCourseExists(shortCourseNumber))
                {
                    shortCourseId = GetShortCourseId(shortCourseNumber);
                    UpdateShortCourse(shortCourseId, message, adminUserId, leaderUserId);
                }
                else
                {
                    shortCourseId = CreateShortCourse(message, adminUserId, leaderUserId);
                }

                if (adminUserId != Guid.Empty)
                {
                    SetOwnershipOfShortCourse(shortCourseId, adminUserId);
                }

                SetShortCourseStatusToApproved(shortCourseId);
                //CreateShortCourseOfferings(message, shortCourseId, adminUserId);
            }
            catch (Exception exception)
            {
                logger.LogWithContext(LogLevel.Error, "Error occurred during processing of create short course message", exception, JsonConvert.SerializeObject(message));
            }
        }

        private static bool ShouldProcessMessage(SC_GENERIC message)
        {
            try
            {
                if (message.OE_KODE == ouCode)
                {
                    if (Convert.ToInt32(message.YEAR_OFFERED) > cutoffYear)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception exception)
            {
                logger.LogWithContext(LogLevel.Error, "Create Short Course: could not determine message relevance", exception, JsonConvert.SerializeObject(message));
            }

            return false;
        }

        private static bool CreateUsers(SC_GENERIC message, out Guid adminUserId, out Guid leaderUserId)
        {
            bool leaderCreationSuccess = true;
            bool adminCreationSuccess = true;

            // Course Leader
            try
            {
                string leaderUsername = GetFullyQualifiedUsername(message.LEADER_USER_NAME);

                if (UserExists(leaderUsername))
                {
                    leaderUserId = GetUserId(leaderUsername);
                    UpdateShortCourseUser(leaderUserId, leaderUsername, message.LEADER_FIRST_NAME.ToTitleCase(), message.LEADER_LAST_NAME.ToTitleCase(), message.LEADER_US_NUMBER, message.LEADER_FACT_OU_CODE, message.LEADER_EMAIL_ADDRESS);
                }
                else
                {
                    leaderUserId = CreateShortCourseUser(leaderUsername, message.LEADER_FIRST_NAME.ToTitleCase(), message.LEADER_LAST_NAME.ToTitleCase(), message.LEADER_US_NUMBER, message.LEADER_FACT_OU_CODE, message.LEADER_EMAIL_ADDRESS);
                }

                if (UserHasRole(leaderUserId, courseLeaderRoleName) == false)
                {
                    AssignRoleToUser(courseLeaderRoleName, leaderUserId);
                }
            }
            catch
            {
                leaderUserId = Guid.Empty;
                leaderCreationSuccess = false;
            }

            // Course Administrator
            try
            {
                string adminUsername = GetFullyQualifiedUsername(message.ADMIN_USER_NAME);

                if (UserExists(adminUsername))
                {
                    adminUserId = GetUserId(adminUsername);
                    UpdateShortCourseUser(adminUserId, adminUsername, message.ADMIN_FIRST_NAME.ToTitleCase(), message.ADMIN_LAST_NAME.ToTitleCase(), message.ADMIN_US_NUMBER, message.ADMIN_FACT_OU_CODE, message.ADMIN_EMAIL_ADDRESS);
                }
                else
                {
                    adminUserId = CreateShortCourseUser(adminUsername, message.ADMIN_FIRST_NAME.ToTitleCase(), message.ADMIN_LAST_NAME.ToTitleCase(), message.ADMIN_US_NUMBER, message.ADMIN_FACT_OU_CODE, message.ADMIN_EMAIL_ADDRESS);
                }

                if (UserHasRole(adminUserId, courseAdminRoleName) == false)
                {
                    AssignRoleToUser(courseAdminRoleName, adminUserId);
                }

                if (UserHasRole(adminUserId, courseLeaderRoleName) == false)
                {
                    AssignRoleToUser(courseLeaderRoleName, adminUserId);
                }
            }
            catch
            {
                adminUserId = Guid.Empty;
                adminCreationSuccess = false;
            }

            return leaderCreationSuccess && adminCreationSuccess;
        }

        private static stb_shortcourse ConstructShortCourseObject(SC_GENERIC message, Guid adminUserId, Guid leaderUserId)
        {
            //Guid presentationLanguageId = GetPresentationLanguageIdFromSisCode(message.PRESENTATION_LANG);
            bool advertiseOnWeb = (message.ADVERTISE_ON_WEB.Trim() == "1") ? true : false;

            var shortCourse = new stb_shortcourse()
            {
                stb_ShortCourseNumber = message.SC_NUMBER,
                stb_NQFLevel = message.NQF_LEVEL,
                stb_OrganizationalUnit = message.OE_KODE,
                stb_CostCentre = message.KOSTEPUNT_KODE,
                stb_SisCode = message.PROGRAM_CODE,
                stb_ShortCourseName = message.SC_TITLE,
                stb_FieldOfStudy = message.FIELD_OF_STUDY,
                stb_ProjectCode = message.PROJEK_NO,
                stb_AdvertiseOnWebTwoOptions = advertiseOnWeb,
                //stb_PresentationLanguageLookup = new CrmEntityReference(stb_PresentationLanguage.EntityLogicalName, presentationLanguageId),
                stb_ApprovedYear = new DateTime(Convert.ToInt32(message.YEAR_OFFERED), 1, 2),
                stb_SCRSAdministrator = message.ON_BEHALF_OF_US_NR,
                TransactionCurrencyId = new CrmEntityReference(TransactionCurrency.EntityLogicalName, GetCurrencyId(currencyName))
            };

            if (adminUserId != Guid.Empty)
            {
                shortCourse.stb_CourseAdministrator_UserLookup = new CrmEntityReference(SystemUser.EntityLogicalName, adminUserId);
            }

            if (leaderUserId != Guid.Empty)
            {
                shortCourse.stb_CourseLeader_UserLookup = new CrmEntityReference(SystemUser.EntityLogicalName, leaderUserId);
            }

            switch (message.CERTIFICATE_TYPE)
            {
                case "Competence":
                    shortCourse.stb_CertificateTypeTwoOptions = true;
                    break;

                case "Attendance":
                    shortCourse.stb_CertificateTypeTwoOptions = false;
                    break;

                default:
                    throw new Exception();
            }

            switch (message.SELECTION_COURSE)
            {
                case "0":
                    shortCourse.stb_SelectionBasedTwoOptions = false;
                    break;

                case "1":
                    shortCourse.stb_SelectionBasedTwoOptions = true;
                    break;

                default:
                    throw new Exception();
            }

            return shortCourse;
        }

        private static Guid CreateShortCourse(SC_GENERIC message, Guid adminUserId, Guid leaderUserId)
        {
            Guid shortCourseId;
            string shortCourseNumber;

            using (var crmServiceContext = new CrmServiceContext(_orgService))
            {
                var shortCourse = ConstructShortCourseObject(message, adminUserId, leaderUserId);

                crmServiceContext.AddObject(shortCourse);
                crmServiceContext.SaveChanges();

                shortCourseId = shortCourse.Id;
                shortCourseNumber = shortCourse.stb_ShortCourseNumber;
            }

            UpdateShortCourseApplicationFormUrl(shortCourseId);

            return shortCourseId;
        }

        private static void UpdateShortCourse(Guid shortCourseId, SC_GENERIC message, Guid adminUserId, Guid leaderUserId)
        {
            string shortCourseNumber;

            try
            {
                using (var organizationService = new OrganizationService(CrmServer.CrmConnection))
                {
                    var shortCourse = ConstructShortCourseObject(message, adminUserId, leaderUserId);

                    shortCourse.Id = shortCourseId;
                    shortCourseNumber = shortCourse.stb_ShortCourseNumber;

                    organizationService.Update(shortCourse);
                }

                UpdateShortCourseApplicationFormUrl(shortCourseId);
            }
            catch (Exception exception)
            {
                logger.LogWithContext(LogLevel.Error, "Could not update short course", exception, new
                {
                    ShortCourseId = shortCourseId,
                    AdminUserId = adminUserId,
                    LeaderUserId = leaderUserId,
                    Message = JsonConvert.SerializeObject(message)
                });
                throw;
            }
        }

        private static void UpdateShortCourseApplicationFormUrl(Guid shortCourseId)
        {
            using (var organizationService = new OrganizationService(CrmServer.CrmConnection))
            {
                var shortCourse = new stb_shortcourse()
                {
                    Id = shortCourseId,
                    stb_URLForApplication = Properties.Settings.Default.ApplicationFormUrl + "?iID=" + shortCourseId
                };

                organizationService.Update(shortCourse);
            }
        }

        private static void UpdateOfferingApplicationFormUrl(Guid shortCourseOfferingId)
        {
            using (var organizationService = new OrganizationService(CrmServer.CrmConnection))
            {
                stb_shortcourseoffering offering = new stb_shortcourseoffering()
                {
                    Id = shortCourseOfferingId,
                    stb_OfferingURL = Properties.Settings.Default.ApplicationFormUrl + "?oID=" + shortCourseOfferingId
                };

                organizationService.Update(offering);
            }
        }

        private static void CreateShortCourseOfferings(SC_GENERIC message, Guid shortCourseId, Guid adminUserId)
        {
            int shortCourseYearOffered = GetShortCourseYearOffered(shortCourseId);

            using (var crmServiceContext = new CrmServiceContext(_orgService))
            {
                var existingShortCourseOfferings = from c in crmServiceContext.stb_shortcourseofferingSet
                                                   where c.stb_ShortCourseLookup.Id == shortCourseId
                                                   select c;

                int existingShortCourseOfferingCount = GetOfferingsWithoutStartDateCount(existingShortCourseOfferings.ToList<stb_shortcourseoffering>());
                int shortCourseOfferingCount = Convert.ToInt32(message.PRESENT_FREQ);

                for (int i = 0; i < shortCourseOfferingCount - existingShortCourseOfferingCount; i++)
                {
                    var shortCourseOffering = new stb_shortcourseoffering()
                    {
                        stb_OfferingNameEnglish = message.SC_TITLE,
                        stb_ShortCourseLookup = new CrmEntityReference("stb_shortcourse", shortCourseId)
                    };

                    crmServiceContext.AddObject(shortCourseOffering);
                    crmServiceContext.SaveChanges();

                    if (adminUserId != Guid.Empty)
                    {
                        SetOwnershipOfShortCourseOffering(shortCourseOffering.Id, adminUserId);
                    }

                    SetOfferingStatusToNewOffering(shortCourseOffering.Id);
                    UpdateOfferingApplicationFormUrl(shortCourseOffering.Id);
                }
            }
        }

        private static Guid CreateShortCourseUser(string username, string firstName, string lastName, string usNumber, string ouCode, string emailAddress)
        {
            Guid result;

            try
            {
                using (var crmServiceContext = new CrmServiceContext(_orgService))
                {
                    var businessUnitId = GetBusinessUnitIdFromOUCode(ouCode);

                    var user = new SystemUser()
                    {
                        DomainName = username.Trim(),
                        FirstName = firstName.Trim(),
                        LastName = lastName.Trim(),
                        stb_usnumber = usNumber.Trim(),
                        InternalEMailAddress = emailAddress.Trim(),
                        BusinessUnitId = new CrmEntityReference("businessunit", businessUnitId),
                        //CALType = 0,
                        //AccessMode = 0,
                        //IncomingEmailDeliveryMethod = emailDeliveryMethod
                        //OutgoingEmailDeliveryMethod = emailDeliveryMethod
                        CALType = new OptionSetValue((int)SystemUserCALType.Professional),
                        AccessMode= new OptionSetValue((int)SystemUserAccessMode.ReadWrite),
                        //Assigned when mailbox for user gets created in 2013.
                        //IncomingEmailDeliveryMethod = new OptionSetValue((int)MailboxIncomingEmailDeliveryMethod.MicrosoftDynamicsCRMforOutlook),
                        //OutgoingEmailDeliveryMethod = new OptionSetValue((int)MailboxOutgoingEmailDeliveryMethod.MicrosoftDynamicsCRMforOutlook)
                    };

                    crmServiceContext.AddObject(user);
                    crmServiceContext.SaveChanges();

                    result = user.Id;
                }

            }
            catch (Exception exception)
            {
                logger.LogWithContext(LogLevel.Error, "Could not create short course user", exception, new
                {
                    UserName = username,
                    FirstName = firstName,
                    LastName = lastName,
                    UsNumber = usNumber,
                    OuCode = ouCode,
                    EmailAddress = emailAddress
                });
                throw;
            }

            return result;
        }

        private static void UpdateShortCourseUser(Guid userId, string username, string firstName, string lastName, string usNumber, string ouCode, string emailAddress)
        {
            try
            {
                using (var organizationService = new OrganizationService(CrmServer.CrmConnection))
                {
                    string safeOuCode = (String.IsNullOrEmpty(ouCode) == true) ? "0" : ouCode;
                    var businessUnitId = GetBusinessUnitId(businessUnitName);

                    var user = new SystemUser()
                    {
                        Id = userId,
                        DomainName = username,
                        FirstName = firstName.Trim(),
                        LastName = lastName.Trim(),
                        stb_usnumber = usNumber.Trim(),
                        BusinessUnitId = new CrmEntityReference("businessunit", businessUnitId),
                        //CALType = 0,
                        //AccessMode = 0,
                        //IncomingEmailDeliveryMethod = emailDeliveryMethod
                        //OutgoingEmailDeliveryMethod = emailDeliveryMethod
                        CALType = new OptionSetValue((int)SystemUserCALType.Professional),
                        AccessMode = new OptionSetValue((int)SystemUserAccessMode.ReadWrite)
                        //Assigned when mailbox for user gets created in 2013.
                        //IncomingEmailDeliveryMethod = new OptionSetValue((int)MailboxIncomingEmailDeliveryMethod.MicrosoftDynamicsCRMforOutlook),
                        //OutgoingEmailDeliveryMethod = new OptionSetValue((int)MailboxOutgoingEmailDeliveryMethod.MicrosoftDynamicsCRMforOutlook)
                    };

                    organizationService.Update(user);
                }
            }
            catch (Exception exception)
            {
                logger.LogWithContext(LogLevel.Error, "Could not update short course user", exception, new
                {
                    UserId = userId,
                    UserName = username,
                    FirstName = firstName,
                    LastName = lastName,
                    UsNumber = usNumber,
                    OuCode = ouCode,
                    EmailAddress = emailAddress
                });

                throw;
            }
        }

        #region CRM helper methods

        private static int GetOfferingsWithoutStartDateCount(List<stb_shortcourseoffering> offerings)
        {
            int count = 0;

            foreach (var offering in offerings)
            {
                if (offering.stb_StartDate.HasValue == false)
                {
                    count++;
                }
            }

            return count;
        }

        //private static Guid GetPresentationLanguageIdFromSisCode(string sisCode)
        //{
        //    using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
        //    {
        //        var crmPresentationLanguage = (from c in crmServiceContext.stb_PresentationLanguageSet
        //                                       where c.stb_SisCode == sisCode
        //                                       select c).FirstOrDefault();

        //        if (crmPresentationLanguage == null)
        //        {
        //            throw new Exception();
        //        }
        //        else
        //        {
        //            return crmPresentationLanguage.Id;
        //        }
        //    }
        //}

        private static bool ShortCourseExists(string shortCourseNumber)
        {
            using (var crmServiceContext = new CrmServiceContext(_orgService))
            {
                var shortCourse = (from s in crmServiceContext.stb_shortcourseSet
                                   where s.stb_ShortCourseNumber == shortCourseNumber
                                   select s).FirstOrDefault();

                if (shortCourse == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        private static Guid GetShortCourseId(string shortCourseNumber)
        {
            using (var crmServiceContext = new CrmServiceContext(_orgService))
            {
                var shortCourse = (from s in crmServiceContext.stb_shortcourseSet
                                   where s.stb_ShortCourseNumber == shortCourseNumber
                                   select s).FirstOrDefault();

                if (shortCourse == null)
                {
                    logger.LogWithContext(LogLevel.Error, "Could not find short course given short course number", new { ShortCourseNumber = shortCourseNumber });
                }

                return shortCourse.Id;
            }
        }

        private static int GetShortCourseYearOffered(Guid shortCourseId)
        {
            using (var crmServiceContext = new CrmServiceContext(_orgService))
            {
                var shortCourse = (from s in crmServiceContext.stb_shortcourseSet
                                   where s.stb_shortcourseId == shortCourseId
                                   select s).FirstOrDefault();

                if (shortCourse == null)
                {
                    logger.LogWithContext(LogLevel.Error, "Could not find short course given short course ID", new { ShortCourseId = shortCourseId });
                    throw new Exception();
                }

                if (shortCourse.stb_ApprovedYear.HasValue)
                {
                    return shortCourse.stb_ApprovedYear.Value.Year;
                }
                else
                {
                    return DateTime.MinValue.Year;
                }
            }
        }

        private static bool UserExists(string userName)
        {
            using (var crmServiceContext = new CrmServiceContext(_orgService))
            {
                var user = (from u in crmServiceContext.SystemUserSet
                            where u.DomainName == userName
                            select u).FirstOrDefault();

                if (user == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        private static void AssignRoleToUser(string roleName, Guid userId)
        {
            AssignRoleToUser(GetRoleId(roleName, GetUserBusinessUnit(userId)), userId);
        }

        private static void AssignRoleToUser(Guid roleId, Guid userId)
        {
            var associateRequest = new AssociateRequest();

            associateRequest.RelatedEntities = new EntityReferenceCollection();
            associateRequest.RelatedEntities.Add(new EntityReference("systemuser", userId));
            associateRequest.Relationship = new Relationship("systemuserroles_association");
            associateRequest.Target = new EntityReference("role", roleId);

            using (var organizationService = new OrganizationService(CrmServer.CrmConnection))
            {
                organizationService.Execute(associateRequest);
            }
        }

        private static Guid GetRoleId(string roleName, Guid businessUnitId)
        {
            using (var crmServiceContext = new CrmServiceContext(_orgService))
            {
                var role = (from r in crmServiceContext.RoleSet
                            where r.Name == roleName
                            where r.BusinessUnitId.Id == businessUnitId
                            select r).FirstOrDefault();

                if (role == null)
                {
                    logger.LogWithContext(LogLevel.Error, "Could not find role", new { RoleName = roleName, BusinessUnitId = businessUnitId });
                    throw new Exception();
                }

                return role.Id;
            }
        }

        private static Guid GetUserId(string userName)
        {
              using (var crmServiceContext = new CrmServiceContext(_orgService))
            {
                var user = (from u in crmServiceContext.SystemUserSet
                            where u.DomainName == userName
                            select u).FirstOrDefault();

                if (user == null)
                {
                    logger.LogWithContext(LogLevel.Error, "Could not find user given username", new { UserName = userName });
                    throw new Exception();
                }

                return user.Id;
            }
        }

        private static Guid GetUserBusinessUnit(Guid userId)
        {
            using (var crmServiceContext = new CrmServiceContext(_orgService))
            {
                var user = (from u in crmServiceContext.SystemUserSet
                            where u.Id == userId
                            select u).FirstOrDefault();

                if (user == null)
                {
                    logger.LogWithContext(LogLevel.Error, "Could not find user given user ID", new { UserId = userId });
                    throw new Exception();
                }

                return user.BusinessUnitId.Id;
            }
        }

        private static Guid GetBusinessUnitId(string businessUnitName)
        {
            using (var crmServiceContext = new CrmServiceContext(_orgService))
            {
                var businessUnit = (from b in crmServiceContext.BusinessUnitSet
                                    where b.Name == businessUnitName
                                    select b).FirstOrDefault();

                if (businessUnit == null)
                {
                    logger.LogWithContext(LogLevel.Error, "Could not find business unit given business unit name", new { BusinessUnitName = businessUnitName });
                    throw new Exception();
                }

                return businessUnit.Id;
            }
        }

        private static Guid GetBusinessUnitIdFromOUCode(string ouCode)
        {
            using (var crmServiceContext = new CrmServiceContext(_orgService))
            {
                var businessUnit = (from b in crmServiceContext.BusinessUnitSet
                                    where b.DivisionName == ouCode
                                    select b).FirstOrDefault();

                if (businessUnit == null)
                {
                    logger.LogWithContext(LogLevel.Error, "Could not find business unit given OU code", new { OUCode = ouCode });
                    throw new Exception("Could not find business unit.");
                }

                return businessUnit.Id;
            }
        }

        private static void SetShortCourseStatusToApproved(Guid shortCourseId)
        {
            using (var organizationService = new OrganizationService(CrmServer.CrmConnection))
            {
                SetStateRequest setStateRequest = new SetStateRequest()
                {
                    EntityMoniker = new EntityReference
                    {
                        Id = shortCourseId,
                        LogicalName = stb_shortcourse.EntityLogicalName,
                    },
                    State = new OptionSetValue(0),
                    Status = new OptionSetValue((int)stb_shortcourse_statuscode.Approved)
                };

                organizationService.Execute(setStateRequest);
            }
        }

        private static void SetOfferingStatusToNewOffering(Guid offeringId)
        {
            using (var organizationService = new OrganizationService(CrmServer.CrmConnection))
            {
                SetStateRequest setStateRequest = new SetStateRequest()
                {
                    EntityMoniker = new EntityReference
                    {
                        Id = offeringId,
                        LogicalName = stb_shortcourseoffering.EntityLogicalName,
                    },
                    State = new OptionSetValue(0),
                    Status = new OptionSetValue((int)stb_shortcourseoffering_statuscode.New)
                };

                organizationService.Execute(setStateRequest);
            }
        }

        private static void SetOwnershipOfShortCourse(Guid shortCourseId, Guid ownerId)
        {
            using (var organizationService = new OrganizationService(CrmServer.CrmConnection))
            {
                var assignRequest = new AssignRequest
                {
                    Assignee = new EntityReference(SystemUser.EntityLogicalName, ownerId),
                    Target = new EntityReference(stb_shortcourse.EntityLogicalName, shortCourseId)
                };

                organizationService.Execute(assignRequest);
            }
        }

        private static void SetOwnershipOfShortCourseOffering(Guid shortCourseOfferingId, Guid ownerId)
        {
            using (var organizationService = new OrganizationService(CrmServer.CrmConnection))
            {
                var assignRequest = new AssignRequest
                {
                    Assignee = new EntityReference(SystemUser.EntityLogicalName, ownerId),
                    Target = new EntityReference(stb_shortcourseoffering.EntityLogicalName, shortCourseOfferingId)
                };

                organizationService.Execute(assignRequest);
            }
        }

        private static bool UserHasRole(Guid userId, Guid roleId)
        {
            using (var organizationService = new OrganizationService(CrmServer.CrmConnection))
            {
                LinkEntity systemUserLink = new LinkEntity()
                {
                    LinkFromEntityName = SystemUserRoles.EntityLogicalName,
                    LinkFromAttributeName = "systemuserid",
                    LinkToEntityName = SystemUser.EntityLogicalName,
                    LinkToAttributeName = "systemuserid",
                    LinkCriteria =
                    {
                        Conditions =
                        {
                            new ConditionExpression("systemuserid", ConditionOperator.Equal, userId)
                        }
                    }
                };

                QueryExpression linkQuery = new QueryExpression()
                {
                    EntityName = Role.EntityLogicalName,
                    ColumnSet = new ColumnSet("roleid"),
                    LinkEntities =
                {
                    new LinkEntity()
                    {
                        LinkFromEntityName = Role.EntityLogicalName,
                        LinkFromAttributeName = "roleid",
                        LinkToEntityName = SystemUserRoles.EntityLogicalName,
                        LinkToAttributeName = "roleid",
                        LinkEntities = {systemUserLink}
                    }
                },
                    Criteria =
                    {
                        Conditions =
                        {
                            new ConditionExpression("roleid", ConditionOperator.Equal, roleId)
                        }
                    }
                };

                EntityCollection matchEntities = organizationService.RetrieveMultiple(linkQuery);

                return (matchEntities.Entities.Count > 0);
            }
        }

        private static bool UserHasRole(Guid userId, string roleName)
        {
            Guid userBusinessUnit = GetUserBusinessUnit(userId);

            return UserHasRole(userId, GetRoleId(roleName, userBusinessUnit));
        }

        #endregion

        #region RTAD queries

        private static string GetDomainOfUsername(string username)
        {
            string domain = null;

            try
            {
                RtadQueries.ServiceUrl = Properties.Settings.Default.RtadServiceUrl;
                RtadQueries.ServiceUser = Properties.Settings.Default.RtadServiceUsername;
                RtadQueries.ServicePassword = Properties.Settings.Default.RtadServicePassword;
                RtadQueries.ProxyUser = Properties.Settings.Default.RtadServiceProxyUsername;
                RtadQueries.ProxyPassword = Properties.Settings.Default.RtadServiceProxyPassword;

                domain = RtadQueries.GetDomainOfUsername(username);
            }
            catch (Exception exception)
            {
                logger.LogWithContext(LogLevel.Error, "Could not get domain of username " + username + " from RTAD", "");
            }

            return domain;
        }

        private static string GetUsername(string usNumber)
        {
            string username = null;

            try
            {
                RtadQueries.ServiceUrl = Properties.Settings.Default.RtadServiceUrl;
                RtadQueries.ServiceUser = Properties.Settings.Default.RtadServiceUsername;
                RtadQueries.ServicePassword = Properties.Settings.Default.RtadServicePassword;
                RtadQueries.ProxyUser = Properties.Settings.Default.RtadServiceProxyUsername;
                RtadQueries.ProxyPassword = Properties.Settings.Default.RtadServiceProxyPassword;

                username = RtadQueries.GetUsernameOfUsNumber(usNumber);
            }
            catch (Exception exception)
            {
                logger.LogWithContext(LogLevel.Error, "Could not get username for US number " + usNumber + " from RTAD", exception, "");
            }

            return username;
        }

        #endregion

        private static string GetFullyQualifiedUsername(string username)
        {
            string domain = GetDomainOfUsername(username);

            if (String.IsNullOrEmpty(domain))
            {
                logger.LogWithContext(LogLevel.Error, "Could not get domain of username " + username + " from RTAD", "");
                domain = "BELPARK";     // default to BELPARK domain
            }

            return domain + "\\" + username;
        }

        private static Guid GetCurrencyId(string currencyName)
        {
            using (var crmServiceContext = new CrmServiceContext(_orgService))
            {
                var crmCurrency = (from c in crmServiceContext.TransactionCurrencySet
                                   where c.CurrencyName == currencyName
                                   select c).FirstOrDefault();

                if (crmCurrency == null)
                {
                    throw new Exception();
                }
                else
                {
                    return crmCurrency.Id;
                }
            }
        }
    }
}
