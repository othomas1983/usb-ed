﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mercator;
using StellenboschUniversity.Messaging.Oic.Dto;
using StellenboschUniversity.UsbEd.Integration.Crm;
using StellenboschUniversity.Messaging.Oic.Dto.Address;

namespace StellenboschUniversity.UsbEd.Integration.Maps.MessageTypes
{
    public static class AddressMaps
    {
        private static Mapper<U_ADRES_NAT_UPD_V3, Contact> naturalAddressMap;
        private static Mapper<U_ADRES_HRMS_UPD_V3, Contact> hrAddressMap;
        private static Mapper<U_ADRES_DNR_UPD_V2, Contact> donorAddressMap;
        private static Mapper<U_ADRES_USBCRM_UPD_V2, Contact> usbAddressMap;

        static AddressMaps()
        {
            InitializeNaturalMap();
            InitializeHR();
            InitializeDonor();
            InitializeUsb();
        }

        public static void InitializeNaturalMap()
        {
            naturalAddressMap = new Mapper<U_ADRES_NAT_UPD_V3, Contact>();

            naturalAddressMap.AddRule("ADR_LYN1", "address1_line1");
            naturalAddressMap.AddRule("ADR_LYN3", "stb_address1_Suburb");
            naturalAddressMap.AddRule("ADR_LYN4", "stb_address1_City");
            naturalAddressMap.AddRule("ADR_LYN5", "address1_stateorprovince");
            naturalAddressMap.AddRule("ZIP_CODE", "stb_address1_postalcode");
            naturalAddressMap.AddRule("ADR_LYN1", "address1_line1");

            naturalAddressMap.AddRule(oicMessage =>
            {
                return oicMessage.ADR_LYN2 + " " + oicMessage.ADR_LYN2A + " " + oicMessage.ADR_LYN2B;
            }
            , "address1_line2");
        }

        public static void InitializeHR()
        {
            hrAddressMap = new Mapper<U_ADRES_HRMS_UPD_V3, Contact>();

            hrAddressMap.AddRule("ADR_LYN1", "address1_line1");
            hrAddressMap.AddRule("ADR_LYN3", "stb_address1_Suburb");
            hrAddressMap.AddRule("ADR_LYN4", "stb_address1_City");
            hrAddressMap.AddRule("ADR_LYN5", "address1_stateorprovince");
            hrAddressMap.AddRule("ZIP_CODE", "stb_address1_postalcode");
            hrAddressMap.AddRule("ADR_LYN1", "address1_line1");

            naturalAddressMap.AddRule(oicMessage =>
            {
                return oicMessage.ADR_LYN2 + " " + oicMessage.ADR_LYN2A + " " + oicMessage.ADR_LYN2B;
            }
            , "address1_line2");
        }

        public static void InitializeDonor()
        {
            donorAddressMap = new Mapper<U_ADRES_DNR_UPD_V2, Contact>();

            donorAddressMap.AddRule("ADR_LYN1", "address1_line1");
            donorAddressMap.AddRule("ADR_LYN3", "stb_address1_Suburb");
            donorAddressMap.AddRule("ADR_LYN4", "stb_address1_City");
            donorAddressMap.AddRule("ADR_LYN5", "address1_stateorprovince");
            donorAddressMap.AddRule("ZIP_CODE", "stb_address1_postalcode");
            donorAddressMap.AddRule("ADR_LYN1", "address1_line1");

            donorAddressMap.AddRule(oicMessage =>
            {
                return oicMessage.ADR_LYN2 + " " + oicMessage.ADR_LYN2A + " " + oicMessage.ADR_LYN2B;
            }
            , "address1_line2");
        }

        public static void InitializeUsb()
        {
            usbAddressMap = new Mapper<U_ADRES_USBCRM_UPD_V2, Contact>();

            usbAddressMap.AddRule("ADR_LYN1", "address1_line1");
            usbAddressMap.AddRule("ADR_LYN3", "stb_address1_Suburb");
            usbAddressMap.AddRule("ADR_LYN4", "stb_address1_City");
            usbAddressMap.AddRule("ADR_LYN5", "address1_stateorprovince");
            usbAddressMap.AddRule("ZIP_CODE", "stb_address1_postalcode");
            usbAddressMap.AddRule("ADR_LYN1", "address1_line1");

            usbAddressMap.AddRule(oicMessage =>
            {
                return oicMessage.ADR_LYN2 + " " + oicMessage.ADR_LYN2A + " " + oicMessage.ADR_LYN2B;
            }
            , "address1_line2");
        }

        public static Mapper<U_ADRES_NAT_UPD_V3, Contact> Natural
        {
            get
            {
                return naturalAddressMap;
            }
        }

        public static Mapper<U_ADRES_HRMS_UPD_V3, Contact> HR
        {
            get
            {
                return hrAddressMap;
            }
        }

        public static Mapper<U_ADRES_DNR_UPD_V2, Contact> Donor
        {
            get
            {
                return donorAddressMap;
            }
        }

        public static Mapper<U_ADRES_USBCRM_UPD_V2, Contact> Usb
        {
            get
            {
                return usbAddressMap;
            }
        }
    }
}
