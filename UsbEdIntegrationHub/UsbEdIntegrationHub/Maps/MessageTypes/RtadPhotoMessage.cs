﻿using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk;
using StellenboschUniversity.Messaging.Oic.Dto.Rtad;
using StellenboschUniversity.UsbEd.Integration.Crm;
using StellenboschUniversity.UsbEd.Integration.Crm.Utilities;
using System;
using System.Linq;

namespace StellenboschUniversity.UsbEd.Integration.Maps.MessageTypes
{
    public class RtadPhotoMessage
    {
        private static CrmConnection crmConnection = new CrmConnection(Properties.Settings.Default.CrmConnectionString);
        private static IOrganizationService _orgService = new OrganizationService(crmConnection);
        public static void Process(FOTO message)
        {
            var usNumber = message.US_NOMMER;
            var contactId = GetContactWithUsNumber(usNumber);

            if (contactId.HasValue == false)
            {
                return;
            }

            var photoData = Convert.FromBase64String(message.FOTO_BASE64.Replace(" ", "").Replace("\r", "").Replace("\n", ""));

            if (photoData.Length > 0)
            {
                UpdateContactPhoto(contactId.Value, photoData);
            }
        }
       
        private static Guid? GetContactWithUsNumber(string usNumber)
        {
            Contact contact;

            using (var crmServiceContext = new CrmServiceContext(_orgService))
            {
                contact = (from c in crmServiceContext.ContactSet
                           where c.stb_USNumber == usNumber
                           select c).FirstOrDefault();
            }

            if (contact == null)
            {
                return null;
            }
            else
            {
                return contact.Id;
            }
        }

        private static void UpdateContactPhoto(Guid contactId, byte[] photoData)
        {
            using (var crmServiceContext = new CrmServiceContext(_orgService))
            {
                var contact = new Contact();
                contact.ContactId = contactId;
                contact.EntityImage = photoData;
                //contact.EntityState = EntityState.Changed;

                crmServiceContext.Attach(contact);
                crmServiceContext.UpdateObject(contact);
                crmServiceContext.SaveChanges();
            }
        }
    }
}
