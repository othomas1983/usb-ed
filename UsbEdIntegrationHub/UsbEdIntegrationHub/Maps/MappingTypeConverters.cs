﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using Microsoft.Xrm.Client;
using StellenboschUniversity.UsbEd.Integration.Crm.Utilities;

namespace StellenboschUniversity.UsbEd.Integration.Maps
{
    public static class MappingTypeConverters
    {
        private static string codeField = "stb_siscode";
        private static string alternateCodeField = "stb_afrigiscode";

        public static CrmEntityReference ConvertSisCodeToCrmEntityReference(string referencedEntityName, string sisCode)
        {
            if (String.IsNullOrEmpty(sisCode))
            {
                return null;
            }

            Guid? entityId;

            if (CrmUtilities.EntityHasField(referencedEntityName, codeField))
            {
                entityId = CrmUtilities.RetrieveEntityIdWithKeyField(referencedEntityName, codeField, sisCode);
            }
            else
            {
                if (CrmUtilities.EntityHasField(referencedEntityName, alternateCodeField))
                {
                    entityId = CrmUtilities.RetrieveEntityIdWithKeyField(referencedEntityName, alternateCodeField, sisCode);
                }
                else
                {
                    throw new Exception(String.Format("Referenced entity does not contain a SIS code field (either {0} or {1})", codeField, alternateCodeField));
                }
            }

            if (entityId.HasValue)
            {
                return new CrmEntityReference(referencedEntityName, entityId.Value);
            }
            else
            {
                return null;
            }
        }

        public static DateTime? ConvertSisDateToDateTime(string sisDate)
        {
            DateTime? result;

            try
            {
                result = DateTime.ParseExact(sisDate, "yyyyMMdd", CultureInfo.InvariantCulture).ToLocalTime();
            }
            catch
            {
                result = null;
            }

            return result;
        }
    }
}
