﻿using System;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using NLog;
using StellenboschUniversity.Messaging.Oic.Dto;
using StellenboschUniversity.Integration.Messaging.Oic;
using StellenboschUniversity.UsbEd.Integration.Crm;
using StellenboschUniversity.UsbEd.Integration.Maps;
using StellenboschUniversity.UsbEd.Integration.Maps.MessageTypes;
using StellenboschUniversity.UsbEd.Integration.Crm.Utilities;
using StellenboschUniversity.Integration.Messaging.Oic.Relay;
using StellenboschUniversity.Messaging.Oic.Dto.Rtad;
using StellenboschUniversity.Messaging.Oic.Dto.Address;
using StellenboschUniversity.Messaging.Oic.Dto.Person;
using Microsoft.Xrm.Sdk;

namespace StellenboschUniversity.UsbEd.Integration
{
    public static class MappingAgent
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private static OicRelayClient oicClient;
        private static string postalAddressTypeCode = "P";
        private static string messageUsNumberField = "US_NOMMER";
        private static string messageSequenceNumberField = "MSG_SEQ_NO";
        private static string messageAddressTypeField = "ADR_TIPE_KODE";
        private static string messageSequenceNativeCode = "USBED";
        private static string messagePath;

        private static CrmConnection crmConnection = new CrmConnection(Properties.Settings.Default.CrmConnectionString);
        private static IOrganizationService _orgService = new OrganizationService(crmConnection);

        static MappingAgent()
        {
            oicClient = new OicRelayClient(Properties.Settings.Default.OicRelayAddress, Properties.Settings.Default.OicRelayQueueName);
        }
        
        public static string MessagePath
        {
            get
            {
                return messagePath;
            }

            set
            {
                messagePath = value;
            }
        }

        public static void HandleMessage(string message)
        {
            if (String.IsNullOrEmpty(message))
            {
                return;
            }
            
            logger.Info("In HandleMessage with message=" + message);
            
            var messageXml = XDocument.Parse(message);

            if (IsMessageRelevant(messageXml) == false)
            {
                return;
            }

            var messageType = messageXml.Root.Name.ToString();
            var messageUsNumber = GetMessageUsNumber(messageXml);
            var contact = GetContactWithUsNumber(messageUsNumber);

            logger.Info("Dequeued message of type " + messageType);

            switch (messageType)
            {
                #region Person

                case "PERSOON_NAT_UPD_V4":

                    if (contact != null)
                    {
                        var personMessageNatural = OicMessageSerializer.Deserialize<PERSOON_NAT_UPD_V4>(message);
                        Mappers.PersonNatural.Map(personMessageNatural, contact);
                        SaveContactToCrm(contact);
                    }

                    break;

                case "PERSOON_HRMS_UPD_V3":

                    if (contact != null)
                    {
                        var personMessageHR = OicMessageSerializer.Deserialize<PERSOON_HRMS_UPD_V3>(message);
                        Mappers.PersonHR.Map(personMessageHR, contact);
                        SaveContactToCrm(contact);
                    }

                    break;

                case "PERSOON_DNR_UPD_V4":

                    if (contact != null)
                    {
                        var personMessageDonor = OicMessageSerializer.Deserialize<PERSOON_DNR_UPD_V4>(message);
                        Mappers.PersonDonor.Map(personMessageDonor, contact);
                        SaveContactToCrm(contact);
                    }

                    break;

                case "PERSOON_USBCRM_UPD_V5":

                    if (contact != null)
                    {
                        var personMessageUsb = OicMessageSerializer.Deserialize<PERSOON_USBCRM_UPD_V5>(message);
                        Mappers.PersonUsb.Map(personMessageUsb, contact);
                        SaveContactToCrm(contact);
                    }

                    break;

                #endregion

                #region Address

                case "U_ADRES_NAT_UPD_V3":

                    if (contact != null)
                    {
                        var addressMessageNatural = OicMessageSerializer.Deserialize<U_ADRES_NAT_UPD_V3>(message);
                        Mappers.AddressNatural.Map(addressMessageNatural, contact);
                        SaveContactToCrm(contact);
                    }

                    break;

                case "U_ADRES_HRMS_UPD_V3":

                    if (contact != null)
                    {
                        var addressMessageHR = OicMessageSerializer.Deserialize<U_ADRES_HRMS_UPD_V3>(message);
                        Mappers.AddressHR.Map(addressMessageHR, contact);
                        SaveContactToCrm(contact);
                    }

                    break;

                case "U_ADRES_DNR_UPD_V2":

                    if (contact != null)
                    {
                        var addressMessageDonor = OicMessageSerializer.Deserialize<U_ADRES_DNR_UPD_V2>(message);
                        Mappers.AddressDonor.Map(addressMessageDonor, contact);
                        SaveContactToCrm(contact);
                    }

                    break;

                case "U_ADRES_USBCRM_UPD_V2":

                    if (contact != null)
                    {
                        var addressMessageUsb = OicMessageSerializer.Deserialize<U_ADRES_USBCRM_UPD_V2>(message);
                        Mappers.AddressUsb.Map(addressMessageUsb, contact);
                        SaveContactToCrm(contact);
                    }

                    break;

                #endregion

                #region Telephone

                case "PERSOON_RTAD_WTEL_UPD_V2":

                    if (contact != null)
                    {
                        var telephoneMessageRtad = OicMessageSerializer.Deserialize<PERSOON_RTAD_WTEL_UPD_V2>(message);
                        Mappers.TelephoneRtad.Map(telephoneMessageRtad, contact);
                        SaveContactToCrm(contact);
                    }

                    break;

                case "PERSOON_TELEFOON_HRMS_UPD_V2":

                    if (contact != null)
                    {
                        var telephoneMessageHR = OicMessageSerializer.Deserialize<PERSOON_TELEFOON_HRMS_UPD_V2>(message);
                        Mappers.TelephoneHR.Map(telephoneMessageHR, contact);
                        SaveContactToCrm(contact);
                    }

                    break;

                #endregion

                #region Email

                case "PERSOON_RTAD_EMAIL_UPD_V2":

                    if (contact != null)
                    {
                        var emailMessageRtad = OicMessageSerializer.Deserialize<PERSOON_RTAD_EMAIL_UPD_V2>(message);
                        Mappers.EmailRtad.Map(emailMessageRtad, contact);
                        SaveContactToCrm(contact);
                    }

                    break;

                #endregion

                #region Short Course

                case "SC_GENERIC":

                    logger.Warn("Received Create Short Course Message: {0}", message);
                    var shortCourseCreate = OicMessageSerializer.Deserialize<SC_GENERIC>(message);
                    CreateShortCourseMessage.Process(shortCourseCreate);

                    break;

                #endregion

                #region RTAD

                case "FOTO":

                    var photoMessage = OicMessageSerializer.Deserialize<FOTO>(message);
                    RtadPhotoMessage.Process(photoMessage);

                    break;

                #endregion

                default:
                    break;
            }
        }

        public static string LoadMessageFromFile()
        {
            string message;

            using (var sr = new StreamReader(messagePath))
            {
                message = sr.ReadToEnd();
            }

            return message;
        }

        private static string GetMessageUsNumber(XDocument messageXml)
        {
            foreach (XElement element in messageXml.Root.Elements())
            {
                if (element.Name.ToString() == messageUsNumberField)
                {
                    return element.Value;
                }
            }

            return null;
        }

        private static bool IsMessageRelevant(XDocument messageXml)
        {
            foreach (XElement element in messageXml.Root.Elements())
            {
                string elementName = element.Name.ToString();
                
                if (elementName == messageSequenceNumberField)
                {
                    if (element.Value.Contains(messageSequenceNativeCode))
                    {
                        return false;
                    }
                }

                if (elementName == messageAddressTypeField)
                {
                    if (element.Value != postalAddressTypeCode)
                    {
                        return false;
                    }
                }
            }

            return true;
        }
       
        private static Contact GetContactWithUsNumber(string usNumber)
        {
            if (String.IsNullOrEmpty(usNumber))
            {
                return null;
            }

            using (var crmServiceContext = new CrmServiceContext(_orgService))
            {
                return crmServiceContext.ContactSet.Where(p => p.stb_USNumber == usNumber).FirstOrDefault();
            }
        }

        private static void SaveContactToCrm(Contact contact)
        {
            using (var crmServiceContext = new CrmServiceContext(_orgService))
            {
                crmServiceContext.Attach(contact);
                crmServiceContext.UpdateObject(contact);
                crmServiceContext.SaveChanges();
            }
        }

        public static string DequeueMessage()
        {
            return oicClient.Dequeue();
        }
    }
}
