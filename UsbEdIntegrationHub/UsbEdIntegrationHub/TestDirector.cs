﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mercator;
using StellenboschUniversity.Messaging.Oic.Dto;
using StellenboschUniversity.UsbEd.Integration.Crm;
using StellenboschUniversity.UsbEd.Integration.Maps;
using StellenboschUniversity.Messaging.Oic.Dto.Person;

namespace StellenboschUniversity.UsbEd.Integration
{
    public static class TestDirector
    {
        private static string persoonMessagePath = @"D:\Source\InfrastructureDotNet\OicMessages\Examples\Person\PERSOON_USBCRM_UPD_V5_1.xml";

        public static void TestCrmToOic()
        {
            PERSOON_NAT_UPD_V4 personObject = new PERSOON_NAT_UPD_V4();
            personObject.US_VAN = "Surname";
            personObject.US_VOORNAME = "First Names";
                        
            var messageXmlBytes = OicMessageSerializer.Serialize(personObject, typeof(PERSOON_NAT_UPD_V4));
            var messageXml = Encoding.GetEncoding("iso-8859-1").GetString(messageXmlBytes);

            Console.WriteLine();
        }

        public static void TestPerson()
        {
            var message = (new StreamReader(persoonMessagePath)).ReadToEnd();
            var messageType = OicMessageSerializer.GetMessageType(message);
            var messageObject = OicMessageSerializer.Deserialize(message, messageType);

            //MapperInitializer.InitializeOicToCrmMaps(messageType);
            //var destination = Mapper.DynamicMap<Contact>(messageObject);

            //CrmServer.OrganizationService.Update(destination);

            Console.WriteLine();
        }

        public static void TestAddress()
        {

        }

        public static void TestTelephone()
        {

        }
    }
}
