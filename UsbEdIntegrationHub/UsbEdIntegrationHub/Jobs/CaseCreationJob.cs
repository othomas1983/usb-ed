﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quartz;

namespace StellenboschUniversity.ShortCoursesDivision.IntegrationServer.Jobs
{
    [DisallowConcurrentExecution]
    public class CaseCreationJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            try
            {
                Console.WriteLine("Executing CaseCreationJob.");

                //var caseCreator = new CaseCreator(Settings.CrmConnectionString);
                //caseCreator.SynchronizeLogEvents();
            }
            catch (Exception e)
            {
                // best practice is to catch all exceptions in IJob.Execute method
            }
        }

        public static string Name
        {
            get
            {
                return typeof(CaseCreationJob).ToString();
            }
        }
    }
}
