﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quartz;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;

namespace StellenboschUniversity.ShortCoursesDivision.IntegrationServer.Jobs
{
    //[DisallowConcurrentExecution]
    //public class CourseExpiryJob : IJob
    //{
    //    private const int expiryYearDifference = 2;

    //    public void Execute(IJobExecutionContext context)
    //    {
    //        try
    //        {
    //            Console.WriteLine("Executing CourseExpiryJob.");

    //            ScdEventSource.Log.IntegrationServerCourseExpiryJobStarted();

    //            if (DateTime.Now.Month == 12)
    //            {
    //                var coursesToExpire = GetCoursesToExpire();

    //                foreach (var courseToExpire in coursesToExpire)
    //                {
    //                    ExpireCourse(courseToExpire.Item1, courseToExpire.Item2);
    //                }
    //            }
    //        }
    //        catch (Exception exception)
    //        {
    //            // best practice is to catch all exceptions in IJob.Execute method
    //            ScdEventSource.Log.IntegrationServerCourseExpiryUnknownError(exception.Message);
    //        }
    //    }

    //    private void ExpireCourse(Guid courseId, string courseNumber)
    //    {
    //        using (var organizationService = new OrganizationService(CrmConnection.Parse(Settings.CrmConnectionString)))
    //        {
    //            SetStateRequest setStateRequest = new SetStateRequest()
    //            {
    //                EntityMoniker = new EntityReference
    //                {
    //                    Id = courseId,
    //                    LogicalName = stb_shortcourse.EntityLogicalName,
    //                },
    //                State = new OptionSetValue(1),
    //                Status = new OptionSetValue((int)stb_shortcourse_statuscode.ApprovalExpired)
    //            };

    //            organizationService.Execute(setStateRequest);
    //        }

    //        ScdEventSource.Log.IntegrationServerCourseMarkedAsExpired(courseId, courseNumber);
    //    }

    //    private List<Tuple<Guid, string>> GetCoursesToExpire()
    //    {
    //        int currentYear = DateTime.Now.Year;

    //        var result = new List<Tuple<Guid, string>>();

    //        using (var crmServiceContext = new CrmServiceContext(CrmConnection.Parse(Settings.CrmConnectionString)))
    //        {
    //            var courseApprovedYears = from c in crmServiceContext.stb_shortcourseSet
    //                                      where c.statuscode == (int)stb_shortcourse_statuscode.Approved
    //                                      select new { Id = c.stb_shortcourseId, Number = c.stb_ShortCourseNumber, ApprovedYear = c.stb_ApprovedYear };

    //            foreach (var courseApprovedYear in courseApprovedYears)
    //            {
    //                if (courseApprovedYear.Id.HasValue && courseApprovedYear.ApprovedYear.HasValue)
    //                {
    //                    int approvedYear = courseApprovedYear.ApprovedYear.Value.Year;

    //                    if (currentYear - approvedYear >= expiryYearDifference)
    //                    {
    //                        result.Add(new Tuple<Guid, string>(courseApprovedYear.Id.Value, courseApprovedYear.Number));
    //                    }
    //                }
    //            }
    //        }

    //        return result;
    //    }

    //    public static string Name
    //    {
    //        get
    //        {
    //            return typeof(CourseExpiryJob).ToString();
    //        }
    //    }
    //}
}
