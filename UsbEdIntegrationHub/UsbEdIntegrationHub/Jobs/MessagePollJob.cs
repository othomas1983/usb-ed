﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quartz;
using NLog;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using StellenboschUniversity.UsbEd.Integration;
using StellenboschUniversity.Utilities.NLogGelfExtensions;


namespace StellenboschUniversity.ShortCoursesDivision.IntegrationServer.Jobs
{
    [DisallowConcurrentExecution]
    public class MessagePollJob : IJob
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public void Execute(IJobExecutionContext context)
        {
            string message = "";

            try
            {
                Initialize();

                try
                {
#if DEBUG
                    //message = MappingAgent.LoadMessageFromFile();
                    message = MappingAgent.DequeueMessage();
#else
                    message = MappingAgent.DequeueMessage();
#endif
                }
                catch (Exception exception)
                {
                    logger.Error(exception, "Error occurred while trying to poll OIC message queue");
                }

                logger.Info("HandleMessage: " + message);
                MappingAgent.HandleMessage(message);
            }
            catch (Exception exception2)
            {
                string exceptionDetails = exception2.ToString();

                if (exception2.InnerException != null)
                {
                    exceptionDetails += " InnerException: " + exception2.InnerException.ToString();

                    if (exception2.InnerException.InnerException != null)
                    {
                        exceptionDetails += " InnerException: " + exception2.InnerException.InnerException.ToString();
                    }
                }

                logger.Error("Message polling job error. Exception: " + exceptionDetails + " Message: " + message);
            }
        }

        private void Initialize()
        {
            MappingAgent.MessagePath = @"D:\Temp\OIC messages\testSC.xml";
        }

        public static string Name
        {
            get
            {
                return typeof(MessagePollJob).ToString();
            }
        }
    }
}
