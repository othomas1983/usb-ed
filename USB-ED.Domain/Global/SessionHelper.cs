﻿using System.Security;
using USB.Models.FacultyManagement;
using System.Web;
using System.Web.Configuration;

namespace USB_ED.Global
{
    public class SessionHelper
    {
        public static User LoggedInUser
        {
            get
            {
                if (HttpContext.Current.Session != null && HttpContext.Current.Session["user"] != null)
                {
                    return (User)HttpContext.Current.Session["user"];
                }
                throw new SecurityException("Your session has timed out. Please sign in again.");
            }
            set { HttpContext.Current.Session["user"] = value; }
        }

        public static bool HasLoadedUser()
        {
            if (SessionHelper.LoggedInUser.LoadedUserCVID == null)
            {
                return false;
            }

            return SessionHelper.LoggedInUser.LoadedUserCVID != SessionHelper.LoggedInUser.CVID;
        }
    }

    public static class FeatureSwitch
    {
        public static bool LegacyLogin => bool.Parse(WebConfigurationManager.AppSettings["LegacyLogIn"]);
    }
}