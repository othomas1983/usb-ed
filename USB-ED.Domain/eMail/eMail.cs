﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using USB_ED.Models;
using USB_ED.Models.VenueBooking;
using SettingManager = USB_ED.Properties.Settings;

namespace USB_ED
{
    public class eMail
    {
        const string boldStart = "<b>";
        const string boldEnd = "</b>";
        const string newLine = "<br/>";
        const string space = "&nbsp";

        MailMessage mail = new MailMessage();
        SmtpClient smtpServer = new SmtpClient();

        public void SendVenueBookingMail(VenueBookingForm model)
        {
            var smptClient = SettingManager.Default.SMTPClient;
            // var userName = SettingManager.Default.MailUserName;
            //var pwd = SettingManager.Default.MailPwd;
            var portNumber = SettingManager.Default.MailPortNumber;

            smtpServer = new SmtpClient(smptClient);
            smtpServer.Port = portNumber;
            smtpServer.UseDefaultCredentials = false;

            SendMailToClient(model);

            SendMailToVenue(model);

            //SendMailToCatering(attachment);
        }

        private void SendMailToClient(VenueBookingForm model)
        {
            try
            {
                mail.To.Add(model.eMail);
                mail.From = new MailAddress(SettingManager.Default.MailUserName);
                mail.Subject = SettingManager.Default.ClientMailSubject;
                mail.IsBodyHtml = true;
                mail.Body = BodyHeading() + Environment.NewLine + VenueBookingBody(model);
                
                smtpServer.Send(mail);
            }
            catch (Exception ex)
            {
                var exceptionMessage = ex.Message;
                //do nothing
            }
        }

        private string BodyHeading()
        {
            var heading = "Thank you for choosing to host your event with us." + newLine +
                            "Your booking will be confirmed, subject to availability, within 48 hours and sent to the e- mail address or mobile number provided." + newLine +
                            "We look forward to the pleasure of having you as our guest." + newLine + newLine +
                            "Sincerely" + newLine + newLine +
                            "University of Stellenbosch" + newLine + newLine +
                            "Bellville Park Campus" + newLine + newLine +
                            "E-mail: venues@belpark.sun.ac.za" + newLine + newLine + newLine;
            return heading;
        }

        private string VenueBookingBody(VenueBookingForm model)
        {
            var result = model.FullDay ? "Full Day Event" : "Half Day Event";
            
            var mailBody = "<h2>Venue Booking</h2>" +
                    "<b>Event Information - " + result + "</b>" + newLine +
                    "Event Name: " + model.EventName + newLine +
                    "Number of people expected: " + model.BookingNumberOfPeopleExpected + newLine +
                    "Booking Date: " + model.BookingDate + newLine +
                    "Time In: " + model.BookingTimeIn + newLine +
                    "Time Out: " + model.BookingTimeOut + newLine +

                    "<b>Billing Information</b>" + newLine +
                    "Person Requesting Venue: " + model.PersonRequestingVenue + newLine +
                    "Contact Number: " + model.ContactNumber + newLine +
                    "E-Mail: " + model.eMail + newLine +
                    "Company Name: " + model.CompanyName + newLine +
                    "Billing Address: " + model.BillingAddress + newLine +
                    "VAT Number: " + model.VATNumber + newLine +
                    "VAT Registration Number: " + model.VATRegistrationNumber + newLine +
                    "Person To Be Billed: " + model.PersonToBeBilled + newLine +

                    "<b>Catering Requirements</b>" + newLine;

                    if (model.CateringRequirements.CateringPackageA)
                    {
                        mailBody += " Package A" + newLine +
                            "Tea & Coffee on arrival." + newLine +
                            "Mid Morning Tea & Coffee served with a biscuit." + newLine +
                            "Meal of the day with a glass of juice OR cans." + newLine +
                            "Tea & Coffee." + newLine +

                            "Arrival Time" + model.CateringRequirements.CateringPackageATeaCoffeeArrivalTime + newLine +
                            "Midmorning Time" + model.CateringRequirements.CateringPackageATeaCoffeeMidmorningTime + newLine +
                            "Afternoon Time" + model.CateringRequirements.CateringPackageATeaCoffeeAfternoonTime + newLine +
                            "Lunch Time" + model.CateringRequirements.CateringPackageALunchTime + newLine;
                    }

                    if (model.CateringRequirements.CateringPackageB)
                    {
                        mailBody += " Package B" + newLine +
                        "Tea & Coffee on arrival." + newLine +
                        "Mid Morning Tea & Coffee served with Tartlets." + newLine +
                        "Buffet lunch with a glass of juice OR cans." + newLine +
                        "Afternoon Tea & Coffee with biscuits." + newLine +

                        "Arrival Time" + model.CateringRequirements.CateringPackageBTeaCoffeeArrivalTime + newLine +
                        "Midmorning Time" + model.CateringRequirements.CateringPackageBTeaCoffeeMidmorningTime + newLine +
                        "Afternoon Time" + model.CateringRequirements.CateringPackageBTeaCoffeeAfternoonTime + newLine +
                        "Lunch Time" + model.CateringRequirements.CateringPackageBLunchTime + newLine;
                    }

                    if (model.CateringRequirements.CateringPackageC)
                    {
                        mailBody += " Package C" + newLine +
                        "Tea & Coffee on arrival." + newLine +
                        "Mid Morning Tea & Coffee served with a tea snack." + newLine +
                        "Buffet lunch with a glass of juice OR cans." + newLine +
                        "Tea & Cofee with biscuits." + newLine +

                        "Arrival Time " + model.CateringRequirements.CateringPackageCTeaCoffeeArrivalTime + newLine +
                        "Midmorning Time " + model.CateringRequirements.CateringPackageCTeaCoffeeMidmorningTime + newLine +
                        "Afternoon Time " + model.CateringRequirements.CateringPackageCTeaCoffeeAfternoonTime + newLine +
                        "Lunch Time " + model.CateringRequirements.CateringPackageCLunchTime + newLine;
                    }

                    var props = model.CateringRequirements.GetType().GetProperties().Where(
                        prop => Attribute.IsDefined(prop, typeof(RequirementAttribute)));

                    foreach (var item in props)
                    {
                        var selected = model.CateringRequirements.GetType().GetProperty(item.Name).GetValue(model.CateringRequirements).AsBool().GetValueOrDefault();

                        if (selected)
                        {
                            object[] attribute = item.GetCustomAttributes(typeof(RequirementAttribute), true);
                            var displayName = (attribute[0] as RequirementAttribute).DisplayName;
                            var hasTimes = (attribute[0] as RequirementAttribute).HasTimes;
                            var count = 0;
                            if (model.CateringRequirements.GetType().GetProperty(item.Name + "Count").GetValue(model.CateringRequirements).IsNotNull())
                            {
                                count = (int)model.CateringRequirements.GetType().GetProperty(item.Name + "Count").GetValue(model.CateringRequirements);
                            }

                            if (hasTimes)
                            {
                                var morningTime = model.CateringRequirements.GetType().GetProperty(item.Name + "MorningTime").GetValue(model.CateringRequirements).AsString();
                                var midMorningTime = model.CateringRequirements.GetType().GetProperty(item.Name + "MidMorningTime").GetValue(model.CateringRequirements).AsString();
                                var lunchTime = model.CateringRequirements.GetType().GetProperty(item.Name + "LunchTime").GetValue(model.CateringRequirements).AsString();
                                var AfternoonTime = model.CateringRequirements.GetType().GetProperty(item.Name + "AfternoonTime").GetValue(model.CateringRequirements).AsString();
                                var eveningTime = model.CateringRequirements.GetType().GetProperty(item.Name + "EveningTime").GetValue(model.CateringRequirements).AsString();

                                mailBody += CateringRequirements(displayName.AddSpaceBeforeCapital(),
                                count,
                                morningTime,
                                midMorningTime,
                                lunchTime,
                                AfternoonTime,
                                eveningTime);
                            }
                            else
                            {
                                mailBody += CateringRequirements(displayName, count);
                            }
                        }
                    }

            return mailBody;
        }

        private void SendMailToVenue(VenueBookingForm model)
        {
            try
            {
                mail = new MailMessage();

                mail.To.Add(SettingManager.Default.VenueMailAddress);
                mail.From = new MailAddress(SettingManager.Default.MailUserName);
                mail.Subject = SettingManager.Default.VenueMailSubject;
                mail.Body = SettingManager.Default.VenueMailBody + VenueBookingBody(model);

                smtpServer.Send(mail);
            }
            catch (Exception ex)
            {
                var exceptionMessage = ex.Message;
                //do nothing
            }
        }

        private void SendMailToCatering(Attachment attachment)
        {
            try
            {
                mail = new MailMessage();

                mail.To.Add(SettingManager.Default.CateringMailAddress);
                mail.Subject = SettingManager.Default.CateringMailSubject;
                mail.Body = SettingManager.Default.CateringMailBody;
                mail.Attachments.Add(attachment);

                smtpServer.Send(mail);
            }
            catch (Exception ex)
            {
                var exceptionMessage = ex.Message;
                //do nothing
            }
        }

        private string CateringPackageItems(string itemName, string itemTime)
        {
            return itemName + ": " + itemTime + newLine;
        }

        private string CateringRequirements(string requirement, int? count)
        {
            var requirements = string.Empty;
            requirements = boldStart + requirement + boldEnd + newLine;
            requirements += "Number: " + count + newLine;

            return requirement;
        }

        private string CateringRequirements(string requirement, int? count, string morningTime,
            string midMorningTime, string lunchTime, string AfternoonTime, string eveningTime)
        {
            var requirements = string.Empty;
            var timeMorning = morningTime != string.Empty ? morningTime : "N/A";
            var timeMidMorning = midMorningTime != string.Empty ? midMorningTime : "N/A";
            var timeLunch = lunchTime != string.Empty ? lunchTime : "N/A";
            var timeAfterNoon = AfternoonTime != string.Empty ? AfternoonTime : "N/A";
            var timeEvening = eveningTime != string.Empty ? eveningTime : "N/A";

            requirements = boldStart + requirement + boldEnd + newLine;
            requirements += "Number of people: " + count + newLine;
            requirements += "Morning Time: " + timeMorning + newLine;
            requirements += "Mid Morning Time: " + timeMidMorning + newLine;
            requirements += "Lunch Time: " + timeLunch + newLine;
            requirements += "After Noon Time: " + timeAfterNoon + newLine;
            requirements += "Evening Time: " + timeEvening + newLine;

            return requirements;
        }
    
    }
}