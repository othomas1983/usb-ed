﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using USB_ED.EntityFrameworkModels;
using USB_ED.Global;
using USB_ED.Models;
using USB_ED.Models.Enums;

namespace USB_ED.Tasks
{
    public class MyCommitmentTasks
    {
        /// <summary>
        /// Save my commitment
        /// </summary>
        /// <param name="commitmentID"></param>
        /// <param name="performanceCategoryID"></param>
        /// <param name="cvId"></param>
        /// <param name="year"></param>
        /// <param name="commitment"></param>
        /// <param name="commitmentDesc"></param>
        /// <returns></returns>
        public int SaveMyCommitment(int performanceCategoryID, int cvId, int? year, string commitment,
            string commitmentDesc)
        {
            try
            {
                using (var entity = new FacultyManagementEntities())
                {
                    var userName = SessionHelper.LoggedInUser.EmployeeName;

                    var existingRecord =
                        entity.SP_COMMITMENTREAD(cvId)
                            .FirstOrDefault(x => x.PerformanceCategoryID == performanceCategoryID);

                    if (existingRecord.IsNotNull() &&
                        existingRecord.PerformanceCategory != PerformanceCategory.Other.ToString())
                    {
                        return entity.SP_COMMITMENTUPDATE(existingRecord.CommitmentID, commitment, commitmentDesc, year,
                            userName);
                    }

                    return entity.SP_COMMITMENTCREATE(performanceCategoryID, cvId, year, commitment, commitmentDesc,
                        userName);
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        /// <summary>
        /// Get performance categories
        /// </summary>
        /// <returns></returns>
        public List<CommitmentPerformanceCategory> LoadPerformanceCategories()
        {
            try
            {
                using (var entity = new FacultyManagementEntities())
                {
                    var result = entity.sp_PerformanceCategoryRead().ToList();

                    var json = new JavaScriptSerializer().Serialize(result);

                    var categories = JsonConvert.DeserializeObject<List<CommitmentPerformanceCategory>>(json);

                    return categories;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Get my commitments
        /// </summary>
        /// <param name="cvId"></param>
        /// <returns></returns>
        public List<MyCommitment> LoadMyCommitments(int cvId)
        {
            try
            {
                using (var entity = new FacultyManagementEntities())
                {
                    var result = entity.SP_COMMITMENTREAD(cvId).OrderBy(x => x.PerformanceCategoryID).ToList();

                    var json = new JavaScriptSerializer().Serialize(result);

                    var commitments = JsonConvert.DeserializeObject<List<MyCommitment>>(json);

                    return commitments;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
