﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using USB_ED.Models;
using USB_ED.Models.VenueBooking;

namespace USB_ED.Tasks.VenueBookings
{
    enum CateringPackageTimes
    {
        ArrivalTeaTime,
        MidmorningTeaTime,
        AfternoonTeaTime,
        LunchTime
    }

    enum CateringTimes
    {
        MorningTime,
        MidMorningTime,
        LunchTime,
        AfternoonTime,
        EveningTime
    }

    public class VenueBookingTasks
    {
        #region Public Methods

        public Venue LoadVenue(string roomNumber)
        {
            var campusVenues = new za.ac.sun.belpark.servicebus.CampusVenues();
            var result = campusVenues.GetVenueDetails(roomNumber);

            var venueJson = JsonConvert.SerializeObject(result);
            var venue = JsonConvert.DeserializeObject<Venue>(venueJson);

            return venue;
        }    

        public List<SelectListItem> LoadBusinessUnits()
        {
            using (var entity = new USB_ED.EntityFrameworkModels.BookingsEntities())
            {
                var list = entity.sp_BusinessUnitRead().ToList();
                var business = ResolveBusinessUnits(list);

                return business;
            }
        }

        public VenueBookingForm LoadEvent(int venueId)
        {
            var model = new VenueBookingForm();

            using (var entity = new USB_ED.EntityFrameworkModels.BookingsEntities())
            {
                var venueBooking = new VenueBookingForm();

                var booking = entity.sp_VenueBookingRead(venueId).FirstOrDefault();                
                model = ResolveBooking(booking, venueBooking);                

                var catering = entity.sp_CateringInfoRead(venueId).ToList();
                ResolveCateringRequirements(catering, model);


                //model.CateringRequirements = catering;
            }

            return model;
        }

        public string SubmitBookingForm(VenueBookingForm model)
        {
            try
            {
                using (var entity = new USB_ED.EntityFrameworkModels.BookingsEntities())
                {
                    var roomNumber = new Int32();
                    int.TryParse(model.RoomNumber, out roomNumber);

                    var venue = entity.sp_VenueRead(roomNumber).FirstOrDefault();
                    model.VenueId = venue.VenueID;

                    var timeIn = new System.TimeSpan();
                    var timeOut = new System.TimeSpan();

                    System.TimeSpan.TryParse(model.BookingTimeIn, out timeIn);
                    System.TimeSpan.TryParse(model.BookingTimeOut, out timeOut);

                    var output = new SqlParameter("ResultList", SqlDbType.NVarChar, 4000);
                    output.Direction = ParameterDirection.Output;

                    if (model.VenueBookingID > 0)
                    {
                        entity.sp_VenueBookingUpdate(model.VenueBookingID, model.EventName, model.CompanyName, model.PersonRequestingVenue, null,
                            model.ContactNumber, model.eMail, null, model.VATNumber, model.VATRegistrationNumber, model.BillingAddress,
                            model.PersonToBeBilled, null, model.ContactNumber, model.eMail, null, model.BookingNumberOfPeopleExpected,
                            model.VenueId, null, null, timeIn, timeOut, null, model.BookingDate, Convert.ToInt32(model.BusinessUnitId),
                            model.DataProjector, model.MicroPhone, model.LaserPointer, model.PenAndPaper, model.MineralWater, model.Flowers,
                            model.SpecialRequirements, null);

                        PopulateTVP(model, model.VenueBookingID, entity);
                    }
                    else
                    {
                        var venueBookingId = new System.Data.Entity.Core.Objects.ObjectParameter("VenueBookingID", SqlDbType.Int);

                        var result = entity.sp_VenueBookingCreate(model.EventName, model.CompanyName, model.PersonRequestingVenue, null,
                            model.ContactNumber, model.eMail, null, model.VATNumber, model.VATRegistrationNumber, model.BillingAddress,
                            model.PersonToBeBilled, null, model.ContactNumber, model.eMail, null, model.BookingNumberOfPeopleExpected,
                            model.VenueId, null, null, timeIn, timeOut, null, model.BookingDate, Convert.ToInt32(model.BusinessUnitId),
                            model.DataProjector, model.MicroPhone, model.LaserPointer, model.PenAndPaper, model.MineralWater, model.Flowers,
                            model.SpecialRequirements, null, venueBookingId);

                        PopulateTVP(model, Convert.ToInt32(venueBookingId.Value), entity);
                    }

                    //TODO: change 
                    //return result.AsString();
                    return "1";
                }
            }
            catch (Exception ex)
            {
                return "Unable to process your request at this time. Please try again later or contact the University of Stellenbosch Belville Park Campus to finalise your booking." + Environment.NewLine +
                    ex.InnerException.Message;
            }
        }

        #endregion

        #region Private Methods

        private List<USB_ED.EntityFrameworkModels.sp_CateringInfoRead_Result> LoadCateringInfo(int venueBookingId)
        {
            using (var entity = new USB_ED.EntityFrameworkModels.BookingsEntities())
            {
                var cateringInfo = entity.sp_CateringInfoRead(venueBookingId).ToList();

                return cateringInfo;
            }
        }

        #region Re-Load Booking

        private VenueBookingForm ResolveBooking(EntityFrameworkModels.sp_VenueBookingRead_Result result, VenueBookingForm venueBooking)
        {
            var startHour = result.TimeIn.Value.Hours.AsString().PadRight(2, '0');
            var startMinutes = result.TimeIn.Value.Minutes.AsString().PadRight(2, '0');
            var endHour = result.TimeOut.Value.Hours.AsString().PadRight(2, '0');
            var endMinutes = result.TimeOut.Value.Minutes.AsString().PadRight(2, '0');

            venueBooking.BillingAddress = result.BillingAddress;
            venueBooking.BookingDate = result.EventDate.GetValueOrDefault();
            venueBooking.BookingNumberOfPeopleExpected = result.NoOfGuests.GetValueOrDefault();
            venueBooking.BookingTimeIn = startHour.AsString() + ":" + startMinutes.AsString();
            venueBooking.BookingTimeOut = endHour.AsString() + ":" + endMinutes.AsString();
            //venueBooking.BookingVenue = item.b
            venueBooking.BusinessUnitId = result.BusinessUnitID.AsString();
            venueBooking.CompanyName = result.CompanyName;
            venueBooking.ContactNumber = result.CompanyName;
            venueBooking.DataProjector = result.DataProjector.GetValueOrDefault();
            venueBooking.eMail = result.EmailAddress;
            venueBooking.EventName = result.EventName;
            venueBooking.Flowers = result.Flowers.GetValueOrDefault();
            //venueBooking.FullDay = item.full
            venueBooking.LaserPointer = result.LaserPointer.GetValueOrDefault();
            venueBooking.MicroPhone = result.Microphone.GetValueOrDefault();
            venueBooking.MineralWater = result.StillBottleMineralWater.GetValueOrDefault();
            venueBooking.PenAndPaper = result.PenAndPaper.GetValueOrDefault();
            venueBooking.PersonRequestingVenue = result.ContactPersonName + " " + result.ContactPersonSurname;
            venueBooking.PersonToBeBilled = result.ContactPersonName + " " + result.ContactPersonSurname;
            venueBooking.RoomNumber = result.VenueNo;
            venueBooking.SpecialRequirements = result.SpecialRequirements;
            venueBooking.VATNumber = result.VATNo;
            venueBooking.VATRegistrationNumber = result.VATRegNo;

            venueBooking.Venue = new Venue()
            {
                Name = result.VenueName,
                Number = result.VenueNo
            };

            venueBooking.VenueBookingID = result.VenueBookingID;
            venueBooking.VenueId = result.VenueID.GetValueOrDefault();

            return venueBooking;
        }

        private void ResolveCateringRequirements(List<EntityFrameworkModels.sp_CateringInfoRead_Result> catering, VenueBookingForm model)
        {
            model.CateringRequirements = new Models.VenueBooking.CateringRequirements();

            foreach (var item in catering.Where(c => c.CateringPlan == "A"))
            {
                model.CateringRequirements.CateringPackageA = true;
                SetCateringPackageATimes(item, model);
            }

            foreach (var item in catering.Where(c => c.CateringPlan == "B"))
            {
                model.CateringRequirements.CateringPackageB = true;
                SetCateringPackageBTimes(item, model);
            }

            foreach (var item in catering.Where(c => c.CateringPlan == "C"))
            {
                model.CateringRequirements.CateringPackageB = true;
                SetCateringPackageCTimes(item, model);
            }

            foreach (var item in catering.Where(c => c.CateringPlan.IsNullOrEmpty()))
            {
                var propertyInfo = model.CateringRequirements.GetType().GetProperty(item.Item);
                
                if (propertyInfo.IsNotNull())
                {
                    propertyInfo.SetValue(model.CateringRequirements, Convert.ChangeType(true, propertyInfo.PropertyType), null);

                    var cateringInfoId = model.CateringRequirements.GetType().GetProperty(item.Item + "CateringInfoID");
                    cateringInfoId.SetValue(model.CateringRequirements, Convert.ChangeType(item.CateringInfoID, typeof(int), null));

                    var noOfPeople = model.CateringRequirements.GetType().GetProperty(item.Item + "Count");
                    noOfPeople.SetValue(model.CateringRequirements, Convert.ChangeType(item.NoOfPeople, typeof(int), null));

                    var hour = item.Times.Value.Hours.AsString().PadLeft(2, '0');
                    var min = item.Times.Value.Minutes.AsString().PadRight(2, '0');
                    var time = hour + ":" + min;

                    //var timeOption = model.CateringRequirements.GetType().GetProperty(item.TimeOption.AsString());
                    //timeOption.SetValue(model.CateringRequirements, Convert.ChangeType(item.TimeOption, timeOption.PropertyType), null);

                    switch (item.TimeOption)
                    {
                        case "MorningTime":
                            {
                                var morningTimes = model.CateringRequirements.GetType().GetProperty(item.Item + "MorningTime");
                                if (morningTimes.IsNotNull())
                                {
                                    morningTimes.SetValue(model.CateringRequirements, time);
                                }
                                break;
                            }
                        case "MidMorningTime":
                            {
                                var midMorningTimes = model.CateringRequirements.GetType().GetProperty(item.Item + "MidMorningTime");
                                if (midMorningTimes.IsNotNull())
                                {
                                    midMorningTimes.SetValue(model.CateringRequirements, time);
                                }
                                break;
                            }
                        case "LunchTime":
                            {
                                var lunchTime = model.CateringRequirements.GetType().GetProperty(item.Item + "LunchTime");
                                if (lunchTime.IsNotNull())
                                {
                                    lunchTime.SetValue(model.CateringRequirements, time);
                                }
                                break;
                            }
                        case "AfternoonTime":
                            {
                                var afternoonTime = model.CateringRequirements.GetType().GetProperty(item.Item + "AfternoonTime");
                                if (afternoonTime.IsNotNull())
                                {
                                    afternoonTime.SetValue(model.CateringRequirements, time);
                                }

                                break;
                            }
                        case "EveningTime":
                            {
                                var eveningTime = model.CateringRequirements.GetType().GetProperty(item.Item + "EveningTime");
                                if (eveningTime.IsNotNull())
                                {
                                    eveningTime.SetValue(model.CateringRequirements, time);
                                }
                                break;
                            }

                        default:
                            break;

                    }
                }
            }
        }

        private void SetCateringPackageATimes(EntityFrameworkModels.sp_CateringInfoRead_Result item, VenueBookingForm model)
        {
            var cateringTime = new CateringPackageTimes();
            model.CateringRequirements.CateringPackageACateringInfoID = item.CateringInfoID;

            var timeOption = Enum.TryParse(item.TimeOption, out cateringTime);
            switch (cateringTime)
            {
                case CateringPackageTimes.ArrivalTeaTime:
                    {
                        model.CateringRequirements.CateringPackageATeaCoffeeArrivalTime = item.Times.AsString();
                        break;
                    }
                case CateringPackageTimes.MidmorningTeaTime:
                    {
                        model.CateringRequirements.CateringPackageATeaCoffeeMidmorningTime = item.Times.AsString();
                        break;
                    }
                case CateringPackageTimes.AfternoonTeaTime:
                    {
                        model.CateringRequirements.CateringPackageATeaCoffeeAfternoonTime = item.Times.AsString();
                        break;
                    }
                case CateringPackageTimes.LunchTime:
                    {
                        model.CateringRequirements.CateringPackageALunchTime = item.Times.AsString();
                        break;
                    }
                default:
                    break;
            }
        }

        private void SetCateringPackageBTimes(EntityFrameworkModels.sp_CateringInfoRead_Result item, VenueBookingForm model)
        {
            var cateringTime = new CateringPackageTimes();
            model.CateringRequirements.CateringPackageBCateringInfoID = item.CateringInfoID;

            var timeOption = Enum.TryParse(item.TimeOption, out cateringTime);
            switch (cateringTime)
            {
                case CateringPackageTimes.ArrivalTeaTime:
                    {
                        model.CateringRequirements.CateringPackageBTeaCoffeeArrivalTime = item.Times.AsString();
                        break;
                    }
                case CateringPackageTimes.MidmorningTeaTime:
                    {
                        model.CateringRequirements.CateringPackageBTeaCoffeeMidmorningTime = item.Times.AsString();
                        break;
                    }
                case CateringPackageTimes.AfternoonTeaTime:
                    {
                        model.CateringRequirements.CateringPackageBTeaCoffeeAfternoonTime = item.Times.AsString();
                        break;
                    }
                case CateringPackageTimes.LunchTime:
                    {
                        model.CateringRequirements.CateringPackageBLunchTime = item.Times.AsString();
                        break;
                    }
                default:
                    break;
            }
        }

        private void SetCateringPackageCTimes(EntityFrameworkModels.sp_CateringInfoRead_Result item, VenueBookingForm model)
        {
            var cateringTime = new CateringPackageTimes();
            model.CateringRequirements.CateringPackageCCateringInfoID = item.CateringInfoID;

            var timeOption = Enum.TryParse(item.TimeOption, out cateringTime);
            switch (cateringTime)
            {
                case CateringPackageTimes.ArrivalTeaTime:
                    {
                        model.CateringRequirements.CateringPackageCTeaCoffeeArrivalTime = item.Times.AsString();
                        break;
                    }
                case CateringPackageTimes.MidmorningTeaTime:
                    {
                        model.CateringRequirements.CateringPackageCTeaCoffeeMidmorningTime = item.Times.AsString();
                        break;
                    }
                case CateringPackageTimes.AfternoonTeaTime:
                    {
                        model.CateringRequirements.CateringPackageCTeaCoffeeAfternoonTime = item.Times.AsString();
                        break;
                    }
                case CateringPackageTimes.LunchTime:
                    {
                        model.CateringRequirements.CateringPackageCLunchTime = item.Times.AsString();
                        break;
                    }
                default:
                    break;
            }
        }

        #endregion

        #region Save New Booking

        private void PopulateTVP(VenueBookingForm model, int venueBookingId, USB_ED.EntityFrameworkModels.BookingsEntities entity)
        {
            var cateringInfo = LoadCateringInfo(model.VenueBookingID);
            var dataTable = new DataTable();

            dataTable.Columns.Add("VenueBookingID");
            dataTable.Columns.Add("Item");
            dataTable.Columns.Add("NoOfPeople");
            dataTable.Columns.Add("Times", typeof(TimeSpan));
            dataTable.Columns.Add("TimeOption");
            dataTable.Columns.Add("CateringPlan");
            dataTable.Columns.Add("CateringInfoID");
            dataTable.Columns.Add("isActive");

            int? packageACateringInfoId = null;
            int? packageBCateringInfoId = null;
            int? packageCCateringInfoId = null;

            var packageACateringInfo = cateringInfo.Where(c => c.CateringPlan == "A").FirstOrDefault();
            if (packageACateringInfo.IsNotNull())
            {
                packageACateringInfoId = packageACateringInfo.CateringInfoID;
            }
            
            var packageBCateringInfo = cateringInfo.Where(c => c.CateringPlan == "B").FirstOrDefault();
            if (packageBCateringInfo.IsNotNull())
            {
                packageACateringInfoId = packageBCateringInfo.CateringInfoID;
            }
            
            var packageCCateringInfo = cateringInfo.Where(c => c.CateringPlan == "C").FirstOrDefault();
            if (packageCCateringInfo.IsNotNull())
            {
                packageACateringInfoId = packageCCateringInfo.CateringInfoID;
            }

            dataTable = ResolveCateringPackageA(dataTable, model, venueBookingId, packageACateringInfoId);
            dataTable = ResolveCateringPackageB(dataTable, model, venueBookingId, packageBCateringInfoId);
            dataTable = ResolveCateringPackageC(dataTable, model, venueBookingId, packageCCateringInfoId);

            var props = model.CateringRequirements.GetType().GetProperties().Where(
                       prop => Attribute.IsDefined(prop, typeof(RequirementAttribute)));

            foreach (var item in props)
            {
                var newRow = dataTable.NewRow();
                var selected = model.CateringRequirements.GetType().GetProperty(item.Name).GetValue(model.CateringRequirements).AsBool().GetValueOrDefault();

                //if (selected)
                //{
                    object[] attribute = item.GetCustomAttributes(typeof(RequirementAttribute), true);
                    var displayName = (attribute[0] as RequirementAttribute).DisplayName;
                    var hasTimes = (attribute[0] as RequirementAttribute).HasTimes;
                    var count = 0;
                    if (model.CateringRequirements.GetType().GetProperty(item.Name + "Count").GetValue(model.CateringRequirements).IsNotNull())
                    {
                        count = (int)model.CateringRequirements.GetType().GetProperty(item.Name + "Count").GetValue(model.CateringRequirements);
                    }

                    if (item.Name == "Corkage")
                    {
                        var Id = CateringInfoId(cateringInfo, "Corkage", null);

                        dataTable.Rows.Add(AddNewRow(newRow, venueBookingId, displayName, count, null, null, null, Id, selected));                        
                    }

                    if (hasTimes)
                    {
                        var morningTime = model.CateringRequirements.GetType().GetProperty(item.Name + "MorningTime").GetValue(model.CateringRequirements).AsString();
                        if (morningTime.IsNotNullOrEmpty())
                        {
                            newRow = dataTable.NewRow();                            
                            var Id = CateringInfoId(cateringInfo, item.Name, "MorningTime");
                            dataTable.Rows.Add(AddNewRow(newRow, venueBookingId, displayName, count, morningTime, CateringTimes.MorningTime.AsString(), null, Id, selected));
                        }

                        var midMorningTime = model.CateringRequirements.GetType().GetProperty(item.Name + "MidMorningTime").GetValue(model.CateringRequirements).AsString();
                        if (midMorningTime.IsNotNullOrEmpty())
                        {
                            newRow = dataTable.NewRow();
                            var Id = CateringInfoId(cateringInfo, item.Name, "MidMorningTime");
                            dataTable.Rows.Add(AddNewRow(newRow, venueBookingId, displayName, count, midMorningTime, CateringTimes.MidMorningTime.AsString(), null, Id, selected));
                        }

                        var lunchTime = model.CateringRequirements.GetType().GetProperty(item.Name + "LunchTime").GetValue(model.CateringRequirements).AsString();
                        if (lunchTime.IsNotNullOrEmpty())
                        {
                            newRow = dataTable.NewRow();
                            var Id = CateringInfoId(cateringInfo, item.Name, "LunchTime");
                            dataTable.Rows.Add(AddNewRow(newRow, venueBookingId, displayName, count, lunchTime, CateringTimes.LunchTime.AsString(), null, Id, selected));
                        }

                        var AfternoonTime = model.CateringRequirements.GetType().GetProperty(item.Name + "AfternoonTime").GetValue(model.CateringRequirements).AsString();
                        if (AfternoonTime.IsNotNullOrEmpty())
                        {
                            newRow = dataTable.NewRow();
                            var Id = CateringInfoId(cateringInfo, item.Name, "AfternoonTime");
                            dataTable.Rows.Add(AddNewRow(newRow, venueBookingId, displayName, count, AfternoonTime, CateringTimes.AfternoonTime.AsString(), null, Id, selected));
                        }

                        var eveningTime = model.CateringRequirements.GetType().GetProperty(item.Name + "EveningTime").GetValue(model.CateringRequirements).AsString();
                        if (eveningTime.IsNotNullOrEmpty())
                        {
                            newRow = dataTable.NewRow();
                            var Id = CateringInfoId(cateringInfo, item.Name, "EveningTime");
                            dataTable.Rows.Add(AddNewRow(newRow, venueBookingId, displayName, count, eveningTime, CateringTimes.EveningTime.AsString(), null, Id, selected));
                        }
                    }
                    else
                    {
                        //dataTable.Rows.Add(AddNewRow(newRow, venueBookingId, displayName, count, "10:00:00", displayName));
                    }
                //}
            }
            CreateCateringInfo(dataTable, entity);
            UpdateCateringInfo(dataTable, entity);
            DeleteCateringInfo(dataTable, entity);
        }

        private int? CateringInfoId(List<EntityFrameworkModels.sp_CateringInfoRead_Result> cateringInfo, string itemName, string timeOption)
        {
            int? id = null;
            var cateringItem = cateringInfo.Where(c => c.Item == itemName && c.TimeOption == timeOption).FirstOrDefault();

            if (cateringItem.IsNotNull())
            {
                id = cateringItem.CateringInfoID;
            }

            return id;
        }

        private DataRow AddNewRow(DataRow newRow, int venueBookingId, string displayName, int count, string time, string timeOption, string cateringPlan, int? cateringInfoId, bool isActive)
        {
            newRow["VenueBookingID"] = venueBookingId;
            newRow["Item"] = displayName;
            newRow["NoOfPeople"] = count;

            var timeRequired = new TimeSpan();
            TimeSpan.TryParse(time, out timeRequired);
            if (timeRequired == null)
            {
                newRow["Times"] = DBNull.Value;
            }
            else
            {
                newRow["Times"] = timeRequired;
            }

            newRow["TimeOption"] = timeOption;
            newRow["CateringPlan"] = cateringPlan;
            newRow["CateringInfoID"] = cateringInfoId;
            newRow["isActive"] = isActive;

            return newRow;
        }

        private void CreateCateringInfo(DataTable dataTable, USB_ED.EntityFrameworkModels.BookingsEntities entity)
        {
            var tvpTable = new SqlParameter("tvpCateringInfoTable", SqlDbType.Structured);
            tvpTable.Value = dataTable;
            tvpTable.TypeName = "CateringInfoTable";

            var result = entity.Database.ExecuteSqlCommand("EXEC sp_CateringInfoCreate @tvpCateringInfoTable", tvpTable);
        }

        private void UpdateCateringInfo(DataTable dataTable, USB_ED.EntityFrameworkModels.BookingsEntities entity)
        {
            var tvpTable = new SqlParameter("tvpCateringInfoTable", SqlDbType.Structured);
            tvpTable.Value = dataTable;
            tvpTable.TypeName = "CateringInfoTable";

            var result = entity.Database.ExecuteSqlCommand("EXEC sp_CateringInfoUpdate @tvpCateringInfoTable", tvpTable);
        }

        private void DeleteCateringInfo(DataTable dataTable, USB_ED.EntityFrameworkModels.BookingsEntities entity)
        {
            var tvpTable = new SqlParameter("tvpCateringInfoTable", SqlDbType.Structured);
            tvpTable.Value = dataTable;
            tvpTable.TypeName = "CateringInfoTable";

            var result = entity.Database.ExecuteSqlCommand("EXEC sp_CateringInfoDelete @tvpCateringInfoTable", tvpTable);
        }

        private List<SelectListItem> ResolveBusinessUnits(List<EntityFrameworkModels.sp_BusinessUnitRead_Result> list)
        {
            var businessUnits = new List<SelectListItem>();
            businessUnits.Add(new SelectListItem(){ Value = "", Text = ""});
            foreach (var item in list)
            {
                businessUnits.Add(new SelectListItem()
                {
                    Value = item.BusinessUnitID.AsString(),
                    Text = item.BusinessUnitName
                });
            }

            return businessUnits;
        }

        private VenueBookingForm ResolveVenue(EntityFrameworkModels.sp_VenueRead_Result venue, VenueBookingForm venueBooking)
        {
            venueBooking.RoomNumber = venue.VenueNo;
            
            return venueBooking;
        }

        private DataTable ResolveCateringPackageA(DataTable dataTable, VenueBookingForm model, int venueBookingId, int? cateringInfoId)
        {
            var peopleCount = model.BookingNumberOfPeopleExpected;
            if (model.CateringRequirements.CateringPackageA.IsNotNull())
            {
                var newRow = dataTable.NewRow();
                var arrivalTeaTime = model.CateringRequirements.CateringPackageATeaCoffeeArrivalTime;
                var midmorningTeaTime = model.CateringRequirements.CateringPackageATeaCoffeeMidmorningTime;
                var afternoonTeaTime = model.CateringRequirements.CateringPackageATeaCoffeeAfternoonTime;
                var lunchTime = model.CateringRequirements.CateringPackageALunchTime;

                if (arrivalTeaTime.IsNotNullOrEmpty())
                {
                    newRow = dataTable.NewRow();
                    dataTable.Rows.Add(AddNewRow(newRow, venueBookingId, "Tea/Coffee", peopleCount, arrivalTeaTime, CateringPackageTimes.ArrivalTeaTime.AsString(), "A", cateringInfoId, model.CateringRequirements.CateringPackageA));
                }
                if (midmorningTeaTime.IsNotNullOrEmpty())
                {
                    newRow = dataTable.NewRow();
                    dataTable.Rows.Add(AddNewRow(newRow, venueBookingId, "Tea/Coffee", peopleCount, midmorningTeaTime, CateringPackageTimes.MidmorningTeaTime.AsString(), "A", cateringInfoId, model.CateringRequirements.CateringPackageA));
                }
                if (afternoonTeaTime.IsNotNullOrEmpty())
                {
                    newRow = dataTable.NewRow();
                    dataTable.Rows.Add(AddNewRow(newRow, venueBookingId, "Tea/Coffee", peopleCount, afternoonTeaTime, CateringPackageTimes.AfternoonTeaTime.AsString(), "A", cateringInfoId, model.CateringRequirements.CateringPackageA));
                }
                if (lunchTime.IsNotNullOrEmpty())
                {
                    newRow = dataTable.NewRow();
                    dataTable.Rows.Add(AddNewRow(newRow, venueBookingId, "Lunch", peopleCount, lunchTime, CateringPackageTimes.LunchTime.AsString(), "A", cateringInfoId, model.CateringRequirements.CateringPackageA));
                }
            }

            return dataTable;
        }

        private DataTable ResolveCateringPackageB(DataTable dataTable, VenueBookingForm model, int venueBookingId, int? cateringInfoId)
        {
            var peopleCount = model.BookingNumberOfPeopleExpected;
            if (model.CateringRequirements.CateringPackageB.IsNotNull())
            {
                var newRow = dataTable.NewRow();
                var arrivalTeaTime = model.CateringRequirements.CateringPackageBTeaCoffeeArrivalTime;
                var midmorningTeaTime = model.CateringRequirements.CateringPackageBTeaCoffeeMidmorningTime;
                var afternoonTeaTime = model.CateringRequirements.CateringPackageBTeaCoffeeAfternoonTime;
                var lunchTime = model.CateringRequirements.CateringPackageBLunchTime;

                if (arrivalTeaTime.IsNotNullOrEmpty())
                {
                    newRow = dataTable.NewRow();
                    dataTable.Rows.Add(AddNewRow(newRow, venueBookingId, "Tea/Coffee", peopleCount, arrivalTeaTime, CateringPackageTimes.ArrivalTeaTime.AsString(), "B", cateringInfoId, model.CateringRequirements.CateringPackageB));
                }
                if (midmorningTeaTime.IsNotNullOrEmpty())
                {
                    newRow = dataTable.NewRow();
                    dataTable.Rows.Add(AddNewRow(newRow, venueBookingId, "Tea/Coffee", peopleCount, midmorningTeaTime, CateringPackageTimes.MidmorningTeaTime.AsString(), "B", cateringInfoId, model.CateringRequirements.CateringPackageB));
                }
                if (afternoonTeaTime.IsNotNullOrEmpty())
                {
                    newRow = dataTable.NewRow();
                    dataTable.Rows.Add(AddNewRow(newRow, venueBookingId, "Tea/Coffee", peopleCount, afternoonTeaTime, CateringPackageTimes.AfternoonTeaTime.AsString(), "B", cateringInfoId, model.CateringRequirements.CateringPackageB));
                }
                if (lunchTime.IsNotNullOrEmpty())
                {
                    newRow = dataTable.NewRow();
                    dataTable.Rows.Add(AddNewRow(newRow, venueBookingId, "Lunch", peopleCount, lunchTime, CateringPackageTimes.LunchTime.AsString(), "B", cateringInfoId, model.CateringRequirements.CateringPackageB));
                }
            }
            return dataTable;
        }

        private DataTable ResolveCateringPackageC(DataTable dataTable, VenueBookingForm model, int venueBookingId, int? cateringInfoId)
        {
            var peopleCount = model.BookingNumberOfPeopleExpected;
            if (model.CateringRequirements.CateringPackageC.IsNotNull())
            {
                var newRow = dataTable.NewRow();
                var arrivalTeaTime = model.CateringRequirements.CateringPackageCTeaCoffeeArrivalTime;
                var midmorningTeaTime = model.CateringRequirements.CateringPackageCTeaCoffeeMidmorningTime;
                var afternoonTeaTime = model.CateringRequirements.CateringPackageCTeaCoffeeAfternoonTime;
                var lunchTime = model.CateringRequirements.CateringPackageCLunchTime;

                if (arrivalTeaTime.IsNotNullOrEmpty())
                {
                    newRow = dataTable.NewRow();
                    dataTable.Rows.Add(AddNewRow(newRow, venueBookingId, "Tea/Coffee", peopleCount, arrivalTeaTime, CateringPackageTimes.ArrivalTeaTime.AsString(), "C", cateringInfoId, model.CateringRequirements.CateringPackageC));
                }
                if (midmorningTeaTime.IsNotNullOrEmpty())
                {
                    newRow = dataTable.NewRow();
                    dataTable.Rows.Add(AddNewRow(newRow, venueBookingId, "Tea/Coffee", peopleCount, midmorningTeaTime, CateringPackageTimes.MidmorningTeaTime.AsString(), "C", cateringInfoId, model.CateringRequirements.CateringPackageC));
                }
                if (afternoonTeaTime.IsNotNullOrEmpty())
                {
                    newRow = dataTable.NewRow();
                    dataTable.Rows.Add(AddNewRow(newRow, venueBookingId, "Tea/Coffee", peopleCount, afternoonTeaTime, CateringPackageTimes.AfternoonTeaTime.AsString(), "C", cateringInfoId, model.CateringRequirements.CateringPackageC));
                }
                if (lunchTime.IsNotNullOrEmpty())
                {
                    newRow = dataTable.NewRow();
                    dataTable.Rows.Add(AddNewRow(newRow, venueBookingId, "Lunch", peopleCount, lunchTime, CateringPackageTimes.LunchTime.AsString(), "C", cateringInfoId, model.CateringRequirements.CateringPackageC));
                }
            }

            return dataTable;
        }

        #endregion

        #endregion
    }
}
