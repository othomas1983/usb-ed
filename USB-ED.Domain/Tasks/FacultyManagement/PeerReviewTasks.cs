﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using USB_ED.EntityFrameworkModels;
using USB_ED.Global;
using USB_ED.Models;

namespace USB_ED.Tasks
{
    public class PeerReviewTasks
    {
        public int SavePeerReview(int peerCVId, int reviewerCVId, int? performanceManagementID, string category, string assesmentOption, string description, int year)
        {
            try
            {
                int result;
                using (var entity = new FacultyManagementEntities())
                {
                    var userName = SessionHelper.LoggedInUser.EmployeeName;

                    if (performanceManagementID.IsNotNull())
                    {
                        result = entity.sp_PeerPerformanceManagementUpdate(performanceManagementID, description, userName);
                    }
                    else
                    {
                        result = entity.sp_PeerPerformanceManagementCreate(reviewerCVId, peerCVId, assesmentOption, category, description, year, userName);
                    }

                    return result;
                }
            }
            catch
            {
                return 0;
            }
        }

        public int UpdatePeerReview(int performanceManagementID, string description)
        {
            try
            {
                using (var entity = new FacultyManagementEntities())
                {
                    var id = SessionHelper.LoggedInUser.CVID;
                    var userName = SessionHelper.LoggedInUser.EmployeeName;

                    var result = entity.sp_PeerPerformanceManagementUpdate(performanceManagementID, description, userName);

                    return result;
                }
            }
            catch
            {
                return 0;
            }
        }

        public PeerReview LoadPeerReview(int peerCVId, int reviewerCVId)
        {
            try
            {
                using (var entity = new FacultyManagementEntities())
                {
                    var id = SessionHelper.LoggedInUser.CVID;

                    var result = entity.sp_PeerPerformanceManagementRead(peerCVId).GroupBy(c => c.Category).Select(r => new { CategoryName = r.Key, Values = r.Where(i => i.Category == r.Key) }).ToList();

                    var model = new PeerReview()
                    {
                        PeerReviewIntegrity =
                                        result.Where(r => r.CategoryName == "Integrity")
                                        .SelectMany(r => r.Values.Select(v => new PeerReviewIntegrity
                                        {
                                            Category = r.CategoryName,
                                            PeerCVID = v.PeerCVID,
                                            Description = v.Description,
                                            PeerPerformanceManagementID = v.PeerPerformanceManagementID,
                                            ValueCommitment = v.ValueCommitment,
                                            Year = v.Year
                                        }).ToList()).ToList(),
                        PeerReviewInclusivity =
                                       result.Where(r => r.CategoryName == "Inclusivity")
                                       .SelectMany(r => r.Values.Select(v => new PeerReviewInclusivity
                                       {
                                           Category = r.CategoryName,
                                           PeerCVID = v.PeerCVID,
                                           Description = v.Description,
                                           PeerPerformanceManagementID = v.PeerPerformanceManagementID,
                                           ValueCommitment = v.ValueCommitment,
                                           Year = v.Year
                                       }).ToList()).ToList(),
                        PeerReviewInnovation =
                                        result.Where(r => r.CategoryName == "Innovation")
                                        .SelectMany(r => r.Values.Select(v => new PeerReviewInnovation
                                        {
                                            Category = r.CategoryName,
                                            PeerCVID = v.PeerCVID,
                                            Description = v.Description,
                                            PeerPerformanceManagementID = v.PeerPerformanceManagementID,
                                            ValueCommitment = v.ValueCommitment,
                                            Year = v.Year
                                        }).ToList()).ToList(),
                        PeerReviewEngagement =
                                        result.Where(r => r.CategoryName == "Engagement")
                                        .SelectMany(r => r.Values.Select(v => new PeerReviewEngagement
                                        {
                                            Category = r.CategoryName,
                                            PeerCVID = v.PeerCVID,
                                            Description = v.Description,
                                            PeerPerformanceManagementID = v.PeerPerformanceManagementID,
                                            ValueCommitment = v.ValueCommitment,
                                            Year = v.Year
                                        }).ToList()).ToList(),
                        PeerReviewExcellence =
                                        result.Where(r => r.CategoryName == "Excellence")
                                        .SelectMany(r => r.Values.Select(v => new PeerReviewExcellence
                                        {
                                            Category = r.CategoryName,
                                            PeerCVID = v.PeerCVID,
                                            Description = v.Description,
                                            PeerPerformanceManagementID = v.PeerPerformanceManagementID,
                                            ValueCommitment = v.ValueCommitment,
                                            Year = v.Year
                                        }).ToList()).ToList(),
                        PeerReviewSustainability =
                                        result.Where(r => r.CategoryName == "Sustainability")
                                        .SelectMany(r => r.Values.Select(v => new PeerReviewSustainability
                                        {
                                            Category = r.CategoryName,
                                            PeerCVID = v.PeerCVID,
                                            Description = v.Description,
                                            PeerPerformanceManagementID = v.PeerPerformanceManagementID,
                                            ValueCommitment = v.ValueCommitment,
                                            Year = v.Year
                                        }).ToList()).ToList()
                    
                    };

                    return model;
                }
            }
            catch
            {
                return null;
            }
        }
    }
}
