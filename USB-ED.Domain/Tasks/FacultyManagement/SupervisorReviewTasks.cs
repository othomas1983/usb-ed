﻿using System;
using System.Collections.Generic;
using System.Linq;
using USB_ED.EntityFrameworkModels;
using USB_ED.Global;
using USB_ED.Models;

namespace USB_ED.Tasks
{
    public class SupervisorReviewTasks
    {
        /// <summary>
        /// Save supervisor review
        /// </summary>
        /// <param name="supervisorCVId"></param>
        /// <param name="reviewerCVId"></param>
        /// <param name="performanceManagementID"></param>
        /// <param name="category"></param>
        /// <param name="assesmentOption"></param>
        /// <param name="description"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public int SaveSupervisorReview(int supervisorCVId,
            List<SupervisorReviewIntegrity> supervisorReviewIntegrityModel,
            List<SupervisorReviewInclusivity> supervisorReviewInclusivityModel,
            List<SupervisorReviewInnovation> supervisorReviewInnovationModel,
            List<SupervisorReviewEngagement> supervisorReviewEngagementModel,
            List<SupervisorReviewExcellence> supervisorReviewExcellenceModel,
            List<SupervisorReviewSustainability> supervisorReviewSustainabilityModel, int cvId)
        {
            try
            {
                using (var entity = new FacultyManagementEntities())
                {
                    int saved = 0;

                    var userName = SessionHelper.LoggedInUser.EmployeeName;

                    foreach (var item in supervisorReviewIntegrityModel)
                    {
                        if (item.SupervisorPerformanceManagementID != 0)
                        {
                            saved =
                                entity.sp_SupervisorPerformanceManagementUpdate(item.SupervisorPerformanceManagementID,
                                    item.Description, userName);
                        }
                        else if (item.Year.HasValue && item.ValueCommitment.IsNotNullOrEmpty())
                        {
                            saved = entity.sp_SupervisorPerformanceManagementCreate(cvId, supervisorCVId,
                                item.ValueCommitment, "Integrity", item.Description, item.Year, userName);
                        }
                    }

                    foreach (var item in supervisorReviewInclusivityModel)
                    {
                        if (item.SupervisorPerformanceManagementID != 0)
                        {
                            saved =
                                entity.sp_SupervisorPerformanceManagementUpdate(item.SupervisorPerformanceManagementID,
                                    item.Description, userName);
                        }
                        else if (item.Year.HasValue && item.ValueCommitment.IsNotNullOrEmpty())
                        {
                            saved = entity.sp_SupervisorPerformanceManagementCreate(cvId, supervisorCVId,
                                item.ValueCommitment, "Inclusivity", item.Description, item.Year, userName);
                        }
                    }

                    foreach (var item in supervisorReviewInnovationModel)
                    {
                        if (item.SupervisorPerformanceManagementID != 0)
                        {
                            saved =
                                entity.sp_SupervisorPerformanceManagementUpdate(item.SupervisorPerformanceManagementID,
                                    item.Description, userName);
                        }
                        else if (item.Year.HasValue && item.ValueCommitment.IsNotNullOrEmpty())
                        {
                            saved = entity.sp_SupervisorPerformanceManagementCreate(cvId, supervisorCVId,
                                item.ValueCommitment, "Innovation", item.Description, item.Year, userName);
                        }
                    }

                    foreach (var item in supervisorReviewEngagementModel)
                    {
                        if (item.SupervisorPerformanceManagementID != 0)
                        {
                            saved =
                                entity.sp_SupervisorPerformanceManagementUpdate(item.SupervisorPerformanceManagementID,
                                    item.Description, userName);
                        }
                        else if (item.Year.HasValue && item.ValueCommitment.IsNotNullOrEmpty())
                        {
                            saved = entity.sp_SupervisorPerformanceManagementCreate(cvId, supervisorCVId,
                                item.ValueCommitment, "Engagement", item.Description, item.Year, userName);
                        }
                    }

                    foreach (var item in supervisorReviewExcellenceModel)
                    {
                        if (item.SupervisorPerformanceManagementID != 0)
                        {
                            saved =
                                entity.sp_SupervisorPerformanceManagementUpdate(item.SupervisorPerformanceManagementID,
                                    item.Description, userName);
                        }
                        else if (item.Year.HasValue && item.ValueCommitment.IsNotNullOrEmpty())
                        {
                            saved = entity.sp_SupervisorPerformanceManagementCreate(cvId, supervisorCVId,
                                item.ValueCommitment, "Excellence", item.Description, item.Year, userName);
                        }
                    }

                    foreach (var item in supervisorReviewSustainabilityModel)
                    {
                        if (item.SupervisorPerformanceManagementID != 0)
                        {
                            saved =
                                entity.sp_SupervisorPerformanceManagementUpdate(item.SupervisorPerformanceManagementID,
                                    item.Description, userName);
                        }
                        else if (item.Year.HasValue && item.ValueCommitment.IsNotNullOrEmpty())
                        {
                            saved = entity.sp_SupervisorPerformanceManagementCreate(cvId, supervisorCVId,
                                item.ValueCommitment, "Sustainability", item.Description, item.Year, userName);
                        }
                    }

                    return saved;
                }
            }
            catch
            {
                return 0;
            }
        }

        /// <summary>
        /// Update supervisor review
        /// </summary>
        /// <param name="performanceManagementID"></param>
        /// <param name="description"></param>
        /// <returns></returns>
        public int UpdateSupervisorReview(int performanceManagementID, string description)
        {
            try
            {
                using (var entity = new FacultyManagementEntities())
                {
                    var userName = SessionHelper.LoggedInUser.EmployeeName;

                    var result = entity.sp_SupervisorPerformanceManagementUpdate(performanceManagementID, description,
                        userName);

                    return result;
                }
            }
            catch
            {
                return 0;
            }
        }

        /// <summary>
        /// Get supervisor review
        /// </summary>
        /// <param name="supervisorCVId"></param>
        /// <param name="reviewerCVId"></param>
        /// <returns></returns>
        public SupervisorReview LoadSupervisorReview(int supervisorCVId, int reviewerCVId)
        {
            try
            {
                using (var entity = new FacultyManagementEntities())
                {
                    var result =
                        entity.sp_SupervisorPerformanceManagementRead(supervisorCVId)
                            .GroupBy(c => c.Category)
                            .Select(r => new {CategoryName = r.Key, Values = r.Where(i => i.Category == r.Key)})
                            .ToList();

                    var model = new SupervisorReview()
                    {
                        SupervisorReviewIntegrity =
                            result.Where(r => r.CategoryName == "Integrity")
                                .SelectMany(r => r.Values.Select(v => new SupervisorReviewIntegrity
                                {
                                    Category = r.CategoryName,
                                    SupervisorCVID = v.SupervisorCVID,
                                    Description = v.Description,
                                    SupervisorPerformanceManagementID = v.SupervisorPerformanceManagementID,
                                    ValueCommitment = v.ValueCommitment,
                                    Year = v.Year
                                }).ToList()).ToList(),
                        SupervisorReviewInclusivity =
                            result.Where(r => r.CategoryName == "Inclusivity")
                                .SelectMany(r => r.Values.Select(v => new SupervisorReviewInclusivity
                                {
                                    Category = r.CategoryName,
                                    SupervisorCVID = v.SupervisorCVID,
                                    Description = v.Description,
                                    SupervisorPerformanceManagementID = v.SupervisorPerformanceManagementID,
                                    ValueCommitment = v.ValueCommitment,
                                    Year = v.Year
                                }).ToList()).ToList(),
                        SupervisorReviewInnovation =
                            result.Where(r => r.CategoryName == "Innovation")
                                .SelectMany(r => r.Values.Select(v => new SupervisorReviewInnovation
                                {
                                    Category = r.CategoryName,
                                    SupervisorCVID = v.SupervisorCVID,
                                    Description = v.Description,
                                    SupervisorPerformanceManagementID = v.SupervisorPerformanceManagementID,
                                    ValueCommitment = v.ValueCommitment,
                                    Year = v.Year
                                }).ToList()).ToList(),
                        SupervisorReviewEngagement =
                            result.Where(r => r.CategoryName == "Engagement")
                                .SelectMany(r => r.Values.Select(v => new SupervisorReviewEngagement
                                {
                                    Category = r.CategoryName,
                                    SupervisorCVID = v.SupervisorCVID,
                                    Description = v.Description,
                                    SupervisorPerformanceManagementID = v.SupervisorPerformanceManagementID,
                                    ValueCommitment = v.ValueCommitment,
                                    Year = v.Year
                                }).ToList()).ToList(),
                        SupervisorReviewExcellence =
                            result.Where(r => r.CategoryName == "Excellence")
                                .SelectMany(r => r.Values.Select(v => new SupervisorReviewExcellence
                                {
                                    Category = r.CategoryName,
                                    SupervisorCVID = v.SupervisorCVID,
                                    Description = v.Description,
                                    SupervisorPerformanceManagementID = v.SupervisorPerformanceManagementID,
                                    ValueCommitment = v.ValueCommitment,
                                    Year = v.Year
                                }).ToList()).ToList(),
                        SupervisorReviewSustainability =
                            result.Where(r => r.CategoryName == "Sustainability")
                                .SelectMany(r => r.Values.Select(v => new SupervisorReviewSustainability
                                {
                                    Category = r.CategoryName,
                                    SupervisorCVID = v.SupervisorCVID,
                                    Description = v.Description,
                                    SupervisorPerformanceManagementID = v.SupervisorPerformanceManagementID,
                                    ValueCommitment = v.ValueCommitment,
                                    Year = v.Year
                                }).ToList()).ToList()

                    };

                    return model;
                }
            }
            catch
            {
                return null;
            }
        }
    }
}