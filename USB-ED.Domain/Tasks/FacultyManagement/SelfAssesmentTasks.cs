﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using USB_ED.EntityFrameworkModels;
using USB_ED.Global;
using USB_ED.Models;

namespace USB_ED.Tasks
{
    public class SelfAssessmentTasks
    {
        public int SaveSelfAssesment(int cvId, int? performanceManagementID, string category, string assesmentOption, string description, int year)
        {
            try
            {
                int result;
                using (var entity = new FacultyManagementEntities())
                {                        
                    var userName = SessionHelper.LoggedInUser.EmployeeName;

                    if (performanceManagementID.IsNotNull())
                    {
                        result = entity.sp_PerformanceManagementUpdate(performanceManagementID, description, userName);
                    }
                    else
                    {
                        result = entity.sp_PerformanceManagementCreate(cvId, assesmentOption, category, description, year, userName);
                    }

                    return result;
                }
            }
            catch
            {
                return 0;
            }
        }

        public int UpdateSelfAssesment(int performanceManagementID, string description)
        {
            try
            {
                using (var entity = new FacultyManagementEntities())
                {
                    var id = SessionHelper.LoggedInUser.CVID;
                    var userName = SessionHelper.LoggedInUser.EmployeeName;

                    var result = entity.sp_PerformanceManagementUpdate(performanceManagementID, description, userName);

                    return result;
                }
            }
            catch
            {
                return 0;
            }
        }

        public SelfReview LoadSelfAssessments(int? CVId)
        {
            try
            {
                using (var entity = new FacultyManagementEntities())
                {
                    var id = SessionHelper.LoggedInUser.CVID;

                    var result = entity.sp_PerformanceManagementRead(CVId).GroupBy(c => c.Category).Select(r => new {CategoryName = r.Key, Values = r.Where(i=> i.Category == r.Key) }).ToList();

                    var model = new SelfReview()
                    {
                        SelfReviewIntegrity =
                                        result.Where(r => r.CategoryName == "Integrity")
                                        .SelectMany(r => r.Values.Select(v => new SelfReviewIntegrity
                                        {
                                            Category = r.CategoryName,
                                            CVID = v.CVID,
                                            Description = v.Description,
                                            PerformanceManagementID = v.PerformanceManagementID,
                                            ValueCommitment = v.ValueCommitment,
                                            Year = v.Year
                                        }).ToList()).ToList(),
                        SelfReviewInclusivity =
                                       result.Where(r => r.CategoryName == "Inclusivity")
                                       .SelectMany(r => r.Values.Select(v => new SelfReviewInclusivity
                                       {
                                           Category = r.CategoryName,
                                           CVID = v.CVID,
                                           Description = v.Description,
                                           PerformanceManagementID = v.PerformanceManagementID,
                                           ValueCommitment = v.ValueCommitment,
                                           Year = v.Year
                                       }).ToList()).ToList(),
                        SelfReviewInnovation =
                                        result.Where(r => r.CategoryName == "Innovation")
                                        .SelectMany(r => r.Values.Select(v => new SelfReviewInnovation
                                        {
                                            Category = r.CategoryName,
                                            CVID = v.CVID,
                                            Description = v.Description,
                                            PerformanceManagementID = v.PerformanceManagementID,
                                            ValueCommitment = v.ValueCommitment,
                                            Year = v.Year
                                        }).ToList()).ToList(),
                        SelfReviewEngagement =
                                        result.Where(r => r.CategoryName == "Engagement")
                                        .SelectMany(r => r.Values.Select(v => new SelfReviewEngagement
                                        {
                                            Category = r.CategoryName,
                                            CVID = v.CVID,
                                            Description = v.Description,
                                            PerformanceManagementID = v.PerformanceManagementID,
                                            ValueCommitment = v.ValueCommitment,
                                            Year = v.Year
                                        }).ToList()).ToList(),
                        SelfReviewExcellence =
                                        result.Where(r => r.CategoryName == "Excellence")
                                        .SelectMany(r => r.Values.Select(v => new SelfReviewExcellence
                                        {
                                            Category = r.CategoryName,
                                            CVID = v.CVID,
                                            Description = v.Description,
                                            PerformanceManagementID = v.PerformanceManagementID,
                                            ValueCommitment = v.ValueCommitment,
                                            Year = v.Year
                                        }).ToList()).ToList(),
                        SelfReviewSustainability =
                                        result.Where(r => r.CategoryName == "Sustainability")
                                        .SelectMany(r => r.Values.Select(v => new SelfReviewSustainability
                                        {
                                            Category = r.CategoryName,
                                            CVID = v.CVID,
                                            Description = v.Description,
                                            PerformanceManagementID = v.PerformanceManagementID,
                                            ValueCommitment = v.ValueCommitment,
                                            Year = v.Year
                                        }).ToList()).ToList()
                    
                    };
                    //var selfReview = new SelfReview{
                        
                        
                        
                      //  Select(r => new SelfReviewEngagement{
                       //     Category = r.CategoryName,
                            //CVID = r.Values
                        //}
                          //  )
                    //}

                    //var json = new JavaScriptSerializer().Serialize(result);

                    //var selfReview = JsonConvert.DeserializeObject<List<SelfReview>>(json);
                    

                    return model;                    
                }
            }
            catch
            {
                return null;
            }
        }
    }
}
