﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Script.Serialization;
using USB.Models.FacultyManagement;
using USB_ED.EntityFrameworkModels;

namespace USB_ED.Tasks
{
	public class ImageUploadTasks
    {
        #region Public Methods

        public string UploadImage(string imageName, string employeeId, byte[] imageData)
        {
            using (var entity = new FacultyManagementEntities())
            {
				employeeId = employeeId.Replace("\\", "");

                var delete = entity.sp_FacultyImageDelete(employeeId);

				imageName = imageName.Replace("\\", "");

                var result = entity.sp_FacultyImageCreate(imageName, imageData);
                var json = new JavaScriptSerializer().Serialize(result);
                return json;
            }
        }

        public List<UserPhoto> LoadImage(string usNumber)
        {
			usNumber = usNumber.Replace("\\", "");

            using (var entity = new FacultyManagementEntities())
            {
                var result = entity.sp_FacultyImageRead(usNumber);
                var json = new JavaScriptSerializer().Serialize(result);

                var userPhoto = JsonConvert.DeserializeObject<List<UserPhoto>>(json);

                return userPhoto;
            }
        }

        #endregion

        #region Private Methods

        private void SaveImageToDisk(string imageName, byte[] imageData)
        {
            var imageUrl = Properties.Settings.Default.FM_ImageUrl;

            if (imageUrl.IsNotNullOrEmpty())
            {
                File.WriteAllBytes(imageUrl + "\\" + imageName, imageData);
            }
        }

        #endregion
    }
}
