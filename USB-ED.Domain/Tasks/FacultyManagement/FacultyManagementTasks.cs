﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;
using USB.Models.FacultyManagement;
using USB_ED.EntityFrameworkModels;
using USB_ED.Models;
using USB_ED.UpdloadServiceReference;
using SettingsMan = USB_ED.Properties.Settings;

namespace USB_ED.Tasks
{
    public class FacultyManagementTasks
    {
        #region Public Methods - Load

        public FacultyManagement Load(string employeeId, string loggedInUserName)
        {
            using (var entity = new FacultyManagementEntities())
            {
                var facultyManagement = new FacultyManagement();
                int newId = 0;
                int CVID;
                var cvId = new ObjectParameter("cVID", typeof(int));
                var personalInfoRead = entity.sp_PersonalInformationRead(employeeId, cvId).FirstOrDefault();

                //var loadPicture = new ImageUploadTasks();
                // var picture = entity.sp_FacultyImageRead("13979558");



                if (newId > 0)
                {
                    CVID = newId;
                }
                else
                {
                    CVID = (int)cvId.Value;
                    facultyManagement.EmployeeId = employeeId.ToString();
                    ResolvePersonalInformation(facultyManagement, personalInfoRead, CVID);
                }

                var awardRead = entity.sp_AwardRead(CVID).ToList();
                var award = ResolveAward(awardRead, CVID);
                facultyManagement.Awards = award;

                var courseDesignRead = entity.sp_CourseDesignRead(CVID).ToList();
                var courseDesing = ResolveCourseDesign(courseDesignRead, CVID);
                facultyManagement.CourseDesigns = courseDesing;

                //var facultyLanguageRead = entity.sp_FacultyLanguageRead(CVID).ToList();
                //var facultyLanguage = ResolveFacultyLanguage(facultyLanguageRead, CVID);
                //facultyManagement.FacultyLanguages = facultyLanguage;

                //var languageRead = entity.sp_LanguageRead(CVID).ToList();
                //var language = ResolveLanguage(languageRead, CVID);
                //facultyManagement.Languages = language;

                var developmentRead = entity.sp_DevelopmentRead(CVID).ToList();
                var development = ResolveDevelopment(developmentRead, CVID);
                facultyManagement.Developments = development;

                var educationRead = entity.sp_EducationRead(CVID).ToList();
                var education = ResolveEducation(educationRead, CVID);
                facultyManagement.Educations = education;

                var grantsRead = entity.sp_GrantRead(CVID).ToList();
                var grant = ResolveGrant(grantsRead, CVID);
                facultyManagement.GrantsFunding = grant;

                var experienceRead = entity.sp_ExperienceRead(CVID).ToList();
                var experience = ResolveExperience(experienceRead, CVID);
                facultyManagement.Experiences = experience;



                //var publicationRead = entity.sp_PublicationRead(CVID).ToList();
                //var publication = ResolvePublication(publicationRead, CVID);
                //facultyManagement.Publications = publication;

                //var researchRead = entity.sp_ResearchRead(CVID).ToList();
                //var research = ResolveResearch(researchRead, CVID);
                //facultyManagement.Researches = research;

                //var teachingLoadRead = entity.sp_TeachingLoadRead(CVID).ToList();
                //var teachingLoad = ResolveTeachingLoad(teachingLoadRead, CVID);
                //facultyManagement.TeachingLoad = teachingLoad;

                //var teachingLoadRead = entity.sp_TeachingLoadTESTRead().ToList();
                //var teachingLoad = ResolveTeachingLoadTEST(teachingLoadRead, CVID);
                //facultyManagement.TeachingLoad = teachingLoad;

                var managementLoadRead = entity.sp_ManagementLoadRead(CVID).ToList();
                var managementLoad = ResolveManagementLoadRead(managementLoadRead, CVID);
                facultyManagement.ManagementLoads = managementLoad;

                //var phdSupervisionLoadRead = entity.sp_PHDSupervisionRead(CVID).ToList();
                //var phdSupervisionLoad = ResolveSupervisionLoadRead(phdSupervisionLoadRead, CVID);
                //facultyManagement.PhdSupervisions = phdSupervisionLoad;

                //var facultyDepartmentLoadRead = entity.sp_FacultyDepartmentRead(CVID).FirstOrDefault();
                //var facultyDepartmentLoad = ResolveFacultyDepartmentLoadRead(facultyDepartmentLoadRead, CVID);
                //facultyManagement.FacultyDepartment = facultyDepartmentLoad;

                return facultyManagement;
            }
        }

        //Please keep it alphabetic//

        public string LoadAccreditationReports()
        {
            using (var entity = new FacultyManagementEntities())
            {
                var result = entity.sp_AccreditationReportsRead();
                var json = new JavaScriptSerializer().Serialize(result);
                return json;
            }
        }

        public List<USB.Models.FacultyManagement.Affiliation> LoadAffiliations()
        {
            using (var entity = new FacultyManagementEntities())
            {
                var result = entity.sp_AffiliationRead();
                var json = new JavaScriptSerializer().Serialize(result);

                return JsonConvert.DeserializeObject<List<USB.Models.FacultyManagement.Affiliation>>(json);
            }
        }

        public string LoadAllUsers()
        {
            using (var entity = new FacultyManagementEntities())
            {
                var result = entity.sp_CVRead();
                var json = new JavaScriptSerializer().Serialize(result);
                return json;
            }
        }


        public List<PermissionCategory> LoadPermissionCategory()
        {
            using (var entity = new FacultyManagementEntities())
            {
                var result = entity.sp_ApplicationUserRoleRead();
                return result.Select(x => new PermissionCategory
                {
                    Description = x.ApplicationUserRole,
                    CategoryId = x.ApplicationUserRoleID
                }).ToList();
            }
        }

        public List<FacultyManagementEditUsers> LoadAvailableUsers()
        {
            using (var entity = new FacultyManagementEntities())
            {
                var result = entity.sp_CVRead();

                return result.Select(x => new FacultyManagementEditUsers
                {
                    CvId = x.CVID,
                    Title = x.Title,
                    Initials = x.Initials,
                    Surname = x.Surname,


                }).ToList();
            }
        }

        public List<FacultyManagementUsersPermission> GetPermittedUsers(int cvid)
        {
            using (var entity = new FacultyManagementEntities())
            {
                var result = entity.sp_UserRoleAssignmentReadPIVOT(cvid);

                return result.Select(x =>

                  new FacultyManagementUsersPermission
                  {
                      CvId = x.AssigneeCVID,
                      Surname = x.AssigneeSurname,
                      Title = x.AssigneeTitle,
                      Initials = x.AssigneeInitials,
                      Development = x.Development.HasValue && x.Development.Value == 1,
                      Publications = x.Publications.HasValue && x.Publications.Value == 1,
                      Education = x.Education.HasValue && x.Education.Value == 1,
                      Experience = x.Experience.HasValue && x.Experience.Value == 1,
                      ManagementLoad = x.Management_Load.HasValue && x.Management_Load.Value == 1,
                      PersonalInformation = x.Personal_Information.HasValue && x.Personal_Information.Value == 1,
                      Research = x.Research.HasValue && x.Research.Value == 1,
                      SupervisionLoad = x.Supervision_Load.HasValue && x.Supervision_Load.Value == 1,
                      TeachingLoad = x.Teaching_Load.HasValue && x.Teaching_Load.Value == 1
                  }

                ).ToList();
            }
        }

        public List<FacultyManagementEditUsers> LoadPermittedUsers(int cvid)
        {
            using (var entity = new FacultyManagementEntities())
            {
                var result = entity.sp_FacultyPermissionRead(cvid);

                return result.Select(x => new FacultyManagementEditUsers
                {
                    CvId = x.AssigneeCVID,
                    Title = x.AssigneeTitle,
                    Initials = x.AssigneeInitials,
                    Surname = x.AssigneeSurname,
                    CategoryPermissionId = x.ApplicationUserRoleID,
                    CategoryPermission = x.ApplicationUserRole

                }).ToList();
            }
        }

        public EducationsAndAwards LoadEducationsAndAwards(int cvId)
        {
            using (var entity = new FacultyManagementEntities())
            {
                var eduAndAward = new EducationsAndAwards();
                var educationRead = entity.sp_EducationRead(cvId).ToList();
                var awardRead = entity.sp_AwardRead(cvId).ToList();
                var grantsRead = entity.sp_GrantRead(cvId).ToList();

                var educationResult = ResolveEducation(educationRead, cvId);
                var awardResult = ResolveAward(awardRead, cvId);
                var grantsResult = ResolveGrant(grantsRead, cvId);

                eduAndAward.Educations = educationResult;
                eduAndAward.Awards = awardResult;
                eduAndAward.GrantsFunding = grantsResult;

                return eduAndAward;
            }
        }

        public string LoadBusinessUnits()
        {
            using (var entity = new FacultyManagementEntities())
            {
                var result = entity.sp_BusinessUnitRead();
                var json = new JavaScriptSerializer().Serialize(result);
                return json;
            }
        }

        public string LoadFacultyPermissions(int cvid)
        {
            using (var entity = new FacultyManagementEntities())
            {
                var result = entity.sp_FacultyPermissionRead(cvid);
                var json = new JavaScriptSerializer().Serialize(result);
                return json;
            }
        }
        
        public string LoadJournals()
        {
            using (var entity = new FacultyManagementEntities())
            {
                var result = entity.sp_JournalRead();
                var serializer = new JavaScriptSerializer { MaxJsonLength = Int32.MaxValue };

                var json = serializer.Serialize(result);
                return json;
            }
        }

        public string LoadManagementLoadCategory()
        {
            using (var entity = new FacultyManagementEntities())
            {
                var result = entity.sp_ManagementLoadCategoryRead();
                var json = new JavaScriptSerializer().Serialize(result);
                return json;
            }
        }

        public void DeleteSingleResearchItem(int id, string username)
        {
            using (var entity = new FacultyManagementEntities())
            {
                entity.sp_DeleteSingleResearchItem(username, id);
            }
        }

        public string LoadPersonalInformation(string employeeId)
        {
            if (employeeId == "undefined")
                employeeId = null;
            using (var entity = new FacultyManagementEntities())
            {
                var cvId = new ObjectParameter("cVID", typeof(int));
                var result = entity.sp_PersonalInformationRead(employeeId, cvId).FirstOrDefault();
                if (result.IsNull())
                {
                    throw new UnauthorizedAccessException("User not found");
                }
                var json = new JavaScriptSerializer().Serialize(result);
                return json;
            }
        }

        public Research LoadResearch(int cvId)
        {
            using (var entity = new FacultyManagementEntities())
            {
                var map = new FacultyManagementDataMapping();
                var research = new Research();
                var researchRead = entity.sp_ResearchHistoryRead(cvId).Where(r => r.isActive);
                var json = new JavaScriptSerializer().Serialize(researchRead);

                var researchHistory = JsonConvert.DeserializeObject<List<ResearchHistory>>(json);

                map.MapToResearchHistory(ref research, researchHistory, cvId);

                return research;
            }
        }

        public List<TeachingLoad> LoadTeachingLoad(int cvId)
        {
            using (var entity = new FacultyManagementEntities())
            {
                var result = entity.sp_TeachingLoadRead(cvId);
                var json = new JavaScriptSerializer().Serialize(result);

                var teachingLoad = JsonConvert.DeserializeObject<List<TeachingLoad>>(json);

                return teachingLoad;
            }
        }

        //public List<Supervisions> LoadSupervisionLoad(int cvId)
        //{
        //    using (var entity = new FacultyManagementEntities())
        //    {
        //        var supervisionLoadRead = entity.sp_SupervisionRead(cvId).ToList();
        //        var supervisionResult = ResolveSupervisionLoadRead(supervisionLoadRead, cvId);

        //        return supervisionResult;
        //    }
        //}

        public ManagementLoad LoadManagementLoad(int cvId)
        {
            using (var entity = new FacultyManagementEntities())
            {
                var managementLoad = new ManagementLoad();
                var map = new FacultyManagementDataMapping();
                var managementLoadRead = entity.sp_ManagementLoadRead(cvId).ToList();
                var resolve = ResolveManagementLoadRead(managementLoadRead, cvId);

                managementLoad.ProgrammeHead = map.MapToProgrammeHead(resolve.Where(m => m.ManagementLoadCategoryID == (int)ManagementLoadCategoryShortName.ProgrammeHead));
                managementLoad.CentreHead = map.MapToCentreHead(resolve.Where(m => m.ManagementLoadCategoryID == (int)ManagementLoadCategoryShortName.CentreHead));
                managementLoad.OrganisingAConference = map.MapToOrganisingAConference(resolve.Where(m => m.ManagementLoadCategoryID == (int)ManagementLoadCategoryShortName.OrganisingConference));
                managementLoad.LeadershipOneOnOne = map.MapToLeadershipOneOnOne(resolve.Where(m => m.ManagementLoadCategoryID == (int)ManagementLoadCategoryShortName.LeadershipOneOnOne));
                managementLoad.ResearchReportOneOnOne = map.MapToResearchReportOneOnOne(resolve.Where(m => m.ManagementLoadCategoryID == (int)ManagementLoadCategoryShortName.ResearchReportOneOnOne));
                managementLoad.ChairOfCommittee = map.MapToChairOfCommittee(resolve.Where(m => m.ManagementLoadCategoryID == (int)ManagementLoadCategoryShortName.ChairOfAdministrativeManagementCommittee));
                managementLoad.TaskTeamMember = map.MapToTaskTeamMember(resolve.Where(m => m.ManagementLoadCategoryID == (int)ManagementLoadCategoryShortName.TaskTeamMember));
                managementLoad.ManagementLoadOther = map.MapToManagementLoadOther(resolve.Where(m => m.ManagementLoadCategoryID == (int)ManagementLoadCategoryShortName.Other));

                return managementLoad;
            }
        }

        public FacultyDepartmentBundle LoadFacultyDepartment(int cvId)
        {
            using (var entity = new FacultyManagementEntities())
            {
                var facultyManagement = new FacultyDepartmentBundle();
                var facultyDepartmentLoadRead = entity.sp_FacultyDepartmentRead(cvId);

                var facultyDepartmentLoad = ResolveFacultyDepartmentLoadRead(facultyDepartmentLoadRead, cvId);

                return facultyDepartmentLoad;
            }
        }

        public Development LoadDevelopment(int cvId)
        {
            using (var entity = new FacultyManagementEntities())
            {
                var development = new Development();
                var map = new FacultyManagementDataMapping();
                var developmentRead = entity.sp_DevelopmentRead(cvId).ToList();
                var resolve = ResolveDevelopment(developmentRead, cvId);

                development.MembershipDevelopment = map.MapToMembershipDevelopment(resolve.Where(r => r.DevelopmentCategoryID == (int)DevelopmentCategoryShortName.Membership));
                development.ContinuingEducationDevelopment = map.MapToContinuingEducationDevelopment(resolve.Where(r => r.DevelopmentCategoryID == (int)DevelopmentCategoryShortName.ContinuingEducation));
                development.Non_AcademicDevelopment = map.MapToNon_AcademicDevelopment(resolve.Where(r => r.DevelopmentCategoryID == (int)DevelopmentCategoryShortName.Non_Academic));
                development.ExtracurricularDevelopment = map.MapToExtracurricularDevelopment(resolve.Where(r => r.DevelopmentCategoryID == (int)DevelopmentCategoryShortName.Extracurricular));

                return development;
            }
        }

        public Experience LoadExperience(int cvId)
        {
            using (var entity = new FacultyManagementEntities())
            {
                var experience = new Experience();
                var map = new FacultyManagementDataMapping();

                var experienceRead = entity.sp_ExperienceRead(cvId).ToList();
                var resolve = ResolveExperience(experienceRead, cvId);

                experience.TeachingExperience = map.MapToTeachingExperience(resolve.Where(r => r.ExperienceCategoryID == (int)ExperienceCategoryShortName.Teaching));
                experience.ExecutiveEducationExperience = map.MapToTExecutiveEducationExperience(resolve.Where(e => e.ExperienceCategoryID == (int)ExperienceCategoryShortName.ExecutiveEducation));
                experience.FullTimeConsultingExperience = map.MapToFullTimeConsultingExperience(resolve.Where(e => e.ExperienceCategoryID == (int)ExperienceCategoryShortName.FullTime));
                experience.PartTimeConsultingExperience = map.MapToPartTimeConsultingExperience(resolve.Where(e => e.ExperienceCategoryID == (int)ExperienceCategoryShortName.PartTime));
                experience.PermanentProfessionalCareerExperience = map.MapToPermanentProfessionalCareerExperience(resolve.Where(e => e.ExperienceCategoryID == (int)ExperienceCategoryShortName.PermanentProfCareer));
                experience.EngagementWithBusinessAndSocietyExperience = map.MapToEngagementWithBusinessAndSociety(resolve.Where(e => e.ExperienceCategoryID == (int)ExperienceCategoryShortName.EngagementWithBusinessAndSociety));
                experience.EditorialBoardmembers = map.MapToEditorialBoardmembers(resolve.Where(e => e.ExperienceCategoryID == (int)ExperienceCategoryShortName.EditorialBoardmembers));

                return experience;
            }
        }

        public List<Contributor> LoadPeopleInResearch(int researchId)
        {
            using (var entity = new FacultyManagementEntities())
            {
                var result = entity.sp_PeopleinResearchRead(researchId);
                var json = new JavaScriptSerializer().Serialize(result);

                var contributors = JsonConvert.DeserializeObject<List<Contributor>>(json);

                foreach (var item in contributors)
                {
                    item.isActive = true;
                }

                return contributors;
            }
        }

        public string LoadPersonRoles()
        {
            using (var entity = new FacultyManagementEntities())
            {
                var result = entity.sp_PersonRoleRead();
                var json = new JavaScriptSerializer().Serialize(result);
                return json;
            }
        }

        public List<Programmes> LoadProgramme()
        {
            using (var entity = new FacultyManagementEntities())
            {
                var result = entity.sp_ProgrammeRead();
                var json = new JavaScriptSerializer().Serialize(result);

                var programmes = JsonConvert.DeserializeObject<List<Programmes>>(json);

                return programmes;
            }
        }

        public List<Programmes> LoadModules()
        {

            using (var entity = new FacultyManagementEntities())
            {
                var result = entity.sp_ModuleRead(2012, 2017);
                var json = new JavaScriptSerializer().Serialize(result);

                var modules = JsonConvert.DeserializeObject<List<Programmes>>(json);

                return modules;
            }
        }

        public List<ProgrammeModuleCountRead> LoadProgrammeModuleCount()
        {
            using (var entity = new FacultyManagementEntities())
            {
                var result = entity.sp_ProgrammeModuleCountRead();
                var json = new JavaScriptSerializer().Serialize(result);

                var programmes = JsonConvert.DeserializeObject<List<ProgrammeModuleCountRead>>(json);

                return programmes;
            }
        }

        public string LoadProgrammeOffering()
        {
            using (var entity = new FacultyManagementEntities())
            {
                var result = entity.sp_ProgrammeOffering();
                var json = new JavaScriptSerializer().Serialize(result);
                return json;
            }
        }

        public List<USB.Models.FacultyManagement.ResearchOutputCategory> LoadResearchOutputCategories()
        {
            using (var entity = new FacultyManagementEntities())
            {
                var result = entity.sp_ResearchOutputCategoryRead();
                var json = new JavaScriptSerializer().Serialize(result);

                var researchOutputCategories = JsonConvert.DeserializeObject<List<USB.Models.FacultyManagement.ResearchOutputCategory>>(json);

                return researchOutputCategories;
            }
        }

        public List<USB.Models.FacultyManagement.ResearchContributionTypes> LoadResearchContributionTypes()
        {
            using (var entity = new FacultyManagementEntities())
            {
                var result = entity.sp_ResearchContributionTypeRead();
                var json = new JavaScriptSerializer().Serialize(result);

                var researchContributionTypes = JsonConvert.DeserializeObject<List<USB.Models.FacultyManagement.ResearchContributionTypes>>(json);

                return researchContributionTypes;
            }
        }

        public List<Language> LoadLanguage(int cvId)
        {
            using (var entity = new FacultyManagementEntities())
            {
                var languageRead = entity.sp_LanguageRead(cvId).ToList();
                var language = ResolveLanguage(languageRead, cvId);
                return language;
            }
        }

        public List<FacultyLanguage> LoadTeachingLanguage(int cvId)
        {
            using (var entity = new FacultyManagementEntities())
            {
                var languageRead = entity.sp_FacultyLanguageRead(cvId).ToList();
                var language = ResolveFacultyLanguage(languageRead, cvId);
                return language;
            }
        }

        public string LoadMainActivity()
        {
            using (var entity = new FacultyManagementEntities())
            {
                var result = entity.sp_MainActivityRead();
                var json = new JavaScriptSerializer().Serialize(result);
                return json;

            }
        }

        public List<DHETStatus> LoadDHETStatus()
        {
            using (var entity = new FacultyManagementEntities())
            {
                var result = entity.sp_DHETStatusRead();
                var json = new JavaScriptSerializer().Serialize(result);

                return JsonConvert.DeserializeObject<List<USB.Models.FacultyManagement.DHETStatus>>(json);
            }
        }

        public List<ABSRating> LoadABSRatings()
        {
            using (var entity = new FacultyManagementEntities())
            {
                var result = entity.sp_ABSRatingRead();
                var json = new JavaScriptSerializer().Serialize(result);
                return JsonConvert.DeserializeObject<List<USB.Models.FacultyManagement.ABSRating>>(json);
            }
        }

        public List<Locus> LoadLocus()
        {
            using (var entity = new FacultyManagementEntities())
            {
                var result = entity.sp_LocusRead();
                var json = new JavaScriptSerializer().Serialize(result);

                return JsonConvert.DeserializeObject<List<Locus>>(json);
            }
        }

        public string LoadDepartment()
        {
            using (var entity = new FacultyManagementEntities())
            {
                var result = entity.sp_DepartmentRead();
                var json = new JavaScriptSerializer().Serialize(result);
                return json;
            }
        }

        public string LoadDepartmentCategory()
        {
            using (var entity = new FacultyManagementEntities())
            {
                var result = entity.sp_DepartmentCategoryRead();
                var json = new JavaScriptSerializer().Serialize(result);
                return json;
            }
        }

        public string LoadAcademicRanks()
        {
            using (var entity = new FacultyManagementEntities())
            {
                var result = entity.sp_AcademicRankRead();
                var json = new JavaScriptSerializer().Serialize(result);
                return json;
            }
        }

        public List<Genders> LoadGenders()
        {
            var gender = new List<Genders>();
            gender.Add(new Genders() { Gender = "Male" });
            gender.Add(new Genders() { Gender = "Female" });

            return gender;
        }

        public string LoadNationality()
        {
            using (var entity = new FacultyManagementEntities())
            {
                var result = entity.sp_NationalityRead();
                var json = new JavaScriptSerializer().Serialize(result);
                return json;
            }
        }

        public string LoadHighestQualification()
        {
            using (var entity = new FacultyManagementEntities())
            {
                var result = entity.sp_HighestQualificationRead();
                var json = new JavaScriptSerializer().Serialize(result);
                return json;
            }
        }

        public string LoadWritehandResearchAssignments(string employeeId)
        {
            string url = SettingsMan.Default.writehandResearchAssignmentsURL;
            var personalInfo = new PersonalInformation().LoadPersonalInformation(employeeId);
            try
            {
                using (WebClient webClient = new WebClient())
                {
                    webClient.Encoding = Encoding.UTF8;
                    webClient.Headers.Add("Content-Type", "application/json");
                    webClient.QueryString.Add("employeeId", personalInfo.EmployeeId);

                    return webClient.DownloadString(url);
                }
            }
            catch (WebException wex)
            {
                string pageContent = new StreamReader(wex.Response.GetResponseStream()).ReadToEnd().ToString();
                return pageContent;
            }
        }

        public List<Supervisions> LoadSupervisions(int cvId)
        {
            using (var entity = new FacultyManagementEntities())
            {
                var result = entity.sp_SupervisionRead(cvId);
                var json = new JavaScriptSerializer().Serialize(result);

                return JsonConvert.DeserializeObject<List<Supervisions>>(json);
            }
        }

        public List<TaskTeamActivity> LoadTaskTeamActivity()
        {
            using (var entity = new FacultyManagementEntities())
            {
                var result = entity.sp_TaskTeamActivityRead();
                var json = new JavaScriptSerializer().Serialize(result);

                return JsonConvert.DeserializeObject<List<TaskTeamActivity>>(json);
            }
        }

        public List<PerformanceWorkload> LoadPerformanceWorkload(int cvId)
        {
            using (var entity = new FacultyManagementEntities())
            {
                var result = entity.sp_PerformanceWorkloadRead(cvId);
                var json = new JavaScriptSerializer().Serialize(result);

                return JsonConvert.DeserializeObject<List<PerformanceWorkload>>(json);
            }
        }

        #endregion

        #region Public Methods - Update

        public void UpdateIsFaculty(string jsonModel)
        {
            var list = JsonConvert.DeserializeObject<List<FacultyManagementUsers>>(jsonModel);
            var dataTable = new DataTable();

            dataTable.Columns.Add("EmployeeID");
            dataTable.Columns.Add("isFaculty");

            foreach (var item in list)
            {
                var newRow = dataTable.NewRow();

                newRow["EmployeeID"] = item.EmployeeId;
                newRow["isFaculty"] = item.isFaculty;

                dataTable.Rows.Add(newRow);
            }

            var tvpTable = new SqlParameter("tvpisFaculty", SqlDbType.Structured);
            tvpTable.Value = dataTable;
            tvpTable.TypeName = "isFacultyTable";

            var output = new SqlParameter("ResultList", SqlDbType.NVarChar, 4000);
            output.Direction = ParameterDirection.Output;

            using (var entity = new FacultyManagementEntities())
            {
                var result = entity.Database.ExecuteSqlCommand("EXEC sp_isFacultyUpdate @tvpisFaculty", tvpTable);
            }
        }

        public int AddNewUserFromAD(string jsonModel, string loggedInUserName)
        {
            var personalInformation = JsonConvert.DeserializeObject<FacultyManagementUsers>(jsonModel);
            int id;
            var personalInfo = new SqlParameter("tvpPersonalInfo", SqlDbType.Structured);
            var dataTable = new DataTable();

            dataTable.Columns.Add("CVID");
            dataTable.Columns.Add("Biography");
            dataTable.Columns.Add("Title");
            dataTable.Columns.Add("Initials");
            dataTable.Columns.Add("Name");
            dataTable.Columns.Add("Surname");
            dataTable.Columns.Add("Gender");
            dataTable.Columns.Add("NationalityID");
            dataTable.Columns.Add("BusinessContactNo");
            dataTable.Columns.Add("EmailAddress");
            dataTable.Columns.Add("Position");
            dataTable.Columns.Add("AcademicRank");
            dataTable.Columns.Add("HighestQualificationID");
            dataTable.Columns.Add("BusinessUnit");
            dataTable.Columns.Add("Expertise");
            dataTable.Columns.Add("EmployeeID");
            dataTable.Columns.Add("HomeInstitution");
            dataTable.Columns.Add("OrganisationalResponsibility");
            dataTable.Columns.Add("AffiliationID");
            dataTable.Columns.Add("ResearchInterest");
            dataTable.Columns.Add("MainActivityID");
            dataTable.Columns.Add("Samaccountname");
            dataTable.Columns.Add("isFaculty");

            var newRow = dataTable.NewRow();
            dataTable.Rows.Add(newRow);

            var userName = new SqlParameter("UserName", loggedInUserName);

            newRow["CVID"] = null;
            newRow["Biography"] = null;
            newRow["Title"] = personalInformation.Title;
            newRow["Initials"] = personalInformation.Initials;
            newRow["Name"] = personalInformation.GivenName;
            newRow["Surname"] = personalInformation.Surname;
            newRow["Gender"] = null;
            newRow["NationalityID"] = null;
            newRow["BusinessContactNo"] = null;
            newRow["EmailAddress"] = personalInformation.EmailAddress;
            newRow["Position"] = null;
            newRow["AcademicRank"] = null;
            newRow["HighestQualificationID"] = null;
            newRow["BusinessUnit"] = null;
            newRow["Expertise"] = null;
            newRow["EmployeeID"] = personalInformation.EmployeeId;
            newRow["HomeInstitution"] = null;
            newRow["OrganisationalResponsibility"] = null;
            newRow["AffiliationID"] = 5;
            newRow["ResearchInterest"] = null;
            newRow["MainActivityID"] = 1;
            newRow["Samaccountname"] = personalInformation.SamAccountName;
            newRow["isFaculty"] = personalInformation.isFaculty;

            personalInfo.Value = dataTable;
            personalInfo.TypeName = "PersonalInformationTable";

            var cvId = new SqlParameter("@CVID", SqlDbType.Int)
            {
                Direction = System.Data.ParameterDirection.Output
            };

            using (var entity = new FacultyManagementEntities())
            {
                id = entity.Database.ExecuteSqlCommand("EXEC sp_PersonalInformationCreate @tvpPersonalInfo, @UserName, @CVID out", personalInfo, userName, cvId);
            }

            return (int)cvId.Value;
        }

        public void UpdateGoogleScholarHIndex(int CVId, int googleScholarHIndex, string loggedInUserName)
        {
            using (var entity = new FacultyManagementEntities())
            {
                entity.sp_GoogleScholarHIndexUpdate(CVId, googleScholarHIndex, loggedInUserName);
            }
        }

        public void UpdatePerformanceWorkload(PerformanceWorkload performanceWorkload, int cvId, string loggedInUser)
        {
            int result;
            using (var entity = new FacultyManagementEntities())
            {
                result = entity.sp_PerformanceWorkloadUpdate(performanceWorkload.PerformanceWorkloadID, cvId, performanceWorkload.Year,
                    performanceWorkload.Percentage, performanceWorkload.Notes, loggedInUser);
            }
        }

        #endregion

        #region AddOrUpdate

        #region AddOrUpdate Awards

        public void AddOrUpdateAward(List<Award> awards, int cvId, string loggedInUser)
        {
            var dataTable = new DataTable();

            dataTable.Columns.Add("CVID");
            dataTable.Columns.Add("Year");
            dataTable.Columns.Add("AwardType");
            dataTable.Columns.Add("AwardDescription");
            dataTable.Columns.Add("Country");
            dataTable.Columns.Add("isActive");
            dataTable.Columns.Add("AwardID");

            foreach (var item in awards)
            {
                var newRow = dataTable.NewRow();

                int? awardId;
                if (item.AwardID == 0)
                {
                    awardId = null;
                }
                else
                {
                    awardId = item.AwardID;
                }

                newRow["CVID"] = cvId;
                newRow["Year"] = item.Year;
                newRow["AwardType"] = item.AwardType;
                newRow["AwardDescription"] = item.AwardDescription;
                newRow["Country"] = item.Country;
                newRow["isActive"] = item.isActive;
                newRow["AwardID"] = awardId;

                dataTable.Rows.Add(newRow);
            }

            var tvpTable = new SqlParameter("tvpAward", SqlDbType.Structured);
            tvpTable.Value = dataTable;
            tvpTable.TypeName = "AwardTable";

            var userName = new SqlParameter("UserName", loggedInUser);
            var output = new SqlParameter("ResultList", SqlDbType.NVarChar, 4000);
            output.Direction = ParameterDirection.Output;

            using (var entity = new FacultyManagementEntities())
            {
                var result = entity.Database.ExecuteSqlCommand("EXEC sp_AwardCUD @TVPAward, @UserName", tvpTable, userName);
            }
        }

        #endregion

        #region AddOrUpdate CourseDesign

        public void AddOrUpdateCourseDesign(string jsonModel, int cvId, string loggedInUser)
        {
            var list = JsonConvert.DeserializeObject<List<CourseDesign>>(jsonModel);
            var dataTable = new DataTable();

            dataTable.Columns.Add("CourseDesignID");
            dataTable.Columns.Add("ProgrammeID");
            dataTable.Columns.Add("Module");
            dataTable.Columns.Add("ContactTime");
            dataTable.Columns.Add("TotalCredit");
            dataTable.Columns.Add("CVID");
            dataTable.Columns.Add("isActive");


            foreach (var item in list)
            {
                var newRow = dataTable.NewRow();

                int? courseDesignId;
                if (item.CourseDesignID == 0)
                {
                    courseDesignId = null;
                }
                else
                {
                    courseDesignId = item.CourseDesignID;
                }

                newRow["CourseDesignID"] = courseDesignId;
                newRow["ProgrammeID"] = item.ProgrammeID;
                newRow["Module"] = item.Module == null ? string.Empty : item.Module;
                newRow["ContactTime"] = item.ContactTime;
                newRow["TotalCredit"] = item.TotalCredit;
                newRow["CVID"] = cvId;
                newRow["isActive"] = item.isActive;

                dataTable.Rows.Add(newRow);
            }

            var tvpTable = new SqlParameter("tvpCourseDesign", SqlDbType.Structured);
            tvpTable.Value = dataTable;
            tvpTable.TypeName = "CourseDesignTable";

            var userName = new SqlParameter("UserName", loggedInUser);
            var output = new SqlParameter("ResultList", SqlDbType.NVarChar, 4000);
            output.Direction = ParameterDirection.Output;

            using (var entity = new FacultyManagementEntities())
            {
                var result = entity.Database.ExecuteSqlCommand("EXEC sp_CourseDesignCUD @TVPCourseDesign, @UserName", tvpTable, userName);
            }
        }

        #endregion

        #region AddOrUpdate Developments

        public void AddOrUpdateDevelopment(Development development, int cvId, string loggedInUser)
        {
            var developmentList = new List<Development>();
            var map = new FacultyManagementDataMapping();

            if (development.MembershipDevelopment.IsNotNull())
            {
                development.Developments = map.MapMembershipDevelopment(developmentList, development.MembershipDevelopment, development.CVId);
            }

            if (development.ContinuingEducationDevelopment.IsNotNull())
            {
                development.Developments = map.MapContinuingEducationDevelopment(developmentList, development.ContinuingEducationDevelopment, development.CVId);
            }

            if (development.Non_AcademicDevelopment.IsNotNull())
            {
                development.Developments = map.MapNon_AcademicDevelopment(developmentList, development.Non_AcademicDevelopment, development.CVId);
            }

            if (development.ExtracurricularDevelopment.IsNotNull())
            {
                development.Developments = map.MapExtracurricularDevelopment(developmentList, development.ExtracurricularDevelopment, development.CVId);
            }

            var dataTable = new DataTable();

            dataTable.Columns.Add("CVID");
            dataTable.Columns.Add("DevelopmentCategoryID");
            dataTable.Columns.Add("Year");
            dataTable.Columns.Add("Accomplishment");
            dataTable.Columns.Add("StartDate", typeof(DateTime)).DateTimeMode = DataSetDateTime.Local;
            dataTable.Columns.Add("EndDate", typeof(DateTime)).DateTimeMode = DataSetDateTime.Local;
            dataTable.Columns.Add("Topic");
            dataTable.Columns.Add("NoOfDays");
            dataTable.Columns.Add("Institution");
            dataTable.Columns.Add("Country");
            dataTable.Columns.Add("isCurrent");
            dataTable.Columns.Add("Position");
            dataTable.Columns.Add("Organisation");
            dataTable.Columns.Add("City");
            dataTable.Columns.Add("isActive");
            dataTable.Columns.Add("DevelopmentID");

            foreach (var item in development.Developments)
            {
                var newRow = dataTable.NewRow();

                int? devID;
                if (item.DevelopmentID == 0)
                {
                    devID = null;
                }
                else
                {
                    devID = item.DevelopmentID;
                }

                newRow["CVID"] = cvId;
                newRow["DevelopmentCategoryID"] = item.DevelopmentCategoryID;
                newRow["StartDate"] = item.StartDate.GetValueOrDefault();
                newRow["EndDate"] = item.EndDate.GetValueOrDefault();
                newRow["Accomplishment"] = item.Accomplishment;

                if (item.StartDate == null)
                {
                    newRow["StartDate"] = DBNull.Value;
                }
                else
                {
                    newRow["StartDate"] = ResolveDate(item.StartDate);
                }

                if (item.EndDate == null)
                {
                    newRow["EndDate"] = DBNull.Value;
                }
                else
                {
                    newRow["EndDate"] = ResolveDate(item.EndDate);
                }


                newRow["Topic"] = item.Topic;
                newRow["NoOfDays"] = item.NoOfDays;
                newRow["Institution"] = item.Institution;
                newRow["Country"] = item.Country;
                newRow["isCurrent"] = item.IsCurrent;
                newRow["Position"] = item.Position;
                newRow["Organisation"] = item.Organisation;
                newRow["City"] = item.City;
                newRow["isActive"] = item.isActive;
                newRow["DevelopmentID"] = devID;

                dataTable.Rows.Add(newRow);
            }

            var tvpTable = new SqlParameter("TVPDevelopment", SqlDbType.Structured);
            tvpTable.Value = dataTable;
            tvpTable.TypeName = "DevelopmentTable";

            var userName = new SqlParameter("UserName", loggedInUser);
            var output = new SqlParameter("ResultList", SqlDbType.NVarChar, 4000);
            output.Direction = ParameterDirection.Output;

            using (var entity = new FacultyManagementEntities())
            {
                var result = entity.Database.ExecuteSqlCommand("EXEC sp_DevelopmentCUD @TVPDevelopment, @UserName", tvpTable, userName);
            }
        }

        #endregion

        #region AddOrUpdate Education

        public void AddOrUpdateEducation(List<Education> education, int cvId, string loggedInUser)
        {
            var dataTable = new DataTable();

            dataTable.Columns.Add("CVID");
            dataTable.Columns.Add("EducationYear");
            dataTable.Columns.Add("Degree");
            dataTable.Columns.Add("FieldOfStudy");
            dataTable.Columns.Add("University");
            dataTable.Columns.Add("EducationCountry");
            dataTable.Columns.Add("isActive");
            dataTable.Columns.Add("EducationID");

            foreach (var item in education)
            {
                var newRow = dataTable.NewRow();

                int? educationId;
                if (item.EducationID == 0)
                {
                    educationId = null;
                }
                else
                {
                    educationId = item.EducationID;
                }

                newRow["CVID"] = cvId;
                newRow["EducationYear"] = item.Year;
                newRow["Degree"] = item.Degree;
                newRow["FieldOfStudy"] = item.FieldOfStudy;
                newRow["University"] = item.University;
                newRow["EducationCountry"] = item.Country;
                newRow["isActive"] = item.isActive;
                newRow["EducationID"] = educationId;

                dataTable.Rows.Add(newRow);
            }

            var tvpTable = new SqlParameter("tvpEducation", SqlDbType.Structured);
            tvpTable.Value = dataTable;
            tvpTable.TypeName = "EducationTable";

            var userName = new SqlParameter("UserName", loggedInUser);
            var output = new SqlParameter("ResultList", SqlDbType.NVarChar, 4000);
            output.Direction = ParameterDirection.Output;

            using (var entity = new FacultyManagementEntities())
            {
                var result = entity.Database.ExecuteSqlCommand("EXEC sp_EducationCUD @TVPEducation, @UserName", tvpTable, userName);
            }
        }

        #endregion

        #region AddOrUpdate Experience

        public void AddOrUpdateExperience(Experience experience, int cvId, string loggedInUser)
        {
            var map = new FacultyManagementDataMapping();
            var experienceList = new List<Experience>();

            if (experience.TeachingExperience.IsNotNull())
            {
                experience.Experiences = map.MapTeachingExperiences(experienceList, experience.TeachingExperience, experience.CVId);
            }

            if (experience.ExecutiveEducationExperience.IsNotNull())
            {
                experience.Experiences = map.MapExecutiveEducationExperience(experienceList, experience.ExecutiveEducationExperience, experience.CVId);
            }

            if (experience.FullTimeConsultingExperience.IsNotNull())
            {
                experience.Experiences = map.MapFullTimeConsultingExperience(experienceList, experience.FullTimeConsultingExperience, experience.CVId);
            }

            if (experience.PartTimeConsultingExperience.IsNotNull())
            {
                experience.Experiences = map.MapPartTimeConsultingExperience(experienceList, experience.PartTimeConsultingExperience, experience.CVId);
            }

            if (experience.PermanentProfessionalCareerExperience.IsNotNull())
            {
                experience.Experiences = map.MapPermanentProfessionalCareerExperience(experienceList, experience.PermanentProfessionalCareerExperience, experience.CVId);
            }
            if (experience.EditorialBoardmembers.IsNotNull())
            {
                experience.Experiences = map.MapToEditorialBoardmembers(experienceList, experience.EditorialBoardmembers, experience.CVId);
            }
            if (experience.EngagementWithBusinessAndSocietyExperience.IsNotNull())
            {
                experience.Experiences = map.MapToEngagementWithBusinessAndSociety(experienceList, experience.EngagementWithBusinessAndSocietyExperience, experience.CVId);
            }

            var dataTable = new DataTable();

            dataTable.Columns.Add("CVID");
            dataTable.Columns.Add("ExperienceCategoryID");
            dataTable.Columns.Add("StartDate", typeof(DateTime)).DateTimeMode = DataSetDateTime.Local;
            dataTable.Columns.Add("EndDate", typeof(DateTime)).DateTimeMode = DataSetDateTime.Local;
            dataTable.Columns.Add("SubjectArea");
            dataTable.Columns.Add("Position");
            dataTable.Columns.Add("Institution");
            dataTable.Columns.Add("City");
            dataTable.Columns.Add("Country");
            dataTable.Columns.Add("IsCurrent");
            dataTable.Columns.Add("isActive");
            dataTable.Columns.Add("ExperienceID");
            dataTable.Columns.Add("NoOfDays");
            dataTable.Columns.Add("Programme");
            dataTable.Columns.Add("KnowledgeArea");
            dataTable.Columns.Add("Client");
            dataTable.Columns.Add("Publication");
            dataTable.Columns.Add("PublicationStatus");
            dataTable.Columns.Add("PublicationEdition");



            foreach (var item in experience.Experiences)
            {
                var newRow = dataTable.NewRow();

                int? experienceID;
                if (item.ExperienceID == 0)
                {
                    experienceID = null;
                }
                else
                {
                    experienceID = item.ExperienceID;
                }

                newRow["CVID"] = cvId;
                newRow["ExperienceCategoryID"] = item.ExperienceCategoryID;
                newRow["StartDate"] = ResolveDate(item.StartDate);
                newRow["EndDate"] = ResolveDate(item.EndDate);
                newRow["SubjectArea"] = item.SubjectArea;
                newRow["Position"] = item.Position;
                newRow["Institution"] = item.Institution;
                newRow["City"] = item.City;
                newRow["Country"] = item.Country;
                newRow["IsCurrent"] = item.IsCurrent;
                newRow["isActive"] = item.isActive;
                newRow["ExperienceID"] = experienceID;
                newRow["NoOfDays"] = item.NoOfDays;
                newRow["Programme"] = item.Programme;
                newRow["KnowledgeArea"] = item.KnowledgeArea;
                newRow["Client"] = item.Client;
                newRow["Publication"] = item.Publication;
                newRow["PublicationStatus"] = item.PublicationStatus;
                newRow["PublicationEdition"] = item.PublicationEdition;


                dataTable.Rows.Add(newRow);
            }

            var tvpTable = new SqlParameter("tvpExperience", SqlDbType.Structured)
            {
                Value = dataTable,
                TypeName = "ExperienceTable"
            };

            var userName = new SqlParameter("UserName", loggedInUser);
            var output = new SqlParameter("ResultList", SqlDbType.NVarChar, 4000);
            output.Direction = ParameterDirection.Output;

            using (var entity = new FacultyManagementEntities())
            {
                var result = entity.Database.ExecuteSqlCommand("EXEC sp_ExperienceCUD @TVPExperience, @UserName", tvpTable, userName);
            }
        }

        #endregion

        #region AddOrUpdate FacultyDepartment

        public void AddOrUpdateFacultyDepartment(FacultyDepartment facultyDepartment, int cvId, string loggedInUser)
        {
            var dataTable = new DataTable();

            int? facultyDepartmentID;
            if (facultyDepartment.FacultyDepartmentID == 0)
            {
                facultyDepartmentID = null;
            }
            else
            {
                facultyDepartmentID = facultyDepartment.FacultyDepartmentID;
            }

            dataTable.Columns.Add("CVID");
            dataTable.Columns.Add("FacultyDepartmentID");
            dataTable.Columns.Add("DepartmentID");
            dataTable.Columns.Add("DepartmentCategoryID");
            dataTable.Columns.Add("isActive");

            var newRow = dataTable.NewRow();
            newRow["CVID"] = cvId;
            newRow["FacultyDepartmentID"] = facultyDepartmentID;
            newRow["DepartmentID"] = facultyDepartment.DepartmentID;
            newRow["DepartmentCategoryID"] = facultyDepartment.DepartmentCategoryID;
            newRow["isActive"] = facultyDepartment.isActive;

            dataTable.Rows.Add(newRow);

            var tvpTable = new SqlParameter("tvpFacultyDepartment", SqlDbType.Structured);
            tvpTable.Value = dataTable;
            tvpTable.TypeName = "FacultyDepartmentTable";

            var userName = new SqlParameter("UserName", loggedInUser);
            var output = new SqlParameter("ResultList", SqlDbType.NVarChar, 4000);
            output.Direction = ParameterDirection.Output;

            using (var entity = new FacultyManagementEntities())
            {
                var result = entity.Database.ExecuteSqlCommand("EXEC sp_FacultyDepartmentCUD @tvpFacultyDepartment, @UserName", tvpTable, userName);
            }
        }

        #endregion

        #region AddOrUpdate Grants

        public void AddOrUpdateGrants(List<GrantsFunding> grantsFunding, int cvId, string loggedInUser)
        {
            var dataTable = new DataTable();

            dataTable.Columns.Add("GrantID");
            dataTable.Columns.Add("CVID");
            dataTable.Columns.Add("Year");
            dataTable.Columns.Add("GrantFunding");
            dataTable.Columns.Add("Description");
            dataTable.Columns.Add("Country");
            dataTable.Columns.Add("isActive");

            foreach (var item in grantsFunding)
            {
                var newRow = dataTable.NewRow();

                int? grantId;
                if (item.GrantID == 0)
                {
                    grantId = null;
                }
                else
                {
                    grantId = item.GrantID;
                }

                newRow["GrantID"] = grantId;
                newRow["CVID"] = cvId;
                newRow["Year"] = item.Year;
                newRow["GrantFunding"] = item.GrantFunding;
                newRow["Description"] = item.Description;
                newRow["Country"] = item.Country;
                newRow["isActive"] = item.isActive;

                dataTable.Rows.Add(newRow);
            }

            var tvpTable = new SqlParameter("tvpGrant", SqlDbType.Structured);
            tvpTable.Value = dataTable;
            tvpTable.TypeName = "GrantTable";

            var userName = new SqlParameter("UserName", loggedInUser);
            var output = new SqlParameter("ResultList", SqlDbType.NVarChar, 4000);
            output.Direction = ParameterDirection.Output;

            using (var entity = new FacultyManagementEntities())
            {
                var result = entity.Database.ExecuteSqlCommand("EXEC sp_GrantCUD @tvpGrant, @UserName", tvpTable, userName);
            }
        }

        #endregion

        #region AddOrUpdate ManagementLoad

        public void AddOrUpdateManagementLoad(ManagementLoad managementLoad, int cvId, string loggedInUser)
        {
            var managementLoadList = new List<ManagementLoad>();
            var map = new FacultyManagementDataMapping();

            if (managementLoad.ProgrammeHead.IsNotNull())
            {
                managementLoad.ManagementLoads = map.MapProgrammeHead(managementLoadList, managementLoad.ProgrammeHead, managementLoad.CVId);
            }

            if (managementLoad.CentreHead.IsNotNull())
            {
                managementLoad.ManagementLoads = map.MapCentreHead(managementLoadList, managementLoad.CentreHead, managementLoad.CVId);
            }

            if (managementLoad.OrganisingAConference.IsNotNull())
            {
                managementLoad.ManagementLoads = map.MapOrganisingAConference(managementLoadList, managementLoad.OrganisingAConference, managementLoad.CVId);
            }

            if (managementLoad.LeadershipOneOnOne.IsNotNull())
            {
                managementLoad.ManagementLoads = map.MapLeadershipOneOnOne(managementLoadList, managementLoad.LeadershipOneOnOne, managementLoad.CVId);
            }
            if (managementLoad.ResearchReportOneOnOne.IsNotNull())
            {
                managementLoad.ManagementLoads = map.MapResearchReportOneOnOne(managementLoadList, managementLoad.ResearchReportOneOnOne, managementLoad.CVId);
            }
            if (managementLoad.ChairOfCommittee.IsNotNull())
            {
                managementLoad.ManagementLoads = map.MapChairOfCommittee(managementLoadList, managementLoad.ChairOfCommittee, managementLoad.CVId);
            }
            if (managementLoad.TaskTeamMember.IsNotNull())
            {
                managementLoad.ManagementLoads = map.MapTaskTeamMember(managementLoadList, managementLoad.TaskTeamMember, managementLoad.CVId);
            }
            if (managementLoad.ManagementLoadOther.IsNotNull())
            {
                managementLoad.ManagementLoads = map.MapManagementLoadOther(managementLoadList, managementLoad.ManagementLoadOther, managementLoad.CVId);
            }

            var dataTable = new DataTable();

            dataTable.Columns.Add("ManagementLoadCategoryID");
            dataTable.Columns.Add("CVID");
            dataTable.Columns.Add("isActive");
            dataTable.Columns.Add("ManagementLoadID");
            dataTable.Columns.Add("Credit");
            dataTable.Columns.Add("ModuleCount");
            dataTable.Columns.Add("StudentCount");
            dataTable.Columns.Add("Description");
            dataTable.Columns.Add("ProgrammeID");
            dataTable.Columns.Add("ProgrammeOfferingID");
            dataTable.Columns.Add("AccreditationReportID");
            dataTable.Columns.Add("ElectiveModuleCount");
            dataTable.Columns.Add("ElectiveSAQACredit");
            dataTable.Columns.Add("ManagementLoadYear");
            dataTable.Columns.Add("ManagementLoadStartDate", typeof(DateTime)).DateTimeMode = DataSetDateTime.Local;
			dataTable.Columns.Add("ManagementLoadEndDate", typeof(DateTime)).DateTimeMode = DataSetDateTime.Local;
			dataTable.Columns.Add("TaskTeamActivityID");

            using (var entity = new FacultyManagementEntities())
            {
                foreach (var item in managementLoad.ManagementLoads)
                {
                    var newRow = dataTable.NewRow();

                    int? id;
                    if (item.ManagementLoadID == 0)
                    {
                        id = null;
                    }
                    else
                    {
                        id = item.ManagementLoadID;
                    }

                    newRow["ManagementLoadCategoryID"] = item.ManagementLoadCategoryID;
                    newRow["CVID"] = cvId;
                    newRow["isActive"] = item.isActive;
                    newRow["ManagementLoadID"] = id;
                    newRow["Credit"] = item.Credit;
                    newRow["ModuleCount"] = item.ModuleCount;
                    newRow["StudentCount"] = item.StudentCount;
                    newRow["Description"] = item.Description;
                    newRow["ProgrammeID"] = item.ProgrammeID;
                    newRow["ProgrammeOfferingID"] = item.ProgrammeOfferingID;
                    newRow["AccreditationReportID"] = DBNull.Value;
                    newRow["ElectiveModuleCount"] = item.ElectiveModuleCount;
                    newRow["ElectiveSAQACredit"] = item.ElectiveSAQACredit;
                    newRow["ManagementLoadYear"] = item.ManagementLoadYear;

					if (item.ManagementLoadStartDate.HasValue)
					{
						newRow["ManagementLoadStartDate"] = ResolveDate(item.ManagementLoadStartDate);
					}

					if (item.ManagementLoadEndDate.HasValue)
					{
						newRow["ManagementLoadEndDate"] = ResolveDate(item.ManagementLoadEndDate);
					}

					newRow["TaskTeamActivityID"] = item.TaskTeamActivityID;

                    dataTable.Rows.Add(newRow);
                }

                var tvpTable = new SqlParameter("tvpManagementLoad", SqlDbType.Structured);
                tvpTable.Value = dataTable;
                tvpTable.TypeName = "ManagementLoadTable";

                var userName = new SqlParameter("UserName", loggedInUser);
                var output = new SqlParameter("ResultList", SqlDbType.NVarChar, 4000);
                output.Direction = ParameterDirection.Output;

                var result = entity.Database.ExecuteSqlCommand("EXEC sp_ManagementLoadCUD @tvpManagementLoad, @UserName", tvpTable, userName);
            }

        }

        #endregion

        #region AddOrUpdate PersonalInformation

        public int AddOrUpdatePersonalInformation(PersonalInformation personalInformation, string loggedInUserName)
        {
            //var personalInformation = JsonConvert.DeserializeObject<PersonalInformation>(jsonModel);
            int id;
            var personalInfo = new SqlParameter("tvpPersonalInfo", SqlDbType.Structured);
            var dataTable = new DataTable();

            dataTable.Columns.Add("CVID");
            dataTable.Columns.Add("Biography");
            dataTable.Columns.Add("Title");
            dataTable.Columns.Add("Initials");
            dataTable.Columns.Add("Name");
            dataTable.Columns.Add("Surname");
            dataTable.Columns.Add("Gender");
            dataTable.Columns.Add("NationalityID");
            dataTable.Columns.Add("BusinessContactNo");
            dataTable.Columns.Add("EmailAddress");
            dataTable.Columns.Add("Position");
            dataTable.Columns.Add("AcademicRank");
            dataTable.Columns.Add("HighestQualificationID");
            dataTable.Columns.Add("BusinessUnit");
            dataTable.Columns.Add("Expertise");
            dataTable.Columns.Add("EmployeeId");
            dataTable.Columns.Add("HomeInstitution");
            dataTable.Columns.Add("OrganisationalResponsibility");
            dataTable.Columns.Add("AffiliationID");
            dataTable.Columns.Add("ResearchInterest");
            dataTable.Columns.Add("MainActivityID");
            dataTable.Columns.Add("Samaccountname");
            dataTable.Columns.Add("isFaculty");

            var newRow = dataTable.NewRow();
            dataTable.Rows.Add(newRow);

            var userName = new SqlParameter("UserName", loggedInUserName);

            int? Id;
            if (personalInformation.CVId == 0)
            {
                Id = null;
            }
            else
            {
                Id = personalInformation.CVId;
            }

            newRow["CVID"] = Id;
            newRow["Biography"] = personalInformation.Biography;
            newRow["Title"] = personalInformation.Title;
            newRow["Initials"] = personalInformation.Initials;
            newRow["Name"] = personalInformation.Name;
            newRow["Surname"] = personalInformation.Surname;
            newRow["Gender"] = personalInformation.Gender;
            newRow["NationalityID"] = personalInformation.NationalityId;
            newRow["BusinessContactNo"] = personalInformation.BusinessContactNo;
            newRow["EmailAddress"] = personalInformation.EmailAddress;
            newRow["Position"] = personalInformation.Position;
            newRow["AcademicRank"] = personalInformation.AcademicRank;
            newRow["HighestQualificationID"] = personalInformation.HighestQualificationId;
            newRow["BusinessUnit"] = personalInformation.BusinessUnit;
            newRow["Expertise"] = personalInformation.Expertise;

            newRow["EmployeeID"] = personalInformation.EmployeeId;
            //if (personalInformation.EmployeeId.HasValue)
            //{
            //    newRow["EmployeeID"] = employeeId;
            //}
            //else
            //{
            //    newRow["EmployeeID"] = personalInformation.EmployeeId;
            //}

            newRow["HomeInstitution"] = personalInformation.HomeInstitution;
            newRow["OrganisationalResponsibility"] = personalInformation.OrganisationalResponsibility;
            newRow["AffiliationID"] = personalInformation.AffiliationID == 0 ? 1 : personalInformation.AffiliationID;
            newRow["ResearchInterest"] = personalInformation.ResearchInterest;
            newRow["MainActivityID"] = personalInformation.MainActivityID == 0 ? 1 : personalInformation.MainActivityID;
            newRow["Samaccountname"] = personalInformation.SamAccountName;
            newRow["isFaculty"] = personalInformation.isFaculty;

            personalInfo.Value = dataTable;
            personalInfo.TypeName = "PersonalInformationTable";

            var cvId = new SqlParameter("@CVID", SqlDbType.Int)
            {
                Direction = System.Data.ParameterDirection.Output
            };

            using (var entity = new FacultyManagementEntities())
            {
                id = entity.Database.ExecuteSqlCommand("EXEC sp_PersonalInformationCreate @tvpPersonalInfo, @UserName, @CVID out", personalInfo, userName, cvId);
            }

            return (int)cvId.Value;
        }

        #endregion

        #region AddOrUpdate PeopleInHistory

        public void AddOrUpdatePeopleInResearch(List<Contributor> peopleInResearch, int cvId, string loggedInUser)
        {
            var dataTable = new DataTable();

            dataTable.Columns.Add("PeopleinResearchID");
            dataTable.Columns.Add("ResearchID");
            dataTable.Columns.Add("CVID");
            dataTable.Columns.Add("PersonRoleID");
            dataTable.Columns.Add("isActive");
            dataTable.Columns.Add("Credit", typeof(decimal));
            dataTable.Columns.Add("ContributorSequence");
            dataTable.Columns.Add("Type");
            dataTable.Columns.Add("Name");
            dataTable.Columns.Add("Surname");
            dataTable.Columns.Add("Initials");
            dataTable.Columns.Add("Institution");
            dataTable.Columns.Add("Email");
            dataTable.Columns.Add("AffiliationID");
            dataTable.Columns.Add("Gender");
            dataTable.Columns.Add("NationalityID");

            using (var entity = new FacultyManagementEntities())
            {
                foreach (var item in peopleInResearch)
                {
                    var newRow = dataTable.NewRow();

                    int? peopleinResearchID;
                    if (item.PeopleinResearchID == 0)
                    {
                        peopleinResearchID = null;
                    }
                    else
                    {
                        peopleinResearchID = item.PeopleinResearchID;
                    }

                    newRow["PeopleinResearchID"] = peopleinResearchID;
                    newRow["ResearchID"] = item.ResearchID;
                    newRow["CVID"] = item.PeopleinResearchCVID;
                    newRow["PersonRoleID"] = item.PersonRoleID > 0 ? item.PersonRoleID : (object)DBNull.Value;
                    newRow["isActive"] = item.isActive;
                    newRow["Credit"] = item.Credit != null ? item.Credit : (object)DBNull.Value;
                    newRow["ContributorSequence"] = item.ContributorSequence;
                    newRow["Name"] = item.Name;
                    newRow["Surname"] = item.Surname;
                    newRow["Initials"] = item.Initials;
                    newRow["Institution"] = item.Institution;
                    newRow["Type"] = item.Type;
                    newRow["Email"] = item.Email;
                    newRow["AffiliationID"] = item.AffiliationID;
                    newRow["Gender"] = item.Gender;
                    newRow["NationalityID"] = item.NationalityID;


                    dataTable.Rows.Add(newRow);
                }

                var tvpTable = new SqlParameter("TVPPeopleinResearch", SqlDbType.Structured);
                tvpTable.Value = dataTable;
                tvpTable.TypeName = "PeopleinResearchTable";

                var userName = new SqlParameter("UserName", loggedInUser);
                //var output = new SqlParameter("ResultList", SqlDbType.NVarChar, 4000);
                //output.Direction = ParameterDirection.Output;

                var result = entity.Database.ExecuteSqlCommand("EXEC sp_PeopleinResearchCUD @TVPPeopleinResearch, @UserName", tvpTable, userName);
            }

        }

        #endregion

        #region AddOrUpdate Language

        public void AddOrUpdateFacultyLanguage(List<FacultyLanguage> list, int cvId, string loggedInUser)
        {
            var dataTable = new DataTable();

            dataTable.Columns.Add("CVID");
            dataTable.Columns.Add("LanguageID");
            dataTable.Columns.Add("isActive");
            dataTable.Columns.Add("FacultyLanguageID");

            using (var entity = new FacultyManagementEntities())
            {
                var result = entity.sp_FacultyLanguageRead(cvId).ToList();
                var dbLanguages = ResolveFacultyLanguage(result, cvId);

                var join = list.Concat(dbLanguages).ToList();

                foreach (var item in join)
                {
                    var match = list.Exists(l => l.LanguageID == item.LanguageID);
                    if (!match)
                    {
                        item.isActive = false;
                    }
                }

                SaveFacultyLanguage(dataTable, entity, join, cvId, loggedInUser);
            }
        }

        #endregion

        #region AddOrUpdate Publication

        public void AddOrUpdatePublication(string jsonModel, int cvId, string loggedInUser)
        {
            var list = JsonConvert.DeserializeObject<List<Publication>>(jsonModel);
            var dataTable = new DataTable();

            dataTable.Columns.Add("CVID");
            dataTable.Columns.Add("PublicationCategoryID");
            dataTable.Columns.Add("PublicationTitle");
            dataTable.Columns.Add("Title");
            dataTable.Columns.Add("Year");
            dataTable.Columns.Add("Date", typeof(DateTime)).DateTimeMode = DataSetDateTime.Local;
            dataTable.Columns.Add("SubjectArea");
            dataTable.Columns.Add("Institution");
            dataTable.Columns.Add("PublisherName");
            dataTable.Columns.Add("PublicationCity");
            dataTable.Columns.Add("NoOfPages");
            dataTable.Columns.Add("CoAuthorName");
            dataTable.Columns.Add("Promoter");
            dataTable.Columns.Add("ResearchType");
            dataTable.Columns.Add("VolumeNumber");
            dataTable.Columns.Add("StartPage");
            dataTable.Columns.Add("EndPage");
            dataTable.Columns.Add("JournalTitle");
            dataTable.Columns.Add("Author");
            dataTable.Columns.Add("Editor");
            dataTable.Columns.Add("BookTitle");
            dataTable.Columns.Add("Month");
            dataTable.Columns.Add("StartDate"); //, typeof(DateTime)).DateTimeMode = DataSetDateTime.Local;
            dataTable.Columns.Add("EndDate"); //, typeof(DateTime)).DateTimeMode = DataSetDateTime.Local;
            dataTable.Columns.Add("SeminarTitle");
            dataTable.Columns.Add("Location");
            dataTable.Columns.Add("ConferenceTitle");
            dataTable.Columns.Add("Country");
            dataTable.Columns.Add("CoPresenter");
            dataTable.Columns.Add("CoOrganiser");
            dataTable.Columns.Add("isActive");
            dataTable.Columns.Add("PublicationID");

            foreach (var item in list)
            {
                var newRow = dataTable.NewRow();

                newRow["CVID"] = cvId;
                newRow["PublicationCategoryID"] = item.PublicationCategoryID;
                newRow["PublicationTitle"] = item.PublicationTitle;
                newRow["Title"] = item.Title;
                newRow["Year"] = item.Year;
                newRow["Date"] = ResolveDate(item.Date);
                newRow["SubjectArea"] = item.SubjectArea;
                newRow["Institution"] = item.Institution;
                newRow["PublisherName"] = item.PublisherName;
                newRow["PublicationCity"] = item.PublicationCity;
                newRow["NoOfPages"] = item.NoOfPages;
                newRow["CoAuthorName"] = item.CoAuthorName;
                newRow["Promoter"] = item.Promoter;
                newRow["ResearchType"] = item.ResearchType;
                newRow["VolumeNumber"] = item.VolumeNumber;
                newRow["StartPage"] = item.StartPage;
                newRow["EndPage"] = item.EndPage;
                newRow["JournalTitle"] = item.JournalTitle;
                newRow["Author"] = item.Author;
                newRow["Editor"] = item.Editor;
                newRow["BookTitle"] = item.BookTitle;
                newRow["Month"] = item.Month;
                newRow["StartDate"] = ResolveDate(item.StartDate);
                newRow["EndDate"] = ResolveDate(item.EndDate);
                newRow["SeminarTitle"] = item.SeminarTitle;
                newRow["Location"] = item.Location;
                newRow["ConferenceTitle"] = item.ConferenceTitle;
                newRow["Country"] = item.Country;
                newRow["CoPresenter"] = item.CoPresenter;
                newRow["CoOrganiser"] = item.CoOrganiser;
                newRow["isActive"] = item.isActive;
                newRow["PublicationID"] = item.PublicationID;

                dataTable.Rows.Add(newRow);
            }

            var tvpTable = new SqlParameter("tvpPublication", SqlDbType.Structured);
            tvpTable.Value = dataTable;
            tvpTable.TypeName = "PublicationTable";

            var userName = new SqlParameter("UserName", loggedInUser);
            var output = new SqlParameter("ResultList", SqlDbType.NVarChar, 4000);
            output.Direction = ParameterDirection.Output;

            using (var entity = new FacultyManagementEntities())
            {
                var result = entity.Database.ExecuteSqlCommand("EXEC sp_PublicationCUD @TVPPublication, @UserName", tvpTable, userName);
            }
        }

        #endregion

        #region AddOrUpdate Research

        public string AddOrUpdateResearch(ResearchHistory item, int cvId, string loggedInUser)
        {
            var dbContributors = new List<sp_PeopleinResearchRead_Result>();
            var dataTable = new DataTable();

            dataTable.Columns.Add("CVID");
            dataTable.Columns.Add("ResearchCategoryID");
            dataTable.Columns.Add("Year");
            dataTable.Columns.Add("ResearchOutputCategoryID");
            dataTable.Columns.Add("DHETStatusID");
            dataTable.Columns.Add("LocusID");
            dataTable.Columns.Add("Description");
            dataTable.Columns.Add("Volume");
            dataTable.Columns.Add("PublisherName");
            dataTable.Columns.Add("PublicationCity");
            dataTable.Columns.Add("Country");
            dataTable.Columns.Add("NoOfPages");
            dataTable.Columns.Add("StartPage");
            dataTable.Columns.Add("EndPage");
            dataTable.Columns.Add("Edition");
            dataTable.Columns.Add("Issue");
            dataTable.Columns.Add("ArticleID");
            dataTable.Columns.Add("ArticleTitle");
            dataTable.Columns.Add("PublicationName");
            dataTable.Columns.Add("PublicationDate");
            dataTable.Columns.Add("SupervisionOrAssessment");
            dataTable.Columns.Add("Thesis");
            dataTable.Columns.Add("Candidate");
            dataTable.Columns.Add("isActive");
            dataTable.Columns.Add("ResearchID");
            dataTable.Columns.Add("CoordinatorID");
            dataTable.Columns.Add("JournalID");
            dataTable.Columns.Add("PublicationFile");
            dataTable.Columns.Add("Note");
            dataTable.Columns.Add("ABSRatingStatusID");
            dataTable.Columns.Add("Credit");
            dataTable.Columns.Add("ResearchContributionTypeID");
            //foreach (var item in researchList)
            //{
            var newRow = dataTable.NewRow();

            int? researchID;
            if (item.ResearchID == 0)
            {
                researchID = null;
            }
            else
            {
                researchID = item.ResearchID;
            }

            if (item.PublicationDate == null)
            {
                newRow["PublicationDate"] = DBNull.Value;
            }
            else
            {

                newRow["PublicationDate"] = item.PublicationDate;
            }

            newRow["CVID"] = item.IsContibutor ? item.ResearchCVID : cvId;
            newRow["ResearchCategoryID"] = item.ResearchCategoryID;
            newRow["Year"] = null;
            newRow["ResearchOutputCategoryID"] = item.ResearchOutputCategoryID > 0 ? item.ResearchOutputCategoryID : (object)DBNull.Value;
            newRow["DHETStatusID"] = item.DHETStatusID;
            newRow["LocusID"] = item.LocusID;
            newRow["Description"] = item.Description;
            newRow["Volume"] = item.Volume;
            newRow["PublisherName"] = item.PublisherName;
            newRow["PublicationCity"] = item.PublicationCity;
            newRow["Country"] = item.Country;
            newRow["NoOfPages"] = item.NoOfPages;
            newRow["StartPage"] = item.StartPage;
            newRow["EndPage"] = item.EndPage;
            newRow["Edition"] = item.Edition;
            newRow["Issue"] = item.Issue;
            newRow["ArticleID"] = item.ArticleID;
            newRow["ArticleTitle"] = item.ArticleTitle;
            newRow["PublicationName"] = item.PublicationName;
            newRow["SupervisionOrAssessment"] = null;
            newRow["Thesis"] = null;
            newRow["Candidate"] = null;
            newRow["isActive"] = item.isActive;
            newRow["ResearchID"] = researchID;
            newRow["CoordinatorID"] = item.CoordinatorID;
            newRow["JournalID"] = item.JournalID;
            newRow["PublicationFile"] = item.PublicationFile;
            newRow["Note"] = item.Note;
            newRow["ABSRatingStatusID"] = item.ABSRatingStatusID;
            newRow["Credit"] = item.Credit;
            newRow["ResearchContributionTypeID"] = item.ResearchContributionTypeID;

            dataTable.Rows.Add(newRow);

            var tvpTable = new SqlParameter("TVPResearch", SqlDbType.Structured);
            tvpTable.Value = dataTable;
            tvpTable.TypeName = "ResearchTable";

            var userName = new SqlParameter("UserName", loggedInUser);

            var researchIdOut = new SqlParameter("@ResearchID", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            };
            using (var entity = new FacultyManagementEntities())
            {
                entity.Database.ExecuteSqlCommand("EXEC sp_ResearchCUD @TVPResearch, @UserName, @ResearchID out", tvpTable, userName, researchIdOut);

                if (researchID.IsNull())
                {
                    dbContributors = entity.sp_PeopleinResearchRead((int)researchIdOut.Value).ToList();
                }
                else
                {
                    dbContributors = entity.sp_PeopleinResearchRead(researchID).ToList();
                }
            }

            var idOut = (int)researchIdOut.Value;

            if (researchID != null)
            {
                idOut = researchID.GetValueOrDefault();
            }

            var json = new JavaScriptSerializer().Serialize(dbContributors);
            var contributors = JsonConvert.DeserializeObject<List<Contributor>>(json);
            var joinedList = item.Contributors.Concat(contributors).ToList();

            //foreach (var listItem in joinedList)
            //{
            //    var match = item.Contributors.Exists(l => l.PeopleinResearchID == listItem.PeopleinResearchID);
            //    if (!match)
            //    {
            //        listItem.isActive = false;
            //    }
            //}

            //if (item.Contributors.IsNotNull() && item.Contributors.Any())
            if (joinedList.IsNotNull() && joinedList.Any())
            {
                //foreach (var contributor in item.Contributors)
                foreach (var contributor in joinedList)
                {
                    var match = item.Contributors.Exists(l => l.PeopleinResearchID == contributor.PeopleinResearchID);
                    if (!match)
                    {
                        contributor.isActive = false;
                        item.Contributors.Add(contributor);
                    }

                    contributor.ResearchID = idOut;
                }
                var task = new PeopleInResearchTask();
                task.UpsertPeopleInResearch(Convert.ToInt32(cvId), item.Contributors);

                AddOrUpdateResearchHistory(item, cvId, idOut, loggedInUser);
            }
            //}

            return "";
        }

        #endregion

        #region AddOrUpdate ResearchHistory

        private void AddOrUpdateResearchHistory(ResearchHistory item, int cvId, int researchId, string loggedInUser)
        {
            var dataTable = new DataTable();

            dataTable.Columns.Add("ResearchID");
            dataTable.Columns.Add("ResearchStatusID");
            dataTable.Columns.Add("ResearchStatusDate", typeof(DateTime));
            dataTable.Columns["ResearchStatusDate"].DateTimeMode = DataSetDateTime.Local;
            dataTable.Columns.Add("isPresent");
            dataTable.Columns.Add("isActive");
            dataTable.Columns.Add("ResearchHistoryID");

            using (var entity = new FacultyManagementEntities())
            {
                ResolveResearchHistoryUpdate(dataTable, researchId, item, entity, cvId);
                UpdateResearchHistory(dataTable, loggedInUser);
            }
        }

        #endregion

        #region AddOrUpdate Supervisions
        public void AddOrUpdateSupervision(Supervisions supervisions, int cvId, string loggedInUser, string sourceSystem)
        {
            var dataTable = new DataTable();

            dataTable.Columns.Add("SupervisionID");
            dataTable.Columns.Add("SecondarySupervisorCVID");
            dataTable.Columns.Add("Year");
            dataTable.Columns.Add("StudentName");
            dataTable.Columns.Add("ThesisTopic");
            dataTable.Columns.Add("Role");
            dataTable.Columns.Add("ProgrammeID");
            dataTable.Columns.Add("ProgrammeOfferingID");
            dataTable.Columns.Add("ProgressStatus");
            dataTable.Columns.Add("SupervisorCredit");
            dataTable.Columns.Add("SecondarySupervisorCredit");
            dataTable.Columns.Add("SourceSystem");


            var newRow = dataTable.NewRow();

            int? supervisionID;
            if (supervisions.SupervisionID == 0)
            {
                supervisionID = null;
            }
            else
            {
                supervisionID = supervisions.SupervisionID;
            }

            newRow["SupervisionID"] = supervisionID;
            newRow["SecondarySupervisorCVID"] = cvId;
            newRow["Year"] = supervisions.Year;
            newRow["StudentName"] = supervisions.StudentName;
            newRow["ThesisTopic"] = supervisions.ThesisTopic;
            newRow["Role"] = supervisions.Role;
            newRow["ProgrammeID"] = supervisions.ProgrammeID;
            newRow["ProgrammeOfferingID"] = supervisions.ProgrammeOfferingID;
            newRow["ProgressStatus"] = supervisions.ProgressStatus;
            newRow["SupervisorCredit"] = supervisions.SupervisorCredit;
            newRow["SecondarySupervisorCredit"] = supervisions.SecondarySupervisorCredit;
            newRow["SourceSystem"] = sourceSystem;


            dataTable.Rows.Add(newRow);

            var tvpTable = new SqlParameter("tvpPhdSupervision", SqlDbType.Structured);
            tvpTable.Value = dataTable;
            tvpTable.TypeName = "PHDSupervisionTable";

            var userName = new SqlParameter("UserName", loggedInUser);
            var output = new SqlParameter("ResultList", SqlDbType.NVarChar, 4000);
            output.Direction = ParameterDirection.Output;

            using (var entity = new FacultyManagementEntities())
            {
                var result = entity.Database.ExecuteSqlCommand("EXEC sp_PHDSupervisionCUD @tvpPhdSupervision, @UserName", tvpTable, userName);
            }
        }
        #endregion

        #region AddOrUpdate FacultyPermission

        public void AddOrUpdateFacultyPermission(string jsonModel, string loggedInUser)
        {
            var list = JsonConvert.DeserializeObject<List<FacultyPermission>>(jsonModel);
            var dataTable = new DataTable();

            dataTable.Columns.Add("CVID");
            dataTable.Columns.Add("ApplicationUserRoleID");
            dataTable.Columns.Add("FacultyPermissionID");
            dataTable.Columns.Add("isActive");

            foreach (var item in list)
            {
                var newRow = dataTable.NewRow();

                int? facultyPermissionID;
                if (item.FacultyPermissionID == 0)
                {
                    facultyPermissionID = null;
                }
                else
                {
                    facultyPermissionID = item.FacultyPermissionID;
                }

                newRow["CVID"] = item.CVID;
                newRow["ApplicationUserRoleID"] = item.ApplicationUserRoleID;
                newRow["FacultyPermissionID"] = item.FacultyPermissionID;
                newRow["isActive"] = item.isActive;

                dataTable.Rows.Add(newRow);
            }

            var tvpTable = new SqlParameter("tvpFacultyPermissionTable", SqlDbType.Structured);
            tvpTable.Value = dataTable;
            tvpTable.TypeName = "FacultyPermissionTable";

            var userName = new SqlParameter("UserName", loggedInUser);
            var output = new SqlParameter("ResultList", SqlDbType.NVarChar, 4000);
            output.Direction = ParameterDirection.Output;

            using (var entity = new FacultyManagementEntities())
            {
                var result = entity.Database.ExecuteSqlCommand("EXEC sp_FacultyPermissionCUD @tvpFacultyPermissionTable, @UserName", tvpTable, userName);
            }
        }

        #endregion

        #region Remove TeachingLoad
        public void RemoveTeachingLoad(int teachingLoadId, string loggedInUser)
        {
            int result;
            using (var entity = new FacultyManagementEntities())
            {
                result = entity.sp_TeachingLoadDelete(teachingLoadId, loggedInUser);
            }
        }

        #endregion

        #region Remove PerformanceWorkload
        public void RemovePerformanceWorkload(int performanceWorkloadId, string loggedInUser)
        {
            int result;
            using (var entity = new FacultyManagementEntities())
            {
                result = entity.sp_PerformanceWorkloadDelete(performanceWorkloadId, loggedInUser);
            }
        }

        #endregion

        #region Add TeachingLoad

        public void AddTeachingLoad(TeachingLoad teachingLoad, int cvId, string loggedInUser)
        {
            int result;
            using (var entity = new FacultyManagementEntities())
            {
                result = entity.sp_TeachingLoadCreate(teachingLoad.Year, cvId, teachingLoad.ProgrammeOffering,
                    teachingLoad.Programme, teachingLoad.Module, teachingLoad.StudentCount, teachingLoad.SessionCount,
                    Convert.ToInt32(teachingLoad.BaseCredit), Convert.ToInt32(teachingLoad.TotalCredits), loggedInUser);
            }
        }

        #endregion

        #region Add Supervision

        public void AddSupervision(Supervisions supervisions, int cvId, string loggedInUser, string sourceSystem)
        {
            int result;
            using (var entity = new FacultyManagementEntities())
            {
                result = entity.sp_SupervisionCreate(cvId, supervisions.SecondarySupervisorCVID, supervisions.Year, supervisions.StudentName,
                    supervisions.ThesisTopic, supervisions.Role, supervisions.ProgrammeID, supervisions.ProgrammeOfferingID, supervisions.ProgressStatus,
                    supervisions.SupervisorCredit, supervisions.SecondarySupervisorCredit, sourceSystem, loggedInUser);
            }
        }

        #endregion

        #region Add PerformanceWorkload

        public void AddPerformanceWorkload(PerformanceWorkload performanceWorkload, int cvId, string loggedInUser)
        {
            int result;
            using (var entity = new FacultyManagementEntities())
            {
                result = entity.sp_PerformanceWorkloadCreate(cvId, performanceWorkload.Year,
                    performanceWorkload.Percentage, performanceWorkload.Notes, loggedInUser);
            }
        }

        #endregion

        #endregion


        #region Resolves

        private void ResolvePersonalInformation(FacultyManagement facultyManagement, sp_PersonalInformationRead_Result result, int CVId)
        {
            facultyManagement.AcademicRank = result.AcademicRank;
            facultyManagement.AffiliationID = result.AffiliationID.GetValueOrDefault();
            facultyManagement.ApplicationUserRole = result.ApplicationUserRole;
            facultyManagement.ApplicationUserRoleID = result.ApplicationUserRoleID;
            facultyManagement.Biography = result.Biography;
            facultyManagement.BusinessContactNo = result.BusinessContactNo;
            facultyManagement.BusinessUnit = result.BusinessUnit;
            facultyManagement.EmailAddress = result.EmailAddress;
            facultyManagement.Expertise = result.Expertise;
            facultyManagement.HighestQualification = result.HighestQualification;
            facultyManagement.HighestQualificationID = result.HighestQualificationID;
            facultyManagement.HomeInstitution = result.HomeInstitution;
            //facultyManagement.Image = picture;
            facultyManagement.Initials = result.Initials;
            facultyManagement.isFaculty = result.isFaculty;
            facultyManagement.MainActivity = result.MainActivity;
            facultyManagement.MainActivityID = result.MainActivityID.GetValueOrDefault();
            facultyManagement.Name = result.Name;
            facultyManagement.ResearchInterest = result.ResearchInterest;
            facultyManagement.SamAccountName = result.Samaccountname;
            facultyManagement.Surname = result.Surname;
            facultyManagement.OrganisationalResponsibility = result.OrganisationalResponsibility;
            facultyManagement.Position = result.Position;
            facultyManagement.Title = result.Title;
            facultyManagement.CVID = CVId;
            facultyManagement.SamAccountName = result.Samaccountname;
        }

        private List<Award> ResolveAward(List<sp_AwardRead_Result> result, int CVID)
        {
            var list = new List<Award>();

            foreach (var item in result)
            {
                list.Add(new Award()
                {
                    AwardID = item.AwardID,
                    AwardDescription = item.AwardDescription,
                    AwardType = item.AwardType,
                    Country = item.Country,
                    CVId = CVID,
                    isActive = item.isActive.GetValueOrDefault(),
                    IsNew = false,
                    Year = item.Year
                });
            }

            return list;
        }

        private List<CourseDesign> ResolveCourseDesign(List<sp_CourseDesignRead_Result> result, int CVID)
        {
            var list = new List<CourseDesign>();

            foreach (var item in result)
            {
                list.Add(new CourseDesign()
                {
                    ContactTime = item.ContactTime,
                    CourseDesignID = item.CourseDesignID,
                    CVId = CVID,
                    Module = item.Module,
                    ProgrammeID = item.ProgrammeID,
                    TotalCredit = item.TotalCredit.GetValueOrDefault(),
                    isActive = true
                });
            }

            return list;
        }

        private List<Research> ResolveResearch(List<sp_ResearchRead_Result> result, int CVID)
        {
            var list = new List<Research>();

            foreach (var item in result)
            {
                list.Add(new Research()
                {
                    ResearchID = item.ResearchID,
                    CVId = CVID,
                    Candidate = item.Candidate,
                    ResearchCategoryID = item.ResearchCategoryID,
                    Description = item.Description,
                    isActive = item.isActive.GetValueOrDefault(),
                    IsNew = false,
                    PublicationDate = item.PublicationDate,
                    PublicationName = item.PublicationName,
                    SupervisionOrAssessment = item.SupervisionOrAssessment,
                    Thesis = item.Thesis,
                    Year = item.Year,
                    JournalID = item.JournalID
                });
            }

            return list;
        }

        private List<FacultyLanguage> ResolveFacultyLanguage(List<sp_FacultyLanguageRead_Result> result, int CVID)
        {
            var list = new List<FacultyLanguage>();

            foreach (var item in result)
            {
                list.Add(new FacultyLanguage()
                {
                    CVId = CVID,
                    FacultyLanguageID = item.FacultyLanguageID,
                    isActive = item.isActive.GetValueOrDefault(),
                    IsNew = false,
                    LanguageName = item.Language,
                    LanguageID = item.LanguageID
                });
            }
            return list;
        }

        private List<Language> ResolveLanguage(List<sp_LanguageRead_Result> result, int CVID)
        {
            var list = new List<Language>();

            foreach (var item in result)
            {
                list.Add(new Language()
                {
                    isActive = item.isActive,
                    LanguageName = item.Language,
                    LanguageID = item.LanguageID,
                });
            }
            return list;
        }

        private List<Education> ResolveEducation(List<sp_EducationRead_Result> result, int CVID)
        {
            var list = new List<Education>();

            foreach (var item in result)
            {
                list.Add(new Education()
                {
                    EducationID = item.EducationID,
                    Country = item.Country,
                    CVId = CVID,
                    Degree = item.Degree,
                    FieldOfStudy = item.FieldOfStudy,
                    isActive = item.isActive.GetValueOrDefault(),
                    IsNew = false,
                    University = item.University,
                    Year = item.Year
                });
            }

            return list;
        }

        private List<GrantsFunding> ResolveGrant(List<sp_GrantRead_Result> result, int CVID)
        {
            var list = new List<GrantsFunding>();

            foreach (var item in result)
            {
                list.Add(new GrantsFunding()
                {
                    Country = item.Country,
                    CVId = CVID,
                    Description = item.Description,
                    GrantFunding = item.GrantFunding,
                    GrantID = item.GrantID,
                    isActive = true,
                    IsNew = false,
                    Year = item.Year
                });
            }
            return list;
        }

        private List<Publication> ResolvePublication(List<sp_PublicationRead_Result> result, int CVID)
        {
            var list = new List<Publication>();

            foreach (var item in result)
            {
                list.Add(new Publication()
                {
                    PublicationID = item.PublicationID,
                    //Author = item.Author,
                    //BookTitle = item.BookTitle,
                    //CoAuthorName = item.CoAuthorName,
                    //ConferenceTitle = item.ConferenceTitle,
                    CoOrganiser = item.CoOrganiser,
                    CoPresenter = item.CoPresenter,
                    Country = item.Country,
                    CVId = CVID,
                    Date = item.Date,
                    Editor = item.Editor,
                    EndDate = item.EndDate,
                    EndPage = item.EndPage,
                    Institution = item.Institution,
                    isActive = item.isActive.GetValueOrDefault(),
                    IsNew = true,
                    JournalTitle = item.JournalTitle,
                    Location = item.Location,
                    Month = item.Month,
                    NoOfPages = item.NoOfPages,
                    Promoter = item.Promoter,
                    PublicationCategoryID = item.PublicationCategoryID,
                    PublicationCity = item.PublicationCity,
                    PublicationTitle = item.PublicationTitle,
                    PublisherName = item.PublisherName,
                    //ResearchType = item.ResearchType,
                    //SeminarTitle = item.SeminarTitle,
                    StartDate = item.StartDate,
                    StartPage = item.StartPage,
                    SubjectArea = item.SubjectArea,
                    //Title = item.Title,
                    VolumeNumber = item.VolumeNumber,
                    Year = item.Year
                });
            }

            return list;
        }

        private List<Experience> ResolveExperience(List<sp_ExperienceRead_Result1> result, int CVID)
        {
            var list = new List<Experience>();

            foreach (var item in result)
            {
                list.Add(new Experience()
                {
                    ExperienceID = item.ExperienceID,
                    City = item.City,
                    Client = item.Client,
                    Country = item.Country,
                    EndDate = item.EndDate,
                    ExperienceCategoryID = item.ExperienceCategoryID,
                    Institution = item.Institution,
                    IsCurrent = item.IsCurrent,
                    IsNew = false,
                    KnowledgeArea = item.KnowledgeArea,
                    NoOfDays = item.NoOfDays,
                    Position = item.Position,
                    Programme = item.Programme,
                    StartDate = item.StartDate,
                    SubjectArea = item.SubjectArea,
                    isActive = true,
                    Publication = item.Publication,
                    PublicationEdition = item.PublicationEdition,
                    PublicationStatus = item.PublicationStatus
                });
            }

            return list;
        }

        private List<Development> ResolveDevelopment(List<sp_DevelopmentRead_Result> result, int CVID)
        {
            var list = new List<Development>();

            foreach (var item in result)
            {
                list.Add(new Development()
                {
                    DevelopmentID = item.DevelopmentID,
                    Accomplishment = item.Accomplishment,
                    City = item.City,
                    Country = item.Country,
                    CVId = CVID,
                    DevelopmentCategoryID = item.DevelopmentCategoryID,
                    EndDate = item.EndDate,
                    Institution = item.Institution,
                    isActive = item.isActive.GetValueOrDefault(),
                    IsCurrent = item.IsCurrent,
                    IsNew = false,
                    NoOfDays = item.NoOfDays,
                    Organisation = item.Organisation,
                    Position = item.Position,
                    StartDate = item.StartDate,
                    Topic = item.Topic,
                });
            }
            return list;
        }

        private List<ManagementLoad> ResolveManagementLoadRead(List<sp_ManagementLoadRead_Result> result, int CVID)
        {
            var list = new List<ManagementLoad>();

            foreach (var item in result)
            {
                list.Add(new ManagementLoad()
                {
                    CVId = CVID,
                    isActive = item.isActive.GetValueOrDefault(),
                    IsNew = false,
                    ManagementLoadCategoryID = item.ManagementLoadCategoryID.GetValueOrDefault(),
                    Credit = item.Credit,
                    ManagementLoadDescription = item.ManagementLoadDescription,
                    Description = item.Description,
                    ManagementLoadID = item.ManagementLoadID,
                    ModuleCount = item.ModuleCount,
                    ProgrammeID = item.ProgrammeID,
                    ProgrammeOfferingID = item.ProgrammeOfferingID,
                    StudentCount = item.StudentCount,
                    ElectiveSAQACredit = item.ElectiveSAQACredit,
                    ElectiveModuleCount = item.ElectiveModuleCount,
                    ManagementLoadYear = item.ManagementLoadYear,
                    ManagementLoadStartDate = item.ManagementLoadStartDate,
                    ManagementLoadEndDate = item.ManagementLoadEndDate,
                    TaskTeamActivityID = item.TaskTeamActivityID
                });
            }

            return list;
        }

        //private List<Supervisions> ResolveSupervisionLoadRead(List<sp_SupervisionRead_Result> result, int CVID)
        //{
        //    var list = new List<Supervisions>();

        //    foreach (var item in result.OrderBy(r => r.Year))
        //    {
        //        list.Add(new Supervisions()
        //        {
        //            CVId = CVID,
        //            isActive = item.isActive,
        //            ProgrammeDescription = item.ProgrammeDescription,
        //            ProgrammeID = item.ProgrammeID,
        //            ProgrammeOfferingDescription = item.ProgrammeOfferingDescription,
        //            ProgrammeOfferingID = item.ProgrammeOfferingID,
        //            //ProgressStatus = item.ProgressStatus,
        //            Role = item.Role,
        //            SecondarySupervisorCredit = item.SecondarySupervisorCredit,
        //            SecondarySupervisorCVID = item.SecondarySupervisorCVID,
        //            //SourceSystem = item.SourceSystem,
        //            StudentName = item.StudentName,
        //            SupervisionID = item.SupervisionID,
        //            SupervisorCredit = item.SupervisorCredit,
        //            SupervisorCVID = item.SupervisorCVID,
        //            ThesisTopic = item.ThesisTopic,
        //            Year = item.Year
        //        });
        //    }

        //    return list;
        //}

        private FacultyDepartmentBundle ResolveFacultyDepartmentLoadRead(ObjectResult<sp_FacultyDepartmentRead_Result> result, int CVID)
        {
            var facultyDepartmentBundle = new FacultyDepartmentBundle();

            if (result != null)
            {
                facultyDepartmentBundle.CVId = CVID;

                foreach (sp_FacultyDepartmentRead_Result facultyDepartment in result)
                {
                    var department = new FacultyDepartment();
                    department.FacultyDepartmentID = facultyDepartment.FacultyDepartmentID;
                    department.DepartmentCategoryName = facultyDepartment.DepartmentCategoryName;
                    department.DepartmentName = facultyDepartment.DepartmentName;
                    department.DepartmentCategoryID = facultyDepartment.DepartmentCategoryID;
                    department.DepartmentID = facultyDepartment.DepartmentID;
                    department.isActive = facultyDepartment.isActive.Value;
                    facultyDepartmentBundle.DepartmentCategories.Add(department);
                }
            }

            return facultyDepartmentBundle;
        }

        private DateTime? ResolveDate(DateTime? date)
        {
            var localDate = new Nullable<DateTime>();
            localDate = date;

            var dt = localDate.GetValueOrDefault().ToLocalTime();
            return dt;
        }

        #region Private Methods

        private void SaveFacultyLanguage(DataTable dataTable, FacultyManagementEntities entity, List<FacultyLanguage> list, int cvId, string loggedInUser)
        {
            foreach (var item in list)
            {
                var newRow = dataTable.NewRow();

                int? facultyLanguageID;
                if (item.FacultyLanguageID == 0)
                {
                    facultyLanguageID = null;
                }
                else
                {
                    facultyLanguageID = item.FacultyLanguageID;
                }

                newRow["CVID"] = cvId;
                newRow["LanguageID"] = item.LanguageID;
                newRow["isActive"] = item.isActive;
                newRow["FacultyLanguageID"] = facultyLanguageID;

                dataTable.Rows.Add(newRow);
            }

            var tvpTable = new SqlParameter("TVPFacultyLanguage", SqlDbType.Structured);
            tvpTable.Value = dataTable;
            tvpTable.TypeName = "FacultyLanguageTable";

            var userName = new SqlParameter("UserName", loggedInUser);
            var output = new SqlParameter("ResultList", SqlDbType.NVarChar, 4000);
            output.Direction = ParameterDirection.Output;

            var result = entity.Database.ExecuteSqlCommand("EXEC sp_FacultyLanguageCUD @TVPFacultyLanguage, @UserName", tvpTable, userName);
        }

        private void CreateResearchAndResearchHistory(ResearchHistory item, int cvId, string loggedInUser)
        {
            var entity = new FacultyManagementEntities();
            //var result = entity.sp_ResearchCreate(cvId, item.Description, item.ResearchStatusDate, item.ABSRatingStatusID,
            //    item.ResearchOutputCategoryID, item.LocusID, item.DHETStatusID, item.Volume, item.Issue, item.PublicationDate,
            //    item.ArticleID, item.ArticleTitle, null, 5, loggedInUser);
        }

        private DataTable ResolveResearchHistoryUpdate(DataTable dt, int researchId, ResearchHistory item, FacultyManagementEntities entity, int cvId)
        {
            var researchHistory = entity.sp_ResearchHistoryRead(cvId).Where(r => r.ResearchID == researchId).ToList();

            bool isPresent;

            if (item.isPresent == null)
            {
                isPresent = false;
            }
            else
            {
                isPresent = true;
            }

            //Current Research
            if (item.ResearchCurrentDate.HasValue)
            {
                var currentRow = dt.NewRow();
                currentRow["ResearchID"] = researchId;
                currentRow["ResearchStatusID"] = 2;
                currentRow["ResearchStatusDate"] = item.ResearchCurrentDate;
                currentRow["isPresent"] = isPresent;
                currentRow["isActive"] = item.ResearchCurrentDate.HasValue;
                currentRow["ResearchHistoryID"] = ResolveResearchHistoryId(researchHistory, 2);

                dt.Rows.Add(currentRow);
            }

            //Submitted Research
            if (item.ResearchSubmittedDate.HasValue)
            {
                var submittedRow = dt.NewRow();
                submittedRow["ResearchID"] = researchId;
                submittedRow["ResearchStatusID"] = 3;
                submittedRow["ResearchStatusDate"] = item.ResearchSubmittedDate;
                submittedRow["isPresent"] = isPresent;
                submittedRow["isActive"] = item.ResearchSubmittedDate.HasValue;
                submittedRow["ResearchHistoryID"] = ResolveResearchHistoryId(researchHistory, 3);

                dt.Rows.Add(submittedRow);
            }

            //Accepted Research

            if (item.ResearchAcceptedDate.HasValue)
            {
                var acceptedRow = dt.NewRow();
                acceptedRow["ResearchID"] = researchId;
                acceptedRow["ResearchStatusID"] = 4;
                acceptedRow["ResearchStatusDate"] = item.ResearchAcceptedDate;
                acceptedRow["isPresent"] = isPresent;
                acceptedRow["isActive"] = item.ResearchAcceptedDate.HasValue;
                acceptedRow["ResearchHistoryID"] = ResolveResearchHistoryId(researchHistory, 4);

                dt.Rows.Add(acceptedRow);
            }

            //Publication Research
            if (item.ResearchPublicationDate.HasValue)
            {
                var publicationRow = dt.NewRow();
                publicationRow["ResearchID"] = researchId;
                publicationRow["ResearchStatusID"] = 5;
                publicationRow["ResearchStatusDate"] = item.ResearchPublicationDate;
                publicationRow["isPresent"] = isPresent;
                publicationRow["isActive"] = item.ResearchPublicationDate.HasValue;
                publicationRow["ResearchHistoryID"] = ResolveResearchHistoryId(researchHistory, 5);

                dt.Rows.Add(publicationRow);
            }
            return dt;
        }

        private int? ResolveResearchHistoryId(List<sp_ResearchHistoryRead_Result> researchHistory, int status)
        {
            var history = researchHistory.Where(r => r.ResearchStatusID == status).FirstOrDefault();

            int? researchHistoryID;
            if (history.IsNull())
            {
                return null;
            }

            if (history.ResearchHistoryID == 0)
            {
                researchHistoryID = null;
            }
            else
            {
                researchHistoryID = history.ResearchHistoryID;
            }

            return researchHistoryID;
        }

        private string UpdateResearchHistory(DataTable dt, string loggedInUser)
        {
            var historyDetails = new SqlParameter("TVPResearchHistory", SqlDbType.Structured);
            historyDetails.Value = dt;
            historyDetails.TypeName = "ResearchHistoryTable";

            var userName = new SqlParameter("UserName", loggedInUser);

            var output = new SqlParameter("ResultList", SqlDbType.NVarChar, 4000);
            output.Direction = ParameterDirection.Output;

            using (var entity = new FacultyManagementEntities())
            {
                var result = entity.Database.ExecuteSqlCommand("EXEC sp_ResearchHistoryCreate @TVPResearchHistory, @UserName", historyDetails, userName);
            }

            return output.Value?.ToString() ?? string.Empty;
        }

        public PublicationUploadResult UploadPublication(string fileName, int[] cvId, byte[] fileData)
        {
            var uploadResult = new PublicationUploadResult();

            var result = Sharepoint.CampusFaculty.UpdloadResearch(fileName, cvId, DateTime.Now, fileData);

            uploadResult.Result = result.Result;
            uploadResult.ResultDetails = result.ResultDetails;

            return uploadResult;
        }

        #endregion

        #endregion

        public int LoadGoogleStudentHIndex(int cvId)
        {
            using (var entity = new FacultyManagementEntities())
            {
                var result = entity.sp_CVRead().Where(c => c.CVID == cvId).FirstOrDefault();

                if (result.IsNotNull())
                {
                    return result.GoogleScholarHIndex.GetValueOrDefault();
                }

                return 0;
            }
        }

        public void UpdateUserPermissions(PermissionsAdminModel[] permissionsAdminModels, int? cvid)
        {
            var dataTable = new DataTable();
            var columns = new[]
            {
                new DataColumn("AssignorCVID"),
                new DataColumn("AssigneeCVID"),
                new DataColumn("ApplicationUserRoleID"),
                new DataColumn("isActive"),
                new DataColumn("FacultyPermissionID")
            };
            dataTable.Columns.AddRange(columns);

            foreach (var model in permissionsAdminModels)
            {
                if(model._cvId == 0)
                    continue;

                if (model.development)
                {
                    var newRow = dataTable.NewRow();
                    newRow["AssigneeCVID"] = model._cvId;
                    newRow["AssignorCVID"] = cvid;
                    newRow["isActive"] = model.isActive;
                    newRow["FacultyPermissionID"] = 0;
                    newRow["ApplicationUserRoleID"] = (int) PermissionsIdMap.Development;
                    dataTable.Rows.Add(newRow);
                }
                if (model.education)
                {
                    var newRow = dataTable.NewRow();
                    newRow["AssigneeCVID"] = model._cvId;
                    newRow["AssignorCVID"] = cvid;
                    newRow["isActive"] = model.isActive;
                    newRow["FacultyPermissionID"] = 0;
                    newRow["ApplicationUserRoleID"] = (int) PermissionsIdMap.Education;
                    dataTable.Rows.Add(newRow);
                }
                if (model.experience)
                {
                    var newRow = dataTable.NewRow();
                    newRow["AssigneeCVID"] = model._cvId;
                    newRow["AssignorCVID"] = cvid;
                    newRow["isActive"] = model.isActive;
                    newRow["FacultyPermissionID"] = 0;
                    newRow["ApplicationUserRoleID"] = (int) PermissionsIdMap.Experience;
                    dataTable.Rows.Add(newRow);
                }
                if (model.managementLoad)
                {
                    var newRow = dataTable.NewRow();
                    newRow["AssigneeCVID"] = model._cvId;
                    newRow["AssignorCVID"] = cvid;
                    newRow["isActive"] = model.isActive;
                    newRow["FacultyPermissionID"] = 0;
                    newRow["ApplicationUserRoleID"] = (int) PermissionsIdMap.ManagementLoad;
                    dataTable.Rows.Add(newRow);
                }
                if (model.personalInformation)
                {
                    var newRow = dataTable.NewRow();
                    newRow["AssigneeCVID"] = model._cvId;
                    newRow["AssignorCVID"] = cvid;
                    newRow["isActive"] = model.isActive;
                    newRow["FacultyPermissionID"] = 0;
                    newRow["ApplicationUserRoleID"] = (int) PermissionsIdMap.PersonalInformation;
                    dataTable.Rows.Add(newRow);
                }
                if (model.publications)
                {
                    var newRow = dataTable.NewRow();
                    newRow["AssigneeCVID"] = model._cvId;
                    newRow["AssignorCVID"] = cvid;
                    newRow["isActive"] = model.isActive;
                    newRow["FacultyPermissionID"] = 0;
                    newRow["ApplicationUserRoleID"] = (int) PermissionsIdMap.Publications;
                    dataTable.Rows.Add(newRow);
                }
                if (model.research)
                {
                    var newRow = dataTable.NewRow();
                    newRow["AssigneeCVID"] = model._cvId;
                    newRow["AssignorCVID"] = cvid;
                    newRow["isActive"] = model.isActive;
                    newRow["FacultyPermissionID"] = 0;
                    newRow["ApplicationUserRoleID"] = (int) PermissionsIdMap.Research;
                    dataTable.Rows.Add(newRow);
                }
                if (model.supervisionLoad)
                {
                    var newRow = dataTable.NewRow();
                    newRow["AssigneeCVID"] = model._cvId;
                    newRow["AssignorCVID"] = cvid;
                    newRow["isActive"] = model.isActive;
                    newRow["FacultyPermissionID"] = 0;
                    newRow["ApplicationUserRoleID"] = (int) PermissionsIdMap.SupervisionLoad;
                    dataTable.Rows.Add(newRow);
                }
                if (model.teachingLoad)
                {
                    var newRow = dataTable.NewRow();
                    newRow["AssigneeCVID"] = model._cvId;
                    newRow["AssignorCVID"] = cvid;
                    newRow["isActive"] = model.isActive;
                    newRow["FacultyPermissionID"] = 0;
                    newRow["ApplicationUserRoleID"] = (int) PermissionsIdMap.TeachingLoad;
                    dataTable.Rows.Add(newRow);

                }
            }
            var tvpTable = new SqlParameter("tvpPermissions", SqlDbType.Structured)
            {
                Value = dataTable,
                TypeName = "UserAssignedPermissions"
            };
            using (var entity = new FacultyManagementEntities())
            {
                entity.Database.ExecuteSqlCommand("EXEC sp_UserRoleAssignmentCUD @tvpPermissions", tvpTable);
            }

        }
        public void FixEfProviderServicesProblem()
        {
            //The Entity Framework provider type 'System.Data.Entity.SqlServer.SqlProviderServices, EntityFramework.SqlServer'
            //for the 'System.Data.SqlClient' ADO.NET provider could not be loaded. 
            //Make sure the provider assembly is available to the running application. 
            //See http://go.microsoft.com/fwlink/?LinkId=260882 for more information.

            var instance = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }
    }

    public enum PermissionsIdMap
    {
        PersonalInformation = 4,
        Education = 5,
        Experience = 6,
        Research = 7,
        Publications = 8,
        Development = 9,
        TeachingLoad = 10,
        SupervisionLoad = 11,
        ManagementLoad = 12
    }
}
