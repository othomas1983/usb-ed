﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Xml;
using Microsoft.SharePoint.Client;
//using Microsoft.SharePoint.Client.Utilities;
using USB.Models.FacultyManagement;
using SettingsMan = USB_ED.Properties.Settings;
namespace Sharepoint
{
    /// <summary>
    /// Summary description for CampusFaculty
    /// </summary>
    [WebService(Namespace = "http://www.usb.ac.za/",
    Description = "A service to submit or retrieve BPC staff and faculty",
    Name = "CampusFaculty")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class CampusFaculty : System.Web.Services.WebService
    {
        [WebMethod]
        public static PublicationUploadResult UpdloadResearch(string filename, int[] cvIDs, DateTime publicationDate, byte[] document)
        {
            //replace each CVID with a SharePoint ListItemID from the CV List
            var result = new PublicationUploadResult();
            try
            {
                using (ClientContext spClient = new ClientContext(SettingsMan.Default.catalogueSiteUrl))
                {
                    spClient.AuthenticationMode = ClientAuthenticationMode.Default;
                    spClient.Credentials = new System.Net.NetworkCredential("airb06", "BPCb0rn3", "belpark");
                    var spDocLibResearch = spClient.Web.Lists.GetByTitle(SettingsMan.Default.researchListTitle);

                    var itemInfo = new FileCreationInformation();
                    itemInfo.Url = SettingsMan.Default.catalogueSiteUrl + SettingsMan.Default.researchListName + @"/" + filename;
                    itemInfo.Content = document;
                    var uploadFile = spDocLibResearch.RootFolder.Files.Add(itemInfo);
                    spClient.Load(uploadFile);
                    spClient.ExecuteQuery();

                    CamlQuery cvFilter = new CamlQuery();    //filter by room number
                    cvFilter.ViewXml = "<View><Query><Where><In><FieldRef Name='CVID'/><Values>";

                    foreach (int cvID in cvIDs)
                    {
                        cvFilter.ViewXml += "<Value Type='Number'>" + cvID.ToString() + "</Value>";
                    }

                    cvFilter.ViewXml += "</Values></In></Where></Query></View>";

                    var cvList = spClient.Web.Lists.GetByTitle(SettingsMan.Default.cvListName);
                    var facultyMembers = cvList.GetItems(cvFilter);
                    spClient.Load(facultyMembers);
                    spClient.ExecuteQuery();

                    if (facultyMembers.Count > 0)
                    {
                        foreach (var cvId in cvIDs)
                        {
                            var lecturers = new List<FieldLookupValue>();
                            foreach (var facultyMember in facultyMembers)
                            {
                                FieldLookupValue lecturer = new FieldLookupValue { LookupId = facultyMember.Id };
                                lecturers.Add(lecturer);
                            }
                            //uploadFile.ListItemAllFields["cvID"] = .Id;
                            uploadFile.ListItemAllFields["Lecturers"] = lecturers;
                            uploadFile.ListItemAllFields["Title"] = filename.Substring(0, filename.LastIndexOf('.'));
                            uploadFile.ListItemAllFields["ResearchPublicationDate"] = publicationDate;
                        }
                    }
                    else
                    {
                        //throw new Exception("No faculty members were found matching the CV IDs submitted.");
                        result.Successful = false;
                        result.Result = "No faculty members were found matching the CV IDs submitted.";
                    }

                    uploadFile.ListItemAllFields.Update();
                    spClient.ExecuteQuery();
                    result.Result = "Success";
                    result.Successful = true;

                }
            }
            catch (Exception cEx)
            {
                result.Result = cEx.Message;
                result.Successful = false;
                result.ResultDetails = cEx.Message;
            }

            //Upload file
            return result;
        }
        
    }
}