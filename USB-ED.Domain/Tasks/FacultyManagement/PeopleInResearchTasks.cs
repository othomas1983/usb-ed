﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using USB.Models.FacultyManagement;
using USB_ED.Global;
using USB_ED.Models;

namespace USB_ED.Tasks
{
    public class PeopleInResearchTask
    {
       


        public void UpsertPeopleInResearch(int CVId, List<Contributor> peopleInResearch)
        {
            var task = new FacultyManagementTasks();
            task.AddOrUpdatePeopleInResearch(peopleInResearch, CVId,SessionHelper.LoggedInUser.EmployeeName);
        }
    }
}

