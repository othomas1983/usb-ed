﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using USB.Models.FacultyManagement;
using USB_ED.Properties;
using SettingsMan = USB_ED.Properties.Settings;
namespace USB_ED.ActiveDirectory
{
    public class ActiveDirectoryUser
    {
        public int EmployeeId { get; set; }
        public string GivenName { get; set; }
        public string Surname { get; set; }
        public string Description { get; set; }
        public string EmailAddress { get; set; }
        public string VoiceTelephoneNumber { get; set; }
    }

    public class ActiveDirectory
    {
        public static ActiveDirectoryUser Login(string userName, string password)
        {
            string domain = Settings.Default.ActivedirectoryDomain; //"bpcad01.belpark.sun.ac.za"; //LDAP://bpcad01.belpark.sun.ac.za/ 
            //const string container = @"OU=BELPARK Staff, DC=belpark,DC=sun,DC=ac,DC=za";// @"DC=Foo,DC=Bar,DC=biz";@"LDAP://OU=BELPARK Staff,DC=belpark,DC=sun,DC=ac,DC=za"
            const string container = @"DC=belpark,DC=sun,DC=ac,DC=za";

            try
            {
                PrincipalContext principalContext;
                using (principalContext = new PrincipalContext(ContextType.Domain, domain, container, userName, password))
                {
                    var result = principalContext.ValidateCredentials(userName, password);
                    return result ? LoadUserInformation(userName, principalContext) : null;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        private static ActiveDirectoryUser LoadUserInformation(string userName, PrincipalContext principalContext)
        {
            string department = string.Empty;
            var ADUser = new ActiveDirectoryUser();

            UserPrincipal user = UserPrincipal.FindByIdentity(principalContext, userName);

            DirectoryEntry directoryEntry = user.GetUnderlyingObject() as DirectoryEntry;

            if (directoryEntry.Properties.Contains("department"))
                department = directoryEntry.Properties["department"].Value.ToString();

            ADUser.EmployeeId = Convert.ToInt32(user.EmployeeId);
            ADUser.GivenName = user.GivenName;
            ADUser.Surname = user.Surname;
            ADUser.Description = user.Description;
            ADUser.EmailAddress = user.EmailAddress;
            ADUser.VoiceTelephoneNumber = user.VoiceTelephoneNumber;

            //var users = Users.LoadAllUsers(user, principalContext);

            return ADUser;
        }
    }
}