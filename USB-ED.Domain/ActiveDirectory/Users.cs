﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using USB_ED.Properties;
using SettingsMan = USB_ED.Properties.Settings;

namespace USB_ED.ActiveDirectory
{
    public class FacultyUser
    {
        public string EmployeeId { get; set; }
        public string DisplayName { get; set; }
        public string GivenName { get; set; }
        public string Surname { get; set; }
        public string Description { get; set; }
        public string EmailAddress { get; set; }
        public string VoiceTelephoneNumber { get; set; }
        public string SamAccountName { get; set; }
    }

    public class Users
    {
        public static List<FacultyUser> ADUsers { get; set; }
        
        public List<FacultyUser> LoadADUsers(string userName, string password)
        {
            ADUsers = new List<FacultyUser>();
            var principalContext = Login(userName, password);
            GroupPrincipal findAllGroups = new GroupPrincipal(principalContext, "*");
            //PrincipalSearcher ps = new PrincipalSearcher(findAllGroups);
            using (var searcher = new PrincipalSearcher(new UserPrincipal(principalContext)))
            //using (var searcher = new PrincipalSearcher(findAllGroups))
            {
                foreach (var result in searcher.FindAll().OrderBy(u => u.DisplayName))
                {
                    var match = true;
                    var u = (UserPrincipal)(result);
                    if (u.EmailAddress.IsNotNullOrEmpty())
                    {
                        var mail = u.EmailAddress.ToUpper();
                        match = mail.Contains("@SPL") || mail.Contains("USB-ED");
                    }
                   
                    if (!match)
                    {
                        if (u.EmployeeId.IsNotNullOrEmpty() && u.GivenName.IsNotNullOrEmpty() && u.DisplayName.IsNotNullOrEmpty())
                        {
                            ADUsers.Add(new FacultyUser()
                            {
                                Description = u.Description,
                                DisplayName = u.Surname + " " + u.GivenName,
                                EmailAddress = u.EmailAddress,
                                EmployeeId = u.EmployeeId,
                                GivenName = u.GivenName,
                                Surname = u.Surname,
                                VoiceTelephoneNumber = u.VoiceTelephoneNumber,
                                SamAccountName = u.SamAccountName
                                
                            });
                        }
                    }
                }
            }
            /* DO NOT REMOVE*/
            //System.IO.StreamWriter sw = new System.IO.StreamWriter("c:\\Projects\\ADUsers.csv");
            //sw.WriteLine("DisplayName|Surname|GivenName|EmployeeId|Description|SamAccountName");
            //foreach (var item in ADUsers)
            //{
            //    sw.WriteLine(item.DisplayName + "|" + item.Surname + "|" + item.GivenName + "|" + item.EmployeeId + "|" + item.Description + "|" + item.SamAccountName);
            //}
            //sw.Close();


            return ADUsers;
        }

        private PrincipalContext Login(string userName, string password)
        {
            string domain = Settings.Default.ActivedirectoryDomain; //"bpcad01.belpark.sun.ac.za"; //LDAP://bpcad01.belpark.sun.ac.za/ 
            const string container = @"OU=BELPARK Staff, DC=belpark,DC=sun,DC=ac,DC=za";// @"DC=Foo,DC=Bar,DC=biz";@"LDAP://OU=BELPARK Staff,DC=belpark,DC=sun,DC=ac,DC=za"

            //PrincipalContext principalContext = null;

            try
            {
                var principalContext = new PrincipalContext(ContextType.Domain, domain, container, userName, password);               
                var result = principalContext.ValidateCredentials(userName, password);

                if (result)
                {
                    return principalContext;
                }
                else
                {
                    return null;
                }
                
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
