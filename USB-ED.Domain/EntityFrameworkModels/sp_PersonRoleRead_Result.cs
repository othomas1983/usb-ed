//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace USB_ED.EntityFrameworkModels
{
    using System;
    
    public partial class sp_PersonRoleRead_Result
    {
        public int PersonRoleID { get; set; }
        public string PersonRoleDescription { get; set; }
        public bool isActive { get; set; }
    }
}
