﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading.Tasks;
using USB_ED.Models;
using USB_ED.Models.VenueBooking;
using System.Web;
using MigraDoc.DocumentObjectModel;
using MigraDoc.Rendering;
using System.IO;
using System.Security.AccessControl;

namespace USB_ED
{
    public class HtmlToPdf
    {
        public System.Web.HttpResponse Response { get; set; }

        public Stream Convert(string fileName, VenueBookingForm model)
        {
            var document = CreateDocument(model);

            MigraDoc.DocumentObjectModel.IO.DdlWriter.WriteToFile(document, AppDomain.CurrentDomain.BaseDirectory +  "\\MigraDoc.mdddl");
            PdfDocumentRenderer renderer = new PdfDocumentRenderer(true, PdfSharp.Pdf.PdfFontEmbedding.Always);

            renderer.Document = document;
            renderer.RenderDocument();


            string path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            if (path.StartsWith(@"file:"))
            {
                path = path.Remove(0, path.ToUpper().IndexOf("C:"));
            }
            if (!Directory.Exists(path + "\\App_TempVenueBookingFiles"))
            {
                Directory.CreateDirectory(path + "\\App_TempVenueBookingFiles");
            }

            var filename = path + "\\App_TempVenueBookingFiles\\CateringRequirements " + DateTime.Now.ToString("dd-MM-yyyy hh-mm") + ".pdf";
            var i = 0;
            while (File.Exists(filename))
            {
                i++;
                filename = path + "\\App_TempVenueBookingFiles\\CateringRequirements " + DateTime.Now.ToString("dd-MM-yyyy hh-mm") + " [" + i + "].pdf";
            }
            renderer.PdfDocument.Close();
            renderer.PdfDocument.Save(filename);

            var fileStream = new FileStream(filename, FileMode.Open, FileAccess.Read);

            return fileStream;
        }

        private Document CreateDocument(VenueBookingForm model)
        {
            Document document = new Document();
            document.Info.Title = "Catering Requirements";
            document.Info.Author = "University of Stellenbosch Business School";

            DefineStyles(document);
            DefineCover(document, model);

            return document;
        }

        public static void DefineCover(Document document, VenueBookingForm model)
        {
            Section section = document.AddSection();

            Paragraph paragraph = section.AddParagraph("Venue Booking");
            paragraph.Format.Font.Name = "";
            paragraph.Format.Font.Size = 24;
            paragraph.Format.Font.Color = Colors.DarkGray;
            paragraph.Format.SpaceAfter = "1cm";
            paragraph.Format.Borders.Width = 0.5;
            paragraph.Format.Borders.Color = Colors.LightGray;
            paragraph.Format.Borders.Distance = 3;
            //paragraph.AddText(FillerText.MediumText);

            //Image image = section.AddImage("..//..//images//Logo landscape.png");
            //image.Width = 10;
            var result = model.FullDay ? "Full Day Event" : "Half Day Event";
            paragraph = section.AddParagraph("Event Information - " + result, "Heading2");

            paragraph = section.AddParagraph("Event Name: " + model.EventName);
            paragraph.Format.LeftIndent = "1cm";
            paragraph.Format.Font.Size = 12;
            paragraph.Format.Font.Color = Colors.DarkGray;

            paragraph = section.AddParagraph("Number of people expected: " + model.BookingNumberOfPeopleExpected);
            paragraph.Format.LeftIndent = "1cm";
            paragraph.Format.Font.Size = 12;
            paragraph.Format.Font.Color = Colors.DarkGray;

            paragraph = section.AddParagraph("Booking Date: " + model.BookingDate.ToString("dd MMM yyyy"));
            paragraph.Format.LeftIndent = "1cm";
            paragraph.Format.Font.Size = 12;
            paragraph.Format.Font.Color = Colors.DarkGray;

            paragraph = section.AddParagraph("Time In: " + model.BookingTimeIn);
            paragraph.Format.LeftIndent = "1cm";
            paragraph.Format.Font.Size = 12;
            paragraph.Format.Font.Color = Colors.DarkGray;

            paragraph = section.AddParagraph("Time Out: " + model.BookingTimeOut);
            paragraph.Format.LeftIndent = "1cm";
            paragraph.Format.Font.Size = 12;
            paragraph.Format.Font.Color = Colors.DarkGray;

            paragraph = section.AddParagraph("Billing Information", "Heading2");

            paragraph = section.AddParagraph("Person Requesting Venue: " + model.PersonRequestingVenue);
            paragraph.Format.LeftIndent = "1cm";
            paragraph.Format.Font.Size = 12;
            paragraph.Format.Font.Color = Colors.DarkGray;

            paragraph = section.AddParagraph("Contact Number: " + model.ContactNumber);
            paragraph.Format.LeftIndent = "1cm";
            paragraph.Format.Font.Size = 12;
            paragraph.Format.Font.Color = Colors.DarkGray;

            paragraph = section.AddParagraph("E-Mail: " + model.eMail);
            paragraph.Format.LeftIndent = "1cm";
            paragraph.Format.Font.Size = 12;
            paragraph.Format.Font.Color = Colors.DarkGray;

            paragraph = section.AddParagraph("Company Name: " + model.CompanyName);
            paragraph.Format.LeftIndent = "1cm";
            paragraph.Format.Font.Size = 12;
            paragraph.Format.Font.Color = Colors.DarkGray;

            paragraph = section.AddParagraph("Billing Address: " + model.BillingAddress);
            paragraph.Format.LeftIndent = "1cm";
            paragraph.Format.Font.Size = 12;
            paragraph.Format.Font.Color = Colors.DarkGray;

            paragraph = section.AddParagraph("VAT Number: " + model.VATNumber);
            paragraph.Format.LeftIndent = "1cm";
            paragraph.Format.Font.Size = 12;
            paragraph.Format.Font.Color = Colors.DarkGray;

            paragraph = section.AddParagraph("VAT Registration Number: " + model.VATRegistrationNumber);
            paragraph.Format.LeftIndent = "1cm";
            paragraph.Format.Font.Size = 12;
            paragraph.Format.Font.Color = Colors.DarkGray;

            paragraph = section.AddParagraph("Person To Be Billed: " + model.PersonToBeBilled);
            paragraph.Format.LeftIndent = "1cm";
            paragraph.Format.Font.Size = 12;
            paragraph.Format.Font.Color = Colors.DarkGray;

            paragraph = section.AddParagraph("Catering Requirements", "Heading2");

            if (model.CateringRequirements.CateringPackageA)
            {
                CateringPackage(section, paragraph, "Package A", "Tea & Coffee on arrival." + Environment.NewLine +
                    "Mid Morning Tea & Coffee served with a biscuit." + Environment.NewLine +
                    "Meal of the day with a glass of juice OR cans." + Environment.NewLine +
                    "Tea & Coffee.");

                paragraph = section.AddParagraph();
                CateringPackageItems(section, paragraph, "Arrival Time", model.CateringRequirements.CateringPackageATeaCoffeeArrivalTime);
                CateringPackageItems(section, paragraph, "Midmorning Time", model.CateringRequirements.CateringPackageATeaCoffeeMidmorningTime);
                CateringPackageItems(section, paragraph, "Afternoon Time", model.CateringRequirements.CateringPackageATeaCoffeeAfternoonTime);
                CateringPackageItems(section, paragraph, "Lunch Time", model.CateringRequirements.CateringPackageALunchTime);
            }

            if (model.CateringRequirements.CateringPackageB)
            {
                CateringPackage(section, paragraph, "Package B", "Tea & Coffee on arrival." + Environment.NewLine +
                    "Mid Morning Tea & Coffee served with Tartlets." + Environment.NewLine +
                    "Buffet lunch with a glass of juice OR cans." + Environment.NewLine +
                    "Afternoon Tea & Coffee with biscuits.");

                paragraph = section.AddParagraph();
                CateringPackageItems(section, paragraph, "Arrival Time", model.CateringRequirements.CateringPackageBTeaCoffeeArrivalTime);
                CateringPackageItems(section, paragraph, "Midmorning Time", model.CateringRequirements.CateringPackageBTeaCoffeeMidmorningTime);
                CateringPackageItems(section, paragraph, "Afternoon Time", model.CateringRequirements.CateringPackageBTeaCoffeeAfternoonTime);
                CateringPackageItems(section, paragraph, "Lunch Time", model.CateringRequirements.CateringPackageBLunchTime);
            }

            if (model.CateringRequirements.CateringPackageC)
            {
                CateringPackage(section, paragraph, "Package C", "Tea & Coffee on arrival." + Environment.NewLine +
                    "Mid Morning Tea & Coffee served with a tea snack." + Environment.NewLine +
                    "Buffet lunch with a glass of juice OR cans." + Environment.NewLine +
                    "Tea & Cofee with biscuits.");

                paragraph = section.AddParagraph();
                CateringPackageItems(section, paragraph, "Arrival Time", model.CateringRequirements.CateringPackageCTeaCoffeeArrivalTime);
                CateringPackageItems(section, paragraph, "Midmorning Time", model.CateringRequirements.CateringPackageCTeaCoffeeMidmorningTime);
                CateringPackageItems(section, paragraph, "Afternoon Time", model.CateringRequirements.CateringPackageCTeaCoffeeAfternoonTime);
                CateringPackageItems(section, paragraph, "Lunch Time", model.CateringRequirements.CateringPackageCLunchTime);
            }

            var props = model.CateringRequirements.GetType().GetProperties().Where(
                prop => Attribute.IsDefined(prop, typeof(RequirementAttribute)));

            foreach (var item in props)
            {
                var selected = model.CateringRequirements.GetType().GetProperty(item.Name).GetValue(model.CateringRequirements).AsBool().GetValueOrDefault();

                if (selected)
                {
                    object[] attribute = item.GetCustomAttributes(typeof(RequirementAttribute), true);
                    var displayName = (attribute[0] as RequirementAttribute).DisplayName;
                    var hasTimes = (attribute[0] as RequirementAttribute).HasTimes;
                    var count = 0;
                    if (model.CateringRequirements.GetType().GetProperty(item.Name + "Count").GetValue(model.CateringRequirements).IsNotNull())
                    {
                        count = (int)model.CateringRequirements.GetType().GetProperty(item.Name + "Count").GetValue(model.CateringRequirements);
                    }

                    if (hasTimes)
                    {
                        var morningTime = model.CateringRequirements.GetType().GetProperty(item.Name + "MorningTime").GetValue(model.CateringRequirements).AsString();
                        var midMorningTime = model.CateringRequirements.GetType().GetProperty(item.Name + "MidMorningTime").GetValue(model.CateringRequirements).AsString();
                        var lunchTime = model.CateringRequirements.GetType().GetProperty(item.Name + "LunchTime").GetValue(model.CateringRequirements).AsString();
                        var AfternoonTime = model.CateringRequirements.GetType().GetProperty(item.Name + "AfternoonTime").GetValue(model.CateringRequirements).AsString();
                        var eveningTime = model.CateringRequirements.GetType().GetProperty(item.Name + "EveningTime").GetValue(model.CateringRequirements).AsString();

                        CateringRequirements(section, paragraph, displayName,
                        count,
                        morningTime,
                        midMorningTime,
                        lunchTime,
                        AfternoonTime,
                        eveningTime);
                    }
                    else
                    {
                        CateringRequirements(section, paragraph, displayName,
                        count);
                    }
                }
            }
        }

        static void CateringPackage(Section section, Paragraph paragraph, string packageName, string packageDescription)
        {
            paragraph = section.AddParagraph(packageName);
            paragraph.Format.LeftIndent = "1cm";
            paragraph.Format.Font.Size = 12;
            paragraph.Format.Font.Color = Colors.DarkGray;
            paragraph.Format.Font.Bold = true;

            paragraph = section.AddParagraph(packageDescription);
            paragraph.Format.LeftIndent = "2cm";
            paragraph.Format.Font.Size = 12;
            paragraph.Format.Font.Color = Colors.DarkGray;
        }

        static void CateringPackageItems(Section section, Paragraph paragraph, string itemName, string itemTime)
        {
            paragraph = section.AddParagraph(itemName + ": " + itemTime);
            paragraph.Format.LeftIndent = "2cm";
            paragraph.Format.Font.Size = 12;
            paragraph.Format.Font.Color = Colors.DarkGray;
        }

        static void CateringRequirements(Section section, Paragraph paragraph, string requirement, int? count, string morningTime,
            string midMorningTime, string lunchTime, string AfternoonTime, string eveningTime)
        {
            paragraph = section.AddParagraph(requirement);
            paragraph.Format.LeftIndent = "1cm";
            paragraph.Format.Font.Size = 12;
            paragraph.Format.Font.Color = Colors.DarkGray;
            paragraph.Format.Font.Bold = true;

            paragraph = section.AddParagraph("Number of people: " + count);
            paragraph.Format.LeftIndent = "2cm";
            paragraph.Format.Font.Size = 12;
            paragraph.Format.Font.Color = Colors.DarkGray;

            paragraph = section.AddParagraph("Morning Time: " + (morningTime != string.Empty ? morningTime : "N/A"));
            paragraph.Format.LeftIndent = "2cm";
            paragraph.Format.Font.Size = 12;
            paragraph.Format.Font.Color = Colors.DarkGray;

            paragraph = section.AddParagraph("Mid Morning Time: " + (midMorningTime != string.Empty ? midMorningTime : "N/A"));
            paragraph.Format.LeftIndent = "2cm";
            paragraph.Format.Font.Size = 12;
            paragraph.Format.Font.Color = Colors.DarkGray;

            paragraph = section.AddParagraph("Lunch Time: " + (lunchTime != string.Empty ? lunchTime : "N/A"));
            paragraph.Format.LeftIndent = "2cm";
            paragraph.Format.Font.Size = 12;
            paragraph.Format.Font.Color = Colors.DarkGray;

            paragraph = section.AddParagraph("After Noon Time: " + (AfternoonTime != string.Empty ? AfternoonTime : "N/A"));
            paragraph.Format.LeftIndent = "2cm";
            paragraph.Format.Font.Size = 12;
            paragraph.Format.Font.Color = Colors.DarkGray;

            paragraph = section.AddParagraph("Evening Time: " + (eveningTime != string.Empty ? eveningTime : "N/A"));
            paragraph.Format.LeftIndent = "2cm";
            paragraph.Format.Font.Size = 12;
            paragraph.Format.Font.Color = Colors.DarkGray;
        }

        static void CateringRequirements(Section section, Paragraph paragraph, string requirement, int? count)
        {
            paragraph = section.AddParagraph(requirement);
            paragraph.Format.LeftIndent = "1cm";
            paragraph.Format.Font.Size = 12;
            paragraph.Format.Font.Color = Colors.DarkGray;
            paragraph.Format.Font.Bold = true;

            paragraph = section.AddParagraph("Number: " + count);
            paragraph.Format.LeftIndent = "2cm";
            paragraph.Format.Font.Size = 12;
            paragraph.Format.Font.Color = Colors.DarkGray;
        }

        static void DefineContentSection(Document document)
        {
            Section section = document.AddSection();
            section.PageSetup.OddAndEvenPagesHeaderFooter = true;
            section.PageSetup.StartingNumber = 1;

            HeaderFooter header = section.Headers.Primary;
            header.AddParagraph("\tOdd Page Header");

            header = section.Headers.EvenPage;
            header.AddParagraph("Even Page Header");

            // Create a paragraph with centered page number. See definition of style "Footer".
            Paragraph paragraph = new Paragraph();
            paragraph.AddTab();
            paragraph.AddPageField();

            // Add paragraph to footer for odd pages.
            section.Footers.Primary.Add(paragraph);
            // Add clone of paragraph to footer for odd pages. Cloning is necessary because an object must
            // not belong to more than one other object. If you forget cloning an exception is thrown.
            section.Footers.EvenPage.Add(paragraph.Clone());
        }

        private string ConvertToHtml(VenueBookingForm model, CateringRequirements cateringRequirements)
        {
            var html = string.Empty;

            html += "<ul style=\"margin: 0 0 0.5em 1.5em;padding: 0;\">";

            html += "<li style=\"margin: 0; padding: 0;\">{0}</li>".FormatInvariantCulture(model.EventName.OrDefault("&nbsp;"));
            html += "<li style=\"margin: 0; padding: 0;\">{0}</li>".FormatInvariantCulture(model.BookingNumberOfPeopleExpected.AsString().OrDefault("&nbsp;"));
            html += "<li style=\"margin: 0; padding: 0;\">{0}</li>".FormatInvariantCulture(model.BookingDate.AsString().OrDefault("&nbsp;"));
            html += "<li style=\"margin: 0; padding: 0;\">{0}</li>".FormatInvariantCulture(model.BookingTimeIn.OrDefault("&nbsp;"));
            html += "<li style=\"margin: 0; padding: 0;\">{0}</li>".FormatInvariantCulture(model.BookingTimeOut.OrDefault("&nbsp;"));
            html += "</ul>";

            return html;
        }

        public static void DefineStyles(Document document)
        {
            // Get the predefined style Normal.
            Style style = document.Styles["Normal"];
            // Because all styles are derived from Normal, the next line changes the 
            // font of the whole document. Or, more exactly, it changes the font of
            // all styles and paragraphs that do not redefine the font.
            style.Font.Name = "Calibri";

            // Heading1 to Heading9 are predefined styles with an outline level. An outline level
            // other than OutlineLevel.BodyText automatically creates the outline (or bookmarks) 
            // in PDF.

            style = document.Styles["Heading1"];
            style.Font.Name = "Calibri";
            style.Font.Size = 14;
            style.Font.Bold = true;
            style.Font.Color = Colors.DarkGray;
            style.ParagraphFormat.PageBreakBefore = true;
            style.ParagraphFormat.SpaceAfter = 6;

            style = document.Styles["Heading2"];
            style.Font.Size = 12;
            style.Font.Bold = true;
            style.ParagraphFormat.PageBreakBefore = false;
            style.ParagraphFormat.SpaceBefore = 6;
            style.ParagraphFormat.SpaceAfter = 3;


            style = document.Styles["Heading3"];
            style.Font.Size = 10;
            style.Font.Bold = true;
            style.ParagraphFormat.SpaceBefore = 6;
            style.ParagraphFormat.SpaceAfter = 3;

            style = document.Styles[StyleNames.Header];
            style.ParagraphFormat.AddTabStop("16cm", TabAlignment.Right);

            style = document.Styles[StyleNames.Footer];
            style.ParagraphFormat.AddTabStop("8cm", TabAlignment.Center);

            // Create a new style called TextBox based on style Normal
            style = document.Styles.AddStyle("TextBox", "Normal");
            style.ParagraphFormat.Alignment = ParagraphAlignment.Justify;
            style.ParagraphFormat.Borders.Width = 2.5;
            style.ParagraphFormat.Borders.Distance = "3pt";
            style.ParagraphFormat.Shading.Color = Colors.SkyBlue;

            // Create a new style called TOC based on style Normal
            style = document.Styles.AddStyle("TOC", "Normal");
            style.ParagraphFormat.AddTabStop("16cm", TabAlignment.Right, TabLeader.Dots);
            style.ParagraphFormat.Font.Color = Colors.Blue;
        }

    }
}
