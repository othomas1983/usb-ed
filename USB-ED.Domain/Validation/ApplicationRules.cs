﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace USB_ED.Validation
{
    #region EmailValidation

    public sealed class ValidateEmailAddress : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value.IsNotNull())
            {
                var emailExpression = @"^(\w+([-+.']\w+)*)@([\w-]+\.)+[\w-]+$".AsString();
                var valid = Regex.Match(value.ToString(), emailExpression).Success;

                if (!valid)
                {
                    return new ValidationResult(
                        FormatErrorMessage(value.ToString()));
                }
            }

            return ValidationResult.Success;
        }

        public override string FormatErrorMessage(string name)
        {
            return "{0} is an invalid email address".FormatInvariantCulture(name);
        }
    }

   

    #endregion
}