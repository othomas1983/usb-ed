﻿namespace USB_ED.Models
{
    public class MyCommitment
    {
        public int CommitmentID { get; set; }

        public int PerformanceCategoryID { get; set; }

        public string PerformanceCategory { get; set; }

        public int CVID { get; set; }

        public int Year { get; set; }

        public string BaseLine { get; set; }

        public string Commitment { get; set; }

        public string CommitmentDesc { get; set; }
    }
}
