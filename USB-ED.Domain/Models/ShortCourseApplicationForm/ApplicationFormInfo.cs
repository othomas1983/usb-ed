﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using SettingsMan = USB_ED.Properties.Settings;
namespace USB_ED.Models
{
    public class ApplicationFormInfo
    {
        protected string shortCourseOfferingsUrl = SettingsMan.Default.shortCourseOfferingsUrl;
        protected string shortCourseOfferingsOpenCRMUrl = SettingsMan.Default.shortCourseOfferingsOpenCRMUrl;
        protected string gendersUrl = SettingsMan.Default.gendersUrl;
        protected string titlesUrl = SettingsMan.Default.titlesUrl;
        protected string ethnicitiesUrl = SettingsMan.Default.ethnicitiesUrl;
        protected string nationalitiesUrl = SettingsMan.Default.nationalitiesUrl;
        protected string foreignIdentificationTypesUrl = SettingsMan.Default.foreignIdentificationTypesUrl;        
        protected string addressCountriesUrl = SettingsMan.Default.addressCountriesUrl;
        protected string languagesUrl = SettingsMan.Default.languagesUrl;
        protected string dietaryRequirementsUrl = SettingsMan.Default.dietaryRequirementsUrl;
        protected string industriesUrl = SettingsMan.Default.industriesUrl;
        protected string workAreasUrl = SettingsMan.Default.workAreasUrl;
        protected string marketingReasonUrl = SettingsMan.Default.marketingReasonUrl;
        protected string marketingSourcesUrl = SettingsMan.Default.marketingSourcesUrl;
        protected string qualificationTypesUrl = SettingsMan.Default.qualificationTypesUrl;
        protected string qualificationFieldsUrl = SettingsMan.Default.qualificationFieldsUrl;
        protected string disabilitiesUrl = SettingsMan.Default.disabilitiesUrl;
        protected string paymentResponsibilitiesUrl = SettingsMan.Default.paymentResponsibilitiesUrl;
        protected string occupationalCategoriesUrl = SettingsMan.Default.occupationalCategoriesUrl;                

        #region Properties
        
        public static ApplicationFormInfo Info;

        public List<SelectListItem> ShortCourseOfferings { get; set; }
        public List<SelectListItem> Languages { get; set; }
        public List<SelectListItem> Nationalities { get; set; }
        public List<SelectListItem> AddressCountries { get; set; }
        public List<SelectListItem> Titles { get; set; }
        public List<SelectListItem> Genders { get; set; }
        public List<SelectListItem> Ethnicities { get; set; }
        public List<SelectListItem> EthnicitiesNonSA { get; set; }
        public List<SelectListItem> DietaryRequirements { get; set; }
        public List<SelectListItem> Industries { get; set; }
        public List<SelectListItem> WorkAreas { get; set; }
        public List<SelectListItem> OccupationalCategories { get; set; }
        public List<SelectListItem> IdDocumentTypes { get; set; }
        public List<SelectListItem> PaymentResponsibility { get; set; }
        public List<SelectListItem> MarketingReasons { get; set; }
        public List<SelectListItem> MarketingSources { get; set; }
        public List<SelectListItem> QualificationTypes { get; set; }
        public List<SelectListItem> QualificationFields { get; set; }
        public List<SelectListItem> Disabilities { get; set; }
        public List<SelectListItem> YesNoList { get; set; }
        public List<SelectListItem> YearsList { get; set; }

        #endregion

        #region Public Methods

        public ApplicationFormInfo LoadInfoMembers(ApplicationForm model, string courseId, string offeringId)
        {
            try
            {
                WebClient client = new WebClient();
                client.Encoding = Encoding.UTF8;
                model.DisplayNameApproved = false;


                //if (Info.IsNull())
                //{
                    Info = new ApplicationFormInfo();

                    Info.ShortCourseOfferings = LoadShortCourseOfferings(model, client, courseId, offeringId);

                    model.HasListItems = true;

                    LoadTitles(client, Info);

                    LoadLanguages(client, Info);

                    LoadGenders(client, Info);

                    LoadNationalities(client, Info);

                    LoadAddressCountries(client, Info);

                    LoadDietaryRequirements(client, Info);

                    LoadPaymentResponsibility(client, Info);

                    LoadEthnicities(client, Info);

                    LoadEthnicitiesNonSA(client, Info);

                    LoadIndustries(client, Info);

                    LoadWorkAreas(client, Info);

                    LoadWorkOccupationalCategories(client, Info);

                    LoadIdDocumentTypes(client, Info);

                    LoadMarketingReasons(client, Info);

                    LoadMarketingSources(client, Info);

                    LoadQualificationTypes(client, Info);

                    LoadQualificationFields(client, Info);

                    LoadDisabilities(client, Info);

                    LoadYesNoList(Info);

                    LoadYearsList(Info);
                //}
                return Info;
            }

            catch
            {
                model.HasListItems = false;
                Info = null;
                return Info;
            }
        }



        #endregion

        #region Private Methods

        private string ResolveCourseStartDate(string value)
        {
            DateTime dateValue;

            if (DateTime.TryParse(value, out dateValue))
            {
                return dateValue.ToString("dd MMM yyyy");
            }
            else
            {
                return null;
            }
        }

        public List<SelectListItem> LoadShortCourseOfferings(ApplicationForm model, WebClient client, string courseId, string offeringId)
        {
            var url = string.Empty;

            if (model.DirectOfferingLink)
            {
                model.DirectOfferingLink = true;
                url = shortCourseOfferingsOpenCRMUrl + "?offeringId=" + offeringId;
            }
            else
            {

                if (model.InsideCRM)
                {
                    model.InsideCRM = true;
                    if (offeringId.IsNotNull())
                    {
                        url = shortCourseOfferingsOpenCRMUrl + "?offeringId=" + offeringId;
                    }
                    else
                    {
                        url = shortCourseOfferingsOpenCRMUrl;
                    }
                }
                else
                {
                    if (courseId.IsNotNull())
                    {
                        url = shortCourseOfferingsUrl + "?courseId=" + courseId;
                    }
                    else
                    {
                        url = shortCourseOfferingsUrl;
                    }
                }
            }

            ShortCourseOfferings = new List<SelectListItem>();

            var jsonResult = client.DownloadString(url).Replace("' ", "\\'");
            dynamic result = JsonConvert.DeserializeObject(jsonResult);

            if (model.DirectOfferingLink)
            {
                foreach (dynamic item in result)
                {
                    if (item.Name == "Id")
                    {
                        model.Offering = item.Value;
                    }

                    if (item.Name == "Name")
                    {
                        model.CourseName = item.Value;
                    }

                    if (item.Name == "StartDate")
                    {
                        model.CourseStartDate = ResolveCourseStartDate(item.Value.ToString());
                    }

                    if (item.Name == "AdministratorName")
                    {
                        model.CourseAdministrator = item.Value;
                    }
                }
            }
            else
            {
                if (model.InsideCRM)
                {
                    foreach (dynamic item in result)
                    {
                        if (item.Name == "Id")
                        {
                            model.Offering = item.Value;
                        }

                        if (item.Name == "Name")
                        {
                            model.CourseName = item.Value;
                        }

                        if (item.Name == "StartDate")
                        {
                            model.CourseStartDate = ResolveCourseStartDate(item.Value.ToString());
                        }

                        if (item.Name == "AdministratorName")
                        {
                            model.CourseAdministrator = item.Value;
                        }
                    }
                }
                else
                {
                    model.ShortCourseInfo = new List<ShortCourseInfo>();
                    ShortCourseOfferings.Add(new SelectListItem { Text = "< Select Course >", Value = "none" });
                    foreach (dynamic item in result)
                    {
                        ShortCourseOfferings.Add(new SelectListItem { Text = item.Name, Value = item.Id });
                        model.ShortCourseInfo.Add(new ShortCourseInfo
                        {
                            OfferingName = item.Name,
                            CourseId = item.Id,
                            CourseAdministrator = item.AdministratorName,
                            OfferingStartDate = ResolveCourseStartDate(item.StartDate.ToString())
                        });

                        model.CourseStartDate = ResolveCourseStartDate(item.StartDate.ToString());
                        model.CourseAdministrator = item.AdministratorName;
                    }
                }


            }

            return ShortCourseOfferings;
        }

        public void LoadTitles(WebClient client, ApplicationFormInfo applicationFormInfo)
        {
            var exist = Cache.Contains("Titles");

            if (exist)
            {
                applicationFormInfo.Titles = (List<SelectListItem>)Cache.GetCachedItem("Titles");
                return;
            }
            
            applicationFormInfo.Titles = new List<SelectListItem>();
            applicationFormInfo.Titles.Add(new SelectListItem { Text = "", Value = "" });
            dynamic result = JsonConvert.DeserializeObject(client.DownloadString(applicationFormInfo.titlesUrl));

            foreach (dynamic item in result)
            {
                applicationFormInfo.Titles.Add(new SelectListItem { Text = item.Name, Value = item.Value });
            }

            Cache.Add("Titles", applicationFormInfo.Titles);
        }

        public void LoadLanguages(WebClient client, ApplicationFormInfo applicationFormInfo)
        {
            var exist = Cache.Contains("Languages");

            if (exist)
            {
                applicationFormInfo.Languages = (List<SelectListItem>)Cache.GetCachedItem("Languages");
                return;
            }

            applicationFormInfo.Languages = new List<SelectListItem>();
            applicationFormInfo.Languages.Add(new SelectListItem { Text = "", Value = "" });
            dynamic result = JsonConvert.DeserializeObject(client.DownloadString(applicationFormInfo.languagesUrl));

            foreach (dynamic item in result)
            {
                applicationFormInfo.Languages.Add(new SelectListItem { Text = item.Name, Value = item.Value });
            }

            Cache.Add("Languages", applicationFormInfo.Languages);
        }

        public void LoadGenders(WebClient client, ApplicationFormInfo applicationFormInfo)
        {
            var exist = Cache.Contains("Genders");

            if (exist)
            {
                applicationFormInfo.Genders = (List<SelectListItem>)Cache.GetCachedItem("Genders");
                return;
            }

            applicationFormInfo.Genders = new List<SelectListItem>();
            applicationFormInfo.Genders.Add(new SelectListItem { Text = "", Value = "" });
            dynamic result = JsonConvert.DeserializeObject(client.DownloadString(applicationFormInfo.gendersUrl));

            foreach (dynamic item in result)
            {
                applicationFormInfo.Genders.Add(new SelectListItem { Text = item.Name, Value = item.Value });
            }

            Cache.Add("Genders", applicationFormInfo.Genders);
        }

        public void LoadNationalities(WebClient client, ApplicationFormInfo applicationFormInfo)
        {
            var exist = Cache.Contains("Nationalities");

            if (exist)
            {
                applicationFormInfo.Nationalities = (List<SelectListItem>)Cache.GetCachedItem("Nationalities");
                return;
            }

            applicationFormInfo.Nationalities = new List<SelectListItem>();
            applicationFormInfo.Nationalities.Add(new SelectListItem { Text = "", Value = "" });
            dynamic result = JsonConvert.DeserializeObject(client.DownloadString(applicationFormInfo.nationalitiesUrl));

            foreach (dynamic item in result)
            {
                applicationFormInfo.Nationalities.Add(new SelectListItem { Text = item.Name, Value = item.Value });
            }

            Cache.Add("Nationalities", applicationFormInfo.Nationalities);
        }

        public void LoadAddressCountries(WebClient client, ApplicationFormInfo applicationFormInfo)
        {
            var exist = Cache.Contains("AddressCountries");

            if (exist)
            {
                applicationFormInfo.AddressCountries = (List<SelectListItem>)Cache.GetCachedItem("AddressCountries");
                return;
            }

            applicationFormInfo.AddressCountries = new List<SelectListItem>();
            applicationFormInfo.AddressCountries.Add(new SelectListItem { Text = "", Value = "" });
            dynamic result = JsonConvert.DeserializeObject(client.DownloadString(applicationFormInfo.addressCountriesUrl));
            
            foreach (dynamic item in result)
            {
                applicationFormInfo.AddressCountries.Add(new SelectListItem { Text = item.Name, Value = item.Value });
            }

            Cache.Add("AddressCountries", applicationFormInfo.AddressCountries);
        }

        public void LoadPaymentResponsibility(WebClient client, ApplicationFormInfo applicationFormInfo)
        {
            var exist = Cache.Contains("PaymentResponsibility");

            if (exist)
            {
                applicationFormInfo.PaymentResponsibility = (List<SelectListItem>)Cache.GetCachedItem("PaymentResponsibility");
                return;
            }

            applicationFormInfo.PaymentResponsibility = new List<SelectListItem>();
            applicationFormInfo.PaymentResponsibility.Add(new SelectListItem { Text = "", Value = "" });
            dynamic result = JsonConvert.DeserializeObject(client.DownloadString(applicationFormInfo.paymentResponsibilitiesUrl));

            foreach (dynamic item in result)
            {
                applicationFormInfo.PaymentResponsibility.Add(new SelectListItem { Text = item.Name, Value = item.Value });
            }

            Cache.Add("PaymentResponsibility", applicationFormInfo.PaymentResponsibility);
        }

        public void LoadEthnicities(WebClient client, ApplicationFormInfo applicationFormInfo)
        {
            var exist = Cache.Contains("Ethnicities");

            if (exist)
            {
                applicationFormInfo.Ethnicities = (List<SelectListItem>)Cache.GetCachedItem("Ethnicities");
                return;
            }

            applicationFormInfo.Ethnicities = new List<SelectListItem>();
            applicationFormInfo.Ethnicities.Add(new SelectListItem { Text = "", Value = "" });
            dynamic result = JsonConvert.DeserializeObject(client.DownloadString(applicationFormInfo.ethnicitiesUrl));

            foreach (dynamic item in result)
            {
                applicationFormInfo.Ethnicities.Add(new SelectListItem { Text = item.Name, Value = item.Value });
            }

            Cache.Add("Ethnicities", applicationFormInfo.Ethnicities);
        }

        public void LoadEthnicitiesNonSA(WebClient client, ApplicationFormInfo applicationFormInfo)
        {
            var exist = Cache.Contains("EthnicitiesNonSA");

            if (exist)
            {
                applicationFormInfo.EthnicitiesNonSA = (List<SelectListItem>)Cache.GetCachedItem("EthnicitiesNonSA");
                return;
            }

            applicationFormInfo.EthnicitiesNonSA = new List<SelectListItem>();
            var africanGuid = applicationFormInfo.Ethnicities.FirstOrDefault(e => e.Text == "African").Value;
            var asianGuid = applicationFormInfo.Ethnicities.FirstOrDefault(e => e.Text == "Indian").Value;
            var caucasianGuid = applicationFormInfo.Ethnicities.FirstOrDefault(e => e.Text == "White").Value;
            var mixedGuid = applicationFormInfo.Ethnicities.FirstOrDefault(e => e.Text == "Coloured").Value;
            var otherGuid = applicationFormInfo.Ethnicities.FirstOrDefault(e => e.Text == "African").Value;

            applicationFormInfo.EthnicitiesNonSA.Add(new SelectListItem { Text = "", Value = "", Selected = true });
            applicationFormInfo.EthnicitiesNonSA.Add(new SelectListItem { Text = "African", Value = africanGuid });
            applicationFormInfo.EthnicitiesNonSA.Add(new SelectListItem { Text = "Asian", Value = asianGuid });
            applicationFormInfo.EthnicitiesNonSA.Add(new SelectListItem { Text = "Caucasian", Value = caucasianGuid });
            applicationFormInfo.EthnicitiesNonSA.Add(new SelectListItem { Text = "Mixed", Value = mixedGuid });
            applicationFormInfo.EthnicitiesNonSA.Add(new SelectListItem { Text = "Other", Value = otherGuid });

            Cache.Add("EthnicitiesNonSA", applicationFormInfo.EthnicitiesNonSA);
        }

        public void LoadDietaryRequirements(WebClient client, ApplicationFormInfo applicationFormInfo)
        {
            var exist = Cache.Contains("DietaryRequirements");

            if (exist)
            {
                applicationFormInfo.DietaryRequirements = (List<SelectListItem>)Cache.GetCachedItem("DietaryRequirements");
                return;
            }

            applicationFormInfo.DietaryRequirements = new List<SelectListItem>();
            applicationFormInfo.DietaryRequirements.Add(new SelectListItem { Text = "", Value = "" });
            dynamic result = JsonConvert.DeserializeObject(client.DownloadString(applicationFormInfo.dietaryRequirementsUrl));

            foreach (dynamic item in result)
            {
                if (item.Name == "None")
                {
                    applicationFormInfo.DietaryRequirements.Add(new SelectListItem { Text = item.Name, Value = item.Value, Selected = true });
                }
                else
                {
                    applicationFormInfo.DietaryRequirements.Add(new SelectListItem { Text = item.Name, Value = item.Value });
                }
            }

            Cache.Add("DietaryRequirements", applicationFormInfo.DietaryRequirements);
        }

        public void LoadIndustries(WebClient client, ApplicationFormInfo applicationFormInfo)
        {
            var exist = Cache.Contains("Industries");

            if (exist)
            {
                applicationFormInfo.Industries = (List<SelectListItem>)Cache.GetCachedItem("Industries");
                return;
            }

            applicationFormInfo.Industries = new List<SelectListItem>();
            applicationFormInfo.Industries.Add(new SelectListItem { Text = "", Value = "" });
            dynamic result = JsonConvert.DeserializeObject(client.DownloadString(applicationFormInfo.industriesUrl));

            foreach (dynamic item in result)
            {
                applicationFormInfo.Industries.Add(new SelectListItem { Text = item.Name, Value = item.Value });
            }

            Cache.Add("Industries", applicationFormInfo.Industries);
        }

        public void LoadWorkAreas(WebClient client, ApplicationFormInfo applicationFormInfo)
        {
            var exist = Cache.Contains("WorkAreas");

            if (exist)
            {
                applicationFormInfo.WorkAreas = (List<SelectListItem>)Cache.GetCachedItem("WorkAreas");
                return;
            }
            applicationFormInfo.WorkAreas = new List<SelectListItem>();
            applicationFormInfo.WorkAreas.Add(new SelectListItem { Text = "", Value = "" });
            dynamic result = JsonConvert.DeserializeObject(client.DownloadString(applicationFormInfo.workAreasUrl));

            foreach (dynamic item in result)
            {
                applicationFormInfo.WorkAreas.Add(new SelectListItem { Text = item.Name, Value = item.Value });
            }

            Cache.Add("WorkAreas", applicationFormInfo.WorkAreas);
        }

        public void LoadWorkOccupationalCategories(WebClient client, ApplicationFormInfo applicationFormInfo)
        {
            var exist = Cache.Contains("OccupationalCategories");

            if (exist)
            {
                applicationFormInfo.OccupationalCategories = (List<SelectListItem>)Cache.GetCachedItem("OccupationalCategories");
                return;
            }
            applicationFormInfo.OccupationalCategories = new List<SelectListItem>();
            applicationFormInfo.OccupationalCategories.Add(new SelectListItem { Text = "", Value = "" });
            dynamic result = JsonConvert.DeserializeObject(client.DownloadString(applicationFormInfo.occupationalCategoriesUrl));

            foreach (dynamic item in result)
            {
                applicationFormInfo.OccupationalCategories.Add(new SelectListItem { Text = item.Name, Value = item.Value });
            }

            Cache.Add("OccupationalCategories", applicationFormInfo.OccupationalCategories);
        }

        public void LoadIdDocumentTypes(WebClient client, ApplicationFormInfo applicationFormInfo)
        {
            var exist = Cache.Contains("IdDocumentTypes");

            if (exist)
            {
                applicationFormInfo.IdDocumentTypes = (List<SelectListItem>)Cache.GetCachedItem("IdDocumentTypes");
                return;
            }

            applicationFormInfo.IdDocumentTypes = new List<SelectListItem>();
            applicationFormInfo.IdDocumentTypes.Add(new SelectListItem { Text = "", Value = "" });
            dynamic result = JsonConvert.DeserializeObject(client.DownloadString(applicationFormInfo.foreignIdentificationTypesUrl));

            foreach (dynamic item in result)
            {
                applicationFormInfo.IdDocumentTypes.Add(new SelectListItem { Text = item.Name, Value = item.Value });
            }

            Cache.Add("IdDocumentTypes", applicationFormInfo.IdDocumentTypes);
        }

        public void LoadMarketingReasons(WebClient client, ApplicationFormInfo applicationFormInfo)
        {
            var exist = Cache.Contains("MarketingReasons");

            if (exist)
            {
                applicationFormInfo.MarketingReasons = (List<SelectListItem>)Cache.GetCachedItem("MarketingReasons");
                return;
            }

            applicationFormInfo.MarketingReasons = new List<SelectListItem>();
            var list = new List<SelectListItem>();
            list.Add(new SelectListItem { Text = "", Value = "" });
            dynamic result = JsonConvert.DeserializeObject(client.DownloadString(applicationFormInfo.marketingReasonUrl));

            foreach (dynamic item in result)
            {
                list.Add(new SelectListItem { Text = item.Name, Value = item.Value });            
            }

            var sortedList = list.OrderBy(m => m.Text);

            foreach (var item in sortedList)
            {
                applicationFormInfo.MarketingReasons.Add(new SelectListItem { Text = item.Text, Value = item.Value });  
            }

            Cache.Add("MarketingReasons", applicationFormInfo.MarketingReasons);
        }

        public void LoadMarketingSources(WebClient client, ApplicationFormInfo applicationFormInfo)
        {
            var exist = Cache.Contains("MarketingSources");

            if (exist)
            {
                applicationFormInfo.MarketingSources = (List<SelectListItem>)Cache.GetCachedItem("MarketingSources");
                return;
            }

            applicationFormInfo.MarketingSources = new List<SelectListItem>();
            var list = new List<SelectListItem>();
            list.Add(new SelectListItem { Text = "", Value = "" });
            dynamic result = JsonConvert.DeserializeObject(client.DownloadString(applicationFormInfo.marketingSourcesUrl));

            foreach (dynamic item in result)
            {
                list.Add(new SelectListItem { Text = item.Name, Value = item.Value });
            }

            var sortedList = list.OrderBy(m => m.Text);

            foreach (var item in sortedList)
            {
                applicationFormInfo.MarketingSources.Add(new SelectListItem { Text = item.Text, Value = item.Value });
            }

            Cache.Add("MarketingSources", applicationFormInfo.MarketingSources);

        }

        public void LoadQualificationTypes(WebClient client, ApplicationFormInfo applicationFormInfo)
        {
            var exist = Cache.Contains("QualificationTypes");

            if (exist)
            {
                applicationFormInfo.QualificationTypes = (List<SelectListItem>)Cache.GetCachedItem("QualificationTypes");
                return;
            }

            applicationFormInfo.QualificationTypes = new List<SelectListItem>();
            applicationFormInfo.QualificationTypes.Add(new SelectListItem { Text = "", Value = "" });
            dynamic result = JsonConvert.DeserializeObject(client.DownloadString(applicationFormInfo.qualificationTypesUrl));

            foreach (dynamic item in result)
            {
                applicationFormInfo.QualificationTypes.Add(new SelectListItem { Text = item.Name, Value = item.Value });
            }

            Cache.Add("QualificationTypes", applicationFormInfo.QualificationTypes);
        }

        public void LoadQualificationFields(WebClient client, ApplicationFormInfo applicationFormInfo)
        {
            var exist = Cache.Contains("QualificationFields");

            if (exist)
            {
                applicationFormInfo.QualificationFields = (List<SelectListItem>)Cache.GetCachedItem("QualificationFields");
                return;
            }

            applicationFormInfo.QualificationFields = new List<SelectListItem>();
            applicationFormInfo.QualificationFields.Add(new SelectListItem { Text = "", Value = "" });
            dynamic result = JsonConvert.DeserializeObject(client.DownloadString(applicationFormInfo.qualificationFieldsUrl));

            foreach (dynamic item in result)
            {
                applicationFormInfo.QualificationFields.Add(new SelectListItem { Text = item.Name, Value = item.Value });
            }

            Cache.Add("QualificationFields", applicationFormInfo.QualificationFields);
        }

        public void LoadDisabilities(WebClient client, ApplicationFormInfo applicationFormInfo)
        {
            var exist = Cache.Contains("Disabilities");

            if (exist)
            {
                applicationFormInfo.Disabilities = (List<SelectListItem>)Cache.GetCachedItem("Disabilities");
                return;
            }

            applicationFormInfo.Disabilities = new List<SelectListItem>();
            applicationFormInfo.Disabilities.Add(new SelectListItem { Text = "", Value = "" });
            dynamic result = JsonConvert.DeserializeObject(client.DownloadString(applicationFormInfo.disabilitiesUrl));

            foreach (dynamic item in result)
            {
                if (item.Name == "None")
                {
                    applicationFormInfo.Disabilities.Add(new SelectListItem { Text = item.Name, Value = item.Value, Selected = true });
                }
                else
                {
                    applicationFormInfo.Disabilities.Add(new SelectListItem { Text = item.Name, Value = item.Value });
                }
            }

            Cache.Add("Disabilities", applicationFormInfo.Disabilities);
        }

        public void LoadYesNoList(ApplicationFormInfo applicationFormInfo)
        {
            var exist = Cache.Contains("YesNoList");

            if (exist)
            {
                applicationFormInfo.YesNoList = (List<SelectListItem>)Cache.GetCachedItem("YesNoList");
                return;
            }

            applicationFormInfo.YesNoList = new List<SelectListItem>();
            applicationFormInfo.YesNoList.Add(new SelectListItem { Text = "Yes", Value = "true" });
            applicationFormInfo.YesNoList.Add(new SelectListItem { Text = "No", Value = "false", Selected = true });

            Cache.Add("YesNoList", applicationFormInfo.YesNoList);
        }

        public void LoadYearsList(ApplicationFormInfo applicationFormInfo)
        {
            var exist = Cache.Contains("YearsList");

            if (exist)
            {
                applicationFormInfo.YearsList = (List<SelectListItem>)Cache.GetCachedItem("YearsList");
                return;
            }

            applicationFormInfo.YearsList = new List<SelectListItem>();
            var start = DateTime.Now.AddYears(-80).Year;
            var end = DateTime.Now.Year;
            applicationFormInfo.YearsList.Add(new SelectListItem { Text = "", Value = "" });

            for (var year = start; year <= end; year++)
            {
                applicationFormInfo.YearsList.Add(new SelectListItem { Text = year.AsString(), Value = year.AsString() });
            }

            Cache.Add("YearsList", applicationFormInfo.YearsList);
        }
    }

        #endregion
}

