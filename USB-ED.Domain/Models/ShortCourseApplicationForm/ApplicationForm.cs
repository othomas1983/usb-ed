﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using USB_ED.Models.ShortCourseApplicationForm;
using USB_ED.Validation;
using SettingsMan = USB_ED.Properties.Settings;

namespace USB_ED.Models
{
    public enum ValidateProgress
    {
        PersonalInformation,
        ContactInformation,
        Qualifications,
        WorkExperience,
        Marketing,
        PaymentInfo,
        ApplicationForm
    }

    public class ShortCourseInfo
    {
        public string OfferingName { get; set; }
        public string OfferingStartDate { get; set; }
        public string OfferingEndDate { get; set; }
        public string CourseAdministrator { get; set; }
        public string CourseId { get; set; }
    }

    public class ApplicationForm
    {
        #region Properties

         [JsonIgnore]
        public List<ShortCourseInfo> ShortCourseInfo { get; set; }

        #region Personal Information

        private string contactNumber;

        private static string applicationKey;

        public string ApplicationKey
        {
            get { return applicationKey; }
            set { applicationKey = value; }
        }

        [JsonIgnore]
        public string DateOfBirthDay { get; set; }
        [JsonIgnore]
        public string DateOfBirthMonth { get; set; }
        [JsonIgnore]
        public string DateOfBirthYear { get; set; }
        [JsonIgnore]
        public bool HasListItems { get; set; }

        public string WebTokenId { get; set; }

        [JsonIgnore]
        public bool InsideCRM { get; set; }
        [JsonIgnore]
        public bool DirectOfferingLink { get; set; }
        [JsonIgnore]
        public bool DisplayNameApproved { get; set; }
        [JsonIgnore]
        public string CopyOfID { get; set; }
        [JsonIgnore]
        public string CopyOfCertificate { get; set; }

        [JsonIgnore]
        public string ValidationSection { get; set; }
        [JsonIgnore]
        public bool Section1Validated { get; set; }
        [JsonIgnore]
        public bool Section2Validated { get; set; }
        [JsonIgnore]
        public bool Section3Validated { get; set; }
        [JsonIgnore]
        public bool Section4Validated { get; set; }
        [JsonIgnore]
        public bool Section5Validated { get; set; }
        [JsonIgnore]
        public string SelectedTab { get; set; }
        
        private string courseStartDate;
        [JsonIgnore]
        public string CourseStartDate
        {
            get { return courseStartDate; }
            set { courseStartDate = ResolveCourseStartDate(value); }
        }

        [JsonIgnore]
        public string CourseAdministrator { get; set; }
        [JsonIgnore]
        public string CourseId { get; set; }
        [JsonIgnore]
        public string CourseName { get; set; }
        [JsonIgnore]
        public string ShortCourseId { get; set; }
        [JsonIgnore]
        public string ShortCourseOfferingId { get; set; }

        public string IdentificationDocumentUploadKey { get; set; }
        public string QualificationDocumentUploadKey { get; set; }

        [Required]
        public Guid Offering { get; set; }

        [Required]
        public string Surname { get; set; }

        [Required]
        [Display(Name = "First Names")]
        public string FirstNames { get; set; }

        [Required]
        public string Initials { get; set; }

        [Display(Name = "Given name")]
        public string GivenName { get; set; }

        public string NameOnCertificate { get; set; }

        [Required]
        public Guid Title { get; set; }

        [Required]
        public Guid Gender { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required]
        [Display(Name = "Date of Birth")]        
        public DateTime DateOfBirth { get; set; }

        [EthnicityValidation]
        public Guid? Ethnicity { get; set; }

        [Required]
        public Guid Nationality { get; set; }        

        [IdNumberValidation]
        [Display(Name = "Identity Number")]
        public string IdNumber { get; set; }

        [Display(Name = "Type of identification:")]
        public Guid? ForeignIdType { get; set; }

        public string PassportNumber { get; set; }

        [Display(Name = "Expiry date:")]
        public DateTime? PassportExpiryDate { get; set; }

        [Display(Name = "Type of identification document")]
        public string ForeignIdNumber { get; set; }

        public DateTime? ForeignIdExpiryDate { get; set; }

        [Required]
        [Display(Name = "Home Language")]
        public Guid Language { get; set; }

        [DietaryValidation]
        [Display(Name = "Dietary requirements")]
        public Guid? DietaryRequirements { get; set; }

        [DietaryValidation]
        public string DietaryRequirementsOther { get; set; }

        [Required]
        public Guid? Disability { get; set; }

        [Display(Name = "Uses Wheelchair")]
        public bool UsesWheelchair { get; set; }

        [Display(Name = "E-mail address")]
        [Required]
        [ValidateEmailAddress]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Contact Number")]
        public string ContactNumber
        {
            get { return contactNumber; }
            set { contactNumber = value.IsNullOrEmpty() ? value : value.Replace(" ", ""); }
        }

        public string CellphoneNumber { get; set; }
        
        public string WorkNumber { get; set; }

        [Display(Name = "Fax")]
        public string FaxNumber { get; set; }
               
        #endregion

        #region PostalAddress

        [PostalAddressValidation]
        public string PostalStreet1 { get; set; }

        public string PostalStreet2 { get; set; }

        [NonSAPostalAddressValidation]
        [Display(Name = "Suburb")]
        public string PostalSuburb { get; set; }

        [NonSAPostalAddressValidation]
        [Display(Name = "City / Town")]
        public string PostalCity { get; set; }

        //[NonSAPostalAddressValidation]
        [Display(Name = "Postal Code")]
        public string PostalPostalCode { get; set; }

        [SAPostalAddressValidation]
        [Display(Name = "Postal Code or Suburb or City/Town")]
        public Guid? PostalAfrigis { get; set; }
        
        [Required]
        [Display(Name = "Country")]
        public Guid PostalCountry { get; set; }

        #endregion

        #region Postal Courier

        public string CourierStreet { get; set; }

        public string CourierSuburb { get; set; }

        public string CourierCity { get; set; }

        public string CourierPostalCode { get; set; }

        public Guid CourierAfrigis { get; set; }

        public Guid CourierCountry { get; set; }

        #endregion

        #region Academic Qualifications

        public string Qualification { get; set; }

        public string QualificationTypeOther { get; set; }

        [Display(Name = "Qualification Type")]
        public Guid QualificationType { get; set; }

        [Display(Name = "Qualification Field")]
        public Guid? QualificationField { get; set; }

        [Display(Name = "Qualification Institution")]
        public string QualificationInstitution { get; set; }

        public string YearAchieved { get; set; }

        #endregion

        #region Employement

        public bool Employed { get; set; }

        public Guid? Industry { get; set; }

        public Guid? WorkArea { get; set; }

        public string JobTitle { get; set; }

        public string Employer { get; set; }

        public string EmployerContactNumber { get; set; }

        public Guid OccupationalCategory { get; set; }

        #endregion

        #region PaymentInformation

        [PaymentResponsibilityValidation]
        [Display(Name = "Payment Responsibility")]
        public Guid PaymentResponsibility { get; set; }

        [PaymentDetailsValidation]
        [Display(Name = "Payment Contact Person")]
        public string PaymentContactPerson { get; set; }

        [PaymentDetailsValidation]
        [Display(Name = "Payment Contact Number")]
        public string PaymentContactNumber { get; set; }

        [PaymentDetailsValidation]
        [ValidatePaymentDetailsEmailAddress]
        [Display(Name = "Payment Contact Email")]
        public string PaymentContactEmail { get; set; }
        
        [Display(Name = "Payment Debtor Code")]
        public string PaymentDebtorCode { get; set; }
         
        #endregion
       
        #region Marketing

        public int MarketingSource { get; set; }

        public string MarketingSourceOther { get; set; }

        public int MarketingReason { get; set; }

        public string MarketingReasonOther { get; set; }

        #endregion

        #region SocialMedia

        public bool UsesTwitter { get; set; }

        public bool UsesFacebook { get; set; }

        public bool UsesLinkedIn { get; set; }

        public string TwitterUsername { get; set; }

        public bool SubscribeToNewsletter { get; set; }

        #endregion

        //public string IdentificationDocumentUploadKey { get; set; }

        //public string QualificationDocumentUploadKey { get; set; }

        public string UsNumber { get; set; }

        #endregion

        #region public Methods

        public bool AllSectionsValidated()
        {
            return Section1Validated == true
                && Section2Validated == true
                && Section3Validated == true
                && Section4Validated == true;
        }

        public static ApplicationSubmissionResult UploadApplicationForm(string applicationForm)
        {
            var applicationSubmissionResult = new ApplicationSubmissionResult();
            try
            {
                string submitUrl = SettingsMan.Default.SubmitAppFormUrl;

                using (WebClient webClient = new WebClient())
                {
                    webClient.Encoding = System.Text.Encoding.UTF8;
                    webClient.Headers.Add("Content-Type", "application/json");
                    var result = webClient.UploadString(submitUrl, applicationForm);

                    var resultStream = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(result));
                    var deserializer = new DataContractJsonSerializer(typeof(ApplicationSubmissionResult));
                    applicationSubmissionResult = (ApplicationSubmissionResult)deserializer.ReadObject(resultStream);

                    return applicationSubmissionResult;
                }

                //catch (WebException wex)
                //{
                //    string pageContent = new StreamReader(wex.Response.GetResponseStream()).ReadToEnd().ToString();
                //    return pageContent;
                //}

                //var service = new USBEdServiceReference.ServiceClient();

                //var result = service.UploadShortCourseApplicationForm(applicationForm);

                //var resultStream = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(result));

                //var deserializer = new DataContractJsonSerializer(typeof(ApplicationSubmissionResult));

                //applicationSubmissionResult = (ApplicationSubmissionResult)deserializer.ReadObject(resultStream);

                //return applicationSubmissionResult;
            }
            catch (Exception ex)
            {
                applicationSubmissionResult.Successful = false;
                applicationSubmissionResult.Message = ex.Message;
                return applicationSubmissionResult;
            }
        }

        public static HttpResponseMessage UploadFile(HttpRequestMessage message)
        { 
            message.RequestUri = new Uri(SettingsMan.Default.upLoadFileURL);

            var client = new HttpClient();
            return client.SendAsync(message).Result;
        }

        public ApplicationForm LoadStudentDetails(ApplicationForm model, string studentNumber, ApplicationFormInfo applicationFormInfo)
        {
            string loadStudentUrl = SettingsMan.Default.loadStudentUrl;
            var webTokenId = model.WebTokenId;

            try
            {
                using (WebClient webClient = new WebClient())
                {
                    webClient.Headers.Add("Content-Type", "application/json");
                    webClient.QueryString.Add("usNumber", studentNumber);
                    webClient.QueryString.Add("webTokenId", webTokenId);

                    return JsonConvert.DeserializeObject<ApplicationForm>(webClient.DownloadString(loadStudentUrl));
                }
            }
            catch (WebException wex)
            {
                return null;
            }
        }

        #endregion

        #region Private Methods

        private string ResolveCourseStartDate(string value)
        {
            DateTime dateValue;

            if (DateTime.TryParse(value, out dateValue))
            {
                return dateValue.ToString("dd MMM yyyy");
            }
            else
            {
                return null;
            }
        }

        #endregion
    }
}

