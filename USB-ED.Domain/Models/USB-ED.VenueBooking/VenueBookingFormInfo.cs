﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace USB_ED.Models
{
    public class VenueBookingFormInfo
    {
        public static List<SelectListItem> VenueTimeInOutList { get; set; }

        public static List<SelectListItem> CateringRequirementsTime { get; set; }

        private static VenueBookingFormInfo Info;

        public static bool HasListItems { get; set; }

        public static VenueBookingFormInfo LoadInfoMembers()
        {
            if (Info.IsNull())
            {
                HasListItems = true;
                Info = new VenueBookingFormInfo();
                WebClient client = new WebClient();

                try
                {
                    LoadVenueTimeInOutList();
                    LoadCateringRequirementsTime();
                }
                catch
                {
                    HasListItems = false;
                }                
            }
            return Info;
        }

        

        //private static void LoadVenues()
        //{
        //    Venues = new List<SelectListItem>();

        //    var service = new USBEdServiceReference.ServiceClient();

        //    var venues = service.LoadVenues();

        //    foreach (var item in venues)
        //    {
        //        Venues.Add(new SelectListItem { Text = item.Value, Value = item.Key.AsString() });
        //    }
        //}

        private static void LoadVenueTimeInOutList()
        {
            VenueTimeInOutList = new List<SelectListItem>();            

            var list = ",07:30,08:00,08:30,09:00,09:30,10:00,10:30,11:00,11:30,12:00,12:30,13:00,13:30,14:00,14:30,15:00,15:30,16:00,16:30,17:00,17:30,18:00,18:30,19:00,19:30,20:00".Split(',');
            
            foreach (var item in list)
            {
                VenueTimeInOutList.Add(new SelectListItem { Text = item, Value = item });                
            }
        }

        private static void LoadCateringRequirementsTime()
        {
            CateringRequirementsTime = new List<SelectListItem>();            

            var list = ",07:30,07:45,08:00,08:15,08:30,08:45,09:00,09:15,09:30,09:45,10:00,10:15,10:30,10:45,11:00,11:15,11:30,11:45,12:00,12:15,12:30,12:45,13:00,13:15,13:30,13:45,14:00,14:15,14:30,14:45,15:00,15:15,15:30,15:45,16:00,16:15,16:30,16:45,17:00,17:15,17:30,17:45,18:00,18:15,18:30,18:45,19:00,19:15,19:30,19:45,20:00".Split(',');

            foreach (var item in list)
            {
                CateringRequirementsTime.Add(new SelectListItem { Text = item, Value = item });                
            }
        }
    }
}