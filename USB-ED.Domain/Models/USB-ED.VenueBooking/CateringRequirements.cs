﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using USB_ED.Validation;

namespace USB_ED.Models.VenueBooking
{
    [System.AttributeUsage(System.AttributeTargets.Property)]
    public class RequirementAttribute : System.Attribute
    {
        public string DisplayName;
        public bool HasTimes;        

        public RequirementAttribute(string name, bool hasTimes)
        {
            DisplayName = name;
            HasTimes = hasTimes;            
        }
    }
    
    public class CateringRequirements
    {
        public bool CateringPackageA { get; set; }
        public int? CateringPackageACateringInfoID { get; set; }
        public string CateringPackageATeaCoffeeArrivalTime { get; set; }
        public string CateringPackageATeaCoffeeMidmorningTime { get; set; }
        public string CateringPackageATeaCoffeeAfternoonTime { get; set; }
        public string CateringPackageALunchTime { get; set; }

        public bool CateringPackageB { get; set; }
        public int? CateringPackageBCateringInfoID { get; set; }
        public string CateringPackageBTeaCoffeeArrivalTime { get; set; }
        public string CateringPackageBTeaCoffeeMidmorningTime { get; set; }
        public string CateringPackageBTeaCoffeeAfternoonTime { get; set; }
        public string CateringPackageBLunchTime { get; set; }

        public bool CateringPackageC { get; set; }
        public int? CateringPackageCCateringInfoID { get; set; }
        public string CateringPackageCTeaCoffeeArrivalTime { get; set; }
        public string CateringPackageCTeaCoffeeMidmorningTime { get; set; }
        public string CateringPackageCTeaCoffeeAfternoonTime { get; set; }
        public string CateringPackageCLunchTime { get; set; }

        [RequirementAttribute("TeaCoffee",true)]
        [Display(Name = "Tea / Coffee")]
        public bool TeaCoffee { get; set; }
        public int? TeaCoffeeCateringInfoID { get; set; }
        [CateringRequirementsCountValidation]
        public int? TeaCoffeeCount { get; set; }
        public string TeaCoffeeMorningTime { get; set; }
        public string TeaCoffeeMidMorningTime { get; set; }
        public string TeaCoffeeLunchTime { get; set; }
        public string TeaCoffeeAfternoonTime { get; set; }
        public string TeaCoffeeEveningTime { get; set; }

        [RequirementAttribute("TeaSnack", true)]
        [Display(Name = "Tea / Snack")]
        public bool TeaSnack { get; set; }
        public int? TeaSnackCateringInfoID { get; set; }
         [CateringRequirementsCountValidation]
        public int? TeaSnackCount { get; set; }
        public string TeaSnackMorningTime { get; set; }
        public string TeaSnackMidMorningTime { get; set; }
        public string TeaSnackLunchTime { get; set; }
        public string TeaSnackAfternoonTime { get; set; }
        public string TeaSnackEveningTime { get; set; }

        [RequirementAttribute("Tartlets", true)]
        [Display(Name = "Tartlets")]
        public bool Tartlets { get; set; }
        public int? TartletsCateringInfoID { get; set; }
         [CateringRequirementsCountValidation]
        public int? TartletsCount { get; set; }
        public string TartletsMorningTime { get; set; }
        public string TartletsMidMorningTime { get; set; }
        public string TartletsLunchTime { get; set; }
        public string TartletsAfternoonTime { get; set; }
        public string TartletsEveningTime { get; set; }

        [RequirementAttribute("Biscuits", true)]
        [Display(Name = "Biscuits")]
        public bool Biscuits { get; set; }
        public int? BiscuitsCateringInfoID { get; set; }
         [CateringRequirementsCountValidation]
        public int? BiscuitsCount { get; set; }
        public string BiscuitsMorningTime { get; set; }
        public string BiscuitsMidMorningTime { get; set; }
        public string BiscuitsLunchTime { get; set; }
        public string BiscuitsAfternoonTime { get; set; }
        public string BiscuitsEveningTime { get; set; }

        [RequirementAttribute("MealOfTheDay", true)]
        [Display(Name = "Meal of the day")]
        public bool MealOfTheDay { get; set; }
        public int? MealOfTheDayCateringInfoID { get; set; }
         [CateringRequirementsCountValidation]
        public int? MealOfTheDayCount { get; set; }
        public string MealOfTheDayMorningTime { get; set; }
        public string MealOfTheDayMidMorningTime { get; set; }
        public string MealOfTheDayLunchTime { get; set; }
        public string MealOfTheDayAfternoonTime { get; set; }
        public string MealOfTheDayEveningTime { get; set; }

        [RequirementAttribute("BuffetLunch", true)]
        [Display(Name = "Buffet Lunch")]
        public bool BuffetLunch { get; set; }
        public int? BuffetLunchCateringInfoID { get; set; }
         [CateringRequirementsCountValidation]
        public int? BuffetLunchCount { get; set; }
        public string BuffetLunchMorningTime { get; set; }
        public string BuffetLunchMidMorningTime { get; set; }
        public string BuffetLunchLunchTime { get; set; }
        public string BuffetLunchAfternoonTime { get; set; }
        public string BuffetLunchEveningTime { get; set; }

        [RequirementAttribute("UpmarketLunch", true)]
        [Display(Name = "Upmarket Lunch")]
        public bool UpmarketLunch { get; set; }
        public int? UpmarketLunchCateringInfoID { get; set; }
         [CateringRequirementsCountValidation]
        public int? UpmarketLunchCount { get; set; }
        public string UpmarketLunchMorningTime { get; set; }
        public string UpmarketLunchMidMorningTime { get; set; }
        public string UpmarketLunchLunchTime { get; set; }
        public string UpmarketLunchAfternoonTime { get; set; }
        public string UpmarketLunchEveningTime { get; set; }

        [RequirementAttribute("Dessert", true)]
        [Display(Name = "Dessert")]
        public bool Dessert { get; set; }
        public int? DessertCateringInfoID { get; set; }
         [CateringRequirementsCountValidation]
        public int? DessertCount { get; set; }
        public string DessertMorningTime { get; set; }
        public string DessertMidMorningTime { get; set; }
        public string DessertLunchTime { get; set; }
        public string DessertAfternoonTime { get; set; }
        public string DessertEveningTime { get; set; }

        [RequirementAttribute("Dinner", true)]
        [Display(Name = "Dinner")]
        public bool Dinner { get; set; }
        public int? DinnerCateringInfoID { get; set; }
         [CateringRequirementsCountValidation]
        public int? DinnerCount { get; set; }
        public string DinnerMorningTime { get; set; }
        public string DinnerMidMorningTime { get; set; }
        public string DinnerLunchTime { get; set; }
        public string DinnerAfternoonTime { get; set; }
        public string DinnerEveningTime { get; set; }

        [RequirementAttribute("Cans", true)]
        [Display(Name = "Cans")]
        public bool Cans { get; set; }
        public int? CansCateringInfoID { get; set; }
         [CateringRequirementsCountValidation]
        public int? CansCount { get; set; }
        public string CansMorningTime { get; set; }
        public string CansMidMorningTime { get; set; }
        public string CansLunchTime { get; set; }
        public string CansAfternoonTime { get; set; }
        public string CansEveningTime { get; set; }

        [RequirementAttribute("MineralWater", true)]
        [Display(Name = "Mineral Water (500ml)")]
        public bool MineralWater { get; set; }
        public int? MineralWaterCateringInfoID { get; set; }
         [CateringRequirementsCountValidation]
        public int? MineralWaterCount { get; set; }
        public string MineralWaterMorningTime { get; set; }
        public string MineralWaterMidMorningTime { get; set; }
        public string MineralWaterLunchTime { get; set; }
        public string MineralWaterAfternoonTime { get; set; }
        public string MineralWaterEveningTime { get; set; }

        [RequirementAttribute("JuicePerGlass", true)]
        [Display(Name = "Juice per glass")]
        public bool JuicePerGlass { get; set; }
        public int? JuicePerGlassCateringInfoID { get; set; }
         [CateringRequirementsCountValidation]
        public int? JuicePerGlassCount { get; set; }
        public string JuicePerGlassMorningTime { get; set; }
        public string JuicePerGlassMidMorningTime { get; set; }
        public string JuicePerGlassLunchTime { get; set; }
        public string JuicePerGlassAfternoonTime { get; set; }
        public string JuicePerGlassEveningTime { get; set; }

        [RequirementAttribute("JugsOfJuice", true)]
        [Display(Name = "Jugs Of Juice")]
        public bool JugsOfJuice { get; set; }
        public int? JugsOfJuiceCateringInfoID { get; set; }
         [CateringRequirementsCountValidation]
        public int? JugsOfJuiceCount { get; set; }
        public string JugsOfJuiceMorningTime { get; set; }
        public string JugsOfJuiceMidMorningTime { get; set; }
        public string JugsOfJuiceLunchTime { get; set; }
        public string JugsOfJuiceAfternoonTime { get; set; }
        public string JugsOfJuiceEveningTime { get; set; }

        [RequirementAttribute("Braai2Meats", true)]
        [Display(Name = "Braai - 2 meats")]
        public bool Braai2Meats { get; set; }
        public int? Braai2MeatsCateringInfoID { get; set; }
         [CateringRequirementsCountValidation]
        public int? Braai2MeatsCount { get; set; }
        public string Braai2MeatsMorningTime { get; set; }
        public string Braai2MeatsMidMorningTime { get; set; }
        public string Braai2MeatsLunchTime { get; set; }
        public string Braai2MeatsAfternoonTime { get; set; }
        public string Braai2MeatsEveningTime { get; set; }

        [RequirementAttribute("Braai3Meats", true)]
        [Display(Name = "Braai - 3 meats")]
        public bool Braai3Meats { get; set; }
        public int? Braai3MeatsCateringInfoID { get; set; }
         [CateringRequirementsCountValidation]
        public int? Braai3MeatsCount { get; set; }
        public string Braai3MeatsMorningTime { get; set; }
        public string Braai3MeatsMidMorningTime { get; set; }
        public string Braai3MeatsLunchTime { get; set; }
        public string Braai3MeatsAfternoonTime { get; set; }
        public string Braai3MeatsEveningTime { get; set; }

        [RequirementAttribute("SpitBraai", true)]
        [Display(Name = "Spit Braai")]
        public bool SpitBraai { get; set; }
        public int? SpitBraaiCateringInfoID { get; set; }
         [CateringRequirementsCountValidation]
        public int? SpitBraaiCount { get; set; }
        public string SpitBraaiMorningTime { get; set; }
        public string SpitBraaiMidMorningTime { get; set; }
        public string SpitBraaiLunchTime { get; set; }
        public string SpitBraaiAfternoonTime { get; set; }
        public string SpitBraaiEveningTime { get; set; }

        [RequirementAttribute("Potjie", true)]
        [Display(Name = "Potjie x 2")]
        public bool Potjie { get; set; }
        public int? PotjieCateringInfoID { get; set; }
         [CateringRequirementsCountValidation]
        public int? PotjieCount { get; set; }
        public string PotjieMorningTime { get; set; }
        public string PotjieMidMorningTime { get; set; }
        public string PotjieLunchTime { get; set; }
        public string PotjieAfternoonTime { get; set; }
        public string PotjieEveningTime { get; set; }

        [RequirementAttribute("CocktailHalf",true)]
        [Display(Name = "Cocktail Half")]
        public bool CockTailHalf { get; set; }
        public int? CocktailHalfCateringInfoID { get; set; }
         [CateringRequirementsCountValidation]
        public int? CockTailHalfCount { get; set; }
        public string CockTailHalfMorningTime { get; set; }
        public string CockTailHalfMidMorningTime { get; set; }
        public string CockTailHalfLunchTime { get; set; }
        public string CockTailHalfAfternoonTime { get; set; }
        public string CockTailHalfEveningTime { get; set; }

        [RequirementAttribute("CockTailFull",true)]
        [Display(Name = "CockTail Full")]
        public bool CockTailFull { get; set; }
        public int? CockTailFullCateringInfoID { get; set; }
         [CateringRequirementsCountValidation]
        public int? CockTailFullCount { get; set; }
        public string CockTailFullMorningTime { get; set; }
        public string CockTailFullMidMorningTime { get; set; }
        public string CockTailFullLunchTime { get; set; }
        public string CockTailFullAfternoonTime { get; set; }
        public string CockTailFullEveningTime { get; set; }

        [RequirementAttribute("BowlFoods",true)]
        [Display(Name = "Bowl Foods")]
        public bool BowlFoods { get; set; }
        public int? BowlFoodsCateringInfoID { get; set; }
        [CateringRequirementsCountValidation]
        public int? BowlFoodsCount { get; set; }
        public string BowlFoodsMorningTime { get; set; }
        public string BowlFoodsMidMorningTime { get; set; }
        public string BowlFoodsLunchTime { get; set; }
        public string BowlFoodsAfternoonTime { get; set; }
        public string BowlFoodsEveningTime { get; set; }

        [RequirementAttribute("BuffetBreakfast",true)]
        [Display(Name = "Buffet Breakfast")]
        public bool BuffetBreakfast { get; set; }
        public int? BuffetBreakfastCateringInfoID { get; set; }
        [CateringRequirementsCountValidation]
        public int? BuffetBreakfastCount { get; set; }
        public string BuffetBreakfastMorningTime { get; set; }
        public string BuffetBreakfastMidMorningTime { get; set; }
        public string BuffetBreakfastLunchTime { get; set; }
        public string BuffetBreakfastAfternoonTime { get; set; }
        public string BuffetBreakfastEveningTime { get; set; }

        [RequirementAttribute("FingerBreakfast",true)]
        public bool FingerBreakfast { get; set; }
        public int? FingerBreakfastCateringInfoID { get; set; }
        [CateringRequirementsCountValidation]
        public int? FingerBreakfastCount { get; set; }
        public string FingerBreakfastMorningTime { get; set; }
        public string FingerBreakfastMidMorningTime { get; set; }
        public string FingerBreakfastLunchTime { get; set; }
        public string FingerBreakfastAfternoonTime { get; set; }
        public string FingerBreakfastEveningTime { get; set; }

        [RequirementAttribute("Corkage",false)]
        public bool Corkage { get; set; }
        public int? CorkageCateringInfoID { get; set; }
        public int CorkageCount { get; set; }
    }
}
