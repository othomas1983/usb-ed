﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.Models.FacultyManagement
{
    public class ResearchContributionTypes
    {
        public int ResearchContributionTypeID { get; set; }
        public string ResearchContributionType { get; set; }
    }
}
