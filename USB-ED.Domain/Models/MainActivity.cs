﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.Models.FacultyManagement
{
    public class MainActivities
    {
        public int MainActivityID { get; set; }
        public string MainActivity { get; set; }
    }
}