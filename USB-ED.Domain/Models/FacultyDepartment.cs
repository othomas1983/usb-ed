﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.Models.FacultyManagement
{
    public class FacultyDepartment : BaseFacultyManagement
    {
        public int FacultyDepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public string DepartmentCategoryName { get; set; }
        public int DepartmentID { get; set; }
        public int DepartmentCategoryID { get; set; }
    }
}
