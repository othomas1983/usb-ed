﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using USB_ED.Validation;

namespace USB_ED.Models
{
    public class USBApplicationForm
    {
        #region Field of Study
        
        public string Programme { get; set; }

        public Guid Offering { get; set; }

        #endregion

        #region Personal Information

        [Required]
        public Guid Title { get; set; }

        [Required]
        public string Initials { get; set; }

        [Required]
        public string Surname { get; set; }

        [Required]
        public string FullFirstNames { get; set; }

        [Required]
        public string GivenName { get; set; }

        [Required]
        public Guid Nationality { get; set; }

        [Display(Name = "Identification Type:")]
        public Guid? ForeignIdType { get; set; }

        public string PermitType { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required]
        [Display(Name = "Date of Birth")]
        public DateTime DateOfBirth { get; set; }

        [Required]
        public Guid Gender { get; set; }

        [EthnicityValidation]
        public Guid? Ethnicity { get; set; }

        public Guid HomeLanguage { get; set; }

        public Guid CorrespondenceLanguage { get; set; }

        public Guid MaritalStatus { get; set; }

        public string MaidenName { get; set; }

        public Guid? Disability { get; set; }

        public string DisabilitiesOther { get; set; }

        public bool UsesWheelchair { get; set; }

        #endregion

        #region Contact Details

        public string BusinessPhone { get; set; }

        public string HomePhone { get; set; }

        public string FaxNumber { get; set; }

        public string MobileNumber { get; set; }

        public string Email { get; set; }

        #endregion
    }
}
