﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using SettingsMan = USB_ED.Properties.Settings;
namespace USB_ED.Models
{
    public class USBApplicationFormInfo
    {
        protected static string programmeOfferingsUrl = SettingsMan.Default.programmeOfferingsUrl;

        protected static string gendersUrl = SettingsMan.Default.gendersUrl;

        protected static string titlesUrl = SettingsMan.Default.titlesUrl;

        protected static string ethnicitiesUrl = SettingsMan.Default.ethnicitiesUrl;

        protected static string nationalitiesUrl = SettingsMan.Default.nationalitiesUrl;

        protected static string foreignIdentificationTypesUrl = SettingsMan.Default.foreignIdentificationTypesUrl;

        protected static string languagesUrl = SettingsMan.Default.languagesUrl;

        //protected static string permitTypesUrl = SettingsMan.Default.permitTypesUrl;


        #region Properties

        private static USBApplicationFormInfo Info;

        public static List<SelectListItem> ProgrammeOfferings { get; set; }

        public static List<SelectListItem> HomeLanguages { get; set; }

        public static List<SelectListItem> CorrespondenceLanguages { get; set; }

        public static List<SelectListItem> Nationalities { get; set; }

        public static List<SelectListItem> AddressCountries { get; set; }

        public static List<SelectListItem> Titles { get; set; }

        public static List<SelectListItem> Genders { get; set; }

        public static List<SelectListItem> MaritalStatuses { get; set; }

        public static List<SelectListItem> Ethnicities { get; set; }

        public static List<SelectListItem> IdDocumentTypes { get; set; }

        public static List<SelectListItem> PermitTypes { get; set; }

        public static List<SelectListItem> Disabilities { get; set; }

        public static List<SelectListItem> YesNoList { get; set; }

        public static bool HasListItems { get; set; }

        #endregion

        #region Public Methods

        public static USBApplicationFormInfo LoadInfoMembers(string courseId)
        {
            try
            {
                WebClient client = new WebClient();
                LoadProgrammeOfferings(client, courseId);

                if (Info.IsNull())
                {
                    HasListItems = true;
                    Info = new USBApplicationFormInfo();

                    LoadTitles(client);

                    LoadHomeLanguages(client);

                    LoadCorrespondenceLanguages(client);

                    LoadGenders(client);

                    LoadMaritalStatuses(client);

                    LoadEthnicities(client);

                    LoadNationalities(client);

                    LoadIdDocumentTypes(client);

                    LoadPermitTypes(client);
                }
            }
            catch (Exception ex)
            {
                HasListItems = false;
            }
            return Info;
        }

        #endregion

        #region Private Methods

        public static void LoadProgrammeOfferings(WebClient client, string courseId)
        {
            var url = string.Empty;
            if (courseId.IsNotNull())
            {
                url = programmeOfferingsUrl + "?courseId=" + courseId;
            }
            else
            {
                url = programmeOfferingsUrl;
            }

            ProgrammeOfferings = new List<SelectListItem>();

            dynamic result = JsonConvert.DeserializeObject(client.DownloadString(url));

            foreach (dynamic item in result)
            {
                ProgrammeOfferings.Add(new SelectListItem { Text = item.Name, Value = item.Id });
            }
        }

        public static void LoadTitles(WebClient client)
        {
            Titles = new List<SelectListItem>();
            Titles.Add(new SelectListItem { Text = "", Value = "" });
            dynamic result = JsonConvert.DeserializeObject(client.DownloadString(titlesUrl));

            foreach (dynamic item in result)
            {
                Titles.Add(new SelectListItem { Text = item.Name, Value = item.Value });
            }
        }

        public static void LoadHomeLanguages(WebClient client)
        {
            HomeLanguages = new List<SelectListItem>();
            HomeLanguages.Add(new SelectListItem { Text = "", Value = "" });
            dynamic result = JsonConvert.DeserializeObject(client.DownloadString(languagesUrl));

            foreach (dynamic item in result)
            {
                HomeLanguages.Add(new SelectListItem { Text = item.Name, Value = item.Value });
            }
        }

        public static void LoadMaritalStatuses(WebClient client)
        {
            MaritalStatuses = new List<SelectListItem>();
            MaritalStatuses.Add(new SelectListItem { Text = "", Value = "0" });
            MaritalStatuses.Add(new SelectListItem { Text = "Divorced", Value = "1" });
            MaritalStatuses.Add(new SelectListItem { Text = "Life Partner", Value = "2" });
            MaritalStatuses.Add(new SelectListItem { Text = "Married", Value = "3" });
            MaritalStatuses.Add(new SelectListItem { Text = "Seperated", Value = "3" });
            MaritalStatuses.Add(new SelectListItem { Text = "Single", Value = "3" });
            MaritalStatuses.Add(new SelectListItem { Text = "Unknown", Value = "3" });
            MaritalStatuses.Add(new SelectListItem { Text = "Widow/Widower", Value = "3" });

            //dynamic result = JsonConvert.DeserializeObject(client.DownloadString(permitTypesUrl));

            //foreach (dynamic item in result)
            //{
            //    IdDocumentTypes.Add(new SelectListItem { Text = item.Name, Value = item.Value });
            //}
        }

        public static void LoadCorrespondenceLanguages(WebClient client)
        {
            CorrespondenceLanguages = new List<SelectListItem>();
            CorrespondenceLanguages.Add(new SelectListItem { Text = "", Value = "0" });
            CorrespondenceLanguages.Add(new SelectListItem { Text = "Afrikaans", Value = "1" });
            CorrespondenceLanguages.Add(new SelectListItem { Text = "English", Value = "2" });
            CorrespondenceLanguages.Add(new SelectListItem { Text = "Stop All Communication", Value = "3" });

            //dynamic result = JsonConvert.DeserializeObject(client.DownloadString(permitTypesUrl));

            //foreach (dynamic item in result)
            //{
            //    IdDocumentTypes.Add(new SelectListItem { Text = item.Name, Value = item.Value });
            //}
        }

        public static void LoadGenders(WebClient client)
        {
            Genders = new List<SelectListItem>();
            Genders.Add(new SelectListItem { Text = "", Value = "" });
            dynamic result = JsonConvert.DeserializeObject(client.DownloadString(gendersUrl));

            foreach (dynamic item in result)
            {
                Genders.Add(new SelectListItem { Text = item.Name, Value = item.Value });
            }
        }

        public static void LoadEthnicities(WebClient client)
        {
            Ethnicities = new List<SelectListItem>();
            Ethnicities.Add(new SelectListItem { Text = "", Value = "" });
            dynamic result = JsonConvert.DeserializeObject(client.DownloadString(ethnicitiesUrl));

            foreach (dynamic item in result)
            {
                Ethnicities.Add(new SelectListItem { Text = item.Name, Value = item.Value });
            }
        }

        public static void LoadNationalities(WebClient client)
        {
            Nationalities = new List<SelectListItem>();
            Nationalities.Add(new SelectListItem { Text = "", Value = "" });
            dynamic result = JsonConvert.DeserializeObject(client.DownloadString(nationalitiesUrl));

            foreach (dynamic item in result)
            {
                Nationalities.Add(new SelectListItem { Text = item.Name, Value = item.Value });
            }
        }

        public static void LoadIdDocumentTypes(WebClient client)
        {
            IdDocumentTypes = new List<SelectListItem>();
            IdDocumentTypes.Add(new SelectListItem { Text = "", Value = "" });
            dynamic result = JsonConvert.DeserializeObject(client.DownloadString(foreignIdentificationTypesUrl));

            foreach (dynamic item in result)
            {
                IdDocumentTypes.Add(new SelectListItem { Text = item.Name, Value = item.Value });
            }
        }

        public static void LoadPermitTypes(WebClient client)
        {
            PermitTypes = new List<SelectListItem>();
            PermitTypes.Add(new SelectListItem { Text = "", Value = "0" });
            PermitTypes.Add(new SelectListItem { Text = "Diplomatic services", Value = "1" });
            PermitTypes.Add(new SelectListItem { Text = "Endorsed visitor;s permit", Value = "2" });
            PermitTypes.Add(new SelectListItem { Text = "No permit", Value = "3" });
            PermitTypes.Add(new SelectListItem { Text = "Residence permit", Value = "4" });
            PermitTypes.Add(new SelectListItem { Text = "Study permit", Value = "5" });
            PermitTypes.Add(new SelectListItem { Text = "Work permit", Value = "6" });

            //dynamic result = JsonConvert.DeserializeObject(client.DownloadString(permitTypesUrl));

            //foreach (dynamic item in result)
            //{
            //    IdDocumentTypes.Add(new SelectListItem { Text = item.Name, Value = item.Value });
            //}
        }

        #endregion
    }    
}
