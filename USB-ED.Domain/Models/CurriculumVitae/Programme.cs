﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.Models.FacultyManagement
{
    public class Programmes : BaseFacultyManagement
    {
        #region Properties

        public int ProgrammeID { get; set; }
        public string Programme { get; set; }
        public Nullable<int> ProgrammeOfferingCount { get; set; }

        #endregion
    }
}