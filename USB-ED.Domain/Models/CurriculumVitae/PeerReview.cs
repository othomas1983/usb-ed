﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB_ED.Models
{
    public class PeerReview
    {
        #region Properties

        public List<PeerReviewIntegrity> PeerReviewIntegrity { get; set; }
        public List<PeerReviewInclusivity> PeerReviewInclusivity { get; set; }
        public List<PeerReviewInnovation> PeerReviewInnovation { get; set; }
        public List<PeerReviewEngagement> PeerReviewEngagement { get; set; }
        public List<PeerReviewExcellence> PeerReviewExcellence { get; set; }
        public List<PeerReviewSustainability> PeerReviewSustainability { get; set; }

        #endregion
    }

    public class PeerReviewIntegrity
    {
        #region Properties

        public int PeerPerformanceManagementID { get; set; }
        public int PeerCVID { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
        public string ValueCommitment { get; set; }
        public Nullable<int> Year { get; set; }

        #endregion
    }

    public class PeerReviewInclusivity
    {
        #region Properties

        public int PeerPerformanceManagementID { get; set; }
        public int PeerCVID { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
        public string ValueCommitment { get; set; }
        public Nullable<int> Year { get; set; }

        #endregion
    }

    public class PeerReviewInnovation
    {
        #region Properties

        public int PeerPerformanceManagementID { get; set; }
        public int PeerCVID { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
        public string ValueCommitment { get; set; }
        public Nullable<int> Year { get; set; }

        #endregion
    }

    public class PeerReviewEngagement
    {
        #region Properties

        public int PeerPerformanceManagementID { get; set; }
        public int PeerCVID { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
        public string ValueCommitment { get; set; }
        public Nullable<int> Year { get; set; }

        #endregion
    }

    public class PeerReviewSustainability
    {
        #region Properties

        public int PeerPerformanceManagementID { get; set; }
        public int PeerCVID { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
        public string ValueCommitment { get; set; }
        public Nullable<int> Year { get; set; }

        #endregion
    }

    public class PeerReviewExcellence
    {
        #region Properties

        public int PeerPerformanceManagementID { get; set; }
        public int PeerCVID { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
        public string ValueCommitment { get; set; }
        public Nullable<int> Year { get; set; }

        #endregion
    }


}
