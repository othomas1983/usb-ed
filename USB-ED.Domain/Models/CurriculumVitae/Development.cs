﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.Models.FacultyManagement
{
    public class Development : BaseFacultyManagement
    {
        #region Properties
        
        public int DevelopmentID { get; set; }
        public string Accomplishment { get; set; }
        public int DevelopmentCategoryID { get; set; }

        public string City { get; set; }
        public string Country { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy-mm-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> EndDate { get; set; }
        public string Institution { get; set; }

        public Nullable<int> NoOfDays { get; set; }
        public string Organisation { get; set; }
        public string Position { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy-mm-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> StartDate { get; set; }
        public string Topic { get; set; }
        public List<ContinuingEducationDevelopment> ContinuingEducationDevelopment { get; set; }
        public List<Development> Developments { get; set; }
        public List<ExtracurricularDevelopment> ExtracurricularDevelopment { get; set; }
        public List<MembershipDevelopment> MembershipDevelopment { get; set; }
        public List<Non_AcademicDevelopment> Non_AcademicDevelopment { get; set; }
        #endregion
    }

    public class MembershipDevelopment : BaseFacultyManagement
    {
        #region Properties

        public int DevelopmentID { get; set; }        
        public int DevelopmentCategoryID { get; set; }
        public string Accomplishment { get; set; }
        public string DateJoined { get; set; }
        public string TerminationDate { get; set; }

        #endregion
    }

    public class ContinuingEducationDevelopment : BaseFacultyManagement
    {

        #region Properties

        public int DevelopmentID { get; set; }
        public int DevelopmentCategoryID { get; set; }

        public string Country { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy-mm-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> EndDate { get; set; }
        public string Institution { get; set; }

        public Nullable<int> NoOfDays { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public string Topic { get; set; }

        #endregion

    }

    public class Non_AcademicDevelopment : BaseFacultyManagement
    {

        #region Properties

        public int DevelopmentID { get; set; }        
        public int DevelopmentCategoryID { get; set; }
        public string Country { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy-mm-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> EndDate { get; set; }
        public string Accomplishment { get; set; }
        public string Institution { get; set; }
 
        public Nullable<int> NoOfDays { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy-mm-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> StartDate { get; set; }

        #endregion
    }

    public class ExtracurricularDevelopment : BaseFacultyManagement
    {

        #region Properties

        public int DevelopmentID { get; set; }
        public int DevelopmentCategoryID { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-mm-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> EndDate { get; set; }
        public string City { get; set; }
        public string Country { get; set; }

        public string Organisation { get; set; }
        public string Position { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy-mm-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> StartDate { get; set; }

        #endregion
    }
}