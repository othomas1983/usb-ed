﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.Models.FacultyManagement
{
    public class CourseDesign : BaseFacultyManagement
    {
        #region Properties

        public int CourseDesignID { get; set; }
        public int ProgrammeID { get; set; }
        public decimal ContactTime { get; set; }
        public string Module { get; set; }
        public decimal TotalCredit { get; set; }

        #endregion
    }
}
