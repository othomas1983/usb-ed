﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.Models.FacultyManagement
{
    public class ProgrammeOfferings
    {
        #region Properties 

        public int ProgrammeOfferingID { get; set; }
        public string ProgrammeOffering { get; set; }
        public int ProgrammeID { get; set; }

        #endregion
    }
}
