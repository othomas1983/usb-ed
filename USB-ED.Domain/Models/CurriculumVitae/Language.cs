﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.Models.FacultyManagement
{
    public class Language
    {
        #region Properties

        public int LanguageID { get; set; }
        public string LanguageName { get; set; }
        public Nullable<bool> isActive { get; set; }

        #endregion
    }
}
