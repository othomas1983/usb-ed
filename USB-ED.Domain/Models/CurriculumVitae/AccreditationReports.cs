﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.Models.FacultyManagement
{
    public class AccreditationReports
    {
        #region Properties

        public int AccreditationReportID { get; set; }
        public string AccreditationReport { get; set; }
        public bool isActive { get; set; }

        #endregion

        #region Public Methods

        public IOrderedEnumerable<AccreditationReports> LoadAccreditationReports()
        {
            var list = new List<AccreditationReports>();

            var task = new USB_ED.Tasks.FacultyManagementTasks();            
            var users = task.LoadAccreditationReports();
            list = JsonConvert.DeserializeObject<List<AccreditationReports>>(users);            

            return list.OrderBy(a =>a.AccreditationReport);
        }

        #endregion
    }
}
