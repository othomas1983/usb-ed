﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB_ED.Models
{
    public class CommitmentPerformanceCategory
    {
        public int PerformanceCategoryID { get; set; }

        public string PerformanceCategory { get; set; }

        public string BaseLine { get; set; }
    }
}
