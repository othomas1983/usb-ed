﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Serialization;
using USB_ED.Global;
using USB_ED.Models;

namespace USB.Models.FacultyManagement
{
    public class FacultyManagement
    {
        #region Properties

        public int AffiliationID { get; set; }
        public int AcademicRankID { get; set; }
        public Nullable<int> ApplicationUserRoleID { get; set; }
        public int CVID { get; set; }
        public string EmployeeId { get; set; }
        public int MainActivityID { get; set; }
        public string AcademicRank { get; set; }
        public string ApplicationUserRole { get; set; }
        public string Biography { get; set; }
        public string BusinessContactNo { get; set; }
        public string BusinessUnit { get; set; }

        public string EmailAddress { get; set; }
        public string Expertise { get; set; }
        public string HighestQualification { get; set; }
        public int? HighestQualificationID { get; set; }
        public string HomeInstitution { get; set; }
        public string JSONResearchAssignments { get; set; }
        //This is used when a Logged in user loads another users data
        public string LoadedEmployeeName { get; set; }
        public string Initials { get; set; }
        public byte[] Image { get; set; }
        public Nullable<bool> isFaculty { get; set; }
        public string Name { get; set; }
        public string MainActivity { get; set; }
        public string OrganisationalResponsibility { get; set; }
        public string Password { get; set; }
        public string Position { get; set; }
        public string ResearchInterest { get; set; }
        public string ResearchCurrent { get; set; }
        public string ResearchPlanned { get; set; }
        public string Programme { get; set; }
        public string SamAccountName { get; set; }
        public string SubjectsTaught { get; set; }
        public string Surname { get; set; }
        public string Gender { get; set; }
        public int Nationality { get; set; }

        public string Title { get; set; }
        public string UserName { get; set; }

        public List<ABSRating> ABSRatings { get; set; }
        public List<AcademicRank> AcademicRanks { get; set; }
        public List<ManagementLoad> AccreditationReports { get; set; }
        public List<Affiliation> Affiliations { get; set; }
        public List<Award> Awards { get; set; }

        public List<BooksCoordinationPublication> BooksCoordinationPublication { get; set; }
        public List<BooksPublication> BooksPublication { get; set; }
        public List<BusinessUnits> BusinessUnits { get; set; }

        public List<ChaptersPublication> ChaptersPublication { get; set; }
        public List<CaseStudiesPublication> CaseStudiesPublication { get; set; }
        public List<ManagementLoad> CentreHead { get; set; }
        public List<CourseDesign> CourseDesigns { get; set; }
        public List<ConferencePublication> ConferencePublication { get; set; }
        public List<ContinuingEducationDevelopment> ContinuingEducationDevelopment { get; set; }

        public List<Department> Departments { get; set; }
        public List<DepartmentCategory> DepartmentCategories { get; set; }
        public List<Development> Developments { get; set; }
        public List<DHETStatus> DHETStatuses { get; set; }

        public List<Education> Educations { get; set; }
        public List<ExecutiveEducationExperience> ExecutiveEducationExperience { get; set; }
        public List<Experience> Experiences { get; set; }
        public List<ExperienceCategory> ExperienceCategories { get; set; }
        public List<ExtracurricularDevelopment> ExtracurricularDevelopment { get; set; }
   
        public FacultyDepartment FacultyDepartment { get; set; }
        public List<FacultyLanguage> FacultyLanguages { get; set; }
        public List<FullTimeConsultingExperience> FullTimeConsultingExperience { get; set; }

        public List<GrantsFunding> GrantsFunding { get; set; }

        public List<LaboratoriesParticipationResearch> LaboratoriesParticipationResearch { get; set; }
        public List<Language> Languages { get; set; }
        public List<ManagementLoad> LeadershipOneOnOne { get; set; }
        public List<Locus> Locuses { get; set; }

        public List<ManagementLoadCategory> ManagementLoadCategory { get; set; }
        public List<ManagementLoad> ManagementLoads { get; set; }
        public List<ManagementLoad> ManagementLoadOther { get; set; }
        public List<MainActivities> MainActivities { get; set; }
        public List<MembershipDevelopment> MembershipDevelopment { get; set; }
        public List<MonographPublication> MonographPublication { get; set; }

        public List<Non_AcademicDevelopment> Non_AcademicDevelopment { get; set; }
        public List<NonRefereedPapersAndCommunicationsPublication> NonRefereedPapersAndCommunicationsPublication { get; set; }
        public List<NonRefereedArticlesPublication> NonRefereedArticlesPublication { get; set; }

        public List<PartTimeConsultingExperience> PartTimeConsultingExperience { get; set; }
        public List<PeerReviewedPublication> PeerReviewedPublication { get; set; }
        public List<PeerReviewedProceedingsPublication> PeerReviewedProceedingsPublication { get; set; }
        public List<PeerReviewedWithoutProceedingsPublication> PeerReviewedWithoutProceedingsPublication { get; set; }
        public List<PermanentProfessionalCareerExperience> PermanentProfessionalCareerExperience { get; set; }
        public List<Contributor> PeopleInResearch { get; set; }
        public List<PhdResearch> PhdResearch { get; set; }
        public List<Programmes> Programmes { get; set; }
        public List<ManagementLoad> ProgrammeHead { get; set; }
        public List<ProgrammeOfferings> ProgrammeOfferings { get; set; }
        public List<ProgrammeParticipationResearch> ProgrammeParticipationResearch { get; set; }
        public List<Publication> Publications { get; set; }

        public List<ManagementLoad> OrganisingAConference { get; set; }

        public List<ManagementLoad> QuestionareAdvisory { get; set; }

        public List<Research> Researches { get; set; }
        public List<ResearchHistory> ResearchHistory { get; set; }
        public List<ManagementLoad> ResearchReportOneOnOne { get; set; }
        public List<ResearchOutputCategory> ResearchOutputCategories { get; set; }

        public List<SeminarsPublication> SeminarsPublication { get; set; }
        public List<ScientificReviewingResearch> ScientificReviewingResearch { get; set; }
        public List<StudyConsultationPublication> StudyConsultationPublication { get; set; }

        public List<TeachingExperience> TeachingExperience { get; set; }
        public List<TeachingLoad> TeachingLoad { get; set; }
        public List<ThesisPublication> ThesisPublication { get; set; }

        #endregion

        #region Public Methods

        public int SaveCurriculumVitae(FacultyManagement model)
        {
            var task = new USB_ED.Tasks.FacultyManagementTasks();            
            var mapping = new FacultyManagementDataMapping();
            
            var cvData = mapping.MapFromClientData(model);

            var personalInformation = mapping.MapPersonalInformation(model);
            var personalInfo = new JavaScriptSerializer().Serialize(personalInformation);

            if (CourseDesigns.IsNotNull())
            {
                var courseDesign = new JavaScriptSerializer().Serialize(CourseDesigns);
                task.AddOrUpdateCourseDesign(courseDesign, CVID,SessionHelper.LoggedInUser.EmployeeName);
            }

            return 0;
        }

        public FacultyManagement LoadFacultyManagement(string employeeId, string loggedInUserName)
        {            

           
            var task = new USB_ED.Tasks.FacultyManagementTasks();

            var facultyManagement = new FacultyManagement();
            var mapping = new FacultyManagementDataMapping();

            var result = task.Load(employeeId, loggedInUserName);

            var managementLoadCategory = task.LoadManagementLoadCategory();
            var managementCategory = JsonConvert.DeserializeObject<List<ManagementLoadCategory>>(managementLoadCategory);

            var teachingLoad = task.LoadTeachingLoad(result.CVID);
            facultyManagement.TeachingLoad = teachingLoad;

            facultyManagement.Programmes = task.LoadProgramme();

            var programmeOffering = task.LoadProgrammeOffering();
            facultyManagement.ProgrammeOfferings = JsonConvert.DeserializeObject<List<ProgrammeOfferings>>(programmeOffering);

            //var dhetStatus = task.LoadDHETStatus();
            //var dhetStatusList = JsonConvert.DeserializeObject<List<USB.Models.FacultyManagement.DHETStatus>>(dhetStatus);

            var businessUnits = task.LoadBusinessUnits();
            facultyManagement.BusinessUnits = JsonConvert.DeserializeObject<List<USB.Models.FacultyManagement.BusinessUnits>>(businessUnits);

            var mainActivities = task.LoadMainActivity();
            var mainActivitiesList = JsonConvert.DeserializeObject<List<USB.Models.FacultyManagement.MainActivities>>(mainActivities);

            var academicRanks = task.LoadAcademicRanks();
            var academicRankList = JsonConvert.DeserializeObject<List<USB.Models.FacultyManagement.AcademicRank>>(academicRanks);

            var departments = task.LoadDepartment();
            var departmentList = JsonConvert.DeserializeObject<List<USB.Models.FacultyManagement.Department>>(departments);

            var departmentCategories = task.LoadDepartmentCategory();
            var departmentCategoriesList = JsonConvert.DeserializeObject<List<USB.Models.FacultyManagement.DepartmentCategory>>(departmentCategories);

            var writeHand = task.LoadWritehandResearchAssignments(result.EmployeeId);
            facultyManagement.JSONResearchAssignments = writeHand;
                    
            FacultyManagementInfo.ManagementLoadCategories = managementCategory;    
         
           // mapping.MapToDHETStatus(facultyManagement, dhetStatusList);
            mapping.MapMainActivities(facultyManagement, mainActivitiesList);
            mapping.MapAcademicRanks(facultyManagement, academicRankList);
       
            facultyManagement.Departments = departmentList;
            facultyManagement.DepartmentCategories = departmentCategoriesList;
            facultyManagement.AcademicRank = result.AcademicRank;
            facultyManagement.AcademicRankID = ResolveAcademicRankId(facultyManagement.AcademicRanks, result.AcademicRank);
            facultyManagement.ApplicationUserRole = result.ApplicationUserRole;
            facultyManagement.ApplicationUserRoleID = result.ApplicationUserRoleID;
            facultyManagement.AffiliationID = result.AffiliationID;
            facultyManagement.Biography = result.Biography;
            facultyManagement.BusinessContactNo = result.BusinessContactNo.TrimEnd(" ");
            facultyManagement.BusinessUnit = result.BusinessUnit;
            facultyManagement.EmailAddress = result.EmailAddress;
            facultyManagement.EmployeeId = result.EmployeeId;
            facultyManagement.Expertise = result.Expertise;
            facultyManagement.HighestQualification = result.HighestQualification;
            facultyManagement.HomeInstitution = result.HomeInstitution;
            facultyManagement.Initials = result.Initials;
            facultyManagement.isFaculty = result.isFaculty;
            facultyManagement.Name = result.Name;                
            facultyManagement.OrganisationalResponsibility = result.OrganisationalResponsibility;
            facultyManagement.Position = result.Position;
            facultyManagement.ResearchInterest = result.ResearchInterest;
            facultyManagement.Surname = result.Surname;
            facultyManagement.Title = result.Title;
            facultyManagement.CVID = result.CVID;
            facultyManagement.MainActivity = result.MainActivity;
            facultyManagement.MainActivityID = result.MainActivityID;

            var map = new FacultyManagementDataMapping();
            map.MapToClientData(facultyManagement, result);

            return facultyManagement;
        }

        #endregion

        #region Private Methods

        private int ResolveAcademicRankId(List<Models.FacultyManagement.AcademicRank> academicRankList, string description)
        {
            if (description.IsNotNullOrEmpty())
            {
                if (academicRankList.IsNotNull())
                {
                    if (academicRankList.Count > 0)
                    {
                        var id = academicRankList.Where(a => a.AcademicRankDescription == description).FirstOrDefault().AcademicRankID;
                        return id;
                    }
                }
            }

            return 1;
        }

        #endregion       
    }
}
