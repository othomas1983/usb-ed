﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.Models.FacultyManagement
{
    public partial class FacultyPermission
    {
        #region Properties

        public int FacultyPermissionID { get; set; }
        public int CVID { get; set; }
        public Nullable<int> ApplicationUserRoleID { get; set; }
        public string ApplicationUserRole { get; set; }
        public bool isActive { get; set; }
        public string Initials { get; set; }
        public string Surname { get; set; }
        public string Title { get; set; }
 

        public string DisplayName
        {
            get { return Surname + " " + Initials + " " + Title; }
        }

        #endregion

        #region Public Methods

        public void UpdateFacultyPermissions(List<FacultyPermission> facultyPermissions)
        {

        }

        public IOrderedEnumerable<FacultyPermission> LoadFacultyPermissions(int cvid)
        {
            var userList = new List<FacultyPermission>();

            var task = new USB_ED.Tasks.FacultyManagementTasks();
            var users = task.LoadFacultyPermissions(cvid);
            userList = JsonConvert.DeserializeObject<List<FacultyPermission>>(users);

            var items = userList.OrderBy(g => g.Surname);
            return items;
        }

        #endregion
    }
}