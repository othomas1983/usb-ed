﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.Models.FacultyManagement
{
    public enum ExperienceCategoryShortName
    {
        #region Enums

        //IMPORTANT - Enum value must match ExperienceCategoryID in database
        Teaching = 1,
        ExecutiveEducation = 2,
        FullTime = 3,
        PartTime = 5,
        PermanentProfCareer = 6,
        EngagementWithBusinessAndSociety = 7,
        EditorialBoardmembers =8
        #endregion
    }

    public class ExperienceCategory
    {
        #region Properties

        public int ExperienceCategoryID { get; set; }
        public string ExperienceCategoryDescription { get; set; }
        public string ShortName { get; set; }

        #endregion
    }
}
