﻿using System.Collections.Generic;

namespace USB_ED.Models
{
    public class SupervisorReview
    {
        #region Properties

        public List<SupervisorReviewIntegrity> SupervisorReviewIntegrity { get; set; }
        public List<SupervisorReviewInclusivity> SupervisorReviewInclusivity { get; set; }
        public List<SupervisorReviewInnovation> SupervisorReviewInnovation { get; set; }
        public List<SupervisorReviewEngagement> SupervisorReviewEngagement { get; set; }
        public List<SupervisorReviewExcellence> SupervisorReviewExcellence { get; set; }
        public List<SupervisorReviewSustainability> SupervisorReviewSustainability { get; set; }

        #endregion
    }

    public class SupervisorReviewIntegrity
    {
        #region Properties

        public int SupervisorPerformanceManagementID { get; set; }
        public int SupervisorCVID { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
        public string ValueCommitment { get; set; }
        public int? Year { get; set; }

        #endregion
    }

    public class SupervisorReviewInclusivity
    {
        #region Properties

        public int SupervisorPerformanceManagementID { get; set; }
        public int SupervisorCVID { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
        public string ValueCommitment { get; set; }
        public int? Year { get; set; }

        #endregion
    }

    public class SupervisorReviewInnovation
    {
        #region Properties

        public int SupervisorPerformanceManagementID { get; set; }
        public int SupervisorCVID { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
        public string ValueCommitment { get; set; }
        public int? Year { get; set; }

        #endregion
    }

    public class SupervisorReviewEngagement
    {
        #region Properties

        public int SupervisorPerformanceManagementID { get; set; }
        public int SupervisorCVID { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
        public string ValueCommitment { get; set; }
        public int? Year { get; set; }

        #endregion
    }

    public class SupervisorReviewSustainability
    {
        #region Properties

        public int SupervisorPerformanceManagementID { get; set; }
        public int SupervisorCVID { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
        public string ValueCommitment { get; set; }
        public int? Year { get; set; }

        #endregion
    }

    public class SupervisorReviewExcellence
    {
        #region Properties

        public int SupervisorPerformanceManagementID { get; set; }
        public int SupervisorCVID { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
        public string ValueCommitment { get; set; }
        public int? Year { get; set; }

        #endregion
    }
}
