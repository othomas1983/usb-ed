﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.Models.FacultyManagement
{
    public enum ManagementLoadCategoryShortName
    {
        #region Enums

        //IMPORTANT - Enum value must match ManagementLoadCategoryID in database
        ProgrammeHead = 1,
        LeadershipOneOnOne = 2,
        CentreHead = 3,
        QuestionnaireAdvisory = 4,
        OrganisingConference = 5,
        AccreditationReports = 6,
        ResearchReportOneOnOne = 7,
        Other = 8,
        ChairOfAdministrativeManagementCommittee = 9,
        TaskTeamMember = 10

        #endregion
    }

    public class ManagementLoad : BaseFacultyManagement
    {
        #region Properties
        
        public int ManagementLoadID { get; set; }
        public Nullable<int> ManagementLoadCategoryID { get; set; }
        public Nullable<int> ProgrammeOfferingID { get; set; }
        public Nullable<int> ProgrammeID { get; set; }
        public Nullable<int> TaskTeamActivityID { get; set; }
        public Nullable<int> ManagementLoadYear { get; set; }
        public DateTime? ManagementLoadStartDate { get; set; }
        public DateTime? ManagementLoadEndDate { get; set; }
        public Nullable<decimal> Credit { get; set; }
        public string Description { get; set; }
        public string ManagementLoadDescription { get; set; }
        public int ManagementLoadCredit { get; set; }
        public Nullable<int> ModuleCount { get; set; }
        public Nullable<int> StudentCount { get; set; }
        public Nullable<int> TotalSAQACredit { get; set; }
        public List<ManagementLoad> CentreHead { get; set; }
        public List<ManagementLoad> LeadershipOneOnOne { get; set; }
        public List<ManagementLoad> ManagementLoads { get; set; }
        public List<ManagementLoad> ManagementLoadOther { get; set; }
        public List<ManagementLoad> OrganisingAConference { get; set; }
        public List<ManagementLoad> ProgrammeHead { get; set; }
        //public List<ManagementLoad> QuestionareAdvisory { get; set; }
        public List<ManagementLoad> ResearchReportOneOnOne { get; set; }
        public List<ManagementLoad> ChairOfCommittee{ get; set; }
        public List<ManagementLoad> TaskTeamMember { get; set; }
        public Nullable<int> ElectiveModuleCount { get; set; }
        public Nullable<int> ElectiveSAQACredit { get; set; }
        #endregion
    }
}