﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.Models.FacultyManagement
{
    public class Writehand
    {
        #region Properties

        public List<Topic> Topics { get; set; }

        #endregion
    }

    public class Topic
    {
        #region Properties

        public string Programme { get; set; }
        public string Status { get; set; }
        public Student Student { get; set; }
        public Supervisor Supervisor { get; set; }
        public string Title { get; set; }

        #endregion
    }

    public class Student
    {
        #region Properties

        public string Name { get; set; }
        public string StudentNumber { get; set; }

        #endregion
    }

    public class Supervisor
    {
        #region Properties

        public string EmployeeId { get; set; }
        public List<Credits> Credits { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }

        #endregion
    }

    public class Credits
    {
        #region Properties

        public decimal Amount { get; set; }
        public DateTime Date { get; set; }
        public string Step { get; set; }

        #endregion
    }

    public class SecondarySupervisor
    {
        #region Properties

        public string EmployeeId { get; set; }
        public List<Credits> Credits { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }

        #endregion
    }
}
