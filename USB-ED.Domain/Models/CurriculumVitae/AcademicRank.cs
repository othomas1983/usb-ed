﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.Models.FacultyManagement
{
    public class AcademicRank
    {
        #region Properties

        public int AcademicRankID { get; set; }
        public string AcademicRankDescription { get; set; }

        #endregion
    }
}
