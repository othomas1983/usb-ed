﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.Models.FacultyManagement
{
    public class Affiliation
    {
        #region Properties

        public int AffiliationID { get; set; }
        public string AffiliationDescription { get; set; }

        #endregion
    }
}
