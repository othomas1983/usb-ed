﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.Models.FacultyManagement
{
    public class PublicationUploadResult
    {
        public int DocumentID { get; set; }
        public string Url { get; set; }
        public string Result { get; set; }
        public bool Successful { get; set; }
        public string ResultDetails { get; set; }
    }
}