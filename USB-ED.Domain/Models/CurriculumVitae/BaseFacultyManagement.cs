﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.Models.FacultyManagement
{
    public class BaseFacultyManagement
    {
        #region Properties

        public int CVId { get; set; }
        public string CreatedBy { get; set; }        
        public bool IsNew { get; set; }
        public bool isActive { get; set; }
        public bool? IsCurrent { get; set; }
        public string ModifiedBy { get; set; }

        #endregion
    }
}
