﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.Models.FacultyManagement
{
    public class ProgrammeModuleCountRead
    {
        public string Programme { get; set; }
        public int ProgrammeID { get; set; }
        public Nullable<int> ModuleCount { get; set; }
        public Nullable<int> TotalSAQACredit { get; set; }
    }
}
