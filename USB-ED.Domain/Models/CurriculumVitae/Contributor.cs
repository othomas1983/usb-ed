﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.Models.FacultyManagement
{
    public class Contributor
    {
        #region Properties
        
        public Guid Id { get; set; }
        public int CVID { get; set; }
        public int PeopleinResearchID { get; set; }
        public Nullable<int> PeopleinResearchCVID { get; set; }
        public Nullable<int> PersonRoleID { get; set; }
        public Nullable<int> ResearchID { get; set; }
        public string ArticleTitle { get; set; }
        public Nullable<int> ContributorSequence { get; set; }
        public Nullable<decimal> Credit { get; set; }
        public string CVName { get; set; }
        public string CVSurname { get; set; }
        public string Email { get; set; }
        public string Initials { get; set; }
        public string Institution { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }        
        public string PersonRoleDescription { get; set; }
        public string Type { get; set; }
        public Nullable<int> AffiliationID { get; set; }
        public string AffiliationDescription { get; set; }
        public string Gender { get; set; }
        public Nullable<int> NationalityID { get; set; }
        public string Nationality { get; set; }
        public bool isActive { get; set; }

        public string ExternalDisplayName
        {
            get { return Surname + ", " + Initials; }
        }

        public string InternalDisplayName
        {
            get { return CVSurname + ", " + CVName; }
        }

        #endregion
    }
    
}
