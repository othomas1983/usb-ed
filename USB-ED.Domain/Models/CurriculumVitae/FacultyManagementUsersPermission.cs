﻿namespace USB.Models.FacultyManagement
{
    public class FacultyManagementUsersPermission
    {
        public int CvId{get; set;}
        public bool PersonalInformation{get; set;}
        public bool Education{get; set;}
        public bool Experience{get; set;}
        public bool Research{get; set;}
        public bool Publications { get; set;}
        public bool Development{get; set;}
        public bool TeachingLoad{get; set;}
        public bool SupervisionLoad{get; set;}
        public bool ManagementLoad{get; set;}
        public string Title{get; set;}
        public string Surname{get; set;}
        public string Initials{get; set;}
    }
}