﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.Models.FacultyManagement
{
    public class ResearchOutputCategory
    {
        #region Properties

        public int ResearchOutputCategoryID { get; set; }
        public string ResearchOutputCategoryDescription { get; set; }

        #endregion 
    }
}
