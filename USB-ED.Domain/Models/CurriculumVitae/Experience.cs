﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.Models.FacultyManagement
{
    public class Experience : BaseFacultyManagement
    {
        private string _subjectArea;
        private string _typeOfEngagement;
        private string _knowledgeArea;
        private string _topic;
        private string _organisation;
        private string _institution;

        #region Properties

        public int ExperienceID { get; set; }
        public int ExperienceCategoryID { get; set; }
        public string Client { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy-mm-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> EndDate { get; set; }

        public string KnowledgeArea
        {
            get { return _knowledgeArea??_topic; }
            set { _knowledgeArea = value; }
        }

        public string Topic
        {
            get { return _topic??_knowledgeArea; }
            set
            {
                _topic = value;
            }
        }

        public string Institution
        {
            get { return _institution??_organisation; }
            set { _institution = value; }
        }

        public string Organisation
        {
            get { return _organisation??_institution; }
            set { _organisation = value; }
        }

        public Nullable<short> NoOfDays { get; set; }
        public string Position { get; set; }
        public string Programme { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy-mm-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> StartDate { get; set; }

        public string SubjectArea
        {
            get { return _subjectArea ?? _typeOfEngagement; }
            set
            {
                _subjectArea = value;
            }
        }

        public string TypeOfEngagement
        {
            get { return _typeOfEngagement ?? _subjectArea; }
            set
            {
                _typeOfEngagement = value ;
                
            }
        }

        public List<Experience> Experiences { get; set; }
        public List<ExecutiveEducationExperience> ExecutiveEducationExperience { get; set; }
        public List<FullTimeConsultingExperience> FullTimeConsultingExperience { get; set; }
        public List<PartTimeConsultingExperience> PartTimeConsultingExperience { get; set; }
        public List<PermanentProfessionalCareerExperience> PermanentProfessionalCareerExperience { get; set; }
        public List<EngagementWithBusinessAndSocietyExperience> EngagementWithBusinessAndSocietyExperience { get; set; }
        public List<TeachingExperience> TeachingExperience { get; set; }

        public string Publication { get; set; }
        public string PublicationStatus { get; set; }
        public Nullable<int> PublicationEdition { get; set; }
        public List<EditorialBoardmembersExperience> EditorialBoardmembers { get; set; }

        #endregion
    }

    public class TeachingExperience : BaseFacultyManagement
    {
        #region Properties

        public int ExperienceID { get; set; }        
        public int ExperienceCategoryID { get; set; }

        public string City { get; set; }
        public string Country { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy-mm-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> EndDate { get; set; }
        public string Institution { get; set; }

        public Nullable<short> NoOfDays { get; set; }
        public string Position { get; set; }
        public string Programme { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy-mm-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> StartDate { get; set; }
        public string SubjectArea { get; set; }

        #endregion
    }

    public class ExecutiveEducationExperience : BaseFacultyManagement
    {
        #region Properties

        public int ExperienceID { get; set; }        
        public int ExperienceCategoryID { get; set; }
        public string Country { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-mm-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> EndDate { get; set; }

        public Nullable<short> NoOfDays { get; set; }
        public string Programme { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy-mm-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> StartDate { get; set; }
        public string SubjectArea { get; set; }

        #endregion
    }

    public class FullTimeConsultingExperience : BaseFacultyManagement
    {
        #region Properties

        public int ExperienceID { get; set; }
        public int ExperienceCategoryID { get; set; }
        public string Client { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public string KnowledgeArea { get; set; }

        public Nullable<System.DateTime> StartDate { get; set; }

        #endregion
    }

    public class PartTimeConsultingExperience : BaseFacultyManagement
    {
        #region Properties

        public int ExperienceID { get; set; }
        public int ExperienceCategoryID { get; set; }
        public string Client { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }

        public Nullable<short> NoOfDays { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public string SubjectArea { get; set; }

        #endregion
    }

    public class PermanentProfessionalCareerExperience : BaseFacultyManagement
    {
        #region Properties

        public int ExperienceID { get; set; }
        public int ExperienceCategoryID { get; set; }
        public string Country { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public string Institution { get; set; }

        public string Position { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }  
   
        #endregion
    }

    public class EngagementWithBusinessAndSocietyExperience : BaseFacultyManagement
    {
        #region Properties

        public int ExperienceID { get; set; }
        public int ExperienceCategoryID { get; set; }
        public string Country { get; set; }
        public string TypeOfEngagement { get; set; }
        public string Topic { get; set; }
        public string Organisation { get; set; }
        public string City { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public string SubjectArea { get; set; }

        #endregion
    }
    public class EditorialBoardmembersExperience : BaseFacultyManagement
    {
        #region Properties

        public int ExperienceID { get; set; }
        public int ExperienceCategoryID { get; set; }
        public string Publication { get; set; }
        public string TypeOfEngagement { get; set; }
        public string PublicationStatus { get; set; }
        public int? PublicationEdition { get; set; }
        public string Institution { get; set; }

        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }

        #endregion
    }
}
