﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using USB.Models.FacultyManagement;

namespace USB.Models.FacultyManagement
{
    public class FacultyLanguage : BaseFacultyManagement
    {
        #region Properties

        public int FacultyLanguageID { get; set; }
        public int LanguageID { get; set; }
        public string LanguageName { get; set; }

        #endregion
    }

    public class PermissionsAdminModel : BaseFacultyManagement
    {
        public bool personalInformation { get; set; }
        public bool education { get; set; }
        public bool experience { get; set; }
        public bool research { get; set; }
        public bool publications { get; set; }
        public bool development { get; set; }
        public bool teachingLoad { get; set; }
        public bool supervisionLoad { get; set; }
        public bool managementLoad { get; set; }
        public string title { get; set; }
        public string surname { get; set; }
        public string initials { get; set; }
        public int _cvId { get; set; }

    }
}
