﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace USB.Models.FacultyManagement
{
    public class FacultyManagementInfo
    {
        #region Properties

        public static List<SelectListItem> BusinessUnits { get; set; }
        public static List<ManagementLoadCategory> ManagementLoadCategories { get; set; }

        #endregion
    }
}
