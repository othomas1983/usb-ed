﻿namespace USB.Models.FacultyManagement
{
    public class User
    {
        #region Constructors 

        public User(int? cvID, string employeeId, string employeeName, string userName, string password, int? loadedUserCVid =null)
        {
            CVID = cvID;
            EmployeeId = employeeId;
            EmployeeName = employeeName;
            UserName = userName;
            Password = password;
            LoadedUserCVID = loadedUserCVid;
        }

        #endregion

        #region Properties
        
        public int? CVID { get; set; }
        public string EmployeeId { get; set; }
        public int? LoadedUserCVID { get; set; }
        public string LoadedUserEmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        #endregion
    }
}
