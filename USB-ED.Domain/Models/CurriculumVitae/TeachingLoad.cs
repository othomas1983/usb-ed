﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.Models.FacultyManagement
{

    public class TeachingLoad
    {
        #region Properties

        public int CVID { get; set; }
        public Nullable<decimal> BaseCredit { get; set; }
        public int isCoTaught { get; set; }
        public string Lecturer { get; set; }
        public string Module { get; set; }
        public string ProgrammeOffering { get; set; }
        public string Programme { get; set; }
        public Nullable<int> SessionCount { get; set; }
        public int StudentCount { get; set; }
        public Nullable<int> TeachingLoadID { get; set; }

        public Nullable<decimal> TotalCredits { get; set; }
        public int Year { get; set; }

        #endregion
    }
}

