﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.Models.FacultyManagement
{
    public class TaskTeamActivity
    {
        public int TaskTeamActivityID { get; set; }
        public string TaskTeamActivityDescription { get; set; }
    }
}
