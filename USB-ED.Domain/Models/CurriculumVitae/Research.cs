﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using USB.Models.FacultyManagement;
using USB_ED.Global;

namespace USB.Models.FacultyManagement
{
    public class Research : BaseFacultyManagement
    {
        #region Properties

        public int ResearchID { get; set; }
        public Nullable<int> ArticleID { get; set; }
        public Nullable<int> ABSRatingStatusID { get; set; }
        public Nullable<int> DHETStatusID { get; set; }
        public Nullable<int> JournalID { get; set; }
        public Nullable<int> LocusID { get; set; }
        public int ResearchCategoryID { get; set; }
        public int ResearchOutputCategoryID { get; set; }

        public string ArticleTitle { get; set; }
        public string Candidate { get; set; }
        public string Description { get; set; }
        public Nullable<int> Issue { get; set; }
        public string PublicationName { get; set; }
        public Nullable<System.DateTime> PublicationDate { get; set; }
        public string SupervisionOrAssessment { get; set; }
        public string Thesis { get; set; }
        public Nullable<int> Volume { get; set; }
        public Nullable<int> Year { get; set; }

        public List<ResearchHistory> ResearchHistory { get; set; }

        #endregion
    }

    public class ResearchHistory
    {
        #region Properties
        public bool IsContibutor { get; set; }
        public int ResearchCVID { get; set; }
        public Nullable<int> ArticleID { get; set; }
        public Nullable<int> ABSRatingStatusID { get; set; }        
        public int CoordinatorID { get; set; }
        public Nullable<int> DHETStatusID { get; set; }
        public Nullable<int> JournalID { get; set; }
        public Nullable<int> LocusID { get; set; }
        public int ResearchCategoryID { get; set; }
        public int ResearchOutputCategoryID { get; set; }
        public Nullable<int> ResearchContributionTypeID { get; set; }
        public string ArticleTitle { get; set; }
        public string ABSRatingStatusDescription { get; set; }
        public string Country { get; set; }
        public List<Contributor> Contributors { get; set; }
        public Nullable<System.Decimal> Credit { get; set; }
        public string Description { get; set; }
        public string DHETStatusDescription { get; set; }
        public string Edition { get; set; }
        public Nullable<int> EndPage { get; set; }
        public string JournalTitle { get; set; }

        private string journalOrPublicationTitle;
        public string JournalOrPublicationTitle
        {
            get
            {
                return PublicationName.IsNotNullOrEmpty() ? PublicationName : ArticleTitle.IsNotNullOrEmpty() ? ArticleTitle : null;
            }
            set { journalOrPublicationTitle = value; }
        }


        public Nullable<int> Issue { get; set; }
        public string LocusDescription { get; set; }
        public Nullable<int> NoOfPages { get; set; }
        public string PublicationName { get; set; }
        public Nullable<System.DateTime> PublicationDate { get; set; }
        public string PublisherName { get; set; }
        public string PublicationCity { get; set; }
        public string ResearchOutputCategoryDescription { get; set; }
        public Nullable<int> StartPage { get; set; }
        public Nullable<int> Volume { get; set; }

        private string allPublicationStandings;
        public string AllPublicationStandings
        {
            get
            {
                return journalOrPublicationTitle + Environment.NewLine + DHETStatusDescription + Environment.NewLine + ABSRatingStatusDescription;
            }
            set { allPublicationStandings = value; }
        }

        public string Status => ResolveStatus();

        public string StatusDate => ResolveStatusDate();

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> ResearchStatusDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> ResearchCurrentDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> ResearchSubmittedDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> ResearchAcceptedDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> ResearchPublicationDate { get; set; }
        public Nullable<System.Decimal> ResearchStatusCredit { get; set; }
        public string ResearchStatusDescription { get; set; }
        public int ResearchHistoryID { get; set; }
        public Nullable<int> ResearchID { get; set; }
        public Nullable<int> ResearchStatusID { get; set; }
        public bool isActive { get; set; }
        public bool isPresent { get; set; }
        public string PublicationFile { get; set; }
        public string Note { get; set; }
        
        #endregion

        #region Private Methods

        private string ResolveStatus()
        {
            var state = string.Empty;

            if (ResearchCurrentDate.HasValue)
            {
                state = "Current";
            }

            if (ResearchSubmittedDate.HasValue)
            {
                state = "Submitted";
            }

            if (ResearchAcceptedDate.HasValue)
            {
                state = "Accepted";
            }
            if (ResearchPublicationDate.HasValue)
            {
                state = "Published ";
            }

            return state;
        }
        private string ResolveStatusDate()
        {
            var date = string.Empty;

            if (ResearchCurrentDate.HasValue)
            {
                date = ResearchCurrentDate.Value.ToString("MMM/yyyy");
            }

            if (ResearchSubmittedDate.HasValue)
            {
                date= ResearchSubmittedDate.Value.ToString("MMM/yyyy");
            }

            if (ResearchAcceptedDate.HasValue)
            {
                date = ResearchAcceptedDate.Value.ToString("MMM/yyyy"); 
            }
            if (ResearchPublicationDate.HasValue)
            {
                date = ResearchPublicationDate.Value.ToString("MMM/yyyy"); 
            }

            return date;
        }


        public void UpdateResearchHistory(ResearchHistory researchHistory, int cvId)
        {
            var task = new USB_ED.Tasks.FacultyManagementTasks();
            task.AddOrUpdateResearch(researchHistory, cvId, SessionHelper.LoggedInUser.EmployeeName);
        }

        #endregion
    }


    public class ResearchStatus
    {
        #region Properties

        public int ResearchStatusID { get; set; }
        public string ResearchStatusDescription { get; set; }

        #endregion
    }

    public class ProgrammeParticipationResearch : BaseFacultyManagement
    {
        #region Properties

        public int ResearchID { get; set; }
        public int ResearchCategoryID { get; set; }
        public string Description { get; set; }
        public Nullable<int> Year { get; set; }

        #endregion
    }

    public class LaboratoriesParticipationResearch : BaseFacultyManagement
    {
        #region Properties

        public int ResearchID { get; set; }
        public int ResearchCategoryID { get; set; }
        public string Description { get; set; }
        public Nullable<int> Year { get; set; }

        #endregion
    }

    public class ScientificReviewingResearch : BaseFacultyManagement
    {
        #region Properties

        public int ResearchID { get; set; }
        public int ResearchCategoryID { get; set; }
        public string Description { get; set; }
        public Nullable<int> Year { get; set; }

        #endregion
    }

    public class PhdResearch : BaseFacultyManagement
    {
        #region Properties

        public int ResearchID { get; set; }
        public int ResearchCategoryID { get; set; }
        public string Candidate { get; set; }
        public string SupervisionOrAssessment { get; set; }
        public string Thesis { get; set; }
        public Nullable<int> Year { get; set; }


        #endregion
    }
}