﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using USB_ED.ActiveDirectory;
using USB_ED.Global;


namespace USB.Models.FacultyManagement
{
    public class FacultyManagementUsers
    {
        #region Properties

        public int CVId { get; set; }
        public string EmployeeId { get; set; }
        public int? AffiliationID { get; set; }
        public int ApplicationUserRoleID { get; set; }

        public string Description { get; set; }
        public string DisplayName
        {
            get { return Surname + ", " + Name; }
        }

        public string EmailAddress { get; set; }
        public string GivenName { get; set; }
        public string Initials { get; set; }
        public bool isFaculty { get; set; }
        public string LoadedEmployeeName { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string UserName { get; set; }
        public string VoiceTelephoneNumber { get; set; }
        public string Title { get; set; }
        public string SamAccountName { get; set; }
        #endregion

        #region Public Methods

        public IOrderedEnumerable<FacultyUser> LoadADUsers(string uname, string pwd)
        {
            var users = new USB_ED.ActiveDirectory.Users();
            return users.LoadADUsers(uname, pwd).OrderBy(i => i.Surname);
        }

        public IOrderedEnumerable<FacultyManagementUsers> LoadFacultyManagementUsers()
        {
            var userList = new List<FacultyManagementUsers>();

            var task = new USB_ED.Tasks.FacultyManagementTasks();
            var users = task.LoadAllUsers();
            userList = JsonConvert.DeserializeObject<List<FacultyManagementUsers>>(users);            

            return userList.Where(u => u.isFaculty == true).OrderBy(u => u.Surname);
        }

        public FacultyManagementUsers LoadFacultyManagementUser(string empId)
        {
            var userList = new List<FacultyManagementUsers>();

            var task = new USB_ED.Tasks.FacultyManagementTasks();
            var users = task.LoadAllUsers();
            userList = JsonConvert.DeserializeObject<List<FacultyManagementUsers>>(users);            

            return userList.FirstOrDefault(u => u.EmployeeId == empId);
        }

        public void UpdateIsFaculty(List<FacultyManagementUsers> users)
        {
            var task = new USB_ED.Tasks.FacultyManagementTasks();            
            var facultyUsers = new JavaScriptSerializer().Serialize(users);
            task.UpdateIsFaculty(facultyUsers);            
        }

        public void AddNewUserUpdateIsFaculty(List<FacultyManagementUsers> users)
        {
            var task = new USB_ED.Tasks.FacultyManagementTasks();
            {
                foreach (var item in users)
                {
                    var match = LoadFacultyManagementUser(item.EmployeeId);

                    if (match.IsNull())
                    {
                        var newUser = new JavaScriptSerializer().Serialize(item);
                        task.AddNewUserFromAD(newUser, SessionHelper.LoggedInUser.EmployeeName);
                    }
                }
            }
        }

        #endregion
    }

    public class Users
    {
        #region Static Properties

        public static List<FacultyManagementUsers> Coordinators { get; set; }

        #endregion

    }

    
}
