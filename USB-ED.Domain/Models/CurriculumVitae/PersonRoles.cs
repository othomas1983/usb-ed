﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace USB.Models.FacultyManagement
{
    public class PersonRoles
    {
        #region Properties

        public int PersonRoleID { get; set; }
        public string PersonRoleDescription { get; set; }
        public bool isActive { get; set; }

        #endregion

        #region Public Methods

        public List<PersonRoles> LoadPersonRoles()
        {
            var roleList = new List<PersonRoles>();

            var task = new USB_ED.Tasks.FacultyManagementTasks();
            var personRoles = task.LoadPersonRoles();
            roleList = JsonConvert.DeserializeObject<List<PersonRoles>>(personRoles);

            return roleList;
        }

        #endregion
    }
}