﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.Models.FacultyManagement
{
    public class PerformanceWorkload : BaseFacultyManagement
    {
        public int PerformanceWorkloadID { get; set; }
        public string Notes { get; set; }
        public Nullable<decimal> Percentage { get; set; }
        public Nullable<int> Year { get; set; }
    }
}
