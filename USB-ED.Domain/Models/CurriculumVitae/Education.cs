﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.Models.FacultyManagement
{
    public partial class Education : BaseFacultyManagement
    {
        #region Properties

        public int EducationID { get; set; }
        public string Country { get; set; }
        public string Degree { get; set; }
        public string FieldOfStudy { get; set; }
        public string University { get; set; }
        public Nullable<int> Year { get; set; }

        #endregion
    }
}

