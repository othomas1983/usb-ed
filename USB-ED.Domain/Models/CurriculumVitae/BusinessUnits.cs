﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.Models.FacultyManagement
{
    public class BusinessUnits
    {
        #region Properties

        public int BusinessUnitID { get; set; }
        public string BusinessUnitName { get; set; }

        #endregion
    }
}