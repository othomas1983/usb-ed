﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.Models.FacultyManagement
{
    public class Nationalities
    {
        public int NationalityID { get; set; }
        public string Nationality { get; set; }
    }
}
