﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.Models.FacultyManagement
{
    public class ManagementLoadCategory
    {
        #region Properties

        public int ManagementLoadID { get; set; }
        public int ManagementLoadCategoryID { get; set; }
        public string ManagementLoadDescription { get; set; }
        public int ManagementLoadCredit { get; set; }
        public bool isActive { get; set; }

        #endregion
    }
}