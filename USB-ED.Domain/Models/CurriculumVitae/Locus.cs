﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.Models.FacultyManagement
{
    public class Locus
    {
        #region Properties

        public int LocusID { get; set; }
        public string LocusDescription { get; set; }
        
        #endregion
    }
}
