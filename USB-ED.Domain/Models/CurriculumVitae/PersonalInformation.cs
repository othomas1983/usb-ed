﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using USB_ED.Tasks;

namespace USB.Models.FacultyManagement
{
    public class PersonalInformation
    {
        #region Properties 

        public int CVId { get; set; }
        public string EmployeeId { get; set; }
        public int MainActivityID { get; set; }
        public string AcademicRank { get; set; }
        public int? AffiliationID { get; set; }
        public string Biography { get; set; }
        public string BusinessContactNo { get; set; }
        public string BusinessUnit { get; set; }
        public string EmailAddress { get; set; }
        public string Expertise { get; set; }
        public string Gender { get; set; }
        public string HighestQualification { get; set; }
        public int? HighestQualificationId { get; set; }
        public string HomeInstitution { get; set; }
        public string Initials { get; set; }
        public bool IsNew { get; set; }
        public bool isFaculty { get; set; }
        public string Name { get; set; }
        public string NationalityId { get; set; }
        public string MainActivity { get; set; }
        public string SamAccountName { get; set; }
        public string Surname { get; set; }
        public string UserName { get; set; }
        public string OrganisationalResponsibility { get; set; }
        public string Photo { get; set; }
        public string Position { get; set; }
        public string ResearchInterest { get; set; }
        public string ResearchCurrent { get; set; }
        public string ResearchPlanned { get; set; }
        public string Title { get; set; }
        public bool ValueChanged { get; set; }
        
        #endregion

        #region Public Methods

        public PersonalInformation LoadPersonalInformation(string employeeId)
        {
            var user = new PersonalInformation();

            var task = new USB_ED.Tasks.FacultyManagementTasks();
            var imageTask = new ImageUploadTasks();

            var users = task.LoadPersonalInformation(employeeId);
            user = JsonConvert.DeserializeObject<PersonalInformation>(users);

            var userPhoto = imageTask.LoadImage(employeeId.AsString());

            if (userPhoto.Count > 0)
            {
                var imageStream = Convert.ToBase64String(userPhoto[0].FacultyImage);
                user.Photo = "data:image/jpg;base64," + imageStream;
            }

            return user;
        }

        #endregion
    }
}
