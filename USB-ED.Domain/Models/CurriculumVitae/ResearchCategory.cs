﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.Models.FacultyManagement
{
    public enum ResearchCategoryShortName
    {
        #region Enums

        //IMPORTANT - Enum value must match ResearchCategoryID in database
        ProgrammeParticipation = 1,
        LaboratoriesParticipation = 2,
        ScientificReviewing = 3,
        PhD = 4

        #endregion
    }

    public class ResearchCategory
    {
        #region Properties

        public int ResearchCategoryID { get; set; }
        public string ResearchCategoryDescription { get; set; }
        public string ShortName { get; set; }

        #endregion 
    }
}
