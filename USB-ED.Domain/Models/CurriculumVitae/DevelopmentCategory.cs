﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.Models.FacultyManagement
{   
    public enum DevelopmentCategoryShortName
    {
        //IMPORTANT - Enum value must match ResearchCategoryID in database
        #region Enums

        Membership = 1,
        ContinuingEducation = 2,
        Non_Academic = 3,
        Extracurricular = 4

        #endregion
    }

    public class DevelopmentCategory
    {
        #region Properties

        public int DevelopmentCategoryID { get; set; }
        public string DevelopmentCategoryDescription { get; set; }
        public string ShortName { get; set; }

        #endregion
    }
}