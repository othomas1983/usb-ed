﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.Models.FacultyManagement
{
    public class PeopleInResearch
    {
        public string PersonRoleDescription { get; set; }
        public int PersonRoleID { get; set; }
        public string ArticleTitle { get; set; }
        public int CVID { get; set; }
        public int ResearchID { get; set; }
        public int PeopleinResearchCVId { get; set; }
    }

    public class ResearchPeople
    {
        public static List<PeopleInResearch> People { get; set; }

    }
}
