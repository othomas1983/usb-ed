﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.Models.FacultyManagement
{
    public class GrantsFunding : BaseFacultyManagement
    {
        public int GrantID { get; set; }
        public string Country { get; set; }
        public string Description { get; set; }
        public string GrantFunding { get; set; }
        public int?  Year { get; set; }
    }
}
