﻿namespace USB.Models.FacultyManagement
{
    public class FacultyManagementEditUsers
    {
        public int CvId { get; set; }
        public string Title { get; set; }
        public string Initials { get; set; }
        public string Surname { get; set; }
        public int CategoryPermissionId { get; set; }
        public string CategoryPermission { get; set; }
    }
}