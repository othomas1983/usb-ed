﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.Models.FacultyManagement
{
    public enum PublicationCategoryShortName
    {

        #region Enums

        //IMPORTANT - Enum value must match PublicationCategoryID in database
        Thesis = 1,
        PeerReviewed = 2,
        Books = 3,
        Monograph = 4,
        Chapters = 5,
        BooksCoordination = 6,
        CaseStudies = 7,
        StudyConsultation = 8,
        NonRefereedArticles = 9,
        PeerReviewedProceedings = 10,
        PeerReviewedWithoutProceedings = 11,
        NonRefereedPapersAndCommunications = 12,
        Seminars = 13,
        Conference = 14

        #endregion
    }

    public class PublicationCategory
    {
        #region Properties

        public int PublicationCategoryID { get; set; }
        public string PublicationCategoryDescription { get; set; }
        public string ShortName { get; set; }

        #endregion
    }
}
