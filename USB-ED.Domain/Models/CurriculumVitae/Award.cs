﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.Models.FacultyManagement
{
    public class Award : BaseFacultyManagement
    {
        #region Properties

        public int AwardID { get; set; }
        public string AwardDescription { get; set; }
        public string AwardType { get; set; }
        public string Country { get; set; }
        public Nullable<int> Year { get; set; }
        #endregion
    }
}
