﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eUSB.Models.FacultyManagement
{
    public class FacultyDepartment
    {
        #region Properties

        public int CVID { get; set; }
        public string DepartmentName { get; set; }
        public string DepartmentCategoryName { get; set; }

        #endregion
    }
}