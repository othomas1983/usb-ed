﻿using System;
using System.Collections.Generic;
using System.Linq;
using USB.Models.FacultyManagement;
using USB_ED.Tasks;

namespace USB_ED.Models
{
    public class FacultyManagementDataMapping
    {
        //internal void MapToManagementLoadCategory(FacultyManagement clientData, FacultyManagementService.FacultyManagement serverData, List<ManagementLoadCategory> managementCategory)
        //{
        //    clientData.ManagementLoadCategories = new List<SelectListItem>();

        //    foreach (var item in managementCategory)
        //    {
        //        clientData.ManagementLoadCategories.Add(new SelectListItem { Text = item.ManagementLoadDescription, Value = item.ManagementLoadCredit.AsString() });
        //    }

        //    clientData.ManagementLoadCategory = new List<ManagementLoadCategory>();
        //    if (serverData.ManagementLoads.IsNotNull())
        //    {
        //        foreach (var item in serverData.ManagementLoads)
        //        {
        //            clientData.ManagementLoadCategory.Add(new ManagementLoadCategory()
        //            {
        //                ManagementLoadID = item.ManagementLoadID,
        //                ManagementLoadCategoryID = item.ManagementLoadCategoryID.GetValueOrDefault(),
        //                ManagementLoadDescription = managementCategory.Where(c => c.ManagementLoadCategoryID == item.ManagementLoadCategoryID).FirstOrDefault().ManagementLoadDescription,
        //                ManagementLoadCredit = managementCategory.Where(c => c.ManagementLoadCategoryID == item.ManagementLoadCategoryID).FirstOrDefault().ManagementLoadCredit,
        //                isActive = item.isActive
        //            });
        //        }
        //    }
        //}

        public List<ResearchHistory> MapToResearchHistory(ref Research clientData, List<ResearchHistory> researchHistory, int cvID)
        {
            clientData.ResearchHistory = new List<ResearchHistory>();
            var task = new FacultyManagementTasks();

            var ids = researchHistory.Select(h => h.ResearchID).Distinct();

            //TEMP FIX: remove once Jeanne changed sp_ResearchHistoryRead to return only active users from PeopleInResearch table
            var contributor = task.LoadPeopleInResearch(clientData.ResearchID).Where(c => c.isActive == true).ToList();


            foreach (var researchId in ids)
            {
                clientData.ResearchHistory.Add(new ResearchHistory());

                foreach (var item in researchHistory.Where(r => r.ResearchID == researchId && r.isPresent))
                {
                    var isContributor = item.ResearchCVID != cvID;

                    clientData.ResearchHistory[clientData.ResearchHistory.Count() - 1].IsContibutor = isContributor;
                    clientData.ResearchHistory[clientData.ResearchHistory.Count() - 1].ResearchCVID = item.ResearchCVID;
                    clientData.ResearchHistory[clientData.ResearchHistory.Count() - 1].ArticleID = item.ArticleID;
                    clientData.ResearchHistory[clientData.ResearchHistory.Count() - 1].ArticleTitle = item.ArticleTitle;
                    clientData.ResearchHistory[clientData.ResearchHistory.Count() - 1].Description = item.Description;
                    //clientData.ResearchHistory[clientData.ResearchHistory.Count() - 1].PublicationDate = item.PublicationDate;
                    clientData.ResearchHistory[clientData.ResearchHistory.Count() - 1].PublicationName = item.PublicationName;
                    clientData.ResearchHistory[clientData.ResearchHistory.Count() - 1].ResearchCategoryID = item.ResearchCategoryID;
                    clientData.ResearchHistory[clientData.ResearchHistory.Count() - 1].ResearchOutputCategoryID = item.ResearchOutputCategoryID;
                    clientData.ResearchHistory[clientData.ResearchHistory.Count() - 1].ResearchOutputCategoryDescription = item.ResearchOutputCategoryDescription;
                    clientData.ResearchHistory[clientData.ResearchHistory.Count() - 1].ResearchContributionTypeID = item.ResearchContributionTypeID;
                    clientData.ResearchHistory[clientData.ResearchHistory.Count() - 1].ABSRatingStatusID = item.ABSRatingStatusID;
                    clientData.ResearchHistory[clientData.ResearchHistory.Count() - 1].ABSRatingStatusDescription = item.ABSRatingStatusDescription;
                    clientData.ResearchHistory[clientData.ResearchHistory.Count() - 1].DHETStatusID = item.DHETStatusID;
                    clientData.ResearchHistory[clientData.ResearchHistory.Count() - 1].DHETStatusDescription = item.DHETStatusDescription;
                    clientData.ResearchHistory[clientData.ResearchHistory.Count() - 1].LocusID = item.LocusID;
                    clientData.ResearchHistory[clientData.ResearchHistory.Count() - 1].LocusDescription = item.LocusDescription;
                    clientData.ResearchHistory[clientData.ResearchHistory.Count() - 1].ResearchHistoryID = item.ResearchHistoryID;
                    clientData.ResearchHistory[clientData.ResearchHistory.Count() - 1].ResearchID = item.ResearchID;
                    clientData.ResearchHistory[clientData.ResearchHistory.Count() - 1].CoordinatorID = item.CoordinatorID;
                    clientData.ResearchHistory[clientData.ResearchHistory.Count() - 1].Credit = item.Credit;
                    clientData.ResearchHistory[clientData.ResearchHistory.Count() - 1].Issue = item.Issue;
                    clientData.ResearchHistory[clientData.ResearchHistory.Count() - 1].Volume = item.Volume;
                    clientData.ResearchHistory[clientData.ResearchHistory.Count() - 1].PublisherName = item.PublisherName;
                    clientData.ResearchHistory[clientData.ResearchHistory.Count() - 1].PublicationCity = item.PublicationCity;
                    clientData.ResearchHistory[clientData.ResearchHistory.Count() - 1].Country = item.Country;
                    clientData.ResearchHistory[clientData.ResearchHistory.Count() - 1].NoOfPages = item.NoOfPages;
                    clientData.ResearchHistory[clientData.ResearchHistory.Count() - 1].StartPage = item.StartPage;
                    clientData.ResearchHistory[clientData.ResearchHistory.Count() - 1].EndPage = item.EndPage;
                    clientData.ResearchHistory[clientData.ResearchHistory.Count() - 1].Edition = item.Edition;


                    clientData.ResearchHistory[clientData.ResearchHistory.Count() - 1].JournalID = item.JournalID;
                    clientData.ResearchHistory[clientData.ResearchHistory.Count() - 1].JournalTitle = item.JournalTitle;
                    //ResearchPlannedDate = ResolvePlannedDate(item.ResearchStatusDate, item.ResearchStatusDescription),
                    clientData.ResearchHistory[clientData.ResearchHistory.Count() - 1].ResearchStatusDescription = item.ResearchStatusDescription;
                    clientData.ResearchHistory[clientData.ResearchHistory.Count() - 1].ResearchStatusID = item.ResearchStatusID;
                    clientData.ResearchHistory[clientData.ResearchHistory.Count() - 1].isActive = item.isActive;
                    clientData.ResearchHistory[clientData.ResearchHistory.Count() - 1].Note = item.Note;
                    clientData.ResearchHistory[clientData.ResearchHistory.Count() - 1].isPresent = item.isPresent;

                    //clientData.ResearchHistory[clientData.ResearchHistory.Count() - 1].ResearchStatusCredit = item.ResearchStatusCredit;

                    switch (item.ResearchStatusID)
                    {
                        case 2:
                            clientData.ResearchHistory[clientData.ResearchHistory.Count() - 1].ResearchCurrentDate = ResolveCurrentDate(item.ResearchStatusDate, item.ResearchStatusDescription);
                            break;
                
                        case 3:
                            clientData.ResearchHistory[clientData.ResearchHistory.Count() - 1].ResearchSubmittedDate = ResolveSubmittedDate(item.ResearchStatusDate, item.ResearchStatusDescription);
                            break;
                  
                        case 4:
                            clientData.ResearchHistory[clientData.ResearchHistory.Count() - 1].ResearchAcceptedDate = ResolveAcceptedDate(item.ResearchStatusDate, item.ResearchStatusDescription);
                            break;
               
                        case 5:
                            clientData.ResearchHistory[clientData.ResearchHistory.Count() - 1].ResearchPublicationDate = ResolvePublicationDate(item.ResearchStatusDate, item.ResearchStatusDescription);
                            break;
                    }

                    //if (item.ResearchID != id)
                    //{
                    //    id = item.ResearchID;
                    //    cv.ResearchHistory.Add(new ResearchHistory()
                    //    {
                    //        Description = item.Description,
                    //        ResearchHistoryID = item.ResearchHistoryID,
                    //        ResearchID = item.ResearchID,
                    //        ResearchPlannedDate = ResolvePlannedDate(item.ResearchStatusDate, item.ResearchStatusDescription),
                    //        ResearchStatusDescription = item.ResearchStatusDescription,
                    //        ResearchStatusID = item.ResearchStatusID,
                    //        isActive = item.isActive,
                    //        isPresent = item.isPresent
                    //    });
                    //}
                    //else
                    //{
                    //    if (item.ResearchStatusID == 2)
                    //    {
                    //        cv.ResearchHistory[cv.ResearchHistory.Count() - 1].ResearchCurrentDate = ResolveCurrentDate(item.ResearchStatusDate, item.ResearchStatusDescription);
                    //    }
                    //    if (item.ResearchStatusID == 3)
                    //    {
                    //        cv.ResearchHistory[cv.ResearchHistory.Count() - 1].ResearchSubmittedDate = ResolveSubmittedDate(item.ResearchStatusDate, item.ResearchStatusDescription);
                    //    }
                    //    if (item.ResearchStatusID == 4)
                    //    {
                    //        cv.ResearchHistory[cv.ResearchHistory.Count() - 1].ResearchAcceptedDate = ResolveAcceptedDate(item.ResearchStatusDate, item.ResearchStatusDescription);
                    //    }
                    //}
                }
            }
            return clientData.ResearchHistory;
        }

        public void MapToResearchOutputCategories(FacultyManagement clientData, List<ResearchOutputCategory> researchOutputCategoriesList)
        {
            clientData.ResearchOutputCategories = new List<ResearchOutputCategory>();

            foreach (var item in researchOutputCategoriesList)
            {
                clientData.ResearchOutputCategories.Add(new ResearchOutputCategory()
                {
                    ResearchOutputCategoryDescription = item.ResearchOutputCategoryDescription,
                    ResearchOutputCategoryID = item.ResearchOutputCategoryID
                });
            }
        }

        public void MapToABSRating(FacultyManagement clientData, List<ABSRating> absRatingList)
        {
            clientData.ABSRatings = new List<ABSRating>();

            foreach (var item in absRatingList)
            {
                clientData.ABSRatings.Add(new ABSRating()
                {
                    ABSRatingStatusDescription = item.ABSRatingStatusDescription,
                    ABSRatingStatusID = item.ABSRatingStatusID
                });
            }
        }

        public void MapToDHETStatus(FacultyManagement clientData, List<DHETStatus> dhetStatusList)
        {
            clientData.DHETStatuses = new List<DHETStatus>();

            foreach (var item in dhetStatusList)
            {
                clientData.DHETStatuses.Add(new DHETStatus()
                {
                    DHETStatusDescription = item.DHETStatusDescription,
                    DHETStatusID = item.DHETStatusID
                });
            }
        }

        public void MapToLocus(FacultyManagement clientData, List<Locus> locusList)
        {
            clientData.Locuses = new List<Locus>();

            foreach (var item in locusList)
            {
                clientData.Locuses.Add(new Locus()
                {
                    LocusDescription = item.LocusDescription,
                    LocusID = item.LocusID
                });
            }
        }

        public void MapToProgramme(FacultyManagement clientData, List<Programmes> programmeList)
        {
            clientData.Programmes = new List<Programmes>();

            foreach (var item in programmeList.OrderBy(p => p.Programme))
            {
                clientData.Programmes.Add(new Programmes()
                {
                    Programme = item.Programme,
                    ProgrammeID = item.ProgrammeID,
                    ProgrammeOfferingCount = item.ProgrammeOfferingCount
                });
            }
        }

        public void MapToAffiliation(FacultyManagement clientData, List<Affiliation> affiliationsList)
        {
            clientData.Affiliations = new List<Affiliation>();

            foreach (var item in affiliationsList)
            {
                clientData.Affiliations.Add(new Affiliation()
                {
                    AffiliationDescription = item.AffiliationDescription,
                    AffiliationID = item.AffiliationID
                });
            }
        }

        public void MapToBusinessUnits(FacultyManagement clientData, List<BusinessUnits> businessUnitsList)
        {
            clientData.BusinessUnits = new List<BusinessUnits>();

            foreach (var item in businessUnitsList)
            {
                clientData.BusinessUnits.Add(new BusinessUnits()
                {
                    BusinessUnitName = item.BusinessUnitName,
                    BusinessUnitID = item.BusinessUnitID
                });
            }
        }



        //internal void MapToResearchPeople(List<Contributor> peopleInHistoryList)
        //{
        //    ResearchPeople.People = new List<Contributor>();

        //    foreach (var item in peopleInHistoryList)
        //    {
        //        ResearchPeople.People.Add(new Contributor()
        //        {
        //            ArticleTitle = item.ArticleTitle,
        //            CVID = item.CVID,
        //            PeopleinResearchCVID = item.PeopleinResearchCVID,
        //            PersonRoleDescription = item.PersonRoleDescription,
        //            PersonRoleID = item.PersonRoleID,
        //            ResearchID = item.ResearchID
        //        });
        //    }
        //}

        internal void MapToJournals(FacultyManagement facultyManagement, List<FacultyManagementJournals> journal)
        {
            Journal.Journals = new List<FacultyManagementJournals>();

            foreach (var item in journal.OrderBy(j => j.JournalTitle))
            {
                Journal.Journals.Add(new FacultyManagementJournals
                {
                    isActive = true,
                    JournalID = item.JournalID,
                    JournalISSN = item.JournalISSN,
                    JournalRating = item.JournalRating,
                    JournalStatus = item.JournalStatus,
                    JournalTitle = item.JournalTitle,
                    Credit = item.Credit
                });
            }
        }

        internal void MapToTeachingLoad(List<TeachingLoad> teachingLoadList)
        {

        }

        internal void MapToFacultyManagementUsers(List<FacultyManagementUsers> userList)
        {
            Users.Coordinators = new List<FacultyManagementUsers>();

            foreach (var item in userList)
            {
                Users.Coordinators.Add(new FacultyManagementUsers()
                {
                    AffiliationID = item.AffiliationID,
                    CVId = item.CVId,
                    //DisplayName = item.DisplayName,
                    EmployeeId = item.EmployeeId,
                    Initials = item.Initials,
                    Name = item.Name,
                    Surname = item.Surname,
                    Title = item.Title,
                    UserName = item.UserName
                });
            }
        }

        internal void MapMainActivities(FacultyManagement clientData, List<MainActivities> mainActivities)
        {
            clientData.MainActivities = new List<MainActivities>();

            foreach (var item in mainActivities)
            {
                clientData.MainActivities.Add(new MainActivities()
                {
                    MainActivityID = item.MainActivityID,
                    MainActivity = item.MainActivity
                });
            }
        }

        internal void MapAcademicRanks(FacultyManagement clientData, List<AcademicRank> academicRank)
        {
            clientData.AcademicRanks = new List<AcademicRank>();

            foreach (var item in academicRank)
            {
                clientData.AcademicRanks.Add(new AcademicRank()
                {
                    AcademicRankID = item.AcademicRankID,
                    AcademicRankDescription = item.AcademicRankDescription
                });
            }
        }


        public FacultyManagement MapToClientData(FacultyManagement clientData, FacultyManagement serverData)
        {
            var cv = clientData;

            #region FacultyDepartment
            cv.FacultyDepartment = new FacultyDepartment();
            cv.FacultyDepartment.FacultyDepartmentID = serverData.FacultyDepartment.FacultyDepartmentID;
            cv.FacultyDepartment.CVId = serverData.FacultyDepartment.CVId;
            cv.FacultyDepartment.DepartmentCategoryName = serverData.FacultyDepartment.DepartmentCategoryName;
            cv.FacultyDepartment.DepartmentName = serverData.FacultyDepartment.DepartmentName;
            cv.FacultyDepartment.DepartmentCategoryID = serverData.FacultyDepartment.DepartmentCategoryID;
            cv.FacultyDepartment.DepartmentID = serverData.FacultyDepartment.DepartmentID;
            #endregion

            #region Language

            cv.Languages = new List<Language>();
            foreach (var item in serverData.Languages)
            {
                cv.Languages.Add(new Language()
                {
                    isActive = item.isActive,
                    LanguageID = item.LanguageID,
                    LanguageName = item.LanguageName
                });
            }

            #endregion

            #region CourseDesing

            cv.CourseDesigns = new List<CourseDesign>();
            foreach (var item in serverData.CourseDesigns)
            {
                cv.CourseDesigns.Add(new CourseDesign()
                {
                    ContactTime = item.ContactTime,
                    CourseDesignID = item.CourseDesignID,
                    CVId = item.CVId,
                    isActive = item.isActive,
                    Module = item.Module,
                    ProgrammeID = item.ProgrammeID,
                    TotalCredit = item.TotalCredit
                });
            }

            #endregion

            #region Education

            cv.Educations = new List<Education>();
            foreach (var item in serverData.Educations)
            {
                cv.Educations.Add(new Education()
                {
                    Country = item.Country,
                    CVId = item.CVId,
                    Degree = item.Degree,
                    EducationID = item.EducationID,
                    FieldOfStudy = item.FieldOfStudy,
                    isActive = item.isActive,
                    IsNew = item.IsNew,
                    University = item.University,
                    Year = item.Year
                });
            }

            #endregion

            #region Award

            cv.Awards = new List<Award>();
            foreach (var item in serverData.Awards)
            {
                cv.Awards.Add(new Award()
                {
                    AwardID = item.AwardID,
                    AwardDescription = item.AwardDescription,
                    AwardType = item.AwardType,
                    Country = item.Country,
                    CVId = item.CVId,
                    isActive = item.isActive,
                    IsNew = item.IsNew,
                    Year = item.Year
                });
            }

            #endregion

            #region FacultyLanguage

            cv.FacultyLanguages = new List<FacultyLanguage>();
            foreach (var item in serverData.FacultyLanguages)
            {
                cv.FacultyLanguages.Add(new FacultyLanguage()
                {
                    CVId = item.CVId,
                    FacultyLanguageID = item.FacultyLanguageID,
                    isActive = item.isActive,
                    IsNew = item.IsNew,
                    LanguageName = item.LanguageName,
                    LanguageID = item.LanguageID
                });
            }

            #endregion


            #region Experience

            cv.TeachingExperience = MapToTeachingExperience(serverData.Experiences.Where(c => c.ExperienceCategoryID == (int)ExperienceCategoryShortName.Teaching));

            cv.ExecutiveEducationExperience = MapToTExecutiveEducationExperience(serverData.Experiences.Where(c => c.ExperienceCategoryID == (int)ExperienceCategoryShortName.ExecutiveEducation));
            cv.PartTimeConsultingExperience = MapToPartTimeConsultingExperience(serverData.Experiences.Where(c => c.ExperienceCategoryID == (int)ExperienceCategoryShortName.PartTime));
            cv.FullTimeConsultingExperience = MapToFullTimeConsultingExperience(serverData.Experiences.Where(c => c.ExperienceCategoryID == (int)ExperienceCategoryShortName.FullTime));
            cv.PermanentProfessionalCareerExperience = MapToPermanentProfessionalCareerExperience(serverData.Experiences.Where(c => c.ExperienceCategoryID == (int)ExperienceCategoryShortName.PermanentProfCareer));

            #endregion

            #region Research
            cv.ProgrammeParticipationResearch = MapToProgrammeParticipationResearch(serverData.Researches.Where(r => r.ResearchCategoryID == (int)ResearchCategoryShortName.ProgrammeParticipation));
            cv.LaboratoriesParticipationResearch = MapToLaboratoriesParticipationResearch(serverData.Researches.Where(r => r.ResearchCategoryID == (int)ResearchCategoryShortName.LaboratoriesParticipation));
            cv.ScientificReviewingResearch = MapToScientificReviewingResearch(serverData.Researches.Where(r => r.ResearchCategoryID == (int)ResearchCategoryShortName.ScientificReviewing));
            cv.PhdResearch = MapToPhdResearch(serverData.Researches.Where(r => r.ResearchCategoryID == (int)ResearchCategoryShortName.PhD));


            #endregion

            #region Publication

            //cv.ThesisPublication = MapToThesisPublication(serverData.Publications.Where(r => r.PublicationCategoryID == (int)PublicationCategoryShortName.Thesis));
            //cv.PeerReviewedPublication = MapToPeerReviewedPublication(serverData.Publications.Where(r => r.PublicationCategoryID == (int)PublicationCategoryShortName.PeerReviewed));
            //cv.BooksPublication = MapToBooksPublication(serverData.Publications.Where(r => r.PublicationCategoryID == (int)PublicationCategoryShortName.Books));
            //cv.MonographPublication = MapToMonographPublication(serverData.Publications.Where(r => r.PublicationCategoryID == (int)PublicationCategoryShortName.Monograph));
            //cv.ChaptersPublication = MapToChaptersPublication(serverData.Publications.Where(r => r.PublicationCategoryID == (int)PublicationCategoryShortName.Chapters));
            //cv.BooksCoordinationPublication = MapToBooksCoordinationPublication(serverData.Publications.Where(r => r.PublicationCategoryID == (int)PublicationCategoryShortName.BooksCoordination));
            //cv.CaseStudiesPublication = MapToCaseStudiesPublication(serverData.Publications.Where(r => r.PublicationCategoryID == (int)PublicationCategoryShortName.CaseStudies));
            //cv.StudyConsultationPublication = MapToStudyConsultationPublication(serverData.Publications.Where(r => r.PublicationCategoryID == (int)PublicationCategoryShortName.StudyConsultation));
            //cv.NonRefereedArticlesPublication = MapToNonRefereedArticlesPublication(serverData.Publications.Where(r => r.PublicationCategoryID == (int)PublicationCategoryShortName.NonRefereedArticles));
            //cv.PeerReviewedProceedingsPublication = MapToPeerReviewedProceedingsPublication(serverData.Publications.Where(r => r.PublicationCategoryID == (int)PublicationCategoryShortName.PeerReviewedProceedings));
            //cv.PeerReviewedWithoutProceedingsPublication = MapToPeerReviewedWithoutProceedingsPublication(serverData.Publications.Where(r => r.PublicationCategoryID == (int)PublicationCategoryShortName.PeerReviewedWithoutProceedings));
            //cv.NonRefereedPapersAndCommunicationsPublication = MapToNonRefereedPapersAndCommunicationsPublication(serverData.Publications.Where(r => r.PublicationCategoryID == (int)PublicationCategoryShortName.NonRefereedPapersAndCommunications));
            //cv.SeminarsPublication = MapToSeminarsPublication(serverData.Publications.Where(r => r.PublicationCategoryID == (int)PublicationCategoryShortName.Seminars));
            //cv.ConferencePublication = MapToConferencePublication(serverData.Publications.Where(r => r.PublicationCategoryID == (int)PublicationCategoryShortName.Conference));

            #endregion

            #region Development

            //cv.MembershipDevelopment = MapToMembershipDevelopment(serverData.Developments.Where(r => r.DevelopmentCategoryID == (int)DevelopmentCategoryShortName.Membership));
            //cv.ContinuingEducationDevelopment = MapToContinuingEducationDevelopment(serverData.Developments.Where(r => r.DevelopmentCategoryID == (int)DevelopmentCategoryShortName.ContinuingEducation));
            //cv.Non_AcademicDevelopment = MapToNon_AcademicDevelopment(serverData.Developments.Where(r => r.DevelopmentCategoryID == (int)DevelopmentCategoryShortName.Non_Academic));
            //cv.ExtracurricularDevelopment = MapToExtracurricularDevelopment(serverData.Developments.Where(r => r.DevelopmentCategoryID == (int)DevelopmentCategoryShortName.Extracurricular));

            #endregion

            #region ManagementLoad
            //cv.ProgrammeHead = MapToProgrammeHead(serverData.ManagementLoads.Where(m => m.ManagementLoadCategoryID == (int)ManagementLoadCategoryShortName.ProgrammeHead));
            //cv.CentreHead = MapToCentreHead(serverData.ManagementLoads.Where(m => m.ManagementLoadCategoryID == (int)ManagementLoadCategoryShortName.CentreHead));
            //cv.QuestionareAdvisory = MapToQuestionareAdvisory(serverData.ManagementLoads.Where(m => m.ManagementLoadCategoryID == (int)ManagementLoadCategoryShortName.QuestionnaireAdvisory));
            //cv.OrganisingAConference = MapToOrganisingAConference(serverData.ManagementLoads.Where(m => m.ManagementLoadCategoryID == (int)ManagementLoadCategoryShortName.OrganisingConference));
            //cv.AccreditationReports = MapToAccreditationReports(serverData.ManagementLoads.Where(m => m.ManagementLoadCategoryID == (int)ManagementLoadCategoryShortName.AccreditationReports));
            //cv.LeadershipOneOnOne = MapToLeadershipOneOnOne(serverData.ManagementLoads.Where(m => m.ManagementLoadCategoryID == (int)ManagementLoadCategoryShortName.LeadershipOneOnOne));
            //cv.ResearchReportOneOnOne = MapToResearchReportOneOnOne(serverData.ManagementLoads.Where(m => m.ManagementLoadCategoryID == (int)ManagementLoadCategoryShortName.ResearchReportOneOnOne));
            //cv.ManagementLoadOther = MapToManagementLoadOther(serverData.ManagementLoads.Where(m => m.ManagementLoadCategoryID == (int)ManagementLoadCategoryShortName.Other));

            #endregion


            //if (cv.TeachingLoad.IsNotNull())
            //{
            //    cv.TeachingSessionsPerProgramme = MapToTeachingSessionsPerProgramme(cv.TeachingLoad.Where(r => r.TeachingLoadID);
            //}

            return cv;
        }

        public FacultyManagement MapFromClientData(FacultyManagement model)
        {
            if (model.Educations.IsNotNull())
            {
                model.Educations = MapEducations(model.Educations, model.CVID);
            }

            if (model.Awards.IsNotNull())
            {
                model.Awards = MapAwards(model.Awards, model.CVID);
            }


            #region Teaching

            var experienceList = new List<Experience>();

            if (model.TeachingExperience.IsNotNull())
            {
                model.Experiences = MapTeachingExperiences(experienceList, model.TeachingExperience, model.CVID);
            }

            if (model.ExecutiveEducationExperience.IsNotNull())
            {
                model.Experiences = MapExecutiveEducationExperience(experienceList, model.ExecutiveEducationExperience, model.CVID);
            }

            if (model.FullTimeConsultingExperience.IsNotNull())
            {
                model.Experiences = MapFullTimeConsultingExperience(experienceList, model.FullTimeConsultingExperience, model.CVID);
            }

            if (model.PartTimeConsultingExperience.IsNotNull())
            {
                model.Experiences = MapPartTimeConsultingExperience(experienceList, model.PartTimeConsultingExperience, model.CVID);
            }

            if (model.PermanentProfessionalCareerExperience.IsNotNull())
            {
                model.Experiences = MapPermanentProfessionalCareerExperience(experienceList, model.PermanentProfessionalCareerExperience, model.CVID);
            }

            #endregion

            #region ResearchHistory

            //var researchHistoryList = new List<USBEdServiceReference.Research>

            #endregion

            #region Research

            //var researchList = new List<Research>();

            //if (model.LaboratoriesParticipationResearch.IsNotNull())
            //{
            //    model.Researches = MapLaboratoriesParticipationResearch(researchList, model.LaboratoriesParticipationResearch, model.CVID);
            //}

            //if (model.ProgrammeParticipationResearch.IsNotNull())
            //{
            //    model.Researches = MapProgrammeParticipationResearch(researchList, model.ProgrammeParticipationResearch, model.CVID);
            //}

            //if (model.ScientificReviewingResearch.IsNotNull())
            //{
            //    model.Researches = MapScientificReviewingResearch(researchList, model.ScientificReviewingResearch, model.CVID);
            //}

            //if (model.PhdResearch.IsNotNull())
            //{
            //    model.Researches = MapPhdResearch(researchList, model.PhdResearch, model.CVID);
            //}

            #endregion

            #region Publications

            var publicationList = new List<Publication>();

            if (model.ThesisPublication.IsNotNull())
            {
                model.Publications = MapThesisPublication(publicationList, model.ThesisPublication, model.CVID);
            }

            if (model.PeerReviewedPublication.IsNotNull())
            {
                model.Publications = MapPeerReviewedPublication(publicationList, model.PeerReviewedPublication, model.CVID);
            }

            if (model.BooksPublication.IsNotNull())
            {
                model.Publications = MapBooksPublication(publicationList, model.BooksPublication, model.CVID);
            }

            if (model.MonographPublication.IsNotNull())
            {
                model.Publications = MapMonographPublication(publicationList, model.MonographPublication, model.CVID);
            }

            if (model.ChaptersPublication.IsNotNull())
            {
                model.Publications = MapChaptersPublication(publicationList, model.ChaptersPublication, model.CVID);
            }

            if (model.BooksCoordinationPublication.IsNotNull())
            {
                model.Publications = MapBooksCoordinationPublication(publicationList, model.BooksCoordinationPublication, model.CVID);
            }

            if (model.CaseStudiesPublication.IsNotNull())
            {
                model.Publications = MapCaseStudiesPublication(publicationList, model.CaseStudiesPublication, model.CVID);
            }

            if (model.StudyConsultationPublication.IsNotNull())
            {
                model.Publications = MapStudyConsultationPublication(publicationList, model.StudyConsultationPublication, model.CVID);
            }

            if (model.NonRefereedArticlesPublication.IsNotNull())
            {
                model.Publications = MapNonRefereedArticlesPublication(publicationList, model.NonRefereedArticlesPublication, model.CVID);
            }

            if (model.PeerReviewedProceedingsPublication.IsNotNull())
            {
                model.Publications = MapPeerReviewedProceedingsPublication(publicationList, model.PeerReviewedProceedingsPublication, model.CVID);
            }

            if (model.PeerReviewedWithoutProceedingsPublication.IsNotNull())
            {
                model.Publications = MapPeerReviewedWithoutProceedingsPublication(publicationList, model.PeerReviewedWithoutProceedingsPublication, model.CVID);
            }

            if (model.NonRefereedPapersAndCommunicationsPublication.IsNotNull())
            {
                model.Publications = MapNonRefereedPapersAndCommunicationsPublication(publicationList, model.NonRefereedPapersAndCommunicationsPublication, model.CVID);
            }

            if (model.SeminarsPublication.IsNotNull())
            {
                model.Publications = MapSeminarsPublication(publicationList, model.SeminarsPublication, model.CVID);
            }

            if (model.ConferencePublication.IsNotNull())
            {
                model.Publications = MapConferencePublication(publicationList, model.ConferencePublication, model.CVID);
            }

            #endregion

            #region Developments

            var developmentList = new List<Development>();
            //if (model.MembershipDevelopment.IsNotNull())
            //{
            //    model.Developments = MapMembershipDevelopment(developmentList, model.MembershipDevelopment, model.CVID);
            //}

            //if (model.ContinuingEducationDevelopment.IsNotNull())
            //{
            //    model.Developments = MapContinuingEducationDevelopment(developmentList, model.ContinuingEducationDevelopment, model.CVID);
            //}

            //if (model.Non_AcademicDevelopment.IsNotNull())
            //{
            //    model.Developments = MapNon_AcademicDevelopment(developmentList, model.Non_AcademicDevelopment, model.CVID);
            //}

            //if (model.ExtracurricularDevelopment.IsNotNull())
            //{
            //    model.Developments = MapExtracurricularDevelopment(developmentList, model.ExtracurricularDevelopment, model.CVID);
            //}

            #endregion

            #region ManagementLoad

            var managementLoadList = new List<ManagementLoad>();
            if (model.ProgrammeHead.IsNotNull())
            {
                model.ManagementLoads = MapProgrammeHead(managementLoadList, model.ProgrammeHead, model.CVID);
            }

            if (model.CentreHead.IsNotNull())
            {
                model.ManagementLoads = MapCentreHead(managementLoadList, model.CentreHead, model.CVID);
            }

            if (model.QuestionareAdvisory.IsNotNull())
            {
                model.ManagementLoads = MapQuestionareAdvisory(managementLoadList, model.QuestionareAdvisory, model.CVID);
            }

            if (model.OrganisingAConference.IsNotNull())
            {
                model.ManagementLoads = MapOrganisingAConference(managementLoadList, model.OrganisingAConference, model.CVID);
            }

            if (model.AccreditationReports.IsNotNull())
            {
                model.ManagementLoads = MapAccreditationReports(managementLoadList, model.AccreditationReports, model.CVID);
            }

            if (model.LeadershipOneOnOne.IsNotNull())
            {
                model.ManagementLoads = MapLeadershipOneOnOne(managementLoadList, model.LeadershipOneOnOne, model.CVID);
            }
            if (model.ResearchReportOneOnOne.IsNotNull())
            {
                model.ManagementLoads = MapResearchReportOneOnOne(managementLoadList, model.ResearchReportOneOnOne, model.CVID);
            }
            if (model.ManagementLoadOther.IsNotNull())
            {
                model.ManagementLoads = MapManagementLoadOther(managementLoadList, model.ManagementLoadOther, model.CVID);
            }

            //MapResearchOther
            #endregion

            //model.ManagementLoads = MapManagementLoads(model.ManagementLoadCategories,
            //    model.ManagementLoadCategory, model.ManagementLoads, model.CVID);


            return model;
        }





        public PersonalInformation MapPersonalInformation(FacultyManagement model)
        {
            var personalInfo = new PersonalInformation();

            personalInfo.CVId = model.CVID;
            personalInfo.AcademicRank = model.AcademicRank;
            personalInfo.AffiliationID = model.AffiliationID;
            personalInfo.Biography = model.Biography;
            personalInfo.BusinessContactNo = model.BusinessContactNo;
            personalInfo.BusinessUnit = model.BusinessUnit;
            personalInfo.EmailAddress = model.EmailAddress;
            personalInfo.EmployeeId = model.EmployeeId;
            personalInfo.Expertise = model.Expertise;
            personalInfo.HighestQualification = model.HighestQualification;
            personalInfo.HighestQualificationId = model.HighestQualificationID;
            personalInfo.HomeInstitution = model.HomeInstitution;
            personalInfo.Initials = model.Initials;
            personalInfo.isFaculty = model.isFaculty.GetValueOrDefault(true);
            personalInfo.MainActivity = model.MainActivity;
            personalInfo.MainActivityID = model.MainActivityID;
            personalInfo.Name = model.Name;
            personalInfo.Position = model.Position;
            personalInfo.ResearchInterest = model.ResearchInterest;
            personalInfo.ResearchCurrent = model.ResearchCurrent;
            personalInfo.ResearchPlanned = model.ResearchPlanned;
            personalInfo.OrganisationalResponsibility = model.OrganisationalResponsibility;
            personalInfo.Surname = model.Surname;
            personalInfo.Title = model.Title;
            personalInfo.UserName = model.UserName;

            return personalInfo;
        }

        #region Public Methods - change to methods to public

        #region Experience Mappers

        public List<TeachingExperience> MapToTeachingExperience(IEnumerable<Experience> experienceList)
        {
            var teachingExperience = new List<TeachingExperience>();
            foreach (var item in experienceList)
            {
                teachingExperience.Add(new TeachingExperience()
                {
                    City = item.City,
                    Country = item.Country,
                    EndDate = item.EndDate,
                    ExperienceCategoryID = item.ExperienceCategoryID,
                    ExperienceID = item.ExperienceID,
                    Institution = item.Institution,
                    IsCurrent = item.IsCurrent.GetValueOrDefault(),
                    Position = item.Position,
                    StartDate = item.StartDate,
                    SubjectArea = item.SubjectArea,
                    isActive = item.isActive
                });
            }

            return teachingExperience;
        }

        public List<PermanentProfessionalCareerExperience> MapToPermanentProfessionalCareerExperience(IEnumerable<Experience> experienceList)
        {
            var experience = new List<PermanentProfessionalCareerExperience>();

            //if (experienceList.Count() == 0)
            //{
            //    experience.Add(new PermanentProfessionalCareerExperience());
            //}

            foreach (var item in experienceList)
            {
                experience.Add(new PermanentProfessionalCareerExperience()
                {
                    Country = item.Country,
                    CVId = item.CVId,
                    EndDate = item.EndDate,
                    ExperienceCategoryID = item.ExperienceCategoryID,
                    ExperienceID = item.ExperienceID,
                    Institution = item.Institution,
                    IsCurrent = item.IsCurrent.GetValueOrDefault(),
                    Position = item.Position,
                    StartDate = item.StartDate,
                    isActive = item.isActive
                });
            }

            return experience;
        }

        public List<EngagementWithBusinessAndSocietyExperience> MapToEngagementWithBusinessAndSociety(IEnumerable<Experience> experienceList)
        {
            return experienceList.Select(item => new EngagementWithBusinessAndSocietyExperience()
            {
                Country = item.Country,
                CVId = item.CVId,
                EndDate = item.EndDate,
                ExperienceCategoryID = item.ExperienceCategoryID,
                ExperienceID = item.ExperienceID,
                Organisation = item.Institution,
                IsCurrent = item.IsCurrent.GetValueOrDefault(),
                StartDate = item.StartDate,
                isActive = item.isActive,
                SubjectArea = item.SubjectArea,
                City = item.City,
                Topic = item.KnowledgeArea,
                TypeOfEngagement = item.SubjectArea,
            }).ToList();
        }
        public List<EditorialBoardmembersExperience> MapToEditorialBoardmembers(IEnumerable<Experience> experienceList)
        {
            return experienceList.Select(item => new EditorialBoardmembersExperience()
            {
                Publication = item.Publication,
                CVId = item.CVId,
                EndDate = item.EndDate,
                ExperienceCategoryID = item.ExperienceCategoryID,
                ExperienceID = item.ExperienceID,
                PublicationEdition = item.PublicationEdition,
                IsCurrent = item.IsCurrent,
                StartDate = item.StartDate,
                isActive = item.isActive,
                PublicationStatus = item.PublicationStatus,
                Institution = item.Institution,
                TypeOfEngagement = item.SubjectArea,
            }).ToList();
        }

        public List<FullTimeConsultingExperience> MapToFullTimeConsultingExperience(IEnumerable<Experience> experienceList)
        {
            var experience = new List<FullTimeConsultingExperience>();

            //if (experienceList.Count() == 0)
            //{
            //    experience.Add(new FullTimeConsultingExperience());
            //}

            foreach (var item in experienceList)
            {
                experience.Add(new FullTimeConsultingExperience()
                {
                    Client = item.Client,
                    CVId = item.CVId,
                    EndDate = item.EndDate,
                    ExperienceCategoryID = item.ExperienceID,
                    ExperienceID = item.ExperienceID,
                    IsCurrent = item.IsCurrent.GetValueOrDefault(),
                    KnowledgeArea = item.KnowledgeArea,
                    StartDate = item.StartDate,
                    isActive = item.isActive
                });
            }

            return experience;
        }

        public List<PartTimeConsultingExperience> MapToPartTimeConsultingExperience(IEnumerable<Experience> experienceList)
        {
            var experience = new List<PartTimeConsultingExperience>();

            //if (experienceList.Count() == 0)
            //{
            //    experience.Add(new PartTimeConsultingExperience());
            //}

            foreach (var item in experienceList)
            {
                experience.Add(new PartTimeConsultingExperience()
                {
                    Client = item.Client,
                    CVId = item.CVId,
                    EndDate = item.EndDate,
                    ExperienceCategoryID = item.ExperienceID,
                    ExperienceID = item.ExperienceID,
                    IsCurrent = item.IsCurrent.GetValueOrDefault(),
                    NoOfDays = item.NoOfDays,
                    StartDate = item.StartDate,
                    SubjectArea = item.SubjectArea,
                    isActive = item.isActive
                });
            }

            return experience;
        }

        public List<ExecutiveEducationExperience> MapToTExecutiveEducationExperience(IEnumerable<Experience> experienceList)
        {
            var experience = new List<ExecutiveEducationExperience>();

            //if (experienceList.Count() == 0)
            //{
            //    experience.Add(new ExecutiveEducationExperience());
            //}

            foreach (var item in experienceList)
            {
                experience.Add(new ExecutiveEducationExperience()
                {
                    CVId = item.CVId,
                    Country = item.Country,
                    EndDate = item.EndDate,
                    ExperienceCategoryID = item.ExperienceID,
                    ExperienceID = item.ExperienceID,
                    IsCurrent = item.IsCurrent.GetValueOrDefault(),
                    NoOfDays = item.NoOfDays,
                    Programme = item.Programme,
                    StartDate = item.StartDate,
                    SubjectArea = item.SubjectArea,
                    isActive = item.isActive
                });
            }

            return experience;
        }

        public List<Experience> MapTeachingExperiences(List<Experience> experienceService, List<TeachingExperience> experienceModel, int cvId)
        {
            if (experienceModel.IsNotNull())
            {
                foreach (var item in experienceModel)
                {
                    experienceService.Add(new Experience()
                    {
                        City = item.City,
                        Country = item.Country,
                        CVId = cvId,
                        EndDate = item.EndDate,
                        ExperienceCategoryID = (int)ExperienceCategoryShortName.Teaching, //item.ExperienceCategoryID,
                        ExperienceID = item.ExperienceID,
                        Institution = item.Institution,
                        NoOfDays = item.NoOfDays,
                        Programme = item.Programme,
                        IsCurrent = item.IsCurrent,
                        Position = item.Position,
                        StartDate = item.StartDate,
                        SubjectArea = item.SubjectArea,
                        isActive = item.isActive
                    });
                }
            }
            return experienceService;
        }

        internal List<Experience> MapToEngagementWithBusinessAndSociety(List<Experience> experienceList, List<EngagementWithBusinessAndSocietyExperience> engagementWithBusiness, int cVId)
        {
            if (engagementWithBusiness.IsNotNull())
            {
                foreach (var item in engagementWithBusiness)
                {
                    experienceList.Add(new Experience()
                    {
                        City = item.City,
                        Country = item.Country,
                        CVId = cVId,
                        EndDate = item.EndDate,
                        ExperienceCategoryID = (int)ExperienceCategoryShortName.EngagementWithBusinessAndSociety,
                        ExperienceID = item.ExperienceID,
                        IsCurrent = true,
                        StartDate = item.StartDate,
                        SubjectArea = item.SubjectArea,
                        isActive = item.isActive,
                        TypeOfEngagement = item.TypeOfEngagement,
                        Institution = item.Organisation,
                        KnowledgeArea = item.Topic
                    });
                }
            }
            return experienceList;
        }

        public List<Experience> MapExecutiveEducationExperience(List<Experience> experienceService, List<ExecutiveEducationExperience> experienceModel, int cvId)
        {
            if (experienceModel.IsNotNull())
            {
                foreach (var item in experienceModel)
                {
                    experienceService.Add(new Experience()
                    {
                        CVId = cvId,
                        Country = item.Country,
                        EndDate = item.EndDate,
                        ExperienceCategoryID = (int)ExperienceCategoryShortName.ExecutiveEducation,// item.ExperienceID,
                        ExperienceID = item.ExperienceID,
                        IsCurrent = item.IsCurrent,
                        NoOfDays = item.NoOfDays,
                        Programme = item.Programme,
                        StartDate = item.StartDate,
                        SubjectArea = item.SubjectArea,
                        isActive = item.isActive
                    });
                }
            }
            return experienceService;
        }

        public List<Experience> MapFullTimeConsultingExperience(List<Experience> experienceService, List<FullTimeConsultingExperience> experienceModel, int cvId)
        {
            if (experienceModel.IsNotNull())
            {
                foreach (var item in experienceModel)
                {
                    experienceService.Add(new Experience()
                    {
                        Client = item.Client,
                        CVId = cvId,
                        EndDate = item.EndDate,
                        ExperienceCategoryID = (int)ExperienceCategoryShortName.FullTime,
                        ExperienceID = item.ExperienceID,
                        IsCurrent = item.IsCurrent,
                        KnowledgeArea = item.KnowledgeArea,
                        StartDate = item.StartDate,
                        isActive = item.isActive
                    });
                }
            }
            return experienceService;
        }

        public List<Experience> MapPartTimeConsultingExperience(List<Experience> experienceService, List<PartTimeConsultingExperience> experienceModel, int cvId)
        {
            if (experienceModel.IsNotNull())
            {
                foreach (var item in experienceModel)
                {
                    experienceService.Add(new Experience()
                    {
                        Client = item.Client,
                        CVId = cvId,
                        EndDate = item.EndDate,
                        ExperienceCategoryID = (int)ExperienceCategoryShortName.PartTime,
                        ExperienceID = item.ExperienceID,
                        IsCurrent = item.IsCurrent,
                        NoOfDays = item.NoOfDays,
                        StartDate = item.StartDate,
                        SubjectArea = item.SubjectArea,
                        isActive = item.isActive
                    });
                }
            }
            return experienceService;
        }

        public List<Experience> MapPermanentProfessionalCareerExperience(List<Experience> experienceService, List<PermanentProfessionalCareerExperience> experienceModel, int cvId)
        {
            if (experienceModel.IsNotNull())
            {
                foreach (var item in experienceModel)
                {
                    experienceService.Add(new Experience()
                    {
                        Country = item.Country,
                        CVId = cvId,
                        EndDate = item.EndDate,
                        ExperienceCategoryID = (int)ExperienceCategoryShortName.PermanentProfCareer, //item.ExperienceCategoryID,
                        ExperienceID = item.ExperienceID,
                        Institution = item.Institution,
                        IsCurrent = item.IsCurrent,
                        Position = item.Position,
                        StartDate = item.StartDate,
                        isActive = item.isActive
                    });
                }
            }
            return experienceService;
        }

        internal List<Experience> MapToEditorialBoardmembers(List<Experience> experienceList, List<EditorialBoardmembersExperience> editorialBoardmembers, int cVId)
        {
            if (editorialBoardmembers.IsNotNull())
            {
                experienceList.AddRange(editorialBoardmembers.Select(item => new Experience
                {
                    CVId = cVId,
                    ExperienceCategoryID = (int)ExperienceCategoryShortName.EditorialBoardmembers,
                    ExperienceID = item.ExperienceID,
                    StartDate = item.StartDate,
                    EndDate = item.EndDate,
                    SubjectArea = item.TypeOfEngagement,
                    Position = null,
                    Institution = item.Institution,
                    City = null,
                    Country = null,
                    IsCurrent = item.IsCurrent,
                    NoOfDays = null,
                    Programme = null,
                    KnowledgeArea = null,
                    Client = null,
                    CreatedBy = null,
                    ModifiedBy = null,
                    Publication = item.Publication,
                    PublicationStatus = item.PublicationStatus,
                    PublicationEdition = item.PublicationEdition,
                    isActive = item.isActive
                }));
            }
            return experienceList;
        }

        #endregion

        #region Research Mappers

        private DateTime? ResolvePlannedDate(DateTime? statusDate, string statusDescription)
        {
            if (statusDescription == "Planned")
            {
                return statusDate;
            }
            return null;
        }

        private DateTime? ResolveCurrentDate(DateTime? statusDate, string statusDescription)
        {
            if (statusDescription == "Current")
            {
                return statusDate;
            }
            return null;
        }

        private DateTime? ResolveSubmittedDate(DateTime? statusDate, string statusDescription)
        {
            if (statusDescription == "Submitted")
            {
                return statusDate;
            }
            return null;
        }

        private DateTime? ResolveAcceptedDate(DateTime? statusDate, string statusDescription)
        {
            if (statusDescription == "Accepted")
            {
                return statusDate;
            }
            return null;
        }

        private DateTime? ResolvePublicationDate(DateTime? statusDate, string statusDescription)
        {
            if (statusDescription == "Published")
            {
                return statusDate;
            }
            return null;
        }

        private List<ProgrammeParticipationResearch> MapToProgrammeParticipationResearch(IEnumerable<Research> researchList)
        {
            var research = new List<ProgrammeParticipationResearch>();
            foreach (var item in researchList)
            {
                research.Add(new ProgrammeParticipationResearch()
                {
                    CVId = item.CVId,
                    Description = item.Description,
                    ResearchCategoryID = item.ResearchCategoryID,
                    ResearchID = item.ResearchID,
                    Year = item.Year,
                    isActive = item.isActive,
                    IsNew = item.IsNew
                });
            }

            return research;
        }

        private List<LaboratoriesParticipationResearch> MapToLaboratoriesParticipationResearch(IEnumerable<Research> researchList)
        {
            var research = new List<LaboratoriesParticipationResearch>();

            foreach (var item in researchList)
            {
                research.Add(new LaboratoriesParticipationResearch()
                {
                    CVId = item.CVId,
                    Description = item.Description,
                    ResearchCategoryID = item.ResearchCategoryID,
                    ResearchID = item.ResearchID,
                    Year = item.Year,
                    isActive = item.isActive,
                    IsNew = item.IsNew
                });
            }

            return research;
        }

        private List<ScientificReviewingResearch> MapToScientificReviewingResearch(IEnumerable<Research> researchList)
        {
            var research = new List<ScientificReviewingResearch>();

            foreach (var item in researchList)
            {
                research.Add(new ScientificReviewingResearch()
                {
                    CVId = item.CVId,
                    Description = item.Description,
                    ResearchCategoryID = item.ResearchCategoryID,
                    ResearchID = item.ResearchID,
                    Year = item.Year,
                    isActive = item.isActive,
                    IsNew = item.IsNew
                });
            }

            return research;
        }

        private List<PhdResearch> MapToPhdResearch(IEnumerable<Research> researchList)
        {
            var research = new List<PhdResearch>();

            foreach (var item in researchList)
            {
                research.Add(new PhdResearch()
                {
                    CVId = item.CVId,
                    Candidate = item.Candidate,
                    ResearchCategoryID = item.ResearchCategoryID,
                    ResearchID = item.ResearchID,
                    SupervisionOrAssessment = item.SupervisionOrAssessment,
                    Thesis = item.Thesis,
                    Year = item.Year,
                    isActive = item.isActive,
                    IsNew = item.IsNew
                });
            }

            return research;
        }

        private List<Research> MapProgrammeParticipationResearch(List<Research> researchService, List<ProgrammeParticipationResearch> researchModel, int cvId)
        {
            if (researchModel.IsNotNull())
            {
                foreach (var item in researchModel)
                {
                    researchService.Add(new Research()
                    {
                        CVId = cvId,
                        Description = item.Description,
                        ResearchCategoryID = (int)ResearchCategoryShortName.ProgrammeParticipation,
                        ResearchID = item.ResearchID,
                        Year = item.Year,
                        isActive = item.isActive,
                    });
                }
            }

            return researchService;
        }

        private List<Research> MapLaboratoriesParticipationResearch(List<Research> researchService, List<LaboratoriesParticipationResearch> researchModel, int cvId)
        {
            if (researchModel.IsNotNull())
            {
                foreach (var item in researchModel)
                {
                    researchService.Add(new Research()
                    {
                        CVId = cvId,
                        Description = item.Description,
                        ResearchCategoryID = (int)ResearchCategoryShortName.LaboratoriesParticipation,
                        ResearchID = item.ResearchID,
                        Year = item.Year,
                        isActive = item.isActive
                    });
                }
            }

            return researchService;
        }

        private List<Research> MapScientificReviewingResearch(List<Research> researchService, List<ScientificReviewingResearch> researchModel, int cvId)
        {
            if (researchModel.IsNotNull())
            {
                foreach (var item in researchModel)
                {
                    researchService.Add(new Research()
                    {
                        CVId = cvId,
                        Description = item.Description,
                        ResearchCategoryID = (int)ResearchCategoryShortName.ScientificReviewing,
                        ResearchID = item.ResearchID,
                        Year = item.Year,
                        isActive = item.isActive
                    });
                }
            }

            return researchService;
        }

        private List<Research> MapPhdResearch(List<Research> researchService, List<PhdResearch> researchModel, int cvId)
        {
            if (researchModel.IsNotNull())
            {
                foreach (var item in researchModel)
                {
                    researchService.Add(new Research()
                    {
                        CVId = cvId,
                        ResearchCategoryID = (int)ResearchCategoryShortName.PhD,
                        ResearchID = item.ResearchID,
                        Thesis = item.Thesis,
                        SupervisionOrAssessment = item.SupervisionOrAssessment,
                        Candidate = item.Candidate,
                        Year = item.Year,
                        isActive = item.isActive
                    });
                }
            }

            return researchService;
        }

        #endregion

        #region Publication Mappers

        private List<ThesisPublication> MapToThesisPublication(IEnumerable<Publication> publicationList)
        {
            var publication = new List<ThesisPublication>();

            if (publicationList.Count() == 0)
            {
                publication.Add(new ThesisPublication());
            }

            foreach (var item in publicationList)
            {
                publication.Add(new ThesisPublication()
                {
                    CVId = item.CVId,
                    Date = item.Date,
                    Institution = item.Institution,
                    Promoter = item.Promoter,
                    PublicationCategoryID = item.PublicationCategoryID,
                    PublicationID = item.PublicationID,
                    PublicationTitle = item.PublicationTitle,
                    SubjectArea = item.SubjectArea,
                    isActive = item.isActive
                });
            }

            return publication;
        }

        private List<PeerReviewedPublication> MapToPeerReviewedPublication(IEnumerable<Publication> publicationList)
        {
            var publication = new List<PeerReviewedPublication>();

            if (publicationList.Count() == 0)
            {
                publication.Add(new PeerReviewedPublication());
            }

            foreach (var item in publicationList)
            {
                publication.Add(new PeerReviewedPublication()
                {
                    CoAuthorName = item.CoAuthorName,
                    CVId = item.CVId,
                    Date = item.Date,
                    EndPage = item.EndPage,
                    PublicationCategoryID = item.PublicationCategoryID,
                    PublicationID = item.PublicationID,
                    PublicationTitle = item.PublicationTitle,
                    ResearchType = item.ResearchType,
                    StartPage = item.StartPage,
                    Title = item.Title,
                    VolumeNumber = item.VolumeNumber,
                    isActive = item.isActive
                });
            }

            return publication;
        }

        private List<BooksPublication> MapToBooksPublication(IEnumerable<Publication> publicationList)
        {
            var publication = new List<BooksPublication>();

            if (publicationList.Count() == 0)
            {
                publication.Add(new BooksPublication());
            }

            foreach (var item in publicationList)
            {
                publication.Add(new BooksPublication()
                {
                    CoAuthorName = item.CoAuthorName,
                    CVId = item.CVId,
                    NoOfPages = item.NoOfPages,
                    PublicationCategoryID = item.PublicationCategoryID,
                    PublicationCity = item.PublicationCity,
                    PublicationID = item.PublicationID,
                    PublicationTitle = item.PublicationTitle,
                    PublisherName = item.PublisherName,
                    ResearchType = item.ResearchType,
                    Year = item.Year,
                    isActive = item.isActive
                });
            }

            return publication;
        }

        private List<MonographPublication> MapToMonographPublication(IEnumerable<Publication> publicationList)
        {
            var publication = new List<MonographPublication>();

            if (publicationList.Count() == 0)
            {
                publication.Add(new MonographPublication());
            }

            foreach (var item in publicationList)
            {
                publication.Add(new MonographPublication()
                {
                    Author = item.Author,
                    CoAuthorName = item.CoAuthorName,
                    CVId = item.CVId,
                    NoOfPages = item.NoOfPages,
                    PublicationCategoryID = item.PublicationCategoryID,
                    PublicationCity = item.PublicationCity,
                    PublicationID = item.PublicationID,
                    PublicationTitle = item.PublicationTitle,
                    PublisherName = item.PublisherName,
                    ResearchType = item.ResearchType,
                    Year = item.Year,
                    isActive = item.isActive
                });
            }

            return publication;
        }

        private List<ChaptersPublication> MapToChaptersPublication(IEnumerable<Publication> publicationList)
        {
            var publication = new List<ChaptersPublication>();

            if (publicationList.Count() == 0)
            {
                publication.Add(new ChaptersPublication());
            }

            foreach (var item in publicationList)
            {
                publication.Add(new ChaptersPublication()
                {
                    BookTitle = item.BookTitle,
                    CoAuthorName = item.CoAuthorName,
                    CVId = item.CVId,
                    Editor = item.Editor,
                    EndPage = item.EndPage,
                    PublicationCategoryID = item.PublicationCategoryID,
                    PublicationCity = item.PublicationCity,
                    PublicationID = item.PublicationID,
                    PublicationTitle = item.PublicationTitle,
                    PublisherName = item.PublisherName,
                    ResearchType = item.ResearchType,
                    StartPage = item.StartPage,
                    Year = item.Year,
                    isActive = item.isActive
                });
            }

            return publication;
        }

        private List<BooksCoordinationPublication> MapToBooksCoordinationPublication(IEnumerable<Publication> publicationList)
        {
            var publication = new List<BooksCoordinationPublication>();

            if (publicationList.Count() == 0)
            {
                publication.Add(new BooksCoordinationPublication());
            }

            foreach (var item in publicationList)
            {
                publication.Add(new BooksCoordinationPublication()
                {
                    BookTitle = item.BookTitle,
                    CoAuthorName = item.CoOrganiser,
                    CVId = item.CVId,
                    NoOfPages = item.NoOfPages,
                    PublicationCategoryID = item.PublicationCategoryID,
                    PublicationCity = item.PublicationCity,
                    PublicationID = item.PublicationID,
                    PublisherName = item.PublisherName,
                    ResearchType = item.ResearchType,
                    Year = item.Year,
                    isActive = item.isActive
                });
            }

            return publication;
        }

        private List<CaseStudiesPublication> MapToCaseStudiesPublication(IEnumerable<Publication> publicationList)
        {
            var publication = new List<CaseStudiesPublication>();

            if (publicationList.Count() == 0)
            {
                publication.Add(new CaseStudiesPublication());
            }

            foreach (var item in publicationList)
            {
                publication.Add(new CaseStudiesPublication()
                {
                    CoAuthorName = item.CoAuthorName,
                    CVId = item.CVId,
                    PublicationCategoryID = item.PublicationCategoryID,
                    PublicationID = item.PublicationID,
                    PublicationTitle = item.PublicationTitle,
                    PublisherName = item.PublisherName,
                    Year = item.Year,
                    isActive = item.isActive
                });
            }

            return publication;
        }

        private List<StudyConsultationPublication> MapToStudyConsultationPublication(IEnumerable<Publication> publicationList)
        {
            var publication = new List<StudyConsultationPublication>();

            if (publicationList.Count() == 0)
            {
                publication.Add(new StudyConsultationPublication());
            }

            foreach (var item in publicationList)
            {
                publication.Add(new StudyConsultationPublication()
                {
                    CoAuthorName = item.CoAuthorName,
                    CVId = item.CVId,
                    PublicationCategoryID = item.PublicationCategoryID,
                    PublicationID = item.PublicationID,
                    PublicationTitle = item.PublicationTitle,
                    Year = item.Year,
                    isActive = item.isActive
                });
            }

            return publication;
        }

        private List<NonRefereedArticlesPublication> MapToNonRefereedArticlesPublication(IEnumerable<Publication> publicationList)
        {
            var publication = new List<NonRefereedArticlesPublication>();

            if (publicationList.Count() == 0)
            {
                publication.Add(new NonRefereedArticlesPublication());
            }

            foreach (var item in publicationList)
            {
                publication.Add(new NonRefereedArticlesPublication()
                {
                    CoAuthorName = item.CoAuthorName,
                    CVId = item.CVId,
                    EndPage = item.EndPage,
                    JournalTitle = item.JournalTitle,
                    PublicationCategoryID = item.PublicationCategoryID,
                    PublicationID = item.PublicationID,
                    PublicationTitle = item.PublicationTitle,
                    ResearchType = item.ResearchType,
                    StartPage = item.StartPage,
                    VolumeNumber = item.VolumeNumber,
                    Year = item.Year,
                    isActive = item.isActive
                });
            }

            return publication;
        }

        private List<PeerReviewedProceedingsPublication> MapToPeerReviewedProceedingsPublication(IEnumerable<Publication> publicationList)
        {
            var publication = new List<PeerReviewedProceedingsPublication>();

            if (publicationList.Count() == 0)
            {
                publication.Add(new PeerReviewedProceedingsPublication());
            }

            foreach (var item in publicationList)
            {
                publication.Add(new PeerReviewedProceedingsPublication()
                {
                    CoAuthorName = item.CoAuthorName,
                    ConferenceTitle = item.ConferenceTitle,
                    Country = item.Country,
                    CVId = item.CVId,
                    EndDate = item.EndDate,
                    Location = item.Location,
                    PublicationCategoryID = item.PublicationCategoryID,
                    PublicationCity = item.PublicationCity,
                    PublicationID = item.PublicationID,
                    PublicationTitle = item.PublicationTitle,
                    ResearchType = item.ResearchType,
                    StartDate = item.StartDate,
                    isActive = item.isActive
                });
            }

            return publication;
        }

        private List<PeerReviewedWithoutProceedingsPublication> MapToPeerReviewedWithoutProceedingsPublication(IEnumerable<Publication> publicationList)
        {
            var publication = new List<PeerReviewedWithoutProceedingsPublication>();

            if (publicationList.Count() == 0)
            {
                publication.Add(new PeerReviewedWithoutProceedingsPublication());
            }

            foreach (var item in publicationList)
            {
                publication.Add(new PeerReviewedWithoutProceedingsPublication()
                {
                    CoAuthorName = item.CoAuthorName,
                    ConferenceTitle = item.ConferenceTitle,
                    Country = item.Country,
                    CVId = item.CVId,
                    EndDate = item.EndDate,
                    Location = item.Location,
                    PublicationCategoryID = item.PublicationCategoryID,
                    PublicationCity = item.PublicationCity,
                    PublicationID = item.PublicationID,
                    PublicationTitle = item.PublicationTitle,
                    ResearchType = item.ResearchType,
                    StartDate = item.StartDate,
                    isActive = item.isActive
                });
            }

            return publication;
        }

        private List<NonRefereedPapersAndCommunicationsPublication> MapToNonRefereedPapersAndCommunicationsPublication(IEnumerable<Publication> publicationList)
        {
            var publication = new List<NonRefereedPapersAndCommunicationsPublication>();

            if (publicationList.Count() == 0)
            {
                publication.Add(new NonRefereedPapersAndCommunicationsPublication());
            }

            foreach (var item in publicationList)
            {
                publication.Add(new NonRefereedPapersAndCommunicationsPublication()
                {
                    CoAuthorName = item.CoAuthorName,
                    ConferenceTitle = item.ConferenceTitle,
                    Country = item.Country,
                    CVId = item.CVId,
                    EndDate = item.EndDate,
                    Location = item.Location,
                    PublicationCategoryID = item.PublicationCategoryID,
                    PublicationCity = item.PublicationCity,
                    PublicationID = item.PublicationID,
                    PublicationTitle = item.PublicationTitle,
                    ResearchType = item.ResearchType,
                    StartDate = item.StartDate,
                    isActive = item.isActive
                });
            }

            return publication;
        }

        private List<SeminarsPublication> MapToSeminarsPublication(IEnumerable<Publication> publicationList)
        {
            var publication = new List<SeminarsPublication>();

            if (publicationList.Count() == 0)
            {
                publication.Add(new SeminarsPublication());
            }

            foreach (var item in publicationList)
            {
                publication.Add(new SeminarsPublication()
                {
                    CoAuthorName = item.CoAuthorName,
                    Country = item.Country,
                    CVId = item.CVId,
                    EndDate = item.EndDate,
                    PublicationCategoryID = item.PublicationCategoryID,
                    PublicationCity = item.PublicationCity,
                    PublicationID = item.PublicationID,
                    PublicationTitle = item.PublicationTitle,
                    ResearchType = item.ResearchType,
                    SeminarTitle = item.SeminarTitle,
                    StartDate = item.StartDate,
                    isActive = item.isActive
                });
            }

            return publication;
        }

        private List<ConferencePublication> MapToConferencePublication(IEnumerable<Publication> publicationList)
        {
            var publication = new List<ConferencePublication>();

            if (publicationList.Count() == 0)
            {
                publication.Add(new ConferencePublication());
            }

            foreach (var item in publicationList)
            {
                publication.Add(new ConferencePublication()
                {
                    CoAuthorName = item.CoAuthorName,
                    ConferenceTitle = item.ConferenceTitle,
                    Country = item.Country,
                    CVId = item.CVId,
                    EndDate = item.EndDate,
                    Location = item.Location,
                    PublicationCategoryID = item.PublicationCategoryID,
                    PublicationCity = item.PublicationCity,
                    PublicationID = item.PublicationID,
                    ResearchType = item.ResearchType,
                    StartDate = item.StartDate,
                    isActive = item.isActive
                });
            }

            return publication;
        }

        private List<Publication> MapThesisPublication(List<Publication> publicationService, List<ThesisPublication> publicationModel, int cvId)
        {
            if (publicationModel.IsNotNull())
            {
                foreach (var item in publicationModel)
                {
                    publicationService.Add(new Publication()
                    {
                        CVId = cvId,
                        Date = item.Date,
                        Institution = item.Institution,
                        Promoter = item.Promoter,
                        PublicationCategoryID = (int)PublicationCategoryShortName.Thesis,
                        PublicationID = item.PublicationID,
                        PublicationTitle = item.PublicationTitle,
                        SubjectArea = item.SubjectArea,
                        isActive = item.isActive
                    });
                }
            }

            return publicationService;
        }

        private List<Publication> MapPeerReviewedPublication(List<Publication> publicationService, List<PeerReviewedPublication> publicationModel, int cvId)
        {
            if (publicationModel.IsNotNull())
            {
                foreach (var item in publicationModel)
                {
                    publicationService.Add(new Publication()
                    {
                        CoAuthorName = item.CoAuthorName,
                        CVId = cvId,
                        Date = item.Date,
                        EndPage = item.EndPage,
                        PublicationCategoryID = (int)PublicationCategoryShortName.PeerReviewed,
                        PublicationID = item.PublicationID,
                        PublicationTitle = item.PublicationTitle,
                        ResearchType = item.ResearchType,
                        StartPage = item.StartPage,
                        Title = item.Title,
                        VolumeNumber = item.VolumeNumber,
                        isActive = item.isActive
                    });
                }
            }

            return publicationService;
        }

        private List<Publication> MapBooksPublication(List<Publication> publicationService, List<BooksPublication> publicationModel, int cvId)
        {
            if (publicationModel.IsNotNull())
            {
                foreach (var item in publicationModel)
                {
                    publicationService.Add(new Publication()
                    {
                        CoAuthorName = item.CoAuthorName,
                        CVId = cvId,
                        NoOfPages = item.NoOfPages,
                        PublicationCategoryID = (int)PublicationCategoryShortName.Books,
                        PublicationCity = item.PublicationCity,
                        PublicationID = item.PublicationID,
                        PublicationTitle = item.PublicationTitle,
                        PublisherName = item.PublisherName,
                        ResearchType = item.ResearchType,
                        Year = item.Year,
                        isActive = item.isActive
                    });
                }
            }

            return publicationService;
        }

        private List<Publication> MapMonographPublication(List<Publication> publicationService, List<MonographPublication> publicationModel, int cvId)
        {
            if (publicationModel.IsNotNull())
            {
                foreach (var item in publicationModel)
                {
                    publicationService.Add(new Publication()
                    {
                        Author = item.Author,
                        CoAuthorName = item.CoAuthorName,
                        CVId = cvId,
                        NoOfPages = item.NoOfPages,
                        PublicationCategoryID = (int)PublicationCategoryShortName.Monograph,
                        PublicationCity = item.PublicationCity,
                        PublicationID = item.PublicationID,
                        PublicationTitle = item.PublicationTitle,
                        PublisherName = item.PublisherName,
                        ResearchType = item.ResearchType,
                        Year = item.Year,
                        isActive = item.isActive
                    });
                }
            }

            return publicationService;
        }

        private List<Publication> MapChaptersPublication(List<Publication> publicationService, List<ChaptersPublication> publicationModel, int cvId)
        {
            if (publicationModel.IsNotNull())
            {
                foreach (var item in publicationModel)
                {
                    publicationService.Add(new Publication()
                    {
                        BookTitle = item.BookTitle,
                        CoAuthorName = item.CoAuthorName,
                        CVId = cvId,
                        Editor = item.Editor,
                        EndPage = item.EndPage,
                        PublicationCategoryID = (int)PublicationCategoryShortName.Chapters,
                        PublicationCity = item.PublicationCity,
                        PublicationID = item.PublicationID,
                        PublicationTitle = item.PublicationTitle,
                        PublisherName = item.PublisherName,
                        ResearchType = item.ResearchType,
                        StartPage = item.StartPage,
                        Year = item.Year,
                        isActive = item.isActive
                    });
                }
            }

            return publicationService;
        }

        private List<Publication> MapBooksCoordinationPublication(List<Publication> publicationService, List<BooksCoordinationPublication> publicationModel, int cvId)
        {
            if (publicationModel.IsNotNull())
            {
                foreach (var item in publicationModel)
                {
                    publicationService.Add(new Publication()
                    {
                        BookTitle = item.BookTitle,
                        CoAuthorName = item.CoAuthorName,
                        CVId = cvId,
                        NoOfPages = item.NoOfPages,
                        PublicationCategoryID = (int)PublicationCategoryShortName.BooksCoordination,
                        PublicationCity = item.PublicationCity,
                        PublicationID = item.PublicationID,
                        PublisherName = item.PublisherName,
                        ResearchType = item.ResearchType,
                        Year = item.Year,
                        isActive = item.isActive
                    });
                }
            }

            return publicationService;
        }

        private List<Publication> MapCaseStudiesPublication(List<Publication> publicationService, List<CaseStudiesPublication> publicationModel, int cvId)
        {
            if (publicationModel.IsNotNull())
            {
                foreach (var item in publicationModel)
                {
                    publicationService.Add(new Publication()
                    {
                        CoAuthorName = item.CoAuthorName,
                        CVId = cvId,
                        PublicationCategoryID = (int)PublicationCategoryShortName.CaseStudies,
                        PublicationID = item.PublicationID,
                        PublicationTitle = item.PublicationTitle,
                        PublisherName = item.PublisherName,
                        Year = item.Year,
                        isActive = item.isActive
                    });
                }
            }

            return publicationService;
        }

        private List<Publication> MapStudyConsultationPublication(List<Publication> publicationService, List<StudyConsultationPublication> publicationModel, int cvId)
        {
            if (publicationModel.IsNotNull())
            {
                foreach (var item in publicationModel)
                {
                    publicationService.Add(new Publication()
                    {
                        CoAuthorName = item.CoAuthorName,
                        CVId = cvId,
                        PublicationCategoryID = (int)PublicationCategoryShortName.StudyConsultation,
                        PublicationID = item.PublicationID,
                        PublicationTitle = item.PublicationTitle,
                        Year = item.Year,
                        isActive = item.isActive
                    });
                }
            }

            return publicationService;
        }

        private List<Publication> MapNonRefereedArticlesPublication(List<Publication> publicationService, List<NonRefereedArticlesPublication> publicationModel, int cvId)
        {
            if (publicationModel.IsNotNull())
            {
                foreach (var item in publicationModel)
                {
                    publicationService.Add(new Publication()
                    {
                        CoAuthorName = item.CoAuthorName,
                        CVId = cvId,
                        EndPage = item.EndPage,
                        JournalTitle = item.JournalTitle,
                        PublicationCategoryID = (int)PublicationCategoryShortName.NonRefereedArticles,
                        PublicationID = item.PublicationID,
                        PublicationTitle = item.PublicationTitle,
                        ResearchType = item.ResearchType,
                        StartPage = item.StartPage,
                        VolumeNumber = item.VolumeNumber,
                        Year = item.Year,
                        isActive = item.isActive
                    });
                }
            }

            return publicationService;
        }

        private List<Publication> MapPeerReviewedProceedingsPublication(List<Publication> publicationService, List<PeerReviewedProceedingsPublication> publicationModel, int cvId)
        {
            if (publicationModel.IsNotNull())
            {
                foreach (var item in publicationModel)
                {
                    publicationService.Add(new Publication()
                    {
                        CoAuthorName = item.CoAuthorName,
                        ConferenceTitle = item.ConferenceTitle,
                        Country = item.Country,
                        CVId = cvId,
                        EndDate = item.EndDate,
                        Location = item.Location,
                        PublicationCategoryID = (int)PublicationCategoryShortName.PeerReviewedProceedings,
                        PublicationCity = item.PublicationCity,
                        PublicationID = item.PublicationID,
                        PublicationTitle = item.PublicationTitle,
                        ResearchType = item.ResearchType,
                        StartDate = item.StartDate,
                        isActive = item.isActive
                    });
                }
            }

            return publicationService;
        }

        private List<Publication> MapPeerReviewedWithoutProceedingsPublication(List<Publication> publicationService, List<PeerReviewedWithoutProceedingsPublication> publicationModel, int cvId)
        {
            if (publicationModel.IsNotNull())
            {
                foreach (var item in publicationModel)
                {
                    publicationService.Add(new Publication()
                    {
                        CoAuthorName = item.CoAuthorName,
                        ConferenceTitle = item.ConferenceTitle,
                        Country = item.Country,
                        CVId = cvId,
                        EndDate = item.EndDate,
                        Location = item.Location,
                        PublicationCategoryID = (int)PublicationCategoryShortName.PeerReviewedWithoutProceedings,
                        PublicationCity = item.PublicationCity,
                        PublicationID = item.PublicationID,
                        PublicationTitle = item.PublicationTitle,
                        ResearchType = item.ResearchType,
                        StartDate = item.StartDate,
                        isActive = item.isActive
                    });
                }
            }

            return publicationService;
        }

        private List<Publication> MapNonRefereedPapersAndCommunicationsPublication(List<Publication> publicationService, List<NonRefereedPapersAndCommunicationsPublication> publicationModel, int cvId)
        {
            if (publicationModel.IsNotNull())
            {
                foreach (var item in publicationModel)
                {
                    publicationService.Add(new Publication()
                    {
                        CoAuthorName = item.CoAuthorName,
                        ConferenceTitle = item.ConferenceTitle,
                        Country = item.Country,
                        CVId = cvId,
                        EndDate = item.EndDate,
                        Location = item.Location,
                        PublicationCategoryID = (int)PublicationCategoryShortName.NonRefereedPapersAndCommunications,
                        PublicationCity = item.PublicationCity,
                        PublicationID = item.PublicationID,
                        PublicationTitle = item.PublicationTitle,
                        ResearchType = item.ResearchType,
                        StartDate = item.StartDate,
                        isActive = item.isActive
                    });
                }
            }

            return publicationService;
        }

        private List<Publication> MapSeminarsPublication(List<Publication> publicationService, List<SeminarsPublication> publicationModel, int cvId)
        {
            if (publicationModel.IsNotNull())
            {
                foreach (var item in publicationModel)
                {
                    publicationService.Add(new Publication()
                    {
                        CoAuthorName = item.CoAuthorName,
                        Country = item.Country,
                        CVId = cvId,
                        EndDate = item.EndDate,
                        PublicationCategoryID = (int)PublicationCategoryShortName.Seminars,
                        PublicationCity = item.PublicationCity,
                        PublicationID = item.PublicationID,
                        PublicationTitle = item.PublicationTitle,
                        ResearchType = item.ResearchType,
                        SeminarTitle = item.SeminarTitle,
                        StartDate = item.StartDate,
                        isActive = item.isActive
                    });
                }
            }

            return publicationService;
        }

        private List<Publication> MapConferencePublication(List<Publication> publicationService, List<ConferencePublication> publicationModel, int cvId)
        {
            if (publicationModel.IsNotNull())
            {
                foreach (var item in publicationModel)
                {
                    publicationService.Add(new Publication()
                    {
                        CoAuthorName = item.CoAuthorName,
                        ConferenceTitle = item.ConferenceTitle,
                        Country = item.Country,
                        CVId = cvId,
                        EndDate = item.EndDate,
                        Location = item.Location,
                        PublicationCategoryID = (int)PublicationCategoryShortName.Conference,
                        PublicationCity = item.PublicationCity,
                        PublicationID = item.PublicationID,
                        ResearchType = item.ResearchType,
                        StartDate = item.StartDate,
                        isActive = item.isActive
                    });
                }
            }

            return publicationService;
        }

        #endregion

        #region Development Mappers

        public List<Development> MapMembershipDevelopment(List<Development> developmentService, List<MembershipDevelopment> developmentModel, int cvId)
        {
            if (developmentModel.IsNotNull())
            {
                foreach (var item in developmentModel)
                {
                    developmentService.Add(new Development()
                    {
                        CVId = cvId,
                        Accomplishment = item.Accomplishment,
                        DevelopmentID = item.DevelopmentID,
                        DevelopmentCategoryID = (int)DevelopmentCategoryShortName.Membership,
                        isActive = item.isActive,
                        EndDate = string.IsNullOrEmpty(item.TerminationDate)? default(DateTime?) : DateTime.Parse(item.TerminationDate),
                        StartDate = string.IsNullOrEmpty(item.DateJoined) ? default(DateTime?) : DateTime.Parse(item.DateJoined),
                        IsCurrent = item.IsCurrent
                    });
                }
            }

            return developmentService;
        }

        public List<Development> MapContinuingEducationDevelopment(List<Development> developmentService, List<ContinuingEducationDevelopment> developmentModel, int cvId)
        {
            if (developmentModel.IsNotNull())
            {
                foreach (var item in developmentModel)
                {
                    developmentService.Add(new Development()
                    {
                        CVId = cvId,
                        DevelopmentID = item.DevelopmentID,
                        DevelopmentCategoryID = (int)DevelopmentCategoryShortName.ContinuingEducation,
                        Country = item.Country,
                        EndDate = item.EndDate,
                        Institution = item.Institution,
                        isActive = item.isActive,
                        IsCurrent = item.IsCurrent,
                        NoOfDays = item.NoOfDays,
                        StartDate = item.StartDate,
                        Topic = item.Topic
                    });
                }
            }

            return developmentService;
        }

        public List<Development> MapNon_AcademicDevelopment(List<Development> developmentService, List<Non_AcademicDevelopment> developmentModel, int cvId)
        {
            if (developmentModel.IsNotNull())
            {
                foreach (var item in developmentModel)
                {
                    developmentService.Add(new Development()
                    {
                        CVId = cvId,
                        DevelopmentID = item.DevelopmentID,
                        DevelopmentCategoryID = (int)DevelopmentCategoryShortName.Non_Academic,
                        Accomplishment = item.Accomplishment,
                        Country = item.Country,
                        EndDate = item.EndDate,
                        Institution = item.Institution,
                        isActive = item.isActive,
                        IsCurrent = item.IsCurrent,
                        NoOfDays = item.NoOfDays,
                        StartDate = item.StartDate
                    });
                }
            }

            return developmentService;
        }

        public List<Development> MapExtracurricularDevelopment(List<Development> developmentService, List<ExtracurricularDevelopment> developmentModel, int cvId)
        {
            if (developmentModel.IsNotNull())
            {
                foreach (var item in developmentModel)
                {
                    developmentService.Add(new Development()
                    {
                        CVId = cvId,
                        DevelopmentID = item.DevelopmentID,
                        DevelopmentCategoryID = (int)DevelopmentCategoryShortName.Extracurricular,
                        City = item.City,
                        Country = item.Country,
                        EndDate = item.EndDate,
                        isActive = item.isActive,
                        IsCurrent = item.IsCurrent,
                        Organisation = item.Organisation,
                        Position = item.Position,
                        StartDate = item.StartDate
                    });
                }
            }

            return developmentService;
        }

        public List<MembershipDevelopment> MapToMembershipDevelopment(IEnumerable<Development> developmentList)
        {
            var development = new List<MembershipDevelopment>();
            foreach (var item in developmentList)
            {
                development.Add(new MembershipDevelopment()
                {
                    CVId = item.CVId,
                    Accomplishment = item.Accomplishment,
                    DevelopmentID = item.DevelopmentID,
                    DevelopmentCategoryID = item.DevelopmentCategoryID,
                    isActive = item.isActive,
                    DateJoined = item.StartDate?.ToString("yyyy-MM-dd") ?? string.Empty,
                    TerminationDate = item.EndDate?.ToString("yyyy-MM-dd") ?? string.Empty,
                    IsCurrent = item.IsCurrent
                });
            }

            return development;
        }

        public List<ContinuingEducationDevelopment> MapToContinuingEducationDevelopment(IEnumerable<Development> developmentList)
        {
            var development = new List<ContinuingEducationDevelopment>();

            foreach (var item in developmentList)
            {
                development.Add(new ContinuingEducationDevelopment()
                {
                    CVId = item.CVId,
                    DevelopmentID = item.DevelopmentID,
                    DevelopmentCategoryID = item.DevelopmentCategoryID,
                    Country = item.Country,
                    EndDate = item.EndDate,
                    Institution = item.Institution,
                    isActive = item.isActive,
                    IsCurrent = item.IsCurrent,
                    NoOfDays = item.NoOfDays,
                    StartDate = item.StartDate,
                    Topic = item.Topic
                });
            }

            return development;
        }

        public List<Non_AcademicDevelopment> MapToNon_AcademicDevelopment(IEnumerable<Development> developmentList)
        {
            var development = new List<Non_AcademicDevelopment>();

            foreach (var item in developmentList)
            {
                development.Add(new Non_AcademicDevelopment()
                {
                    CVId = item.CVId,
                    DevelopmentID = item.DevelopmentID,
                    DevelopmentCategoryID = item.DevelopmentCategoryID,
                    Accomplishment = item.Accomplishment,
                    Country = item.Country,
                    EndDate = item.EndDate,
                    isActive = item.isActive,
                    Institution = item.Institution,
                    IsCurrent = item.IsCurrent,
                    NoOfDays = item.NoOfDays,
                    StartDate = item.StartDate
                });
            }

            return development;
        }

        public List<ExtracurricularDevelopment> MapToExtracurricularDevelopment(IEnumerable<Development> developmentList)
        {
            var development = new List<ExtracurricularDevelopment>();

            foreach (var item in developmentList)
            {
                development.Add(new ExtracurricularDevelopment()
                {
                    CVId = item.CVId,
                    DevelopmentID = item.DevelopmentID,
                    DevelopmentCategoryID = item.DevelopmentCategoryID,
                    City = item.City,
                    Country = item.Country,
                    EndDate = item.EndDate,
                    isActive = item.isActive,
                    IsCurrent = item.IsCurrent,
                    Organisation = item.Organisation,
                    Position = item.Position,
                    StartDate = item.StartDate
                });
            }

            return development;
        }


        #endregion

        #region ManagementLoad

        public List<ManagementLoad> MapProgrammeHead(List<ManagementLoad> managementLoadList, List<ManagementLoad> list, int CVId)
        {
            if (list.IsNotNull())
            {
                foreach (var item in list)
                {
                    managementLoadList.Add(new ManagementLoad()
                    {
                        CVId = CVId,
                        Credit = item.Credit,
                        isActive = item.isActive,
                        ManagementLoadID = item.ManagementLoadID,
                        ManagementLoadCategoryID = (int)ManagementLoadCategoryShortName.ProgrammeHead,
                        ManagementLoadDescription = item.ManagementLoadDescription,
                        Description = item.Description,
                        ProgrammeID = item.ProgrammeID,
                        ProgrammeOfferingID = item.ProgrammeOfferingID,
                        ElectiveModuleCount = item.ElectiveModuleCount,
                        ElectiveSAQACredit = item.ElectiveSAQACredit,
                        ManagementLoadStartDate = item.ManagementLoadStartDate,
                        ManagementLoadEndDate = item.ManagementLoadEndDate
                    });
                }
            }

            return managementLoadList;
        }

        public List<ManagementLoad> MapCentreHead(List<ManagementLoad> managementLoadList, List<ManagementLoad> list, int CVId)
        {
            if (list.IsNotNull())
            {
                foreach (var item in list)
                {
                    managementLoadList.Add(new ManagementLoad()
                    {
                        CVId = CVId,
                        Credit = item.Credit,
                        isActive = item.isActive,
                        ManagementLoadID = item.ManagementLoadID,
                        ManagementLoadCategoryID = (int)ManagementLoadCategoryShortName.CentreHead,
                        ManagementLoadDescription = item.ManagementLoadDescription,
                        Description = item.Description,
                        ManagementLoadStartDate = item.ManagementLoadStartDate,
                        ManagementLoadEndDate = item.ManagementLoadEndDate
                    });
                }
            }

            return managementLoadList;
        }

        public List<ManagementLoad> MapQuestionareAdvisory(List<ManagementLoad> managementLoadList, List<ManagementLoad> list, int CVId)
        {
            if (list.IsNotNull())
            {
                foreach (var item in list)
                {
                    managementLoadList.Add(new ManagementLoad()
                    {
                        CVId = CVId,
                        Credit = item.Credit,
                        isActive = item.isActive,
                        ManagementLoadID = item.ManagementLoadID,
                        ManagementLoadCategoryID = (int)ManagementLoadCategoryShortName.QuestionnaireAdvisory,
                        ManagementLoadDescription = item.ManagementLoadDescription,
                        Description = item.Description,
                    });
                }
            }

            return managementLoadList;
        }

        public List<ManagementLoad> MapOrganisingAConference(List<ManagementLoad> managementLoadList, List<ManagementLoad> list, int CVId)
        {
            if (list.IsNotNull())
            {
                foreach (var item in list)
                {
                    managementLoadList.Add(new ManagementLoad()
                    {
                        CVId = CVId,
                        Credit = item.Credit,
                        isActive = item.isActive,
                        ManagementLoadID = item.ManagementLoadID,
                        ManagementLoadCategoryID = (int)ManagementLoadCategoryShortName.OrganisingConference,
                        ManagementLoadDescription = item.ManagementLoadDescription,
                        Description = item.Description,
                        ManagementLoadStartDate = item.ManagementLoadStartDate,
                        ManagementLoadEndDate = item.ManagementLoadEndDate
                    });
                }
            }

            return managementLoadList;
        }

        public List<ManagementLoad> MapAccreditationReports(List<ManagementLoad> managementLoadList, List<ManagementLoad> list, int CVId)
        {
            if (list.IsNotNull())
            {
                foreach (var item in list)
                {
                    managementLoadList.Add(new ManagementLoad()
                    {
                        CVId = CVId,
                        Credit = item.Credit,
                        isActive = item.isActive,
                        ManagementLoadID = item.ManagementLoadID,
                        ManagementLoadCategoryID = (int)ManagementLoadCategoryShortName.AccreditationReports,
                        ManagementLoadDescription = item.ManagementLoadDescription,
                        Description = item.Description,
                    });
                }
            }

            return managementLoadList;
        }

        public List<ManagementLoad> MapLeadershipOneOnOne(List<ManagementLoad> managementLoadList, List<ManagementLoad> list, int CVId)
        {
            if (list.IsNotNull())
            {
                foreach (var item in list)
                {
                    managementLoadList.Add(new ManagementLoad()
                    {
                        CVId = CVId,
                        Credit = item.Credit,
                        isActive = item.isActive,
                        ManagementLoadID = item.ManagementLoadID,
                        ManagementLoadCategoryID = (int)ManagementLoadCategoryShortName.LeadershipOneOnOne,
                        ManagementLoadDescription = item.ManagementLoadDescription,
                        Description = item.Description,
                        ManagementLoadStartDate = item.ManagementLoadStartDate,
                        ManagementLoadEndDate = item.ManagementLoadEndDate
                    });
                }
            }

            return managementLoadList;
        }

        public List<ManagementLoad> MapResearchReportOneOnOne(List<ManagementLoad> managementLoadList, List<ManagementLoad> list, int CVId)
        {
            if (list.IsNotNull())
            {
                foreach (var item in list)
                {
                    managementLoadList.Add(new ManagementLoad()
                    {
                        CVId = CVId,
                        Credit = item.Credit,
                        isActive = item.isActive,
                        ManagementLoadID = item.ManagementLoadID,
                        ManagementLoadCategoryID = (int)ManagementLoadCategoryShortName.ResearchReportOneOnOne,
                        ManagementLoadDescription = item.ManagementLoadDescription,
                        Description = item.Description,
                    });
                }
            }

            return managementLoadList;
        }

        public List<ManagementLoad> MapChairOfCommittee(List<ManagementLoad> managementLoadList, List<ManagementLoad> list, int CVId)
        {
            if (list.IsNotNull())
            {
                foreach (var item in list)
                {
                    managementLoadList.Add(new ManagementLoad()
                    {
                        CVId = CVId,
                        Credit = item.Credit,
                        isActive = item.isActive,
                        ManagementLoadID = item.ManagementLoadID,
                        ManagementLoadCategoryID = (int)ManagementLoadCategoryShortName.ChairOfAdministrativeManagementCommittee,
                        ManagementLoadDescription = item.ManagementLoadDescription,
                        Description = item.Description,
                        ManagementLoadStartDate = item.ManagementLoadStartDate,
                        ManagementLoadEndDate = item.ManagementLoadEndDate
                    });
                }
            }

            return managementLoadList;
        }

        public List<ManagementLoad> MapTaskTeamMember(List<ManagementLoad> managementLoadList, List<ManagementLoad> list, int CVId)
        {
            if (list.IsNotNull())
            {
                foreach (var item in list)
                {
                    managementLoadList.Add(new ManagementLoad()
                    {
                        CVId = CVId,
                        Credit = item.Credit,
                        isActive = item.isActive,
                        ManagementLoadID = item.ManagementLoadID,
                        ManagementLoadCategoryID = (int)ManagementLoadCategoryShortName.TaskTeamMember,
                        ManagementLoadDescription = item.ManagementLoadDescription,
                        Description = item.Description,
                        ManagementLoadStartDate = item.ManagementLoadStartDate,
                        ManagementLoadEndDate = item.ManagementLoadEndDate,
                        TaskTeamActivityID = item.TaskTeamActivityID
                    });
                }
            }

            return managementLoadList;
        }


        public List<ManagementLoad> MapManagementLoadOther(List<ManagementLoad> managementLoadList, List<ManagementLoad> list, int CVId)
        {
            if (list.IsNotNull())
            {
                foreach (var item in list)
                {
                    managementLoadList.Add(new ManagementLoad()
                    {
                        CVId = CVId,
                        Credit = item.Credit,
                        isActive = item.isActive,
                        ManagementLoadID = item.ManagementLoadID,
                        ManagementLoadCategoryID = (int)ManagementLoadCategoryShortName.Other,
                        ManagementLoadDescription = item.ManagementLoadDescription,
                        Description = item.Description,
                        ManagementLoadStartDate = item.ManagementLoadStartDate,
                        ManagementLoadEndDate = item.ManagementLoadEndDate
                    });
                }
            }

            return managementLoadList;
        }

        public List<ManagementLoad> MapToProgrammeHead(IEnumerable<ManagementLoad> managementLoadList)
        {
            var list = new List<ManagementLoad>();

            foreach (var item in managementLoadList)
            {
                list.Add(new ManagementLoad()
                {
                    CVId = item.CVId,
                    ModuleCount = item.ModuleCount,
                    ProgrammeID = item.ProgrammeID,
                    ProgrammeOfferingID = item.ProgrammeOfferingID,
                    Credit = item.Credit,
                    isActive = item.isActive,
                    ManagementLoadID = item.ManagementLoadID,
                    Description = item.Description,
                    ElectiveModuleCount = item.ElectiveModuleCount,
                    ElectiveSAQACredit = item.ElectiveSAQACredit,
                    ManagementLoadStartDate = item.ManagementLoadStartDate,
                    ManagementLoadEndDate = item.ManagementLoadEndDate
                });
            }

            return list;
        }

        public List<ManagementLoad> MapToCentreHead(IEnumerable<ManagementLoad> managementLoadList)
        {
            var list = new List<ManagementLoad>();

            foreach (var item in managementLoadList)
            {
                list.Add(new ManagementLoad()
                {
                    CVId = item.CVId,
                    Credit = item.Credit,
                    ManagementLoadID = item.ManagementLoadID,
                    ManagementLoadCategoryID = item.ManagementLoadCategoryID,
                    ManagementLoadDescription = item.ManagementLoadDescription,
                    Description = item.Description,
                    isActive = item.isActive,
                    ManagementLoadStartDate = item.ManagementLoadStartDate,
                    ManagementLoadEndDate = item.ManagementLoadEndDate
                });
            }

            return list;
        }

        public List<ManagementLoad> MapToQuestionareAdvisory(IEnumerable<ManagementLoad> managementLoadList)
        {
            var list = new List<ManagementLoad>();

            foreach (var item in managementLoadList)
            {
                list.Add(new ManagementLoad()
                {
                    CVId = item.CVId,
                    Credit = item.Credit,
                    ManagementLoadID = item.ManagementLoadID,
                    ManagementLoadCategoryID = item.ManagementLoadCategoryID,
                    ManagementLoadDescription = item.ManagementLoadDescription,
                    Description = item.Description,
                    isActive = item.isActive
                });
            }

            return list;
        }

        public List<ManagementLoad> MapToOrganisingAConference(IEnumerable<ManagementLoad> managementLoadList)
        {
            var list = new List<ManagementLoad>();

            foreach (var item in managementLoadList)
            {
                list.Add(new ManagementLoad()
                {
                    CVId = item.CVId,
                    Credit = item.Credit,
                    ManagementLoadID = item.ManagementLoadID,
                    ManagementLoadCategoryID = item.ManagementLoadCategoryID,
                    ManagementLoadDescription = item.ManagementLoadDescription,
                    Description = item.Description,
                    isActive = item.isActive,
                    ManagementLoadStartDate = item.ManagementLoadStartDate,
                    ManagementLoadEndDate = item.ManagementLoadEndDate
                });
            }

            return list;
        }

        public List<ManagementLoad> MapToAccreditationReports(IEnumerable<ManagementLoad> managementLoadList)
        {
            var reports = new AccreditationReports();
            //var reportList = reports.LoadAccreditationReports();
            var list = new List<ManagementLoad>();

            //foreach (var item in reportList)
            //{
            //}

            //if (managementLoadList.Count() == 0)
            //{
            //    AccreditationReports
            //}

            foreach (var item in managementLoadList)
            {
                list.Add(new ManagementLoad()
                {
                    CVId = item.CVId,
                    ManagementLoadID = item.ManagementLoadID,
                    ManagementLoadCategoryID = item.ManagementLoadCategoryID,
                    ManagementLoadDescription = item.ManagementLoadDescription,
                    Description = item.Description,
                    Credit = item.Credit,
                    isActive = item.isActive
                });
            }

            return list;
        }

        public List<ManagementLoad> MapToResearchReportOneOnOne(IEnumerable<ManagementLoad> managementLoadList)
        {
            var list = new List<ManagementLoad>();

            foreach (var item in managementLoadList)
            {
                list.Add(new ManagementLoad()
                {
                    CVId = item.CVId,
                    ManagementLoadID = item.ManagementLoadID,
                    ManagementLoadCategoryID = item.ManagementLoadCategoryID,
                    Credit = item.Credit,
                    ManagementLoadDescription = item.ManagementLoadDescription,
                    Description = item.Description,
                    isActive = item.isActive
                });
            }

            return list;
        }

        public List<ManagementLoad> MapToChairOfCommittee(IEnumerable<ManagementLoad> managementLoadList)
        {
            var list = new List<ManagementLoad>();

            foreach (var item in managementLoadList)
            {
                list.Add(new ManagementLoad()
                {
                    CVId = item.CVId,
                    ManagementLoadID = item.ManagementLoadID,
                    ManagementLoadCategoryID = item.ManagementLoadCategoryID,
                    Credit = item.Credit,
                    ManagementLoadDescription = item.ManagementLoadDescription,
                    Description = item.Description,
                    isActive = item.isActive,
                    ManagementLoadStartDate = item.ManagementLoadStartDate,
                    ManagementLoadEndDate = item.ManagementLoadEndDate
                });
            }

            return list;
        }

        public List<ManagementLoad> MapToTaskTeamMember(IEnumerable<ManagementLoad> managementLoadList)
        {
            var list = new List<ManagementLoad>();

            foreach (var item in managementLoadList)
            {
                list.Add(new ManagementLoad()
                {
                    CVId = item.CVId,
                    ManagementLoadID = item.ManagementLoadID,
                    ManagementLoadCategoryID = item.ManagementLoadCategoryID,
                    TaskTeamActivityID = item.TaskTeamActivityID??0,
                    Credit = item.Credit,
                    ManagementLoadDescription = item.ManagementLoadDescription,
                    Description = item.Description,
                    isActive = item.isActive,
                    ManagementLoadStartDate = item.ManagementLoadStartDate,
                    ManagementLoadEndDate = item.ManagementLoadEndDate
                });
            }

            return list;
        }

        public List<ManagementLoad> MapToLeadershipOneOnOne(IEnumerable<ManagementLoad> managementLoadList)
        {
            var list = new List<ManagementLoad>();

            foreach (var item in managementLoadList)
            {
                list.Add(new ManagementLoad()
                {
                    CVId = item.CVId,
                    ManagementLoadID = item.ManagementLoadID,
                    ManagementLoadCategoryID = item.ManagementLoadCategoryID,
                    Credit = item.Credit,
                    ManagementLoadDescription = item.ManagementLoadDescription,
                    Description = item.Description,
                    isActive = item.isActive,
                    ManagementLoadStartDate = item.ManagementLoadStartDate,
                    ManagementLoadEndDate = item.ManagementLoadEndDate
                });
            }

            return list;
        }

        public List<ManagementLoad> MapToManagementLoadOther(IEnumerable<ManagementLoad> managementLoadList)
        {
            var list = new List<ManagementLoad>();

            foreach (var item in managementLoadList)
            {
                list.Add(new ManagementLoad()
                {
                    CVId = item.CVId,
                    Credit = item.Credit,
                    ManagementLoadID = item.ManagementLoadID,
                    ManagementLoadCategoryID = item.ManagementLoadCategoryID,
                    ManagementLoadDescription = item.ManagementLoadDescription,
                    Description = item.Description,
                    isActive = item.isActive,
                    ManagementLoadStartDate = item.ManagementLoadStartDate,
                    ManagementLoadEndDate = item.ManagementLoadEndDate
                });
            }

            return list;
        }

        //private List<ExtracurricularDevelopment> MapToManagementLoad(IEnumerable<ManagementLoad> managementLoadList)
        //{
        //    var managementLoad = new List<ManagementLoad>();

        //    if (managementLoadList.Count() == 0)
        //    {
        //        managementLoad.Add(new ManagementLoad());
        //    }

        //    foreach (var item in managementLoadList)
        //    {
        //        managementLoad.Add(new ManagementLoad()
        //        {
        //            CVID = item.CVID,
        //            DevelopmentID = item.DevelopmentID,
        //            DevelopmentCategoryID = item.DevelopmentCategoryID,
        //            City = item.City,
        //            Country = item.Country,
        //            EndDate = item.EndDate,
        //            IsCurrent = item.IsCurrent,
        //            Organisation = item.Organisation,
        //            Position = item.Position,
        //            StartDate = item.StartDate
        //        });
        //    }

        //    return development;
        //}

        #endregion

        private List<Education> MapEducations(List<Education> eductionModel, int cvId)
        {
            var educationService = new List<Education>();

            foreach (var item in eductionModel)
            {
                educationService.Add(new Education()
                {
                    EducationID = item.EducationID,
                    Country = item.Country,
                    CVId = cvId,
                    Degree = item.Degree,
                    FieldOfStudy = item.FieldOfStudy,
                    University = item.University,
                    Year = item.Year,
                    isActive = item.isActive
                });
            }

            return educationService;
        }

        private List<Award> MapAwards(List<Award> awardModel, int cvId)
        {
            var awardService = new List<Award>();
            if (awardModel.IsNotNull())
            {
                foreach (var item in awardModel)
                {
                    awardService.Add(new Award()
                    {
                        AwardID = item.AwardID,
                        AwardType = item.AwardType,
                        AwardDescription = item.AwardDescription,
                        CVId = cvId,
                        Country = item.Country,
                        Year = item.Year,
                        isActive = item.isActive
                    });
                }
            }
            return awardService;
        }

        private List<FacultyLanguage> MapLanguages(List<Language> languageModel, int cvId)
        {
            var language = new List<FacultyLanguage>();

            if (language.Count == 0)
            {
                language.Add(new FacultyLanguage()
                {
                    CVId = cvId,
                    //Afrikaans = languageModel[0].Afrikaans,
                    //English = languageModel[0].English,
                    //French = languageModel[0].French,
                    //German = languageModel[0].German,
                    //Other = languageModel[0].Other,
                    //LanguageOther = languageModel[0].LanguageOther
                });
            }
            else
            {
                //language[0].Afrikaans = languageModel[0].Afrikaans;
                //language[0].English = languageModel[0].English;
                //language[0].French = languageModel[0].French;
                //language[0].German = languageModel[0].German;
                //language[0].Other = languageModel[0].Other;
                //language[0].LanguageOther = languageModel[0].LanguageOther;

            }

            return language;
        }

        #endregion

    }
}
