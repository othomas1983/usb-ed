﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.Models.FacultyManagement
{
    public class UserPhoto
    {
        #region Properties

        public string USNumber { get; set; }
        public byte[] FacultyImage { get; set; }

        #endregion
    }
}
