﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.Models.FacultyManagement
{
    public class ABSRating
    {
        #region Properties

        public int ABSRatingStatusID { get; set; }
        public string ABSRatingStatusDescription { get; set; }
        public Nullable<decimal> Credit { get; set; }

        #endregion
    }
}
