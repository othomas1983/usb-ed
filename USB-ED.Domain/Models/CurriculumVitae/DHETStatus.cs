﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.Models.FacultyManagement
{
    public class DHETStatus
    {
        #region Properties

        public int DHETStatusID { get; set; }
        public string DHETStatusDescription { get; set; }

        #endregion
    }
}
