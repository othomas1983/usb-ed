﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.Models.FacultyManagement
{
    public class Publication : BaseFacultyManagement
    {
        #region Properties

        public int PublicationID { get; set; }
        public int PublicationCategoryID { get; set; }
        public string PublicationTitle { get; set; }
        public string Title { get; set; }
        public Nullable<int> Year { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> Date { get; set; }

        public string SubjectArea { get; set; }
        public string Institution { get; set; }
        public string PublisherName { get; set; }
        public string PublicationCity { get; set; }
        public string NoOfPages { get; set; }
        public string CoAuthorName { get; set; }
        public string Promoter { get; set; }
        public string ResearchType { get; set; }
        public string VolumeNumber { get; set; }
        public Nullable<int> StartPage { get; set; }
        public Nullable<int> EndPage { get; set; }
        public string JournalTitle { get; set; }
        public string Author { get; set; }
        public string Editor { get; set; }
        public string BookTitle { get; set; }
        public Nullable<int> Month { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> StartDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> EndDate { get; set; }

        public string SeminarTitle { get; set; }
        public string Location { get; set; }
        public string ConferenceTitle { get; set; }
        public string Country { get; set; }
        public string CoPresenter { get; set; }
        public string CoOrganiser { get; set; }

        #endregion
    }

    public class ThesisPublication : BaseFacultyManagement
    {
        #region Properties

        public int PublicationID { get; set; }
        public int PublicationCategoryID { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public string Institution { get; set; }
        public string PublicationTitle { get; set; }
        public string Promoter { get; set; }
        public string SubjectArea { get; set; }

        #endregion
    }

    public class PeerReviewedPublication : BaseFacultyManagement
    {
        #region Properties

        public int PublicationID { get; set; }
        public int PublicationCategoryID { get; set; }
        public string CoAuthorName { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public Nullable<int> EndPage { get; set; }
        public string PublicationTitle { get; set; }
        public string ResearchType { get; set; }
        public Nullable<int> StartPage { get; set; }
        public string Title { get; set; }
        public string VolumeNumber { get; set; }

        #endregion
    }

    public class BooksPublication : BaseFacultyManagement
    {
        #region Properties

        public int PublicationID { get; set; }
        public int PublicationCategoryID { get; set; }
        public string CoAuthorName { get; set; }
        public string NoOfPages { get; set; }
        public string PublisherName { get; set; }
        public string PublicationTitle { get; set; }
        public string PublicationCity { get; set; }
        public string ResearchType { get; set; }
        public Nullable<int> Year { get; set; }

        #endregion
    }

    public class MonographPublication : BaseFacultyManagement
    {
        #region Properties

        public int PublicationID { get; set; }
        public int PublicationCategoryID { get; set; }
        public string Author { get; set; }
        public string CoAuthorName { get; set; }
        public string NoOfPages { get; set; }
        public string PublicationTitle { get; set; }
        public string PublisherName { get; set; }
        public string PublicationCity { get; set; }
        public string ResearchType { get; set; }
        public Nullable<int> Year { get; set; }

        #endregion
    }

    public class ChaptersPublication : BaseFacultyManagement
    {
        #region Properties

        public int PublicationID { get; set; }
        public int PublicationCategoryID { get; set; }
        public string BookTitle { get; set; }
        public string CoAuthorName { get; set; }
        public string Editor { get; set; }
        public Nullable<int> EndPage { get; set; }
        public string PublisherName { get; set; }
        public string PublicationCity { get; set; }
        public string PublicationTitle { get; set; }
        public string ResearchType { get; set; }
        public Nullable<int> StartPage { get; set; }
        public Nullable<int> Year { get; set; }

        #endregion
    }

    public class BooksCoordinationPublication : BaseFacultyManagement
    {
        #region Properties

        public int PublicationID { get; set; }
        public int PublicationCategoryID { get; set; }
        public string BookTitle { get; set; }
        public string CoAuthorName { get; set; }
        public string NoOfPages { get; set; }
        public string PublisherName { get; set; }
        public string PublicationCity { get; set; }
        public string ResearchType { get; set; }
        public Nullable<int> Year { get; set; }

        #endregion
    }

    public class CaseStudiesPublication : BaseFacultyManagement
    {
        #region Properties

        public int PublicationID { get; set; }
        public int PublicationCategoryID { get; set; }
        public string CoAuthorName { get; set; }
        public string PublicationTitle { get; set; }
        public string PublisherName { get; set; }
        public Nullable<int> Year { get; set; }


        #endregion
    }

    public class StudyConsultationPublication : BaseFacultyManagement
    {
        #region Properties

        public int PublicationID { get; set; }
        public int PublicationCategoryID { get; set; }
        public string CoAuthorName { get; set; }
        public string PublicationTitle { get; set; }
        public Nullable<int> Year { get; set; }

        #endregion
    }

    public class NonRefereedArticlesPublication : BaseFacultyManagement
    {
        #region Properties

        public int PublicationID { get; set; }
        public int PublicationCategoryID { get; set; }
        public string CoAuthorName { get; set; }
        public Nullable<int> EndPage { get; set; }
        public string JournalTitle { get; set; }
        public string PublicationTitle { get; set; }
        public string ResearchType { get; set; }
        public Nullable<int> StartPage { get; set; }
        public string VolumeNumber { get; set; }
        public Nullable<int> Year { get; set; }

        #endregion
    }

    public class PeerReviewedProceedingsPublication : BaseFacultyManagement
    {
        #region Properties

        public int PublicationID { get; set; }
        public int PublicationCategoryID { get; set; }
        public string ConferenceTitle { get; set; }
        public string Country { get; set; }
        public string CoAuthorName { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public string Location { get; set; }
        public string PublicationTitle { get; set; }
        public string PublicationCity { get; set; }
        public string ResearchType { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }

        #endregion
    }

    public class PeerReviewedWithoutProceedingsPublication : BaseFacultyManagement
    {
        #region Properties

        public int PublicationID { get; set; }
        public int PublicationCategoryID { get; set; }
        public string ConferenceTitle { get; set; }
        public string Country { get; set; }
        public string CoAuthorName { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public string Location { get; set; }
        public string PublicationTitle { get; set; }
        public string PublicationCity { get; set; }
        public string ResearchType { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }

        #endregion
    }

    public class NonRefereedPapersAndCommunicationsPublication : BaseFacultyManagement
    {
        #region Properties

        public int PublicationID { get; set; }
        public int PublicationCategoryID { get; set; }
        public string ConferenceTitle { get; set; }
        public string Country { get; set; }
        public string CoAuthorName { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public string Location { get; set; }
        public string PublicationTitle { get; set; }
        public string PublicationCity { get; set; }
        public string ResearchType { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }

        #endregion
    }

    public class SeminarsPublication : BaseFacultyManagement
    {
        #region Properties

        public int PublicationID { get; set; }
        public int PublicationCategoryID { get; set; }
        public string Country { get; set; }
        public string CoAuthorName { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public string Location { get; set; }
        public string PublicationTitle { get; set; }
        public string PublicationCity { get; set; }
        public string ResearchType { get; set; }
        public string SeminarTitle { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }

        #endregion
    }

    public class ConferencePublication : BaseFacultyManagement
    {
        #region Properties

        public int PublicationID { get; set; }
        public int PublicationCategoryID { get; set; }
        public string ConferenceTitle { get; set; }
        public string Country { get; set; }
        public string CoAuthorName { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public string Location { get; set; }
        public string PublicationCity { get; set; }
        public string ResearchType { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }

        #endregion
    }
}
