﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.Models.FacultyManagement
{
    public class FacultyManagementJournals
    {
        #region Properties

        public int JournalID { get; set; }
        public string JournalTitle { get; set; }
        public string JournalISSN { get; set; }
        public string JournalStatus { get; set; }
        public string JournalRating { get; set; }
        public bool isActive { get; set; }
        public Nullable<decimal> Credit { get; set; }

        #endregion

        #region Public Methods

        public IOrderedEnumerable<FacultyManagementJournals> LoadJournals()
        {
            var journalList = new List<FacultyManagementJournals>();

            var task = new USB_ED.Tasks.FacultyManagementTasks();
            var journal = task.LoadJournals();
            journalList = JsonConvert.DeserializeObject<List<FacultyManagementJournals>>(journal);

            return journalList.OrderBy(u => u.JournalTitle);
        }

        #endregion
    }

    public class Journal
    {
        #region Static Properties

        public static List<FacultyManagementJournals> Journals { get; set; }

        #endregion
    }
}
