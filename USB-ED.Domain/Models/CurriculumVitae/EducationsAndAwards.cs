﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.Models.FacultyManagement
{
    public class EducationsAndAwards
    {
        #region Properties

        public List<Education> Educations { get; set; }
        public List<Award> Awards { get; set; }
        public List<GrantsFunding> GrantsFunding { get; set; }
        #endregion
    }
   public class PermissionCategory
{
        public int CategoryId { get; set; }
        public string Description { get; set; }
}
}
