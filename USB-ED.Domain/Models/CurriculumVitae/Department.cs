﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.Models.FacultyManagement
{
    public class Department
    {
        #region Properties

        public int DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public Nullable<bool> isActive { get; set; }

        #endregion
    }
}
