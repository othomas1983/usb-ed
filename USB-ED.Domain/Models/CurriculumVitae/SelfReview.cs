﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB_ED.Models
{
    public class SelfReview
    {
        #region Properties

        public List<SelfReviewIntegrity> SelfReviewIntegrity { get; set; }
        public List<SelfReviewInclusivity> SelfReviewInclusivity { get; set; }
        public List<SelfReviewInnovation> SelfReviewInnovation { get; set; }
        public List<SelfReviewEngagement> SelfReviewEngagement { get; set; }
        public List<SelfReviewExcellence> SelfReviewExcellence { get; set; }
        public List<SelfReviewSustainability> SelfReviewSustainability { get; set; }

        #endregion
    }

    public class SelfReviewIntegrity
    {
        #region Properties

        public int PerformanceManagementID { get; set; }
        public int CVID { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
        public string ValueCommitment { get; set; }
        public Nullable<int> Year { get; set; }

        #endregion
    }

    public class SelfReviewInclusivity
    {
        #region Properties

        public int PerformanceManagementID { get; set; }
        public int CVID { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
        public string ValueCommitment { get; set; }
        public Nullable<int> Year { get; set; }

        #endregion
    }

    public class SelfReviewInnovation
    {
        #region Properties

        public int PerformanceManagementID { get; set; }
        public int CVID { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
        public string ValueCommitment { get; set; }
        public Nullable<int> Year { get; set; }

        #endregion
    }

    public class SelfReviewEngagement
    {
        #region Properties

        public int PerformanceManagementID { get; set; }
        public int CVID { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
        public string ValueCommitment { get; set; }
        public Nullable<int> Year { get; set; }

        #endregion
    }

    public class SelfReviewSustainability
    {
        #region Properties

        public int PerformanceManagementID { get; set; }
        public int CVID { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
        public string ValueCommitment { get; set; }
        public Nullable<int> Year { get; set; }

        #endregion
    }

    public class SelfReviewExcellence
    {
        #region Properties

        public int PerformanceManagementID { get; set; }
        public int CVID { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
        public string ValueCommitment { get; set; }
        public Nullable<int> Year { get; set; }

        #endregion
    }


}
