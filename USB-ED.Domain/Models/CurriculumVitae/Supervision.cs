﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.Models.FacultyManagement
{
    public class Supervisions : BaseFacultyManagement
    {
        #region Properties

        public int SupervisionID { get; set; }
        public Nullable<int> SupervisorCVID { get; set; }
        public string FirstSupervisor { get; set; }
        public Nullable<int> SecondarySupervisorCVID { get; set; }
        public string SecondSupervisor { get; set; }
        public Nullable<int> Year { get; set; }
        public string StudentName { get; set; }
        public string ThesisTopic { get; set; }
        public string Role { get; set; }
        public Nullable<int> ProgrammeID { get; set; }
        public Nullable<int> ProgrammeOfferingID { get; set; }
        public string ProgrammeDescription { get; set; }
        public string ProgrammeOfferingDescription { get; set; }
        public Nullable<decimal> SupervisorCredit { get; set; }
        public Nullable<decimal> SecondarySupervisorCredit { get; set; }
        public string SourceSystem { get; set; }
        public string ProgressStatus { get; set; }
        #endregion
    }
}
