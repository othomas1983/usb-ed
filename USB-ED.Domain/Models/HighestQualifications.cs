﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.Models.FacultyManagement
{
    public class HighestQualifications
    {
        public int HighestQualificationID { get; set; }
        public string HighestQualification { get; set; }
    }
}
