﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.Models.FacultyManagement
{
    public class DepartmentCategory
    {
        public int DepartmentCategoryID { get; set; }
        public string DepartmentCategoryName { get; set; }
        public int DepartmentID { get; set; }
        public Nullable<bool> isActive { get; set; }
    }
}