﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB.Models.FacultyManagement
{
    public class FacultyDepartmentBundle : BaseFacultyManagement
    {
        public FacultyDepartmentBundle()
        {
            DepartmentCategories = new List<FacultyDepartment>();
        }
        public int FacultyDepartmentID { get; set; }
        public List<FacultyDepartment> DepartmentCategories { get; set; }
    }
}
