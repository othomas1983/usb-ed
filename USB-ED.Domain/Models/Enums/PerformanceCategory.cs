﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace USB_ED.Models.Enums
{
    public enum PerformanceCategory
    {
        TeachingLoad = 1,
        SupervisorLoad = 2,
        ResearchActivities = 3,
        Other = 4
    }
}
