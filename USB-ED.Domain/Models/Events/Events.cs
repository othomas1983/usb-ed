﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.Net;
using USB_ED.Validation;
using SettingsMan = USB_ED.Properties.Settings;

namespace USB_ED.Models
{
    public class Events
    {
        #region Properties

        public Guid CampaignId { get; set; }

        public bool CanRegister { get; set; }

        public bool EventExists { get; set; }

        public string EventName { get; set; }

        public string EventLocation { get; set; }

        private string eventStartDate;
        public string EventStartDateTime
        {
            get { return eventStartDate; }
            set { eventStartDate = ResolveEventDate(value); }
        }

        [Required]
        public string Surname { get; set; }

        [Required]
        public string Name { get; set; }

        public string Organisation { get; set; }

        public string JobTitle { get; set; }

        public string BusinessPhone { get; set; }

        [Required]
        [Display(Name="Mobile Phone")]
        public string MobilePhone { get; set; }

        [Required]
        [ValidateEmailAddress]
        [Display(Name="E-mail")]
        public string Email { get; set; }

        public string IdNumber { get; set; }

        public string ForeignIdNumber { get; set; }

        public string PassportNumber { get; set; }

        #endregion

        #region Private Methods

        private string ResolveEventDate(string value)
        {
            DateTime dateValue;

            if (DateTime.TryParse(value, out dateValue))
            {
                return dateValue.ToString("dd MMM yyyy | hh:mm tt");
            }
            else
            {
                return null;
            }
        }

        #endregion
    }

    public class EventsInfo
    {
        protected string getEventsUrl = SettingsMan.Default.GetEventsUrl;

        public string Id { get; set; }        
       
        public string Html { get; set; }        
        public bool Catering { get; set; }
        public bool Cocktail { get; set; }
        public bool PartnerFunction { get; set; }
        

        //public static string EndDate
        //{
        //    get { return eventEndDate; }
        //    set { eventEndDate = ResolveEventDate(value); }
        //}
       
        //public static string Location { get; set; }
        //public static string City { get; set; }
        //public static string Country { get; set; }
        //public static bool FullyBooked { get; set; }

        #region Public Methods
        public void LoadEvents(Events model, string query)
        {
            WebClient client = new WebClient();
            var url = string.Empty;

            try
            {
                if (query.IsNotNull())
                {
                    url = getEventsUrl + "?eventId=" + query;

                    dynamic result = JsonConvert.DeserializeObject(client.DownloadString(url));

                    model.EventExists = true;

                    foreach (dynamic item in result)
                    {
                        if (item.Name.ToUpper() == "ID")
                        {
                            Id = item.Value;
                        }

                        if (item.Name.ToUpper() == "NAME")
                        {
                            model.EventName = item.Value;
                        }

                        if (item.Name.ToUpper() == "HTML")
                        {
                            Html = item.Value;
                        }

                        if (item.Name.ToUpper() == "STARTDATETIME")
                        {
                            model.EventStartDateTime= item.Value.ToString();
                        }

                        if (item.Name.ToUpper() == "CANREGISTER")
                        {
                            model.CanRegister = item.Value;
                        }

                        if (item.Name.ToUpper() == "CATERING")
                        {
                            Catering = item.Value;
                        }

                        if (item.Name.ToUpper() == "COCKTAIL")
                        {
                            Cocktail = item.Value;
                        }

                        if (item.Name.ToUpper() == "PARTNERFUNCTION")
                        {
                            PartnerFunction = item.Value;
                        }

                        if (item.Name.ToUpper() == "LOCATION")
                        {
                            model.EventLocation = item.Value;
                        }

                        //if (item.Name == "city")
                        //{
                        //    City = item.Value;
                        //}

                        //if (item.Name == "country")
                        //{
                        //    Country = item.Value;
                        //}

                        //if (item.Name == "fullyBooked")
                        //{
                        //    FullyBooked = item.Value;
                        //}
                    }
                }
            }
            catch
            {
                model.EventExists = false;
            }
        }

        public void BookEvent(string json)
        {
            var service = new USBEdServiceReference.ServiceClient();
            
            var result = service.BookEvent(json);
        }

        #endregion
    }
}
