﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using ShortCourseApplicationForm.Domain.CRM;
using ShortCourseApplicationForm.Domain.Crm;
using ShortCourseApplicationForm.Domain.Sharepoint;


namespace USBStudentService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class StudentService : IStudentService
    {
        public string GetStudentNumber(string email)
        {

            string result = null;
            result = CrmQueries.GetUsNumber(email);
            return result;
        }

        public ApplicationSubmissionResult CreateContactAndEnrollStudent(Guid ShortCourseOffering, string EmailAdress, string LastName, string FirstNames,
            string NameOnCertificate, string Initials, Guid Title, Guid Gender, Guid Nationality, Guid Ethnicity, DateTime DateOfBirth,
            string ForeignIdNumber, Guid ForeignIdType, DateTime ForeignIdExpiryDate, string PassportNumber, DateTime PassportExpiryDate,
            string IdNumber, Guid DietaryRequirements, string DietaryRequirementsOther, Guid Language, Guid Disability, bool DoYouUseAWheelchair,
            string Environment, Guid PostalCountry, Guid Province, string PostalSuburb,
            string PostalCity, string PostalStreet1, string PostalStreet2, string PostalPostalCode)
        {
            //HomeLanguage???
            Guid? enrolmentId;
            Guid? contactId;
            string UsNumber = CrmQueries.GetUsNumber(EmailAdress);

            enrolmentId = CrmQueries.GetEnrolmentId(UsNumber, ShortCourseOffering);
            contactId = CrmQueries.GetContactId(UsNumber);
            try {
                CrmActions.CreateOrUpdateContact(ShortCourseOffering, EmailAdress, LastName, FirstNames,
                                NameOnCertificate, Initials, Title, Gender, Nationality, Ethnicity, DateOfBirth, ForeignIdNumber,
                                ForeignIdType, ForeignIdExpiryDate, PassportNumber, PassportExpiryDate, IdNumber, DietaryRequirements,
                                DietaryRequirementsOther, Language, Disability, DoYouUseAWheelchair, contactId, enrolmentId, UsNumber, Environment,
                                PostalCountry, Province, PostalSuburb, PostalCity, PostalStreet1, PostalStreet2, PostalPostalCode);

                return new ApplicationSubmissionResult
                {
                    Successful = true,
                    Message = "",
                    USNumber = UsNumber,
                    EnrolmentId = enrolmentId
                };
            }
            catch(Exception ex)
            {
                return new ApplicationSubmissionResult
                {
                    Successful = false,
                    Message = "Could not create or update contact. " + ex.Message
                };
            }
            
        }

        public bool UploadRequiredDocuments(Guid ShortCourseOffering, string email,
                    string documentName, int documentLevel, string documentDescription, int documentId, byte[] documentBytes)
        {


            string studentNumber = null;
            studentNumber = CrmQueries.GetUsNumber(email);

            Guid enrollment = Guid.Empty;
            CrmQueries.GetEnrolmentId(studentNumber, ShortCourseOffering);


            try
            {
                if (!CrmFileUpload.DoesDocumentLocationForEnrollmentExist(enrollment))
                {
                    CrmFileUpload.CreateUploadedDocumentLocation(enrollment);
                }

                // Add document
                SharepointActions.UploadDocument(documentName, studentNumber, documentLevel, "",
                    documentDescription, documentId, enrollment, documentBytes);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        /**/
        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }  

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }

       


    }
}
