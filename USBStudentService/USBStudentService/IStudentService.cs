﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace USBStudentService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IStudentService
    {

        [OperationContract]
        string GetData(int value);

        [OperationContract]
        CompositeType GetDataUsingDataContract(CompositeType composite);

        [OperationContract]
        string GetStudentNumber(string email);

        [OperationContract]
        string CreateContactAndEnrollStudent(Guid ShortCourseOffering, string EmailAdress, string LastName, string FirstNames,
            string NameOnCertificate, string Initials, Guid Title, Guid Gender, Guid Nationality, Guid Ethnicity, DateTime DateOfBirth,
            string ForeignIdNumber, Guid ForeignIdType, DateTime ForeignIdExpiryDate, string PassportNumber, DateTime PassportExpiryDate, 
            string IdNumber, Guid DietaryRequirements, string DietaryRequirementsOther, Guid Language, Guid Disability, bool DoYouUseAWheelchair,
            string Environment, Guid PostalCountry, Guid Province, string PostalSuburb,
            string PostalCity, string PostalStreet1, string PostalStreet2, string PostalPostalCode);

        [OperationContract]
        bool UploadRequiredDocuments(Guid ShortCourseOffering, string email,
                    string documentName, int documentLevel, string documentDescription, int documentId, byte[] documentBytes);
    }


    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class CompositeType
    {
        bool boolValue = true;
        string stringValue = "Hello ";

        [DataMember]
        public bool BoolValue
        {
            get { return boolValue; }
            set { boolValue = value; }
        }

        [DataMember]
        public string StringValue
        {
            get { return stringValue; }
            set { stringValue = value; }
        }
    }
}
