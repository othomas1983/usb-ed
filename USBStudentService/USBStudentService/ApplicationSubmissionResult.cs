﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace USBStudentService
{
    public class ApplicationSubmissionResult
    {
        public string USNumber { get; set; }

        public Guid? EnrolmentId { get; set; } 

        public bool Successful { get; set; }

        public string Message { get; set; }
    }
}