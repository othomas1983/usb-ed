﻿
namespace USB.RPL.Web
{
    public static class SessionKeys
    {
        public const string ApplicationContext = "ApplicationContext";

        public const string Profile = "Profile";

        public const string Culture = "Culture";
    }
}