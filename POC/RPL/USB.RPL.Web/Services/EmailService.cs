﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using USB.RPL.Domain.User;
using System.Text;
using System.Net;

namespace USB.RPL.Web.Services
{
    public static class EmailService
    {
        public static void SendRegistrationEmail(UserProfile profile)
        {
            try
            {
                var smtpServer = Config.SMTP;
                if (smtpServer.IsNotNullOrEmpty())
                {
                    var client = new SmtpClient(smtpServer);

                    var mailMessage = new MailMessage()
                    {
                        From = new MailAddress(Config.SupportFromAddress)
                    };

                    mailMessage.To.Add(new MailAddress(Config.SupportToAddress));
                    mailMessage.Subject = "RPL User Registration";
                    mailMessage.Body = GetRegistrationBody(profile); ;

                    client.Send(mailMessage);
                }
            }
            catch { }
        }

        public static void SendApprovalEmail(UserProfile profile)
        {
            try
            {
                var smtpServer = Config.SMTP;
                if (smtpServer.IsNotNullOrEmpty())
                {
                    var client = new SmtpClient(smtpServer);
                    var mailMessage = new MailMessage()
                    {
                        From = new MailAddress(Config.SupportFromAddress)
                    };

                    mailMessage.To.Add(new MailAddress(Config.SupportToAddress));
                    mailMessage.Subject = "RPL Basket Approval";
                    mailMessage.Body = GetApprovalBody(profile);

                    client.Send(mailMessage);
                }
            }
            catch { }
        }


        private static string GetRegistrationBody(UserProfile profile)
        {
            var body = new StringBuilder();
            body.AppendLine("Hi Support");
            body.AppendLine("");
            body.AppendLine("The user with the following information has registered and needs approval:");
            body.AppendLine("");
            body.AppendLine("Title: {0}".FormatInvariantCulture(profile.Title));
            body.AppendLine("Name: {0}".FormatInvariantCulture(profile.FirstName));
            body.AppendLine("Surname: {0}".FormatInvariantCulture(profile.LastName));
            body.AppendLine("ID Number: {0}".FormatInvariantCulture(profile.IDForceNumber));
            body.AppendLine("Email: {0}".FormatInvariantCulture(profile.Email));
            body.AppendLine("Cellphone: {0}".FormatInvariantCulture(profile.Cellphone));
            body.AppendLine("Language: {0}".FormatInvariantCulture(profile.Language));
            body.AppendLine("Nationality: {0}".FormatInvariantCulture(profile.Race));
            body.AppendLine("Gender: {0}".FormatInvariantCulture(profile.Gender));
            body.AppendLine("Marital Status: {0}".FormatInvariantCulture(profile.MaritalStatus));
            body.AppendLine("Location: {0}".FormatInvariantCulture(profile.Location));
            body.AppendLine("Province: {0}".FormatInvariantCulture(profile.Province));
            body.AppendLine("RPL Discipline: {0}".FormatInvariantCulture(profile.RPLDiscipline));
            body.AppendLine("");
            body.AppendLine("Regards");
            body.AppendLine("RPL Administration");
            return body.ToString();
        }

        private static string GetApprovalBody(UserProfile profile)
        {
            var body = new StringBuilder();
            body.AppendLine("Hi Support");
            body.AppendLine("");
            body.AppendLine("The user with the following information needs there basket approved:");
            body.AppendLine("");
            body.AppendLine("User Id: {0}".FormatInvariantCulture(profile.Id));
            body.AppendLine("Title: {0}".FormatInvariantCulture(profile.Title));
            body.AppendLine("Name: {0}".FormatInvariantCulture(profile.FirstName));
            body.AppendLine("Surname: {0}".FormatInvariantCulture(profile.LastName));
            body.AppendLine("ID Number: {0}".FormatInvariantCulture(profile.IDForceNumber));
            body.AppendLine("Email: {0}".FormatInvariantCulture(profile.Email));
            body.AppendLine("Cellphone: {0}".FormatInvariantCulture(profile.Cellphone));
            body.AppendLine("");
            body.AppendLine("Regards");
            body.AppendLine("RPL Administration");
            return body.ToString();
        }
    }
}