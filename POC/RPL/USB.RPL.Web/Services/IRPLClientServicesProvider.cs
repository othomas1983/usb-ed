﻿using System;
using USB.RPL.Domain;
using USB.RPL.Domain.Services;
using USB.RPL.Domain.User;


namespace USB.RPL.Web.Services
{
    public interface IRPLClientServicesProvider : IDisposable
    {
        /// <summary>
        /// Loads the specified profile. 
        /// </summary>
        UserProfile GetProfile(string email, string cellphone, string surname);

        /// <summary>
        /// Provides access to the application's entity cache
        /// </summary>
        IEntityCache Cache { get; set; }

        /// <summary>
        /// Provides access to the application cache
        /// </summary>
        IEntityCache GlobalCache { get; set; }

        /// <summary>
        /// Provides access to the application's client side authentication services
        /// </summary>
        IClientSideAuthenticationService ClientAuthentication { get; }

        /// <summary>
        /// Provides access to the application's User services
        /// </summary>
        IUserService Users { get; }

        /// <summary>
        /// Provides a simple means to publish a topic based event to registered subscribers
        /// </summary>
        void Publish(string topic, object payload);

    }
}
