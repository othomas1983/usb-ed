﻿using System;
using System.Collections.Generic;
using USB.RPL.Domain;
using USB.RPL.Domain.Services;
using USB.RPL.Domain.User;

namespace USB.RPL.Web.Services
{
    /// <summary>
    /// RPL Client Facade. Provides access to domain services and components
    /// </summary>
    public class RPLClient : IRPLClientServicesProvider
    {
        #region Locals

        private IRPLSystemFacade RPLSystem;

        private static object cacheLock = new object();

        private IDictionary<string, IList<Action<PublishedEvent>>> subscriptions;

        private static IClientSideAuthenticationService clientAuthenticationService;

        #endregion

        #region Constructors

        public RPLClient()
        {
            RPLSystem = RPLSystemFactory.CreateSystem();

            clientAuthenticationService = clientAuthenticationService ?? InitFormsAuthenticationService();
            subscriptions = new Dictionary<string, IList<Action<PublishedEvent>>>();

            Subscribe(EventTopics.LogOff, (payload) => CacheManager.Handle(this, EventTopics.LogOff, payload));
        }

        #endregion

        #region Properties

        public IClientSideAuthenticationService ClientAuthentication
        {
            get
            {
                return clientAuthenticationService;
            }
        }

        public IUserService Users
        {
            get
            {
                return RPLSystem.GetService<IUserService>();
            }
        }

        public IEntityCache Cache
        {
            get { return this.RPLSystem.Cache; }
            set { this.RPLSystem.Cache = value; }
        }

        public IEntityCache GlobalCache
        {
            get { return this.RPLSystem.Global; }
            set { this.RPLSystem.Global = value; }
        }

        #endregion

        #region Methods

        public UserProfile GetProfile(string email, string cellphone, string surname)
        {
            return Users.LoadUserProfile(email, cellphone, surname);
        }

        private IClientSideAuthenticationService InitFormsAuthenticationService()
        {
            lock (cacheLock)
            {
                if (clientAuthenticationService.IsNull())
                {
                    clientAuthenticationService = new FormsAuthenticationService();
                }
            }

            return clientAuthenticationService;
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            this.RPLSystem.Dispose();
        }

        #endregion

        #region Methods - PubSub

        private void Subscribe(string topic, Action<PublishedEvent> action)
        {
            if (!subscriptions.ContainsKey(topic))
            {
                subscriptions.Add(topic, new List<Action<PublishedEvent>>());
            }

            subscriptions[topic].Add(action);
        }

        public void Publish(string topic, object payload)
        {
            if (subscriptions.ContainsKey(topic))
            {
                var syncdPayload = new PublishedEvent(payload, Guid.NewGuid().ToString());
                subscriptions[topic].ForEach(a => a.Invoke(syncdPayload));
            }
        }

        #endregion



 
    }
}
