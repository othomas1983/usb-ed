﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Airborne.Domain.Security;
using USB.RPL.Domain;

namespace USB.RPL.Web.Services
{
    /// <summary>
    /// A interface for a service that manages the FormsAuthentication of the MVC application.
    /// </summary>
    public interface IClientSideAuthenticationService
    {
        

        /// <summary>
        /// Signs in the specified username using the MVC FormsAuthentication.
        /// </summary>
        /// <param name="userName">Username to sign in.</param>
        /// <param name="rememberUser">Persist log in information to cookie.</param>
        void SignIn(string email, bool rememberUser);

        /// <summary>
        /// Signs out the currently signed in user using the MVC FormsAuthentication.
        /// </summary>
        void SignOut();
    }
}