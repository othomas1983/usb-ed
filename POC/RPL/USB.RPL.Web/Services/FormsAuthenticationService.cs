﻿using System.ComponentModel.Composition;
using System.Web;
using System;
using System.Web.Security;
using Airborne;
using USB.RPL.Domain;
using USB.RPL.Domain.Services;

namespace USB.RPL.Web.Services
{
    [Export(typeof(IClientSideAuthenticationService))]
    public class FormsAuthenticationService : IClientSideAuthenticationService
    {

        

        public void SignIn(string email, bool rememberUser)
        {
            Guard.ArgumentNotEmpty(email, "email");

            FormsAuthentication.SetAuthCookie(email, rememberUser);
        }

        public void SignOut()
        {
            FormsAuthentication.SignOut();
            HttpContext.Current.Session.Abandon();
        }
    }
}