﻿using USB.RPL.Web.Services;

namespace USB.RPL.Web
{
    public static class CacheManager
    {
        public static void Init(IRPLClientServicesProvider system)
        {

        }

        public static class Keys
        {
            public const string SiteContent = "SiteContent";
            public const string TrackingSession = "TrackingSession";
            public const string SystemMessage = "SystemMessage";
        }

        internal static string syncTokenTracker;

        public static void Handle(IRPLClientServicesProvider system, string topic, PublishedEvent @event)
        {

            if (topic == EventTopics.LogOff)
            {
                system.Cache.Clear();
            }

            syncTokenTracker = @event.SyncToken;
        }
    }

    //TODO Extract
    public class PublishedEvent
    {
        public PublishedEvent(object payload, string syncToken)
        {
            Payload = payload;
            SyncToken = syncToken;
        }

        public object Payload { get; private set; }

        public string SyncToken { get; private set; }
    }

    //TODO Extract
    public static class EventTopics
    {
        public static string LogOff = "LogOffEvent";
    }
}
