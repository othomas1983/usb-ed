﻿using System;
using System.Web;
using System.Web.SessionState;
using USB.RPL.Domain;
using System.Diagnostics;

namespace USB.RPL.Web
{
    [DebuggerStepThrough] 
    public class SessionCache : IEntityCache
    {
        HttpSessionState cache;

        public SessionCache()
        {
            this.cache = HttpContext.Current.Session;
        }

        private string GetCacheKey(object key, Type type)
        {
            return "{0}_{1}".FormatInvariantCulture(type, key);
        }

        public void Clear()
        {
            cache.Clear();
        }

        public void Save<T>(object key, T value) where T : class
        {
            var ck = GetCacheKey(key, typeof(T));
            Add(ck, value);
        }

        public void Add(string key, object value)
        {
            if (cache.Contains(key))
            {
                cache[key] = value;
            }
            else
            {
                cache.Add(key, value);
            }
        }



        public void Evict<T>(object key)
        {
            var ck = GetCacheKey(key, typeof(T));

            if (cache.Contains(ck))
            {
                cache.Remove(ck);
            }
        }

        public T Get<T>(object key) where T : class
        {
            var ck = GetCacheKey(key, typeof(T));
            return cache[ck] as T;
        }

        public T Get<T>(object key, Func<T> loadAction) where T : class
        {
            var instance = Get<T>(key) ?? loadAction.Invoke();

            if (instance.IsNotNull())
            {
                Save<T>(key, instance);
            }

            return instance;
        }
    }
}