﻿using System;
using System.Web;
using System.Web.Caching;
using USB.RPL.Domain;
using System.Diagnostics;

namespace USB.RPL.Web
{
    [DebuggerStepThrough]
    public class GlobalCache : IEntityCache
    {
        Cache cache;

        public GlobalCache()
        {
            this.cache = HttpContext.Current.Cache;
        }

        private string GetCacheKey(object key, Type type)
        {
            return "{0}_{1}".FormatInvariantCulture(type, key);
        }

        public void Clear()
        {
            //not supported ...
        }

        public void Save<T>(object key, T value) where T : class
        {
            var ck = GetCacheKey(key, typeof(T));

            Add(ck, value);
        }

        public void Evict<T>(object key)
        {
            var ck = GetCacheKey(key, typeof(T));
            cache.Remove(ck);
        }

        public T Get<T>(object key) where T : class
        {
            var ck = GetCacheKey(key, typeof(T));
            return cache[ck] as T;
        }

        public T Get<T>(object key, Func<T> loadAction) where T : class
        {
            var instance = Get<T>(key) ?? loadAction.Invoke();

            if (instance.IsNotNull())
            {
                Save<T>(key, instance);
            }

            return instance;
        }

        public void Add(string key, object value)
        {
            cache.Insert(key, value, null, Cache.NoAbsoluteExpiration, new TimeSpan(24, 0, 0));
        }
    }
}