﻿using System;
using USB.RPL.Web.Properties;

namespace USB.RPL.Web.Content
{
    public class ContentProvider
    {
        //private static CultureInfo culture = CultureInfo.GetCultureInfo("es-ES");//Thread.CurrentThread.CurrentCulture;

        private static ContentProvider provider;
        public static ContentProvider Instance
        {
            get
            {
                if (provider.IsNull())
                {
                    provider = new ContentProvider();
                }
                return provider;
            }
        }
        
        public string GetString(string key)
        {
            return ContentLabels.ResourceManager.GetString(key, ContentLabels.Culture);
        }

        public string GetLabel(string key)
        {

            return LabelText.ResourceManager.GetString(key, LabelText.Culture);
        }

        public string GetMessage(string key)
        {

            return ErrorLabels.ResourceManager.GetString(key, ErrorLabels.Culture);
        }
    }
}