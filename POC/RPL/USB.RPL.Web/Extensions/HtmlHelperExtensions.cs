﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web.Mvc.Html;
using System.Web.Routing;
using System.Web.UI;
using USB.RPL.Web;
using USB.RPL.Web.Content;


namespace System.Web.Mvc
{


    public static class HtmlHelperExtensions
    {

        public static string Absolute(this UrlHelper url, string path)
        {
            var requestUrl = url.RequestContext.HttpContext.Request.Url;

            var relevantToApp = url.Content(path);

            return "{0}{1}".FormatInvariantCulture(requestUrl.GetLeftPart(UriPartial.Authority), relevantToApp);
        }


        /// <summary>
        /// Gets a string out of the content resource file
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetContentString(this HtmlHelper helper, string key)
        {
            return ContentProvider.Instance.GetString(key);
        }

        /// <summary>
        /// Gets a string out of the labels resource file
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetLabelString(this HtmlHelper helper, string key)
        {
            return ContentProvider.Instance.GetLabel(key);
        }

        /// <summary>
        /// Gets a string out of the content resource file
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetMessageString(this HtmlHelper helper, string key)
        {
            return ContentProvider.Instance.GetMessage(key);
        }

        /// <summary>
        /// Allows location irrelevant usage of scripts in views.
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="scriptName">Name of the script to generate path for.</param>
        /// <returns>Correct path for script regardless of calling location.</returns>
        public static string Script(this HtmlHelper instance, string scriptName)
        {
            var path = instance.ViewContext.HttpContext.Request.ApplicationPath;

            return path + (path[path.Length - 1] == '/' ? "" : "/") + @"Scripts/" + scriptName;
        }


        public static string ScriptStatic(this HtmlHelper instance, string scriptName)
        {
            return ScriptResolver.ResolveScriptPath(instance, scriptName);
        }


        public static string StyleStatic(this HtmlHelper instance, string styleName)
        {
            return ScriptResolver.ResolveStylePath(instance, styleName);
        }

        public static string Style(this HtmlHelper instance, string styleName)
        {
            return new UrlHelper(instance.ViewContext.RequestContext).Absolute(@"~/content/css/" + styleName);
        }


        /// <summary>
        /// Allows location irrelevant usage of images in views.
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="imageName">Name of the image to generate path for.</param>
        /// <returns>Correct path for image regardless of calling location.</returns>
        public static string Image(this HtmlHelper instance, string imageName)
        {
            var path = instance.ViewContext.HttpContext.Request.ApplicationPath;
            return path + (path[path.Length - 1] == '/' ? "" : "/") + @"Content/gfx/" + imageName;
        }

        /// <summary>
        /// Generates view code for a ReCaptcha implementation. Use in conjunction with CaptchaValidatorAttribute.
        /// </summary>
        /// <param name="instance"></param>
        /// <returns>Html markup for a ReCaptcha implementation.</returns>       
        public static string GenerateCaptcha(this HtmlHelper instance)
        {

            var captchaControl = new Recaptcha.RecaptchaControl
                    {
                        ID = "recaptcha",
                        Theme = "clean",
                        PublicKey = "6LdM6rkSAAAAAIDOe1U7wPvXXtv_N-z8ToVBsgU4",
                        PrivateKey = "6LdM6rkSAAAAACypFtMNhTUzbUfv_GOsG3N2_4MI"
                    };

            var htmlWriter = new HtmlTextWriter(new StringWriter());

            captchaControl.RenderControl(htmlWriter);

            return htmlWriter.InnerWriter.ToString();
        }

        /// <summary>
        /// Generates markup for first model level error in the current ModelStateDictionary. 
        /// Useful to replace a summary listing when space is tight.
        /// </summary>
        /// <param name="instance"></param>
        /// <returns>Valid HTML markup for the first model level error in the current ModelStateDictionary.</returns>
        public static MvcHtmlString FirstModelValidationError(this HtmlHelper instance)
        {
            if (instance.ViewData.ModelState[""].IsNull())
            {
                return MvcHtmlString.Create("");
            }

            return MvcHtmlString.Create(instance.ViewData.ModelState[""].Errors[0].ErrorMessage ?? "");
        }

        #region CheckboxList

        /// <summary>
        /// Generates a strongly typed Checkbox List from the provided model.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TValue">The type of the objects contained in the list</typeparam>
        /// <param name="instance">An instance of the HtmlHelperObject</param>
        /// <param name="expression">An expression that identifies the object that contains the properties to render.</param>
        /// <param name="selectedFunction">A function that returns a boolean indicating whether the given object should be checked.</param>
        /// <param name="valueFunction">A function that returns a value for the id attribute of the checkbox list.</param>
        /// <returns>A list of html input elements of type 'checkbox', one for each item in the specified IEnumerable</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures")]
        public static MvcHtmlString CheckBoxListFor<TModel, TValue>(this HtmlHelper<TModel> instance, Expression<Func<TModel, IEnumerable<TValue>>> expression, Expression<Func<TValue, bool>> selectedFunction, Expression<Func<TValue, object>> valueFunction)
        {
            return CheckBoxListFor(instance, expression, selectedFunction, valueFunction, null);
        }

        /// <summary>
        /// Generates a strongly typed Checkbox List from the provided model.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TValue">The type of the objects contained in the list</typeparam>
        /// <param name="instance">An instance of the HtmlHelperObject</param>
        /// <param name="expression">An expression that identifies the object that contains the properties to render.</param>
        /// <param name="selectedFunction">A function that returns a boolean indicating whether the given object should be checked.</param>
        /// <param name="valueFunction">A function that returns a value for the id attribute of the checkbox list.</param>
        /// <returns>A list of html input elements of type 'checkbox', one for each item in the specified IEnumerable</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures")]
        public static MvcHtmlString CheckBoxListFor<TModel, TValue>(this HtmlHelper<TModel> instance, Expression<Func<TModel, IEnumerable<TValue>>> expression, Expression<Func<TValue, bool>> selectedFunction, Expression<Func<TValue, object>> valueFunction, object htmlAttributes)
        {
            return CheckBoxListFor(instance, expression, selectedFunction, valueFunction, valueFunction, htmlAttributes);
        }

        /// <summary>
        /// Generates a strongly typed Checkbox List from the provided model.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TValue">The type of the objects contained in the list</typeparam>
        /// <param name="instance">An instance of the HtmlHelperObject</param>
        /// <param name="expression">An expression that identifies the object that contains the properties to render.</param>
        /// <param name="selectedFunction">A function that returns a boolean indicating whether the given object should be checked.</param>
        /// <param name="valueFunction">A function that returns a value for the innerHtml (label) of the checkbox list.</param>
        /// <param name="idFunction">A function that returns a value for the id attribute of the checkbox list.</param>
        /// <returns>A list of html input elements of type 'checkbox', one for each item in the specified IEnumerable</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures")]
        public static MvcHtmlString CheckBoxListFor<TModel, TValue>(this HtmlHelper<TModel> instance, Expression<Func<TModel, IEnumerable<TValue>>> expression, Expression<Func<TValue, bool>> selectedFunction, Expression<Func<TValue, object>> valueFunction, Expression<Func<TValue, object>> idFunction, object htmlAttributes)
        {
            var root = new StringBuilder();
            var propertyName = ExpressionHelper.GetExpressionText(expression);

            var model = (TModel)instance.ViewData.Model;
            var list = expression.Compile().Invoke(model);

            var selectedAction = selectedFunction.Compile();
            var valueAction = valueFunction.Compile();
            var idAction = idFunction.Compile();

            foreach (TValue theListObject in list)
            {
                var element = new TagBuilder("input");
                element.Attributes.Add("type", "checkbox");
                if (selectedAction.Invoke(theListObject))
                {
                    element.Attributes.Add("checked", "true");
                }
                element.Attributes.Add("name", propertyName);
                element.Attributes.Add("id", idAction.Invoke(theListObject).ToString());
                element.InnerHtml = valueAction.Invoke(theListObject).ToString();
                if (htmlAttributes.IsNotNull())
                {
                    element.MergeAttributes(((IDictionary<string, object>)new RouteValueDictionary(htmlAttributes)), true);
                }
                root.Append(element.ToString());
                root.Append("<br />");
            }


            return MvcHtmlString.Create(root.ToString());
        }
        #endregion

        #region RadioButtonList
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures")]
        public static MvcHtmlString RadioButtonListFor<TModel, TValue>(this HtmlHelper<TModel> instance, Expression<Func<TModel, IEnumerable<TValue>>> expression, Expression<Func<TValue, bool>> selectedFunction, Expression<Func<TValue, object>> valueFunction)
        {
            return RadioButtonListFor(instance, expression, selectedFunction, valueFunction, null);
        }

        /// <summary>
        /// Generates a strongly typed RadioButton List from the provided model.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TValue">The type of the objects contained in the list</typeparam>
        /// <param name="instance">An instance of the HtmlHelperObject</param>
        /// <param name="expression">An expression that identifies the object that contains the properties to render.</param>
        /// <param name="selectedFunction">A function that returns a boolean indicating whether the given object should be checked.</param>
        /// <param name="valueFunction">A function that returns a value for the id attribute of the RadioButton list.</param>
        /// <returns>A list of html input elements of type 'RadioButton', one for each item in the specified IEnumerable</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures")]
        public static MvcHtmlString RadioButtonListFor<TModel, TValue>(this HtmlHelper<TModel> instance, Expression<Func<TModel, IEnumerable<TValue>>> expression, Expression<Func<TValue, bool>> selectedFunction, Expression<Func<TValue, object>> valueFunction, object htmlAttributes)
        {
            return RadioButtonListFor(instance, expression, selectedFunction, valueFunction, valueFunction, htmlAttributes);
        }


        /// <summary>
        /// Generates a strongly typed RadioButton List from the provided model.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TValue">The type of the objects contained in the list</typeparam>
        /// <param name="instance">An instance of the HtmlHelperObject</param>
        /// <param name="expression">An expression that identifies the object that contains the properties to render.</param>
        /// <param name="selectedFunction">A function that returns a boolean indicating whether the given object should be checked.</param>
        /// <param name="valueFunction">A function that returns a value for the innerHtml (label) of the RadioButton list.</param>
        /// <param name="idFunction">A function that returns a value for the id attribute of the RadioButton list.</param>
        /// <returns>A list of html input elements of type 'RadioButton', one for each item in the specified IEnumerable</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures")]
        public static MvcHtmlString RadioButtonListFor<TModel, TValue>(this HtmlHelper<TModel> instance, Expression<Func<TModel, IEnumerable<TValue>>> expression, Expression<Func<TValue, bool>> selectedFunction, Expression<Func<TValue, object>> valueFunction, Expression<Func<TValue, object>> idFunction, object htmlAttributes)
        {
            return RadioButtonListFor(instance, expression, selectedFunction, valueFunction, valueFunction, idFunction, htmlAttributes);
        }
        /// <summary>
        /// Generates a strongly typed RadioButton List from the provided model.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TValue">The type of the objects contained in the list</typeparam>
        /// <param name="instance">An instance of the HtmlHelperObject</param>
        /// <param name="expression">An expression that identifies the object that contains the properties to render.</param>
        /// <param name="selectedFunction">A function that returns a boolean indicating whether the given object should be checked.</param>
        /// <param name="valueFunction">A function that returns a value for the innerHtml (label) of the RadioButton list.</param>
        /// <param name="idFunction">A function that returns a value for the id attribute of the RadioButton list.</param>
        /// <returns>A list of html input elements of type 'RadioButton', one for each item in the specified IEnumerable</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures")]
        public static MvcHtmlString RadioButtonListFor<TModel, TValue>(this HtmlHelper<TModel> instance, Expression<Func<TModel, IEnumerable<TValue>>> expression, Expression<Func<TValue, bool>> selectedFunction, Expression<Func<TValue, object>> valueFunction, Expression<Func<TValue, object>> displayFunction, Expression<Func<TValue, object>> idFunction, object htmlAttributes)
        {
            var root = new StringBuilder();
            var propertyName = ExpressionHelper.GetExpressionText(expression);

            var model = (TModel)instance.ViewData.Model;
            var list = expression.Compile().Invoke(model);

            var selectedAction = (selectedFunction.IsNull()) ? null : selectedFunction.Compile();
            var valueAction = valueFunction.Compile();
            var idAction = idFunction.Compile();
            var displayAction = displayFunction.Compile();

            foreach (TValue theListObject in list)
            {
                var element = new TagBuilder("input");
                element.Attributes.Add("type", "radio");
                if (selectedAction.IsNotNull() && selectedAction.Invoke(theListObject))
                {
                    element.Attributes.Add("checked", "true");
                }
                element.Attributes.Add("name", propertyName);
                element.Attributes.Add("id", idAction.Invoke(theListObject).ToString());
                element.InnerHtml = valueAction.Invoke(theListObject).ToString();
                if (htmlAttributes.IsNotNull())
                {
                    element.MergeAttributes(((IDictionary<string, object>)new RouteValueDictionary(htmlAttributes)), true);
                }
                root.Append(element.ToString());
                root.Append(displayAction.Invoke(theListObject).ToString());
                root.Append("<br />");
            }


            return MvcHtmlString.Create(root.ToString());
        }

        public static MvcHtmlString DropDownList(this HtmlHelper helper, string name, Type type)
        {
            if (!type.IsEnum) throw new ArgumentException("Type is not an enum.");
            var enums = new List<SelectListItem>();
            foreach (int value in Enum.GetValues(type))
            {
                enums.Add(new SelectListItem { Value = value.ToString(), Text = Enum.GetName(type, value) });
            }
            return System.Web.Mvc.Html.SelectExtensions.DropDownList(helper, name, enums);
        }

        public static MvcHtmlString RadioButtonForSelectList<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            IEnumerable<SelectListItem> listOfValues)
        {
            return RadioButtonForSelectList<TModel, TProperty>(htmlHelper, expression, listOfValues, null);
        } 

        public static MvcHtmlString RadioButtonForSelectList<TModel, TProperty>(
            this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression,
            IEnumerable<SelectListItem> listOfValues,
             object htmlAttributes)
        {
            var metaData = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            var sb = new StringBuilder();

            if (listOfValues != null)
            {
                // Create a radio button for each item in the list 
                foreach (SelectListItem item in listOfValues)
                {
                    // Generate an id to be given to the radio button field 
                    var id = string.Format("{0}_{1}", metaData.PropertyName, item.Value);

                    // Create and populate a radio button using the existing html helpers 
                    var label = htmlHelper.Label(id, HttpUtility.HtmlEncode(item.Text));
                    var radio = htmlHelper.RadioButtonFor(expression, item.Value, htmlAttributes).ToHtmlString();
                    // Create the html string that will be returned to the client 
                    // e.g. <input data-val="true" data-val-required="You must select an option" id="TestRadio_1" name="TestRadio" type="radio" value="1" /><label for="TestRadio_1">Line1</label> 
                  sb.AppendFormat("{0}{1}", radio, label);
                }
            }

            return MvcHtmlString.Create(sb.ToString());
        } 


        #endregion

        public static string ToJasonString(this HtmlHelper instance, object objectToSerialise)
        {
            var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            return serializer.Serialize(objectToSerialise);
        }


    }



    internal static class ScriptResolver
    {
        internal static string ResolveScriptPath(HtmlHelper html, string scriptName)
        {
            var setName = scriptName.Split('.').First();
            var path = Combres.Mvc.MvcExtensions.CombresUrl(html, setName);
            return path;
        }

        internal static string ResolveStylePath(HtmlHelper html, string styleName)
        {
            var setName = styleName.Split('.').First();
            var path = Combres.Mvc.MvcExtensions.CombresUrl(html, setName);
            return path;
        }
    }
}