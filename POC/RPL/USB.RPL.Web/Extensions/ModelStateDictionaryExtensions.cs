﻿using System.Collections.Generic;
using System.Linq;
using Airborne.Notifications;

namespace System.Web.Mvc
{
    public static class ModelStateDictionaryExtensions
    {
        /// <summary>
        /// Merges error notifications in a NotificationCollection into the current ModelStateDictionary.
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="notifications">A NotificationCollection to merge.</param>
        public static void MergeWith(this ModelStateDictionary instance, IEnumerable<Notification> notifications)
        {
            foreach (var notification in notifications)
            {
                //TODO text must be mulitLingual?
                var key = notification.Tag.IsNotNull() ? notification.Tag.ToString() : string.Empty;
                instance.AddModelError(key, notification.Text);
            }
        }

        public static void MergeWith(this ModelStateDictionary instance, NotificationCollection notifications, NotificationSeverity filter)
        {
            MergeWith(instance, notifications.Where(n => n.Severity == filter));
        }

        public static void MergeWith(this ModelStateDictionary instance, NotificationCollection notifications)
        {
            MergeWith(instance, notifications.ToArray());
        }
    }
}
