﻿using System.Web.Routing;
using System.Web.Mvc;
using System;
using USB.RPL.Web.Controllers;

namespace USB.RPL.Web.Extensions
{
    /// <summary>
    /// A utility class for generating RedirectToRouteResults
    /// </summary>
    public static class Redirects
    {

        public static RedirectToRouteResult ToBrowserNotSupported()
        {
            return new RedirectToRouteResult(new RouteValueDictionary(new { Area = "", @Action = "BrowserNotSupported", @Controller = "Home" }));
        }

        public static RedirectToRouteResult ToFileNotFound(string filename)
        {
            return new RedirectToRouteResult(new RouteValueDictionary(new { Area = "", @Action = "FileNotFound", @Controller = "Home" })).AddRouteValue("filename", filename);
        }

        public static RedirectToRouteResult ToPageNotFound()
        {
            return new RedirectToRouteResult(new RouteValueDictionary(new { Area = "", @Action = "PageNotFound", @Controller = "Home" }));
        }

        public static RedirectToRouteResult ToInternalErrorPage()
        {
            return new RedirectToRouteResult(new RouteValueDictionary(new { Area = "", @Action = "InternalError", @Controller = "Home" }));
        }

        public static void Unauthorised(AuthorizationContext filterContext, RPLController controller)
        {
            filterContext.Result = UnauthorisedAction(filterContext, controller);
        }

        private static ActionResult UnauthorisedAction(ControllerContext context, RPLController controller)
        {
            var action = controller.RouteData.Values["Action"].ToString().CamelCaseToWords();
            var area = controller.DisplayName;

            if (context.HttpContext.Request.Headers["X-Requested-With"] != null
                && context.HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
            {
                var result = new JsonResult()
                {
                    Data = new
                    {
                        errors = String.Format("You are not authorised to use the {0} action in the {1} area.",
                        action, area)
                    },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };

                return result;
            }
            else
            {
                var result = new ViewResult()
                {
                    ViewName = "NotAuthorized"
                };
                result.ViewData = controller.ViewData;
                result.ViewData["User"] = controller.ViewData["User"];
                result.ViewData["Action"] = action;
                result.ViewData["Tab"] = area;

                return result;
            }
        }

        /// <summary>
        /// Generate a RedirectToRouteResult for the Portal page.
        /// </summary>
        /// <returns>A RedirectToRouteResult for the Page page action.</returns>
        public static RedirectToRouteResult ToPortalPage()
        {
            return new RedirectToRouteResult(new RouteValueDictionary(new { Area = "Portal", @Action = "Index", @Controller = "Portal" }));
        }

 
        public static RedirectToRouteResult ToLandingPage()
        {
            return new RedirectToRouteResult(new RouteValueDictionary(new { Area = "", @Action = "Index", @Controller = "Home" }));
        }

        private static string CreateRequestToken()
        {
            return "{0}{1}{2}{3}".FormatInvariantCulture(DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond);
        }

        /// <summary>
        /// Generate a RedirectToRouteResult for Welcome
        /// </summary>
        /// <returns></returns>
        public static RedirectToRouteResult ToWelcome()
        {
            return new RedirectToRouteResult(new RouteValueDictionary(new { Area = "", @Action = "Welcome", @Controller = "Account" }));
        }

        /// <summary>
        /// Generate a RedirectToRouteResult for LogOn
        /// </summary>
        /// <returns></returns>
        public static RedirectToRouteResult ToLogOn()
        {
            return new RedirectToRouteResult(new RouteValueDictionary(new { Area = "", @Action = "LogOn", @Controller = "Account" }));
        }


        /// <summary>
        /// Generate a RedirectToRouteResult for the specified action in the current controller in the default area.
        /// </summary>
        /// <param name="Action">The action to target.</param>
        /// <returns>A RedirectToRouteResult for the targeted action.</returns>
        public static RedirectToRouteResult ToRoute(string Action)
        {
            return new RedirectToRouteResult(new RouteValueDictionary(new { Area = "", @Action = Action }));
        }

        /// <summary>
        /// Generate a RedirectToRouteResult for the specified action in the specified controller in the default area.
        /// </summary>
        /// <param name="Action">The action to target.</param>
        /// <param name="Controller">The controller to target.</param>
        /// <returns>A RedirectToRouteResult for the targeted action in the targeted controller.</returns>
        public static RedirectToRouteResult ToRoute(string Action, string Controller)
        {
            return new RedirectToRouteResult(new RouteValueDictionary(new { Area = "", @Action = Action, @Controller = Controller }));
        }

        /// <summary>
        /// Generate a RedirectToRouteResult for the specified action in the specified controller in the specified area.
        /// </summary>
        /// <param name="Action">The action to target.</param>
        /// <param name="Controller">The controller to target.</param>
        /// <param name="Area">The area to target.</param>
        /// <returns>A RedirectToRouteResult for the targeted action in the targeted controller in a specified Area.</returns>
        public static RedirectToRouteResult ToRoute(string Action, string Controller, string Area)
        {
            return new RedirectToRouteResult(new RouteValueDictionary(new { @Area = Area, @Action = Action, @Controller = Controller }));
        }

        /// <summary>
        /// Generate a RedirectResult for an external URL.
        /// </summary>
        /// <param name="Url">The Url to target.</param>
        /// <returns>A RedirectResult for the targetted Url.</returns>
        internal static RedirectResult ToUrl(string Url)
        {
            return new RedirectResult(Url);
        }

    }
}
