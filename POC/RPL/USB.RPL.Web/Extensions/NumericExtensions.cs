﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace USB.RPL.Web.Extensions
{
    /// <summary>
    /// Used to display numeric values on the UI.
    /// </summary>
    public static class NumericExtensions
    {
        /// <summary>
        /// Show numeric as a reader friendly text.
        /// </summary>
        public static string ShowFriendly(this int? value)
        {
            return value > 0 ? value.ToString() : "";
        }

        /// <summary>
        /// Show numeric as a reader friendly text.
        /// </summary>
        public static string ShowFriendly(this double value)
        {
            return value > 0 ? System.Math.Round(value, 2).ToString() : "";
        }

    }
}