﻿using System.Collections.Generic;
using System.Linq;
using Airborne;
using System.Globalization;
using System.Collections;

namespace System.Web.Mvc
{
    /// <summary>
    /// Extends the SelectListItem class
    /// </summary>
    public static class SelectListItemExtensions
    {

        #region Methods

        /// <summary>
        /// Sets all items in the list to not be selected
        /// </summary>
        /// <param name="items"></param>
        public static void ClearSelectedItems(this IEnumerable<SelectListItem> items)
        {
            items.ToList().ForEach(item => item.Selected = false);
        }

        /// <summary>
        /// Gets the text of the selected item
        /// </summary>
        public static string GetSelectedText(this IEnumerable<SelectListItem> items)
        {
            var selectedItem = items.FirstOrDefault(i => i.Selected);
            return selectedItem.IsNotNull() ? selectedItem.Text : String.Empty;
        }

        /// <summary>
        /// Gets the value of the selected item 
        /// </summary>
        public static string GetSelectedValue(this IEnumerable<SelectListItem> items)
        {
            var selectedItem = items.FirstOrDefault(i => i.Selected);
            return selectedItem.IsNotNull() ? selectedItem.Value : String.Empty;
        }

        /// <summary>
        /// Populates list from Enum and sets selected items or default.
        /// </summary>
        public static void PopulateFromEnum<T>(this IList<SelectListItem> items, object selectedItem, object defaultValue)
        {
            Guard.ArgumentNotNull(items, "items");

            var type = typeof(T);
            
            Enum.GetNames(type).ToList().ForEach(e => items.Add(new SelectListItem() { Text = e, Value = e }));
            var unknown = items.FirstOrDefault(i => i.Value.ToString() == "UnKnown");
            var undefined = items.FirstOrDefault(i => i.Value.ToString() == "Undefined");
            if (unknown.IsNotNull())
            {
                items.Remove(unknown);
            }

            if (undefined.IsNotNull())
            {
                items.Remove(undefined);
            }

            selectedItem = selectedItem.IsNotNull() ? selectedItem : defaultValue;
            if (selectedItem != null)
            {
                var match = items.FirstOrDefault(i => i.Value == ((T)selectedItem).ToString());
                if (match.IsNotNull())
                {
                    match.Selected = true;
                }
            }
        }

        /// <summary>
        /// Populates list from Enum and sets selected.
        /// </summary>
        public static void PopulateFromEnum<T>(this IList<SelectListItem> items, object selectedItem)
        {
            items.PopulateFromEnum<T>(selectedItem, null);
        }

        /// <summary>
        /// Populates list from Enum.
        /// </summary>
        public static void PopulateFromEnum<T>(this IList<SelectListItem> items)
        {
            items.PopulateFromEnum<T>(null, null);
        }

        /// <summary>
        /// Populates items with numeric values. Sets selected item & min/max post and prefixes.
        /// </summary>
        public static void Populate(this IList<SelectListItem> items, int start, int end, double increment, double? selectedValue, string minPrefix, string maxPostfix)
        {
            Guard.ArgumentNotNull(items, "items");

            var selectedValueMatch = selectedValue.IsNotNull() ? (int)selectedValue : start - 100;
            for (double i = start; i <= end; i = i + increment)
            {
                var display = i.ToString();
                if ((i == start) && ((minPrefix.Trim().Length > 0) && (minPrefix.Trim().Length < 3)))
                {
                    display = minPrefix + (start + increment).ToString();
                }
                else if((i == start) && (minPrefix.Trim().Length > 2))
                {
                    display = minPrefix;
                }

                if ((i == end) && ((maxPostfix.Trim().Length > 0) && (maxPostfix.Trim().Length < 3)))
                {
                    display = (end - increment).ToString() + maxPostfix;
                }
                else if ((i == end) && (maxPostfix.Trim().Length > 2))
                {
                    display = maxPostfix;
                }
               

                items.Add(new SelectListItem() { Text = display, Value = i.ToString(), Selected = (i == selectedValueMatch) });
               
            }
        }

        /// <summary>
        /// Populates items with numeric values. Sets selected item & max postfix.
        /// </summary>
        public static void Populate(this IList<SelectListItem> items, int start, int end, double increment, double? selectedValue, string maxPostfix)
        {
            items.Populate(start, end, increment, selectedValue, "", maxPostfix);
        }

        /// <summary>
        /// Populates items with numeric values. Sets selected item.
        /// </summary>
        public static void Populate(this IList<SelectListItem> items, int start, int end, double increment, double? selectedValue)
        {
            items.Populate(start, end, increment, selectedValue, "", "");
        }

        /// <summary>
        /// Populates items with numeric values. 
        /// </summary>
        public static void Populate(this IList<SelectListItem> items, int start, int end, double increment)
        {
            items.Populate(start, end, increment, null, "", "");
        }

        /// <summary>
        /// Populate list items with months of the year.
        /// </summary>
        public static void PopulateWithMonthsOfYear(this IList<SelectListItem> items)
        {
            Guard.ArgumentNotNull(items, "items");

            for (int i = 1; i <= 12; i++)
            {
                items.Add(new SelectListItem() { Value = i.ToString(), Text = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(i) });
            }
        }

        /// <summary>
        /// Populate list items with minutes.
        /// </summary>
        public static void PopulateWithMinutes(this IList<SelectListItem> items)
        {
            Guard.ArgumentNotNull(items, "items");

            for (int i = 0; i < 60; i++)
            {
                items.Add(new SelectListItem() { Value = i.ToString("00"), Text = i.ToString("00") });
            }
        }

        /// <summary>
        /// Populate list items with seconds.
        /// </summary>
        public static void PopulateWithSeconds(this IList<SelectListItem> items)
        {
            Guard.ArgumentNotNull(items, "items");

            for (int i = 0; i < 60; i++)
            {
                items.Add(new SelectListItem() { Value = i.ToString("00"), Text = i.ToString("00") });
            }
        }

        #endregion

    }
}
