﻿using Airborne.Notifications;

namespace System.Web.Mvc
{
    public static class JsonResultExtensions
    {
        //TODO: rename so more generic e.g Successful
        public static JsonResult SaveSuccessful(this JsonResult json)
        {
            json.Data = new { SaveSuccessful = true };
            return json;
        }
     
        public static JsonResult Error(this JsonResult json)
        {
            json.Data = new { HasErrors = true };
            return json;
        }

        //TODO: rename so more generic e.g Failed (in the json data = SaveFailed = Failed)
        public static JsonResult Failed(this JsonResult json, NotificationCollection notifications)
        {
            json.Data = new { SaveFailed = true, Notifications = notifications };
            return json;
        }
    }
}