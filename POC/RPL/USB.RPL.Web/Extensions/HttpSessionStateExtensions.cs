﻿using System;
using System.Collections.Generic;
using System.Web.SessionState;
using USB.RPL.Domain;
using System.Web;
using System.Collections;
using System.Linq;
using USB.RPL.Domain.User;
using USB.RPL.Web.Contexts;

namespace USB.RPL.Web
{
    public static class SessionExtensions
    {
        /// <summary>
        /// Provides a type safe extension method to query a HttpSessionStateBase instance
        /// </summary>
        public static T Get<T>(this HttpSessionStateBase session, string key)
        {
            return GetValue<T>(key, session);
        }

        /// <summary>
        /// Provides a type safe extension method to query a HttpSessionState instance
        /// </summary>
        public static T Get<T>(this HttpSessionState session, string key)
        {
            return GetValue<T>(key, session);
        }

        private static T GetValue<T>(string key, object session)
        {
            dynamic sessionToQuery = session; // performace hit ???

            if (sessionToQuery[key] == null)
            {
                return default(T);
            }

            return (T)sessionToQuery[key];
        }
        
        public static bool Contains(this HttpSessionStateBase session, string key)
        {
            return session[key].IsNotNull();
        }

        public static bool Contains(this HttpSessionState session, string key)
        {
           return session[key].IsNotNull();
        }
        
        public static bool UserIsLoggedOn(this HttpSessionState session)
        {
            return UserIsLoggedOn(session[SessionKeys.ApplicationContext] as ApplicationContext);
        }

        public static bool UserIsLoggedOn(this HttpSessionStateBase session)
        {
            return UserIsLoggedOn(session[SessionKeys.ApplicationContext] as ApplicationContext);
        }

        private static bool UserIsLoggedOn(ApplicationContext context)
        {
            try
            {
                if(context.IsNotNull()) 
                {
                    return context.CurrentUser.IsNotNull();
                }
            }
            catch
            {
                return false;
            }
            return false;
        }

    }
}
