﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using USB.RPL.Web.Utilities;

namespace System.Web.Mvc
{
    /// <summary>
    /// Extension methods to extend the ActionResult class.
    /// </summary>
    public static class ActionResultExtensions
    {
        #region Methods

        public static string Capture(this ActionResult result, ControllerContext controllerContext, HttpResponseBase response)
        {
            using (var it = new ResponseCapture(response))
            {
                result.ExecuteResult(controllerContext);
                return it.ToString();
            }
        }

        #endregion
    }
}