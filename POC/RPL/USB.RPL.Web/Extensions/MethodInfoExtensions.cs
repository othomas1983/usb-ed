﻿using System.Linq;


namespace System.Reflection
{

    public static class MethodInfoExtensions
    {
        public static T GetCustomAttribute<T>(this MethodInfo value) where T : Attribute
        {
            return value.GetCustomAttributes(typeof(T), true).FirstOrDefault() as T;
        }
    }

}