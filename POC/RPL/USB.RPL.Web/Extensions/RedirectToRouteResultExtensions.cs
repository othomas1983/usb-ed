﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace USB.RPL.Web.Extensions
{
    public static class RedirectToRouteResultsExtensions
    {
        /// <summary>
        /// Allows adding of a GET variable or a route value to an existing RedirectToRouteResult
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="key">The key of the added value.</param>
        /// <param name="value">The value to add.</param>
        /// <returns>The existing RedirectToRouteResult with the specified value added.</returns>
        public static RedirectToRouteResult AddRouteValue(this RedirectToRouteResult instance, string key, object value)
        {
            instance.RouteValues.Add(key, value);
            return instance;
        }
    }
}
