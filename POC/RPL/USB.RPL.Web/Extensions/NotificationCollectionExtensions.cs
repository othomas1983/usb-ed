﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using USB.RPL.Web.Wrappers;

namespace Airborne.Notifications
{
    public static class NotificationCollectionExtensions
    {

        /// <summary>
        /// Creates a validation error notification list
        /// </summary>
        public static IList<ErrorNotification> ToValidationErrorResult(this NotificationCollection notification)
        {
            var errorNotifications = new List<ErrorNotification>();
            notification.ToList().ForEach(n => errorNotifications.Add(ErrorNotification.Create(n.Code, n.Text)));
            return errorNotifications;
        }

        
    }
}