﻿using System;
using System.Configuration;

namespace USB.RPL.Web
{
    public static class Config
    {
        public static int DefaultSessionTimeoutMinutes
        {
            get
            {
                return int.Parse(ConfigurationManager.AppSettings["DefaultSessionTimeoutMinutes"]);
            }
        }

        public static int DefaultSessionTimeoutWarningMinutes
        {
            get
            {
                return int.Parse(ConfigurationManager.AppSettings["DefaultSessionTimeoutWarningMinutes"]);
            }
        }

        public static int DefaultPaymentReferenceUses
        {
            get
            {
                return int.Parse(ConfigurationManager.AppSettings["DefaultPaymentReferenceUses"]);
            }
        }

        public static string SMTP
        {
            get
            {
                return ConfigurationManager.AppSettings["SMTP"];
            }
        }

        public static string SupportFromAddress
        {
            get
            {
                return ConfigurationManager.AppSettings["SupportFromAddress"];
            }
        }

        public static string SupportToAddress
        {
            get
            {
                return ConfigurationManager.AppSettings["SupportToAddress"];
            }
        }


        


        
        
    }
}