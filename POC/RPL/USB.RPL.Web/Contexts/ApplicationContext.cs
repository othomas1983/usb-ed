﻿using System;
using System.Web;
using USB.RPL.Domain.User;

namespace USB.RPL.Web.Contexts
{

    /// <summary>
    /// Used as easy to use easy to debug 'statefull' application context 
    /// </summary>
    public class ApplicationContext : IBindableContext
    {
        #region Properties

        /// <summary>
        /// Gets the RPL application id
        /// </summary>
        public int ApplicationId { get; set; }

        /// <summary>
        /// Returns the current ApplicationContext within the HttpSession
        /// </summary>
        public static ApplicationContext Instance
        {
            get
            {
                var session = HttpContext.Current.Session;

                if (session.Contains(SessionKeys.ApplicationContext))
                {
                    return session[SessionKeys.ApplicationContext] as ApplicationContext;
                }

                return null;
            }
        }

        /// <summary>
        /// Gets the session timeout value in minutes
        /// </summary>
        public int SessionTimeout { get; set; }

        /// <summary>
        /// Provides access to the Member Identity data
        /// </summary>
        public UserIdentity CurrentUser { get; set; }

        /// <summary>
        /// Provides access to the Authentication State
        /// </summary>
        //public AuthenticationContext AuthenticationContext { get; set; }

        public string ServerName { get; set; }
       
        #endregion

        #region Methods

        /// <summary>
        /// Clears the AuthenticationContext
        /// </summary>
        public void Clear()
        {
            this.CurrentUser = null;
        }

        #endregion

        #region IBindableContext Members

        public IBindableContext Bind(object instance)
        {
            var user = instance as UserProfile;

            if (user.IsNotNull())
            {
                CurrentUser = new UserIdentity(user);
            };
            
            return this;
        }

        #endregion
    }


 


}