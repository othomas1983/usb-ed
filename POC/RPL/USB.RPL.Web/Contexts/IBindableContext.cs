﻿
namespace USB.RPL.Web.Contexts
{
    public interface IBindableContext
    {
        IBindableContext Bind(object instance);
    }
}