﻿using System.Collections.Generic;
using USB.RPL.Domain.User;
using System;

namespace USB.RPL.Web.Contexts
{
    /// <summary>
    /// Represents a User's Identity within a given context
    /// </summary>
    public class UserIdentity 
    {
        #region Constructors

        public UserIdentity(UserProfile user)
        {
            UserId = user.Id;
            Name = user.FirstName;
            Surname = user.LastName;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the user id.
        /// </summary>
        /// <value>The user id.</value>
        public long? UserId { get; private set; }

        public string Name { get; private set; }

        public string Surname { get; private set; }

        #endregion
        
    }
}