﻿using System;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using System.Web.Routing;
using Combres;
using USB.RPL.Web.Properties;
using Microsoft.Web.Mvc.AspNet4;
using USB.RPL.Web.Contexts;
using USB.RPL.Web.Controllers;
using USB.RPL.Web.Services;
using System.Web.Security;
using System.Security.Principal;
using System.Web;

namespace USB.RPL.Web
{

    public class MvcApplication : System.Web.HttpApplication
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
        protected void Application_Start()
        {

            var cult = CultureInfo.GetCultures(CultureTypes.AllCultures).FirstOrDefault(c => c.Name.Equals("en", StringComparison.InvariantCultureIgnoreCase));

            if (cult.IsNotNull())
            {
                LabelText.Culture = cult;
                ErrorLabels.Culture = cult;
                ContentLabels.Culture = cult;
            }

            Bootstrapper.Run();

            //this is added so that we can take advantage of the MVC Futures library
            ModelMetadataProviders.Current = new DataAnnotations4ModelMetadataProvider();
            DataAnnotations4ModelValidatorProvider.RegisterProvider();

        }

        protected void Session_Start()
        {
            var applicationId = int.Parse(ConfigurationManager.AppSettings["RPL.ApplicationId"], CultureInfo.InvariantCulture);

            var context = new ApplicationContext()
            {
                ApplicationId = applicationId,
                ServerName = Server.MachineName
            };

            Session[SessionKeys.ApplicationContext] = context;
        }

    }

    #region Bootstrapper

    public class Bootstrapper
    {
        protected Bootstrapper()
        {
            var container = new CompositionContainer(new AssemblyCatalog(Assembly.GetExecutingAssembly()));
            container.ComposeParts(this);
        }

        protected void Initialize()
        {
            new InitSystem().Execute();
            new RegisterRoutesTask().Execute();
            new RegisterControllerFactoryTask().Execute();
        }

        public static void Run()
        {
            new Bootstrapper().Initialize();
        }
    }

    public interface IBootstrapperTask
    {
        void Execute();
    }

    public class InitSystem : IBootstrapperTask
    {
        public void Execute()
        {
           // do any caching or initialisation here
        }
    }

    public class RegisterRoutesTask : IBootstrapperTask
    {
        private readonly RouteCollection routes;

        #region Constructors

        public RegisterRoutesTask()
            : this(RouteTable.Routes)
        {

        }

        public RegisterRoutesTask(RouteCollection routes)
        {
            this.routes = routes;
        }

        #endregion

        #region IBootstrapperTask Members

        public void Execute()
        {
            routes.AddCombresRoute("Combres");

            AreaRegistration.RegisterAllAreas();

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.IgnoreRoute("getstarted/{*pathInfo}");

            routes.IgnoreRoute("content/{*pathInfo}");

            routes.IgnoreRoute("images/{*pathInfo}");

            routes.IgnoreRoute("scripts/{*pathInfo}");

            routes.IgnoreRoute("{*favicon}", new { favicon = @"(.*/)?favicon.ico(/.*)?" });

            routes.MapRoute("Default", "{controller}/{action}/{id}", new { controller = "Home", action = "Index", id = UrlParameter.Optional });
        }

        #endregion
    }

    public class RegisterControllerFactoryTask : IBootstrapperTask
    {
        #region IBootstrapperTask Members

        public void Execute()
        {
            ControllerBuilder.Current.SetControllerFactory(new ControllerFactory());
        }

        #endregion
    }
    
    #endregion
}
