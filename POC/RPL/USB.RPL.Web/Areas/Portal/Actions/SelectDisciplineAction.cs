﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using USB.RPL.Web.Actions;
using System.Web.Mvc;
using USB.RPL.Web.Services;
using USB.RPL.Web.Areas.Portal.Models;
using Airborne;
using USB.RPL.Web.Controllers;
using USB.RPL.Domain.User;

namespace USB.RPL.Web.Areas.Portal.Actions
{
    public class SelectDisciplineAction : RPLControllerAction
    {
         #region Constructors

        public SelectDisciplineAction(IRPLClientServicesProvider client)
            : base(client)
        {
        }

        #endregion

        #region Properties

        public Func<PortalModel, ViewResult> ShowView { get; set; }

        public int SelectedDiscipline { get; set; }
        public string NewDiscipline { get; set; }

        #endregion

        #region Methods

        public override ActionResult Invoke(RPLController controller)
        {
            Guard.ArgumentNotNull(controller, "controller");

            var model = new PortalModel();
            var profile = RPLClient.Cache.Get<UserProfile>("UserProfile");
            var basket = RPLClient.Cache.Get<UserProfileBasket>("Basket");

            if (SelectedDiscipline != 0)
            {
                var discipline = RPLClient.Users.GetDiscipline(SelectedDiscipline);
                basket.Discipline = discipline;
            }
            else
            {
                var discipline = new Discipline() { Name = NewDiscipline, LearningTypeId = basket.LearningType.Id };
                RPLClient.Users.SaveDiscipline(discipline);
                basket.Discipline = discipline;
            }

            RPLClient.Cache.Save("UserProfile", profile);
            RPLClient.Cache.Save("Basket", basket);
            model.Courses = RPLClient.Users.Courses(basket.LearningType.Id, basket.Discipline.Id);
            model.Courses.Add(new Course() { Id = 0, Name = "Other", IsVetted = true });
            model.BasketItem = basket;
            model.Bind(profile);
            return ShowView.Invoke(model);
        }

        #endregion
    }
}