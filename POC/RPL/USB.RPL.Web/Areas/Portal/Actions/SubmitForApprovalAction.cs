﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using USB.RPL.Web.Actions;
using System.Web.Mvc;
using USB.RPL.Web.Services;
using USB.RPL.Web.Areas.Portal.Models;
using Airborne;
using USB.RPL.Web.Controllers;
using USB.RPL.Domain.User;
using System.IO;

namespace USB.RPL.Web.Areas.Portal.Actions
{
    public class SubmitForApprovalAction : RPLControllerAction<PortalModel>
    {
         #region Constructors

        public SubmitForApprovalAction(IRPLClientServicesProvider client)
            : base(client)
        {
        }

        #endregion

        #region Properties

        public HttpPostedFileBase File { get; set; }

        public Func<PortalModel, ViewResult> ShowView { get; set; }

        #endregion

        #region Methods

        public override ActionResult Execute(RPLController controller, PortalModel model)
        {
            Guard.ArgumentNotNull(controller, "controller");

            var profile = RPLClient.Cache.Get<UserProfile>("UserProfile");
            var loadedProfile = RPLClient.Users.LoadUserProfile(profile.Id);

            if (File.IsNotNull() && File.InputStream.Length > 0)
            {
                byte[] fileData = null;
                using (var inputStream = File.InputStream)
                {
                    var memoryStream = inputStream as MemoryStream;
                    if (memoryStream == null)
                    {
                        memoryStream = new MemoryStream();
                        inputStream.CopyTo(memoryStream);
                    }
                    fileData = memoryStream.ToArray();
                }
                var dotIndex = File.FileName.IndexOf('.');
                var extension = File.FileName.Substring(dotIndex + 1, (File.FileName.Length) - (dotIndex + 1));
                var fileName = File.FileName.Substring(0, dotIndex);
                loadedProfile.OtherDocuments.Add(new SupportingDocument() { Document = fileData, Name = fileName, Extension = extension, UserId = profile.Id });
            }

            loadedProfile.Comment = model.Comments;
            var userPaymentReference = RPLClient.Users.GetUserPaymentReference(model.PaymentReference);
            var paymentReference = RPLClient.Users.GetPaymentReference(model.PaymentReference);
            if ((userPaymentReference.IsNotNull() && userPaymentReference.UserId != loadedProfile.Id) || paymentReference.IsNull())
            {
                model.Error = "Payment reference invalid.";
            }
            else if (userPaymentReference.IsNotNull() && userPaymentReference.UserId == loadedProfile.Id && userPaymentReference.TimesUsed >= Config.DefaultPaymentReferenceUses)
            {
                model.Error = "Payment reference has reached its usage limit of {0}.".FormatInvariantCulture(Config.DefaultPaymentReferenceUses);
            }
            else if (userPaymentReference.IsNotNull() && userPaymentReference.UserId == loadedProfile.Id && userPaymentReference.TimesUsed < Config.DefaultPaymentReferenceUses)
            {
                var reference = loadedProfile.PaymentReferences.First();
                reference.TimesUsed += 1;
                var notifcations = RPLClient.Users.SaveUserProfile(loadedProfile);
            }
            else
            {
                loadedProfile.PaymentReferences.Add(new UserPaymentReference() { TimesUsed = 1, UserId = loadedProfile.Id, PaymentReference = paymentReference });
                var notifcations = RPLClient.Users.SaveUserProfile(loadedProfile);
            }
            if (model.Error.IsNullOrEmpty())
            {
                EmailService.SendApprovalEmail(loadedProfile);
            }
            RPLClient.Cache.Save("UserProfile", loadedProfile);
            
            model.ReadOnlyBasket = false;
            model.Bind(profile);
            return ShowView.Invoke(model);
        }

        #endregion
    }
}