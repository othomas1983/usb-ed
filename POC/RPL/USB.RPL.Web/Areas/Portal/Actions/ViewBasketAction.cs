﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using USB.RPL.Web.Actions;
using System.Web.Mvc;
using USB.RPL.Web.Services;
using USB.RPL.Web.Areas.Portal.Models;
using Airborne;
using USB.RPL.Web.Controllers;
using USB.RPL.Domain.User;

namespace USB.RPL.Web.Areas.Portal.Actions
{
    public class ViewBasketAction : RPLControllerAction
    {
         #region Constructors

        public ViewBasketAction(IRPLClientServicesProvider client)
            : base(client)
        {
        }

        #endregion

        #region Properties

        public Func<PortalModel, ViewResult> ShowView { get; set; }

        #endregion

        #region Methods

        public override ActionResult Invoke(RPLController controller)
        {
            Guard.ArgumentNotNull(controller, "controller");

            var model = new PortalModel();
            var profile = RPLClient.Cache.Get<UserProfile>("UserProfile");
            var basket = RPLClient.Cache.Get<UserProfileBasket>("Basket");
            model.ReadOnlyBasket = true;
            model.BasketItem = basket;
            model.Bind(profile);
            return ShowView.Invoke(model);
        }

        #endregion
    }
}