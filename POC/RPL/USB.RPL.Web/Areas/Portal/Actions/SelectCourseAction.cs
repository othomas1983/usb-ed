﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using USB.RPL.Web.Actions;
using System.Web.Mvc;
using USB.RPL.Web.Services;
using USB.RPL.Web.Areas.Portal.Models;
using Airborne;
using USB.RPL.Web.Controllers;
using USB.RPL.Domain.User;

namespace USB.RPL.Web.Areas.Portal.Actions
{
    public class SelectCourseAction : RPLControllerAction
    {
         #region Constructors

        public SelectCourseAction(IRPLClientServicesProvider client)
            : base(client)
        {
        }

        #endregion

        #region Properties

        public Func<PortalModel, ViewResult> ShowView { get; set; }

        public int SelectedCourse { get; set; }
        public string NewCourse { get; set; }

        #endregion

        #region Methods

        public override ActionResult Invoke(RPLController controller)
        {
            Guard.ArgumentNotNull(controller, "controller");

            var model = new PortalModel();
            var profile = RPLClient.Cache.Get<UserProfile>("UserProfile");
            var basket = RPLClient.Cache.Get<UserProfileBasket>("Basket");

            if (SelectedCourse != 0)
            {
                var course = RPLClient.Users.GetCourse(SelectedCourse);
                basket.Course = course;
            }
            else
            {
                int? disciplineId = null;
                if (basket.Discipline.IsNotNull())
                {
                    disciplineId = basket.Discipline.Id;
                }
                var course = new Course() { Name = NewCourse, LearningTypeId = basket.LearningType.Id, DisciplineId = disciplineId };
                RPLClient.Users.SaveCourse(course);
                basket.Course = course;
            }

            RPLClient.Cache.Save("UserProfile", profile);
            RPLClient.Cache.Save("Basket", basket);
            model.CourseStatuses = RPLClient.Users.CourseStatuses();
            model.Institutions = RPLClient.Users.Institutions(basket.LearningType.Id);
            model.Institutions.Add(new Institution() { Id = 0, Name = "Other", IsVetted = true });
            model.BasketItem = basket;
            model.Bind(profile);
            return ShowView.Invoke(model);
        }

        #endregion
    }
}