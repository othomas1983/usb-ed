﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using USB.RPL.Web.Actions;
using System.Web.Mvc;
using USB.RPL.Web.Services;
using USB.RPL.Web.Areas.Portal.Models;
using Airborne;
using USB.RPL.Web.Controllers;
using USB.RPL.Domain.User;
using System.IO;

namespace USB.RPL.Web.Areas.Portal.Actions
{
    public class SaveProfileAction : RPLControllerAction<PortalModel>
    {
        #region Constructors

        public SaveProfileAction(IRPLClientServicesProvider client)
            : base(client)
        {
        }

        #endregion

        #region Properties

        public Func<PortalModel, ViewResult> ShowView { get; set; }

        public HttpPostedFileBase File { get; set; }

        #endregion

        #region Methods

        public override ActionResult Execute(RPLController controller, PortalModel model)
        {
            Guard.ArgumentNotNull(controller, "controller");
            Guard.ArgumentNotNull(model, "model");

            var profile = RPLClient.Cache.Get<UserProfile>("UserProfile");
            var basket = RPLClient.Cache.Get<UserProfileBasket>("Basket");

            var loadedProfile = RPLClient.Users.LoadUserProfile(profile.Id);

            basket.CourseReference = model.BasketItem.CourseReference;
            basket.CourseReferencePerson = model.BasketItem.CourseReferencePerson;
            basket.CourseStatus = RPLClient.Users.GetCourseStatus(model.CourseStatus);

            basket.DateOfCompletion = model.BasketItem.DateOfCompletion;
            basket.DateOfEmployment = model.BasketItem.DateOfEmployment;
            basket.DegreeLevel = model.BasketItem.DegreeLevel;
            basket.EmployedAs = model.BasketItem.EmployedAs;

            basket.EmployedBy = model.BasketItem.EmployedBy;
            basket.HighestGrade = model.BasketItem.HighestGrade;
            basket.HighestGradeSchool = model.BasketItem.HighestGradeSchool;
            if (model.Institution.HasValue)
            {
                if (model.Institution != 0)
                {
                    basket.Institution = RPLClient.Users.GetInstitution(model.Institution.Value);
                }
                else
                {
                    var institution = new Institution() { Name = model.NewInstitution, LearningTypeId = basket.LearningType.Id };
                    RPLClient.Users.SaveInstitution(institution);
                    basket.Institution = institution;
                }
            }
            basket.InstitutionAddress = model.BasketItem.InstitutionAddress;
            basket.ReferenceContactDetails = model.BasketItem.ReferenceContactDetails;
            basket.SecurityIndustry = model.BasketItem.ReferenceContactDetails;


            if (File.IsNotNull() && File.InputStream.Length > 0)
            {
                byte[] fileData = null;
                using (var inputStream = File.InputStream)
                {
                    var memoryStream = inputStream as MemoryStream;
                    if (memoryStream == null)
                    {
                        memoryStream = new MemoryStream();
                        inputStream.CopyTo(memoryStream);
                    }
                    fileData = memoryStream.ToArray();
                }
                var dotIndex = File.FileName.IndexOf('.');
                var extension = File.FileName.Substring(dotIndex + 1, (File.FileName.Length) - (dotIndex + 1));
                var fileName = File.FileName.Substring(0, dotIndex);
                basket.Document = fileData;
                basket.DocumentName = fileName;
                basket.Extension = extension;
            }

            basket.UserId = loadedProfile.Id;

            loadedProfile.Basket.Add(basket);

            var notifcations = RPLClient.Users.SaveUserProfile(loadedProfile);
            if (!notifcations.HasErrors())
            {
                RPLClient.Cache.Evict<UserProfileBasket>(basket);
                model.InfoSaved = true;
            }
            else
            {
                model.InfoSaved = false;
            }
            model.CourseStatuses = RPLClient.Users.CourseStatuses();
            model.Institutions = RPLClient.Users.Institutions(basket.LearningType.Id);
            model.Institutions.Add(new Institution() { Id = 0, Name = "Other" });
            model.BasketItem = basket;
            RPLClient.Cache.Save("UserProfile", loadedProfile);
            model.Bind(loadedProfile);
            return ShowView.Invoke(model);
        }

        #endregion

        
    }
}