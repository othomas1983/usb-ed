﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using USB.RPL.Web.Actions;
using System.Web.Mvc;
using USB.RPL.Web.Services;
using USB.RPL.Web.Areas.Portal.Models;
using Airborne;
using USB.RPL.Web.Controllers;
using USB.RPL.Domain.User;
using System.IO;

namespace USB.RPL.Web.Areas.Portal.Actions
{
    public class UploadDocumentAction : RPLControllerAction<PortalModel>
    {
        #region Constructors

        public UploadDocumentAction(IRPLClientServicesProvider client)
            : base(client)
        {
        }

        #endregion

        #region Properties

        public Func<PortalModel, ViewResult> ShowView { get; set; }

        public HttpPostedFileBase File { get; set; }

        #endregion

        #region Methods

        public override ActionResult Execute(RPLController controller, PortalModel model)
        {
            Guard.ArgumentNotNull(controller, "controller");

            var profile = RPLClient.Cache.Get<UserProfile>("UserProfile");
            var basket = RPLClient.Cache.Get<UserProfileBasket>("Basket");
            if (File.IsNotNull() && File.InputStream.Length > 0)
            {
                byte[] fileData = null;
                using (var inputStream = File.InputStream)
                {
                    var memoryStream = inputStream as MemoryStream;
                    if (memoryStream == null)
                    {
                        memoryStream = new MemoryStream();
                        inputStream.CopyTo(memoryStream);
                    }
                    fileData = memoryStream.ToArray();
                }
                var dotIndex = File.FileName.IndexOf('.');
                var extension = File.FileName.Substring(dotIndex + 1, (File.FileName.Length) - (dotIndex + 1));
                var fileName = File.FileName.Substring(0, dotIndex);
                basket.Document = fileData;
                basket.DocumentName = fileName;
                basket.Extension =  extension;
            }
            RPLClient.Cache.Save("UserProfile", profile);
            RPLClient.Cache.Save("UserProfile", basket);

            model.CourseStatuses = RPLClient.Users.CourseStatuses();
            model.Institutions = RPLClient.Users.Institutions(basket.LearningType.Id);
            model.Institutions.Add(new Institution() { Id = 0, Name = "Other" });
            model.BasketItem = basket;
            model.Bind(profile);
            return ShowView.Invoke(model);
        }

        #endregion
    }
}