﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using USB.RPL.Web.Actions;
using System.Web.Mvc;
using USB.RPL.Web.Services;
using USB.RPL.Web.Areas.Portal.Models;
using Airborne;
using USB.RPL.Web.Controllers;
using USB.RPL.Domain.User;

namespace USB.RPL.Web.Areas.Portal.Actions
{
    public class SelectLearningTypeAction : RPLControllerAction
    {
         #region Constructors

        public SelectLearningTypeAction(IRPLClientServicesProvider client)
            : base(client)
        {
        }

        #endregion

        #region Properties

        public Func<PortalModel, ViewResult> ShowView { get; set; }

        public int SelectedLearningType { get; set; }

        #endregion

        #region Methods

        public override ActionResult Invoke(RPLController controller)
        {
            Guard.ArgumentNotNull(controller, "controller");

            var model = new PortalModel();
            var profile = RPLClient.Cache.Get<UserProfile>("UserProfile");

            var selectedLearningType = RPLClient.Users.GetLearningType(SelectedLearningType);
            
            var basket = new UserProfileBasket() { LearningType = selectedLearningType };

            RPLClient.Cache.Save("UserProfile", profile);
            RPLClient.Cache.Save("Basket", basket);

            if (basket.LearningType.Name == "Schooling Information"
                       || basket.LearningType.Name == "General Education"
                       || basket.LearningType.Name == "Tertiary Education")
            {
                basket.Discipline = null;
                model.Courses = RPLClient.Users.Courses(basket.LearningType.Id, null);
                model.Courses.Add(new Course() { Id = 0, Name = "Other", IsVetted = true });
            }
            else
            {
                model.Disciplines = RPLClient.Users.Disciplines(basket.LearningType.Id);
                model.Disciplines.Add(new Discipline() { Id = 0, Name = "Other", IsVetted = true });
            }

            model.BasketItem = basket;
            model.Bind(profile);
            return ShowView.Invoke(model);
        }

        #endregion
    }
}