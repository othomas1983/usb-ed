﻿using System.Web.Mvc;
using System;
using USB.RPL.Web.Controllers;
using USB.RPL.Web.Extensions;
using USB.RPL.Web.Areas.Portal.Actions;
using System.Web;
using System.IO;
using USB.RPL.Web.Areas.Portal.Models;
using USB.RPL.Domain.User;

namespace USB.RPL.Web.Areas.Portal.Controllers
{
    public class PortalController : RPLController
    {
        #region Properties

        public override string DisplayName
        {
            get { return "Portal"; }
        }

        #endregion

        #region Controller Actions


        [Authentication()]
        public ActionResult Index()
        {
            return View("Guide", new PortalModel());
        }

        [Authentication()]
        public ActionResult LearningType()
        {
            var model = new PortalModel();
            var profile = RPLClient.Cache.Get<UserProfile>("UserProfile");
            model.Bind(profile);
            return View("LearningType", model);
        }

        [Authentication()]
        public ActionResult SelectLearningType(int learningType)
        {
            return new SelectLearningTypeAction(RPLClient)
            {
                SelectedLearningType = learningType,
                ShowView = (viewModel) =>
                {
                    var basket = RPLClient.Cache.Get<UserProfileBasket>("Basket");
                    if (basket.LearningType.Name == "Schooling Information"
                        || basket.LearningType.Name == "General Education"
                        || basket.LearningType.Name == "Tertiary Education")
                    {
                        return View("Course", viewModel);
                    }
                    else
                    {
                        return View("Discipline", viewModel);
                    }
                }
            }.Invoke(this);
        }

        [Authentication()]
        public ActionResult SelectedDiscipline(int discipline, string newDiscipline)
        {
            return new SelectDisciplineAction(RPLClient)
            {
                SelectedDiscipline = discipline,
                NewDiscipline = newDiscipline,
                ShowView = (viewModel) =>
                {
                    return View("Course", viewModel);
                }
            }.Invoke(this);

        }

        [Authentication()]
        public ActionResult SelectedCourse(int course, string newCourse)
        {
            return new SelectCourseAction(RPLClient)
            {
                SelectedCourse = course,
                NewCourse = newCourse,
                ShowView = (viewModel) =>
                {
                    return View("CatalogueInformation", viewModel);
                }
            }.Invoke(this);

        }

        [Authentication()]
        public ActionResult UploadFile(HttpPostedFileBase file, PortalModel model)
        {
            return new UploadDocumentAction(RPLClient)
            {
                File = file,
                ShowView = (viewModel) =>
                {
                    return View("CatalogueInformation", viewModel);
                }
            }.Execute(this, model);
        }

        [Authentication()]
        public ActionResult SaveBasket(HttpPostedFileBase file, PortalModel model)
        {
            return new SaveProfileAction(RPLClient)
            {
                File = file,
                ShowView = (viewModel) =>
                {
                    return View("CatalogueInformation", viewModel);
                }
            }.Execute(this, model);
        }

        [Authentication()]
        public ActionResult ViewBasket()
        {
            return new ViewBasketAction(RPLClient)
            {
                ShowView = (viewModel) =>
                {
                    return View("Approval", viewModel);
                }
            }.Invoke(this);

        }

        [Authentication()]
        public ActionResult ApproveBasket()
        {
            return new ApprovalBasketAction(RPLClient)
            {
                ShowView = (viewModel) =>
                {
                    return View("Approval", viewModel);
                }
            }.Invoke(this);

        }

        [Authentication()]
        public ActionResult SumitForApproval(HttpPostedFileBase file, PortalModel model)
        {
            return new SubmitForApprovalAction(RPLClient)
            {
                File = file,
                ShowView = (viewModel) =>
                {
                    if (viewModel.Error.IsNullOrEmpty())
                    {
                        return View("ApprovalComplete", viewModel);
                    }
                    else
                    {
                        return View("Approval", viewModel);
                    }
                }
            }.Execute(this, model);

        }
              
        #endregion
    }
}
