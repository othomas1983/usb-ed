﻿using System;
using System.Linq;
using USB.RPL.Domain.User;
using System.Collections.Generic;

namespace USB.RPL.Web.Areas.Portal.Models
{
    public class PortalModel
    {
        public UserProfile Profile { get; set; }

        public IList<Discipline> Disciplines { get; set; }

        public IList<Course> Courses { get; set; }

        public IList<Institution> Institutions { get; set; }

        public IList<CourseStatus> CourseStatuses { get; set; }

        public UserProfileBasket BasketItem { get; set; }

        public string NewInstitution { get; set; }

        public int? Institution { get; set; }

        public int CourseStatus { get; set; }

        public bool? InfoSaved { get; set; }

        public bool ReadOnlyBasket { get; set; }

        public string PaymentReference { get; set; }

        public string Comments { get; set; }

        public string Error { get; set; }

        public void Bind(UserProfile profile)
        {
            Profile = profile;
        }
    }
}