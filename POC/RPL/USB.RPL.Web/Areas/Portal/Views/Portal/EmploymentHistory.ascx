﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<USB.RPL.Web.Areas.Portal.Models.PortalModel>" %>
<script type="text/javascript">

    $(document).ready(function () {

        $(function () {
            var dates = $('#DateOfEmployment').datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                showOn: 'button',
                buttonImage: '../Content/gfx/calendar.gif',
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                dateFormat: 'dd/mm/yy',
                minDate: Infinity,
                onSelect: function (selectedDate) {
                    var option = this.id == "dateFrom" ? "minDate" : "maxDate";
                    var instance = $(this).data("datepicker");
                    var date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
                    dates.not(this).datepicker("option", option, date);
                }
            });
        });

    });        
</script>
<table class="levels" width="50%">
			<tr>
				<td>Employed by:</td>
				<td><%: Html.TextBoxFor(m => m.BasketItem.EmployedBy) %></td>
			</tr>
			<tr>
				<td>Date of employment:</td>
				<td><div style="width: 250px;display: block;height: 35px;"><%: Html.TextBoxFor(m => m.BasketItem.DateOfEmployment, new { id = "DateOfEmployment" })%></div></td>
			</tr>
			<tr>
				<td>Employed as:</td>
				<td><%: Html.TextBoxFor(m => m.BasketItem.EmployedAs) %></td>
			</tr>
			<tr>
				<td>Reference/Contact Details</td>
				<td><%: Html.TextBoxFor(m => m.BasketItem.ReferenceContactDetails) %></td>
			</tr>
            
            <tr>
				<td>Upload File:</td>
				<td><input name="file" id="file" type="file" /></td>
			</tr>

		</table>	