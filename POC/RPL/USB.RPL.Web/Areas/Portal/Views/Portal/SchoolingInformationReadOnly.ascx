﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<USB.RPL.Domain.User.UserProfileBasket>" %>
<table>
<tr>
    <td>
        <strong>Selected Course:&nbsp;</strong>
    </td>
    <td><%= Model.Course.IsNotNull() ? Model.Course.Name : string.Empty%></td>
</tr>
<tr>
    <td>
        &nbsp;
    </td>
    <td>
        <table class="learningData">
            <tr>
                <td>
                    Institution:
                </td>
                <td>
                    <%= Model.Institution.IsNotNull() ? Model.Institution.Name : string.Empty %>
                </td>
            </tr>
            <tr>
                <td>
                    Highest Grade:
                </td>
                <td>
                     <%= Model.HighestGrade %>
                </td>
            </tr>
            <tr>
                <td>
                    School where highest grade was achieved:
                </td>
                <td>
                    <%= Model.HighestGradeSchool %>
                </td>
            </tr>
            <tr>
                <td>
                    Address of school:
                </td>
                <td>
                    <%= Model.InstitutionAddress %>
                </td>
            </tr>
        </table>
    </td>
</tr>
</table>