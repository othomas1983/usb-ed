﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RPL.Master" Inherits="System.Web.Mvc.ViewPage<USB.RPL.Web.Areas.Portal.Models.PortalModel>" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="Header" runat="server">
<script type="text/javascript">
    $(document).ready(function () {

        $(".stepLink").attr('disabled', 'disabled');
        $(".step").attr('class', 'stepD');
        $("#welcomeBreadCrumb").removeAttr('disabled').parent().attr('class', 'step');
        $("#chooseLearningBreadCrumb").removeAttr('disabled').parent().attr('class', 'step');
    });
 </script>
</asp:Content>

<%--<asp:Content ID="SubBreadCrumbsContent" ContentPlaceHolderID="SubBreadCrumbs" runat="server">
Wizard Guide > Choose Learning Type > Select Discipline > Select Course > Capture Info 
</asp:Content> --%>

<asp:Content ID="SubBreadCrumbsContent" ContentPlaceHolderID="SubBreadCrumbs" runat="server">
<%
     var pathToGuide = Url.Action("Index", new { controller = "Portal", area = "Portal" });
     var pathToLearningType = Url.Action("LearningType", new { controller = "Portal", area = "Portal" });
     var pathToDiscipline = Url.Action("SelectLearningType", new { controller = "Portal", area = "Portal" });
     
     var pathToCourse = "";

     if (Model.BasketItem.LearningType.Id == 4 || Model.BasketItem.LearningType.Id == 5 || Model.BasketItem.LearningType.Id == 6)
     {
         pathToCourse = Url.Action("SelectedDiscipline", new { controller = "Portal", area = "Portal" });
         pathToCourse += "?discipline=" + Model.BasketItem.Discipline.Id;
     }
     else
     {
         pathToCourse = Url.Action("SelectLearningType", new { controller = "Portal", area = "Portal" });
         pathToCourse += "?learningType=" + Model.BasketItem.LearningType.Id;
     }
     
     
    %>
<a href="<%=pathToGuide %>" >Wizard Guide</a> > <a href="<%=pathToLearningType %>">Choose Learning Type</a> 

<%if (Model.BasketItem.LearningType.Id == 4 || Model.BasketItem.LearningType.Id == 5 || Model.BasketItem.LearningType.Id == 6)
  {%>
 > <a href="<%=pathToDiscipline %>?learningType=<%= Model.BasketItem.LearningType.Id %>">Select Discipline</a>
 <%} %>
  > <a href="<%=pathToCourse %>" >Select Course</a>
  > Capture Info 
</asp:Content>



<asp:Content ID="PageContent" ContentPlaceHolderID="Content" runat="server">
<%
     var pathToLearningType = Url.Action("LearningType", new { controller = "Portal", area = "Portal" });
    %>
<div class="heading">
			<h1>Capture Info For Catalogue Learning Items:</h1>
		</div>
         <% using (Html.BeginForm("SaveBasket", "Portal", FormMethod.Post, new { id = "saveBasketForm", enctype="multipart/form-data" }))
            { %>

        <%if (Model.BasketItem.LearningType.Id == 1)
          { %>
          <% Html.RenderPartial("SchoolingInformation"); %>
        <%} %>
        <%if (Model.BasketItem.LearningType.Id == 2)
          { %>
          <% Html.RenderPartial("GeneralEducation"); %>
        <%} %>
        <%if (Model.BasketItem.LearningType.Id == 3)
          { %>
          <% Html.RenderPartial("TertiaryEducation"); %>
        <%} %>
        <%if (Model.BasketItem.LearningType.Id == 4)
          { %>
          <% Html.RenderPartial("SectorSpecificEducation"); %>
        <%} %>
        <%if (Model.BasketItem.LearningType.Id == 5)
          { %>
          <% Html.RenderPartial("VocationalEducation"); %>
        <%} %>
        <%if (Model.BasketItem.LearningType.Id == 6)
          { %>
          <% Html.RenderPartial("EmploymentHistory"); %>
        <%} %>
		

		    <br />
		    <div class="c"></div>
			<div class="button fr"><input name="Submit" type="submit" value="Save to Basket" /></div>	
            <%} %>

            <%if (Model.InfoSaved.HasValue && Model.InfoSaved.Value)
            { %>
           
			<div id="confirmationBox" class="loginRight regContent fr notifyBox s" style="width:150px;display:none; margin:15px 5px 5px 5px">
				<div class="statusIcon success "></div>
				<h3>Your information has been saved to your basket</h3>
				<div class="c"></div>
					<p>Please follow the steps to add more items to your basket.</p>
			</div>
             <script type="text/javascript">
                 $(document).ready(function () {
                     $('#confirmationBox').dialog({ width:380, autoOpen: true, modal: true, draggable: false, resizable: false,
                         buttons: [{
                             text: "Ok",
                             click: function () { $(this).dialog("close"); location.href = '<%= pathToLearningType%>'; }
                         }],
                         close:function() 
                        {
                            
                            
                            location.href = '<%= pathToLearningType%>';
                        },

                     });
                 });
            </script>
		    <%} %>
            <%if (Model.InfoSaved.HasValue && !Model.InfoSaved.Value)
            { %>
           
			<div id="failDialog" class="loginRight regContent fr notifyBox e" style="width:150px; display:none; margin:15px 5px 5px 5px">
				<div class="statusIcon error"></div>
				<h3>Sorry your information is not saved</h3>
				<div class="c"></div>
					<p>Please try again, if the problem presists please contact the system administrator.</p>
			</div>

             <script type="text/javascript">
                 $(document).ready(function () {
                     $('#failDialog').dialog({ width: 300, autoOpen: true, modal: true, draggable: false, resizable: false,
                         buttons: [{
                             text: "Ok",
                             click: function () { $(this).dialog("close"); }
                         }]
                     });
                 });
            </script>
		    <%} %>
</asp:Content>                                                         