﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<USB.RPL.Domain.User.UserProfileBasket>" %>
<table>
<tr>
    <td>
        <strong>Selected Discipline:&nbsp;</strong>
    </td>
    <td><%= Model.Discipline.IsNotNull() ? Model.Discipline.Name : string.Empty%></td>
</tr>
<tr>
    <td>
        <strong>Selected Course:&nbsp;</strong>
    </td>
    <td><%= Model.Course.IsNotNull() ? Model.Course.Name : string.Empty%></td>
</tr>
<tr>
    <td>
         &nbsp;
    </td>
    <td>
        <table class="learningData">
            <tr>
                <td>
                    Employed by:
                </td>
                <td>
                   <%= Model.EmployedBy %>
                </td>
            </tr>
            <tr>
                <td>
                    Date of employment:
                </td>
                <td>
                    <%= Model.DateOfEmployment.IsNotNull() ? Model.DateOfEmployment.Value.ToString("dd/MM/yyyy") : "" %>
                </td>
            </tr>
            <tr>
                <td>
                    Employed as:
                </td>
                <td>
                    <%= Model.EmployedAs %>
                </td>
            </tr>
            <tr>
                <td>
                    Reference/Contact Details
                </td>
                <td>
                    <%= Model.ReferenceContactDetails %>
                </td>
            </tr>
        </table>
    </td>
</tr>
</table>
