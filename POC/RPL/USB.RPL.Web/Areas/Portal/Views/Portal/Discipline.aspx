﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RPL.Master" Inherits="System.Web.Mvc.ViewPage<USB.RPL.Web.Areas.Portal.Models.PortalModel>" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="Header" runat="server">
<script type="text/javascript">
    $(document).ready(function () {

        $(".stepLink").attr('disabled', 'disabled');
        $(".step").attr('class', 'stepD');
        $("#welcomeBreadCrumb").removeAttr('disabled').parent().attr('class', 'step');
        $("#chooseLearningBreadCrumb").removeAttr('disabled').parent().attr('class', 'step');

        $('#discipline').change(function () {
            if ($(this).val() == 0) {
                $("#newDiscipline").show();
            } else {
                $("#newDiscipline").hide();
            }
        });
        if ($('#discipline').val() == 0) {
            $("#newDiscipline").show();
        } else {
            $("#newDiscipline").hide();
        }
        $("#newDiscipline").mouseup(function (e) {
            e.preventDefault();
            $(this).select();
        });
    });
 </script>
</asp:Content>

<asp:Content ID="TitleContent" ContentPlaceHolderID="Title" runat="server">
RPL - Choose Learning
</asp:Content> 
<asp:Content ID="SubBreadCrumbsContent" ContentPlaceHolderID="SubBreadCrumbs" runat="server">
<%
     var pathToGuide = Url.Action("Index", new { controller = "Portal", area = "Portal" });
     var pathToLearningType = Url.Action("LearningType", new { controller = "Portal", area = "Portal" });
    %>
<a href="<%=pathToGuide %>" >Wizard Guide</a> > <a href="<%=pathToLearningType %>">Choose Learning Type</a> > Select Discipline 
</asp:Content> 
<asp:Content ID="PageContent" ContentPlaceHolderID="Content" runat="server">
<div class="heading">
			<h1>Select Discipline:</h1>
		</div>

         <% using (Html.BeginForm("SelectedDiscipline", "Portal", FormMethod.Post, new { id = "disciplineForm" }))
            { %>
		<div class="divisionSelect">
			<h1 class="divHeader">Please select your discipline:</h1>
			<span>
				<select name="discipline" id="discipline" class="selectDiv">
                    <%
                        var vettedDisciplines = Model.Disciplines.Where(c => c.IsVetted);
                        var notVettedDisciplines = Model.Disciplines.Where(c => !c.IsVetted);
                     %>

                     <% if (vettedDisciplines.IsNotNull() && vettedDisciplines.Count() > 0)
                        { %>
                        <optgroup label="Vetted">
					        <%foreach (var discipline in vettedDisciplines)
                            { %>
                            <option value="<%=discipline.Id %>"><%= discipline.Name%></option>
                            <%} %>
                        </optgroup>
                        <%} %>

                     <% if (notVettedDisciplines.IsNotNull() && notVettedDisciplines.Count() > 0)
                        { %>
                        <optgroup label="Not Vetted">
                    	    <%foreach (var discipline in notVettedDisciplines)
                            { %>
                            <option value="<%=discipline.Id %>"><%= discipline.Name%></option>
                            <%} %>
                        </optgroup>
                    <%} %>

				</select>
				
				<input name="newDiscipline" id="newDiscipline" type="text" value="Please enter discipline" />
				
			</span>
		</div>


    <br />
    <div class="c"></div>
	<div class="button fr"><input name="Submit" type="submit" value="Next" /></div>
	<%} %>
	
<div class="c"></div>

</asp:Content>                                                         