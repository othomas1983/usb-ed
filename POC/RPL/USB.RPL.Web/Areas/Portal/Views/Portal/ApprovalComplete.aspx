﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RPL.Master" Inherits="System.Web.Mvc.ViewPage<USB.RPL.Web.Areas.Portal.Models.PortalModel>" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="Header" runat="server">
<script type="text/javascript">
    $(document).ready(function () {

        $(".stepLink").attr('disabled', 'disabled');
        $(".step").attr('class', 'stepD');
        $("#welcomeBreadCrumb").removeAttr('disabled').parent().attr('class', 'step');
        $("#chooseLearningBreadCrumb").removeAttr('disabled').parent().attr('class', 'step');
        $("#approvalBreadCrumb").removeAttr('disabled').parent().attr('class', 'step');
        $("#approvalCompleteBreadCrumb").removeAttr('disabled').parent().attr('class', 'step');
    });
 </script>
</asp:Content>

<asp:Content ID="TitleContent" ContentPlaceHolderID="Title" runat="server">
RPL - Completion
</asp:Content> 

<asp:Content ID="PageContent" ContentPlaceHolderID="Content" runat="server">
<div class="heading">
			<h1>Application Complete</h1>
		</div>
		
	<div class="submitSection centerDivTable" style="width: 670px;">
            <div class="sectionBody">
                <table cellpadding="0" cellspacing="0">
                    <tbody>
                    	<tr>
                            <td>
                            	It will take approximately 6 days to verify your information. We will get back to you by email.
                            </td>
                        </tr>
                     </tbody>
                </table>
            </div>
        </div>
</asp:Content>                                                         