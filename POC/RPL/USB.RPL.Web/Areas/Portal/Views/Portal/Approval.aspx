﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RPL.Master" Inherits="System.Web.Mvc.ViewPage<USB.RPL.Web.Areas.Portal.Models.PortalModel>" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="Header" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {

            $(".stepLink").attr('disabled', 'disabled');
            $(".step").attr('class', 'stepD');
            $("#welcomeBreadCrumb").removeAttr('disabled').parent().attr('class', 'step');
            $("#chooseLearningBreadCrumb").removeAttr('disabled').parent().attr('class', 'step');
            $("#approvalBreadCrumb").removeAttr('disabled').parent().attr('class', 'step');
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#accordion").accordion();
        });
    </script>

</asp:Content>

<asp:Content ID="MessagesContent" ContentPlaceHolderID="Messages" runat="server">
    <!--Start Error Message-->
    <% var displayErrorStyle = Model.Error.IsNotNullOrEmpty() ? "style=\"display:block;margin-left:10px; margin-right:10px\"" : "style=\"display:none;margin-left:10px; margin-right:10px\"";  %>
    <div id="errorLabel"  class="errorBox" <%= displayErrorStyle%>>
        <%=Model.Error %>
    </div>
    <!--End Error Message-->
   
</asp:Content>

<asp:Content ID="TitleContent" ContentPlaceHolderID="Title" runat="server">
RPL - Approve Basket
</asp:Content> 

<asp:Content ID="PageContent" ContentPlaceHolderID="Content" runat="server">
<%
    var pathToApprovalComplete = Url.Action("ApprovalComplete", new { controller = "Portal", area = "Portal" });
    %>

<div class="heading">
			<h1><%= Model.ReadOnlyBasket ? "View Basket" : "Approve Basket" %>:</h1>
		</div>
 <% using (Html.BeginForm("SumitForApproval", "Portal", FormMethod.Post, new { id = "approvalForm", enctype="multipart/form-data" }))
            { %>
		
	    <div class="button butApproval">
            <%if (Model.Profile.Basket.Count() != 0 && !Model.ReadOnlyBasket){%>
		    <input name="Submit" type="submit" value="Submit for Approval"   />
            <%} %>
	    </div>   
        
        <div class="submitSection centerDivTable" style="width: 820px;">
            <div class="sectionHeader"><%= Model.ReadOnlyBasket ? "Basket Information" : "Personally verify basket"%></div>
            <div class="sectionBody">
                <div id="accordion">

                    <%
                        var schoolingInformation = Model.Profile.Basket.Where(b => b.LearningType.Id == 1);
                        var generalEducation = Model.Profile.Basket.Where(b => b.LearningType.Id == 2);
                        var tertiaryEducation = Model.Profile.Basket.Where(b => b.LearningType.Id == 3);
                        var sectorSpecificEducation = Model.Profile.Basket.Where(b => b.LearningType.Id == 4);
                        var vocationalEducation = Model.Profile.Basket.Where(b => b.LearningType.Id == 5);
                        var employmentHistory = Model.Profile.Basket.Where(b => b.LearningType.Id == 6);
                        
                     %>
                     <%if (schoolingInformation.IsNotNull() && schoolingInformation.Count() > 0)
                       {%>
                       <h3><a href="#">Schooling Information</a></h3>
                       <div>
                           <% foreach (var basketItem in schoolingInformation)
                               { %>
                               <% Html.RenderPartial("SchoolingInformationReadOnly", basketItem); %>
                              <%  }%>
                              </div>
                           
                      <% } %>

                        <%if (generalEducation.IsNotNull() && generalEducation.Count() > 0)
                       {%>
                       <h3><a href="#">General Education</a></h3>
                       <div>
                       <% foreach (var basketItem in generalEducation)
                               { %>
                               <% Html.RenderPartial("GeneralEducationReadOnly", basketItem); %>
                              <%  }%>
                              </div>
                      <% } %>

                        <%if (tertiaryEducation.IsNotNull() && tertiaryEducation.Count() > 0)
                       {%>
                       <h3><a href="#">Tertiary Education</a></h3>
                       <div>
                       <% foreach (var basketItem in tertiaryEducation)
                               { %>
                               <% Html.RenderPartial("TertiaryEducationReadOnly", basketItem); %>
                              <%  }%>
                              </div>
                     <%  } %>

                       <%if (sectorSpecificEducation.IsNotNull() && sectorSpecificEducation.Count() > 0)
                       {%>
                       <h3><a href="#">Sector Specific Information</a></h3>
                       <div>
                       <% foreach (var basketItem in sectorSpecificEducation)
                               { %>
                               <% Html.RenderPartial("SectorSpecificEducationReadOnly", basketItem); %>
                              <%  }%>
                              </div>
                     <%  } %>

                        <%if (vocationalEducation.IsNotNull() && vocationalEducation.Count() > 0)
                       {%>
                       <h3><a href="#">Vocational Training</a></h3>
                       <div>
                       <% foreach (var basketItem in vocationalEducation)
                               { %>
                               <% Html.RenderPartial("VocationalEducationReadOnly", basketItem); %>
                              <%  }%>
                              </div>
                      <% } %>

                        <%if (employmentHistory.IsNotNull() && employmentHistory.Count() > 0)
                       {%>
                       <h3><a href="#">Employment History</a></h3>
                       <div>
                       <% foreach (var basketItem in employmentHistory)
                               { %>
                               <% Html.RenderPartial("EmploymentHistoryReadOnly", basketItem); %>
                              <%  }%>
                              </div>
                      <% } %>
                        
   	            </div>
            </div>
            <%if (!Model.ReadOnlyBasket)
            { %> 
                   
            <table cellpadding="0" cellspacing="0" width="100%">
                <tbody>                  	

						<tr>
                        <td style="vertical-align:top">
                            <strong>Additional Information:</strong>

                        </td>
                        <td class="learningData">
                            <%: Html.TextAreaFor(m => m.Comments, new { cols = "20", rows = "2" })%>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top">
                            <strong>Attachment:</strong>
                        </td>
                        <td>
                            <input name="file" id="file" type="file" />
                        </td>
                    </tr>
                    

                </tbody>
            </table>
            <%} %>
        </div>
        <%if (!Model.ReadOnlyBasket)
          { %>
        <div class="submitSection centerDivTable" style="width: 820px;">
            <div class="sectionHeader">Capture payment</div>
            <div class="sectionBody">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                    	<tr>
                            <td>Please make payment to USB-ED by “$PaymentMehtods" ... <br />
	                            <span>On payment, you will receive a reference number to be entered below.</span>
                            </td>
                    	</tr>
                    	<tr><td><%: Html.TextBoxFor(m => m.PaymentReference) %></td>
                    	</tr>
                    </tbody>
                </table>
             </div>
             </div>
		
	<br />
    <div class="c"></div>

     <%} %>
	<br />
		
	<div class="c"></div>
    <%} %>
</asp:Content>                                                         