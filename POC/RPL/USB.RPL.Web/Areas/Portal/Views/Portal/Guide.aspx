﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RPL.Master" Inherits="System.Web.Mvc.ViewPage<USB.RPL.Web.Areas.Portal.Models.PortalModel>" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="Header" runat="server">
<script type="text/javascript">
    $(document).ready(function () {

        $(".stepLink").attr('disabled', 'disabled');
        $(".step").attr('class', 'stepD');
        $("#welcomeBreadCrumb").removeAttr('disabled').parent().attr('class', 'step');
        $("#chooseLearningBreadCrumb").removeAttr('disabled').parent().attr('class', 'step');
    });
 </script>
</asp:Content>

<asp:Content ID="TitleContent" ContentPlaceHolderID="Title" runat="server">
RPL - Choose Learning
</asp:Content> 

<asp:Content ID="SubBreadCrumbsContent" ContentPlaceHolderID="SubBreadCrumbs" runat="server">
Wizard Guide
</asp:Content> 

<asp:Content ID="PageContent" ContentPlaceHolderID="Content" runat="server">
<%
     var pathToLearningType = Url.Action("LearningType", new { controller = "Portal", area = "Portal" });
    %>
    <div class="heading">
			<h1>Step By Step Guide:</h1>
		</div>

		<ul class="stepsGuide">
			<li>Please choose learning type</li>
			<li>Please select discipline from dropdown</li>
			<li>Please select course from dropdown</li>
			<li>Please capture information</li>
			<li>Save to basket</li>
			<li class="subItem">By the end of this process you would have built a basket of learning items to be formally verified. (Recognition of Prior Learning)</li>
		</ul>

    <div class="c"></div>
    <br />
	<div class="button fr">
		<a href="<%=pathToLearningType %>"><input name="Submit" type="submit" value="Continue" /></a>
	</div>

</asp:Content>                                                         