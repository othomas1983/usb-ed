﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RPL.Master" Inherits="System.Web.Mvc.ViewPage<USB.RPL.Web.Areas.Portal.Models.PortalModel>" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="Header" runat="server">
<script type="text/javascript">
    $(document).ready(function () {

        $(".stepLink").attr('disabled', 'disabled');
        $(".step").attr('class', 'stepD');
        $("#welcomeBreadCrumb").removeAttr('disabled').parent().attr('class', 'step');
        $("#chooseLearningBreadCrumb").removeAttr('disabled').parent().attr('class', 'step');

        $('#course').change(function () {
            if ($(this).val() == 0) {
                $("#newCourse").show();
            } else {
                $("#newCourse").hide();
            }
        });
        if ($('#course').val() == 0) {
            $("#newCourse").show();
        } else {
            $("#newCourse").hide();
        }
        $("#newCourse").mouseup(function (e) {
            e.preventDefault();
            $(this).select();
        });
    });
 </script>
</asp:Content>

<asp:Content ID="TitleContent" ContentPlaceHolderID="Title" runat="server">
RPL - Choose Learning
</asp:Content> 
<asp:Content ID="SubBreadCrumbsContent" ContentPlaceHolderID="SubBreadCrumbs" runat="server">
<%
     var pathToGuide = Url.Action("Index", new { controller = "Portal", area = "Portal" });
     var pathToLearningType = Url.Action("LearningType", new { controller = "Portal", area = "Portal" });
     var pathToDiscipline = Url.Action("SelectLearningType", new { controller = "Portal", area = "Portal" });
    %>
<a href="<%=pathToGuide %>" >Wizard Guide</a> > <a href="<%=pathToLearningType %>">Choose Learning Type</a> 

<%if (Model.BasketItem.LearningType.Id == 4 || Model.BasketItem.LearningType.Id == 5 || Model.BasketItem.LearningType.Id == 6)
  {%>
 > <a href="<%=pathToDiscipline %>?learningType=<%= Model.BasketItem.LearningType.Id %>">Select Discipline</a>
 <%} %>
  > Select Course 
</asp:Content>

<asp:Content ID="PageContent" ContentPlaceHolderID="Content" runat="server">
<%
    var pathToCatelogueInfo = Url.Action("CatalogueInformation", new { controller = "Portal", area = "Portal" });
    %>
<div class="heading">
			<h1>Select Course:</h1>
		</div>

        <% using (Html.BeginForm("SelectedCourse", "Portal", FormMethod.Post, new { id = "courseForm" }))
           { %>
		<div class="divisionSelect">
			<h1 class="divHeader">Please Select The Course:</h1>
			<span>
				<select name="course" id="course" class="selectDiv">
					<%
         
                        var vettedCourses = Model.Courses.Where(c => c.IsVetted);
                        var notVettedCourses = Model.Courses.Where(c => !c.IsVetted);
                     %>
                     <% if (vettedCourses.IsNotNull() && vettedCourses.Count() > 0)
                        { %>
                        <optgroup label="Vetted">
                            <%
                            foreach (var course in vettedCourses)
                            { %>
                                    <option value="<%=course.Id %>"><%= course.Name%></option>
                            <%} %>
                        </optgroup>
                    <%} %>
                     <% if (notVettedCourses.IsNotNull() && notVettedCourses.Count() > 0)
                        { %>
                        <optgroup label="Not Vetted">

                            <%
                            foreach (var course in notVettedCourses)
                            { %>
                                <option value="<%=course.Id %>"><%= course.Name%></option>
                            <%} %>
                        </optgroup>
                    <%} %>
				</select>
				
				<input name="newCourse" id="newCourse" type="text" value="Please enter course" />
				
			</span>
		</div>


		    <br />
		    <div class="c"></div>
			<div class="button fr"><input name="Submit" type="submit" value="Next" /></div>	
            <%} %>
</asp:Content>                                                         