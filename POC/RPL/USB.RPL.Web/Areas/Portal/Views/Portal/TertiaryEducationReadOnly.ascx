﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<USB.RPL.Domain.User.UserProfileBasket>" %>
<table>
<tr>
    <td>
        <strong>Selected Course:&nbsp;</strong>
    </td>
    <td><%= Model.Course.IsNotNull() ? Model.Course.Name : string.Empty%></td>
</tr>
<tr>
    <td>
         &nbsp;
    </td>
    <td>
        <table class="learningData">
            <tr>
                <td>
                    Course Status:
                </td>
                <td>
                    <%= Model.CourseStatus.Name%>
                </td>
            </tr>
            <tr>
                <td>
                    Date of Completion:
                </td>
                <td>
                   <%= Model.DateOfCompletion.IsNotNull() ? Model.DateOfCompletion.Value.ToString("dd/MM/yyyy") : "" %>
                </td>
            </tr>
            <tr>
                <td>
                    Institution:
                </td>
                <td>
                    <%= Model.Institution.IsNotNull() ? Model.Institution.Name : string.Empty %>
                </td>
            </tr>
            <tr>
                <td>
                    Address of institution:
                </td>
                <td>
                    <%= Model.InstitutionAddress %>
                </td>
            </tr>
            <tr>
                <td>
                    Degree Level:
                </td>
                <td>
                    <%= Model.DegreeLevel %>
                </td>
            </tr>
            <tr>
                <td>
                    Course Reference #:
                </td>
                <td>
                    <%= Model.CourseReference %>
                </td>
            </tr>
            <tr>
                <td>
                    Course Reference Person:
                </td>
                <td>
                    <%= Model.CourseReferencePerson %>
                </td>
            </tr>
        </table>
    </td>
</tr>
</table>
