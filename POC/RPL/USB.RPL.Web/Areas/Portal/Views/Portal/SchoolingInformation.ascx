﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<USB.RPL.Web.Areas.Portal.Models.PortalModel>" %>
<script type="text/javascript">

    $(document).ready(function () {


        $('#Institution').change(function () {
            if ($(this).val() == 0) {
                $("#NewInstitution").val("Please enter institution");
                $("#otherInstitutionRow").show();
            } else {
                $("#otherInstitutionRow").hide();
            }
        });
        if ($('#Institution').val() == 0) {
            $("#NewInstitution").val("Please enter institution");
            $("#otherInstitutionRow").show();
        } else {
            $("#otherInstitutionRow").hide();
        }
        $("#NewInstitution").mouseup(function (e) {
            e.preventDefault();
            $(this).select();
        });

    });        
</script>
<table class="levels" width="50%">
            <tr>
				<td>Institution:</td>
				<td>
                 <select name="Institution" id="Institution">
                    <%
                        var vettedInstitutions = Model.Institutions.Where(c => c.IsVetted);
                        var notVettedInstitutions = Model.Institutions.Where(c => !c.IsVetted);
                     %>
                      <% if (vettedInstitutions.IsNotNull() && vettedInstitutions.Count() > 0)
                         { %>
                          <optgroup label="Vetted">
					        <%foreach (var institution in vettedInstitutions)
                            { %>
                            <option value="<%=institution.Id %>"><%= institution.Name%></option>
                            <%} %>
                        </optgroup>
                        <%} %>

                        <% if (notVettedInstitutions.IsNotNull() && notVettedInstitutions.Count() > 0)
                         { %>
                         <optgroup label="Not Vetted">
					        <%foreach (var institution in notVettedInstitutions)
                            { %>
                            <option value="<%=institution.Id %>"><%= institution.Name%></option>
                            <%} %>
                        </optgroup>
                        <%} %>
				</select>
				</td>
			</tr>
            <tr id="otherInstitutionRow">
				<td>Other Institution:</td>
				<td><%: Html.TextBoxFor(m => m.NewInstitution)%></td>
			</tr>
			<tr>
				<td>Highest Grade:</td>
				<td><%: Html.TextBoxFor(m => m.BasketItem.HighestGrade)%></td>
			</tr>
			<tr>
				<td>School where highest grade was achieved:</td>
				<td><%: Html.TextBoxFor(m => m.BasketItem.HighestGradeSchool)%></td>
			</tr>
			<tr>
				<td>Address of school:</td>
				<td><%: Html.TextAreaFor(m => m.BasketItem.InstitutionAddress, new { cols="20", rows="2", @class="fixedText" })%></td>
			</tr>
            <tr>
				<td>Upload File:</td>
				<td><input name="file" id="file" type="file" /></td>
			</tr>

		</table>		