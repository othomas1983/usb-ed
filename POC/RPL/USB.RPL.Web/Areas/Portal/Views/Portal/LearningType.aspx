﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RPL.Master" Inherits="System.Web.Mvc.ViewPage<USB.RPL.Web.Areas.Portal.Models.PortalModel>" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="Header" runat="server">
<script type="text/javascript">
    $(document).ready(function () {

        $(".stepLink").attr('disabled', 'disabled');
        $(".step").attr('class', 'stepD');
        $("#welcomeBreadCrumb").removeAttr('disabled').parent().attr('class', 'step');
        $("#chooseLearningBreadCrumb").removeAttr('disabled').parent().attr('class', 'step');
    });
 </script>
</asp:Content>

<asp:Content ID="TitleContent" ContentPlaceHolderID="Title" runat="server">
RPL - Choose Learning
</asp:Content> 
<asp:Content ID="SubBreadCrumbsContent" ContentPlaceHolderID="SubBreadCrumbs" runat="server">
 <%
     var pathToGuide = Url.Action("Index", new { controller = "Portal", area = "Portal" });
    %>
<a href="<%= pathToGuide%>">Wizard Guide</a> > Choose Learning Type
</asp:Content> 
<asp:Content ID="PageContent" ContentPlaceHolderID="Content" runat="server">
 <%
     var pathToApproval = Url.Action("ApproveBasket", new { controller = "Portal", area = "Portal" });
     var selectedLearningTypeURL = Url.Action("SelectLearningType", new { controller = "Portal", area = "Portal" });
    %>

    		<div class="heading">
			<h1>Choose Learning Type:</h1>
		</div>
	
		<div class="typeWrapper schooling">
				<p>Please provide us with some background regarding the high school you attended, and the results you attained.</p>
			<a href="<%= selectedLearningTypeURL %>?learningType=1">
				<span class="wrapperHeading">
					Schooling Information
				</span>
			</a>	
		</div>
		
		<div class="typeWrapper general">
				<p>Please list any short courses or diplomas you have completed.</p>
			<a href="<%= selectedLearningTypeURL %>?learningType=2">
				<span class="wrapperHeading">
					General Education
				</span>
			</a>	
		</div>
				
		<div class="typeWrapper tertiary">
				<p>Here we require information about the tertiary institution you attended, and your highest qualification (eg BA Deg at Stellenbsoch University).</p>
			<a href="<%= selectedLearningTypeURL %>?learningType=3">
				<span class="wrapperHeading">
					Tertiary Education
				</span>
			</a>	
		</div>

		<div class="typeWrapper sector">
				<p>Have you completed any courses within your industry? (eg Prince2 Project Management Certification, MCSE, etc). </p>
			<a href="<%= selectedLearningTypeURL %>?learningType=4">
				<span class="wrapperHeading">
					Sector Specific Information
				</span>
			</a>	
		</div>

		<div class="typeWrapper vocational">
				<p>Have you done any studying in any of the vocational fields like Nursing, Education, Law, etc. </p>
			<a href="<%= selectedLearningTypeURL %>?learningType=5">
				<span class="wrapperHeading">
					Vocational Training
				</span>
			</a>	
		</div>

		<div class="typeWrapper employment">
				<p>Give usa as much background information as possible. A list of jobs you have had in the past is required here.</p>
			<a href="<%= selectedLearningTypeURL %>?learningType=6">
				<span class="wrapperHeading">
					Employment History
				</span>
			</a>	
		</div>
		
    <br />
    <div class="c"></div>
    <br />
	<div class="button fr">
     <%if (Model.Profile.Basket.Count() != 0)
       {%>
		<a href="<%=pathToApproval %>"><input name="Submit" type="submit" value="Approve Basket" /></a>
        <%} %>
	</div>
</asp:Content>                                                         