﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<USB.RPL.Web.Areas.Portal.Models.PortalModel>" %>
<script type="text/javascript">

    $(document).ready(function () {

        $(function () {
           
            var dates = $('#DateOfCompletion').datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                showOn: 'button',
                buttonImage: '../Content/gfx/calendar.gif',
                buttonImageOnly: true,
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                dateFormat: 'dd/mm/yy',
                minDate: Infinity,
                onSelect: function (selectedDate) {
                    var option = this.id == "dateFrom" ? "minDate" : "maxDate";
                    var instance = $(this).data("datepicker");
                    var date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
                    dates.not(this).datepicker("option", option, date);
                }
            });
        });

        $('#Institution').change(function () {
            if ($(this).val() == 0) {
                $("#NewInstitution").val("Please enter institution");
                $("#otherInstitutionRow").show();
            } else {
                $("#otherInstitutionRow").hide();
            }
        });
        if ($('#Institution').val() == 0) {
            $("#NewInstitution").val("Please enter institution");
            $("#otherInstitutionRow").show();
        } else {
            $("#otherInstitutionRow").hide();
        }
        $("#NewInstitution").mouseup(function (e) {
            e.preventDefault();
            $(this).select();
        });

    });        
</script>
<table class="levels" width="50%">
            <tr>
				<td>Course Status:</td>
				<td>
                    <select name="CourseStatus" id="CourseStatus">
					<%foreach (var status in Model.CourseStatuses)
                    { %>
                    <option value="<%=status.Id %>"><%= status.Name%></option>
                    <%} %>
				</select>
				</td>
			</tr>
			<tr>
				<td>Date of Completion:</td>
				<td><div style="width: 250px;display: block;height: 35px;"><%: Html.TextBoxFor(m => m.BasketItem.DateOfCompletion, new { id = "DateOfCompletion" })%></div></td>
			</tr>

            <tr>
				<td>Institution:</td>
				<td>
                 <select name="Institution" id="Institution">
                    <%
                        var vettedInstitutions = Model.Institutions.Where(c => c.IsVetted);
                        var notVettedInstitutions = Model.Institutions.Where(c => !c.IsVetted);
                     %>
                      <% if (vettedInstitutions.IsNotNull() && vettedInstitutions.Count() > 0)
                         { %>
                          <optgroup label="Vetted">
					        <%foreach (var institution in vettedInstitutions)
                            { %>
                            <option value="<%=institution.Id %>"><%= institution.Name%></option>
                            <%} %>
                        </optgroup>
                        <%} %>

                        <% if (notVettedInstitutions.IsNotNull() && notVettedInstitutions.Count() > 0)
                         { %>
                         <optgroup label="Not Vetted">
					        <%foreach (var institution in notVettedInstitutions)
                            { %>
                            <option value="<%=institution.Id %>"><%= institution.Name%></option>
                            <%} %>
                        </optgroup>
                        <%} %>
				</select>
				</td>
			</tr>
            <tr id="otherInstitutionRow">
				<td>Other Institution:</td>
				<td><%: Html.TextBoxFor(m => m.NewInstitution)%></td>
			</tr>
            <tr>
				<td>Address of institution:</td>
				<td><%: Html.TextAreaFor(m => m.BasketItem.InstitutionAddress, new { cols="20", rows="2", @class="fixedText" })%></td>
			</tr>
			<tr>
				<td>Degree Level:</td>
				<td><%: Html.TextBoxFor(m => m.BasketItem.DegreeLevel)%></td>
			</tr>
            <tr>
				<td>Course Reference #:</td>
				<td><%: Html.TextBoxFor(m => m.BasketItem.CourseReference)%></td>
			</tr>
			<tr>
				<td>Course Reference Person:</td>
				<td><%: Html.TextBoxFor(m => m.BasketItem.CourseReferencePerson)%></td>
			</tr>
            <tr>
				<td>Upload File:</td>
				<td><input name="file" id="file" type="file" /></td>
			</tr>

		</table>	