﻿using System.Web.Mvc;

namespace USB.RPL.Web.Areas.Portal
{
    public class PortalAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Portal";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Portal_default",
                "Portal/{action}/{id}",
                new { controller = "Portal", action = "Index", id = UrlParameter.Optional }
            );
        }

    }
}
