﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.SessionState;
using USB.RPL.Web.Contexts;
using USB.RPL.Web.Services;

namespace USB.RPL.Web.Controllers
{
    public class ControllerFactory : IControllerFactory
    {
        #region Locals

        DefaultControllerFactory controllerFactory;

        #endregion

        #region Constructors

        public ControllerFactory()
        {
            controllerFactory = new DefaultControllerFactory();
        }

        #endregion

        #region IControllerFactory Members

        public IController CreateController(System.Web.Routing.RequestContext requestContext, string controllerName)
        {
            var controller = controllerFactory.CreateController(requestContext, controllerName);

            InitController(controller as RPLController);

            EnforceRequestValidation(controller);

            return controller;
        }

        public void ReleaseController(IController controller)
        {
            var disposable = controller as IDisposable;
            if (disposable != null)
            {
                disposable.Dispose();
            }
        }

        private static void EnforceRequestValidation(IController controller) 
        {
            var secure = controller as Controller;

            if(secure.IsNotNull()) 
            {
                secure.ValidateRequest = true;
            }
        }

        public SessionStateBehavior GetControllerSessionBehavior(RequestContext requestContext, string controllerName)
        {
            return SessionStateBehavior.Default;
        }

        #endregion

        #region Methods

        private void InitController(RPLController controller)
        {
            if (controller.IsNotNull())
            {
                controller.RPLClient = new RPLClient();
                controller.ApplicationContext = HttpContext.Current.Session[SessionKeys.ApplicationContext] as ApplicationContext;

            }
        }

        #endregion


        
    }
}