﻿using System;
using System.Web.Mvc;
using USB.RPL.Domain;
using USB.RPL.Web.Contexts;
using USB.RPL.Web.Services;

namespace USB.RPL.Web.Controllers
{
    public abstract class RPLController : Controller, IDisposable
    {
        public ApplicationContext ApplicationContext { get; set; }

        public string HttpMethod
        {
            get
            {
                return this.HttpContext.Request.RequestType;
            }
        }

        public IRPLClientServicesProvider RPLClient { get; set; }

        public bool UserIsLoggedOn()
        {
            return ResolveUserId().IsNotNull() && this.ApplicationContext.CurrentUser.IsNotNull();
        }

        public long? ResolveUserId()
        {
            if (ApplicationContext.IsNotNull())
            {
                return ApplicationContext.CurrentUser.IsNotNull() ? ApplicationContext.CurrentUser.UserId : null;
            }

            return null;
        }

        public void SetCache(IEntityCache cache)
        {
            if (cache is SessionCache)
            {
                this.RPLClient.Cache = cache;
            }

            if (cache is GlobalCache)
            {
                this.RPLClient.GlobalCache = cache;
            }
        }

        public abstract string DisplayName { get; }
        
        #region IDisposable Members

        void IDisposable.Dispose()
        {
            RPLClient.Dispose();
        }

        #endregion
    }
}
