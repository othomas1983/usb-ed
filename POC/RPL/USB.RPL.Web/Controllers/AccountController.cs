﻿using System.Web.Mvc;
using USB.RPL.Web.Actions;
using USB.RPL.Web.Contexts;
using USB.RPL.Web.Extensions;
using USB.RPL.Web.Models;

namespace USB.RPL.Web.Controllers
{
    /// <summary>
    /// Controller to handle all account and authorization requests.
    /// </summary>
    [HandleError(View="InternalError")]
    [CheckRequiredBrowser]
    public partial class AccountController : RPLController
    {
        #region Properties

        public override string DisplayName
        {
            get { return "Account"; }
        }

        #endregion

        #region Actions


        /// <summary>
        /// Handles a HTTP GET request for displaying a welcome on page.
        /// </summary>
        /// <returns>An appropriate ViewResult</returns>
        [HttpGet]
        public virtual ActionResult Welcome()
        {
            return View("Welcome");
        }

        /// <summary>
        /// Handles a HTTP GET request for displaying a welcome on page.
        /// </summary>
        /// <returns>An appropriate ViewResult</returns>
        [HttpGet]
        public virtual ActionResult Register()
        {
            return View("Register", new RegisterUserViewModel());
        }

        /// <summary>
        /// Handles a HTTP GET request for displaying a welcome on page.
        /// </summary>
        /// <returns>An appropriate ViewResult</returns>
        [HttpPost]
        public virtual ActionResult RegisterUser(RegisterUserViewModel model)
        {
            return new RegisterUserAction(RPLClient)
            {
                ShowView = (viewModel) =>
                {
                    return View("Register", viewModel);
                }
            }.Execute(this, model);
        }

        /// <summary>
        /// Handles a HTTP GET request for displaying a log on page.
        /// </summary>
        /// <returns>An appropriate ViewResult</returns>
        [HttpGet]
        public virtual ActionResult LogOn()
        {
            if (UserIsLoggedOn())
            {
                return Redirects.ToPortalPage();
            }
            else
            {
                LogOff();
                return View();
            }
        }

        /// <summary>
        /// Handles a HTTP POST request for submitting log on information for a user.
        /// Validates information and logs user in.
        /// </summary>
        /// <param name="model">Instance of LogOnViewModel containing submitted log on information.</param>
        /// <param name="returnUrl">A URL to redirect to on succesful login</param>
        /// <returns>An appropriate ViewResult</returns>
        [HttpPost]
        public virtual ActionResult LogOn(LogOnViewModel model, string returnUrl)
        {
                return new LogOnUserAction(RPLClient)
                {
                    ViewResult = () => View(model),
                    ReturnUrl = returnUrl,
                    Context = new ApplicationContext()
                }.Execute(this, model);
        }
 
        /// <summary>
        /// Handles a HTTP GET request to log of the currently logged in user.
        /// </summary>
        /// <returns>An approtiate ViewResult</returns>
        [HttpGet]
        public virtual ActionResult LogOff()
        {
            return new LogOffAction(RPLClient).Invoke(this);
        }

        #endregion
    }
}