﻿using System.Web.Mvc;
using USB.RPL.Web.Extensions;

namespace USB.RPL.Web.Controllers
{
    [HandleError]
    public partial class HomeController : RPLController
    {
        public virtual ActionResult Index()
        {
            return Redirects.ToPortalPage();
        }

        public ActionResult BrowserNotSupported()
        {
            return View();
        }

        [HttpGet]
        public ActionResult FileNotFound(string filename)
        {
            var key = "Filename";

            if (TempData.ContainsKey(key))
            {
                TempData.Remove(key);
            }

            TempData.Add(key, filename);
            return View();
        }

        [HttpGet]
        public ActionResult InternalError()
        {
            return View();
        }

        public ActionResult Error()
        {
            if (this.Session.UserIsLoggedOn())
            {
                return View("InternalError");
            }
    
            return View();
        }

        [HttpGet]
        [Authentication]
        public ActionResult ShowCombres()
        {

#if !DEBUG

           throw new InvalidOperationException();
#endif


            return View();
        }


        [HttpGet]
        public ActionResult PageNotFound()
        {
            return View();
        }

        public override string DisplayName
        {
            get { return "Home"; }
        }
    }
}
