﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace USB.RPL.Web.Extensions
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class CheckRequiredBrowserAttribute : ActionFilterAttribute
    {
        #region Overrides

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var controller = filterContext.Controller;

            if (!BrowserIsCompatible(controller.ControllerContext.RequestContext))
            {
                filterContext.Result = Redirects.ToBrowserNotSupported();
            }

            base.OnActionExecuting(filterContext);
        }

        #endregion

        #region  Methods

        //Checks if browser is IE7 or later
        private bool BrowserIsCompatible(RequestContext request)
        {
            var browser = request.HttpContext.Request.Browser;
            var name = browser.Browser;
            var version = (float)(browser.MajorVersion + browser.MinorVersion);

            return IsCompatibleIE(name, version);
        }

        public bool IsCompatibleIE(string name, float version)
        {
            if (!(name == "IE" && version < 7))
            {
                return true;
            }
            return false;
        }
        #endregion
    }
}