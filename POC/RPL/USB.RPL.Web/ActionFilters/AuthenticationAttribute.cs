﻿using System;
using System.Linq;
using System.Web.Mvc;
using Airborne.Utilities;
using USB.RPL.Domain.User;
using USB.RPL.Web.Contexts;
using USB.RPL.Web.Controllers;

namespace USB.RPL.Web.Extensions
{
    public class AuthenticationAttribute : AuthorizeAttribute
    {
        #region Constructors

        public AuthenticationAttribute()
        {
        }

        #endregion

        #region Methods

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);
            var controller = filterContext.Controller as RPLController;
            var applicationContext = controller.ApplicationContext;

            if (applicationContext.IsNull() 
                || applicationContext.CurrentUser.IsNull()
                || !filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                controller.RPLClient.ClientAuthentication.SignOut();
                HandleUnauthorizedRequest(filterContext);
            }
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.Request.Headers["X-Requested-With"] != null
               && filterContext.HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
            {
                filterContext.Result = new JsonResult() { Data = new { expired = true }, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
            else
            {
                filterContext.Result = Redirects.ToWelcome();
            }

        }

        #endregion


    }
}