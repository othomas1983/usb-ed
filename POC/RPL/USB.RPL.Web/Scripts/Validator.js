﻿/*

    Used for validation of elements. 
    Has a dependancy on jQuery

*/

function Validator() {
}

// Returns true if ALL elements are empty
Validator.prototype.AreAllValuesEmpty = function (selector) {

    var fieldsAreEmpty = false;
    $(selector).each(
        function () {
            if ($(this).val().length == 0) {
                fieldsAreEmpty = true;
            }
            else {
                fieldsAreEmpty = false;
            }
            return fieldsAreEmpty;
        });

    return fieldsAreEmpty;

}


// Returns true if ANY elements are empty
Validator.prototype.AreAnyValuesEmpty = function (selector) {

    var fieldsAreEmpty = false;
    $(selector).each(
        function () {
            if ($(this).val().length == 0) {
                fieldsAreEmpty = true;                
            }
        });

    return fieldsAreEmpty;

}

// Returns false if ANY elements are false
Validator.prototype.AreAnyValuesFalse = function (selector) {

    $(selector).each(
        function () {
            if ($(this).val() == false) {
                return false;
            }
        });

    return true;

}