﻿function SessionManager() {

    //#region Properties
    var instance;

    this.SessionTimeoutSeconds;
    this.CountDownSeconds;
    this.SecondsBeforePrompt;
    this.NotificationDialog;
    this.DisplayCountDownIntervalId;
    this.PromptToExtendSessionTimeoutId;
    this.OriginalTitle;
    this.Count;
    this.PathToLogOff;
    this.PathToExtendSession;
    //#endregion

    //#region Methods

    this.Load = function () {

        instance = this;

        //Minus 5 to cater for the delay
        this.SecondsBeforePrompt = this.SessionTimeoutSeconds - this.CountDownSeconds - 5;
        this.NotificationDialog = $('#sm-countdown-dialog');
        this.OriginalTitle = document.title;
        this.Count = this.CountDownSeconds;

        this.StartSessionManager(instance);
        $(':input').change(function () {
            instance.CloseSessionDialog();
        });
        $(window).bind("scroll", this.CloseSessionDialog);
        $(document).bind("click", this.CloseSessionDialog);

    };

    this.StartSessionManager = function (instance) {
        instance.PromptToExtendSessionTimeoutId = window.setTimeout(instance.PromptToExtendSession, (instance.SecondsBeforePrompt * 1000));

    };

    this.CloseSessionDialog = function () {
        instance.NotificationDialog.dialog('close');
        document.title = instance.OriginalTitle;
        instance.RefreshSession();
    };

    this.PromptToExtendSession = function () {
        instance.NotificationDialog.dialog({
            title: 'Session Timeout Warning',
            height: 150,
            width: 350,
            bgiframe: true,
            modal: true,
            buttons: {
                'Continue': function () {
                    $(this).dialog('close');
                    instance.RefreshSession();
                    document.title = instance.OriginalTitle;
                },
                'Log Out': function () {
                    instance.EndSession(instance);
                }
            }
        });
        instance.Count = instance.CountDownSeconds;
        instance.DisplayCountDown(instance);
    };

    this.PadLeft = function (string, padMask) {
        string = '' + string;
        return (padMask.substr(0, (padMask.length - string.length)) + string);
    };

    this.DisplayCountDown = function (instance) {
        var CountDown = function () {
            var cd = new Date(instance.Count * 1000),
                    minutes = cd.getUTCMinutes(),
                    seconds = cd.getUTCSeconds(),
                    minutesDisplay = minutes === 1 ? '1 minute ' : minutes === 0 ? '' : minutes + ' minutes ',
                    secondsDisplay = seconds === 1 ? '1 second' : seconds + ' seconds',
                    cdDisplay = minutesDisplay + secondsDisplay;

            document.title = 'Expire in ' + instance.PadLeft(minutes, '00') + ':' + instance.PadLeft(seconds, '00');
            $('#sm-countdown').html(cdDisplay);
            if (instance.Count === 0) {
                document.title = 'Session Expired';
                instance.EndSession(instance);
            }
            instance.Count--;
        };
        CountDown();
        instance.DisplayCountDownIntervalId = window.setInterval(CountDown, 1000);
    };

    this.RefreshSession = function () {

        window.clearInterval(instance.DisplayCountDownIntervalId);
        window.clearTimeout(instance.PromptToExtendSessionTimeoutId);

        $.post(instance.PathToExtendSession, null, function () { });

        instance.StartSessionManager(instance);
    };

    this.EndSession = function (instance) {
        instance.NotificationDialog.dialog('close');
        location.href = instance.PathToLogOff;
    };

   
    //#endregion

}
