/*
 * jQuery Reveal Plugin 1.0
 * www.ZURB.com
 * Copyright 2010, ZURB
 * Free to use under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
*/


(function ($) {



    /*---------------------------
    Defaults for Reveal
    ----------------------------*/

    /*---------------------------
    Listener for data-reveal-id attributes
    ----------------------------*/

    $('a[data-reveal-id]').live('click', function (e) {
        e.preventDefault();
        var modalLocation = $(this).attr('data-reveal-id');
        $('#' + modalLocation).reveal($(this).data(), this);
    });

    /*---------------------------
    Extend and Execute
    ----------------------------*/

    $.fn.reveal = function (options, sender) {


        //TODO: ensure that no modal background is open ...

        var modelLeftPos;
        var modelTopPos;

        if (sender !== null) {
            var top = parseInt($(sender).position().top);
            var left = parseInt($(sender).position().left);

            var senderX = left + (parseInt($(sender).css('width')) / 2);
            modelLeftPos = (senderX - (parseInt($(this).css('width')) / 2) + 35) + 'px';

            var modelHeight = parseInt($(this).css('height'));

            if (isNaN(modelHeight)) {
                modelHeight = 300;
            }

            var senderY = top + (parseInt($(sender).css('height')) / 2);
            var modelTopPos = (senderY - (modelHeight / 2)) + 'px';
        }

        var defaults = {
            animation: 'fadeAndPop', //fade, fadeAndPop, none
            animationspeed: 300, //how fast animtions are
            closeonbackgroundclick: true, //if you click background will modal close?
            dismissmodalclass: 'close-reveal-modal' //the class of a button or element that will close an open modal
        };

        //Extend dem' options
        var options = $.extend({}, defaults, options);

        return this.each(function () {

            /*---------------------------
            Global Variables
            ----------------------------*/

            var modal = $(this),
        		topMeasure = modelTopPos != null ? modelTopPos : parseInt(modal.css('top')),
				topOffset = modal.height() + topMeasure,
          		locked = false,
				modalBG = $('.reveal-modal-bg');


            /*---------------------------
            Create Modal BG
            ----------------------------*/
            if (modalBG.length == 0) {

                var dashboard = $('#MyDashBoard');
                modalBG = $('<div class="reveal-modal-bg" />').insertAfter(dashboard);
            }

            /*---------------------------
            Open & Close Animations
            ----------------------------*/
            //Entrance Animations
            modal.bind('reveal:open', function () {

                modalBG.unbind('click.modalEvent');
                $('.' + options.dismissmodalclass).unbind('click.modalEvent');

                if (!locked) {
                    lockModal();
                    //GuyC: Fix to "deShmick" the reveal if browser is IE 8 or less
                    if ($.browser.msie && parseFloat($.browser.version) < 9.0) {
                        modal.css({ 'visibility': 'visible', 'left': modelLeftPos, 'top': modelTopPos });
                        modalBG.show();
                        modal.show();
                        unlockModal();
                    }
                    else {
                        modal.css({ 'opacity': 0, 'visibility': 'visible', 'left': modelLeftPos, 'top': modelTopPos });
                        modalBG.fadeIn(options.animationspeed / 2);
                        modal.delay(options.animationspeed / 2).animate({
                            "opacity": 1
                        }, options.animationspeed, unlockModal());
                    }
                    //end fix
                }
                modal.unbind('reveal:open');
            });

            //Closing Animation
            modal.bind('reveal:close', function () {

                if (!locked) {

                    lockModal();

                    modalBG.css({ 'display': 'none' });
                    //GuyC: Fix to "deShmick" the reveal if browser is IE 8 or less
                    if ($.browser.msie && parseFloat($.browser.version) < 9.0) {
                        modal.hide();
                        unlockModal();
                    } else {
                        modal.animate({
                            "opacity": 0
                        }, options.animationspeed, function () {
                            modal.css({ 'opacity': 1, 'visibility': 'hidden', 'top': topMeasure });
                            unlockModal();
                        });
                    }
                    //end fix
                }

                modal.unbind('reveal:close');
            });

            /*---------------------------
            Open and add Closing Listeners
            ----------------------------*/
            //Open Modal Immediately
            modal.trigger('reveal:open')

            //Close Modal Listeners
            var closeButton = $('.' + options.dismissmodalclass).bind('click.modalEvent', function () {
                modal.trigger('reveal:close')
            });

            if (options.closeonbackgroundclick) {

                modalBG.bind('click.modalEvent', function () {

                    modal.trigger('reveal:close')
                });
            }
            $('body').keyup(function (e) {
                if (e.which === 27) { modal.trigger('reveal:close'); } // 27 is the keycode for the Escape key
            });


            /*---------------------------
            Animations Locks
            ----------------------------*/
            function unlockModal() {
                locked = false;
            }
            function lockModal() {
                locked = true;
            }

        }); //each call
    } //orbit plugin call
})(jQuery);
        
