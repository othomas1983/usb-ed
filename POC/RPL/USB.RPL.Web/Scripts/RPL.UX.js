﻿
//Helper class for UX
function RPLUX() {

    this.validationErrorClassName = "ValidationError";
    this.errorGroupBoxClassName = "errorGroupBox";
    this.screenIsBusy = false; // tracks the current busy state for the screen.

    //performs required field UI validation
    this.ValidateRequiredFields = function (objects) {

        this.ClearValidationIndicators();
        var isValid = true;

        for (var i = 0; i < objects.length; i++) {

            if (objects[i]) {
                isValid = isValid & objects[i].Validate();
            }
        }

        return isValid;
    }

    //performs required field UI validation
    this.ValidateRequiredFields2 = function (objects, parentArea) {

        var isValid = this.ValidateRequiredFields(objects);

        if (!isValid) {
            this.DisplayValidationGroups(parentArea);
        }

        return isValid;
    }


    //clears validation indicators
    this.ClearValidationIndicators = function () {
        $("." + this.validationErrorClassName).removeClass(this.validationErrorClassName);
    }


    //displays and scrolls to the group error box (if any) for an element that has been marked with .ValidationError class
    this.DisplayValidationGroups = function (parentArea) {

        $("." + this.errorGroupBoxClassName).hide();
        $("." + this.validationErrorClassName).closest("." + parentArea).find("." + this.errorGroupBoxClassName).show();

        var errorBox = $("." + this.errorGroupBoxClassName + ":visible").first();
        if (errorBox) {
            $('html,body').animate({ scrollTop: errorBox.offset().top - 20 }, 'slow');
        }
    }

    //displays the group error box (if any) for an element that has been marked with .ValidationError class
    this.DisplayValidationGroupsWithoutScroll = function (parentArea, message) {

        $("." + this.errorGroupBoxClassName).hide();
        $("." + this.validationErrorClassName).closest("." + parentArea).find("." + this.errorGroupBoxClassName).show();
        $("." + this.validationErrorClassName).closest("." + parentArea).find("." + this.errorGroupBoxClassName).html(message);
        var errorBox = $("." + this.errorGroupBoxClassName + ":visible").first();

    }

    //tags all elements
    this.DisplayValidationIndicators = function (selectorPattern, validationErrors, parentArea) {

        var errorTagger = new ControlErrorTagger(selectorPattern);
        errorTagger.Tag(validationErrors);

        if (parentArea) {
            this.DisplayValidationGroups(parentArea);
        }
    }



    //toggles the state of the screen busy indicator

    this.ToggleBusyState = function (message) {
        this.SetBusyState(!this.screenIsBusy, message);
    }

    //sets the busy state of the screen
    this.SetBusyState = function (isBusy, message) {
        if (isBusy) {
            var busyMessage = message != null ? message : "Just a sec! We\'re processing your information.";
            $.blockUI({
                css: {border: 'none', backgroundColor: 'transparent'},
                message: '<div class="busyprocess"><p>' + busyMessage + '</p></div>'
            });
        } else {
            $.unblockUI();
        }

        this.screenIsBusy = isBusy;
    }


    /*Fancy scroll to a target element*/
    this.ScrollToPanel = function (element) {
        $('html,body').animate({ scrollTop: element.offset().top - 20 }, 'slow');
    }

    //Fancy scroll to top
    this.ScrollToTop = function () {
        this.ScrollToPanel($('html,body'));
    }

    //Toggle the dropdown visibility
    this.ToggleVisibility = function (element) {
        $(".OptionsPanel").hide();
        $(".OptionsPanel").each(
            function () {
                if ($(this)[0] != $(element)[0]) {
                    $(this).hide();
                }
            }
        );

        $(element).slideToggle("fast");

        if ($(element).is(":visible")) {
            $(element).hide();
        } else {
            $(element).show();
        }
    };

    //Hide error Labels
    this.HideErrorMessage = function (label) {
        label.hide();
    };
                    
    //show error labels
    this.ShowErrorMessage = function (label, message, style) {

        this.SetStatusMessage(label, message);

        if (style) {
            label.removeClass("success-box");
            label.addClass("errorBox");
        }
    };

    //Show Success Message
    this.ShowSuccessMessage = function (label, message, style) {

        this.SetStatusMessage(label, message);

        if (style) {
            label.removeClass("errorBox");
            label.addClass("success-box");
        }
    };

    //Sets the message
    this.SetStatusMessage = function (label, message) {
        label.html(message);
        label.show();
    };


    this.ToggleSpinner = function (instance) {
        if (instance.Spinner.is(":hidden")) {
            instance.Spinner.show();
        } else {
            instance.Spinner.hide();
        }
    };

    this.AutoScrollToDropDown = function (element) {
        if (element.is(":visible")) {
            $('html,body').animate({ scrollTop: element.offset().top - 200 }, 'slow');
        }
    };
}
