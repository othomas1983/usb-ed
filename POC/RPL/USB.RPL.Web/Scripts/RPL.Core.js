﻿/// <reference path="jquery/jquery-1.4.2.js" />

$(document).ready(function () {
    $('.bubbleInfo').each(function () {
        initialiseBubbleInfos(this);
    });

    $('.hoverContainer').each(function () {

        var trigger = $('.hoverTrigger', this);
        var popup = $('.hoverContent', this).css({ position: 'absolute' }).hide();
        var triggerPosition = trigger.offset();
        trigger.hover(function (e) {
            popup.css({ top: triggerPosition.bottom }).show();
        },
        function () {
            popup.hide();
        });

    });

    $(".editor-field-h").each(function () {
        var container = $(this);
        $(".input-validation-error", container).keydown(function () {
            $(".field-validation-error", container).remove();
            $(this).removeClass("input-validation-error");
        }).change(function () {
            $(".field-validation-error", container).remove();
            $(this).removeClass("input-validation-error");
        });
    });

    $(".focusOnLoad").focus();


});



function initialiseBubbleInfos(bubble) {
    //alert("Init");
    var trigger = $('.trigger', bubble);
    var popup = $('.popup', bubble);

    // options
    var time = 80;
    var hideDelayTimer = null;

    // tracker
    var beingShown = false;
    var shown = false;

    // set the mouseover and mouseout on both element
    $(".trigger , .popup", bubble).focusin(function () {
        if (!shown) {
            showPopup(trigger, popup, time, hideDelayTimer);
            shown = true;
        }
    });
    $(".trigger , .popup", bubble).focusout(function () {
        if (shown) {
            hideDelayTimer = hidePopup(trigger, popup, time, hideDelayTimer);
            shown = false;
        }
    });

}

function showPopup(trigger, popup, time, hideDelayTimer) {
    var beingShown = false;
    // stops the hide event if we move from the trigger to the popup element
    if (hideDelayTimer) clearTimeout(hideDelayTimer);

    // don't trigger the animation again if we're being shown, or already visible
    if (beingShown) {
        return;
    } else {
        beingShown = true;

        // reset position of popup box
        popup.css({
            display: 'block'
        })

        // (we're using chaining on the popup) now animate it's opacity and position
        .animate({ opacity: 1 }, time, 'swing', function () {
            // once the animation is complete, set the tracker variables
            beingShown = false;
        })
        .css('z-index', 20);
    } // endif
}

function hidePopup(trigger, popup, time, hideDelayTimer) {
    //alert("Hide!");
    var hideDelay = 100;
    // reset the timer if we get fired again - avoids double animations
    if (hideDelayTimer) clearTimeout(hideDelayTimer);

    // store the timer so that it can be cleared in the mouseover if required
    hideDelayTimer = setTimeout(function () {
        hideDelayTimer = null;
        popup.animate({ opacity: 0 }, time, 'swing', function () {
            // once the animate is complete, set the tracker variables
            //shown = false;
            // hide the popup entirely after the effect (opacity alone doesn't do the job)
            popup.css('display', 'none');
        });
    }, hideDelay);

    return hideDelayTimer;
}



