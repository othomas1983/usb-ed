﻿String.prototype.trim = function () {
    return this.replace(/(?:(?:^|\n)\s+|\s+(?:$|\n))/g, "");
};

String.prototype.isSpacesOrEmpty = function () {
    var test = this.trim();
    return (test.length == 0);
};

String.prototype.isValidEmailAddress = function () {
    var emailAddress = this.trim();
    if (emailAddress.length > 0) {
        var emailExpression = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
        return emailExpression.test(emailAddress);
    }
};

String.prototype.contains = function (substring) {

    return (this.indexOf(substring) >= 0);
};