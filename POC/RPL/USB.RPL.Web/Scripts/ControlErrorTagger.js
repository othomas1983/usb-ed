﻿/*
Description:
This class is used to tag controls with errors. When postbacks come back with validation errors
this class marks them with the error.

Dependancies:
    - latest jquery library
    - jquery.tooltip.min.js
    - StringExtensions.js
*/

function ControlErrorTagger(selectorPattern) {

    // i.e. ".Measure_" or ""#Measure_"
    this.SelectorPattern = selectorPattern;
    this.ErrorClass = "ValidationError";
    this.ValidationErrors = [];
    this.ControlsToTag = [];
    //#region Methods

    this.ShowErrors = function () {

        for (var a in this.ControlsToTag) {
            var control = $('#' + this.ControlsToTag[a]);

            errors = GetErrorsForControl(control, this);
            if(errors != ''){
                this.TagElement(control, errors);
            }
        }
    }

    function GetErrorsForControl(control, instance) {
        var message = '';
        var taggedCodes = control.attr("validationTag").split(',');

        for (var a in instance.ValidationErrors) {
            for (var b in taggedCodes) {
                if (taggedCodes[b] == instance.ValidationErrors[a].Code) {
                    message += '<span>'+instance.ValidationErrors[a].Error + '</span><br/>';
                    break;
                }
            }
        }
        return message;
    }

    this.Tag = function (validationErrors) {

        for (var i in validationErrors) {

            var error = validationErrors[i];

            var controlId = "";

            var element = $(this.SelectorPattern + controlId);
            this.TagElement(element, error.Error);
        }
    }

    this.TagEmptyFields = function (fieldsArray, msg) {
        var fieldsWereTagged = false;
        for (var i = 0; i < fieldsArray.length; i++) {
            if ( ($(fieldsArray[i].ElementToValidate).val() != undefined) && ( $(fieldsArray[i].ElementToValidate).val().isSpacesOrEmpty()) ) {
                fieldsWereTagged = true;
                var elementToTag = $(fieldsArray[i].ElementToTag).length > 0 ? $(fieldsArray[i].ElementToTag) : $(fieldsArray[i].ElementToValidate);
                this.TagElement(elementToTag, msg);
            }
        }
        return fieldsWereTagged;
    };

    this.TagElement = function (element, msg) {
        
        element.attr("title", msg);
        element.addClass(this.ErrorClass);
        element.click(function (e) {
            if($(element).is("input")){
                e.preventDefault();
            }
            var currentTarget = $("#" + e.currentTarget.id);
            currentTarget.removeClass((new ControlErrorTagger()).ErrorClass);
            currentTarget.removeAttr("title");
            currentTarget.tooltip();
            element.unbind("click");
        });
        element.show();
        element.tooltip({
        bodyHandler: function() {         
        return $("<div/>").html(msg);     
        });
    };


    this.TagInValid = function (validationErrors) {
        for (var i in validationErrors) {
            var error = validationErrors[i];
            if (error.Tag != undefined) {
                var element = $("#" + error.Tag);
                this.TagElement(element, error.Error);
            }
        }
    }
    //#endregion

}