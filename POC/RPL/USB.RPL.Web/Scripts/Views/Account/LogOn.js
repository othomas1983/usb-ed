﻿function LogOn() {

    //#region Properties

    this.RequiredFieldMessage = "";

    //#endregion

    //#region Methods

    this.Load = function () {

        this.UX = new RPLUX();

        this.IsLoaded = false;
        this.errorBox = $(".errorBox");
        this.LogInButton = $("#submit");
        this.SurnameField = $("#Surname");
        this.EmailField = $("#Email");
        this.CellphoneField = $("#Cellphone");
        this.IDForceNumberField = $("#IDForceNumber");
        this.Form = $("#logonForm");
        var instance = this;

        $(document).ready(function () {
            var options = {
                auto: 5,
                scroll: 1,
                wrap: 'both'
            }


            if (parent != self) {
                top.location.href = document.location.href;
            }

            $("#logonForm").keypress(function (e) {
                if (e.keyCode == '13') {
                    if (instance.Validate(instance)) {
                        instance.Form.submit();
                        return false;  //suppresses "Ding!" in IE do not delete
                    }
                }
            });

            instance.LogInButton.click(function (e) { e.preventDefault(); if (instance.Validate(instance)) { instance.Form.submit(); } });

            instance.SurnameField.focus();
        });

        this.IsLoaded = true;
    };

    this.Validate = function (instance) {

        if (!instance.IsLoaded) {
            return true;
        }

        var elementsToValidate = new Array();
        elementsToValidate.push({ ElementToValidate: instance.SurnameField, ElementToTag: instance.SurnameField });
        elementsToValidate.push({ ElementToValidate: instance.EmailField, ElementToTag: instance.EmailField });
        elementsToValidate.push({ ElementToValidate: instance.CellphoneField, ElementToTag: instance.CellphoneField });
        elementsToValidate.push({ ElementToValidate: instance.IDForceNumberField, ElementToTag: instance.IDForceNumberField });


        var tagger = new ControlErrorTagger();
        var isValid = true;
        if (instance.EmailField.val().isSpacesOrEmpty() || instance.CellphoneField.val().isSpacesOrEmpty() || instance.IDForceNumberField.val().isSpacesOrEmpty() || instance.SurnameField.val().isSpacesOrEmpty()) {
            isValid = !tagger.TagEmptyFields(elementsToValidate, instance.RequiredFieldMessage);

        }

        return isValid;
    };

    //#endregion

}
