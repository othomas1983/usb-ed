﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace USB.RPL.Web.Wrappers
{
    /// <summary>
    /// Wrapper class for displaying error messages on the view
    /// </summary>
    public class ErrorNotification
    {
        #region Properties

        public string Code { get; set; }
        public string Error { get; set; }

        #endregion

        #region Methods

        public static ErrorNotification Create(string code, string error)
        {
            return new ErrorNotification()
            {
                Code = code,
                Error = error
            };
        }

        #endregion
    }
}