﻿using Airborne;
using USB.RPL.Web.Controllers;
using USB.RPL.Web.Services;

namespace USB.RPL.Web.Actions
{
    /// <summary>
    /// A base class for Controller based actions. Provides safe access to a IRPLClientServicesProvider.
    /// </summary>
    /// <typeparam name="TModel">The type of the model to be used in the action</typeparam>
    public abstract class RPLControllerAction<TModel> : ControllerAction<RPLController, TModel>
        where TModel : class
    {

        protected string HttpVerb { get; set; }

        public bool IsPostAction
        {
            get
            {
                return HttpVerb.ToLowerInvariant().Equals("post");
            }
        }



        #region Locals

        protected IRPLClientServicesProvider RPLClient;

        #endregion

        #region Constructors

        /// <summary>
        /// Initialises the instance IRPLClientServicesProvider.
        /// </summary>
        /// <param name="RPLClient">The instance of IRPLClientServicesProvider to use.</param>
        public RPLControllerAction(IRPLClientServicesProvider RPLClient)
        {
            Guard.InstanceNotNull(RPLClient, "RPLClient");

            this.RPLClient = RPLClient;
        }

        #endregion
    }



    /// <summary>
    /// A base class for Controller based actions. Provides safe access to a IRPLClientServicesProvider.
    /// </summary>
    public abstract class RPLControllerAction : ControllerAction<RPLController>
    {
        #region Constructors

        /// <summary>
        /// Initialises the instance IRPLClientServicesProvider.
        /// </summary>
        /// <param name="RPLClient">The instance of IRPLClientServicesProvider to use.</param>
        public RPLControllerAction(IRPLClientServicesProvider RPLClient)
        {
            Guard.InstanceNotNull(RPLClient, "RPLClient");

            this.RPLClient = RPLClient;
        }

        #endregion

        #region Locals

        protected IRPLClientServicesProvider RPLClient;

        #endregion
    }





 


}
