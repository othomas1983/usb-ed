﻿using System.Web.Mvc;

namespace USB.RPL.Web.Actions
{
    public abstract class ControllerAction<TController>
        where TController : Controller
    {

        public abstract ActionResult Invoke(TController controller);

        protected ViewResult View(Controller controller)
        {
            var viewData = controller.ViewData;

            var viewResult = new ViewResult()
            {
                ViewData = controller.ViewData,
                TempData = controller.TempData
            };

            return viewResult;
        }

        protected ViewResult View(Controller controller, string ViewName)
        {
            var viewData = controller.ViewData;

            var viewResult = new ViewResult()
            {
                ViewData = controller.ViewData,
                TempData = controller.TempData,
                ViewName = ViewName
            };

            return viewResult;
        }

        protected PartialViewResult PartialView(Controller controller)
        {
            var viewData = controller.ViewData;

            var viewResult = new PartialViewResult()
            {
                ViewData = controller.ViewData,
                TempData = controller.TempData
            };

            return viewResult;
        }

    }

}