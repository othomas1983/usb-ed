﻿using System.Web.Mvc;
using USB.RPL.Web.Controllers;
using USB.RPL.Web.Extensions;
using USB.RPL.Web.Services;
using System;
using System.Web;

namespace USB.RPL.Web.Actions
{
    /// <summary>
    /// Calls the ClientAuthentication service of the specified IRPLClientServicesProvider instance to sign out
    /// the currently signed in user properly.
    /// </summary>
    public class LogOffAction : RPLControllerAction
    {
        #region Constructors

        /// <summary>
        /// Initialises a new instance of LogOffAction using the specified IRPLClientServicesProvider.
        /// </summary>
        /// <param name="RPLClient">An instance of IRPLClientServicesProvider</param>
        public LogOffAction(IRPLClientServicesProvider RPLClient) : base(RPLClient) { }

        #endregion

        /// <summary>
        /// Signs out the currently signed in user using the specified instance of IRPLClientServicesProvider.
        /// </summary>
        /// <param name="controller">The calling instance of RPLController</param>
        /// <returns>An appropriate action result</returns>
        public override ActionResult Invoke(RPLController controller)
        {
            var userId = controller.ResolveUserId();
            RPLClient.ClientAuthentication.SignOut();

            RPLClient.Publish(EventTopics.LogOff, userId);
            if (controller.ApplicationContext.IsNotNull())
            {
                controller.ApplicationContext.Clear();
            }

            return Redirects.ToWelcome();
        }
    }
}