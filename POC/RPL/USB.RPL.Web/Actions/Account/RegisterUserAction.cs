﻿using System;
using System.Web.Mvc;
using Airborne;
using USB.RPL.Domain.User;
using USB.RPL.Web.Content;
using USB.RPL.Web.Contexts;
using USB.RPL.Web.Controllers;
using USB.RPL.Web.Extensions;
using USB.RPL.Web.Models;
using USB.RPL.Web.Services;
using System.Web;

namespace USB.RPL.Web.Actions
{
    public class RegisterUserAction : RPLControllerAction<RegisterUserViewModel>
    {
        #region Constructors

        public RegisterUserAction(IRPLClientServicesProvider RPLClient)
            : base(RPLClient)
        {

        }

        #endregion

        #region Properties

        public Func<RegisterUserViewModel, ViewResult> ShowView { get; set; }
        
        #endregion

        #region Overrides

        /// <summary>
        /// Signs in the specified user using the specified instance of IRPLClientServicesProvider.
        /// </summary>
        /// <param name="controller">The calling instance of RPLController</param>
        /// <returns>An appropriate action result</returns>
        public override ActionResult Execute(RPLController controller, RegisterUserViewModel model)
        {
            Guard.InstanceNotNull(controller, "controller");
            Guard.ArgumentNotNull(model, "model");


            var profile = new UserProfile(){
                Cellphone = model.Cellphone,
                Email = model.Email,
                FirstName = model.Name,
                Gender = model.Gender,
                IDForceNumber = model.IDForceNumber,
                Language = model.Language,
                LastName = model.Surname,
                MaritalStatus = model.MaritalStatus,
                Location = model.Location,
                Province = model.Province,
                Race = model.Race,
                RPLDiscipline = model.RPLDiscipline,
                Status = UserStatus.Pending,
                Title = model.Title
            };

            RPLClient.Users.SaveUserProfile(profile);
            model.InfoSaved = true;
            EmailService.SendRegistrationEmail(profile);
          
            return ShowView.Invoke(model);;
        }

        #endregion
    }

}
