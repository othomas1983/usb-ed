﻿using System;
using System.Web.Mvc;
using Airborne;
using USB.RPL.Domain.User;
using USB.RPL.Web.Content;
using USB.RPL.Web.Contexts;
using USB.RPL.Web.Controllers;
using USB.RPL.Web.Extensions;
using USB.RPL.Web.Models;
using USB.RPL.Web.Services;
using System.Web;

namespace USB.RPL.Web.Actions
{
    /// <summary>
    /// Calls the ClientAuthentication service of the specified IRPLClientServicesProvider instance to sign in 
    /// the specified user and redirect to a url if specified.
    /// </summary>
    public class LogOnUserAction : RPLControllerAction<LogOnViewModel>
    {
        #region Constructors

        /// <summary>
        /// Initialises a new instance of LogOnUserAction using the specified IRPLClientServicesProvider.
        /// </summary>
        /// <param name="RPLClient">An instance of IRPLClientServicesProvider</param>
        public LogOnUserAction(IRPLClientServicesProvider RPLClient)
            : base(RPLClient)
        {

        }

        #endregion

        #region Properties

        /// <summary>
        /// A url to redirect to if login succeeds
        /// </summary>
        public string ReturnUrl { get; set; }

        /// <summary>
        /// A function returning a ViewResult that will be populated with the calculated information.
        /// </summary>
        public Func<ViewResultBase> ViewResult;
        
        public ApplicationContext Context { get; set; }

        #endregion

        #region Overrides

        /// <summary>
        /// Signs in the specified user using the specified instance of IRPLClientServicesProvider.
        /// </summary>
        /// <param name="controller">The calling instance of RPLController</param>
        /// <returns>An appropriate action result</returns>
        public override ActionResult Execute(RPLController controller, LogOnViewModel model)
        {
            Guard.InstanceNotNull(controller, "controller");
            Guard.ArgumentNotNull(model, "model");
            Guard.InstanceNotNull(ViewResult, "ViewResult");

            if (RPLClient.Users.Authenticate(model.Email, model.Cellphone, model.Surname))
            {
                var context = controller.ApplicationContext;
                var userProfile = RPLClient.GetProfile(model.Email, model.Cellphone, model.Surname);
                context.Bind(userProfile);
                RPLClient.Cache.Save("UserProfile", userProfile);
                context.SessionTimeout = Config.DefaultSessionTimeoutMinutes;
                RPLClient.ClientAuthentication.SignIn(model.Email, false);
                return Redirects.ToLandingPage();
            }
            else
            {
                model.LoginFailed = true;
            }

            var view = ViewResult.Invoke();
            view.ViewData.Model = model;
            return view;
        }

        #endregion
    }

}
