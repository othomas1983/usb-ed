﻿using Airborne;
using USB.RPL.Web.Services;

namespace USB.RPL.Web.Actions
{
    /// <summary>
    /// Abstract Class for RPL action that takes a model
    /// </summary>
    public abstract class RPLAction<TResult, TModel> : RPLAction<TResult>
    {
        #region Locals
      
        protected TModel model;

        #endregion

        #region Method

        public virtual TResult Invoke(object context, TModel model)
        {
            Init(context);

            this.model = model;

            return InvokeInternal();
        }

        #endregion
    }

    /// <summary>
    /// Abstract Class for RPL action that does not take a model
    /// </summary>
    public abstract class RPLAction<TResult>
    {
        #region Locals

        protected long? userId;

        protected IRPLClientServicesProvider RPLClient;

        #endregion

        #region Methods

        protected virtual void Init(dynamic context)
        {
            Guard.ArgumentNotNull(context.RPLClient, "RPLClient");
            Guard.ArgumentNotNull(context.UserId, "UserId");

            RPLClient = context.RPLClient;
            userId = context.UserId;
        }

        public virtual TResult Invoke(object context)
        {
            Init(context);

            return InvokeInternal();
        }

        protected abstract TResult InvokeInternal();

        #endregion
    }

}