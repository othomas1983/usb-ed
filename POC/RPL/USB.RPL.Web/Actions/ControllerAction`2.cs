﻿using System.Web.Mvc;

namespace USB.RPL.Web.Actions
{
    public abstract class ControllerAction<TController, TModel>
        where TController : Controller
        where TModel : class
    {
        public abstract ActionResult Execute(TController controller, TModel model);

        protected ViewResult View(Controller controller, string ViewName)
        {
            var viewData = controller.ViewData;

            var viewResult = new ViewResult()
            {
                ViewData = controller.ViewData,
                TempData = controller.TempData,
                ViewName = ViewName
            };

            return viewResult;
        }

        protected ViewResult View(Controller controller, object model)
        {
            var viewData = controller.ViewData;

            viewData.Model = model;

            var viewResult = new ViewResult()
            {
                ViewData = controller.ViewData,
                TempData = controller.TempData
            };

            return viewResult;
        }

        protected ViewResult View(Controller controller, object model, string ViewName)
        {
            var viewData = controller.ViewData;

            viewData.Model = model;

            var viewResult = new ViewResult()
            {
                ViewData = controller.ViewData,
                TempData = controller.TempData,
                ViewName = ViewName
            };

            return viewResult;
        }


        protected PartialViewResult PartialView(Controller controller, object model)
        {
            var viewData = controller.ViewData;

            viewData.Model = model;

            var viewResult = new PartialViewResult()
            {
                ViewData = controller.ViewData,
                TempData = controller.TempData
            };

            return viewResult;
        }
    }

}