﻿using USB.RPL.Web.Controllers;

namespace USB.RPL.Web.Actions
{
    /// <summary>
    /// Creates a specific context for the action to work with. This context is specialized to the requirements of the action
    /// </summary>
    public class ActionContextFactory
    {
        #region CreateActionContext Overloads

        /// <summary>
        /// Creates a generic action context
        /// </summary>
        public static object CreateActionContext(RPLController controller)
        {
            return CreateGenericContext(controller);
        }

        #endregion

        #region Contexts

        private static object CreateGenericContext(RPLController controller)
        {
            return new
            {
                RPLClient = controller.RPLClient,
                UserId = controller.ResolveUserId(),
                ApplicationContext = controller.ApplicationContext,
            };
        }

        #endregion
    }
}
