﻿using System.ComponentModel.DataAnnotations;
using USB.RPL.Web.Properties;

namespace USB.RPL.Web.Models
{
    /// <summary>
    /// A model class containing all the reuired information for the log on process.
    /// </summary>
    public class LogOnViewModel
    {

        public string Surname { get; set; }

        public string Email { get; set; }

        public string Cellphone { get; set; }

        public string IDForceNumber { get; set; }

        public bool LoginFailed { get; set; }
    }

}