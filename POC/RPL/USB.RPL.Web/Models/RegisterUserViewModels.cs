﻿using System.ComponentModel.DataAnnotations;
using USB.RPL.Web.Properties;

namespace USB.RPL.Web.Models
{
    public class RegisterUserViewModel
    {
        public string Title { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string Email { get; set; }

        public string Cellphone { get; set; }

        public string IDForceNumber { get; set; }

        public string Language { get; set; }

        public string Race { get; set; }

        public string Gender { get; set; }

        public string MaritalStatus { get; set; }

        public string Location { get; set; }

        public string Province { get; set; }

        public string RPLDiscipline { get; set; }

        public bool InfoSaved { get; set; }

    }

}