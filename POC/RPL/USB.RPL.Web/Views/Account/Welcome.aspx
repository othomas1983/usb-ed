﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Login.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderPlaceHolder" runat="server">
<title>RPL - Welcome</title>
<script type="text/javascript">
    $(document).ready(function () {
        $("#contentWrapper").attr('class', 'loginBG');
    });
 </script>
</asp:Content>


<asp:Content ID="WelcomeContent" ContentPlaceHolderID="MainContent" runat="server">
<%
    var pathToLogin = Url.Action("LogOn", new { controller = "Account", area = "" });
    var pathToRegister = Url.Action("Register", new { controller = "Account", area = "" });
    
%>
    <div class="loginLeft loginContent fl">
				<div class="heading">
					<h1>Welcome please select your next step:</h1>
				</div>
				
                <p>By the end of this process, you'll be able to track your learning history and know your NQF Level</p>
				
				<div class="welcomeWrapper">
					<a href="<%= pathToRegister %>">Register</a>
				</div>
				
				<div class="welcomeWrapper">
					<a href="<%= pathToLogin %>">Login</a>
				</div>
			</div>
			
			<div class="loginRight loginContent fr">
				<div class="heading">
					<h1>For Assistance:</h1>
				</div>

				<table class="loginSection contact">
					<tr>
						<td style="width:250px;"><strong>Tel:</strong></td>
						<td>+27 945 4567</td>
					</tr>
					<tr>
						<td><strong>Email:</strong></td>
						<td>info@usbrpl.co.za</td>
					</tr>
				</table>
			</div>

</asp:Content>                                                         