﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Login.Master" Inherits="System.Web.Mvc.ViewPage<USB.RPL.Web.Models.LogOnViewModel>" %>

<asp:Content runat="server" ContentPlaceHolderID="HeaderPlaceHolder">
    <title>RPL - Login</title>
    <script language="javascript" type="text/javascript">
        var logOn = new LogOn();
        
        logOn.RequiredFieldMessage = "This is a required field";
        $(function () { logOn.Load(); });
    </script>
</asp:Content>
<asp:Content ID="loginContent" ContentPlaceHolderID="MainContent" runat="server">
   <%
    var pathToRegister = Url.Action("Register", new { controller = "Account", area = "" });
    
%> 
 <% using (Html.BeginForm("LogOn", "Account", FormMethod.Post, new { id = "logonForm" }))
       { %>
    			<div class="loginLeft loginContent fl">
				<div class="heading">
					<h1>Please login below:</h1>
					<span class="register"><a href="<%= pathToRegister %>">Register Here</a></span>
				</div>
                <%if (Model.IsNotNull() && Model.LoginFailed)
                  { %>
			<div class="errorBox" style="width:450px; margin-left:10px; margin-right:10px;">
                Login Failed: Please check information below and try again.
            </div>
            <%} %>
				<table class="loginSection">
					<tr>
						<td><label>Surname</label></td>
						<td><%: Html.TextBoxFor(m => m.Surname, new { tabindex = 1 })%></td>
					</tr>
					<tr>
						<td><label>Email</label></td>
						<td><%: Html.TextBoxFor(m => m.Email, new { tabindex = 2 })%></td>
					</tr>
					<tr>
						<td><label>Cellphone</label></td>
						<td><%: Html.TextBoxFor(m => m.Cellphone, new { tabindex = 3 })%></td>
					</tr>
					<tr>
						<td><label>ID/Force</label></td>
						<td><%: Html.TextBoxFor(m => m.IDForceNumber, new { tabindex = 4 })%></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td class="login button">
                        <a href="" id="submit" tabindex="5"><input type="button" value="Login" /></a>
						</td>
					</tr>
				</table>
				
                <div style="height: 23px">
						<input name="Checkbox1" type="checkbox" />I have read and agree to the policies.	
					</div>
                    <div style="height: 23px">
						<input name="Checkbox1" type="checkbox" />I agree to pay an administration fee of R1200 before completion of the process.
					</div>
                    <div style="height: 23px">
						<input name="Checkbox1" type="checkbox" />I have read the guidelines on the process.
                        </div>
			</div>
			
			<div class="loginRight loginContent fr">
				<div class="heading">
					<h1>For Assistance:</h1>
				</div>

				<table class="loginSection contact">
					<tr>
						<td style="width:250px;"><strong>Tel:</strong></td>
						<td>+27 945 4567</td>
					</tr>
					<tr>
						<td><strong>Email:</strong></td>
						<td>info@usbrpl.co.za</td>
					</tr>
				</table>
			</div>

            <% } %>
    <%--<div class="heading">
			<h1>Please provide us with your login details:</h1>
		</div>
    <% using (Html.BeginForm("LogOn", "Account", FormMethod.Post, new { id = "logonForm" }))
       { %>
    
            <table class="fieldSection centerDivTable" style="width: 670px; border:0px">
			    <tr>
				    <td><label >Email</label></td>
				    <td><%: Html.TextBoxFor(m => m.Email, new { tabindex = 1 })%></td>
			    </tr>
			    <tr>
				    <td><label >Cellphone</label></td>
				    <td><%: Html.TextBoxFor(m => m.Cellphone, new { tabindex = 2 })%></td>
			    </tr>
			    <tr>
				    <td><label >ID/Force</label></td>
				    <td><%: Html.TextBoxFor(m => m.IDForceNumber, new { tabindex = 3 })%></td>
			    </tr>
			    <tr>
				    <td>&nbsp;</td>
				    <td class="login">
                        <a href="" id="submit"><input type="button" value="Login" /></a>
                    </td>
			    </tr>
		    </table>
    
    <% } %>--%>
</asp:Content>
