﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Login.Master" Inherits="System.Web.Mvc.ViewPage<USB.RPL.Web.Models.RegisterUserViewModel>" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeaderPlaceHolder" runat="server">
<title>RPL - Register</title>
<script type="text/javascript">
    $(document).ready(function () {

        $("#contentWrapper").attr('class', 'regBG');

    });
 </script>
</asp:Content>


<asp:Content ID="WelcomeContent" ContentPlaceHolderID="MainContent" runat="server">
<%
    var pathToLogin = Url.Action("LogOn", new { controller = "Account", area = "" });
    
%>
			<div class="loginLeft regContent fl">
				<div class="heading">
					<h1>Please complete the registration form:</h1>
					<span class="back"><a href="<%= pathToLogin %>">Login Here</a></span>
				</div>
			
			<div class="c"></div>
			<% using (Html.BeginForm("RegisterUser", "Account", FormMethod.Post, new { id = "registerForm" }))
                { %>
				<table class="regSection fl">
					<tr>
						<td><label>Title</label></td>
						<td><%: Html.TextBoxFor(m => m.Title, new { tabindex = 1 })%></td>
					</tr>
					
					<tr>
						<td><label>Name(s)</label></td>
						<td><%: Html.TextBoxFor(m => m.Name, new { tabindex = 2 })%></td>
					</tr>
					<tr>
						<td><label>Surname</label></td>
						<td><%: Html.TextBoxFor(m => m.Surname, new { tabindex = 3 })%></td>
					</tr>
					<tr>
						<td><label>ID Number</label></td>
						<td><%: Html.TextBoxFor(m => m.IDForceNumber, new { tabindex = 4 })%></td>
					</tr>
					<tr>
						<td><label>Email</label></td>
						<td><%: Html.TextBoxFor(m => m.Email, new { tabindex = 5 })%></td>
					</tr>
					<tr>
						<td><label>Cellphone</label></td>
						<td><%: Html.TextBoxFor(m => m.Cellphone, new { tabindex = 6 })%></td>
					</tr>					
				</table>
				
				<table class="regSection fr">
					<tr>
						<td><label>Language</label></td>
						<td><%: Html.TextBoxFor(m => m.Language, new { tabindex = 7 })%></td>
					</tr>
					
					<tr>
						<td><label >Nationality</label></td>
						<td><%: Html.TextBoxFor(m => m.Race, new { tabindex = 8 })%></td>
					</tr>
					<tr>
						<td><label >Gender</label></td>
						<td><%: Html.TextBoxFor(m => m.Gender, new { tabindex = 9 })%></td>
					</tr>
					<tr>
						<td><label >Marital Status</label></td>
						<td><%: Html.TextBoxFor(m => m.MaritalStatus, new { tabindex = 10 })%></td>
					</tr>
					<tr>
						<td><label class="regField">Location</label></td>
						<td class="regInput"><%: Html.TextBoxFor(m => m.Location, new { tabindex = 11 })%><br /><span>(Where are you based now?)</span></td>
					</tr>
					<tr>
						<td><label >Province</label></td>
						<td><%: Html.TextBoxFor(m => m.Province, new { tabindex = 12 })%></td>
					</tr>
					<tr>
						<td><label >RPL Discipline</label></td>
						<td><%: Html.TextBoxFor(m => m.RPLDiscipline, new { tabindex = 13 })%></td>
					</tr>

					
					<tr>
						<td>&nbsp;</td>
						<td class="login button">
							<input name="Submit" type="submit" value="Submit" tabindex="14" />
						</td>
					</tr>
				</table>	
                <%} %>			
			</div>
			
			<div class="loginRight regContent regContact fr">
				<div class="heading">
					<h1>For Assistance:</h1>
				</div>

				<table class="loginSection contact">
					<tr>
						<td style="width:250px;"><strong>Tel:</strong></td>
						<td>+27 945 4567</td>
					</tr>
					<tr>
						<td><strong>Email:</strong></td>
						<td>info@usbrpl.co.za</td>
					</tr>
				</table>
			</div>
			<%if (Model.InfoSaved)
            { %>
           
			<div id="confirmationBox" class="loginRight regContent fr notifyBox s" style="display:none; margin:15px 5px 5px 5px">
				<div class="statusIcon success "></div>
				<h3>Thank you, your info has been received</h3>
				<div class="c"></div>
					<p>Please allow approximately 6 days for information to be verified. You will receicve an email once the verification process has been completed.</p>
			</div>
             <script type="text/javascript">
                 $(document).ready(function () {
                     $('#confirmationBox').dialog({ autoOpen: true, modal: true, draggable: false, resizable: false,
                         buttons: [{
                             text: "Ok",
                             click: function () { $(this).dialog("close"); location.href = '<%= pathToLogin%>'; }
                         }]
                     });
                 });
            </script>
		    <%} %>
			<%--<div class="loginRight regContent fr notifyBox e">
				<div class="statusIcon error"></div>
				<h3>Sorry your info is not verified</h3>
				<div class="c"></div>
					<p>Please try again later.</p>
			</div>--%>


</asp:Content>                                                         