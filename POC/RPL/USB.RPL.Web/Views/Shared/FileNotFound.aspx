﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RPL.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>


<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">

<div class="page-not-found">
    <span class="ErrorImg"></span>
 	    <h2>Sorry, we can't find that file.</h2>
       		<p>File Not Found <%Response.Write(this.TempData["Filename"]);%></p>
</div>
   
</asp:Content> 
