﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Login.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>


<asp:Content ID="Content3" ContentPlaceHolderID="PageTitlePlaceHolder" runat="server">
      Browser is incompatible
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="page-not-found">
       <span class="ErrorImg"></span>
          <h2>Incompatible Browser</h2>
            <p>The Internet browser you’re using is not supported by RPL. Portions of our website may not display or <br />
               function properly when used with this browser.</p>
            <p>We recommend using the most recent version of one of these supported browsers:</p>
	            <ul>
	                <li><a href="http://www.microsoft.com/windows/internet-explorer/default.aspx" title="Download Microsoft Internet Explorer">Download Microsoft Internet Explorer 7 or above</a></li>
	                <li><a href="http://www.google.com/chrome" title="Download Google Chrome">Download Google Chrome</a></li>
	                <li><a href="http://www.mozilla.com/en-US/firefox/">Download Mozilla Firefox</a></li>
	            </ul>
     </div>

</asp:Content>