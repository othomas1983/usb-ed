﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RPL.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<%@ Import Namespace="USB.RPL.Web" %>

<asp:Content ID="Content" ContentPlaceHolderID="Content" runat="server">

<!-- ERROR PAGE -->

<%
    if (!Session.UserIsLoggedOn())
    {
        Response.Redirect("~/home/Error");
    }
 %>

    <div class="page-not-found">
       <span class="ErrorImg"></span>
         <h2>Oops, an unknown error has occurred</h2>
            <p>Please return to the <%:Html.ActionLink("HomePage", "Index", new { @Controller = "Portal", @Area = "Portal"})%> or try again.</p>
    </div>
</asp:Content>



