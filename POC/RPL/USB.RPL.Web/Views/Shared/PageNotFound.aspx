﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RPL.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>




<asp:Content ID="Content" ContentPlaceHolderID="Content" runat="server">

<div class="page-not-found">
    <span class="ErrorImg"></span>
 		<h2>Sorry, we can't find that page.</h2>
            <p>Please return to the <%:Html.ActionLink("HomePage", "Index", new { @Controller = "Portal", @Area = "Portal"})%> or try again.</p> 
</div>

</asp:Content>


