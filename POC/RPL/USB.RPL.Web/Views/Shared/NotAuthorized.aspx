﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/RPL.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">

<div class="page-not-found">
    <span class="ErrorImg"></span>
 		<h2>You are currently not authorized to access the <%: ViewData["Action"] %> page in the <%: ViewData["Tab"] %> area.</h2>
	        <p>Please contact your system administrator for further information.</p>
</div>

</asp:Content>
