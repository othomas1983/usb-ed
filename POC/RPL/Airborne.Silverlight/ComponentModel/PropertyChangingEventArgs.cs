﻿
namespace System.ComponentModel
{
    /// <summary>
    /// Provides data for the System.ComponentModel.INotifyPropertyChanging.PropertyChanging event.
    /// Copied from the full implementation of .net for compatability reasons
    /// </summary>
    public class PropertyChangingEventArgs : EventArgs
    {

        /// <summary>
        /// Initializes a new instance of the System.ComponentModel.PropertyChangingEventArgs
        ///     class.
        /// </summary>
        /// <param name="propertyName">The name of the property whose value is changing.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PropertyChangingEventArgs(string propertyName)
        {
            PropertyName = propertyName;
        }

        /// <summary>
        /// Gets the name of the property whose value is changing.
        /// </summary>
        public virtual string PropertyName { get; set; }
    }
}