﻿
namespace System.ComponentModel
{

    /// <summary>
    /// Represents the method that will handle the System.ComponentModel.INotifyPropertyChanging.PropertyChanging
    ///     event of an System.ComponentModel.INotifyPropertyChanging interface.
    ///      Copied from the full implementation of .net for compatability reasons
    /// </summary>
    /// <param name="sender"> The source of the event.</param>
    /// <param name="e">A System.ComponentModel.PropertyChangingEventArgs that contains the event data.</param>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1003:UseGenericEventHandlerInstances")]
    public delegate void PropertyChangingEventHandler(object sender, PropertyChangingEventArgs e);
}
