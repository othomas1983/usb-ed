﻿namespace System.ComponentModel
{
    /// <summary>
    /// Notifies clients that a property value is changing.
    /// Copied from the full implementation of .net for compatability reasons
    /// </summary>
    public interface INotifyPropertyChanging
    {
        /// <summary>
        /// Occurs when a property value is changing.
        /// </summary>
        event PropertyChangingEventHandler PropertyChanging;
    }
}