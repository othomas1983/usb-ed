﻿using System;
using USB.RPL.Domain.User;

namespace USB.RPL.Persistence.Converters
{
    public class QualificationLevelConverter : SimpleConverterBase<QualificationLevel>
    {
        public QualificationLevelConverter()
            : base(DBNull.Value)
        {
            AddTranslation(QualificationLevel.Junior, 1);
            AddTranslation(QualificationLevel.Intermediate, 2);
            AddTranslation(QualificationLevel.Senior, 3);
        }
    }
}
