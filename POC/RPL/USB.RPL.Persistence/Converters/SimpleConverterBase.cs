﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using NHibernate;
using NHibernate.SqlTypes;
using NHibernate.UserTypes;

namespace USB.RPL.Persistence.Converters
{
    /// <summary>
    /// Provides the base class implementation for a simple converter.
    /// </summary>
    public abstract class SimpleConverterBase<T> : IUserType
    {
        private readonly object nullValue;
        protected readonly List<KeyValuePair<T, object>> TranslationList;

        #region Constructors

        protected SimpleConverterBase(object nullValue)
        {
            this.nullValue = nullValue;
            TranslationList = new List<KeyValuePair<T, object>>();
        }

        #endregion

        #region Properties


        #endregion

        #region Public Methods

        public void AddTranslation(T key, object value)
        {
            TranslationList.Add(new KeyValuePair<T, object>(key, value));
        }

        #endregion

        protected bool IsValueNull(object value)
        {
            return value == null || value.GetType() == typeof(System.DBNull);
        }


        #region Implementation of IUserType

        new public bool Equals(object x, object y)
        {
            if (x == y) return true;
            if (x == null || y == null) return false;
            return x.Equals(y);
        }

        public int GetHashCode(object x)
        {
            if (x != null)
            {
                return x.GetHashCode();
            }
            return 0;
        }

        public virtual object NullSafeGet(IDataReader rs, string[] names, object owner)
        {
            var dataValue = rs.GetValue(rs.GetOrdinal(names[0]));

            var objectValue = TranslationList.FirstOrDefault(kvp => kvp.Value.Equals(dataValue)).Key;
            return objectValue == null ? nullValue : objectValue;
        }

        public void NullSafeSet(IDbCommand cmd, object value, int index)
        {
            var dataValue = TranslationList.FirstOrDefault(kvp => kvp.Key.Equals(value)).Value;
            ((IDataParameter)cmd.Parameters[index]).Value = dataValue ?? nullValue;
        }

        public object DeepCopy(object value)
        {
            return value;
        }

        public object Replace(object original, object target, object owner)
        {
            return original;
        }

        public object Assemble(object cached, object owner)
        {
            return cached;
        }

        public object Disassemble(object value)
        {
            return value;
        }

        public virtual SqlType[] SqlTypes
        {
            get { return new[] { NHibernateUtil.String.SqlType }; }
        }

        public Type ReturnedType
        {
            get { return typeof(T); }
        }

        public bool IsMutable
        {
            get { return false; }
        }

        #endregion
    }
}
