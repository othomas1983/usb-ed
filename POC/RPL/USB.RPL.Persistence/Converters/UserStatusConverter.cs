﻿using System;
using USB.RPL.Domain.User;

namespace USB.RPL.Persistence.Converters
{
    public class UserStatusConverter : SimpleConverterBase<UserStatus>
    {
        public UserStatusConverter()
            : base(DBNull.Value)
        {
            AddTranslation(UserStatus.Pending, 1);
            AddTranslation(UserStatus.Confirmed, 2);
            AddTranslation(UserStatus.Copied, 3);
            AddTranslation(UserStatus.Update, 4);
        }
    }
}
