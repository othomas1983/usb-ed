﻿using System.Data;

namespace USB.RPL.Persistence.Commands
{
    public static class IDataParameterCollectionExtensions
    {
        public static void AddWithValue(this IDataParameterCollection list, IDbCommand cmd, string parameterName, object value)
        {
            var parameter = cmd.CreateParameter();
            parameter.ParameterName = parameterName;
            parameter.Value = value;
            cmd.Parameters.Add(parameter);
        }
    }
}
