﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace USB.RPL.Domain.Submissions.User
{
    public class SubmissionProfileCourse
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public string Course { get; set; }

        public string Qualification { get; set; }
    }
}
