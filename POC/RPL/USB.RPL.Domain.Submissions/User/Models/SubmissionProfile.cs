﻿using System;
using System.Collections.Generic;
using System.Linq;
using Airborne.Domain;

namespace USB.RPL.Domain.Submissions.User
{

    public class SubmissionProfile : IAggregate
    {
        public SubmissionProfile()
        {
            Courses = new List<SubmissionProfileCourse>();
        }

        #region Properties

        public int Id { get; set; }

        public int? PrimaryId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Field { get; set; }

        public IList<SubmissionProfileCourse> Courses { get; set; }

        #endregion

    }

}
