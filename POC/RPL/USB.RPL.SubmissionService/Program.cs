﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using log4net;

namespace USB.RPL.SubmissionService
{
    static class Program
    {
        private static ILog Logger = LogManager.GetLogger("SubmissionSchedule");

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            log4net.Config.XmlConfigurator.Configure();

            var interactive = args.FirstOrDefault(arg => string.Compare(arg, "/console", StringComparison.OrdinalIgnoreCase) == 0);

            if (interactive.IsNullOrEmpty())
            {
                Logger.Info("Start process.....");
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[] 
			    { 
				    new SubmissionService() 
			    };
                ServiceBase.Run(ServicesToRun);
            }
            else
            {
                var schedular = new SubmissionSchedular();
                schedular.Start();

                Console.WriteLine("Starting in interactive mode : press 'return' to quit");
                Console.ReadLine();
                Console.WriteLine("Stopping processing...");

                while (schedular.IsBusy)
                {
                    Thread.Sleep(1000);
                }
            }
        }
    }
}
