﻿using System.Configuration;

namespace USB.RPL.SubmissionService
{
    /// <summary>
    /// Calculator submission schedule section configuration settings
    /// </summary>
    public class ScheduleConfiguration : ConfigurationSection
    {
        /// <summary>
        /// Gets or sets the polling interval in minutes.
        /// </summary>
        /// <value>The polling interval.</value>
        [ConfigurationProperty(Descriptors.PollingInterval, DefaultValue = 15d)]
        public double PollingInterval
        {
            get
            {
                return (double)this[Descriptors.PollingInterval];
            }
            set
            {
                this[Descriptors.PollingInterval] = value;
            }
        }

        [ConfigurationProperty(Descriptors.SmtpServer)]
        public string SmtpServer
        {
            get
            {
                return (string)this[Descriptors.SmtpServer];
            }
            set
            {
                this[Descriptors.SmtpServer] = value;
            }
        }

        [ConfigurationProperty(Descriptors.AdminEmailAddress)]
        public string AdminEmailAddress
        {
            get
            {
                return (string)this[Descriptors.AdminEmailAddress];
            }
            set
            {
                this[Descriptors.AdminEmailAddress] = value;
            }
        }

        /// <summary>
        /// Gets the email of the people that need to be alerted in case of an issue
        /// </summary>
        /// <value>The addresses.</value>
        [ConfigurationProperty(Descriptors.AlertAddresses, IsDefaultCollection = false)]
        [ConfigurationCollection(typeof(ScheduleAlerts),
            AddItemName = "add",
            ClearItemsName = "clear",
            RemoveItemName = "remove")]
        public ScheduleAlerts AlertAddresses
        {
            get
            {
                ScheduleAlerts addresses =
                    (ScheduleAlerts)base[Descriptors.AlertAddresses];
                return addresses;
            }
        }

        public static class Descriptors
        {
            public const string PollingInterval = "pollingInterval";
            public const string SmtpServer = "smtpServer";
            public const string AlertAddresses = "alertAddresses";
            public const string AdminEmailAddress = "adminEmailAddress";
        }
    }


}
