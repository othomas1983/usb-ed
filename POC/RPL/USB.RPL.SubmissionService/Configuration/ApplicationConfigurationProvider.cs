﻿using System.Collections.Generic;
using System.Configuration;
using System;
using System.Linq;
using Microsoft.Practices.Unity;

namespace USB.RPL.SubmissionService
{
    public class ApplicationConfigurationProvider : IConfigurationProvider
    {
        private ScheduleConfiguration ConfigSettings { get; set; }

        public ApplicationConfigurationProvider()
        {
            ConfigSettings = ConfigurationManager.GetSection("serviceTimerSchedule") as ScheduleConfiguration;
        }

        #region IConfigurationProvider Members

        public double PollingInterval
        {
            get
            {
                return ConfigSettings.PollingInterval;
            }
            set
            {
                ConfigSettings.PollingInterval = value;
            }
        }

        public string SmtpServer
        {
            get
            {
                return ConfigSettings.SmtpServer;
            }
            set
            {
                ConfigSettings.SmtpServer = value;
            }
        }

        public string AdminEmailAddress
        {
            get
            {
                return ConfigSettings.AdminEmailAddress;
            }
            set
            {
                ConfigSettings.AdminEmailAddress = value;
            }
        }

        public IEnumerable<string> AlertEmailAddresses
        {
            get
            {
                IList<string> addresses = new List<string>();
                foreach (var item in ConfigSettings.AlertAddresses.OfType<AlertConfigElement>())
                {
                    addresses.Add(item.Address);
                }
                return addresses;
            }
        }


        #endregion
    }
}
