﻿using System.Collections.Generic;

namespace USB.RPL.SubmissionService
{
    public interface IConfigurationProvider
    {
        /// <summary>
        /// Polling interval in seconds
        /// </summary>
        double PollingInterval { get; }

        /// <summary>
        /// Smtp Server
        /// </summary>
        string SmtpServer { get; }

        /// <summary>
        /// Admin Email Address
        /// </summary>
        string AdminEmailAddress { get; }

        /// <summary>
        /// Addresses to be alerted on an error.
        /// </summary>
        IEnumerable<string> AlertEmailAddresses { get; }

    }
}
