﻿namespace USB.RPL.SubmissionService
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SubmissionServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.SubmissionServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // SubmissionServiceProcessInstaller
            // 
            this.SubmissionServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.SubmissionServiceProcessInstaller.Password = null;
            this.SubmissionServiceProcessInstaller.Username = null;
            // 
            // SubmissionServiceInstaller
            // 
            this.SubmissionServiceInstaller.DisplayName = "Submission Service - RPL";
            this.SubmissionServiceInstaller.ServiceName = "SubmissionService";
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.SubmissionServiceProcessInstaller,
            this.SubmissionServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller SubmissionServiceProcessInstaller;
        private System.ServiceProcess.ServiceInstaller SubmissionServiceInstaller;
    }
}