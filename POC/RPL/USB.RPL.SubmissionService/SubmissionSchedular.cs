﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Net.Mail;
using System.Timers;
using Airborne.Domain.Repository;
using Airborne.Notifications;
using USB.RPL.Domain;
using log4net;
using Airborne.Nhibernate;
using Airborne.Nhibernate.Domain.Repository;
using USB.RPL.Domain.User;
using USB.RPL.Domain.Submissions.User;

namespace USB.RPL.SubmissionService
{
    public class SubmissionSchedular : IDisposable
    {
        #region Properties

        public bool IsBusy { get; private set; }

        #endregion

        #region Locals

        private static ILog Logger = LogManager.GetLogger("SubmissionSchedule");

        private IRPLSystemFacade RPLSystem;

        IConfigurationProvider configuration;

        Timer scheduleTimer;

        bool serviceStopped;

        object mailLock = new object();

        public IRepository SubmissionRepository { get; private set; }

        public IRepository PrimaryRepository { get; private set; }

        #endregion

        #region Constructors

        public SubmissionSchedular()
        {
            RPLSystem = RPLSystemFactory.CreateSystem();
            configuration = RPLSystem.GetService<IConfigurationProvider>();
        }

        #endregion

        #region Methods


        /// <summary>
        /// Starts the timer loop that will initiate processing
        /// based on the polling interval set in configuration
        /// </summary>
        public void Start()
        {
            Logger.InfoFormat("Service starting {0}", DateTime.Now);

            if (scheduleTimer.IsNotNull())
            {
                Logger.Info("Dispose previous timer");
                scheduleTimer.Dispose();
            }

            scheduleTimer = ConfigureTimer();
            serviceStopped = false;
            IsBusy = true;

            scheduleTimer.Start();
        }

        public void Stop()
        {
            Logger.Error("Indication recieved to stop the service");
            serviceStopped = true;
        }

        private void InitiateProcessing()
        {
            SetupRepositories();
            try
            {
                TransferPrimaryProfilesToSubmission();
            }
            catch (Exception ex)
            {
                Logger.Info("Error transfering profiles to the submission database...");
                Logger.Error(ex.Message, ex);
            }

            if (serviceStopped)
            {
                RaiseProcessorStopped();
            }
        }

        private void TransferPrimaryProfilesToSubmission()
        {
            Logger.Info("Finding records to process in primary...");
            var profiles = PrimaryRepository.FindAll<PrimaryProfile>(up => up.Status == UserStatus.Pending);

            Logger.InfoFormat("Found {0} confirmed records in staging to process", profiles.Count());

            foreach (var profile in profiles)
            {
                var submissionProfile = SubmissionRepository.First<SubmissionProfile>(sp => sp.PrimaryId == profile.Id);

                if (submissionProfile.IsNull())
                {
                    submissionProfile = new SubmissionProfile();
                }
                //submissionProfile.Field = profile.Field.Name;
                submissionProfile.FirstName = profile.FirstName;
                submissionProfile.LastName = profile.LastName;
                submissionProfile.PrimaryId = profile.Id;

                submissionProfile.Courses.Clear();
                //foreach (var course in profile.Courses)
                //{
                //    submissionProfile.Courses.Add(new SubmissionProfileCourse() { Course = course.Course.Name, Qualification = course.Qualification.ToString() });
                //}

                using (var tx = SubmissionRepository.BeginTransaction())
                {
                    try
                    {
                        SubmissionRepository.Update(submissionProfile);
                        tx.Commit();
                        Logger.InfoFormat("Added profile '{0} to the submission database", profile.Id);
                    }
                    catch (Exception ex)
                    {
                        tx.Rollback();
                    }
                }

                using (var tx = PrimaryRepository.BeginTransaction())
                {
                    try
                    {
                        profile.Status = UserStatus.Copied;
                        PrimaryRepository.Update(profile);
                        tx.Commit();
                        Logger.InfoFormat("Added profile '{0} to the submission database", profile.Id);
                    }
                    catch (Exception ex)
                    {
                        tx.Rollback();
                    }
                }
                
            }
        }

        private void SetupRepositories()
        {

            var submissionSession = SessionFactory.Instance.CreateSession(ConfigurationManager.AppSettings["SubmissionConfig"]);
            SubmissionRepository = new NhibernateRepository(submissionSession);
            RPLSystem = RPLSystemFactory.CreateSystem();
            PrimaryRepository = RPLSystem.Repository;
        }

        private Timer ConfigureTimer()
        {
            var timer = new Timer();
            timer.Interval = configuration.PollingInterval * 60 * 1000;
            Logger.InfoFormat("Interval set to {0} minutes", configuration.PollingInterval);
            timer.Elapsed += (s, e) => ScheduleTimerElapsed();

            return timer;
        }

        private void ScheduleTimerElapsed()
        {
            scheduleTimer.Stop();
            Logger.Info("Initiate processing");
            Stopwatch timer = Stopwatch.StartNew();
            try
            {
                InitiateProcessing();
                timer.Stop();
                Logger.InfoFormat("Processing took : {0} seconds", timer.Elapsed.TotalSeconds);

                if (!serviceStopped)
                {
                    scheduleTimer.Start();
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("Error: {0}", ex.ToString());
                Logger.ErrorFormat("The polling may have stopped even though the service is running. Restart the service. Exception occured after {0} seconds of processing. ", timer.Elapsed.TotalSeconds, ex.ToString());
                SendErrorMail(ex.ToString());
                RaiseUnhandledError();
                RaiseProcessorStopped();
            }
        }

        private void SendErrorMail(string body)
        {
            try
            {
                Logger.InfoFormat("Sending error alerts : {0}", body);
                var smtpServer = configuration.SmtpServer;
                if (smtpServer.IsNotNullOrEmpty())
                {
                    var client = new SmtpClient(smtpServer);

                    var mailMessage = new MailMessage()
                    {
                        From = new MailAddress(configuration.AdminEmailAddress)
                    };

                    Logger.InfoFormat("Adding {0} to be alerted", configuration.AlertEmailAddresses.AsSeparatedString(",", new Converter<string, string>(cs => cs)));
                    mailMessage.To.AddRange(configuration.AlertEmailAddresses);
                    mailMessage.Subject = "USB: Primary copy to Sumbission Error";
                    mailMessage.Body = "Error occured at {0} \r\n {1}".FormatCurrentCulture(DateTime.Now, body);

                    client.Send(mailMessage);
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("Error sending alert email : {0}", ex);
            }
        }

        #region UnhandledError

        public event EventHandler UnhandledError;

        protected virtual void OnUnhandledError(EventArgs args)
        {

            if (UnhandledError.IsNotNull())
            {
                UnhandledError(this, args);
            }
        }

        private void RaiseUnhandledError()
        {
            OnUnhandledError(EventArgs.Empty);
        }

        #endregion

        #region ProcessorStopped

        public event EventHandler ProcessorStopped;

        protected virtual void OnProcessorStopped(EventArgs args)
        {
            if (ProcessorStopped.IsNotNull())
            {
                ProcessorStopped(this, args);
            }
        }

        private void RaiseProcessorStopped()
        {
            OnProcessorStopped(EventArgs.Empty);
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                scheduleTimer.Dispose();
            }
        }

        #endregion

        #endregion

    }
}
