﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;

namespace USB.RPL.SubmissionService
{
    public partial class SubmissionService : ServiceBase
    {
        private SubmissionSchedular Processor;
        public SubmissionService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            Processor = new SubmissionSchedular();
            Processor.UnhandledError += ProcessorUnhandledError;
            Processor.ProcessorStopped += ProcessorProcessorStopped;
            Processor.Start();
        }

        public void ProcessorProcessorStopped(object sender, EventArgs e)
        {
            if (Processor.IsNotNull())
            {
                Processor.Dispose();
            }
        }

        private void ProcessorUnhandledError(object sender, EventArgs e)
        {
            this.Stop();
        }

        protected override void OnStop()
        {
            Processor.Stop();
        }
    }
}
