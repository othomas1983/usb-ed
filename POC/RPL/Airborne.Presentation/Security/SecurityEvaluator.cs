﻿using Airborne.Domain.Security;

namespace Airborne.Presentation.Security
{
    /// <summary>
    /// Wrapper to evaluate the security rights on a <see cref="SecurityContext"/>
    /// </summary>
    public class SecurityEvaluator : ISecurityEvaluator
    {
        private SecurityContext Context { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="SecurityEvaluator"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public SecurityEvaluator(SecurityContext context)
        {
            Guard.ArgumentNotNull(context, "context");
            Context = context;
        }

        #region ISecurityEvaluator Members

        /// <summary>
        /// Verifes if read access to <paramref name="functionName"/> has been granted in the current security context
        /// </summary>
        /// <param name="functionName"></param>
        /// <returns></returns>
        public bool CanRead(string functionName)
        {
            return Context.HasAccessTo(functionName);
        }

        /// <summary>
        /// Verifes if write access to <paramref name="functionName"/> has been granted in the current security context
        /// </summary>
        /// <param name="functionName"></param>
        /// <returns></returns>
        public bool CanWrite(string functionName)
        {
            return Context.HasWriteAccessTo(functionName);
        }

        #endregion
    }
}
