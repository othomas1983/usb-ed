﻿
namespace Airborne.Presentation.Security
{
    /// <summary>
    /// Contract for security evaluator
    /// </summary>
    public interface ISecurityEvaluator
    {
        /// <summary>
        /// Verifes if read access to <paramref name="functionName" /> has been granted in the current context
        /// </summary>
        bool CanRead(string functionName);

        /// <summary>
        /// Verifes if write access to <paramref name="functionName" /> has been granted in the current context
        /// </summary>
        bool CanWrite(string functionName);
    }
}
