﻿using Airborne.Presentation.Events;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Composite.Presentation.Events;
using Microsoft.Practices.Unity;


namespace Airborne.Presentation
{
    /// <summary>
    /// Base class for the prism modules.
    /// </summary>
    public abstract class ModuleBase : IModule
    {
        /// <summary>
        /// Gets or sets the container.
        /// </summary>
        /// <value>The container.</value>
        protected IUnityContainer Container { get; set; }

        /// <summary>
        /// Gets or sets the event aggregator.
        /// </summary>
        /// <value>The aggregator.</value>
        protected IEventAggregator Aggregator { get; set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="ModuleBase"/> class.
        /// </summary>
        protected ModuleBase(IUnityContainer container, IEventAggregator aggregator)
        {
            Container = container;
            Aggregator = aggregator;

        }
        #region IModule Members

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        public virtual void Initialize()
        {
            RegisterTypes(Container);
            Aggregator.GetEvent<GenericEvent<object>>().Subscribe(OnLoadApplication, ThreadOption.UIThread, true, p => p.Topic == EventTopics.OnStartApplication);

        }

        private void OnLoadApplication(EventParameters<object> parameters)
        {
            StartApplication();
        }

        /// <summary>
        /// Once implemented this method will register any neccesary types 
        /// </summary>
        /// <param name="container">The container.</param>
        protected abstract void RegisterTypes(IUnityContainer container);

        /// <summary>
        /// Is called when the application is started.
        /// </summary>
        protected abstract void StartApplication();

        #endregion
    }
}
