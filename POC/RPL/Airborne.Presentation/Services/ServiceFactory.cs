﻿using System.Security.Principal;

namespace Airborne.Presentation.Services
{
    /// <summary>
    /// Factory to create services and inject custom behavior
    /// </summary>
    public static class ServiceFactory
    {


        /// <summary>
        /// Gets the service.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "identity"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter")]
        public static TServiceType GetService<TServiceType, TServiceContract>(IIdentity identity)
            where TServiceType : System.ServiceModel.ClientBase<TServiceContract>, new()
            where TServiceContract : class
        {

            TServiceType service = new TServiceType();

            return service;
        }

    }
}
