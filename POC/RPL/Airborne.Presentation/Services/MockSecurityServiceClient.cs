﻿using System;
using Airborne.Domain.Security;

namespace Airborne.Presentation.Services
{
    /// <summary>
    /// Mock security client used for testing
    /// </summary>
    public class MockSecurityServiceClient : ISecurityServiceClient
    {

        #region ISecurityServiceClient Members

        /// <summary>
        /// Authenticates the current user.
        /// </summary>
        public void BeginAuthenticate(Action<bool, string> callback)
        {
            callback(true, "security service");
        }

        /// <summary>
        /// Resolves the current user.
        /// </summary>
        public void BeginResolveUser(Action<SecurityContext, string> callback)
        {
            callback(SecurityContext.Empty, "");
        }

        #endregion

        #region ISecurityServiceClient Members
        /// <summary>
        /// Authenticates the current user.
        /// </summary>
        public void Authenticate(string username, string password, Action<SecurityContext> callback, Action<Exception> errorCallBack)
        {
            callback(SecurityContext.Empty);
        }

        #endregion

        #region ISecurityServiceClient Members


        public void KeepSessionAlive(Action<bool> callback, Action<Exception> errorCallBack)
        {
            callback(true);
        }

        #endregion
    }
}
