﻿using System;
using Airborne.Domain.Security;

namespace Airborne.Presentation.Services
{
    /// <summary>
    /// Contract for the security service that will provide security details
    /// </summary>
    public interface ISecurityServiceClient
    {
        /// <summary>
        /// Authenticates the current user.
        /// </summary>
        void Authenticate(string username, string password, Action<SecurityContext> callback, Action<Exception> errorCallBack);

        /// <summary>
        /// Resolves the current user.
        /// </summary>
        void BeginResolveUser(Action<SecurityContext, string> callback);

        /// <summary>
        /// Keeps the current session is stil alive.
        /// </summary>
        void KeepSessionAlive(Action<bool> callback, Action<Exception> errorCallBack);
    }
}
