﻿

namespace Airborne.Presentation
{
    /// <summary>
    /// Event topics that is used in teh infrastructure
    /// </summary>
    public static class EventTopics
    {
        /// <summary>
        /// Event that can be raised when the application is to be initialised fully
        /// </summary>
        public const string OnStartApplication = "OnStartApplication";

        /// <summary>
        /// Event that is to be raised when a busy state has exited.
        /// </summary>
        public const string OnStopBusyState = "OnStopBusyState";


        /// <summary>
        /// Event that is raised when a view is closed
        /// </summary>
        public const string OnViewClosed = "OnViewClosed";

        /// <summary>
        /// Event that is raised when a view is opened
        /// </summary>
        public const string OnViewOpened = "OnViewOpened";

        /// <summary>
        /// Event that is raised when a application error has been raised
        /// </summary>
        public const string OnApplicationError = "OnApplicationError";

        /// <summary>
        /// Event that is raised to authenticate the user
        /// </summary>
        public const string OnAuthenticate = "OnAuthenticate";

        /// <summary>
        /// Event that is raised once user aunthentication has been completed
        /// </summary>
        public const string OnAuthenticationCompleted = "OnAuthenticationCompleted";

        /// <summary>
        /// Event that is raised when user authentication has failed
        /// </summary>
        public const string OnAuthenticationFailed = "OnAuthenticationFailed";

        /// <summary>
        /// Event that is raised when a user has been authenticated
        /// </summary>
        public const string OnSuccessfulAuthentication = "OnSuccessfulAuthentication";


        /// <summary>
        /// Event that is raised to show the Login screen
        /// </summary>
        public const string OnShowLogin = "OnShowLogin";

        /// <summary>
        /// Event that is raised when the user logs out
        /// </summary>
        public const string OnLogout = "OnLogout";

        /// <summary>
        /// Event that is raised when a booking needs to be created in a class
        /// </summary>
        public const string OnBookClass = "OnBookClass";


        /// <summary>
        /// Event that is raised when the advanced search view needs to be shown
        /// </summary>
        public const string OnShowAdvancedClassSearch = "OnShowAdvancedClassSearch";

        /// <summary>
        /// Event that is raised when a view is selected from the Home Page
        /// </summary>
        public const string OnViewSelectedFromHomePage = "OnViewSelectedFromHomePage";

        /// <summary>
        /// Event that is raised when its time to keep the session alive
        /// </summary>
        public const string OnKeepSessionAlive = "OnKeepSessionAlive";
    }
}
