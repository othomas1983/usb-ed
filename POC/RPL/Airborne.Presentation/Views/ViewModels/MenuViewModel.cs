﻿using System;
using System.ComponentModel;
using System.Linq;
using Airborne.Presentation.Events;
using Airborne.Presentation.Workspaces;
using System.Collections.Generic;

namespace Airborne.Presentation.Views.ViewModels
{
    /// <summary>
    /// View model for the menu view
    /// </summary>
    public class MenuViewModel : INotifyPropertyChanged
    {
        private WorkspaceCollection workspaces;
        private Workspace selectedWorkspace;


        /// <summary>
        /// Initializes a new instance of the <see cref="MenuViewModel"/> class.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MenuViewModel()
        {
            Workspaces = new WorkspaceCollection();
        }

        /// <summary>
        /// Gets or sets a collection of workspaces.
        /// </summary>
        /// <value>The workspaces.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public WorkspaceCollection Workspaces
        {
            get
            {
                return workspaces;
            }
            set
            {
                if (value != workspaces)
                {
                    workspaces = value;
                    RaisePropertyChanged("Workspaces");
                    RaisePropertyChanged("AllViews");
                    //RaisePropertyChanged("SelectedView");
                }
            }
        }

        /// <summary>
        /// Gets all the views from all the workspaces.
        /// </summary>
        public IEnumerable<View> AllViews
        {
            get
            {
                return Workspaces.IsNotNull()? Workspaces.ListAllViews() : null;
            }
        }

        /// <summary>
        /// Gets Current Role.
        /// </summary>
        /// <value>The role.</value>
        private string currentRole;
       
        /// <summary>
        /// Property sets and returns the current role
        /// </summary>
        public string CurrentRole
        {
            get
            {
                return currentRole;
            }
            set
            {
                currentRole = value;
                RaisePropertyChanged("CurrentRole");
            }
        }

        /// <summary>
        /// Gets or sets the selected view.
        /// </summary>
        /// <value>The selected view.</value>
        private View selectedView;

        /// <summary>
        /// Gets and Sets SelectedView
        /// </summary>
        public View SelectedView
        {
            get
            {
                return selectedView;
            }
            set
            {
                selectedView = value;
                if (selectedView.IsNotNull())
                {
                    RaisePropertyChanged("SelectedView");
                    RaiseViewChanged(selectedView);
                }
            }
        }

        /// <summary>
        /// Adds a new registration to the collection
        /// </summary>
        public void AddRegistration(WorkspaceRegistration registration)
        {
            Workspaces.AddRegistration(registration);
            RaisePropertyChanged("AllViews");
            SelectHomepage();
        }

        /// <summary>
        /// Selects the home page.
        /// </summary>
        public void SelectHomepage()
        {
            var homepageView = Workspaces.ListAllViews().FirstOrDefault(view => (string)view.Tag == WorkspaceRegistration.HomepageIdentifier);
            if (homepageView.IsNotNull())
            {
                NavigateToView(homepageView, true);
            }
        }

        /// <summary>
        /// Navigates to view.
        /// </summary>
        public void NavigateToView(View view, bool resolvePath)
        {
            if (view.IsNotNull())
            {
                if (resolvePath)
                {
                    SelectedView = view;
                }

                RaiseViewChanged(view);
            }
            else
            {
                RaiseViewCleared();
            }
        }

        /// <summary>
        /// Shows the default or home view.
        /// </summary>
        public void ShowDefault()
        {
            SelectHomepage();
        }

        #region Events

        #region ViewCleared

        /// <summary>
        /// Raised when a view is cleared.
        /// </summary>
        public event EventHandler<GenericEventArgs<object>> ViewCleared;

        private void RaiseViewCleared()
        {
            var args = new GenericEventArgs<object>(null);
            OnViewCleared(args);
        }


        /// <summary>
        /// Raises the <see cref="E:ViewCleared"/> event.
        /// </summary>
        protected virtual void OnViewCleared(GenericEventArgs<object> args)
        {
            var handler = this.ViewCleared;
            if (handler != null)
            {
                handler(this, args);
            }
        }

        #endregion

        #region WorkspaceChanged

        /// <summary>
        /// Occurs when the workspace has changed.
        /// </summary>
        public event EventHandler<GenericEventArgs<Workspace>> WorkspaceChanged;

        private void RaiseWorkspaceChanged(Workspace workspace)
        {
            GenericEventArgs<Workspace> args = new GenericEventArgs<Workspace>(workspace);
            OnWorkspaceChanged(args);
        }

        /// <summary>
        /// Raises the <see cref="E:WorkspaceChanged"/> event.
        /// </summary>
        protected virtual void OnWorkspaceChanged(GenericEventArgs<Workspace> args)
        {
            EventHandler<GenericEventArgs<Workspace>> handler = this.WorkspaceChanged;
            if (handler != null)
            {
                handler(this, args);
            }
        }

        #endregion

        #region SectionChanged

        /// <summary>
        /// Occurs when the section changed.
        /// </summary>
        public event EventHandler<GenericEventArgs<Section>> SectionChanged;

        private void RaiseSectionChanged(Section selectedSection)
        {
            GenericEventArgs<Section> args = new GenericEventArgs<Section>(selectedSection);
            OnSectionChanged(args);
        }

        /// <summary>
        /// Raises the <see cref="E:SectionChanged"/> event.
        /// </summary>
        protected virtual void OnSectionChanged(GenericEventArgs<Section> args)
        {
            EventHandler<GenericEventArgs<Section>> handler = this.SectionChanged;
            if (handler != null)
            {
                handler(this, args);
            }
        }

        #endregion

        #region ViewChanged

        /// <summary>
        /// Occurs when the view changed.
        /// </summary>
        public event EventHandler<GenericEventArgs<View>> ViewChanged;

        private object previousView;
        private void RaiseViewChanged(View view)
        {
            if (previousView != view.Tag)
            {
                previousView = view.Tag;
                OnViewChanged(new GenericEventArgs<View>(view));
            }
        }

        /// <summary>
        /// Raises the <see cref="E:ViewChanged"/> event.
        /// </summary>
        protected virtual void OnViewChanged(GenericEventArgs<View> args)
        {
            EventHandler<GenericEventArgs<View>> handler = this.ViewChanged;
            if (handler != null)
            {
                handler(this, args);
            }
        }

        #endregion
      
        #region INotifyPropertyChanged Members

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string propertyName)
        {
            OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Raises the <see cref="E:PropertyChanged"/> event.
        /// </summary>
        protected virtual void OnPropertyChanged(PropertyChangedEventArgs args)
        {
            if (PropertyChanged.IsNotNull())
            {
                PropertyChanged(this, args);
            }
        }

        #endregion
        #endregion


    }
}
