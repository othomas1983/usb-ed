﻿
namespace Airborne.Presentation.Views
{

    /// <summary>
    /// Contract for the view to show logon errors
    /// </summary>
    public interface ILogOnErrorView
    {
        /// <summary>
        /// Error Description
        /// </summary>
        string ErrorDescription { get; set; }

        /// <summary>
        /// Additional information
        /// </summary>
        string ErrorInformation { get; set; }
    }
}
