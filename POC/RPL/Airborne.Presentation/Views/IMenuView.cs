﻿using Airborne.Presentation.Views.ViewModels;

namespace Airborne.Presentation.Views
{
    /// <summary>
    /// Contract for the interaction of the menu view
    /// </summary>
    public interface IMenuView
    {
        /// <summary>
        /// Gets or sets the model that is used to interact with this view.
        /// </summary>
        /// <value>The model.</value>
        MenuViewModel Model { get; set; }

    }
}
