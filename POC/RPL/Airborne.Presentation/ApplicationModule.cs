﻿using Airborne.Presentation.Controllers;
using Airborne.Presentation.Services;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Unity;

namespace Airborne.Presentation
{
    /// <summary>
    /// Default module to run the infrastructure application from.
    /// </summary>
    public class ApplicationModule : ModuleBase
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationModule"/> class.
        /// </summary>
        /// <param name="container"></param>
        /// <param name="aggregator"></param>
        public ApplicationModule(IUnityContainer container, IEventAggregator aggregator)
            : base(container, aggregator)
        {

        }

        #region IModule Members

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        public override void Initialize()
        {
            base.Initialize();

            Container.Resolve<IApplicationController>().Initialize();
        }

        #endregion

        /// <summary>
        /// Once implemented this method will register any neccesary types
        /// </summary>
        /// <param name="container">The container.</param>
        protected override void RegisterTypes(IUnityContainer container)
        {
            container.RegisterType<IApplicationController, ApplicationController>(new ContainerControlledLifetimeManager());
            //container.RegisterType<ISecurityServiceClient, MockSecurityServiceClient>(new ContainerControlledLifetimeManager());
            
            //container.RegisterType<ILogOnErrorView, LogOnErrorView>(new ContainerControlledLifetimeManager());
            //container.RegisterType<IHomePageView, HomePageView>(new ContainerControlledLifetimeManager());
            //container.RegisterType<ICustomiseBackgroundView, CustomiseBackgroundView>();
            //container.RegisterType<IAnnouncementDataServiceProvider, AnnouncementServiceProvider>(new ContainerControlledLifetimeManager());
            //container.RegisterType<IBackgroundManager, BackgroundManager>(new ContainerControlledLifetimeManager());
        }

        /// <summary>
        /// Is called when the application is started.
        /// </summary>
        protected override void StartApplication()
        {

        }

    }
}
