﻿
namespace Airborne.Presentation.Events
{
    /// <summary>
    /// Class for user credentials username and password
    /// </summary>
    public class UserCredentialArgs
    {
        /// <summary>
        /// Gets or sets the username.
        /// </summary>
        public string Username { get; set; }


        /// <summary>
        /// Gets or sets the password.
        /// </summary>
       public string Password { get; set; }
    }
}
