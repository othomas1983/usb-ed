﻿
namespace Airborne.Presentation.Events
{
    /// <summary>
    /// Structure used to pass generic event parameters.
    /// </summary>
    /// <typeparam name="TValue"></typeparam>
    public class EventParameters<TValue>
    {
        /// <summary>
        /// Gets or sets the topic.
        /// </summary>
        /// <value>The topic.</value>
        public string Topic { get; private set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>The value.</value>
        public TValue Value { get; private set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="EventParameters&lt;TValue&gt;"/> class.
        /// </summary>
        public EventParameters(string topic, TValue value)
        {
            Topic = topic;
            Value = value;
        }
    }
}
