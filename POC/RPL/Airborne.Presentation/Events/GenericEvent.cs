﻿using Microsoft.Practices.Composite.Presentation.Events;

namespace Airborne.Presentation.Events
{
    /// <summary>
    /// Creates a generic composite event to enable loosely typed events to be raised.
    /// </summary>
    /// <typeparam name="TPayload">The type of the payload.</typeparam>
    public class GenericEvent<TPayload> : CompositePresentationEvent<EventParameters<TPayload>>
    {
    }
}
