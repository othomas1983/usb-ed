﻿using System;

namespace Airborne.Presentation.Events
{
    /// <summary>
    /// Generic event used in a situation when a selection needs to be made.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ItemSelectedEventArgs<T> : EventArgs 
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ItemSelectedEventArgs&lt;T&gt;"/> class.
        /// </summary>
        public ItemSelectedEventArgs(T selectedItem)
        {
            SelectedItem = selectedItem;
        }


        /// <summary>
        /// Gets the items that was selected.
        /// </summary>
        /// <value>The selected item.</value>
        public T SelectedItem { get; private set; }
    }
}
