﻿using System;

namespace Airborne.Presentation.Events
{
    /// <summary>
    /// Generic event argument
    /// </summary>
    public class GenericEventArgs<TData> : EventArgs
    {
        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        /// <value>The data.</value>
        public TData Data { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericEventArgs&lt;TData&gt;"/> class.
        /// </summary>
        public GenericEventArgs(TData data)
        {
            Data = data;
        }
    }
}
