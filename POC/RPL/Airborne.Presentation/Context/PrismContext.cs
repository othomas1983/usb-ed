﻿using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Composite.Regions;
using Microsoft.Practices.Unity;

namespace Airborne.Presentation.Context
{
    /// <summary>
    /// Default implementation of <see cref="IPrismContext"/>
    /// </summary>
    public class PrismContext : IPrismContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PrismContext"/> class.
        /// </summary>
        public PrismContext(IEventAggregator eventAggregator,
                                  IRegionManager regionManager,
                                  IUnityContainer unityContainer,
                                  IModuleManager moduleManager)
        {
        
            EventAggregator = eventAggregator;
            RegionManager = regionManager;
            Container = unityContainer;
            ModuleManager = moduleManager;
        }

        #region IPrismContext Members

        /// <summary>
        /// Gets the event aggregator.
        /// </summary>
        /// <value>The event aggregator.</value>
        public IEventAggregator EventAggregator
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the region manager.
        /// </summary>
        /// <value>The region manager.</value>
        public IRegionManager RegionManager
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the container.
        /// </summary>
        /// <value>The container.</value>
        public IUnityContainer Container
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the module manager.
        /// </summary>
        /// <value>The module manager.</value>
        public IModuleManager ModuleManager
        {
            get;
            private set;
        }

        #endregion

     
    }
}