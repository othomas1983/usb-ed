﻿using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Composite.Regions;
using Microsoft.Practices.Unity;

namespace Airborne.Presentation.Context
{
    /// <summary>
    /// Context containing the commonly used prism objects
    /// </summary>
    public interface IPrismContext
    {

        /// <summary>
        /// Gets the event aggregator.
        /// </summary>
        /// <value>The event aggregator.</value>
        IEventAggregator EventAggregator { get; }

        /// <summary>
        /// Gets the region manager.
        /// </summary>
        /// <value>The region manager.</value>
        IRegionManager RegionManager { get; }

        /// <summary>
        /// Gets the container.
        /// </summary>
        /// <value>The container.</value>
        IUnityContainer Container { get; }

        /// <summary>
        /// Gets the module manager.
        /// </summary>
        /// <value>The module manager.</value>
        IModuleManager ModuleManager { get; }
    }
}