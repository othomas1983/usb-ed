﻿using System;
using System.Linq;
using Microsoft.Practices.Composite.Regions;

namespace Airborne.Presentation.Extensions
{
    /// <summary>
    /// Extension methods for the <see cref="IRegionManager"/>
    /// </summary>
    public static class RegionManagerExtensions
    {
        /// <summary>
        /// Shows view in region
        /// </summary>
        public static void ShowViewInRegion(this IRegionManager regionManager, string regionName, object view, bool removeExistingView)
        {
            IRegion targetRegion = regionManager.Regions[regionName];
            if (removeExistingView)
            {
                object currentView = targetRegion.ActiveViews.FirstOrDefault();
                if (!currentView.IsNull() && currentView != view)
                {
                    targetRegion.Remove(currentView);
                }
            }

            if (targetRegion.GetView(view.ToString()).IsNull())
            {
                targetRegion.Add(view, view.ToString());
            }
            targetRegion.Activate(view);
        }

        /// <summary>
        /// Shows the view in region.
        /// </summary>
        public static void ShowViewInRegion(this IRegionManager regionManager, string regionName, object view)
        {
            ShowViewInRegion(regionManager, regionName, view, true);
        }

        /// <summary>
        /// Determines whether the active view in the region is of type <typeparam name="T"/>.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter", Justification = "Considered - deemed not required..")]
        public static bool IsActiveViewOfType<T>(this IRegionManager regionManager, string regionName) where T : class
        {
            if (regionManager.Regions[regionName].IsNull())
            {
                return false;
            }

            var currentView = regionManager.Regions[regionName].GetCurrentView() as T;
            if (currentView.IsNull())
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Gets the current view for the region.
        /// </summary>
        public static object GetCurrentView(this IRegion region)
        {
            object currentView = region.ActiveViews.FirstOrDefault();
            return currentView;
        }
    }
}
