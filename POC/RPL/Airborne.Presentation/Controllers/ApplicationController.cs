﻿using System;
using Airborne.Domain.Security;
using Airborne.Presentation.Context;
using Airborne.Presentation.Controls;
using Airborne.Presentation.Events;
using Airborne.Presentation.Properties;
using Airborne.Presentation.Security;
using Airborne.Presentation.Services;
using Airborne.Presentation.Views;
using Airborne.Presentation.Views.ViewModels;
using Airborne.Presentation.Workspaces;
using Airborne.Presentation.Workspaces.Events;
using Microsoft.Practices.Composite.Presentation.Events;
using Microsoft.Practices.Unity;
using System.Windows;



namespace Airborne.Presentation.Controllers
{
    /// <summary>
    /// Primary controller for manageing the interaction of the application 
    /// within the context of the workspace framework
    /// </summary>
    public class ApplicationController : WorkspaceController, IApplicationController
    {
        #region Properties

        /// <summary>
        /// Gets or sets the menu view.
        /// </summary>
        /// <value>The menu view.</value>
        public IMenuView MenuView { get; set; }

        /// <summary>
        /// Gets or sets the log on error view.
        /// </summary>
        /// <value>The log on error view.</value>
        //[Dependency]
        public ILogOnErrorView LogOnErrorView { get; set; }

        /// <summary>
        /// Gets or sets the home page view.
        /// </summary>
        /// <value>The home page view.</value>
        public IHomepageView HomepageView { get; set; }


        #endregion

        #region Locals

        private ISecurityEvaluator SecurityEvaluator;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ApplicationController"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        public ApplicationController(IPrismContext context)
            : base(context)
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// Initializes this instance of controller.
        /// </summary>
        public override void Initialize()
        {
            SubscribeTo<object>(EventTopics.OnShowLogin, ShowLogin);
            SubscribeTo<UserCredentialArgs>(EventTopics.OnAuthenticate, Authenticate);
            SubscribeTo<SecurityContext>(EventTopics.OnAuthenticationCompleted, AuthenticationCompleted);
            SubscribeTo<object>(EventTopics.OnLogout, OnLogout);
            SubscribeTo<object>(EventTopics.OnKeepSessionAlive, KeepSessionAlive);
        }
        
        /// <summary>
        /// Displays the Login screen
        /// </summary>
        public void ShowLogin(EventParameters<object> args)
        {
            var loginView = Container.Resolve<ILogOnView>();

            if (loginView.IsNotNull())
            {
                this.DisplayView("LoginRegion", loginView);
            }
        }

        private void OnLogout(EventParameters<object> args)
        {
            ClearWorkspaces();
            if (MenuView.IsNotNull())
            {
                MenuView.Model = null;
                MenuView = null;
                GC.GetTotalMemory(false);
            }
        }
        
        /// <summary>
        /// Indicates that the application has started
        /// </summary>
        public void ApplicationStarted()
        {
            MenuView.Model.ShowDefault();
        }

        private void Authenticate(EventParameters<UserCredentialArgs> args)
        {
            var credentials = args.Value;
            var securityService = Resolve<ISecurityServiceClient>();

            securityService.Authenticate(credentials.Username, credentials.Password, (context) => { PublishEvent<SecurityContext>(EventTopics.OnAuthenticationCompleted, context); PublishEvent<object>(EventTopics.OnSuccessfulAuthentication, null); }, (ex) => PublishEvent<Exception>(EventTopics.OnAuthenticationFailed, ex));
        }

        private void AuthenticationCompleted(EventParameters<SecurityContext> args)
        {
            var context = args.Value;

            if (context.IsNotNull())
            {
                LogOnErrorView = null;

                RegisterInstance<SecurityContext>(context);
                SecurityEvaluator = new SecurityEvaluator(context);

                SecurityEvaluatorInstance.Instance = SecurityEvaluator;

                ShowMenu();

                EventAggregator.GetEvent<RegisterWorkspaceEvent>().Subscribe(RegisterWorkspace, ThreadOption.UIThread, true);

                RegisterHomeWorkspace();
                PublishEvent<object>(EventTopics.OnStartApplication, null);
            }
            else
            {
                ShowErrorView(Resources.ErrorAuthenticationFailed, "Login FAILED");
            }

        }
        
        private void ShowErrorView(string error, string information)
        {
            PublishEvent<object>(EventTopics.OnStopBusyState, null);
            LogOnErrorView.ErrorDescription = error;
            LogOnErrorView.ErrorInformation = information;

            DisplayPrimaryView(LogOnErrorView.ToString(), LogOnErrorView);
        }

        private void ShowMenu()
        {
            MenuView = Container.Resolve<IMenuView>();
            //RegisterInstance<IMenuView>(MenuView);

            var currentSecurityContext = Container.Resolve<SecurityContext>();
            var roleName = currentSecurityContext.Detail.IsNotNull() ? currentSecurityContext.Detail.CurrentRole.DisplayName : String.Empty;
            
            var model = new MenuViewModel {Workspaces = new WorkspaceCollection(), CurrentRole = roleName};

            model.SectionChanged += (e, args) => RaiseWorkspaceEvent(new WorkspaceChangedEventArgs() { Change = WorkspaceClassification.Section, Name = args.Data.Name });
            model.ViewChanged += (e, args) => RaiseWorkspaceEvent(new WorkspaceChangedEventArgs() { Change = WorkspaceClassification.View, Registration = args.Data.Registration, Name = args.Data.Name });

            model.WorkspaceChanged += (e, args) =>
            {
                args.Data.IsSelected = true;
                RaiseWorkspaceEvent(new WorkspaceChangedEventArgs() { Change = WorkspaceClassification.Workspace, Name = args.Data.Name });
            };

            MenuView.Model = model;

            DisplayView(Regions.MenuRegion, MenuView);
        }

        private void RegisterHomeWorkspace()
        {
            HomepageView = null;
            RegisterHomepage(() => DisplayHomePage());
        }

        private void DisplayHomePage()
        {
            if (HomepageView.IsNull())
            {
                HomepageView = Container.Resolve<IHomepageView>();
            }

            DisplayPrimaryView("HomePage", HomepageView);
        }
        
        private void UpdateOpenItemsMenuHint(EventParameters<string> payload)
        {
            //var activeViews = GetWorkspaceViews(MenuView).Where(v => IsViewActive(v.Name));
            //MenuView.UpdateOpenItems(activeViews);
        }

        private void RaiseWorkspaceEvent(WorkspaceChangedEventArgs args)
        {
            EventAggregator.GetEvent<WorkspaceChangedEvent>().Publish(args);
        }

        private void RegisterWorkspace(WorkspaceRegistration registration)
        {
            MenuView.Model.AddRegistration(registration);
        }
        
        /// <summary>
        /// Handles the error.
        /// </summary>
        protected override void HandleError(System.Exception exception)
        {
            //
        }

        private void KeepSessionAlive(EventParameters<object> parameters)
        {
            var securityService = Resolve<ISecurityServiceClient>();
            securityService.KeepSessionAlive((result) => { this.CheckResult(result); }, (ex) => this.OnKeepSessionAliveError(ex));
        }

        public void CheckResult(bool IsAlive)
        {
            if (!IsAlive)
            {
                PublishLogoutEvent();
            }
        }

        public void OnKeepSessionAliveError(Exception ex)
        {
            PublishLogoutEvent();
        }

        private void PublishLogoutEvent()
        {
            PublishEvent<object>(EventTopics.OnLogout, "Your session has expired. Please refresh your page.");
        }
        #endregion
    }
}
