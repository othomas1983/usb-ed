﻿
namespace Airborne.Presentation.Controllers
{
    /// <summary>
    /// Contract for the application controller
    /// </summary>
    public interface IApplicationController : IController
    {
        /// <summary>
        /// Indicates that the application has started
        /// </summary>
        void ApplicationStarted();
    }
}
