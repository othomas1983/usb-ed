﻿
namespace Airborne.Presentation.Controllers
{
    /// <summary>
    /// Contract for controller
    /// </summary>
    public interface IController
    {
        /// <summary>
        /// Initializes this instance of controller.
        /// </summary>
        void Initialize();
    }
}
