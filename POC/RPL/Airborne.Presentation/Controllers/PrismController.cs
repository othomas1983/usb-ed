﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Airborne.Presentation.Context;
using Airborne.Presentation.Controls;
using Airborne.Presentation.Events;
using Airborne.Presentation.Workspaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Presentation.Events;
using Microsoft.Practices.Unity;

namespace Airborne.Presentation.Controllers
{
    /// <summary>
    /// Controller that caters for prism infrastructure
    /// </summary>
    public abstract class PrismController : IController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PrismController"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        protected PrismController(IPrismContext context)
        {
            Context = context;
        }

        /// <summary>
        /// Gets or sets the current context.
        /// </summary>
        /// <value>The context.</value>
        protected IPrismContext Context { get; set; }

        /// <summary>
        /// Gets the container.
        /// </summary>
        /// <value>The container.</value>
        protected IUnityContainer Container
        {
            get
            {
                return Context.Container;
            }
        }

        /// <summary>
        /// Gets the event aggregator.
        /// </summary>
        /// <value>The event aggregator.</value>
        protected IEventAggregator EventAggregator
        {
            get
            {
                return Context.EventAggregator;
            }
        }

        /// <summary>
        /// Publish an event.
        /// </summary>
        public void PublishEvent<T>(string topic, T payload)
        {
            Context.EventAggregator.GetEvent<GenericEvent<T>>().Publish(new EventParameters<T>(topic, payload));
        }

        /// <summary>
        /// Subscribes to an event.
        /// </summary>
        public SubscriptionToken Subscribe<T>(Action<EventParameters<T>> action, ThreadOption threadOption, string topic)
        {
            return Context.EventAggregator.GetEvent<GenericEvent<T>>().Subscribe(action, threadOption, true, p => p.Topic == topic);
        }

        /// <summary>
        /// Subscribes to an event topic specifically on th Ui thread.
        /// </summary>
        public SubscriptionToken SubscribeTo<T>(string topic, Action<EventParameters<T>> action)
        {
            return Context.EventAggregator.GetEvent<GenericEvent<T>>().Subscribe(action, ThreadOption.UIThread, true, p => p.Topic == topic);
        }



        /// <summary>
        /// Resolve instance
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter")]
        public T Resolve<T>()
        {
            return Context.Container.Resolve<T>();
        }

        /// <summary>
        /// Register Instance
        /// </summary>
        public void RegisterInstance<T>(T instance)
        {
            Container.RegisterInstance<T>(instance);
        }

        /// <summary>
        /// Displays the specified view in the primary region
        /// </summary>
        protected void DisplayPrimaryView(string viewName, object view)
        {
            ShowView(Regions.MainRegion, viewName, view);
        }

        /// <summary>
        /// Displays the specified view in the specified region
        /// </summary>
        protected void DisplayView(string region, object view)
        {
            ShowView(region, view.ToString(), view);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        private void ShowView(string regionName, string viewName, object view)
        {
            Action action = delegate
            {
                var region = Context.RegionManager.Regions[regionName];

                var existingView = region.GetView(viewName);
                if (existingView.IsNotNull())
                {
                    region.Remove(existingView);
                }

                if (!region.Views.Contains(view))
                {
                    region.Add(view, viewName);
                }
                foreach (var item in region.Views)
                {
                    var controlItem = item as Control;
                    if (item != view && controlItem.IsNotNull())
                    {
                        //((Control)item).FadeOut(0.3); 
                        controlItem.Visibility = Visibility.Collapsed; // fade out
                    }
                }
                var control = view as Control;
                if (control != null)
                {
                    control.Opacity = 0;
                    control.Visibility = Visibility.Visible;
                    control.FadeIn(0.8);

                }
                region.Activate(view);
                PublishEvent<string>("ViewOpened", null);
            };
            //the reason for doing this is that in the test cases it references a agcore
            //assembly that causes the test to fail (hence not as simple as doing a null check)
            //under normal operating conditions the dispatcher should run fine - so no exception problems
            try
            {

#if !SILVERLIGHT
                Application.Current.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, action);
#else
                Deployment.Current.Dispatcher.BeginInvoke(action);
#endif
            }
            catch (Exception)
            {
                action();
            }
        }

        /// <summary>
        /// Removes the specified view from the primary region.  
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal void CloseView(string viewName)
        {
            var region = Context.RegionManager.Regions[Regions.MainRegion];
            var view = region.GetView(viewName);

            CloseView(view);
        }

        /// <summary>
        /// Removes the specified view from the primary region.  
        /// </summary>
        public void CloseView(object view)
        {
            if (view.IsNotNull())
            {
                var region = Context.RegionManager.Regions[Regions.MainRegion];
                region.Remove(view);

                PublishEvent<string>(EventTopics.OnViewClosed, null);
            }
        }

        /// <summary>
        /// Removes all views from the primary region.  
        /// </summary>
        public void CloseAllViews()
        {
            var region = Context.RegionManager.Regions[Regions.MainRegion];

            var viewsToRemove = region.Views.ToList();

            foreach (var view in viewsToRemove)
            { 
                region.Remove(view);
            }

            PublishEvent<string>(EventTopics.OnViewClosed, null);
        }




        /// <summary>
        /// Returns Active views for the primary region.  
        /// </summary>
        public IEnumerable<object> ActiveViews
        {
            get
            {
                var region = Context.RegionManager.Regions[Regions.MainRegion];
                return region.Views.ToList();
            }
        }

        /// <summary>
        /// Returns true if the specified view is the curretly active view.  
        /// </summary>
        public bool IsViewActive(string viewName)
        {
            var region = Context.RegionManager.Regions[Regions.MainRegion];
            return region.GetView(viewName).IsNotNull();
        }

        /// <summary>
        /// Executes the specified action on the instance of <typeparamref name="T"/>.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="call">The call.</param>
        public void Execute<T>(Action<T> call) where T : class
        {
            var service = Container.Resolve<T>();
            call.Invoke(service);
        }

        #region IController Members

        /// <summary>
        /// Initializes this instance of controller.
        /// </summary>
        public abstract void Initialize();

        #endregion
    }
}
