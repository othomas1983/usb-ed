﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Resources;
using Airborne.Presentation.Context;
using Airborne.Presentation.Workspaces.Events;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Presentation.Events;
using Microsoft.Practices.Unity;
using Airborne.Presentation.Events;
using Airborne.Presentation.Properties;


namespace Airborne.Presentation.Workspaces
{
    /// <summary>
    /// Manager for workspaces
    /// </summary>
    public class WorkspaceManager : IWorkspaceManager, IDisposable
    {
        private IDictionary<string, Action> WorkspaceCallbacks { get; set; }
        private IPrismContext Context { get; set; }
        private RegisterWorkspaceEvent RegisterEvent { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Need to keep track of it so that it does not die before required, but we can explicitly kill the reference on dispose")]
        private SubscriptionToken WorkspaceChangeSubscription { get; set; }
        private ResourceManager ResourceManager { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="WorkspaceManager"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="resourceManager">Resource manager</param>
        public WorkspaceManager(IPrismContext context, ResourceManager resourceManager)
        {
            ResourceManager = resourceManager;
            Context = context;
            WorkspaceCallbacks = new Dictionary<string, Action>();
            WorkspaceChangeSubscription = Context.EventAggregator.GetEvent<Events.WorkspaceChangedEvent>().Subscribe(WorkspaceChangeEvent, ThreadOption.UIThread);
            RegisterEvent = Context.EventAggregator.GetEvent<RegisterWorkspaceEvent>();
        }

        private string ResolveViewName(string function)
        {
            if (ResourceManager.IsNotNull())
            {
                try
                {
                    var viewName = Resources.ResourceManager.GetString(function, CultureInfo.InvariantCulture);
                    viewName = viewName.IsNotNullOrEmpty() ? function.CamelCaseToWords().Replace('_', ' ') : viewName;
                    return viewName;
                }
                catch (ArgumentNullException) { }
                catch (InvalidOperationException) { }
                catch (MissingManifestResourceException) { }
            }
            return function;
        }

        /// <summary>
        /// Called when workspaces change.
        /// This should not be used directly.
        /// </summary>
        /// <param name="args">The <see cref="Airborne.Presentation.Workspaces.Events.WorkspaceChangedEventArgs"/> instance containing the event data.</param>
        public void WorkspaceChangeEvent(WorkspaceChangedEventArgs args)
        {
            if (args.Change == WorkspaceClassification.View)
            {
                string tag = args.Registration.Identifier.ToString();
                if (WorkspaceCallbacks.ContainsKey(tag))
                {
                    WorkspaceCallbacks[tag]();
                }
            }
        }


        /// <summary>
        /// Registers a new workspace with a callback for when the view is selected.
        /// </summary>
        public void Register(WorkspaceRegistration registration, Action callback)
        {
            Guard.ArgumentNotNull(registration, "registration");
            Guard.ArgumentNotNull(callback, "callBack");

            if (registration.Identifier.IsNull())
            {
                registration.Identifier = Guid.NewGuid().ToString();
            }

            if (CanRegister(registration))
            {
                registration.ViewDisplayName = ResolveViewName(registration.Function);

                if (registration.ViewName.IsNullOrEmpty())
                {
                    registration.ViewName = registration.ViewDisplayName; //ResolveViewName(registration.Function);
                }

                WorkspaceCallbacks.Add(registration.Identifier.ToString(), callback);
                RegisterEvent.Publish(registration);
            }
        }

        /// <summary>
        /// Evaluates the workspace sinks to see if teh registration can be made.
        /// </summary>
        private bool CanRegister(WorkspaceRegistration registration)
        {
            var workspaceRegistrationSinks = Context.Container.ResolveAll(typeof(IWorkspaceRegistrationSink)).OfType<IWorkspaceRegistrationSink>();
            foreach (var item in workspaceRegistrationSinks)
            {
                if (!item.CanRegister(registration))
                {
                    return false;
                }
            }
            return true;
        }

        public void Clear()
        {
            WorkspaceCallbacks.Clear();
        }

      

        #region IDisposable Members

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                WorkspaceChangeSubscription = null;
            }
        }

        #endregion

    }
}
