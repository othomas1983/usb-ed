﻿
namespace Airborne.Presentation.Workspaces
{
    /// <summary>
    /// Contract to control the createion of items in the workspace.
    /// </summary>
    public interface IWorkspaceRegistrationSink
    {
        /// <summary>
        /// Determines whether this instance can register the specified workspace.
        /// </summary>
        /// <param name="workspace">The workspace.</param>
        /// <returns>
        /// 	<c>true</c> if this instance can register the specified workspace; otherwise, <c>false</c>.
        /// </returns>
        bool CanRegister(WorkspaceRegistration workspace);
    }
}
