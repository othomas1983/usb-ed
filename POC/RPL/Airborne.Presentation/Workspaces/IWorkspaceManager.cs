﻿using System;

namespace Airborne.Presentation.Workspaces
{
    /// <summary>
    /// Contract for the workspace manager.
    /// </summary>
    public interface IWorkspaceManager
    {
        /// <summary>
        /// Registers a new workspace with a callback for when the view is selected.
        /// </summary>
        void Register(WorkspaceRegistration registration, Action callback);

        void Clear();
    }
}
