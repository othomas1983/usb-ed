﻿
namespace Airborne.Presentation.Workspaces
{
    /// <summary>
    /// Used to identify the change that has occured when the <see cref="Airborne.Presentation.Workspaces.Events.WorkspaceChangedEvent"/> is raised
    /// </summary>
    public enum WorkspaceClassification
    {
        /// <summary>
        /// Workspace 
        /// </summary>
        Workspace,

        /// <summary>
        /// Section
        /// </summary>
        Section,

        /// <summary>
        /// View
        /// </summary>
        View
    }
}
