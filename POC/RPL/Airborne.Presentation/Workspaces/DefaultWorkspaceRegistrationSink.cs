﻿using Airborne.Domain.Security;
using System.Linq;
using Microsoft.Practices.Unity;
using System;

namespace Airborne.Presentation.Workspaces
{
    /// <summary>
    /// Default registration sink
    /// </summary>
    public class DefaultWorkspaceRegistrationSink : IWorkspaceRegistrationSink
    {
        #region Locals

        IUnityContainer container;

        string[] alwaysAvailible = { "HOMEPAGE" };

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultWorkspaceRegistrationSink"/> class with the specified security context.
        /// </summary>
        public DefaultWorkspaceRegistrationSink(SecurityContext context)
        {
            Guard.ArgumentNotNull(context, "context");
            SecurityContext = context;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultWorkspaceRegistrationSink"/> class with a IUnityContainer container.
        /// </summary>
        [InjectionConstructor]
        public DefaultWorkspaceRegistrationSink(IUnityContainer container)
        {
             this.container = container;
        }
        
        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultWorkspaceRegistrationSink"/> class.
        /// </summary>
        public DefaultWorkspaceRegistrationSink()
        {

        }

        #endregion

        SecurityContext context;
        private SecurityContext SecurityContext
        {
            get
            {
                return container.IsNotNull() ? container.Resolve<SecurityContext>() : context;
            }

            set { context = value; }
        }

        #region Methods

        private bool IsAlwaysAvailible(string functionName)
        {
            return  alwaysAvailible.Contains(functionName.ToUpper());
        }

        #endregion

        #region IWorkspaceRegistrationSink Members

        /// <summary>
        /// Determines whether this instance can register the specified workspace.
        /// </summary>
        public bool CanRegister(WorkspaceRegistration workspace)
        {
            var functionName = workspace.Function;
            return IsAlwaysAvailible(functionName) || SecurityContext.HasAccessTo(functionName);
        }

        #endregion
    }
}
