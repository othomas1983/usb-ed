﻿using Microsoft.Practices.Composite.Presentation.Events;

namespace Airborne.Presentation.Workspaces.Events
{
    /// <summary>
    /// Event that is raised when a workspace changes
    /// </summary>
    public class WorkspaceChangedEvent 
        : CompositePresentationEvent<WorkspaceChangedEventArgs>
    {
    }
}
