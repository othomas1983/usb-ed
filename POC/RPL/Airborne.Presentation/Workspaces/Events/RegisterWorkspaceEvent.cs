﻿using Microsoft.Practices.Composite.Presentation.Events;

namespace Airborne.Presentation.Workspaces.Events
{
    /// <summary>
    /// Event that is used for the registration of workspaces
    /// </summary>
    public class RegisterWorkspaceEvent 
        : CompositePresentationEvent<WorkspaceRegistration>
    {

    }
}
