﻿
namespace Airborne.Presentation.Workspaces.Events
{
    /// <summary>
    /// Event args that is used when a workspace is changed
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1711:IdentifiersShouldNotHaveIncorrectSuffix", Justification="This represents the event args as understood by prism")]
    public class WorkspaceChangedEventArgs
    {
        /// <summary>
        /// If availbale the registration details of the view.
        /// </summary>
        public WorkspaceRegistration Registration { get; set; }

        /// <summary>
        /// The name of the object defined by the change classification <see cref="WorkspaceClassification"/>
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Defines the level of change.
        /// </summary>
        public WorkspaceClassification Change { get; set; }
    }
}
