﻿using System;

namespace Airborne.Presentation.Workspaces
{
    /// <summary>
    /// Class that contains all the information to create a <see cref="Workspace"/> heirarchy
    /// </summary>
    public class WorkspaceRegistration
    {
        /// <summary>
        /// Identifier for the known homme page view
        /// </summary>
        public const string HomepageIdentifier = "homepage";

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="WorkspaceRegistration"/> class.
        /// </summary>
        private WorkspaceRegistration()
        {

        }
        /// <summary>
        /// Initializes a new instance of the <see cref="WorkspaceRegistration"/> class.
        /// </summary>
        public WorkspaceRegistration(string workspace, string section, string function)
        {
            Workspace = workspace;
            Section = section;
            Function = function;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="WorkspaceRegistration"/> class.
        /// </summary>
        public WorkspaceRegistration(string workspace, string section, string function, object identifier)
            : this(workspace, section, function)
        {
            Identifier = identifier;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets if this registration is the home page.
        /// </summary>
        /// <value>The is home page.</value>
        public bool? SetAsHomepage { get; set; }

        /// <summary>
        /// Gets or sets the workspacename that this entry will be categorised under.
        /// </summary>
        /// <value>The workspace.</value>
        public string Workspace { get; set; }


        /// <summary>
        /// Gets or sets the section that this entry will be categorised under.
        /// </summary>
        /// <value>The section.</value>
        public string Section { get; set; }

        /// <summary>
        /// Gets or sets the section's order.
        /// </summary>
        /// <value>The section order.</value>
        public int? SectionOrder { get; set; }


        /// <summary>
        /// Gets or sets the view.
        /// </summary>
        /// <value>The view.</value>
        public string ViewName { get; set; }

        /// <summary>
        /// Gets or sets the view's display name.
        /// </summary>
        /// <value>The view's Display name.</value>
        public string ViewDisplayName { get; set; }

        /// <summary>
        /// Gets or sets the function.
        /// </summary>
        /// <value>The function.</value>
        public string Function { get; set; }

        /// <summary>
        /// Gets or sets if this registration defines the default view.
        /// </summary>
        /// <value>The is default view.</value>
        public bool? IsDefaultView { get; set; }

        /// <summary>
        /// Gets or sets if this registration defines the startup section.
        /// </summary>
        /// <value>The is default view.</value>
        public bool IsStartupSection { get; set; }

        /// <summary>
        /// Gets or sets the view's order.
        /// </summary>
        /// <value>The view order.</value>
        public int? ViewOrder { get; set; }

        /// <summary>
        /// Gets or sets a custom identifier.
        /// </summary>
        /// <value>The identifier.</value>
        public object Identifier { get; set; }
        #endregion

        #region Override Methods
        /// <summary>
        /// Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        /// A hash code for the current <see cref="T:System.Object"/>.
        /// </returns>
        public override int GetHashCode()
        {
            return (Workspace ?? "").GetHashCode() & (Section ?? "").GetHashCode() & (Function ?? ViewName).GetHashCode();
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object"/> is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
        /// <returns>
        /// 	<c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        /// <exception cref="T:System.NullReferenceException">The <paramref name="obj"/> parameter is null.</exception>
        public override bool Equals(object obj)
        {
            WorkspaceRegistration registration = obj as WorkspaceRegistration;
            if (obj.IsNotNull())
            {
                return registration.GetHashCode() == this.GetHashCode();
            }

            return base.Equals(obj);
        }
        #endregion

        #region Fluent Methods
        /// <summary>
        /// Sets this view as being the default in the section
        /// </summary>
        /// <returns></returns>
        public WorkspaceRegistration AsDefault()
        {
            this.IsDefaultView = true;
            return this;
        }

        /// <summary>
        /// Sets the relevant section as being the default in the workspace
        /// </summary>
        /// <returns></returns>
        public WorkspaceRegistration AsStartup()
        {
            this.IsStartupSection = true;
            return this;
        }

        /// <summary>
        /// Sets the view's order.
        /// Take note that if you set this to 1 and all other views are not ordered - it will still be in the last location.
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public WorkspaceRegistration WithViewOrderOf(int order)
        {
            this.ViewOrder = order;
            return this;
        }

        /// <summary>
        /// Setst the sections order.
        /// Take note that if you set this to 1 and all other sectiosn are not ordered - it will still be in the last location.
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public WorkspaceRegistration WithSectionOrderOf(int order)
        {
            this.SectionOrder = order;
            return this;
        }

        /// <summary>
        /// Determines whether this instance is to be used for the homepage.
        /// </summary>
        /// <returns></returns>
        public WorkspaceRegistration IsHomepage()
        {
            this.SetAsHomepage = true;
            return this;

        }
        #endregion

        #region Static Methods
        /// <summary>
        /// Instantiates a default workspace registration
        /// </summary>
        public static WorkspaceRegistration Default(string workspace, string section, string function)
        {
            return new WorkspaceRegistration(workspace, section, function);
        }

        /// <summary>
        /// Instantiates a workspace registration for the home page
        /// </summary>
        public static WorkspaceRegistration Homepage()
        {
            var homepage = new WorkspaceRegistration("Home", "Welcome", "Homepage", HomepageIdentifier)
                .IsHomepage()
                .AsStartup();
            return homepage;

        }
        #endregion
    }
}
