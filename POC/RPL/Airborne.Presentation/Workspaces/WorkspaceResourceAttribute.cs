﻿using System;

namespace Airborne.Presentation.Workspaces
{
    /// <summary>
    /// Attribute to define the location of the resources used to 
    /// indicate the resource dictionary for the assemblies workspaces
    /// </summary>
    [AttributeUsage(AttributeTargets.Assembly, AllowMultiple = false)]
    public sealed class WorkspaceResourceAttribute : Attribute
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WorkspaceResourceAttribute"/> class.
        /// </summary>
        /// <param name="resourceType">Type of the resouce.</param>
        public WorkspaceResourceAttribute(Type resourceType)
        {
            ResourceType = resourceType;
        }


        /// <summary>
        /// Gets or sets the resource dictionary.
        /// </summary>
        public Type ResourceType
        {
            get;
            private set;
        }
    }
}
