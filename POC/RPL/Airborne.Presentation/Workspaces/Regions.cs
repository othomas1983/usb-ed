﻿
namespace Airborne.Presentation.Workspaces
{
    /// <summary>
    /// Regions that are of significance to the workspaces
    /// </summary>
    public static class Regions
    {

        /// <summary>
        /// Main region used for displaying views
        /// </summary>
        public const string MainRegion = "MainRegion";

        /// <summary>
        /// Region used to display the menu
        /// </summary>
        public const string MenuRegion = "MenuRegion";
    }
}
