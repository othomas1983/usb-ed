﻿using System;
using System.Linq;
using System.Reflection;
using System.Resources;
using Airborne.Presentation.Context;
using Airborne.Presentation.Controllers;
using Airborne.Presentation.Events;

namespace Airborne.Presentation.Workspaces
{
    /// <summary>
    /// Controller class that exposes services related to the workspace infrastructure
    /// </summary>
    public abstract class WorkspaceController : PrismController
    {
        /// <summary>
        /// Gets or sets the workspace manager.
        /// </summary>
        protected IWorkspaceManager Workspace { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="WorkspaceController"/> class.
        /// </summary>
        /// <param name="context">The context.</param>
        protected WorkspaceController(IPrismContext context)
            : base(context)
        {
            Workspace = new WorkspaceManager(context, ResolveResourceManager(Assembly.GetCallingAssembly())); //TODO : resolve
        }

        /// <summary>
        /// Registers a new view with the workspace
        /// </summary>
        protected WorkspaceRegistration RegisterView(string workspace, string section, string function, Action callback)
        {
            return RegisterView(workspace, section, function, null, callback);
        }

        /// <summary>
        /// Registers a new view with the workspace
        /// </summary>
        protected WorkspaceRegistration RegisterView(string workspace, string section, string function, string viewName, Action callback)
        {
            WorkspaceRegistration registration = WorkspaceRegistration.Default(workspace, section, function);
            registration.ViewName = viewName;

            Workspace.Register(registration, callback);

            return registration;
        }

        /// <summary>
        /// Registers a new view with the workspace
        /// </summary>
        protected WorkspaceRegistration RegisterHomepage(Action callback)
        {
            WorkspaceRegistration registration = WorkspaceRegistration.Homepage();

            Workspace.Register(registration, callback);

            return registration;
        }

        /// <summary>
        /// Resolves the resource manager from the attribute on the calling assembly.
        /// </summary>
        /// <returns></returns>
        private static ResourceManager ResolveResourceManager(Assembly assembly)
        {

            var workspaceResouce = assembly.GetCustomAttributes(typeof(WorkspaceResourceAttribute), false)
                                            .OfType<WorkspaceResourceAttribute>()
                                            .FirstOrDefault();
            if (workspaceResouce.IsNotNull())
            {
                return new ResourceManager(workspaceResouce.ResourceType);
            }

            return null;
        }

        /// <summary>
        /// Clears all workspaces
        /// </summary>
        protected void ClearWorkspaces()
        {
            Workspace.Clear();
        }


        /// <summary>
        /// Notifies to hande an error that has occured
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void ErrorSubscriber(EventParameters<Exception> data)
        {
            HandleError(data.Value);
        }

        /// <summary>
        /// Once implemented will contain logic to handle and or display errors.
        /// </summary>
        protected abstract void HandleError(Exception exception);
    }
}
