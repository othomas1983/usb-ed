﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;

namespace Airborne.Presentation.Workspaces
{
    /// <summary>
    /// The section forms a container for <see cref="View"/>'s and is a part of a <see cref="Workspace"/>
    /// </summary>
    public class Section : INotifyPropertyChanged
    {
        private string name;
        private bool isSelected;

        /// <summary>
        /// Initializes a new instance of the <see cref="Section"/> class.
        /// </summary>
        public Section()
        {
            Views = new ObservableCollection<View>(); 
        }

        /// <summary>
        /// Gets or sets the parent of the section.
        /// </summary>
        /// <value>The parent.</value>
        public Workspace Parent
        {
            get;
            set;
        }

        
        /// <summary>
        /// Gets or sets the name of the section.
        /// </summary>
        /// <value>The name.</value>
        public string Name {
            get
            {
                return name;
            }
            set
            {
                if (name != value)
                {
                    name = value;
                    RaisePropertyChanged("Name");
                }
            }
        }

        
        /// <summary>
        /// Gets or sets a value indicating whether this section is selected.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is selected; otherwise, <c>false</c>.
        /// </value>
        public bool IsSelected
        {
            get
            {
                return isSelected;
            }
            set
            {
                if (value != isSelected)
                {
                    isSelected = value;
                    RaisePropertyChanged("IsSelected");
                }
            }
        }

        /// <summary>
        /// Gets or sets an indication if this is the startup section.
        /// </summary>
        /// <value>The order.</value>
        public bool IsStartup { get; set; }

        /// <summary>
        /// Gets or sets an optional order that the section should be displayed in.
        /// </summary>
        /// <value>The order.</value>
        public int? Order { get; set; }

        /// <summary>
        /// Gets or sets the description of the section.
        /// </summary>
        /// <value>The description.</value>
        public string Description { get; set; }

        /// <summary>
        /// List of views. 
        /// To add use the AddView method!
        /// </summary>
        public ObservableCollection<View> Views
        {
            get;
            private set;
        }

        /// <summary>
        /// Adds a view into the section.
        /// </summary>
        /// <param name="view">The view.</param>
        public void AddView(View view)
        {
            Guard.ArgumentNotNull(view, "view");
            if (!Views.Contains(view))
            {
                view.Parent = this;
                Views.Add(view);
            }
        }

        #region PropertyChanged

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventArgs args = new PropertyChangedEventArgs(propertyName);
            OnPropertyChanged(args);
        }

        /// <summary>
        /// Raises the <see cref="E:PropertyChanged"/> event.
        /// </summary>
        /// <param name="args">The <see cref="System.ComponentModel.PropertyChangedEventArgs"/> instance containing the event data.</param>
        protected virtual void OnPropertyChanged(PropertyChangedEventArgs args)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, args);
            }
        }

        #endregion

        /// <summary>
        /// Clears the selection.
        /// </summary>
        public void ClearSelection()
        {
            this.IsSelected = false;
            Views.ForEach(delegate(View view)
            {
                view.IsSelected = false;
            });

        }

        /// <summary>
        /// Selects a view.
        /// </summary>
        /// <param name="view">The view.</param>
        /// <returns></returns>
        public bool SelectView(View view)
        {
            ClearSelection();

            View selectedView = Views.FirstOrDefault(v => v.Tag == view.Tag);
            if (selectedView.IsNotNull())
            {
                this.IsSelected = true;
                selectedView.IsSelected = true;
            }

            return this.IsSelected;
        }

        /// <summary>
        /// Sets this instance as the selected section.
        /// </summary>
        public void SetSelected()
        {
            Parent.SetSelectedSection(this);
        }
    }
}
