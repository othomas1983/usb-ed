﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace Airborne.Presentation.Workspaces
{
    /// <summary>
    /// This forms the highest level container for <see cref="Section"/>'s and <see cref="View"/>s
    /// </summary>
    public class Workspace : INotifyPropertyChanged
    {
        private string name;
        private bool isSelected;

        /// <summary>
        /// Initializes a new instance of the <see cref="Workspace"/> class.
        /// </summary>
        public Workspace()
        {
            Sections = new ObservableCollection<Section>();
        }


        /// <summary>
        /// Gets or sets the name of the workspace.
        /// </summary>
        /// <value>The name.</value>
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                if (name != value)
                {
                    name = value;
                    RaisePropertyChanged("Name");
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="Workspace"/> is selected.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this <see cref="Workspace"/> is selected; otherwise, <c>false</c>.
        /// </value>
        public bool IsSelected
        {
            get
            {
                return isSelected;
            }
            set
            {
                if (value != isSelected)
                {
                    isSelected = value;
                    RaisePropertyChanged("IsSelected");
                }
            }
        }


        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>The description.</value>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the <see cref="Section"/>s that is contained in the <see cref="Workspace"/>.
        /// </summary>
        /// <value>The sections.</value>
        public ObservableCollection<Section> Sections
        {
            get;
           private set;
        }

        /// <summary>
        /// Adds a <see cref="Section"/> to the <see cref="Workspace"/>.
        /// </summary>
        /// <param name="section">The section.</param>
        public void AddSection(Section section)
        {
            Guard.ArgumentNotNull(section, "section");
            if (!Sections.Contains(section))
            {
                section.Parent = this;
                Sections.Add(section);
            }

        }

        /// <summary>
        /// Clears the selected items.
        /// </summary>
        public void ClearSelection()
        {
            IsSelected = false;
            Sections.ForEach(delegate(Section section)
            {
                section.ClearSelection();
            });
        }

        /// <summary>
        /// Selects a view.
        /// </summary>
        /// <param name="view">The view.</param>
        /// <returns></returns>
        public bool SelectView(View view)
        {
            ClearSelection();

            foreach (var item in Sections)
            {
                if (item.SelectView(view))
                {
                    IsSelected = true;
                    break;
                }
            }
            return IsSelected;
        }

        #region PropertyChanged

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventArgs args = new PropertyChangedEventArgs(propertyName);
            OnPropertyChanged(args);
        }

        /// <summary>
        /// Raises the <see cref="E:PropertyChanged"/> event.
        /// </summary>
        /// <param name="args">The <see cref="System.ComponentModel.PropertyChangedEventArgs"/> instance containing the event data.</param>
        protected virtual void OnPropertyChanged(PropertyChangedEventArgs args)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, args);
            }
        }

        #endregion

        /// <summary>
        /// Selects a new section
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal void SetSelectedSection(Section section)
        {
            Sections.ForEach(s => s.IsSelected = (s == section));
        }
    }
}