﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Airborne.Presentation.Workspaces
{
    /// <summary>
    /// This fors a reference to an actual view.
    /// </summary>
    public class View : INotifyPropertyChanged
    {
        private string name;
        private bool isSelected;
        private string displayName;

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                if (name != value)
                {
                    name = value;
                    RaisePropertyChanged("Name");
                }
            }
        }

        /// <summary>
        /// Gets or sets the friendly name.
        /// </summary>
        /// <value>The name.</value>
        public string DisplayName
        {
            get
            {
                return displayName.IsNotNullOrEmpty() ? displayName.Replace('_',' ') : displayName;
            }
            set
            {
                if (displayName != value)
                {
                    displayName = value;
                    RaisePropertyChanged("Greeting");
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this view is selected.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this view is selected; otherwise, <c>false</c>.
        /// </value>
        public bool IsSelected
        {
            get
            {
                return isSelected;
            }
            set
            {
                if (value != isSelected)
                {
                    isSelected = value;
                    RaisePropertyChanged("IsSelected");
                }
            }
        }

        /// <summary>
        /// Gets or sets the optional order that the view should be displayed in.
        /// </summary>
        /// <value>The order.</value>
        public int? Order { get; set; }

        /// <summary>
        /// Gets or sets the parent <see cref="Section"/> of the view.
        /// </summary>
        /// <value>The parent.</value>
        public Section Parent
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is default.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is default; otherwise, <c>false</c>.
        /// </value>
        public bool IsDefault
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets an option identifier for the view.
        /// </summary>
        /// <value>The tag.</value>
        public object Tag { get; set; }

        /// <summary>
        /// Gets or sets the registration that the view hierarchy is created from.
        /// </summary>
        /// <value>The registration.</value>
        public WorkspaceRegistration Registration { get; set; }

        #region PropertyChanged

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventArgs args = new PropertyChangedEventArgs(propertyName);
            OnPropertyChanged(args);
        }

        /// <summary>
        /// Raises the <see cref="E:PropertyChanged"/> event.
        /// </summary>
        /// <param name="args">The <see cref="System.ComponentModel.PropertyChangedEventArgs"/> instance containing the event data.</param>
        protected virtual void OnPropertyChanged(PropertyChangedEventArgs args)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, args);
            }
        }

        #endregion

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </returns>
        public override string ToString()
        {
            string crumb = this.Name;
            if (this.Parent.IsNotNull())
            {
                crumb = string.Concat(this.Parent.Name, " > ", crumb);

                if (this.Parent.Parent.IsNotNull())
                {
                    crumb = string.Concat(this.Parent.Parent.Name, " > ", crumb);
                }
            }
            return crumb;
        }
    }
}
