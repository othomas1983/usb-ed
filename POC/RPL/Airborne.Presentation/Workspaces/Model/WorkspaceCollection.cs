﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Airborne.Presentation.Workspaces
{
    /// <summary>
    /// Represents a collection of <see cref="Workspace"/>s
    /// </summary>
    public class WorkspaceCollection : ObservableCollection<Workspace>
    {
        private IList<WorkspaceRegistration> registrations;


        /// <summary>
        /// Initializes a new instance of the <see cref="WorkspaceCollection"/> class.
        /// </summary>
        public WorkspaceCollection()
        {
            registrations = new List<WorkspaceRegistration>();
        }

        /// <summary>
        /// Selects a view.
        /// </summary>
        /// <param name="view">The view.</param>
        public void SelectView(View view)
        {
            this.ForEach((workspace) => workspace.SelectView(view));
        }

        /// <summary>
        /// Clears the selection.
        /// </summary>
        public void ClearSelection()
        {
            this.ForEach((workspace) => workspace.ClearSelection());

        }



        /// <summary>
        /// Gets a list of all <see cref="WorkspaceRegistration"/> that has been added to the collection.
        /// </summary>
        /// <value>The registrations.</value>
        public IEnumerable<WorkspaceRegistration> Registrations
        {
            get
            {
                return registrations;
            }
        }
        /// <summary>
        /// Buils a <see cref="Workspace"/> hierarchy based on a <see cref="WorkspaceRegistration"/>.
        /// </summary>
        /// <param name="registration">The registration.</param>
        public void AddRegistration(WorkspaceRegistration registration)
        {
            if (!registrations.Select(r=> r.Function).Contains(registration.Function))
            {
                registrations.Add(registration);

                Workspace current = AddWorkspace(registration);

                Section section = AddSection(registration, current);

                AddView(registration, section);
            }
        }

        private static void AddView(WorkspaceRegistration registration, Section section)
        {
            View view = section.Views.FirstOrDefault(v => v.Tag == registration.Identifier);
            if (view.IsNull())
            {
                view = new View()
                {
                    DisplayName = registration.ViewDisplayName,
                    Name = registration.Function,
                    Tag = registration.Identifier,
                    Order = registration.ViewOrder,
                    Registration = registration,
                    IsDefault = registration.IsDefaultView ?? false
                };
                section.AddView(view);
            }
            else
            {
                if ((view.Order ?? 0) < (registration.ViewOrder ?? 0))
                {
                    view.Order = registration.ViewOrder;
                }
            }
        }

        private static Section AddSection(WorkspaceRegistration registration, Workspace current)
        {
            Section section = current.Sections.FirstOrDefault(s => string.Compare(s.Name, registration.Section, StringComparison.OrdinalIgnoreCase) == 0);
            if (section.IsNull())
            {
                section = new Section() { Name = registration.Section, Order = registration.SectionOrder, IsStartup = registration.IsStartupSection };
                current.AddSection(section);
            }
            else
            {
                if ((section.Order ?? 0) < (registration.SectionOrder ?? 0))
                {
                    section.Order = registration.SectionOrder;
                }
            }
            return section;
        }

        private Workspace AddWorkspace(WorkspaceRegistration registration)
        {
            Workspace current = this.FirstOrDefault(w => string.Compare(w.Name, registration.Workspace, StringComparison.OrdinalIgnoreCase) == 0);
            if (current.IsNull())
            {
                current = new Workspace() { Name = registration.Workspace };
                Add(current);
            }
            return current;
        }

        /// <summary>
        /// Lists all views.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<View> ListAllViews()
        {
            List<View> views = new List<View>();

            this.ForEach(
                (workspace) => workspace.Sections.ForEach(
                    (section) => section.Views.ForEach(
                        (view) => views.Add(view)
                     )
                )
            );

            return views;
        }
    }
}
