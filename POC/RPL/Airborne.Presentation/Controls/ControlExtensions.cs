﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;


namespace Airborne.Presentation.Controls
{
    /// <summary>
    /// Control extensions
    /// </summary>
    public static class ControlExtensions
    {
        /// <summary>
        /// Fade in animation
        /// </summary>
        public static void FadeIn(this UIElement instance, double seconds)
        {
            instance.Opacity = 0;
            var animation = new DoubleAnimation();
            animation.From = 0;
            animation.To = 1;
            animation.Duration = new Duration(TimeSpan.FromMilliseconds(seconds * 1000));
            Storyboard.SetTarget(animation, instance);
            Storyboard.SetTargetProperty(animation, new PropertyPath(Control.OpacityProperty));
            var fadeStoryboard = new Storyboard();
            fadeStoryboard.Children.Add(animation);
            fadeStoryboard.Begin();
        }

        /// <summary>
        /// Fade out animation
        /// </summary>
        public static void Fadeout(this UIElement instance, double seconds)
        {
            var animation = new DoubleAnimation();
            animation.From = instance.Opacity;
            animation.To = 0;
            animation.Duration = new Duration(TimeSpan.FromMilliseconds(seconds * 1000));
            Storyboard.SetTarget(animation, instance);
            Storyboard.SetTargetProperty(animation, new PropertyPath(Control.OpacityProperty));
            var fadeStoryboard = new Storyboard();
            fadeStoryboard.Children.Add(animation);
            fadeStoryboard.Begin();
        }
    }
}
