﻿using Airborne.Presentation.Security;

namespace Airborne.Presentation.Controls
{
    /// <summary>
    /// Instance provider for the security evaluator. 
    /// This is to be used internally for teh dependancy properties
    /// </summary>
    internal static class SecurityEvaluatorInstance
    {
        /// <summary>
        /// Gets or sets the evaluator instance.
        /// </summary>
        public static ISecurityEvaluator Instance
        {
            get;
            set;
        }
    }
}
