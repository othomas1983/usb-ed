﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace Airborne.Presentation.Controls
{
    /// <summary>
    /// Performs dependancy property evaluation for controls
    /// </summary>
    public sealed class SecurityAccessControl
    {
        /// <summary>
        /// Hidden constructor
        /// </summary>
        private SecurityAccessControl()
        {

        }

        /// <summary>
        /// Sets the value that is evaluated to determine if the control is to be disabled or not.
        /// </summary>
        public static readonly DependencyProperty EnableFunction = DependencyProperty.RegisterAttached("EnableFunction", typeof(string), typeof(SecurityAccessControl), new PropertyMetadata(string.Empty, OnEnableFunctionChanged));

        private static void OnEnableFunctionChanged(DependencyObject dp, DependencyPropertyChangedEventArgs e)
        {
            var control = dp as Control;
            control.IsEnabled = false;
            Evaluate(control, (string)e.NewValue, () => control.IsEnabled = true);
        }


        private static void Evaluate(Control control, string function, Action action)
        {
            if (control.IsNotNull() && SecurityEvaluatorInstance.Instance.IsNotNull())
            {
                if (SecurityEvaluatorInstance.Instance.CanWrite(function))
                {
                    action.Invoke();
                }
            }
        }



        /// <summary>
        /// Sets the enable function.
        /// </summary>
        /// <param name="dp">The dp.</param>
        /// <param name="value">The value.</param>
        public static void SetEnableFunction(DependencyObject dp, string value)
        {
            dp.SetValue(EnableFunction, value);
        }

        /// <summary>
        /// Gets the enable function.
        /// </summary>
        /// <param name="dp">The dp.</param>
        /// <returns></returns>
        public static string GetEnableFunction(DependencyObject dp)
        {
            return (string)dp.GetValue(EnableFunction);
        }



        /// <summary>
        /// Sets the value that is evaluated to determine if the control is to be visible or not.
        /// </summary>
        public static readonly DependencyProperty VisibleFunction = DependencyProperty.RegisterAttached("VisibleFunction", typeof(string), typeof(SecurityAccessControl), new PropertyMetadata(string.Empty, OnVisibleFunctionChanged));

        private static void OnVisibleFunctionChanged(DependencyObject dp, DependencyPropertyChangedEventArgs e)
        {
            var control = dp as Control;
            control.Visibility = Visibility.Collapsed;
            Evaluate(control, (string)e.NewValue, () => control.Visibility = Visibility.Visible);
        }

        /// <summary>
        /// Sets the visible function.
        /// </summary>
        /// <param name="dp">The dp.</param>
        /// <param name="value">The value.</param>
        public static void SetVisibleFunction(DependencyObject dp, string value)
        {
            dp.SetValue(VisibleFunction, value);
        }

        /// <summary>
        /// Gets the visible function.
        /// </summary>
        /// <param name="dp">The dp.</param>
        /// <returns></returns>
        public static string GetVisibleFunction(DependencyObject dp)
        {
            return (string)dp.GetValue(VisibleFunction);
        }
    }
}
