﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Airborne.Presentation.Context;
using Airborne.Presentation.Views;
using Airborne.Presentation.Views.ViewModels;
using Airborne.Presentation.Workspaces;
using Airborne.Presentation.Events;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Presentation.Events;
using System.Collections.ObjectModel;
using System.Collections;

namespace Airborne.Presentation.Silverlight.Views
{
    /// <summary>
    /// Default implementation of the menu view
    /// </summary>
    public partial class MenuView : UserControl, IMenuView
    {
        #region Locals
        IEventAggregator aggregator;
        #endregion

        [Dependency]
        public IEventAggregator Aggregator
        {
            get { return aggregator; }
            set { 
                    aggregator = value;
                    aggregator.GetEvent<GenericEvent<string>>().Subscribe(OnViewSelectedFromHomePage, ThreadOption.UIThread, true, p => p.Topic.Equals(EventTopics.OnViewSelectedFromHomePage));
                }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MenuView"/> class.
        /// </summary>
        public MenuView()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Gets or sets the model that is used to interact with this view.
        /// </summary>
        /// <value>The model.</value>
        public MenuViewModel Model
        {
            get
            {
                return DataContext as MenuViewModel;
            }
            set
            {
                DataContext = value;
            }
        }

        private void OnLogout(object sender, RoutedEventArgs e)
        {
            this.Aggregator.GetEvent<GenericEvent<object>>().Publish(new EventParameters<object>(EventTopics.OnLogout, null));
            Model.Workspaces = null;
            viewCombobox.ItemsSource = null;
        }

        private void OnViewSelectedFromHomePage(EventParameters<string> args)
        {
            if (Model.IsNotNull())
            {
                var workspaces = Model.Workspaces.ListAllViews();
                var views = workspaces.IsNotNull() ? workspaces.Select(v => v) : null;
                var view = views.IsNotNull() ? views.FirstOrDefault(v => v.Name == args.Value) : null;

                if (view.IsNotNull())
                {
                    viewCombobox.SelectedItem = view;
                    Model.SelectedView = view;
                }
            }
        }
    }
}
