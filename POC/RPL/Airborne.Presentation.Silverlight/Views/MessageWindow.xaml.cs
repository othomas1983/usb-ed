﻿using System.Windows;
using System.Windows.Controls;

namespace Airborne.Presentation.Silverlight.Views
{
    /// <summary>
    /// Default view for displaying messages
    /// </summary>
    public partial class MessageWindow : ChildWindow
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MessageWindow"/> class.
        /// </summary>
        public MessageWindow()
        {
            InitializeComponent();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        /// <summary>
        /// Factory method to create and show a <see cref="MessageWindow"/>
        /// </summary>
        public static void Show(string title, string error)
        {
            MessageWindow window = new MessageWindow();
            window.Title = title;
            window.ErrorTextBlock.Text = error;
            window.Show();
        }
    }
}

