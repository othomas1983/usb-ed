﻿using System.Reflection;
using Airborne.Presentation.Workspaces;
using Airborne.Presentation.Properties;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Airborne.Presentation.Silverlight")]
[assembly: AssemblyDescription("")]


[assembly: WorkspaceResource(typeof(Resources))]
