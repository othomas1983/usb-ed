﻿CREATE TABLE [dbo].[tb_PaymentReference] (
    [PaymentReferenceID] INT          IDENTITY (1, 1) NOT NULL,
    [PaymentReference]   VARCHAR (50) NULL
);

