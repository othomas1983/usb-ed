﻿CREATE TABLE [dbo].[tb_UserPaymentReference] (
    [UserPaymentReferenceID] INT IDENTITY (1, 1) NOT NULL,
    [PaymentReferenceID]     INT NOT NULL,
    [TimesUsed]              INT NULL,
    [UserID]                 INT NOT NULL
);

