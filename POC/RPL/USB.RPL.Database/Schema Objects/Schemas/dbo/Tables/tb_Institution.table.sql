﻿CREATE TABLE [dbo].[tb_Institution] (
    [InstitutionID]  INT           IDENTITY (1, 1) NOT NULL,
    [Name]           VARCHAR (100) NULL,
    [Vetted]         BIT           NULL,
    [LearningTypeID] INT           NULL
);



