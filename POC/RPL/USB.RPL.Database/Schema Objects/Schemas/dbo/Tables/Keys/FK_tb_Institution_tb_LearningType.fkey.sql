﻿ALTER TABLE [dbo].[tb_Institution]
    ADD CONSTRAINT [FK_tb_Institution_tb_LearningType] FOREIGN KEY ([LearningTypeID]) REFERENCES [dbo].[tb_LearningType] ([LearningTypeID]) ON DELETE NO ACTION ON UPDATE NO ACTION;

