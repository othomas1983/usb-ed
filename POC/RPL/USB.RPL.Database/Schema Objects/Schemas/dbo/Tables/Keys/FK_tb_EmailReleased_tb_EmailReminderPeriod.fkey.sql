﻿ALTER TABLE [dbo].[tb_EmailReleased]
    ADD CONSTRAINT [FK_tb_EmailReleased_tb_EmailReminderPeriod] FOREIGN KEY ([EmailReminderPeriodID]) REFERENCES [dbo].[tb_EmailReminderPeriod] ([EmailReminderPeriodID]) ON DELETE NO ACTION ON UPDATE NO ACTION;

