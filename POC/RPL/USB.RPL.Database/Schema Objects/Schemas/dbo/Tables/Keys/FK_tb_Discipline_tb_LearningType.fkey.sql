﻿ALTER TABLE [dbo].[tb_Discipline]
    ADD CONSTRAINT [FK_tb_Discipline_tb_LearningType] FOREIGN KEY ([LearningTypeID]) REFERENCES [dbo].[tb_LearningType] ([LearningTypeID]) ON DELETE NO ACTION ON UPDATE NO ACTION;

