﻿ALTER TABLE [dbo].[tb_UserPaymentReference]
    ADD CONSTRAINT [FK_tb_UserPaymentReference_tb_PaymentReference] FOREIGN KEY ([PaymentReferenceID]) REFERENCES [dbo].[tb_PaymentReference] ([PaymentReferenceID]) ON DELETE NO ACTION ON UPDATE NO ACTION;

