﻿CREATE TABLE [dbo].[tb_User] (
    [UserID]        INT           IDENTITY (1, 1) NOT NULL,
    [UserStatusID]  INT           NOT NULL,
    [FirstName]     VARCHAR (50)  NOT NULL,
    [LastName]      VARCHAR (50)  NULL,
    [FieldID]       INT           NULL,
    [PrimaryID]     INT           NULL,
    [Comment]       VARCHAR (250) NULL,
    [Title]         VARCHAR (50)  NULL,
    [IDNumber]      VARCHAR (15)  NULL,
    [Email]         VARCHAR (100) NULL,
    [Cellphone]     VARCHAR (20)  NULL,
    [Language]      VARCHAR (50)  NULL,
    [Race]          VARCHAR (50)  NULL,
    [MaritalStatus] VARCHAR (50)  NULL,
    [RPLDiscipline] VARCHAR (50)  NULL,
    [Gender]        VARCHAR (50)  NULL,
    [Province]      VARCHAR (100) NULL,
    [Location]      VARCHAR (100) NULL
);









