﻿CREATE TABLE [dbo].[tb_SupportingDocument] (
    [SupportingDocumentID] INT             IDENTITY (1, 1) NOT NULL,
    [SupportingDocument]   VARBINARY (MAX) NULL,
    [Extension]            VARCHAR (10)    NULL,
    [Name]                 VARCHAR (100)   NULL,
    [UserID]               INT             NULL
);



