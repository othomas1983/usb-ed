﻿CREATE TABLE [dbo].[tb_Discipline] (
    [DisciplineID]   INT           IDENTITY (1, 1) NOT NULL,
    [Name]           VARCHAR (100) NOT NULL,
    [Vetted]         BIT           NOT NULL,
    [LearningTypeID] INT           NULL
);

