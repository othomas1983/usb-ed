﻿CREATE PROCEDURE [dbo].[up_JOB_Reminders_ProcessReminders]

AS

BEGIN TRANSACTION

BEGIN TRY

		INSERT INTO dbo.tb_EmailQueue
		SELECT
			EE.EmailEventID,
			0 AS IsTransfered,
			null AS DateTransfered,
			null AS VoucherTransactionID,
			DATEADD(day, (-ERP.Period), V.ExpiryDate) AS DateCreated,
			0 AS Cancelled,
			V.VoucherID,
			ERP.EmailReminderPeriodID
		FROM
			dbo.tb_EmailReminderPeriod ERP
				JOIN dbo.tb_EmailEvents EE
					JOIN tb_Voucher V
						ON EE.MerchantID = V.MerchantID AND EE.LanguageID = V.CustomerLanguageID
					ON ERP.EmailID = EE.EmailID
		WHERE
			ERP.IsActive = 1 AND EE.IsActive = 1 AND V.VoucherStatusID = 1 AND V.ExpiryDate >= GETDATE()
			AND	ERP.EmailReminderPeriodID NOT IN 
				(SELECT EmailReminderPeriodID FROM dbo.tb_EmailQueue WHERE VoucherID = V.VoucherID)
				
				
    COMMIT TRANSACTION

END TRY
BEGIN CATCH
	 --SELECT 
	 --       ERROR_NUMBER() AS ErrorNumber
	 --       ,ERROR_SEVERITY() AS ErrorSeverity
	 --       ,ERROR_STATE() AS ErrorState
	 --       ,ERROR_PROCEDURE() AS ErrorProcedure
	 --       ,ERROR_LINE() AS ErrorLine
	 --       ,ERROR_MESSAGE() AS ErrorMessage;
	print ERROR_MESSAGE()
	IF @@TRANCOUNT > 0
		--WE CAN WRITE THE ERROR TO LOG FILE OR ERROR DATABASE
        ROLLBACK TRANSACTION
END CATCH