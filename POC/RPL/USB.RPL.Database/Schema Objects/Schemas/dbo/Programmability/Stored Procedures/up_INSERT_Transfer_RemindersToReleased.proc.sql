﻿

/*
Description       : Transfers Emails to Release table to be send
Usage             : Used by SQL JOB
Created By        : Divan Muller (Airborne)
Created On        : 05 April 2012
Current Version   : v1
Update History    : 
      <Date> - <Developer> - <Details>
*/

CREATE PROCEDURE [dbo].[up_INSERT_Transfer_RemindersToReleased]
		@VoucherID BIGINT,
		@EmailReminderPeriodID BIGINT,
		@EmailQueueID BIGINT,
		@EmailEventID VARCHAR(50)
AS

BEGIN TRANSACTION

BEGIN TRY

--DECLARE @VoucherID BIGINT,
--		@EmailReminderPeriodID BIGINT,
--		@EmailQueueID BIGINT,
--		@EmailEventID VARCHAR(50)
--SET @VoucherID = 14
--SET @EmailReminderPeriodID = 2
--SET @EmailEventID = 'Expiry Reminder'
--SET @EmailQueueID = 881

DECLARE @LanguageID BIGINT,
		@EventSubject VARCHAR(200),
		@EventBody VARCHAR(MAX),
		@HashedBody VARCHAR(MAX),
		@ToAddresses VARCHAR(MAX),
		@FromAddresses VARCHAR(MAX),
		@BCCAddresses VARCHAR(MAX),
		@EmailSettingsID INT,
		@Counter INT,
		@MaxFieldID INT,
		@FillPointName VARCHAR(50),
		@FillPointColumnName VARCHAR(50),
		@FillPointValue VARCHAR(50),
		@dynmaicsql			AS  NVARCHAR(1000),
		@dynamicparamdec	AS NVARCHAR(1000),
		@valueofid			AS INT,
		@returnedvalue		AS VARCHAR(100)


--GET LANGUAGE AND ToEmail Address
		SELECT TOP 1 
			@LanguageID = ISNULL(V.CustomerLanguageID, 8),
			@ToAddresses = ISNULL(V.CustomerEmail, '')
		FROM 
			dbo.tb_Voucher V
		WHERE 
			V.VoucherID = @VoucherID
			
		--PRINT 'LanguageID: ' + ISNULL(CAST(@LanguageID AS VARCHAR(20)), '')
		--PRINT 'ToAddresses: ' + ISNULL(CAST(@ToAddresses AS VARCHAR(20)), '')		
		
--GET SETTINGSID AND SET THE FROM AND BCC EMAIL ADDRESSES BASED ON SETTINGS FROM CLIENT

		SELECT @EmailSettingsID = EmailSettingsID 
		FROM tb_Voucher V JOIN tb_Merchant M ON V.MerchantID = M.MerchantID
		SELECT @FromAddresses = ISNULL(FromAddress, ''), @BCCAddresses = ISNULL(BCCAddress, '')
		FROM tb_EmailSettings WHERE EmailSettingsID = @EmailSettingsID
		
		--PRINT 'EmailSettingsID: ' + ISNULL(CAST(@EmailSettingsID AS VARCHAR(20)), '')
						
--GET THE EMAIL SUBJECT AND TEMPLATE
		SELECT TOP 1 @EventSubject = ISNULL([Subject], ''),@EventBody = ISNULL(Template, '')
		FROM dbo.tb_EmailEvents WHERE LanguageID = @LanguageID AND EmailEventID = @EmailEventID
--SET HASHED VARIABLE EQUAL TO BODY
		SET @HashedBody = @EventBody
		
		--PRINT 'EventSubject: ' + ISNULL(CAST(@EventSubject AS VARCHAR(MAX)), '')
		--PRINT 'EventBody: ' + ISNULL(CAST(@EventBody AS VARCHAR(MAX)), '')		
		--PRINT 'HashedBody: ' + ISNULL(CAST(@HashedBody AS VARCHAR(MAX)), '')

--GET ID COUNT
		SELECT @MaxFieldID = MAX(EmailFieldID) 
		FROM tb_EmailFields
			
		--PRINT 'MaxFieldID: ' + ISNULL(CAST(@MaxFieldID AS VARCHAR(20)), '')		
		
		SET @Counter = 1
		WHILE @Counter <= @MaxFieldID
			BEGIN	
			
				--PRINT 'Counter: ' + ISNULL(CAST(@Counter AS VARCHAR(20)), '')
			
				SELECT
					@FillPointName = Name,
					@FillPointColumnName = DatabaseName
				FROM tb_EmailFields
				WHERE EmailFieldID = @Counter
				
				--PRINT 'FillPointName: ' + ISNULL(CAST(@FillPointName AS VARCHAR(20)), '')
				--PRINT 'FillPointColumnName: ' + ISNULL(CAST(@FillPointColumnName AS VARCHAR(20)), '')
				
				IF ISNULL(@FillPointColumnName, '') <> ''
					BEGIN
						--GET COLUMN VALUE
						SET @dynmaicsql = N'SELECT @value = R.' + ISNULL(@FillPointColumnName, '') + N'
												FROM (SELECT * FROM 
														dbo.uf_TEMPLATE_Get_TemplateData_ByVoucherID('+CAST(@VoucherID AS NVARCHAR(30))+N')) R'
						
						--PRINT 'dynmaicsql: ' + ISNULL(CAST(@dynmaicsql AS VARCHAR(20)), '')
						
						SET @dynamicparamdec = '@value nvarchar(200) output'
						EXECUTE sp_executesql 
								 @dynmaicsql
								,@dynamicparamdec 
								,@returnedvalue  OUTPUT
						--SET VALUE THAT WILL BE USED TO POPULATE THE FILLPOINT		 
						SET @FillPointValue = @returnedvalue
						
						--PRINT 'FillPointValue: ' + ISNULL(CAST(@FillPointValue AS VARCHAR(20)), '')
						
						--SET NON MASKED DATA THAT WILL BE USED TO SENT TO THE CLIENT
						SET @EventBody = REPLACE(ISNULL(@EventBody, ''),ISNULL(@FillPointName, ''), ISNULL(@FillPointValue, '')) 
						
						--SETUP MASKED DATA THAT WILL BE SEEN BY SUPPORT DEPARTMENTS
						IF SUBSTRING(@FillPointName, 1, 3) = '###'
							BEGIN
								-- MASKING DATA ACCORDING TO FILLPOINT VALUE
								SET @HashedBody = REPLACE(ISNULL(@HashedBody, ''),ISNULL(@FillPointName, ''), '##############') 
							END
						ELSE
							BEGIN
								SET @HashedBody = REPLACE(ISNULL(@HashedBody, ''),ISNULL(@FillPointName, ''), ISNULL(@FillPointValue, '')) 
							END
						
					END

				SET @Counter = @Counter + 1
				--print 'COUNTER: '+ISNULL(CAST(@counter AS VARCHAR(10)), '0')
			 END
			 
--INSERT VALUES INTO RELEASED TABLE

			 INSERT INTO [tb_EmailReleased]
			   (
			   [EmailEventID]
			   ,[EmailSentStatusID]
			   ,[MailSentDate]
			   ,[RetrySend]
			   ,[DateCreated]
			   ,[VoucherID]
			   ,[EmailSubject]
			   ,[EmailBody]
			   ,[ViewHashedMail]
			   ,[EmailToAddresses]
			   ,[EmailFromAddresses]
			   ,[EmailBCCAddresses]
			   ,[EmailReminderPeriodID]
			   )
			SELECT
				@EmailEventID
				,1
				,null
				,0
				,GETDATE()
				,@VoucherID
				,@EventSubject 
				,ISNULL(@EventBody, '')
				,ISNULL(@HashedBody, '')
				,@ToAddresses
				,@FromAddresses
				,@BCCAddresses
				,@EmailReminderPeriodID
				
--NO ERRORS, UPDATE THE EMAILQUEUE TABLE
			UPDATE tb_EmailQueue SET
				IsTransfered = 1
				,DateTransfered = GETDATE()
			WHERE
				EmailQueueID = @EmailQueueID
				
				
	COMMIT TRANSACTION

END TRY
BEGIN CATCH
	 --SELECT 
	 --       ERROR_NUMBER() AS ErrorNumber
	 --       ,ERROR_SEVERITY() AS ErrorSeverity
	 --       ,ERROR_STATE() AS ErrorState
	 --       ,ERROR_PROCEDURE() AS ErrorProcedure
	 --       ,ERROR_LINE() AS ErrorLine
	 --       ,ERROR_MESSAGE() AS ErrorMessage;
	print ERROR_MESSAGE()
	IF @@TRANCOUNT > 0
		--WE CAN WRITE THE ERROR TO LOG FILE OR ERROR DATABASE
        ROLLBACK TRANSACTION
END CATCH