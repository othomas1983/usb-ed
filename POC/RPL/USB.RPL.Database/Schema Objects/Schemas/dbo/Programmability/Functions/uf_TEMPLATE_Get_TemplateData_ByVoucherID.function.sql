﻿/*
Description       : GET NORMALISED TABLE FOR FILL POINTS
Usage             : Used by SQL JOB
Created By        : Divan Muller (Airborne)
Created On        : 24 April 2012
Current Version   : v1
Update History    : 
      <Date> - <Developer> - <Details>
*/


CREATE FUNCTION [dbo].[uf_TEMPLATE_Get_TemplateData_ByVoucherID]
(  
 @VoucherID BIGINT
)   
	RETURNS @TemplateData TABLE
	 (
		[CurrencyCode] varchar(50),
		[AppliedAmount] varchar(50),
		[Balance] varchar(50),
		[OpeningBalanceAmount] varchar(50),
		[ExpiryDate] varchar(50),
		[VoucherNumber] varchar(50),
		[VoucherStatus] varchar(50),
		[RecipientEmail] varchar(50),
		[Title] varchar(50),
		[FirstName] varchar(50),
		[LastName] varchar(50),
		[Language] varchar(50),
		[CustomerID] varchar(50),
		[PNRNumber] varchar(50),
		[PNRDate] varchar(50),
		[CustomFieldName] varchar(100),
		[CustomeFieldValue] varchar(100),
		[DatacashReference] varchar(50),
		[MerchantReference] varchar(50),
		[TransactionDate] datetime
	 )
 
AS 
	BEGIN
	
		DECLARE @VoucherTransactionID BIGINT
		SELECT @VoucherTransactionID = VoucherTransactionsID
		FROM tb_VoucherTransactions 
		WHERE VoucherID = @VoucherID AND TransactionsTypeID = 1
	
		INSERT INTO @TemplateData
		SELECT
			ISNULL(C.CurrencyCode3, '') 
			,ISNULL(VTran.TransactionAmount, '0.00')
			,ISNULL(V.CurrentBalance, '0.00')
			,ISNULL(V.VoucherAmount, '0.00')
			,ISNULL(V.ExpiryDate, '')
			,ISNULL(V.VoucherNumber, '')
			,ISNULL(VS.VoucherStatus, '')
			,ISNULL(V.CustomerEmail, '')
			,ISNULL(V.CustomerTitle, '')
			,ISNULL(V.CustomerFirstName, '')
			,ISNULL(V.CustomerLastName, '')
			,ISNULL(L.LanguageCode, '')
			,ISNULL(V.MerchantCustomerID, '')
			,ISNULL((SELECT TOP 1 B.PnrNumber FROM dbo.tb_Bookings B WHERE B.VoucherTransactionsID = Vtran.VoucherTransactionsID), '')
			,ISNULL((SELECT TOP 1 B.PnrDate FROM dbo.tb_Bookings B WHERE B.VoucherTransactionsID = Vtran.VoucherTransactionsID), '')
			,ISNULL((SELECT TOP 1 CF.Name FROM dbo.tb_CustomFields CF WHERE CF.VoucherTransactionsID = Vtran.VoucherTransactionsID ORDER BY CustomFieldID DESC), '')
			,ISNULL((SELECT TOP 1 CF.Value FROM dbo.tb_CustomFields CF WHERE CF.VoucherTransactionsID = Vtran.VoucherTransactionsID ORDER BY CustomFieldID DESC), '')
			,ISNULL(VTran.DatacashReference, '')
			,ISNULL(VTran.MerchantTransactionReference, '')
			,ISNULL(VTran.TransactionDate, '')
		FROM
			tb_Voucher V
				JOIN tb_Languages L
					ON V.CustomerLanguageID = L.LanguageID
				JOIN tb_VoucherStatus VS
					ON V.VoucherStatusID = VS.VoucherStatusID
				JOIN tb_Merchant M
					JOIN tb_MerchantVoucherType MVT
						JOIN tb_VoucherType VT
							JOIN tb_Currency C
								ON VT.CurrencyID = C.CurrencyID
							ON MVT.VoucherTypeID = VT.VoucherTypeID
						ON M.MerchantID = MVT.MerchantID
					ON V.MerchantID = M.MerchantID
				JOIN tb_VoucherTransactions VTran
					ON V.VoucherID = VTran.VoucherID
		WHERE
			VTran.VoucherTransactionsID = @VoucherTransactionID
	
		RETURN		
	END