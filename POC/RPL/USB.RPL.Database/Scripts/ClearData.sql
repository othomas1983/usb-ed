USE RPLPrimary
GO

truncate table dbo.tb_UserCourse
truncate table dbo.tb_SupportingDocument
truncate table dbo.tb_User

USE RPLStaging
GO

truncate table dbo.tb_UserCourse
truncate table dbo.tb_SupportingDocument
truncate table dbo.tb_User


USE RPLSubmissions
GO

truncate table dbo.tb_UserCourse
truncate table dbo.tb_User