﻿-- =============================================
-- Script Template
-- =============================================

USE [RPLStaging]
GO
/****** Object:  Table [dbo].[tb_CourseStatus]    Script Date: 10/08/2012 22:07:31 ******/
SET IDENTITY_INSERT [dbo].[tb_CourseStatus] ON
INSERT [dbo].[tb_CourseStatus] ([CourseStatusID], [Name]) VALUES (1, N'Current')
INSERT [dbo].[tb_CourseStatus] ([CourseStatusID], [Name]) VALUES (2, N'Completed')
INSERT [dbo].[tb_CourseStatus] ([CourseStatusID], [Name]) VALUES (3, N'Not Completed')
SET IDENTITY_INSERT [dbo].[tb_CourseStatus] OFF
/****** Object:  Table [dbo].[tb_UserStatus]    Script Date: 10/08/2012 22:07:31 ******/
SET IDENTITY_INSERT [dbo].[tb_UserStatus] ON
INSERT [dbo].[tb_UserStatus] ([UserStatusID], [Status]) VALUES (1, N'Pending')
INSERT [dbo].[tb_UserStatus] ([UserStatusID], [Status]) VALUES (2, N'Comfirmed')
INSERT [dbo].[tb_UserStatus] ([UserStatusID], [Status]) VALUES (3, N'Copied')
INSERT [dbo].[tb_UserStatus] ([UserStatusID], [Status]) VALUES (4, N'Update')
SET IDENTITY_INSERT [dbo].[tb_UserStatus] OFF
/****** Object:  Table [dbo].[tb_LearningType]    Script Date: 10/08/2012 22:07:31 ******/
SET IDENTITY_INSERT [dbo].[tb_LearningType] ON
INSERT [dbo].[tb_LearningType] ([LearningTypeID], [Name]) VALUES (1, N'Schooling Information')
INSERT [dbo].[tb_LearningType] ([LearningTypeID], [Name]) VALUES (2, N'General Education')
INSERT [dbo].[tb_LearningType] ([LearningTypeID], [Name]) VALUES (3, N'Tertiary Education')
INSERT [dbo].[tb_LearningType] ([LearningTypeID], [Name]) VALUES (4, N'Sector Specific Information')
INSERT [dbo].[tb_LearningType] ([LearningTypeID], [Name]) VALUES (5, N'Vocational Training ')
INSERT [dbo].[tb_LearningType] ([LearningTypeID], [Name]) VALUES (6, N'Employment History')
SET IDENTITY_INSERT [dbo].[tb_LearningType] OFF
/****** Object:  Table [dbo].[tb_Institution]    Script Date: 10/08/2012 22:07:31 ******/
SET IDENTITY_INSERT [dbo].[tb_Institution] ON
INSERT [dbo].[tb_Institution] ([InstitutionID], [Name], [Vetted]) VALUES (1, N'University of Stellenbosch', 1)
INSERT [dbo].[tb_Institution] ([InstitutionID], [Name], [Vetted]) VALUES (2, N'University of Cape Town', 1)
INSERT [dbo].[tb_Institution] ([InstitutionID], [Name], [Vetted]) VALUES (3, N'CPUT', 0)
SET IDENTITY_INSERT [dbo].[tb_Institution] OFF
/****** Object:  Table [dbo].[tb_Discipline]    Script Date: 10/08/2012 22:07:31 ******/
SET IDENTITY_INSERT [dbo].[tb_Discipline] ON
INSERT [dbo].[tb_Discipline] ([DisciplineID], [Name], [Vetted], [LearningTypeID]) VALUES (1, N'South African National Defence Force', 1, 4)
INSERT [dbo].[tb_Discipline] ([DisciplineID], [Name], [Vetted], [LearningTypeID]) VALUES (2, N'South African Police Service', 1, 5)
INSERT [dbo].[tb_Discipline] ([DisciplineID], [Name], [Vetted], [LearningTypeID]) VALUES (3, N'Private Security', 1, 5)
INSERT [dbo].[tb_Discipline] ([DisciplineID], [Name], [Vetted], [LearningTypeID]) VALUES (6, N'State Security Agency', 1, 4)
INSERT [dbo].[tb_Discipline] ([DisciplineID], [Name], [Vetted], [LearningTypeID]) VALUES (7, N'IMPS-SA', 1, 5)
INSERT [dbo].[tb_Discipline] ([DisciplineID], [Name], [Vetted], [LearningTypeID]) VALUES (8, N'Youth 17 to 24 Years', 1, 6)
INSERT [dbo].[tb_Discipline] ([DisciplineID], [Name], [Vetted], [LearningTypeID]) VALUES (9, N'Youth 25 to 34 Years', 0, 6)
SET IDENTITY_INSERT [dbo].[tb_Discipline] OFF
/****** Object:  Table [dbo].[tb_Course]    Script Date: 10/08/2012 22:07:31 ******/
SET IDENTITY_INSERT [dbo].[tb_Course] ON
INSERT [dbo].[tb_Course] ([CourseID], [Name], [Vetted], [LearningTypeID], [DisciplineID]) VALUES (1, N'Phase 1: Basic training and tactical policing programme', 1, 1, NULL)
INSERT [dbo].[tb_Course] ([CourseID], [Name], [Vetted], [LearningTypeID], [DisciplineID]) VALUES (2, N'Phase 2: Field training as student constable in uniform', 1, 2, NULL)
INSERT [dbo].[tb_Course] ([CourseID], [Name], [Vetted], [LearningTypeID], [DisciplineID]) VALUES (3, N'Phase 3: In-service training', 1, 3, NULL)
INSERT [dbo].[tb_Course] ([CourseID], [Name], [Vetted], [LearningTypeID], [DisciplineID]) VALUES (4, N'Basic Management Learning Programme Level  1 - BMLP 1', 1, 4, 1)
INSERT [dbo].[tb_Course] ([CourseID], [Name], [Vetted], [LearningTypeID], [DisciplineID]) VALUES (5, N'Basic Management Learning Programme Level  2 - BMLP 2', 1, 4, 2)
INSERT [dbo].[tb_Course] ([CourseID], [Name], [Vetted], [LearningTypeID], [DisciplineID]) VALUES (6, N'Junior Management Learning Programme  - JMDP', 1, 4, 3)
INSERT [dbo].[tb_Course] ([CourseID], [Name], [Vetted], [LearningTypeID], [DisciplineID]) VALUES (7, N'Middle Management Learning Programme - MMDP', 1, 6, 6)
INSERT [dbo].[tb_Course] ([CourseID], [Name], [Vetted], [LearningTypeID], [DisciplineID]) VALUES (8, N'Executive Development Learning Programme - EDLP', 1, 5, 7)
SET IDENTITY_INSERT [dbo].[tb_Course] OFF

