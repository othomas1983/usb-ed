﻿using System;
using Airborne.Nhibernate;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using USB.RPL.Domain.User;

namespace USB.RPL.Domain.Tests.Persistence
{
    [TestClass]
    public class RPLMappingTestFixture
    {
        [TestInitialize]
        public void Initialise()
        {
            Factory = SessionFactory.Instance;
        }

        private static SessionFactory Factory { get; set; }

        [TestMethod]
        public void EnsureMappingsAreCorrect()
        {
            log4net.Config.XmlConfigurator.Configure();


            TestMapping(typeof(PaymentReference));
            TestMapping(typeof(UserPaymentReference));
            
            TestMapping(typeof(SupportingDocument));
            TestMapping(typeof(CourseStatus));
            TestMapping(typeof(LearningType));
            TestMapping(typeof(Course));
            TestMapping(typeof(Discipline));
            TestMapping(typeof(Institution));
            TestMapping(typeof(UserProfileBasket));
            TestMapping(typeof(UserProfile));
            

        }

        #region Methods

        private void TestMapping(Type type)
        {
            var session = Factory.CreateSession("");
            var results = session.CreateCriteria(type).SetMaxResults(10).List();

            Assert.IsNotNull(results);
        }

        #endregion
    }
}
