﻿using System.Collections.Generic;
using System.Linq;
using Airborne.Domain.Repository;
using Airborne.Domain.Rules;
using USB.RPL.Domain.Validation;
using Microsoft.Practices.Unity;

namespace USB.RPL.Domain.Tests.Utils
{
    public static class TestHelpers
    {
        public static IRPLSystemFacade CreateSystem()
        {
            var container = RPLSystemFactory.ConfigureContainer();

            container.RegisterInstance<IRepository>(new MemoryRepository());

            return RPLSystemFactory.CreateSystem(container);
        }

        public static IUnityContainer CreateUnityContainer()
        {
            return RPLSystemFactory.ConfigureContainer(true);

        }

        public static IEnumerable<IRule> ResolveRules<TRule>(object instance, ValidationContext context) where TRule : class
        {
            return context.ResolveRules(instance).Where(rule => typeof(TRule).IsAssignableFrom(rule.GetType()));
        }
    }
}
