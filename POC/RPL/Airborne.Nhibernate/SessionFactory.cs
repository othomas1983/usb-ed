﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using System.Configuration;
using NHibernate.Metadata;
using System.Diagnostics;

namespace Airborne.Nhibernate
{
    /// <summary>
    /// A factory for creating and managing the nhibernate sessions.
    /// </summary>
    //[DebuggerStepThrough]
    public class SessionFactory
    {
        /// <summary>
        /// Cache of session factories
        /// </summary>
        private IDictionary<string, ISessionFactory> sessionFactories;

        #region SessionFactory Singleton
        private static SessionFactory sessionFactory;
        private static object mutex = new object();

        /// <summary>
        /// Gets a thread safe single instance of <see cref="SessionFactory"/>
        /// </summary>
        public static SessionFactory Instance
        {
            get
            {
                lock (mutex)
                {
                    if (sessionFactory == null)
                    {
                        sessionFactory = new SessionFactory();
                    }
                }
                return sessionFactory;
            }
        }
        #endregion


        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SessionFactory"/> class.
        /// </summary>
        private SessionFactory()
        {
            sessionFactories = new Dictionary<string, ISessionFactory>();
        }

        #endregion
        #region Properties
        /// <summary>
        /// Indexer that returns a new session based on teh connection name
        /// </summary>
        public ISession this[string configuration]
        {
            get
            {
                return this.CreateSession(configuration);
            }
        }

        /// <summary>
        /// Get a list of regsitered connection names
        /// </summary>
        public string[] GetConnectionNames()
        {
            string[] connectionNames = new string[sessionFactories.Keys.Count];
            sessionFactories.Keys.CopyTo(connectionNames, 0);
            return connectionNames;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Create a new session based on the configuration.
        /// </summary>
        public ISession CreateSession(string configuration)
        {
            VerifySessionFactory(configuration);

            return sessionFactories[configuration].OpenSession();
        }

        /// <summary>
        /// Creates a stateless session. This session is optimised for bulk inserting etc.
        /// Set the adonet.batch_size in the configuration for the session factory.
        /// </summary>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public IStatelessSession CreateStatelessSession(string configuration)
        {
            VerifySessionFactory(configuration);
            return sessionFactories[configuration].OpenStatelessSession();
        }


        /// <summary>
        /// Verifies that the session factory for the <paramref name="configuration"/> exists.
        /// </summary>
        private void VerifySessionFactory(string configuration)
        {
            lock (mutex)
            {
                if (!sessionFactories.ContainsKey(configuration))
                {
                    NHibernate.Cfg.Configuration cfg = new NHibernate.Cfg.Configuration();
                    if (string.IsNullOrEmpty(configuration))
                    {
                        cfg.Configure();
                    }
                    else
                    {
                        string configSettings = ConfigurationManager.AppSettings[configuration];

                        if (string.IsNullOrEmpty(configSettings))
                        {
                            configSettings = configuration;
                        }

                        cfg.Configure(configSettings);
                    }
                    sessionFactories.Add(configuration, cfg.BuildSessionFactory());
                }
            }
        }

        #endregion
    }
}
