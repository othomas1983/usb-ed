﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Airborne.Domain.UnitOfWork;
using NHibernate;
using System.Data;

namespace Airborne.Nhibernate.Domain.UnitOfWork
{
    /// <summary>
    /// NHibernate specific implementation of the <see cref="UnitOfWorkBase"/>
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Nhibernate")]
    public class NhibernateUnitOfWork : UnitOfWorkBase, IDisposable
    {

        /// <summary>
        /// Gets or sets the session.
        /// </summary>
        /// <value>The session.</value>
        public ISession Session
        {
            get;
            set;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NhibernateUnitOfWork"/> class.
        /// </summary>
        public NhibernateUnitOfWork()
        {
            Session = SessionFactory.Instance.CreateSession(string.Empty);
        }

        /// <summary>
        /// Holds the status of the unit of work. If it has been comitted or rolled back.
        /// </summary>
        private bool IsComplete
        {
            get;
            set;
        }

        #region Methods
        /// <summary>
        /// Sets the session to a custom instance.
        /// </summary>
        /// <param name="session">The session.</param>
        public void SetSession(ISession session)
        {
            Guard.ArgumentNotNull(session, "session");
            this.Session = session;
        }


        /// <summary>
        /// Performs <paramref name="action"/> on the specific itmes registered for <paramref name="type"/>
        /// </summary>
        private void ExecuteAction(RegistrationType type, Action<object> action)
        {
            var items = ObjectRegister
                .Where( instances=>instances.Value == type)
                .Select( instance=>instance.Key);

            if ((items != null) && (items.Count() > 0))
            {
                foreach (object item in items)
                {
                    action(item);
                }
            }
        }
        #endregion

        /// <summary>
        /// Commits the unit of work
        /// </summary>
        public override void Commit()
        {
            IsComplete = true;
            if (Session.IsConnected && Session.IsOpen)
            {
                using (ITransaction transaction = Session.BeginTransaction(IsolationLevel.RepeatableRead))
                {
                    try
                    {
                        ExecuteAction(RegistrationType.New, Session.SaveOrUpdate);
                        ExecuteAction(RegistrationType.Dirty, Session.Update);
                        ExecuteAction(RegistrationType.Deleted, Session.Delete);
                        transaction.Commit();
                        ObjectRegister.Clear();
                    }
                    catch
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
            }
        }

        /// <summary>
        /// Performs a rollback on this instance 
        /// of the <see cref="UnitOfWorkBase"/>
        /// </summary>
        public override void Rollback()
        {
            IsComplete = true;
            ExecuteAction(RegistrationType.Dirty, Session.Refresh);
            ObjectRegister.Clear();
        }



        #region IDisposable Members

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1063:ImplementIDisposableCorrectly")]
        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (!IsComplete)
                {
                    Rollback();
                }
                Session.Dispose();
            }
        }
        #endregion
			
    }
}
