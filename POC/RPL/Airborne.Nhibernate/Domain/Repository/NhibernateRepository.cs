﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Linq.Expressions;
using Airborne.Domain.Repository;
using Airborne.Domain.Repository.QueryObjects;
using NHibernate;
using NHibernate.Linq;
using Microsoft.Practices.Unity;
using Airborne.Caching;
using Airborne.Domain.ValueObjects;

namespace Airborne.Nhibernate.Domain.Repository
{
    /// <summary>
    /// This is an nHibernate specific implementation of the <see cref="IRepository"/>.
    /// </summary>
    public class NhibernateRepository : IRepository
    {
        #region Properties
        
        /// <summary>
        /// The nhibernate session that this repository 
        /// is associated with.
        /// </summary>
        public ISession Session
        {
            get;
            set;
        }


        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="NhibernateRepository"/> class.
        /// </summary>
        /// <param name="session">The session.</param>
        public NhibernateRepository(ISession session)
        {
            Session = session;
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="NhibernateRepository"/> class.
        /// </summary>
        [InjectionConstructor]
        public NhibernateRepository()
        {
            Configure();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Starts a transaction
        /// </summary>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public Airborne.Domain.Repository.ITransaction BeginTransaction()
        {
            return new NhibernateTransactionWrapper(Session.BeginTransaction());
        }

        private void Configure()
        {
            Session = SessionFactory.Instance.CreateSession(string.Empty);
        }

        private ICriteria Convert<T>(Criteria criteria)
        {
            ICriteria sessionCriteria = Session.CreateCriteria(typeof(T));
            ICriteria actual = CriteriaBuilder.BuildCriteria(sessionCriteria, criteria);
            return actual;
        }
        #endregion

        #region IRepository Members

        /// <summary>
        /// This Does nothing at the moment
        /// </summary>
        public void Init()
        {
            //do nothing ...
        }


        /// <summary>
        /// Saves the specified entity back to the repository
        /// </summary>
        public void Add(object entity)
        {
            Guard.ArgumentNotNull(entity, "entity");
            Session.SaveOrUpdate(entity);
        }

        /// <summary>
        /// Deletes the specified entity in the repository
        /// </summary>
        public void Remove(object entity)
        {
            Guard.ArgumentNotNull(entity, "entity");
            Session.Delete(entity);

        }

        /// <summary>
        /// This method shows intent to perform update actions on the <param name="entity"/>
        /// </summary>
        public void Update(Airborne.Domain.IAggregate entity)
        {
            Guard.ArgumentNotNull(entity, "entity");
            Session.SaveOrUpdate(entity);
    
        }


        /// <summary>
        /// Refreshes an entity
        /// </summary>
        public virtual void Refresh(object entity)
        {
            if (entity.IsNotNull())
            {
                Session.Refresh(entity);
            }
        }


        /// <summary>
        /// Performs a merge operation
        /// </summary>
        public object Merge(object obj)
        {
            return Session.Merge(obj);
        }

        /// <summary>
        /// Finds top 'N' entities matching an expression
        /// </summary>
        public IEnumerable<T> Top<T>(Expression<Func<T, bool>> predicate, int top)
        {
            return Session.Linq<T>().Where(predicate).Take(top);
        }

        /// <summary>
        /// Tries the find one or returns the default value for the type.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures")]
        public T First<T>(Expression<Func<T, bool>> predicate)
        {
            return Session.Linq<T>().Where(predicate).FirstOrDefault();
        }

        /// <summary>
        /// Finds top 'N' entities matching an expression ordered ascending
        /// </summary>
        public IEnumerable<T> TopOrderBy<T, TKey>(Expression<Func<T, bool>> where, Expression<Func<T, TKey>> orderBy, int top)
        {
            return Session.Linq<T>().Where(where).OrderBy(orderBy).Take(top);
        }

        /// <summary>
        /// Finds top 'N' entities matching an expression ordered descending
        /// </summary>
        public IEnumerable<T> TopOrderByDescending<T, TKey>(Expression<Func<T, bool>> where, Expression<Func<T, TKey>> orderBy, int top)
        {
            return Session.Linq<T>().Where(where).OrderByDescending(orderBy).Take(top);
        }

        /// <summary>
        /// Tries the find one or returns the default value for the type.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter")]
        public T First<T>(Criteria criteria)
        {
            ICriteria sessionCriteria = Convert<T>(criteria);
            return sessionCriteria.SetMaxResults(1).List<T>().FirstOrDefault();
        }

        /// <summary>
        /// Finds all entities in the repository
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter")]
        public IEnumerable<T> FindAll<T>()
        {
            var criteria = Session.CreateCriteria(typeof(T));

            if (typeof(IValueObject).IsAssignableFrom(typeof(T)))
            {
                criteria.SetCacheable(true);
            }
            return criteria.List<T>();
        }


        //TODO: Change Func<T,bool> expression to Expression<Func<T, bool>> expression
        /// <summary>
        /// Finds all entities matching an expression
        /// </summary>
        //public IEnumerable<T> FindAll<T>(Func<T, bool> expression)
        public IEnumerable<T> FindAll<T>(Expression<Func<T, bool>> predicate)
        {
            return Session.Linq<T>().Where(predicate);
        }

        /// <summary>
        /// Finds all entities matching a filter
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter")]
        public IEnumerable<T> FindAll<T>(Criteria criteria)
        {
            ICriteria sessionCriteria = Convert<T>(criteria);
            return sessionCriteria.List<T>();
        }

        /// <summary>
        /// Finds an entity based on a known id or key.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter")]
        public T FindById<T>(object id) where T : class
        {
            return Session.Get<T>(id);
        }

        #endregion

        #region IDisposable Members

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (Session.IsOpen)
                {
                    Session.Close();
                }

                Session.Dispose();
            }
        }



        #endregion


        
    }
}
