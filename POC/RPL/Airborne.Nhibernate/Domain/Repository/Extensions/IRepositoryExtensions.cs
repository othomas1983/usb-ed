﻿using System;
using Airborne.Domain.Repository;
using Airborne.Nhibernate.Domain.Repository;

namespace Airborne.Domain.Repository
{
    /// <summary>
    /// Extends the IRepository interface with convience methods
    /// </summary>
    public static class IRepositoryExtensions
    {

        /// <summary>
        /// Performs an object merge within the repository's session
        /// </summary>
        public static void Merge(this IRepository repository, object obj)
        {
            var repo = repository as NhibernateRepository;
            if (repo.IsNotNull())
            {
                repo.Session.Merge(obj);
            }
        }
    }
}