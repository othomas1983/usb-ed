﻿using System;
using NHibernate;

namespace Airborne.Nhibernate.Domain.Repository
{
    /// <summary>
    /// Wrapper for an nhibernate transaction
    /// </summary>
    internal class NhibernateTransactionWrapper : Airborne.Domain.Repository.ITransaction, IDisposable
    {
        private ITransaction Transaction { get; set; }

        public NhibernateTransactionWrapper(ITransaction transaction)
        {
            Guard.ArgumentNotNull(transaction, "transaction");
            Transaction = transaction;

        }

        /// <summary>
        /// Gets a value indicating whether this transaction is active.
        /// </summary>
        /// <value></value>
        public bool IsActive
        {
            get { return Transaction.IsActive; }
        }

        /// <summary>
        /// Gets a value indicating whether this transaction was committed.
        /// </summary>
        /// <value></value>
        public bool WasCommitted
        {
            get { return Transaction.WasCommitted; }
        }

        /// <summary>
        /// Gets a value indicating whether this transaction was rolled back.
        /// </summary>
        /// <value></value>
        public bool WasRolledBack
        {
            get { return Transaction.WasRolledBack; }
        }

        /// <summary>
        /// Commits this transaction.
        /// </summary>
        public void Commit()
        {
            Transaction.Commit();
        }

        public void Rollback()
        {
            Transaction.Rollback();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Transaction.Dispose();
        }
    }
}
