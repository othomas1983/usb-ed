using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;

namespace Airborne.Domain.Repository.QueryObjects
{
    /// <summary>
    /// Nhibernate expression builder. Taking <see cref="Criteria"/> and converts it to nhibernate specific
    /// <see cref="ICriteria"/>.
    /// </summary>
    public static class CriteriaBuilder
    {
        /// <summary>
        /// Builds the <see cref="ICriteria"/> base on the <see cref="Criteria"/>
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "nhibernate", Justification = "It is in fact spelled correctly")]
        public static ICriteria BuildCriteria(ICriteria nhibernateCriteria, Criteria criteria)
        {

            if (criteria.Filter.IsNotNull())
            {
                ICriterion criterion = BuildCriterion(criteria.Filter);
                nhibernateCriteria.Add(criterion);
            }

            if (criteria.Sorting.Count() > 0)
            {
                BuildSort(nhibernateCriteria, criteria.Sorting);
            }

            if (criteria.FetchSize.HasValue)
            {
                nhibernateCriteria.SetMaxResults(criteria.FetchSize.Value);
            }

            if (criteria.Distinct)
            {
                nhibernateCriteria.SetResultTransformer(new DistinctRootEntityResultTransformer());
            }

            return nhibernateCriteria;
        }

        private static void BuildSort(ICriteria criteria, IEnumerable<Sort> sorting)
        {
            foreach (var item in sorting)
            {
                if (item.Ascending)
                {
                    criteria.AddOrder(Order.Asc(item.Property));
                }
                else
                {
                    criteria.AddOrder(Order.Desc(item.Property));
                }
            }
        }

        private static ICriterion BuildCriterion(Filter filter)
        {
            var equalityFilter = filter as SimpleFilter;
            if (equalityFilter.IsNotNull())
            {
                return ResolveEqualityFilter(equalityFilter);
            }

            var operatorFilter = filter as OperatorFilter;
            if (operatorFilter.IsNotNull())
            {
                return ResolveOperatorFilter(operatorFilter);
            }

            var inFilter = filter as InFilter;
            if (inFilter.IsNotNull())
            {
                return Expression.In(inFilter.Property, inFilter.Values as ICollection);
            }

            var betweenFilter = filter as BetweenFilter;
            if (betweenFilter.IsNotNull())
            {
                return Expression.Between(betweenFilter.Property, betweenFilter.Lower, betweenFilter.Upper);
            }

            return null;
        }

        private static ICriterion ResolveOperatorFilter(OperatorFilter filter)
        {
            ICriterion criterion = null;
            switch (filter.Operator)
            {
                case OperatorType.And:
                    {
                        criterion = Expression.And(BuildCriterion(filter.Left), BuildCriterion(filter.Right));
                        break;
                    }
                case OperatorType.Or:
                    {
                        criterion = Expression.Or(BuildCriterion(filter.Left), BuildCriterion(filter.Right));
                        break;
                    }
            }
            return criterion;
        }

        private static ICriterion ResolveEqualityFilter(SimpleFilter filter)
        {
            ICriterion criterion = null;
            switch (filter.Operator)
            {
                case FilterType.Equal:
                    {
                        criterion = Expression.Eq(filter.Property, filter.Value);
                        break;
                    }
                case FilterType.NotEqual:
                    {
                        criterion = Expression.Not(Expression.Eq(filter.Property, filter.Value));
                        break;
                    }
                case FilterType.GreaterThan:
                    {
                        criterion = Expression.Gt(filter.Property, filter.Value);
                        break;
                    }
                case FilterType.GreaterThanOrEqualTo:
                    {
                        criterion = Expression.Ge(filter.Property, filter.Value);
                        break;
                    }
                case FilterType.LessThan:
                    {
                        criterion = Expression.Lt(filter.Property, filter.Value);
                        break;
                    }
                case FilterType.LessThanOrEqualTo:
                    {
                        criterion = Expression.Le(filter.Property, filter.Value);
                        break;
                    }
                case FilterType.Like:
                    {
                        criterion = Expression.Like(filter.Property, Convert.ToString(filter.Value, CultureInfo.InvariantCulture), MatchMode.Anywhere);
                        break;
                    }
                case FilterType.IsNull:
                    {
                        criterion = Expression.IsNull(filter.Property);
                        break;
                    }
            }
            return criterion;
        }
    }
}
