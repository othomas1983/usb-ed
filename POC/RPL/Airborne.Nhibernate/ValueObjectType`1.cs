﻿using System;
using System.Data;
using System.Diagnostics;
using System.Reflection;
using Airborne.Domain.ValueObjects;
using NHibernate;
using NHibernate.Engine;
using NHibernate.Type;
using NHibernate.UserTypes;

namespace Airborne.Nhibernate
{
    /// <summary>
    /// A custom implementation of an nhibernate type mapping. This type is to convert a string value
    /// from the database to a fully identified instance of <see cref="ValueObject"/>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ValueObjectType<T> : ICompositeUserType where T : ValueObject
    {
        #region [Static Members]

        private static readonly BindingFlags DefaultDiscoveryFlags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance;

        #endregion

        #region [Constructors]
        /// <summary>
        /// Initializes a new instance of the <see cref="ValueObjectType&lt;T&gt;"/> class.
        /// </summary>
        public ValueObjectType()
        {
            this.IsMutable = true;
            this.PropertyTypes = new IType[] { NHibernateUtil.String };
            this.PropertyNames = new string[] { "Code" };
        }
        #endregion

        #region [Properties]
        /// <summary>
        /// An array of property names to map to the &lt;T&gt;. This should correspond to the column structure left-to-right.
        /// </summary>
        public string[] PropertyNames
        {
            get;
            private set;
        }
        /// <summary>
        /// The <see cref="IType"/> correspondents to the properties being mapped. use <b>NHibernateUtil.<i>x</i></b> static types 
        /// for these.
        /// </summary>
        public IType[] PropertyTypes
        {
            get;
            private set;
        }
        /// <summary>
        /// The <see cref="Type"/> of the class being mapped.
        /// </summary>
        public Type ReturnedClass
        {
            get { return typeof(T); }
        }
        /// <summary>
        /// Obviously, Value Objects should be false.
        /// </summary>
        public bool IsMutable
        {
            get;
            private set;
        }
        #endregion

        #region [Public Methods]
        /// <summary>
        /// Gets the property value.
        /// </summary>
        public object GetPropertyValue(object component, int property)
        {
            PropertyInfo prop = ReturnedClass.GetProperty(PropertyNames[property], DefaultDiscoveryFlags);
            object target = component;
            //Accomodate static properties (get/set accessors can be static)
            if (prop.GetGetMethod(true).IsStatic)
            {
                //NULL the target for static property handling
                target = null;
            }

            return prop.GetValue(target, null);
        }

        /// <summary>
        /// Sets the property value.
        /// </summary>
        public void SetPropertyValue(object component, int property, object value)
        {
            if (!IsMutable)
            {
                throw new ArgumentException(ReturnedClass.UnderlyingSystemType.ToString() +
                    " is an immutable object.SetPropertyValue isn't supported.");
            }

            PropertyInfo prop = ReturnedClass.GetProperty(PropertyNames[property], DefaultDiscoveryFlags);
            object target = component;
            //Accomodate static properties (get/set accessors can be static)
            if (prop.GetGetMethod(true).IsStatic)
            {
                //NULL the target for static property handling
                target = null;
            }
            prop.SetValue(target, value, null);

        }

        /// <summary>
        /// Verifies the equality fo <param name="x"/> and <param name="y"/>
        /// </summary>
        public new bool Equals(object x, object y)
        {
            if (x == y) { return true; }
            if (x == null && y == null) { return true; }
            if (x == null || y == null) { return false; }
            return x.Equals(y);
        }


        /// <summary>
        /// Returns a null safe value
        /// </summary>
        public virtual object NullSafeGet(IDataReader dr, string[] names, ISessionImplementor session, object owner)
        {
            if (dr == null)
            {
                return null;
            }


            object[] vals = new object[PropertyNames.Length];
            for (int i = 0; i < PropertyNames.Length; i++)
            {
                vals[i] = PropertyTypes[i].NullSafeGet(dr, names[i], session, owner);

            }

            if (vals[0] != null)
            {
                T value = ValueObject.Repository.Find<T>(vals[0].ToString());
                return value;
            }
            else
            {
                return null;
            }

        }


        /// <summary>
        /// NHibernate API implementation fro a null safe set of the value from the database to the destination instance.
        /// </summary>
        public void NullSafeSet(IDbCommand cmd, object value, int index, ISessionImplementor session)
        {
            if (value == null)
            {
                PropertyTypes[0].NullSafeSet(cmd, null, index, session);
            }
            else
            {
                int propIndexer = index;

                object target = value;

                for (int i = 0; i < PropertyNames.Length; i++)
                {
                    PropertyInfo prop = ReturnedClass.GetProperty(PropertyNames[i]);
                    //Accomodate static properties (get/set accessors can be static)
                    if (prop.GetGetMethod(true).IsStatic)
                    {
                        target = null;
                    }

                    object propValue = prop.GetValue(target, null);

                    PropertyTypes[i].NullSafeSet(cmd, propValue, propIndexer, session);
                    propIndexer++;
                }
            }
        }

        /// <summary>
        /// Creates a deep copy of the object
        /// </summary>
        public virtual object DeepCopy(object value)
        {
            return value;
        }

        /// <summary>
        /// Disassembles the specified value.
        /// </summary>
        public object Disassemble(object value, ISessionImplementor session)
        {
            return DeepCopy(value);
        }

        /// <summary>
        /// Assembles the specified cache.
        /// </summary>
        public object Assemble(object cached, ISessionImplementor session, object owner)
        {
            return DeepCopy(cached);
        }


        /// <summary>
        /// Gets the hash code.
        /// </summary>
        public int GetHashCode(object x)
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Depricated
        /// </summary>
        public object Replace(object original, object target, ISessionImplementor session, object owner)
        {
            Debug.Fail("Called method");
            return null;
        }

        #endregion
    }
}
