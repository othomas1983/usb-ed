﻿CREATE TABLE [dbo].[tb_UserCourse] (
    [UserCourseID]    INT IDENTITY (1, 1) NOT NULL,
    [UserID]          INT NULL,
    [CourseID]        INT NULL,
    [QualificationID] INT NULL
);

