﻿CREATE TABLE [dbo].[tb_User] (
    [UserID]       INT           IDENTITY (1, 1) NOT NULL,
    [UserStatusID] INT           NOT NULL,
    [FirstName]    VARCHAR (50)  NOT NULL,
    [LastName]     VARCHAR (50)  NULL,
    [FieldID]      INT           NULL,
    [Comment]      VARCHAR (250) NULL
);



