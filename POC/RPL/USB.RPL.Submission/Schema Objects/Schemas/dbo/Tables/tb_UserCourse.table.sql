﻿CREATE TABLE [dbo].[tb_UserCourse] (
    [UserCourseID]  INT           IDENTITY (1, 1) NOT NULL,
    [UserID]        INT           NULL,
    [CourseName]    VARCHAR (100) NULL,
    [Qualification] VARCHAR (100) NULL
);

