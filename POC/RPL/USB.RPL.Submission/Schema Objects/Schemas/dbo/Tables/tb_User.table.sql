﻿CREATE TABLE [dbo].[tb_User] (
    [UserID]    INT          IDENTITY (1, 1) NOT NULL,
    [FirstName] VARCHAR (50) NOT NULL,
    [LastName]  VARCHAR (50) NULL,
    [FieldName] VARCHAR (50) NULL,
    [PrimaryID] INT          NULL
);

