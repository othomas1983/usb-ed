﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace USB.RPL.Domain.Queries
{
    [DataContract]
    public abstract partial class QuerySpecification
    {

    }

    [DataContract]
    public partial class SinglePropertyQuery<T> : QuerySpecification
    {
        #region Properties

        [DataMember]
        public T Value { get; set; }

        #endregion
    }

}
