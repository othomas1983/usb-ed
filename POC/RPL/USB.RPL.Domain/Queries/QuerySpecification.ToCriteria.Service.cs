﻿using System;
using Airborne.Domain.Repository.QueryObjects;

namespace USB.RPL.Domain.Queries
{

    public abstract partial class QuerySpecification
    {
        public abstract Criteria ToCriteria();
    }

    public partial class SinglePropertyQuery<T> : QuerySpecification
    {

        public override Criteria ToCriteria()
        {
            throw new NotImplementedException();
        }
    }
}