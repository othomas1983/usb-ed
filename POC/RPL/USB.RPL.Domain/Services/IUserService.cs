﻿using USB.RPL.Domain.User;
using Airborne.Notifications;
using System.Collections.Generic;

namespace USB.RPL.Domain.Services
{
    /// <summary>
    /// Defines the contractual obligation for value object based domain services.
    /// </summary>
    public interface IUserService
    {
        bool Authenticate(string email, string cellphone, string surname);

        UserProfile LoadUserProfile(long userId);
        UserProfile LoadUserProfile(string email, string cellphone, string surname);
        NotificationCollection SaveUserProfile(UserProfile profile);

        LearningType GetLearningType(int id);

        Course GetCourse(int id);
        IList<Course> Courses(int learningTypeId, int? disciplineId);
        NotificationCollection SaveCourse(Course course);

        Discipline GetDiscipline(int id);
        IList<Discipline> Disciplines(int learningTypeId);
        NotificationCollection SaveDiscipline(Discipline discipline);

        Institution GetInstitution(int id);
        IList<Institution> Institutions(int learningTypeId);
        NotificationCollection SaveInstitution(Institution institution);

        CourseStatus GetCourseStatus(int id);
        IList<CourseStatus> CourseStatuses();

        UserPaymentReference GetUserPaymentReference(string paymentReference);
        PaymentReference GetPaymentReference(string paymentReference);
       
    }
}
