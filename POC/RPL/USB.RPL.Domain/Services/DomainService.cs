﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Airborne;
using Airborne.Caching;
using Airborne.Domain;
using Airborne.Domain.Repository;
using Airborne.Domain.Repository.QueryObjects;
using Airborne.Domain.Rules;
using Airborne.Logging;
using Airborne.Nhibernate;
using Airborne.Notifications;
using USB.RPL.Domain.Validation;

namespace USB.RPL.Domain.Services
{
    /// <summary>
    /// Represents a base Domain service class that provides cross cutting concerns functionality. e.g Repository access and logging support 
    /// </summary>
    public abstract class DomainService
    {
        #region Properties

        /// <summary>
        /// Provides access to the RPLSystem
        /// </summary>
        public IRPLSystemFacade RPLSystem { get; set; }

        public IRepository Repository
        {
            get
            {
                return RPLSystem.Repository;
            }
        }
        
        #endregion

        #region Methods

        public NotificationCollection Validate(object instance, IValidationContext context)
        {
            return context.Validate(instance);
        }

        /// <summary>
        /// Convenience Method that provides validation and persistance functionality.
        /// A GenericValidationContext will be utilized to perform the validation.
        /// </summary>
        protected NotificationCollection SaveAggregate(IAggregate aggregate)
        {
            return SaveAggregate(aggregate, new GenericSaveContext(this.RPLSystem), true, false);
        }

        /// <summary>
        /// Convenience Method that provides validation and persistance functionality.
        /// The specified validation context will be utilized to perform the validation.
        /// </summary>
        protected NotificationCollection SaveAggregate(IAggregate aggregate, IValidationContext context)
        {
            return SaveAggregate(aggregate, context, true, false);
        }

        /// <summary>
        /// Convenience Method that provides validation and persistance functionality.
        /// A GenericValidationContext will be utilized to perform the validation.
        /// </summary>
        /// <param name="throwsException">Throws the exception if one is raised, to be handled manualy.</param>
        protected NotificationCollection SaveAggregate(IAggregate aggregate, bool throwsException)
        {
            return SaveAggregate(aggregate, new GenericSaveContext(this.RPLSystem), true, throwsException);
        }

        /// <summary>
        /// Convenience Method that provides validation and persistance functionality.
        /// </summary>
        /// <param name="aggregate">The instance to save.</param>
        /// <param name="context">The context to validate against. If Null a generic save context will be created.</param>
        /// <param name="isolated">Used to control the transaction scope of the save call. If true a new transaction scope will be created.</param>
        protected NotificationCollection SaveAggregate(IAggregate aggregate, IValidationContext context, bool isolated)
        {
            return SaveAggregate(aggregate, context ?? new GenericSaveContext(this.RPLSystem), isolated, false);
        }

        /// <summary>
        /// Convenience Method that provides validation and persistance functionality.
        /// </summary>
        /// <param name="aggregate">The instance to save.</param>
        /// <param name="context">The context to validate against. If Null the validation will be skipped.</param>
        /// <param name="isolated">Used to control the transaction scope of the save call. If true a new transaction scope will be created.</param>
        /// <param name="throwsException">Throws the exception if one is raised, to be handled manualy.</param>
        protected NotificationCollection SaveAggregate(IAggregate aggregate, IValidationContext context, bool isolated, bool throwsException)
        {
            Guard.ArgumentNotNull(aggregate, "aggregate");
            Guard.ArgumentNotNull(context, "context");

            var notifications = NotificationCollection.CreateEmpty();

            notifications = Validate(aggregate, context, throwsException);

            if (!notifications.HasErrors())
            {
                using (var tx = isolated ? Repository.BeginTransaction() : new NestedTransaction())
                {
                    try
                    {
                        Repository.Update(aggregate);
                        tx.Commit();
                        CacheAggregate(aggregate);
                    }
                    catch (Exception ex)
                    {
                        tx.Rollback();

                        if (RPLSystem.Logger.IsNotNull())
                        {
                            var msg = "Error saving aggregate {0}".FormatInvariantCulture(aggregate.ToString());
                            RPLSystem.Logger.Error(ex.AsLoggableMessage(msg));
                        }

                        if (throwsException)
                        {
                            throw;
                        }

                        notifications.AddMessage(Notification.Create("An error occured while saving", NotificationSeverity.Error));
                    }
                }
            }

            return notifications;
        }

        private NotificationCollection LogSaveError(string aggregate, Exception ex)
        {
            var notifications = NotificationCollection.CreateEmpty();
            if (RPLSystem.Logger.IsNotNull())
            {
                var msg = "Error saving aggregate {0}".FormatInvariantCulture(aggregate);
                RPLSystem.Logger.Error(ex.AsLoggableMessage(msg));
            }
            notifications.AddMessage(Notification.Create("An error occured while saving", NotificationSeverity.Error));
            return notifications;
        }

        private NotificationCollection Validate(IAggregate aggregate, IValidationContext context, bool throwsException)
        {
            var notifications = NotificationCollection.CreateEmpty();
            try
            {
                notifications += Validate(aggregate, context);
            }
            catch (Exception ex)
            {
                if (RPLSystem.Logger.IsNotNull())
                {
                    var msg = "Error validating aggregate {0}".FormatInvariantCulture(aggregate.ToString());
                    RPLSystem.Logger.Error(ex.AsLoggableMessage(msg));
                }
                if (throwsException)
                {
                    throw;
                }
                notifications.AddMessage(Notification.Create("An error occured while saving", NotificationSeverity.Error));
            }
            return notifications;
        }

        private void CacheAggregate(IAggregate aggregate)
        {
            var cachable = aggregate as ICachable;
            if (cachable.IsNotNull() && cachable.IsImplicitlyCached)
            {
                CacheInstance(cachable);
            }
        }

        /// <summary>
        /// Convenience Method that provides explicit cache functionality to domain services.
        /// </summary>
        protected void CacheInstance(ICachable instance)
        {
            if (instance.IsNotNull() && RPLSystem.Cache.IsNotNull())
            {
                RPLSystem.Cache.Add(instance.CacheKey, instance);
            }
        }

        /// <summary>
        /// Convenience Method that provides batch processing execution within a transaction scope.
        /// </summary>
        protected NotificationCollection Batch<T>(ITransaction tx, IEnumerable<T> source, Func<T, bool, NotificationCollection> action)
        {
            var notifications = NotificationCollection.CreateEmpty();

            try
            {
                foreach (var item in source)
                {
                    notifications += action.Invoke(item, false);
                }

                tx.Commit(!notifications.HasErrors(), true);
            }
            catch (Exception ex)
            {
                tx.Rollback();
                notifications.AddException(ex);
            }

            return notifications;
        }

        /// <summary>
        /// Convenience method that exposes and wraps the repository Find method.
        /// </summary>
        protected T Find<T>(Expression<Func<T, bool>> query)
        {
            return Repository.First<T>(query);
        }

        /// <summary>
        /// Convenience method that exposes and wraps the repository FindAll method.
        /// </summary>
        protected IEnumerable<T> FindAll<T>(Expression<Func<T, bool>> query)
        {
            return Repository.FindAll<T>(query);
        }

        /// <summary>
        /// Convenience method that exposes and wraps the repository FindAll method.
        /// </summary>
        protected IEnumerable<T> FindAll<T>(Criteria criteria)
        {
            return Repository.FindAll<T>(criteria);
        }

        /// <summary>
        /// Merges the aggregate with another aggregate already in the repository.
        /// </summary>
        protected T Merge<T>(T aggregate) where T : IAggregate
        {
            dynamic repo = Repository as dynamic;
            return repo.Merge(aggregate);
        }

        /// <summary>
        /// Convenience method that will merge the aggregate with a repository instance if it exists.
        /// </summary>
        protected T ResolveActual<T>(T instance, int id) where T : class, IAggregate
        {
            var actual = Repository.FindById<T>(id);
            return actual.IsNotNull() ? Merge<T>(instance) : instance;
        }
        
        #endregion

        #region Nested Types

        /// <summary>
        /// Committing inner transactions is ignored by the SQL Server Database Engine. The transaction is either committed or rolled back based on the action taken at the end of the outermost transaction. If the outer transaction is committed, the inner nested transactions are also committed. If the outer transaction is rolled back, then all inner transactions are also rolled back, regardless of whether or not the inner transactions were individually committed.
        /// </summary>
        internal sealed class NestedTransaction : ITransaction
        {
            #region ITransaction Members

            public bool IsActive
            {
                get { return true; }
            }

            public bool WasCommitted
            {
                get { return false; }
            }

            public bool WasRolledBack
            {
                get { return false; }
            }

            public void Commit() { }

            public void Rollback() { }

            #endregion

            #region IDisposable Members

            public void Dispose() { }

            #endregion
        }

        /// <summary>
        /// Convenience Method that provides validation and removal functionality.
        /// A GenericValidationContext will be utilized to perform the validation.
        /// </summary>
        /// <param name="aggregate">The instance to validate and remove</param>
        /// <returns>NotificationCollection</returns>
        protected NotificationCollection DeleteAggregate(IAggregate aggregate)
        {
            return DeleteAggregate(aggregate, new GenericDeleteContext(this.RPLSystem));
        }

        /// <summary>
        /// Convenience Method that provides validation and removal functionality.
        /// The specified validation context will be utilized to perform the validation.
        /// </summary>
        /// <param name="aggregate">The instance to validate and remove</param>
        /// <param name="context">Validation Context</param>
        /// <returns>NotificationCollection</returns>
        protected NotificationCollection DeleteAggregate(IAggregate aggregate, IValidationContext context)
        {

            Guard.ArgumentNotNull(aggregate, "aggregate");
            Guard.ArgumentNotNull(context, "context");

            var notifications = NotificationCollection.CreateEmpty();

            try
            {
                notifications += Validate(aggregate, context);
            }
            catch (Exception ex)
            {
                if (RPLSystem.Logger.IsNotNull())
                {
                    var msg = "Error validating aggregate {0}".FormatInvariantCulture(aggregate.ToString());
                    RPLSystem.Logger.Error(ex.AsLoggableMessage(msg));
                }
                notifications.AddMessage(Notification.Create("An error occured while deleting", NotificationSeverity.Error));
            }

            if (!notifications.HasErrors())
            {
                using (var tx = Repository.BeginTransaction())
                {
                    try
                    {
                        Repository.Remove(aggregate);
                        tx.Commit();
                    }
                    catch (Exception ex)
                    {
                        tx.Rollback();
                        notifications.AddMessage(Notification.Create("An error occured while deleting", NotificationSeverity.Error));
                    }
                }
            }

            return notifications;
        }

        #endregion
    }
}
