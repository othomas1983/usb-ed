﻿using USB.RPL.Domain.User;
using System;
using Airborne.Notifications;
using System.Collections.Generic;
using System.Linq;

namespace USB.RPL.Domain.Services
{
    public class UserService : DomainService, IUserService
    {
        public bool Authenticate(string email, string cellphone, string surname)
        {
            return Repository.First<UserProfile>(up => up.Email == email && up.Cellphone == cellphone && up.LastName == surname).IsNotNull();
        }

        public UserProfile LoadUserProfile(long userId)
        {
            return Repository.First<UserProfile>(up => up.Id == userId);
        }

        public UserProfile LoadUserProfile(string email, string cellphone, string surname)
        {
            return Repository.First<UserProfile>(up => up.Email == email && up.Cellphone == cellphone && up.LastName == surname);
        }

        public NotificationCollection SaveUserProfile(UserProfile profile)
        {
            return SaveAggregate(profile);
        }

        public LearningType GetLearningType(int id)
        {
            return Repository.FindById<LearningType>(id);
        }

        public Course GetCourse(int id)
        {
            return Repository.FindById<Course>(id);
        }

        public IList<Course> Courses(int learningTypeId, int? disciplineId)
        {
            return Repository.FindAll<Course>(c => c.LearningTypeId == learningTypeId && c.DisciplineId == disciplineId).ToList();
        }

        public NotificationCollection SaveCourse(Course course)
        {
            return SaveAggregate(course);
        }

        public Discipline GetDiscipline(int id)
        {
            return Repository.FindById<Discipline>(id);
        }

        public IList<Discipline> Disciplines(int learningTypeId)
        {
            return Repository.FindAll<Discipline>(d => d.LearningTypeId == learningTypeId).ToList();
        }

        public NotificationCollection SaveDiscipline(Discipline discipline)
        {
            return SaveAggregate(discipline);
        }

        public Institution GetInstitution(int id)
        {
            return Repository.FindById<Institution>(id);
        }

        public IList<Institution> Institutions(int learningTypeId)
        {
            return Repository.FindAll<Institution>(i => i.LearningTypeId == learningTypeId).ToList();
        }

        public NotificationCollection SaveInstitution(Institution institution)
        {
            return SaveAggregate(institution);
        }

        public CourseStatus GetCourseStatus(int id)
        {
            return Repository.FindById<CourseStatus>(id);
        }

        public IList<CourseStatus> CourseStatuses()
        {
            return Repository.FindAll<CourseStatus>().ToList();
        }

        public UserPaymentReference GetUserPaymentReference(string paymentReference)
        {
            return Repository.First<UserPaymentReference>(pr => pr.PaymentReference.Reference == paymentReference);
        }

        public PaymentReference GetPaymentReference(string paymentReference)
        {
            return Repository.First<PaymentReference>(pr => pr.Reference == paymentReference);
        }
    }
}
