﻿using System;
using System.Collections.Generic;

namespace USB.RPL.Domain
{
    public static class IIdentifiableExtensions
    {
        /// <summary>
        /// Gets the Transient state of the object (No identifier value is considered transient) 
        /// </summary>
        public static bool IsTransient<T>(this IIdentifiable<T> instance)
        {
            return instance.Id.IsNull();
        }

        /// <summary>
        /// Gets the Transient state of the object (No identifier value is considered transient). allows specific ids to be considered Transient.  
        /// </summary>
        public static bool IsTransient<T>(this IIdentifiable<T> instance, params T[] transientIds)
        {
            return transientIds.Contains(id => id.Equals(instance.Id)) || instance.Id.IsNull();
        }


    }
}
