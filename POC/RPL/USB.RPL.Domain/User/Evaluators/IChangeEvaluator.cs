﻿
namespace USB.RPL.Domain.User.Evaluators
{
    public interface IChangeEvaluator<T>
    {
        void SnapShot(T instance);

        bool HasChanges(T result);
    }
}
