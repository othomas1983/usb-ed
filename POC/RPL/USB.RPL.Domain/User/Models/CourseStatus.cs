﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Airborne.Domain;

namespace USB.RPL.Domain.User
{
    public class CourseStatus : IAggregate
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
