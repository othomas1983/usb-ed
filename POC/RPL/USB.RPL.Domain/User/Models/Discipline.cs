﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Airborne.Domain;

namespace USB.RPL.Domain.User
{
    public class Discipline : IAggregate
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsVetted { get; set; }
        public int LearningTypeId { get; set; }
    }
}
