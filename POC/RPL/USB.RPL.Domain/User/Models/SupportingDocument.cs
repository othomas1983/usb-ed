﻿
namespace USB.RPL.Domain.User
{
    public class SupportingDocument
    {
        public int Id { get; set; }
        public byte[] Document { get; set; }
        public string Extension { get; set; }
        public string Name { get; set; }

        public int UserId { get; set; }
    }
}
