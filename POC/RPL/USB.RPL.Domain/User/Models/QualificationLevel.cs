﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace USB.RPL.Domain.User
{
    public enum QualificationLevel
    {
        Junior = 1,
        Intermediate = 2,
        Senior = 3
    }
}
