﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace USB.RPL.Domain.User
{
    public class LearningType
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
