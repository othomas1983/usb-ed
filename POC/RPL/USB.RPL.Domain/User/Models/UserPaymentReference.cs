﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace USB.RPL.Domain.User
{
    public class UserPaymentReference
    {
        public int Id { get; set; }
        public PaymentReference PaymentReference { get; set; }
        public int UserId { get; set; }
        public int TimesUsed { get; set; }
    }
}
