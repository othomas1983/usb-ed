﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace USB.RPL.Domain.User
{
    public class PaymentReference
    {
        public int Id { get; set; }
        public string Reference { get; set; }
    }
}
