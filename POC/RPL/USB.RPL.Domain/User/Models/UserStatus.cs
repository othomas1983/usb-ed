﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace USB.RPL.Domain.User
{
    public enum UserStatus
    {
        Undefined = 0,
        Pending = 1,
        Confirmed = 2,
        Copied = 3,
        Update = 4
    }
}
