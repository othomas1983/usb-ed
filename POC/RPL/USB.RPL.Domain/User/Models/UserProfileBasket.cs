﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace USB.RPL.Domain.User
{
    public class UserProfileBasket
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public LearningType LearningType { get; set; }
        public Course Course { get; set; }
        public Discipline Discipline { get; set; }
        public Institution Institution { get; set; }
        public CourseStatus CourseStatus { get; set; }
        public string HighestGrade { get; set; }
        public string HighestGradeSchool { get; set; }
        public string InstitutionAddress { get; set; }
        public string DegreeLevel { get; set; }
        public string ReferenceContactDetails { get; set; }
        public string SecurityIndustry { get; set; }
        public string EmployedBy { get; set; }
        public DateTime? DateOfEmployment { get; set; }
        public string EmployedAs { get; set; }
        public DateTime? DateOfCompletion { get; set; }
        public string CourseReference { get; set; }
        public string CourseReferencePerson { get; set; }

        public byte[] Document { get; set; }
        public string Extension { get; set; }
        public string DocumentName { get; set; }
        
    }
}
