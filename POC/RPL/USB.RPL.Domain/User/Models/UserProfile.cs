﻿using System;
using System.Collections.Generic;
using System.Linq;
using Airborne.Domain;

namespace USB.RPL.Domain.User
{

    public class UserProfile : IAggregate
    {
        public UserProfile()
        {
            Basket = new List<UserProfileBasket>();
        }
        #region Properties

        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Title { get; set; }

        public string Email { get; set; }

        public string Cellphone { get; set; }

        public string IDForceNumber { get; set; }

        public string Language { get; set; }

        public string Race { get; set; }

        public string Gender { get; set; }

        public string MaritalStatus { get; set; }

        public string Location { get; set; }

        public string Province { get; set; }

        public string RPLDiscipline { get; set; }

        public int? PrimaryId { get; set; }

        public UserStatus Status { get; set; }

        public IList<UserProfileBasket> Basket {get; set;}

        public IList<SupportingDocument> OtherDocuments { get; set; }

        public IList<UserPaymentReference> PaymentReferences { get; set; }

        public string Comment { get; set; }

        #endregion

    }

}
