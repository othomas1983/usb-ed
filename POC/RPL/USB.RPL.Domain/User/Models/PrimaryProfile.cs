﻿using System;
using System.Collections.Generic;
using System.Linq;
using Airborne.Domain;

namespace USB.RPL.Domain.User
{

    public class PrimaryProfile : IAggregate
    {
        public PrimaryProfile()
        {
            //Courses = new List<UserCourse>();
            Documents = new List<SupportingDocument>();
        }

        #region Properties

        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public UserStatus Status { get; set; }

        //public FieldofService Field { get; set; }

        //public IList<UserCourse> Courses { get; set;}

        public IList<SupportingDocument> Documents { get; set; }

        public string Comment { get; set; }

        #endregion

    }

}
