﻿namespace USB.RPL.Domain.Validation
{
    /// <summary>
    /// Represents a generic delete context
    /// </summary>
    public class GenericDeleteContext : ValidationContext
    {
        public const string Key = "Delete";

        public GenericDeleteContext(IRPLSystemFacade system)
            : base(system)
        {

        }

        public override bool IsForContext(string key)
        {
            return Key.ToUpperInvariant().Equals(key.ToUpperInvariant());
        }
    }
}
