﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace USB.RPL.Domain.Validation
{
    /// <summary>
    /// Represents a generic validation context that can be utilized when contextual validation is not required. 
    /// </summary>
    public class GenericValidationContext : ValidationContext
    {

        public GenericValidationContext(IRPLSystemFacade system)
            : base(system)
        {

        }

        public override bool IsForContext(string key)
        {
            return true;
        }
    }
}