﻿namespace USB.RPL.Domain.Validation
{
    /// <summary>
    /// Represents a generic save context
    /// </summary>
    public class GenericSaveContext : ValidationContext
    {
        public const string Key = "Save";

        public GenericSaveContext(IRPLSystemFacade system)
            : base(system)
        {

        }

        public override bool IsForContext(string key)
        {
            return Key.ToUpperInvariant().Equals(key.ToUpperInvariant());
        }
    }
}
