﻿using System.Collections.Generic;
using Airborne.Domain.Rules;
using Airborne.Domain.Rules.Resolution;
using Airborne.Notifications;

namespace USB.RPL.Domain.Validation
{
    /// <summary>
    /// The RPL base validation context that provides access to the system instance, rules resolution and validation.
    /// </summary>
    public abstract class ValidationContext : IValidationContext
    {
        #region Properties

        public IRPLSystemFacade System { get; private set; }

        #endregion

        #region Locals

        IResolutionStrategy resolver;

        #endregion

        #region Constructors

        public ValidationContext(IRPLSystemFacade system)
        {
            System = system;
            resolver = new DeclarativeResolution();
        }

        #endregion

        #region IValidationContext Members

        public abstract bool IsForContext(string key);

        public virtual NotificationCollection Validate(object instance)
        {
            return resolver.Validate(instance, this);
        }

        #endregion

        #region Methods

        public IEnumerable<IRule> ResolveRules(object instance)
        {
            return resolver.ResolveRules(instance, this);
        }

        #endregion
    }
}