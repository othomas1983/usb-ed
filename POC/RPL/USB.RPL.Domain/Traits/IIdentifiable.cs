﻿namespace USB.RPL.Domain
{
    public interface IIdentifiable<T>
    {
        T Id { get; }
    }
}