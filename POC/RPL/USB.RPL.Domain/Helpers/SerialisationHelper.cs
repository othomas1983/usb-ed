﻿using System.Text;
using System.IO;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace USB.RPL.Domain.Helpers
{
    public static class SerialisationHelper
    {
        /// <summary>
        /// Serialise an instance of <typeparamref name="T"/> to an xml representation.
        /// </summary>
        public static string Serialise<T>(T instance, IEnumerable<System.Type> KnownTypes = null)
        {
            using (var memoryStream = new MemoryStream())
            {
                var serialiser = KnownTypes == null ? new DataContractSerializer(typeof(T)) : new DataContractSerializer(typeof(T), KnownTypes);
                serialiser.WriteObject(memoryStream, instance);
                memoryStream.Flush();
                memoryStream.Position = 0;

                var reader = new StreamReader(memoryStream);
                return reader.ReadToEnd();
            }
        }

        public static T Deserialise<T>(string input, IEnumerable<System.Type> KnownTypes = null)
        {
            using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(input)))
            {
                var serialiser = KnownTypes == null ? new DataContractSerializer(typeof(T)) : new DataContractSerializer(typeof(T), KnownTypes);
                return (T)serialiser.ReadObject(stream);
            }
        }
    }
}
