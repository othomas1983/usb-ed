﻿using System;

namespace USB.RPL.Domain
{
    //TODO: Rename to something like ICacheProvider ? 
    //TODO: Move to Airborne.Framework project add default .Net cache profvider and Appfabic implementation
    public interface IEntityCache
    {
        void Add(string key, object value);

        void Save<T>(object key, T value) where T : class;

        T Get<T>(object key) where T : class;

        T Get<T>(object key, Func<T> loader) where T : class;

        void Evict<T>(object key);

        void Clear();
    }
}