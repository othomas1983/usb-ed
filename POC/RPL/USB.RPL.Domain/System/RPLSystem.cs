﻿using System;
using Airborne.Domain.Repository;
using Airborne.Logging;
using USB.RPL.Domain.Services;
using USB.RPL.Domain.User;
using Microsoft.Practices.Unity;

namespace USB.RPL.Domain
{
    /// <summary>
    /// Concrete Implementation of the <see cref="IRPLSystemFacade"/> type
    /// </summary>
    internal sealed class RPLSystem : IRPLSystemFacade
    {
        #region Locals

        internal IUnityContainer container;

        #endregion

        #region Constructors

        [InjectionConstructor]
        public RPLSystem(IUnityContainer container)
        {
            this.container = container;
        }

        #endregion

        #region Properties

        [Dependency]
        public IRepository Repository { get; set; }

        public UserProfile GetUser(int userId)
        {
            if (Cache.IsNotNull())
            {
                return Cache.Get<UserProfile>(userId);
            }

            return null;
        }

        [OptionalDependency("SessionCache")]
        public IEntityCache Cache { get; set; }

        [OptionalDependency("GlobalCache")]
        public IEntityCache Global { get; set; }

        [Dependency]
        public ILogger Logger { get; set; }

        #endregion

        #region Methods

        public TService GetService<TService>()
        {
            var service = container.Resolve<TService>(new DependencyOverride<IRPLSystemFacade>(this));

            if (service is DomainService)
            {
                var domainService = service as DomainService;
                domainService.RPLSystem = this;
            }

            return service;
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            // container.Dispose();
            this.Repository.Dispose();
        }

        #endregion

    }
}