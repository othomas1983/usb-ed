﻿using System;
using System.Configuration;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;

namespace USB.RPL.Domain
{
    ///<summary>
    /// An <see cref="IRPLSystemFacade"/> Factory that uses the Unity to resolve and compose dependancies.
    /// </summary>
    public class RPLSystemFactory
    {
        #region Locals

        private static object containerLock = new object();

        internal static IUnityContainer container;

        #endregion
        
        #region Methods

        /// <summary>
        /// Factory method for the creation of an <see cref="IRPLSystemFacade" /> instance
        /// </summary>
        public static IRPLSystemFacade CreateSystem()
        {
            var unityContainer = container ?? ConfigureContainer();

            return CreateSystem(unityContainer);
        }

        public static IRPLSystemFacade CreateSystem(IUnityContainer container)
        {
            return container.Resolve<IRPLSystemFacade>();
        }

        internal static IUnityContainer ConfigureContainer()
        {
            return ConfigureContainer(false);
        }

        internal static IUnityContainer ConfigureContainer(bool forceRefresh)
        {
            lock (containerLock)
            {
                if (container.IsNull() || forceRefresh)
                {
                    container = new UnityContainer();
                    var section =  (UnityConfigurationSection)ConfigurationManager.GetSection("unity");
                    section.Configure(container);
                }
            }

            return container;
        }

        #endregion
    }
}