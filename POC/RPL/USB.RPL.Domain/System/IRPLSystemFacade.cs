﻿using System;
using Airborne.Domain.Repository;
using Airborne.Logging;
using USB.RPL.Domain.User;

namespace USB.RPL.Domain
{
    /// <summary>
    /// Provides a convenient an easy to use interface into the RPL application domain.
    /// </summary>
    public interface IRPLSystemFacade : IDisposable
    {

        /// <summary>
        /// Provides access to the domain's repository implementation
        /// </summary>
        IRepository Repository { get; }

        /// <summary>
        /// Loads the specified member from cache
        /// </summary>
        /// <param name="membershipNumber"></param>
        /// <returns></returns>
        UserProfile GetUser(int userId);

        /// <summary>
        /// Provides access to the domain's cache implementation
        /// </summary>
        IEntityCache Cache { get; set; }

        /// <summary>
        /// Provides access to the domain's global cache implementation
        /// </summary>
        IEntityCache Global { get; set; }


        /// <summary>
        /// Provides access to the domain's logging implementation
        /// </summary>
        ILogger Logger { get; set; }

        
        /// <summary>
        /// Locates and returns a service of type TService
        /// </summary>
        TService GetService<TService>();

    }
}