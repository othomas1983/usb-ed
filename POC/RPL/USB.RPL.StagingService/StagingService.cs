﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;

namespace USB.RPL.StagingService
{
    public partial class StagingService : ServiceBase
    {
        private StagingSchedular Processor;
        public StagingService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            Processor = new StagingSchedular();
            Processor.UnhandledError += ProcessorUnhandledError;
            Processor.ProcessorStopped += ProcessorProcessorStopped;
            Processor.Start();
        }

        public void ProcessorProcessorStopped(object sender, EventArgs e)
        {
            if (Processor.IsNotNull())
            {
                Processor.Dispose();
            }
        }

        private void ProcessorUnhandledError(object sender, EventArgs e)
        {
            this.Stop();
        }

        protected override void OnStop()
        {
            Processor.Stop();
        }
    }
}
