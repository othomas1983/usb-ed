﻿using System;
using System.Configuration;

namespace USB.RPL.StagingService
{
    public class ScheduleAlerts : ConfigurationElementCollection
    {
        public ScheduleAlerts()
        {
        }

        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.AddRemoveClearMap;
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new AlertConfigElement();
        }

        protected override Object GetElementKey(ConfigurationElement element)
        {
            return ((AlertConfigElement)element).Address;
        }

        public AlertConfigElement this[int index]
        {
            get
            {
                return (AlertConfigElement)BaseGet(index);
            }
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }

        new public AlertConfigElement this[string address]
        {
            get
            {
                return (AlertConfigElement)BaseGet(address);
            }
        }

        public int IndexOf(AlertConfigElement url)
        {
            return BaseIndexOf(url);
        }

        public void Add(AlertConfigElement url)
        {
            BaseAdd(url);
        }
        protected override void BaseAdd(ConfigurationElement element)
        {
            BaseAdd(element, false);
        }

        public void Remove(AlertConfigElement url)
        {
            if (BaseIndexOf(url) >= 0)
            {
                BaseRemove(url.Address);
            }
        }

        public void RemoveAt(int index)
        {
            BaseRemoveAt(index);
        }

        public void Remove(string name)
        {
            BaseRemove(name);
        }

        public void Clear()
        {
            BaseClear();
        }

    }
}
