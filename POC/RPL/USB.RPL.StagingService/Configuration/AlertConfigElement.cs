﻿using System.Configuration;

namespace USB.RPL.StagingService
{
    public class AlertConfigElement : ConfigurationElement
    {
        public AlertConfigElement(string address)
        {
            Address = address;  
        }

        public AlertConfigElement()
        { }

        /// <summary>
        /// Gets or sets a unique address for the strategy.
        /// </summary>
        [ConfigurationProperty(Descriptors.Address, IsRequired = true, IsKey = true)]
        public string Address
        {
            get
            {
                return (string)this[Descriptors.Address];
            }
            set
            {
                this[Descriptors.Address] = value;
            }
        }

        /// <summary>
        /// Descriptors for the configuration elements
        /// </summary>
        public static class Descriptors
        {
            public const string Address = "address";
        }
    }
}
