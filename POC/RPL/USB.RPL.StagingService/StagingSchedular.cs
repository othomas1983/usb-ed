﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Net.Mail;
using System.Timers;
using Airborne.Domain.Repository;
using Airborne.Notifications;
using USB.RPL.Domain;
using log4net;
using Airborne.Nhibernate;
using Airborne.Nhibernate.Domain.Repository;
using USB.RPL.Domain.User;

namespace USB.RPL.StagingService
{
    public class StagingSchedular : IDisposable
    {
        #region Properties

        public bool IsBusy { get; private set; }

        #endregion

        #region Locals

        private static ILog Logger = LogManager.GetLogger("StagingSchedule");

        private IRPLSystemFacade RPLSystem;

        IConfigurationProvider configuration;

        Timer scheduleTimer;

        bool serviceStopped;

        object mailLock = new object();

        public IRepository StagingRepository { get; private set; }

        public IRepository PrimaryRepository { get; private set; }

        #endregion

        #region Constructors

        public StagingSchedular()
        {
            RPLSystem = RPLSystemFactory.CreateSystem();
            configuration = RPLSystem.GetService<IConfigurationProvider>();
        }

        #endregion

        #region Methods


        /// <summary>
        /// Starts the timer loop that will initiate processing
        /// based on the polling interval set in configuration
        /// </summary>
        public void Start()
        {
            Logger.InfoFormat("Service starting {0}", DateTime.Now);

            if (scheduleTimer.IsNotNull())
            {
                Logger.Info("Dispose previous timer");
                scheduleTimer.Dispose();
            }

            scheduleTimer = ConfigureTimer();
            serviceStopped = false;
            IsBusy = true;

            scheduleTimer.Start();
        }

        public void Stop()
        {
            Logger.Error("Indication recieved to stop the service");
            serviceStopped = true;
        }

        private void InitiateProcessing()
        {
            SetupRepositories();
            try
            {
                TransferConfirmedProfilesToPrimary();
            }
            catch (Exception ex)
            {
                Logger.Info("Error transfering profiles to the primary database...");
                Logger.Error(ex.Message, ex);
            }
            try
            {
                TransferProfilesToUpdateToStaging();
            }
            catch (Exception ex)
            {
                Logger.Info("Error transfering profiles to the staging database...");
                Logger.Error(ex.Message, ex);
            }


            if (serviceStopped)
            {
                RaiseProcessorStopped();
            }
        }

        private void TransferConfirmedProfilesToPrimary()
        {
            Logger.Info("Finding records to process in staging...");
            var profiles = StagingRepository.FindAll<UserProfile>(up => up.Status == UserStatus.Confirmed);

            Logger.InfoFormat("Found {0} confirmed records in staging to process", profiles.Count());

            //foreach (var profile in profiles)
            //{
            //    PrimaryProfile primaryProfile = null;
            //    if (profile.PrimaryId.HasValue)
            //    {
            //        primaryProfile = PrimaryRepository.FindById<PrimaryProfile>(profile.PrimaryId.Value);
                    
            //        primaryProfile.FirstName = profile.FirstName;
            //        primaryProfile.LastName = profile.LastName;
            //        primaryProfile.Status = UserStatus.Pending;
            //        primaryProfile.Field = profile.Field.Copy();
            //        primaryProfile.Documents.Clear();
            //        profile.Documents.ForEach(d => { primaryProfile.Documents.Add(new SupportingDocument() { Document = d.Document, Name = d.Name, Extension = d.Extension }); });

            //        foreach (var course in profile.Courses.ToList())
            //        {
            //            var originalCourse = primaryProfile.Courses.FirstOrDefault(c => c.Course.Id == course.Course.Id);

            //            if (originalCourse.IsNull())
            //            {
            //                var newCourse = new UserCourse()
            //                {
            //                    Course = course.Course,
            //                    Qualification = course.Qualification,
            //                    UserId = course.UserId
            //                };
            //                primaryProfile.Courses.Add(newCourse);
            //            }
            //            if (originalCourse.IsNotNull())
            //            {
            //                originalCourse.Course = course.Course;
            //                originalCourse.Qualification = course.Qualification;
            //            }
            //        }
            //        var primaryProfileCourses = primaryProfile.Courses.ToList();
            //        foreach (var course in primaryProfileCourses)
            //        {
            //            var hasCourse = profile.Courses.FirstOrDefault(c => c.Course.Id == course.Course.Id).IsNotNull();

            //            if (!hasCourse)
            //            {
            //                primaryProfile.Courses.RemoveAll(c => c.Course.Id == course.Course.Id);
            //            }
            //        }

            //    }
            //    else
            //    {
            //        primaryProfile = new PrimaryProfile()
            //        {
            //            FirstName = profile.FirstName,
            //            LastName = profile.LastName,
            //            Status = UserStatus.Pending,
            //            Courses = profile.Courses.ToList(),
            //            Documents = profile.Documents.ToList(),
            //            Field = profile.Field.Copy(),
            //        };
                    
            //        primaryProfile.Courses.ForEach(c => { c.Id = 0; c.UserId = 0; });
            //        primaryProfile.Documents.ForEach(c => { c.Id = 0; c.UserId = 0; });
            //    }
                
                

            //    using (var tx = PrimaryRepository.BeginTransaction())
            //    {
            //        try
            //        {
            //            PrimaryRepository.Update(primaryProfile);
            //            tx.Commit();
            //            Logger.InfoFormat("Primary database updated with profile '{0}'", primaryProfile.Id);
            //        }
            //        catch (Exception ex)
            //        {
            //            tx.Rollback();
            //            return;
            //        }
            //    }

            //    using (var tx = StagingRepository.BeginTransaction())
            //    {
            //        try
            //        {
            //            StagingRepository.Remove(profile);
            //            tx.Commit();
            //            Logger.InfoFormat("Removed profile '{0}' from the staging database", profile.Id);
            //        }
            //        catch (Exception ex)
            //        {
            //            tx.Rollback();
            //        }
            //    }
                
            //}
        }

        private void TransferProfilesToUpdateToStaging()
        {
            Logger.Info("Finding records to process in primary...");
            var profiles = PrimaryRepository.FindAll<PrimaryProfile>(up => up.Status == UserStatus.Update);

            Logger.InfoFormat("Found {0} records to move to staging", profiles.Count());

            //foreach (var primaryProfile in profiles)
            //{
            //    var stagingProfile = new UserProfile()
            //    {
            //        PrimaryId = primaryProfile.Id,
            //        FirstName = primaryProfile.FirstName,
            //        LastName = primaryProfile.LastName,
            //        Status = UserStatus.Pending,
            //        Field = primaryProfile.Field.Copy(),
            //    };

            //    primaryProfile.Status = UserStatus.Copied;
            //    primaryProfile.Courses.ForEach(c => { stagingProfile.Courses.Add(new UserCourse() { Course = c.Course, Qualification = c.Qualification }); });
            //    primaryProfile.Documents.ForEach(d => { stagingProfile.Documents.Add(new SupportingDocument() { Document = d.Document, Name = d.Name, Extension = d.Extension }); });

            //    using (var tx = StagingRepository.BeginTransaction())
            //    {
            //        try
            //        {
            //            StagingRepository.Update(stagingProfile);
            //            tx.Commit();
            //            Logger.InfoFormat("Staging database updated with profile '{0}'", stagingProfile.Id);
            //        }
            //        catch (Exception ex)
            //        {
            //            tx.Rollback();
            //            return;
            //        }
            //    }
            //    using (var tx = PrimaryRepository.BeginTransaction())
            //    {
            //        try
            //        {
            //            PrimaryRepository.Update(primaryProfile);
            //            tx.Commit();
            //            Logger.InfoFormat("Primary database profile '{0}' marked as copied", primaryProfile.Id);
            //        }
            //        catch (Exception ex)
            //        {
            //            tx.Rollback();
            //        }
            //    }
            //}
        }

        private void SetupRepositories()
        {
            var primarySession = SessionFactory.Instance.CreateSession(ConfigurationManager.AppSettings["PrimaryConfig"]);
            PrimaryRepository = new NhibernateRepository(primarySession);
            RPLSystem = RPLSystemFactory.CreateSystem();
            StagingRepository = RPLSystem.Repository;
        }

        private Timer ConfigureTimer()
        {
            var timer = new Timer();
            timer.Interval = configuration.PollingInterval * 60 * 1000;
            Logger.InfoFormat("Interval set to {0} minutes", configuration.PollingInterval);
            timer.Elapsed += (s, e) => ScheduleTimerElapsed();

            return timer;
        }

        private void ScheduleTimerElapsed()
        {
            scheduleTimer.Stop();
            Logger.Info("Initiate processing");
            Stopwatch timer = Stopwatch.StartNew();
            try
            {
                InitiateProcessing();
                timer.Stop();
                Logger.InfoFormat("Processing took : {0} seconds", timer.Elapsed.TotalSeconds);

                if (!serviceStopped)
                {
                    scheduleTimer.Start();
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("Error: {0}", ex.ToString());
                Logger.ErrorFormat("The polling may have stopped even though the service is running. Restart the service. Exception occured after {0} seconds of processing. ", timer.Elapsed.TotalSeconds, ex.ToString());
                SendErrorMail(ex.ToString());
                RaiseUnhandledError();
                RaiseProcessorStopped();
            }
        }

        private void SendErrorMail(string body)
        {
            try
            {
                Logger.InfoFormat("Sending error alerts : {0}", body);
                var smtpServer = configuration.SmtpServer;
                if (smtpServer.IsNotNullOrEmpty())
                {
                    var client = new SmtpClient(smtpServer);

                    var mailMessage = new MailMessage()
                    {
                        From = new MailAddress(configuration.AdminEmailAddress)
                    };

                    Logger.InfoFormat("Adding {0} to be alerted", configuration.AlertEmailAddresses.AsSeparatedString(",", new Converter<string, string>(cs => cs)));
                    mailMessage.To.AddRange(configuration.AlertEmailAddresses);
                    mailMessage.Subject = "USB: Email Error";
                    mailMessage.Body = "Error occured at {0} \r\n {1}".FormatCurrentCulture(DateTime.Now, body);

                    client.Send(mailMessage);
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("Error sending alert email : {0}", ex);
            }
        }

        #region UnhandledError

        public event EventHandler UnhandledError;

        protected virtual void OnUnhandledError(EventArgs args)
        {

            if (UnhandledError.IsNotNull())
            {
                UnhandledError(this, args);
            }
        }

        private void RaiseUnhandledError()
        {
            OnUnhandledError(EventArgs.Empty);
        }

        #endregion

        #region ProcessorStopped

        public event EventHandler ProcessorStopped;

        protected virtual void OnProcessorStopped(EventArgs args)
        {
            if (ProcessorStopped.IsNotNull())
            {
                ProcessorStopped(this, args);
            }
        }

        private void RaiseProcessorStopped()
        {
            OnProcessorStopped(EventArgs.Empty);
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                scheduleTimer.Dispose();
            }
        }

        #endregion

        #endregion

    }
}
