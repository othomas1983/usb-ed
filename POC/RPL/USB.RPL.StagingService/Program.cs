﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using log4net;

namespace USB.RPL.StagingService
{
    static class Program
    {

        private static ILog Logger = LogManager.GetLogger("StagingSchedule");

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {

            log4net.Config.XmlConfigurator.Configure();

            var interactive = args.FirstOrDefault(arg => string.Compare(arg, "/console", StringComparison.OrdinalIgnoreCase) == 0);

            if (interactive.IsNullOrEmpty())
            {
                Logger.Info("Start process.....");
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[] 
			    { 
				    new StagingService() 
			    };
                ServiceBase.Run(ServicesToRun);
            }
            else
            {
                var schedular = new StagingSchedular();
                schedular.Start();

                Console.WriteLine("Starting in interactive mode : press 'return' to quit");
                Console.ReadLine();
                Console.WriteLine("Stopping processing...");

                while (schedular.IsBusy)
                {
                    Thread.Sleep(1000);
                }
            }
        }
    }
}
