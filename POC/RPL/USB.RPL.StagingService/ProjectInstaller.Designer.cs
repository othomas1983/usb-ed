﻿namespace USB.RPL.StagingService
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.StagingServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.StagingServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // StagingServiceProcessInstaller
            // 
            this.StagingServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.StagingServiceProcessInstaller.Password = null;
            this.StagingServiceProcessInstaller.Username = null;
            // 
            // StagingServiceInstaller
            // 
            this.StagingServiceInstaller.DisplayName = "Staging Service - RPL";
            this.StagingServiceInstaller.ServiceName = "StagingService";
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.StagingServiceProcessInstaller,
            this.StagingServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller StagingServiceProcessInstaller;
        private System.ServiceProcess.ServiceInstaller StagingServiceInstaller;
    }
}