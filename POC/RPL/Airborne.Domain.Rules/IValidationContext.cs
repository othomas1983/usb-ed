﻿using Airborne.Notifications;

namespace Airborne.Domain.Rules
{
    public interface IValidationContext
    {
        /// <summary>
        /// Cheks wheter a given key is linked to this context
        /// </summary>
        bool IsForContext(string key);

        /// <summary>
        /// Validates an object instance
        /// </summary>
        NotificationCollection Validate(object instance); 
    }
}
