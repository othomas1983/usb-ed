﻿using System.Collections;

namespace Airborne.Domain.Rules
{
    public interface IAllowedRule : IRule
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        IEnumerable GetAllowedValues();
    }
}
