﻿namespace Airborne.Domain.Rules
{
    /// <summary>
    /// Contract for range rule
    /// </summary>
    public interface IRangeRule : IRule
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        object GetMinimum();

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        object GetMaximum();
    }
}