﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Airborne.Notifications;

namespace Airborne.Domain.Rules
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public sealed class RuleContextAttribute : Attribute
    {
        #region Constructors

        public RuleContextAttribute(string key)
            : this(key, NotificationSeverity.Error)
        {
        }

        public RuleContextAttribute(string key, NotificationSeverity severity)
        {
            Key = key;
            Severity = severity;
        }

        #endregion

        #region Properties

        public string Key { get; private set; }

        public NotificationSeverity Severity { get; private set; }

        #endregion
    }
}
