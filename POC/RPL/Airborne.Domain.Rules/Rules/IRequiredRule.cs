﻿namespace Airborne.Domain.Rules
{
    public interface IRequiredRule : IRule
    {
        bool IsRequired();
    }
}