using System;
using System.Collections.Generic;
using System.Reflection;
using Airborne.Notifications;

namespace Airborne.Domain.Rules
{
    public interface IRule
    {
        /// <summary>
        /// The severity for the context
        /// </summary>
        NotificationSeverity Severity { get; set; }

        /// <summary>
        /// The context in which the rule is running
        /// </summary>
        IValidationContext Context { get; set; }

        /// <summary>
        /// The owner for the rule
        /// </summary>
        object Owner { get; set; }

        /// <summary>
        /// The name of the rule
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Execute the rule
        /// </summary>
        NotificationCollection Validate();

        /// <summary>
        /// Indicates whether the rule is for a property
        /// </summary>
        bool IsPropertyRule { get; set; }

        /// <summary>
        /// Specifies the property name if the rule is for a property
        /// </summary>
        string PropertyName { get; set; }
    }
}