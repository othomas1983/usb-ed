﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Airborne.Notifications;
using Airborne.Domain.Rules.Properties;

namespace Airborne.Domain.Rules
{
    public abstract class DateRangeRule<T> : CustomRule<T>, IRangeRule where T : class
    {
        #region IRange Members

        public object GetMinimum()
        {
            return OnGetMinimum();
        }

        public object GetMaximum()
        {
            return OnGetMaximum();
        }

        #endregion

        #region Virtual Members

        protected override NotificationCollection OnValidate()
        {
            var notification = NotificationCollection.CreateEmpty();

            var propertyValue = GetPropertyValue() as DateTime?;

            if (propertyValue.IsNotNull())
            {
                var minimum = OnGetMinimum();

                var maximum = OnGetMaximum();

                if (propertyValue > maximum || propertyValue < minimum)
                {
                    notification.AddMessage(OnCreateMessage());
                }
            }

            return notification;
        }

        protected virtual DateTime OnGetMaximum()
        {
            return DateTime.MaxValue;
        }

        protected virtual DateTime OnGetMinimum()
        {
            return DateTime.MinValue;
        }

        protected virtual RuleNotification OnCreateMessage()
        {
            return CreateMessage(Resources.RangeError, PropertyName, OnGetMinimum().ToLongDateString(), OnGetMaximum().ToLongDateString());
        }

        #endregion
    }
}
