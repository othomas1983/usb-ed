﻿using System;
using Airborne.Domain.Rules.Properties;
using Airborne.Notifications;

namespace Airborne.Domain.Rules
{
    public abstract class IntRangeRule<T> : CustomRule<T>, IRangeRule where T : class
    {
        #region IRange Members

        public object GetMaximum()
        {
            return OnGetMaximum();
        }

        public object GetMinimum()
        {
            return OnGetMinimum();
        }

        #endregion

        #region Virtual Members

        protected override NotificationCollection OnValidate()
        {
            var notification = NotificationCollection.CreateEmpty();

            var propertyValue = GetPropertyValue() as int?;

            if (propertyValue.IsNotNull())
            {
                var minimum = GetMinimum() as int?;
                var maximum = GetMaximum() as int?;

                if (propertyValue < minimum || propertyValue > maximum)
                {
                    notification.AddMessage(OnCreateMessage());
                }
            }

            return notification;
        }

        protected virtual int OnGetMinimum()
        {
            return int.MinValue;
        }

        protected virtual int OnGetMaximum()
        {
            return int.MaxValue;
        }

        protected RuleNotification OnCreateMessage()
        {
            return CreateMessage(Resources.RangeError, PropertyName, OnGetMaximum(), OnGetMinimum());
        }

        #endregion
    }
}
