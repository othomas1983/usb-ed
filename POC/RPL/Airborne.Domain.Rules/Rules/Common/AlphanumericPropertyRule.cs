﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Airborne.Notifications;
using Airborne.Domain.Rules.Properties;

namespace Airborne.Domain.Rules
{
    public abstract class AlphanumericPropertyRule<T> : CustomRule<T> where T : class
    {
        #region Virtual Methods

        protected override NotificationCollection OnValidate()
        {
            var notification = NotificationCollection.CreateEmpty();

            var propertyValue = GetPropertyValue() as string;

            if (propertyValue.IsNotNullOrEmpty())
            {
                if (!propertyValue.ValidateString(AllowedCharacter.Alpha, AllowedCharacter.Numeric))
                {
                    notification.AddMessage(OnCreateMessage());
                }
            }

            return notification;
        }

        protected virtual RuleNotification OnCreateMessage()
        {
            return CreateMessage(Resources.MustBeAlphaNumeric, PropertyName);
        }

        #endregion
    }
}
