using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Airborne.Notifications;

namespace Airborne.Domain.Rules
{
    public static class ValidationHelper
    {
        public static RuleNotification CreateMessage(IRule rule, object instance, string propertyName, string message, string tag, params object[] values)
        {
            Guard.ArgumentNotNull(rule, "rule");

            if (values.IsNotNull())
            {
                message = message.FormatInvariantCulture(values);
            }

            return new RuleNotification(message)
            {
                Code = rule.Name,
                PropertyOwner = instance,
                Severity = rule.Severity,
                PropertyName = propertyName,
                Tag = tag
            };
            
        }

        public static NotificationCollection ValidateNullValue(object propertyOwner, string propertyName, object propertyValue, string message, params object[] values)
        {
            var notification = NotificationCollection.CreateEmpty();

            if (propertyValue.IsNull())
            {
                if (values.IsNotNull())
                {
                    message = message.FormatInvariantCulture(values);
                }

                notification.AddMessage(
                        new RuleNotification(message)
                            {
                                PropertyName = propertyName,
                                PropertyOwner = propertyOwner,
                                Severity = NotificationSeverity.Error
                            });
            }

            return notification;
        }

        public static NotificationCollection ValidateEmptyValue(object propertyOwner, string propertyName, string propertyValue, string message, params object[] values)
        {
            var notification = NotificationCollection.CreateEmpty();

            if (propertyValue.IsNullOrEmpty())
            {
                if (values.IsNotNull())
                {
                    message = message.FormatInvariantCulture(values);
                }

                notification.AddMessage(
                        new RuleNotification(message)
                        {
                            PropertyName = propertyName,
                            PropertyOwner = propertyOwner,
                            Severity = NotificationSeverity.Error
                        });
            }

            return notification;
        }
    }
}
