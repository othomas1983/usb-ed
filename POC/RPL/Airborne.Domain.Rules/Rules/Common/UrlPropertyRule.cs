﻿using System;
using Airborne.Domain.Rules.Properties;
using Airborne.Notifications;

namespace Airborne.Domain.Rules
{
    public abstract class UrlPropertyRule<T> : CustomRule<T> where T : class
    {
        #region Virtual Methods

        protected override NotificationCollection OnValidate()
        {
            var notification = NotificationCollection.CreateEmpty();

            var propertyValue = GetPropertyValue() as string;

            if (propertyValue.IsNotNullOrEmpty())
            {
                Uri uri = null;

                try
                {
                    uri = new Uri(propertyValue);
                }
                catch(UriFormatException)
                {
                }

                if (uri.IsNull() || uri.Scheme.IsNullOrEmpty() || uri.Authority.IsNullOrEmpty())
                {
                    notification.AddMessage(OnCreateMessage());
                }
            }

            return notification;
        }

        protected virtual RuleNotification OnCreateMessage()
        {
            return CreateMessage(Resources.InvalidUrl, PropertyName);
        }

        #endregion
    }
}
