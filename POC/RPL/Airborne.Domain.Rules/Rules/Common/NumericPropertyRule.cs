﻿using System;
using Airborne.Domain.Rules.Properties;
using Airborne.Notifications;

namespace Airborne.Domain.Rules
{
    public abstract class NumericPropertyRule<T> : CustomRule<T> where T : class
    {
        #region Virtual Methods

        protected override NotificationCollection OnValidate()
        {
            var notification = NotificationCollection.CreateEmpty();

            var propertyValue = GetPropertyValue() as string;

            if (propertyValue.IsNotNullOrEmpty())
            {
                if (!propertyValue.ValidateString(AllowedCharacter.Numeric))
                {
                    notification.AddMessage(OnCreateMessage());
                }
            }

            return notification;
        }

        protected virtual RuleNotification OnCreateMessage()
        {
            return CreateMessage(Resources.NotNumeric, PropertyName);
        }

        #endregion
    }
}
