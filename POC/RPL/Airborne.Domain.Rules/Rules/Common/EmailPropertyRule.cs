﻿using System;
using Airborne.Domain.Rules.Properties;
using Airborne.Notifications;

namespace Airborne.Domain.Rules
{
    public abstract class EmailPropertyRule<T> : CustomRule<T> where T : class
    {
        #region Virtual Methods

        protected override NotificationCollection OnValidate()
        {
            var notification = NotificationCollection.CreateEmpty();

            var propertyValue = GetPropertyValue() as string;

            if (propertyValue.IsNotNullOrEmpty())
            {
                if (!propertyValue.ValidateEmailAddress())
                {
                    notification.AddMessage(OnCreateMessage());
                }
            }

            return notification;
        }

        protected virtual RuleNotification OnCreateMessage()
        {
            return CreateMessage(Resources.InvalidEmail, PropertyName);
        }

        #endregion

    }
}
