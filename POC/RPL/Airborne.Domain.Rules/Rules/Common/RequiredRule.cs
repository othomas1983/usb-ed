﻿using System;
using Airborne.Domain.Rules.Properties;
using Airborne.Notifications;

namespace Airborne.Domain.Rules
{
    public abstract class RequiredRule<T> : CustomRule<T>, IRequiredRule where T : class
    {
        #region IRequiredRule Members

        public bool IsRequired()
        {
            return OnIsRequired();
        }

        #endregion

        #region Virtual Members

        protected override NotificationCollection OnValidate()
        {
            var notification = NotificationCollection.CreateEmpty();
            
            if (IsRequired())
            {
                var propertyValue = GetPropertyValue();

                if (propertyValue.IsNull())
                {
                    notification.AddMessage(OnCreateMessage());
                }
                else
                {
                    var propertyType = propertyValue.GetType();

                    if (propertyType.Equals(typeof(string)) && propertyValue.ToString().IsNullOrEmpty())
                    {
                        notification.AddMessage(OnCreateMessage());
                    }
                    else if (propertyType.Equals(typeof(byte[])) && ((byte[])propertyValue).Length == 0)
                    {
                        notification.AddMessage(OnCreateMessage());
                    }
                }
            }

            return notification;
        }

        protected virtual bool OnIsRequired()
        {
            return true;
        }

        protected virtual RuleNotification OnCreateMessage()
        {
            return CreateMessage(Resources.IsRequired, PropertyName);
        }

        #endregion
    }
}
