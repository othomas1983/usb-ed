﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using Airborne.Notifications;
using Airborne.Domain.Rules.Properties;

namespace Airborne.Domain.Rules
{
    public abstract class AllowedRule<T> : CustomRule<T>, IAllowedRule where T : class
    {
        #region IAllowedRule Members

        public IEnumerable GetAllowedValues()
        {
            return OnGetAllowedValues();
        }

        #endregion

        #region Virtual Methods

        protected override NotificationCollection OnValidate()
        {
            var notification = NotificationCollection.CreateEmpty();

            var propertyValue = GetPropertyValue();

            if (propertyValue.IsNotNull())
            {
                if (!OnContainsPropertyValue(GetAllowedValues(), propertyValue))
                {
                    notification.AddMessage(OnCreateMessage());
                }
            }
            
            return notification;
        }

        protected virtual RuleNotification OnCreateMessage()
        {
            return CreateMessage(Resources.NotAllowed, PropertyName);
        }

        protected abstract IEnumerable OnGetAllowedValues();

        protected virtual bool OnContainsPropertyValue(IEnumerable allowedValues, object propertyValue)
        {
            return allowedValues.OfType<object>().Contains(propertyValue);
        }

        #endregion
    }
}
