﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Airborne.Notifications;

namespace Airborne.Domain.Rules
{
    public abstract class CustomRule<T> : IRule where T : class
    {
        #region Properties

        protected T Instance
        {
            get
            {
                return (T)Owner;
            }
        }

        #endregion

        #region IRule Members

        public IValidationContext Context { get; set; }

        public object Owner { get; set; }

        public bool IsPropertyRule { get; set; }

        public string PropertyName { get; set; }

        public string Name { get; set; }

        public NotificationCollection Validate()
        {
            return OnValidate();
        }

        public NotificationSeverity Severity { get; set; }

        #endregion

        #region Methods

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        protected object GetPropertyValue()
        {
            return Instance.GetType().GetProperty(PropertyName).GetValue(Instance, null);
        }

        protected RuleNotification CreateMessage(string message, params object[] values)
        {
            return ValidationHelper.CreateMessage(this, Instance, PropertyName, message, GenerateTag(Instance), values);
        }


        #endregion

        #region Virtual Methods

        protected virtual NotificationCollection OnValidate()
        {
            return NotificationCollection.CreateEmpty();
        }

        protected virtual string GenerateTag(object instance)
        {
            var taggable = instance as ITaggable;
            if (taggable.IsNotNull())
            {
                return taggable.GenerateTag();
            }
            return string.Empty;
        }

        #endregion
    }
}
