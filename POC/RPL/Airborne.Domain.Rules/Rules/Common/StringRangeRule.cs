﻿using System;
using Airborne.Domain.Rules.Properties;
using Airborne.Notifications;

namespace Airborne.Domain.Rules
{
    public abstract class StringRangeRule<T> : CustomRule<T>, IRangeRule where T : class
    {
        #region IRange Members

        public virtual object GetMaximum()
        {
            return OnGetMaximum();
        }

        public virtual object GetMinimum()
        {
            return OnGetMinimum();
        }

        #endregion

        #region Virtual Methods

        protected virtual int OnGetMaximum()
        {
            return 255;
        }

        protected virtual int OnGetMinimum()
        {
            return 1;
        }

        protected override NotificationCollection OnValidate()
        {
            var notification = NotificationCollection.CreateEmpty();

            var propertyValue = GetPropertyValue() as string;

            if (propertyValue.IsNotNullOrEmpty())
            {
                var minimum = OnGetMinimum();
                var maximum = OnGetMaximum();

                if (propertyValue.Length < minimum || propertyValue.Length > maximum)
                {
                    notification.AddMessage(OnCreateMessage());
                }
            }

            return notification;
        }

        protected RuleNotification OnCreateMessage()
        {
            return CreateMessage(Resources.RangeError, PropertyName, OnGetMinimum(), OnGetMaximum());
        }

        #endregion
    }
}
