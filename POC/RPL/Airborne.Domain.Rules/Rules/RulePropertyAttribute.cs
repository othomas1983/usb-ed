﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;

namespace Airborne.Domain.Rules
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public sealed class RulePropertyAttribute : Attribute
    {
        #region Constructors

        public RulePropertyAttribute(string propertyName)
        {
            PropertyName = propertyName;
        }

        public RulePropertyAttribute(string propertyName, Type owner)
        {
            PropertyName = propertyName;
            Owner = owner;
        }

        #endregion

        #region Properties

        public string PropertyName { get; private set; }

        public Type Owner { get; private set; }

        #endregion
    }
}