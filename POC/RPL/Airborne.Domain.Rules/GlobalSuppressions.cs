﻿// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.
//
// To add a suppression to this file, right-click the message in the 
// Error List, point to "Suppress Message(s)", and click 
// "In Project Suppression File".
// You do not need to add suppressions to this file manually.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member", Target = "Airborne.Domain.Rules.ValidationHelper.#CreateMessage(Airborne.Domain.Rules.IRule,System.Object,System.String,System.String,System.String,System.Object[])")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "1", Scope = "member", Target = "Airborne.Domain.Rules.Resolution.DeclarativeResolution.#CreateRules(Airborne.Domain.Rules.IValidationContext,System.Object,System.Type)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "2", Scope = "member", Target = "Airborne.Domain.Rules.Resolution.DeclarativeResolution.#CreateRules(Airborne.Domain.Rules.IValidationContext,System.Object,System.Type)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member", Target = "Airborne.Domain.Rules.Resolution.DeclarativeResolution.#ResolveRules(System.Object,Airborne.Domain.Rules.IValidationContext)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Scope = "member", Target = "Airborne.Domain.Rules.Resolution.DeclarativeResolution.#ResolveRules(System.Object,Airborne.Domain.Rules.IValidationContext,System.Reflection.Assembly)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "2", Scope = "member", Target = "Airborne.Domain.Rules.Resolution.DeclarativeResolution.#ResolveRules(System.Object,Airborne.Domain.Rules.IValidationContext,System.Reflection.Assembly)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "1", Scope = "member", Target = "System.StringExtensions.#ContainsString(System.String,System.AllowedCharacter[])")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "1", Scope = "member", Target = "System.StringExtensions.#StripCharacters(System.String,System.Collections.ObjectModel.ReadOnlyCollection`1<System.AllowedCharacter>)")]
