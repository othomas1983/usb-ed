﻿using System.Collections.Generic;
using Airborne.Notifications;
using System.Reflection;

namespace Airborne.Domain.Rules.Resolution
{
    /// <summary>
    /// When implemented will be used to result rules.
    /// </summary>
    public interface IResolutionStrategy
    {
        /// <summary>
        /// Resolve all rules for the validatable (class &amp; property)
        /// </summary>
        IEnumerable<IRule> ResolveRules(object instance, IValidationContext context, Assembly assembly);

        /// <summary>
        /// Resolve all rules for the validatable (class &amp; property)
        /// </summary>
        IEnumerable<IRule> ResolveRules(object instance, IValidationContext context);

        /// <summary>
        /// Validates an object
        /// </summary>
        NotificationCollection Validate(object instance, IValidationContext context);
    }
}
