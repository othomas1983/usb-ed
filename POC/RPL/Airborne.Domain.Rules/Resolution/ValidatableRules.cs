﻿using System.Collections.Generic;
using System.Reflection;

namespace Airborne.Domain.Rules.Resolution
{
    /// <summary>
    /// Contains a list of rule per assembly
    /// </summary>
    internal class ValidatableRules
    {
        private IDictionary<Assembly, IDictionary<IValidationContext, IList<IRule>>> ruleList = new Dictionary<Assembly, IDictionary<IValidationContext, IList<IRule>>>();

        #region Properties
        /// <summary>
        /// List of rules per assembly
        /// </summary>
        public IDictionary<Assembly, IDictionary<IValidationContext, IList<IRule>>> RuleList
        {
            get
            {
                return ruleList;
            }
        }

        #endregion
    }
}
