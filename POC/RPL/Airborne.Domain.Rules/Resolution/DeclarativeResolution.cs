using System;
using System.Linq;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using Airborne.Notifications;

namespace Airborne.Domain.Rules.Resolution
{
    public class DeclarativeResolution : IResolutionStrategy
    {
        private readonly IDictionary<Type, ValidatableRules> validatableList = new Dictionary<Type, ValidatableRules>();

        #region Methods

        public virtual NotificationCollection Validate(object instance, IValidationContext context)
        {
            var rules = ResolveRules(instance, context);

            var requiredRules = new List<IRequiredRule>();

            var notification = NotificationCollection.CreateEmpty();

            foreach (var requiredRule in rules.OfType<IRequiredRule>())
            {
                var ruleNotification = requiredRule.Validate();
                
                notification += ruleNotification;

                if (ruleNotification.HasErrors())
                {
                    requiredRules.Add(requiredRule);
                }
            }

            foreach (var rule in rules.Where(r => !(r is IRequiredRule)))
            {
                if (!requiredRules.Contains(r => r.PropertyName == rule.PropertyName && r.Owner == rule.Owner))
                {
                    notification += rule.Validate();
                }
            }

            return notification;
        }

        public static IEnumerable<IRule> CreateRules(IValidationContext context, object owner, Type ruleType)
        {
            Guard.ArgumentNotNull(owner, "owner");
            Guard.ArgumentNotNull(ruleType, "ruleType");


            var validatableType = owner.GetType();
            var rules = new List<IRule>();

            var ruleAttribute = ruleType.GetCustomAttributes(typeof(RuleAttribute), false).FirstOrDefault() as RuleAttribute;

            Guard.ArgumentNotNull(ruleAttribute, "No rule attribute found for {0}".FormatCurrentCulture(ruleType.FullName));

            if (ruleAttribute.EntityType.IsNull() || 
                ruleAttribute.EntityType.Equals(validatableType) || 
                validatableType.IsSubclassOf(ruleAttribute.EntityType))
            {
                var contextList = new List<RuleContextAttribute>();
                ruleType.GetCustomAttributes(typeof(RuleContextAttribute), false).ForEach(rc => contextList.Add((RuleContextAttribute)rc));
                var contextEntry = contextList.FirstOrDefault(c => context.IsForContext(c.Key));

                if (contextList.Count == 0 || contextEntry.IsNotNull())
                {
                    var rulePropertyAttributes = ruleType.GetCustomAttributes(typeof(RulePropertyAttribute), false);

                    if (rulePropertyAttributes.Length > 0)
                    {
                        foreach (var rulePropertyAttributeItem in rulePropertyAttributes)
                        {
                            var rulePropertyAttribute = (RulePropertyAttribute)rulePropertyAttributeItem;

                            if (rulePropertyAttribute.Owner.IsNull() || rulePropertyAttribute.Owner == validatableType)
                            {
                                rules.Add(CreateRule(ruleType, context, contextEntry, owner, rulePropertyAttribute));
                            }
                        }
                    }
                    else
                    {
                        rules.Add(CreateRule(ruleType, context, contextEntry, owner));
                    }
                }
            }

            return rules;
        }

        /// <summary>
        /// Resolve rules using reflection and attributes on the validatable class and rule classes base on the given assembly
        /// </summary>
        /// <param name="validatable"></param>
        /// <param name="context"></param>
        /// <param name="assembly">The assembly to load the rules out</param>
        /// <returns></returns>
        public virtual IEnumerable<IRule> ResolveRules(object instance, IValidationContext context, Assembly assembly)
        {
            Guard.ArgumentNotNull(instance, "instance");
            Guard.ArgumentNotNull(assembly, "assembly");

            var validatableType = instance.GetType();

            if (!validatableList.ContainsKey(validatableType))
            {
                validatableList.Add(validatableType, new ValidatableRules());
            }

            var validatableRules = validatableList[validatableType];

            if (!validatableRules.RuleList.ContainsKey(assembly))
            {
                validatableRules.RuleList.Add(assembly, new Dictionary<IValidationContext, IList<IRule>>());
            }

            var ruleList = validatableRules.RuleList[assembly];

            if (ruleList.ContainsKey(context))
            {
                return ruleList[context];
            }
            else
            {
                ruleList.Add(context, new List<IRule>());
            }

            var rules = (List<IRule>)ruleList[context];

            var iRuleType = typeof(IRule);

            foreach (var ruleClass in assembly.GetTypes().Where(t => iRuleType.IsAssignableFrom(t) && !t.IsAbstract))
            {
                rules.AddRange(CreateRules(context, instance, ruleClass));
            }

            return rules.AsEnumerable();
        }

        /// <summary>
        /// Resolve rules using reflection and attributes on the validatable class and rule classes
        /// </summary>
        public virtual IEnumerable<IRule> ResolveRules(object instance, IValidationContext context)
        {
            Guard.ArgumentNotNull(instance, "instance");
            return ResolveRules(instance, context, instance.GetType().Assembly);
        }

        #endregion

        #region Private Methods

        private static IRule CreateRule(Type ruleType, IValidationContext validationContext, RuleContextAttribute contextType, object owner)
        {
            var rule = (IRule)Activator.CreateInstance(ruleType);
            rule.Context = validationContext;
            rule.Owner = owner;
            rule.Severity = contextType.IsNull() ? NotificationSeverity.Error : contextType.Severity;
            rule.Name = ruleType.Name;
            return rule;
        }

        private static IRule CreateRule(Type ruleType, IValidationContext validationContext, RuleContextAttribute contextType, object owner, RulePropertyAttribute propertyType)
        {
            var rule = CreateRule(ruleType, validationContext, contextType, owner);
            rule.IsPropertyRule = true;
            rule.PropertyName = propertyType.PropertyName;
            return rule;
        }

        #endregion
    }
}