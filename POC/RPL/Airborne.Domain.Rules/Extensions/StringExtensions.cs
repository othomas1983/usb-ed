﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using Airborne;

namespace System
{
    public static class StringExtensions
    {
        #region Methods

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "1")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1021:AvoidOutParameters", MessageId = "2#")]
        public static bool ValidateString(this string value, IList<AllowedCharacter> allowedCharacters, out string invalidCharacters)
        {
            if (string.IsNullOrEmpty(value))
            {
                invalidCharacters = "Value required";
                return false;
            }

            var formattedValue = value;

            foreach (AllowedCharacter type in allowedCharacters)
            {
                formattedValue = Regex.Replace(formattedValue, ConvertToRegularExpression(type), string.Empty, RegexOptions.IgnoreCase);
            }

            invalidCharacters = formattedValue;
            return invalidCharacters.IsNullOrEmpty();
        }

        public static bool ValidateString(this string value, params AllowedCharacter[] allowedCharacters)
        {
            var invalidCharacters = string.Empty;
            return ValidateString(value, allowedCharacters, out invalidCharacters);
        }

        public static bool ContainsString(this string value, params AllowedCharacter[] characters)
        {
            Guard.ArgumentNotNull(characters, "characters");

            foreach (AllowedCharacter type in characters)
            {
                if (Regex.Match(value, ConvertToRegularExpression(type), RegexOptions.IgnoreCase).Success)
                {
                    return true;
                }
            }

            return false;
        }

        public static string StripCharacters(this string value, ReadOnlyCollection<AllowedCharacter> allowedCharacters)
        {
            Guard.ArgumentNotEmpty(value, "OriginalValue");
            var formattedValue = value;

            foreach (int item in Enum.GetValues(typeof(AllowedCharacter)))
            {
                var allowedCharacter = (AllowedCharacter)item;
                if (!allowedCharacters.Contains(allowedCharacter))
                {
                    formattedValue = Regex.Replace(formattedValue, ConvertToRegularExpression(allowedCharacter), string.Empty, RegexOptions.IgnoreCase);
                }
            }
            return formattedValue;
        }

        public static bool StringContainsNumerics(this string value)
        {
            var allowedCharacters = new List<AllowedCharacter>() { AllowedCharacter.Numeric }.AsReadOnly();

            return StripCharacters(value, allowedCharacters).IsNotNullOrEmpty();
        }

        public static bool StringIsValidLength(this string value, int minimumLength, int maximumLength)
        {
            if (!string.IsNullOrEmpty(value))
            {
                if (value.Length >= minimumLength && value.Length <= maximumLength)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool ValidateEmailAddress(this string value)
        {
            if (value.IsNullOrEmpty())
            {
                return false;
            }
            var emailExpression = @"^(\w+([-+.']\w+)*)@([\w-]+\.)+[\w-]+$";
            return Regex.Match(value, emailExpression).Success;
        }

        public static bool ValidateUrl(this string value)
        {
            string http = @"http://";
            if (!string.IsNullOrEmpty(value))
            {
                if (value.StartsWith(http, StringComparison.CurrentCulture))
                {
                    var webAddressExpression = @"^(([\w]+:)?\/\/)?(([\d\w]|%[a-fA-f\d]{2,2})+(:([\d\w]|%[a-fA-f\d]{2,2})+)?@)?([\d\w][-\d\w]{0,253}[\d\w]\.)+[\w]{2,4}(:[\d]+)?(\/([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)*(\?(&?([-+_~.\d\w]|%[a-fA-f\d]{2,2})=?)*)?(#([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)?$";

                    return Regex.Match(value, webAddressExpression).Success;
                }
            }
            return false;
        }

        #endregion

        #region Private Methods

        private static string ConvertToRegularExpression(AllowedCharacter allowedCharacter)
        {
            var regExpression = string.Empty;

            switch (allowedCharacter)
            {
                case AllowedCharacter.Apostrophe:
                    regExpression = @"[']";
                    break;
                case AllowedCharacter.Alpha:
                    regExpression = @"[a-zA-Z]";
                    break;
                case AllowedCharacter.Asterisk:
                    regExpression = @"[\*]";
                    break;
                case AllowedCharacter.Backslash:
                    regExpression = @"[\\]";
                    break;
                case AllowedCharacter.ForwardSlash:
                    regExpression = @"[\/]";
                    break;
                case AllowedCharacter.Comma:
                    regExpression = @"[,]";
                    break;
                case AllowedCharacter.Equals:
                    regExpression = @"[\=]";
                    break;
                case AllowedCharacter.Percentage:
                    regExpression = @"[\%]";
                    break;
                case AllowedCharacter.Semicolon:
                    regExpression = @"[;]";
                    break;
                case AllowedCharacter.Dot:
                    regExpression = @"[.]";
                    break;
                case AllowedCharacter.Dash:
                    regExpression = @"[\-]";
                    break;
                case AllowedCharacter.Hash:
                    regExpression = @"[\#]";
                    break;
                case AllowedCharacter.Plus:
                    regExpression = @"[\+]";
                    break;
                case AllowedCharacter.RoundBrackets:
                    regExpression = @"[\(\)]";
                    break;
                case AllowedCharacter.Space:
                    regExpression = @"[\s]";
                    break;
                case AllowedCharacter.Numeric:
                    regExpression = @"[0-9]";
                    break;
                default:
                    break;
            }

            return regExpression;
        }

        #endregion
    }
}
