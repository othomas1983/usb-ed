﻿
namespace System
{
    public enum AllowedCharacter
    {
        Alpha,
        Apostrophe,
        Asterisk,
        Backslash,
        Comma,
        Dot,
        Dash,
        Equals,
        ForwardSlash,
        Hash,
        Numeric,
        Percentage,
        Plus,
        RoundBrackets,
        Semicolon,
        Space
    }
}
