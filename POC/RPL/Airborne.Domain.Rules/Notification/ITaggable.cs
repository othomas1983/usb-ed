﻿
namespace Airborne.Domain.Rules
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Taggable")]
    public interface ITaggable
    {
        string GenerateTag();
    }
}
