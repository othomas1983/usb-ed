﻿using System.Runtime.Serialization;
using Airborne.Notifications;
using System;

namespace Airborne.Domain.Rules
{
    [DataContract]
    public class RuleNotification : Notification
    {
        #region Constructors

        public RuleNotification(string text) : base(text) { }

        #endregion

        #region Properties

        public object PropertyOwner { get; set; }

        [DataMember]
        public string PropertyName { get; set; }

        public virtual string GeneratePropertyHint()
        {
            return "{0}.{1}".FormatInvariantCulture(this.PropertyOwner.GetType().Name, this.PropertyName);
        }

        #endregion
    }
}
