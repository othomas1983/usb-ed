using System;
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;

[assembly : AssemblyCompany("Airborne Consulting")]
[assembly: AssemblyProduct("Datacash RPL")]
[assembly : AssemblyCopyright("Copyright � 2008")]
[assembly : ComVisible(false)]

[assembly : AssemblyVersion("1.0.0.0")]
[assembly : AssemblyFileVersion("1.0.0.0")]
[assembly : NeutralResourcesLanguage("en-US")]
