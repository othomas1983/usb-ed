﻿
namespace System
{

    /// <summary>
    /// Additional extensions on double
    /// </summary>
    public static class DoubleExtensions
    {
        /// <summary>
        /// Returns wether the value falls within the specified start and end range
        /// </summary>
        public static bool Between(this double instance, double start, double end)
        {
            return instance >= start && instance <= end;
        }

        /// <summary>
        /// Returns wether the value falls within the specified start and end range. allows control of wether the start and end must be included in the logical check.
        /// </summary>
        public static bool Between(this double instance, double start, double end, bool includeStart, bool includeEnd)
        {
            var startExp = new Func<bool>(() => { return instance >= start; });
            var endExp = new Func<bool>(() => { return instance <= end; });

            if (!includeStart)
            {
                startExp = () => { return instance > start; };
            }

            if (!includeEnd)
            {
                endExp = () => { return instance < end; };
            }

            return (startExp.Invoke() && endExp.Invoke());
        }
    }

}