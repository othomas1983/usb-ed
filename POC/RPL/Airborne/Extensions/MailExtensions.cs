﻿using System.Collections.Generic;

namespace System.Net.Mail
{
    public static class MailExtensions
    {
        public static void AddRange(this MailAddressCollection value, IEnumerable<string> items)
        {
            foreach (var address in items)
            {
                value.Add(address);
            }
        }
    }
}