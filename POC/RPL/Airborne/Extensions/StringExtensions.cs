﻿using System;
using System.Globalization;
using System.Text;
using System.Linq;
using System.IO;
using System.Xml;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using System.Collections.Generic;
using System.Diagnostics;

namespace System
{
    /// <summary>
    /// Additional extensions on <see cref="System.String"/>
    /// </summary>
    [DebuggerStepThrough]
    public static class StringExtensions
    {
        /// <summary>
        /// Limits this string to a given length and applies an elipse
        /// </summary>
        public static string Elipse(this string input, int length)
        {
            return input.Length > length ? input.Substring(0, length) + "..." : input;
        }


        /// <summary>
        /// Performs a concat join.
        /// </summary>
        public static string ConcatJoin(this string[] value, string prefix, string suffix, string separator)
        {
            var list = new List<string>();
            value.ForEach(val => list.Add(string.Concat(prefix, val.SafeTrim(), suffix)));
            return string.Join(separator, list.ToArray());
        }

        /// <summary>
        /// Performs a safe trim
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string SafeTrim(this string value)
        {
            return string.IsNullOrEmpty(value) ? string.Empty : value.Trim();
        }

        /// <summary>
        /// Formats this string with the CurrentCulture passing in the specified paramaters
        /// </summary>
        public static string FormatCurrentCulture(this String format, params object[] values)
        {
            return string.Format(CultureInfo.CurrentCulture, format, values);
        }

        /// <summary>
        /// Formats this string with the InvariantCulture passing in the specified paramaters
        /// </summary>
        public static string FormatInvariantCulture(this String format, params object[] values)
        {
            return string.Format(CultureInfo.InvariantCulture, format, values);
        }

        /// <summary>
        /// Indicates whether this string is null or empty
        /// </summary>
        public static bool IsNullOrEmpty(this String value)
        {
            return string.IsNullOrEmpty(value);
        }

        /// <summary>
        /// Indicates whether this string is not null or empty
        /// </summary>
        public static bool IsNotNullOrEmpty(this String value)
        {
            return !string.IsNullOrEmpty(value);
        }

        /// <summary>
        /// Does a Left operation on this string
        /// </summary>
        public static string Left(this string value, int length)
        {
            if (value.IsNotNullOrEmpty())
            {
                if (value.Length <= length)
                {
                    return value;
                }
                return value.Substring(0, length);
            }
            return string.Empty;
        }

        /// <summary>
        /// Does a Right operation on this string
        /// </summary>
        public static string Right(this string value, int length)
        {
            if (value.IsNotNullOrEmpty())
            {
                if (value.Length <= length)
                {
                    return value;
                }
                return value.Substring(value.Length - length);
            }
            return string.Empty;
        }

        /// <summary>
        /// Strips all non alphanumeric characters from the string.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string RemoveAllNonAlphanumericChars(this string value)
        {
            return Regex.Replace(value, @"\W*", "");
        }

        /// <summary>
        /// Formats a CamelCased string to words. e.g Camel Cased
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string CamelCaseToWords(this string value)
        {
            if (value.IsNullOrEmpty())
            {
                return string.Empty;
            }

            var words = Regex.Split(value, @"(?<!^)(?<=[a-z])(?=[A-Z]+)");

            var result = new StringBuilder();
            words.ToList().ForEach(w => result.Append("{0} ".FormatInvariantCulture(w.Trim())));

            return result.ToString().Trim();
        }

        /// <summary>
        /// Removes occurrences of characters, provided in the array
        /// </summary>
        public static string Remove(this string value, char[] exclusions)
        {

            if (value.IsNotNullOrEmpty())
            {
                var builder = new StringBuilder(value);

                var characterIndex = value.IndexOfAny(exclusions, 0);
                while (characterIndex != -1)
                {
                    builder.Remove(characterIndex, 1);
                    characterIndex = builder.ToString().IndexOfAny(exclusions, 0);
                }
                return builder.ToString();
            }
            return string.Empty;
        }

        /// <summary>
        /// Returns only the Numeric digits in a string, by stripping out all non 
        /// numeric characters, except those characters in the exclusion list
        /// </summary>
        public static string RemoveText(this string value, char[] exclusions)
        {
            return RemoveDigitOrText(value, true, exclusions);
        }

        /// <summary>
        /// Returns only the Numeric digits in a string
        /// </summary>
        public static string RemoveText(this string value)
        {
            return RemoveDigitOrText(value, true, null);
        }

        /// <summary>
        /// Removes all the numeric digits from a string, except those in the exclusion list
        /// </summary>
        public static string RemoveDigits(this string value, char[] exclusions)
        {
            return RemoveDigitOrText(value, false, exclusions);
        }

        /// <summary>
        /// Removes all the numeric digits from a string
        /// </summary>
        public static string RemoveDigits(this string value)
        {
            return RemoveDigitOrText(value, false, null);
        }

        /// <summary>
        /// Returns only the Numeric digits in a string when bool returnDigit is set to False
        /// Removes all the numeric digits from a string when bool returnDigit is set to True
        /// </summary>
        private static string RemoveDigitOrText(string value, bool removeDigit, char[] exclusions)
        {
            string returnValue = string.Empty;
            bool shouldRemove;
            foreach (char character in value.ToCharArray())
            {
                shouldRemove = removeDigit;
                if (exclusions.IsNotNull() && exclusions.Contains(character))
                {
                    shouldRemove = !removeDigit;

                }
                if (char.IsDigit(character) == shouldRemove)
                {
                    returnValue += character.ToString();
                }
            }
            return returnValue;
        }

        /// <summary>
        /// Repeats the specified string. E.g "x".Repeat(3) = "xxx". nice.
        /// </summary>
        public static string Repeat(this string value, int times)
        {
            var copy = string.Empty;

            for (var x = 0; x < times; x++)
            {
                copy += value;
            }

            return copy;
        }

        /// <summary>
        /// Does a case insensitive invariant culture comparison of two strings.
        /// </summary>
        public static bool IsSameAs(this string value, string target)
        {
            return string.Compare(value, target, StringComparison.OrdinalIgnoreCase) == 0;

        }


        /// <summary>
        /// Converts the first letter of the first word in a phrase to uppercase
        /// </summary>
        public static string ConvertFirstLetterOfPhraseToUppercase(this string value)
        {
            if (value.IsNotNullOrEmpty())
            {
                var convertedString = value.ToLowerInvariant();

                return convertedString[0].ToString().ToUpperInvariant() + convertedString.Substring(1);
            }
            return string.Empty;
        }



        /// <summary>
        /// Strips html tags
        /// </summary>
        public static string AsPlainText(this string value)
        {
            if (value.IsNotNullOrEmpty())
            {
                var htmlTagPattern = @"</?\w+((\s+\w+(\s*=\s*(?:"".*?""|'.*?'|[^'"">\s]+))?)+\s*|\s*)/?>";
                var removeScriptBlockPattern = @"(?m)<script\b[^>]*>\n*\s\n?.*?</script>";
                var removeStyleBlockPattern = @"(?m)<style\b[^>]*>\n*\s\n?.*?</style>";

                var scriptBlockRemoved = Regex.Replace(value, removeScriptBlockPattern, string.Empty, RegexOptions.Singleline);
                var styleBlockRemoved = Regex.Replace(scriptBlockRemoved, removeStyleBlockPattern, string.Empty, RegexOptions.Singleline);
                var htmlRemoved = Regex.Replace(styleBlockRemoved, htmlTagPattern, string.Empty);

                return htmlRemoved;
            }

            return string.Empty;
        }

        /// <summary>
        /// Indicates whether this string is a number
        /// </summary>
        public static bool IsNumber(this String value)
        {
            int number;
            return int.TryParse(value, out number);
        }

        public static IEnumerable<string> GetTaggedValues(this string value, string tag, bool includeTag)
        {
            var list = new List<string>();
            var result = Regex.Matches(value, @"\{0}(.*?)\{0}".FormatInvariantCulture(tag), RegexOptions.IgnoreCase);
            foreach (Match match in result)
            {
                if (includeTag)
                {
                    list.Add(match.Value);
                }
                else
                {
                    list.Add(match.Groups[1].Value);
                }
            }
            return list;
        }

        public static string ReplaceTaggedValues(this string value, string tag, string replaceValue)
        {
            var result = Regex.Replace(value, @"\{0}(.*?)\{0}".FormatInvariantCulture(tag), replaceValue);
            return result;
        }


#if !SILVERLIGHT

        /// <summary>
        /// Returns an object of type T deserialized from a XML string
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="xml"></param>
        /// <returns></returns>
        public static T Deserialize<T>(this string xml) where T : new()
        {
            if (xml.IsNullOrEmpty())
            {
                return new T();
            }
            using (var memoryStream = new MemoryStream(Encoding.Unicode.GetBytes(xml)))
            {
                using (var reader = XmlDictionaryReader.CreateTextReader(memoryStream, Encoding.Unicode, new XmlDictionaryReaderQuotas(), null))
                {
                    var dataContractSerializer = new DataContractSerializer(typeof(T));
                    var deserialized = (T)dataContractSerializer.ReadObject(reader);
                    if (deserialized.IsNull())
                    {
                        return new T();
                    }
                    return deserialized;
                }
            }
        }

        /// <summary>
        /// Provides the ability to compute a MD5 hash
        /// </summary>
        public static string ComputeHash(this string value)
        {
            using (var md5 = new MD5CryptoServiceProvider())
            {
                var originalBytes = ASCIIEncoding.Default.GetBytes(value);
                var encodedBytes = md5.ComputeHash(originalBytes);
                return BitConverter.ToString(encodedBytes);
            }
        }

#endif

    }
}
