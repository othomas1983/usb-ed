﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace System.Collections.Generic
{
    /// <summary>
    /// Additional extensions on System.Collections.Generic.IEnumerable`1
    /// </summary>
    public static class IEnumerableExtensions
    {
        /// <summary>
        ///  Determines whether a sequence contains a specified element(s) by using a predicate
        /// </summary>
        public static bool Contains<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
        {
            return source.Where(predicate).Count() > 0;
        }

        /// <summary>
        /// Returns the specified first index matched on the predicate
        /// </summary>
        public static int? IndexAt<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
        {
            int? index = null;

            int x = 0;
            foreach (var item in source)
            {
                if (predicate.Invoke(item))
                {
                    index = x;
                    break;
                }
                x++;
            }

            return index;
        }

        /// <summary>
        /// Performs a Distinct utilizing the specified predicate
        /// </summary>
        public static IEnumerable<TSource> DistinctBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> predicate)
        {
            var seenKeys = new HashSet<TKey>();
            foreach (TSource element in source)
            {
                if (seenKeys.Add(predicate(element)))
                {
                    yield return element;
                }
            }
        }


        /// <summary>
        /// Creates a randomized order enumerable from the source enumerable
        /// </summary>
        public static IEnumerable<T> Randomize<T>(this IEnumerable<T> source)
        {
            if (source.IsNull())
            {
                return source;
            }

            var randomSort = new List<Tuple<int, T>>();

            var x = DateTime.Now.Millisecond + DateTime.Now.Second;

            foreach (var item in source)
            {
                var index = new Random(x).Next(1, int.MaxValue);
                randomSort.Add(new Tuple<int, T>(index, item));
                x++;
            }

            var sorted = (from r in randomSort
                          orderby r.Item1 ascending
                          select r.Item2).ToList();

            return sorted;
        }

        /// <summary>
        /// Creates a Delimited string value
        /// </summary>
        public static string AsSeparatedString<T>(this IEnumerable<T> source, string separator, Converter<T, string> converter)
        {
            var values = (from item in source select converter(item)).ToArray();
            return values.ConcatJoin(string.Empty, string.Empty, separator);
        }

    }
}