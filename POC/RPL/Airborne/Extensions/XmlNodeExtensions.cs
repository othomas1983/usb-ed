﻿
namespace System.Xml
{
    /// <summary>
    /// Extensions on XmlNode
    /// </summary>
    public static class XmlNodeExtensions
    {
        /// <summary>
        /// Returns the InnerText value of an XmlNode
        /// </summary>
        /// <param name="node"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static object GetValue(this XmlNode node, string key)
        {
            return node.Attributes[key].IsNotNull() ? node.Attributes[key].InnerText : null;
        }

        /// <summary>
        /// Returns the InnerText value of an XmlNode typed as T
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="node"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static T GetValue<T>(this XmlNode node, string key) where T : class
        {
            var value = GetValue(node, key);
            return value as T ?? default(T);
        }

    }
}
