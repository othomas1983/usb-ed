﻿using System;

namespace Airborne.Common
{
    /// <summary>
    /// Wraps the System.Lazy class to provide a bit more flexibility
    /// </summary>
    public class LazyWrapper
    {
        private Lazy<object> value;

        /// <summary>
        /// Registers the specified value factory used to instantiate the value
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="valueFactory">The value factory.</param>
        public void Register<T>(Func<object> valueFactory)
        {
            ValueType = typeof(T);
            value = new Lazy<object>(valueFactory);
        }

        /// <summary>
        /// Gets a value indicating whether this instance's value was created.
        /// </summary>
        public bool IsValueCreated
        {
            get
            {
                return value.IsValueCreated;
            }
        }

        /// <summary>
        /// Gets or sets the type of the value that was registered
        /// </summary>
        public Type ValueType
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the lazy initialised value.
        /// </summary>
        /// <value>The value.</value>
        public object Value
        {
            get
            {
                return value.Value;
            }
        }
    }
}