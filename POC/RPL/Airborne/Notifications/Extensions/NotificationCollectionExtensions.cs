﻿using System;

namespace Airborne.Notifications
{
    /// <summary>
    /// Extends a NotificationCollection instance with convenience methods
    /// </summary>
    public static class NotificationCollectionExtensions
    {
        /// <summary>
        /// Provides the ability to add a  <see cref="System.Exception"/> as a Notification message
        /// </summary>
        public static void AddException(this NotificationCollection instance, Exception exception)
        {
            instance.AddMessage(new Notification(exception.Message, NotificationSeverity.Error));
        }
    }
}