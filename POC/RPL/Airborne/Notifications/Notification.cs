using System.Globalization;
using System.Runtime.Serialization;

namespace Airborne.Notifications
{
    /// <summary>
    /// A notification or message.
    /// </summary>
    [DataContract]
    public class Notification
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Notification"/> class.
        /// </summary>
        /// <param name="text">The text.</param>
        public Notification(string text)
        {
            Guard.ArgumentNotEmpty(text, "text");
            Text = text;
            Severity = NotificationSeverity.Information;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Notification"/> class.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="severity">The severity.</param>
        public Notification(string text, NotificationSeverity severity)
        {
            Guard.ArgumentNotEmpty(text, "text");
            Text = text;
            Severity = severity;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Notification"/> class.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="severity">The severity.</param>
        /// <param name="tag">The Tag.</param>
        public Notification(string text, NotificationSeverity severity, object tag)
        {
            Guard.ArgumentNotEmpty(text, "text");
            Text = text;
            Severity = severity;
            Tag = tag;
        }


        #endregion

        #region Properties

        /// <summary>
        /// The <see cref="NotificationSeverity"/> of the message
        /// </summary>
        [DataMember]
        public NotificationSeverity Severity { get; set; }

        /// <summary>
        /// The <see cref="Notification"/> text.
        /// </summary>
        [DataMember]
        public string Text { get; set; }

        /// <summary>
        /// The <see cref="Notification"/> code.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Gets and sets the message hint
        /// </summary>
        public string Hint { get; set; }

        /// <summary>
        /// Gets and sets grouping data
        /// </summary>
        public string Grouping { get; set; }

        /// <summary>
        /// Used to apply additional taggable data to a notification
        /// </summary>
        public object Tag { get; set; }

        #endregion

        #region Virtual Methods

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </returns>
        public override string ToString()
        {
            return string.Format(CultureInfo.CurrentCulture, "{0} : {1}", Severity, Text);
        }

        #endregion

        #region Static Methods
        /// <summary>
        /// Creates a new instabce of <see cref="Airborne.Notifications.Notification"/>.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="notificationSeverity">The notification severity.</param>
        /// <returns></returns>
        public static Notification Create(string message, NotificationSeverity notificationSeverity)
        {
            return new Notification(message, notificationSeverity);
        }

        public static Notification Create(string message, NotificationSeverity notificationSeverity, string hint)
        {
            var notification = new Notification(message, notificationSeverity);
            notification.Hint = hint;
            return notification;
        }

        #endregion
    }
}