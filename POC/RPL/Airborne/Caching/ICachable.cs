﻿
namespace Airborne.Caching
{
    public interface ICachable
    {
        /// <summary>
        /// Specifies the key to be used in the cache.
        /// </summary>
        string CacheKey { get; }

        /// <summary>
        /// Indicates that the item is auto cached by domain services like save, delete and update.
        /// </summary>
        bool IsImplicitlyCached { get; }
    }
}