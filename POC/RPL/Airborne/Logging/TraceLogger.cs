﻿using System;
using System.Diagnostics;

namespace Airborne.Logging
{
    /// <summary>
    /// ILogger implementation using the System.Diagnostics.Trace as the underlying logger
    /// </summary>
    public class TraceLogger : ILogger
    {
        #region ILogger Members

        public void Info(string message)
        {
            Trace.WriteLine(message, "INFO");
        }

        public void Warn(string message)
        {
            Trace.WriteLine(message, "WARN");
        }

        public void Error(string message)
        {
            Trace.WriteLine(message, "ERROR");
        }

        public void Error(Exception ex)
        {
            var msg = ex.Message + Environment.NewLine + ex.StackTrace;
            Trace.WriteLine(msg, "ERROR");
        }

        #endregion
    }
}