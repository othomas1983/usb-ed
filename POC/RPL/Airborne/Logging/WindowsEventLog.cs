﻿using System;
using System.Configuration;
using System.Diagnostics;

namespace Airborne.Logging
{
    /// <summary>
    /// ILogger implementation based on the .Net System.Diagnostics EventLog.
    /// </summary>
     public class WindowsEventLog : ILogger, IDisposable
    {
        #region Locals

        EventLog logger;

        #endregion

        #region Constructors
         
         /// <summary>
        /// Instantiates as instance of the WindowsEventLogger 
         /// </summary>
        public WindowsEventLog()
        {
            logger = new EventLog();
            logger.Source = ResolveSource();
        }

        #endregion

        #region ILogger Members

        public void Info(string message)
        {
            logger.WriteEntry(message, EventLogEntryType.Information);
        }

        public void Warn(string message)
        {
            logger.WriteEntry(message, EventLogEntryType.Warning);
        }

        public void Error(string message)
        {
            logger.WriteEntry(message, EventLogEntryType.Error);
        }

        public void Error(Exception ex)
        {
            var msg = ex.Message + Environment.NewLine + ex.StackTrace;

            logger.WriteEntry(msg, EventLogEntryType.Error);
        }

        #endregion

        #region Methods

        private static string ResolveSource()
        {
            return ConfigurationManager.AppSettings["EventLogSource"] ?? "Unkown";
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            if (logger.IsNotNull())
            {
                logger.Dispose();
            }
        }

        #endregion
    }
}