
namespace Airborne.Domain.UnitOfWork
{
    /// <summary>
    /// Indicates the way objects can be registered with the unit of work.
    /// </summary>
    public enum RegistrationType
    {

        /// <summary>
        /// Defines the Instance as newly instantiated
        /// </summary>
        New,

        /// <summary>
        /// Defines the instance as an already have been persisted previously.
        /// </summary>
        Dirty,

        /// <summary>
        /// Defines an instance that is to be removed or deleted.
        /// </summary>
        Deleted
    }
}
