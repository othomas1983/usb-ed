﻿using System.Collections;
using System.Linq;


namespace Airborne.Domain.UnitOfWork
{
    /// <summary>
    /// This instance mirrors the functionality of a <see cref="InMemoryUnitOfWork"/> but required a parent <see cref="UnitOfWorkBase"/>.
    /// This implementation will commit changes to the instances that is registered with the parent when it is commited.
    /// </summary>
    public class NestedUnitOfWork : InMemoryUnitOfWork, INestedUnitOfWork
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="NestedUnitOfWork"/> class.
        /// </summary>
        /// <param name="parent">The parent that this is nested from.</param>
        public NestedUnitOfWork(UnitOfWorkBase parent)
        {
            Guard.ArgumentNotNull(parent, "parent");
            this.Parent = parent;
            RegisterParentItems(parent);
        }

        #region Methods
        private void RegisterParentItems(UnitOfWorkBase parent)
        {
            RegisterItems(parent[RegistrationType.New]);
            RegisterItems(parent[RegistrationType.Dirty]);
        }

        private void RegisterItems(IList items)
        {
            foreach (IAggregate item in items.OfType<IAggregate>())
            {
                Register(item, RegistrationType.Dirty);
            }
        }
        #endregion

        #region UnitOfWorkBase Members

        /// <summary>
        /// Commits the unit of work
        /// </summary>
        public override void Commit()
        {
            foreach (IAggregate entity in ObjectRegister.Keys)
            {
                if (!Parent.IsRegistered(entity))
                {
                    Parent.Register(entity, ObjectRegister[entity]);
                }
            }
            ObjectRegister.Clear();
        }

        #endregion

        #region INestedUnitOfWork Members

        /// <summary>
        /// Gets or sets the parent.
        /// </summary>
        public UnitOfWorkBase Parent
        {
            get;
            private set;
        }

        #endregion
    }
}
