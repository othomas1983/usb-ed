using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.ServiceLocation;

namespace Airborne.Domain.UnitOfWork
{
   /// <summary>
    ///A Unit of Work keeps track of everything you do during a <b>business transaction</b> 
    ///that can affect the database. When you're done, it figures out everything that 
    ///needs to be done to alter the database as a result of your work.
    /// <remarks>see http://martinfowler.com/eaaCatalog/unitOfWork.html</remarks>
   /// </summary>
    [Serializable]
    public abstract class UnitOfWorkBase
    {
        #region Fields
        private IDictionary<IAggregate, RegistrationType> register = new Dictionary<IAggregate, RegistrationType>();
        #endregion

        #region Constructors
        #endregion

        #region Properties
        /// <summary>
        /// Register of items in the unit of work
        /// </summary>
        protected IDictionary<IAggregate, RegistrationType> ObjectRegister
        {
            get
            {
                return register;
            }
        }

        /// <summary>
        /// Current unit of work instance.
        /// </summary>
        protected static UnitOfWorkBase Current
        {
            get;
            set;
        }
        #endregion

        #region Methods

        /// <summary>
        /// When overridden this method will be called when ever a instance is registered with this unit of work.
        /// </summary>
        protected virtual void ObjectRegistered(IAggregate instance, RegistrationType type) { }
        #endregion

        #region IUnitOfWork Members

        /// <summary>
        /// Create a nested unit of work
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public virtual UnitOfWorkBase CreateNested()
        {
            return new NestedUnitOfWork(this);
        }

        /// <summary>
        /// Returns a list of objects that has been registered
        /// under the specified registration type
        /// </summary>
        public IList this[RegistrationType type]
        {
            get
            {
                var items = from o in ObjectRegister
                            where o.Value == type
                            select o.Key;

                return items.ToList();
            }
        }

        /// <summary>
        /// Registers the specified instance.
        /// </summary>
        protected internal void Register(IAggregate instance, RegistrationType registrationType)
        {
            ObjectRegister[instance] = registrationType;
            ObjectRegistered(instance, registrationType);
        }

        /// <summary>
        /// Inidicates whether the unit of work knows about the IAggregate.
        /// </summary>
        public bool IsRegistered(IAggregate instance)
        {
            return ObjectRegister.ContainsKey(instance);
        }


        /// <summary>
        /// Commits the unit of work
        /// </summary>
        public abstract void Commit();

        /// <summary>
        /// Rolls back the unit of work
        /// </summary>
        public abstract void Rollback();
        #endregion

        #region Factory Methods
        /// <summary>
        /// Start a new unit of work.
        /// If one has already been created the curretn unit of work will be returned.
        /// </summary>
        public static UnitOfWorkBase Start()
        {
            if (Current.IsNull())
            {
                Current = ServiceLocator.Current.GetInstance<UnitOfWorkBase>();
            }
            return Current;
        }
        #endregion

    }
}
