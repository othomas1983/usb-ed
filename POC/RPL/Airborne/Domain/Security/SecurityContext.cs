﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Airborne.Domain.Security
{

    /// <summary>
    /// Manages the security context surrounding a user
    /// </summary>
    [DataContract]
    public abstract class SecurityContext
    {
      
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SecurityContext"/> class.
        /// </summary>
        /// <param name="user">The user.</param>
        protected SecurityContext(SecurityUser user)
        {
            Guard.ArgumentNotNull(user, "user");
            Detail = user;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SecurityContext"/> class.
        /// </summary>
        protected SecurityContext()
        {
            //Init();
        }

        //private void Init()
        //{
        //    //Functions = new List<TaskAssignment>();
        //}

        #endregion

        #region Properties

        
        /// <summary>
        /// Gets the greeting.
        /// </summary>
        /// <value>The greeting.</value>
        public string Greeting
        {
            get
            {
                return Detail.ToString();
            }
        }

        /// <summary>
        /// Gets the name of the current user.
        /// </summary>
        /// <value>The name.</value>
        public string Name
        {
            get
            {
                return Detail.Name;
            }

        }
        /// <summary>
        /// Gets the surname of teh current user.
        /// </summary>
        /// <value>The surname.</value>
        public string Surname
        {
            get
            {
                return Detail.Surname;
            }
        }

        /// <summary>
        /// Gets or sets the security user instance.
        /// </summary>
        [DataMember]
        public SecurityUser Detail { get; set; }

        /// <summary>
        /// Gets or sets the users security token
        /// </summary>
        [DataMember]
        public string Token { get; set; }
 


        /// <summary>
        /// Gets the combination of all authorised functions.
        /// </summary>
        /// <value>The authorised functions.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Authorised")]
        public IEnumerable<TaskAssignment> AuthorisedFunctions
        {
            get
            {
                if (Detail.IsNotNull())
                {
                    return Detail.AuthorisedFunctions;
                }

                return new List<TaskAssignment>();
            }
        }

        /// <summary>
        /// Determines if the current security context has access to a function.
        /// </summary>
        public bool HasAccessTo(string function)
        {
            return AuthorisedFunctions.FirstOrDefault(tasks => string.Compare(tasks.TaskName, function, StringComparison.OrdinalIgnoreCase) == 0).IsNotNull();
        }

        /// <summary>
        /// Determines if the current security context has write access to a function.
        /// </summary>
        public bool HasWriteAccessTo(string function)
        {
            TaskAssignment assignment = AuthorisedFunctions.FirstOrDefault(tasks => string.Compare(tasks.TaskName, function, StringComparison.OrdinalIgnoreCase) == 0);
            if (assignment.IsNotNull())
            {
                return assignment.IsWritable;
            }
            return false;
        }

        #endregion

        /// <summary>
        /// Test user
        /// </summary>
        public static SecurityContext Empty
        {
            get
            {
                return new TestSecurityContext();
            }
        }

        /// <summary>
        /// Test security context
        /// </summary>
        public class TestSecurityContext : SecurityContext { }

        [OnDeserializing]
        public void OnDeserializing(StreamingContext context)
        {
            //Init();
        }
    }
}
