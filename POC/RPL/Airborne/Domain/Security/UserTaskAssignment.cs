﻿using System;
using System.Runtime.Serialization;

namespace Airborne.Domain.Security
{

    /// <summary>
    /// Assignment of a task directly to a user
    /// </summary>
    [DataContract]
    public class UserTaskAssignment : TaskAssignment
    {
        /// <summary>
        /// User Id
        /// </summary>
        [DataMember]
        public string UserId { get; set; }

        #region Equality

        /// <summary>
        /// Determines whether the specified <see cref="System.Object"/> is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
        /// <returns>
        /// 	<c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            var functionAssignment = obj as UserTaskAssignment;

            if (functionAssignment.IsNotNull())
            {
                return (functionAssignment.Task.Id == Task.Id && functionAssignment.UserId == UserId);
            }

            return base.Equals(obj);
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            if (Id.IsNotNullOrEmpty())
            {
                return Id.GetHashCode();
            }

            return base.GetHashCode();
        }

        #endregion
    }
}
