﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Airborne.Domain.Security
{
    /// <summary>
    /// A view on a user or member that makes sense from a security point of view
    /// </summary>
    [DataContract]
    public class SecurityUser : IAggregate
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SecurityUser"/> class.
        /// </summary>
        public SecurityUser()
        {
            Init();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>The id.</value>
        [DataMember]
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        [DataMember]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the surname.
        /// </summary>
        /// <value>The surname.</value>
        [DataMember]
        public string Surname { get; set; }

        /// <summary>
        /// Gets or sets the current role.
        /// </summary>
        /// <value>The current role.</value>
        [DataMember]
        public Role CurrentRole { get; set; }

        /// <summary>
        /// Gets or sets the tasks that is assigned to the user.
        /// </summary>
        /// <value>The tasks.</value>
        [DataMember]
        public IList<UserTaskAssignment> Tasks { get; set; }

        /// <summary>
        /// Gets the friendly greeting.
        /// </summary>
        /// <value>The greeting.</value>
        public string Greeting
        {
            get
            {
                return string.Concat(Surname, ", ", Name);
            }
        }

        /// <summary>
        /// Gets the amalgamation of authorised functions derived from rthe rols assignment and direct assignments
        /// </summary>
        /// <value>The authorised functions.</value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Authorised")]
        public IEnumerable<TaskAssignment> AuthorisedFunctions
        {
            get
            {
                var actualList = new List<TaskAssignment>();

                if (CurrentRole.IsNotNull())
                {
                    var roleAssignments = CurrentRole.Tasks.ToList();
                    roleAssignments.ForEach(assign => assign.Role = (assign.Role.IsNull()) ? CurrentRole : assign.Role);

                    actualList.AddRange(roleAssignments.OfType<TaskAssignment>());
                }

                if (Tasks.IsNotNull())
                {

                    Tasks.ToList().ForEach((securityFunction) =>
                    {
                        var foundItem = actualList.FirstOrDefault(func => func.Task == securityFunction.Task);
                        if (foundItem.IsNotNull())
                        {
                            actualList.Remove(foundItem);
                        }
                        //overwrite or add the assignment
                        actualList.Add(securityFunction);
                    });
                }

                return actualList;
            }
        }

        #endregion

        #region Overrides

        /// <summary>
        /// Determines whether the specified <see cref="System.Object"/> is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
        /// <returns>
        /// 	<c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        /// <exception cref="T:System.NullReferenceException">
        /// The <paramref name="obj"/> parameter is null.
        /// </exception>
        public override bool Equals(object obj)
        {
            var securityUser = obj as SecurityUser;

            if (securityUser.IsNotNull())
            {
                return (securityUser.Id == Id);
            }

            return base.Equals(obj);
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return this.Id;
        }
        #endregion

        #region Methods 


        #endregion

        [OnDeserializing]
        public void OnDeserializing(StreamingContext context)
        {
            Init();
        }

        private void Init()
        {
            Tasks = new List<UserTaskAssignment>();
        }

    }
}
