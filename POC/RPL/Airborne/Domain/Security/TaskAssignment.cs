﻿using System;
using System.Runtime.Serialization;

namespace Airborne.Domain.Security
{
    /// <summary>
    /// Once implemented will represent an assignment of a task in one way or the other.
    /// </summary>
    [DataContract]
    [KnownType(typeof(RoleTaskAssignment))]
    [KnownType(typeof(UserTaskAssignment))] 
    public abstract class TaskAssignment
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>The id.</value>
        [DataMember]
        public string Id { get; set; }


        /// <summary>
        /// Gets or sets the task.
        /// </summary>
        /// <value>The task.</value>
        [DataMember]
        public SecurityTask Task { get; set; }

        /// <summary>
        /// Gets the name of the task that is assigned.
        /// </summary>
        /// <value>The name of the task.</value>
        public string TaskName
        {
            get
            {
                return Task.Name;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this assignment authorises writing privilege.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is writable; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool IsWritable
        {
            get;
            set;
        }

        #region Equality

        /// <summary>
        /// Determines whether the specified <see cref="System.Object"/> is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
        /// <returns>
        /// 	<c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        /// <exception cref="T:System.NullReferenceException">
        /// The <paramref name="obj"/> parameter is null.
        /// </exception>
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            return GetHashCode().Equals(obj.GetHashCode());
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            if (Id.IsNotNullOrEmpty())
            {
                return Id.GetHashCode();
            }

            return base.GetHashCode();
        }

        #endregion
    }
}
