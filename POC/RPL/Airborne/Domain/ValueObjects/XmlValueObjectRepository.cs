﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using Airborne.Properties;

namespace Airborne.Domain.ValueObjects
{
    /// <summary>
    /// Valueobject repository that gets the 
    /// value object definitions from embedded xml resources.
    /// <remarks>
    /// The Xml definitions must be located in the same location as the 
    /// class definitions and they my be embedded resources.
    /// </remarks>
    /// </summary>
    public class XmlValueObjectRepository : IValueObjectRepository
    {
        #region Fields
        private IDictionary<Type, IList<ValueObject>> valueObjectCache;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="XmlValueObjectRepository"/> class.
        /// </summary>
        public XmlValueObjectRepository()
        {
        }

        #endregion

        #region Public Methods

        #region FindAll

        /// <summary>
        /// Find all the value objects of the <param name="type"/> specified
        /// </summary>
        public ValueObject[] FindAll(Type type)
        {
            return LoadValueObjectsFromCache(type).ToArray();
        }

        /// <summary>
        /// Find all the value objects for <typeparam name="T"/>
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter", Justification = "There is a method that does not require explicit type parameter")]
        public T[] FindAll<T>() where T : ValueObject
        {
            ValueObject[] valueObjects = FindAll(typeof(T));
            return valueObjects.Cast<T>().ToArray();
        }

        #endregion

        #region Find

        /// <summary>
        /// Finds a value object instance based on <paramref name="type"/>
        /// and the <paramref name="code"/>
        /// </summary>
        public ValueObject Find(Type type, string code)
        {
            var valueObject = from vo in FindAll(type)
                              where vo.Code.ToUpper(CultureInfo.InvariantCulture) == code.ToUpper(CultureInfo.InvariantCulture)
                              select vo;

            if ((valueObject != null) && (valueObject.Count() > 0))
            {
                return valueObject.First();
            }
            else
            {
                return null;
            }

        }

        /// <summary>
        /// Find a value object based on the code.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter", Justification = "There is a method that does not require explicit type parameter")]
        public T Find<T>(string code) where T : ValueObject
        {
            return (T)Find(typeof(T), code);
        }

        #endregion

        #endregion

        #region Private Methods
        /// <summary>
        /// Loads value objects from cache
        /// </summary>
        private IList<ValueObject> LoadValueObjectsFromCache(Type valueObjectType)
        {
            IList<ValueObject> valueObjects;

            if (ValueObjectCache.Keys.Contains(valueObjectType))
            {
                valueObjects = ValueObjectCache[valueObjectType];
            }
            else
            {
                valueObjects = LoadFromXml(valueObjectType);

                ValueObjectCache.Add(valueObjectType, valueObjects);
            }

            return valueObjects;
        }

        /// <summary>
        /// Loads value objects definitions contained in xml resource files
        /// </summary>
        private static IList<ValueObject> LoadFromXml(Type valueObjectType)
        {
            List<ValueObject> newValueObjectList = new List<ValueObject>();

            string resourceName = Resources.XmlValueObjectSourceFormat.FormatInvariantCulture(valueObjectType.FullName);

            Stream resource = valueObjectType.Assembly.GetManifestResourceStream(resourceName);

            if (resource == null)
            {
                throw new InvalidDataException(Resources.XmlValueObjectResourceNotFound.FormatCurrentCulture(resourceName, valueObjectType.Name));
            }

            //load data from xml. This logic assumes the filename of the xml file is the same
            //as that of the full class
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(resource);

            foreach (XmlNode valueObjectNode in xmlDocument.LastChild.ChildNodes)
            {
                ValueObject valueObject = (ValueObject)Activator.CreateInstance(valueObjectType, true);

                foreach (XmlNode propertyNode in valueObjectNode.ChildNodes)
                {
                    PropertyInfo propertyInfo = valueObjectType.GetProperty(propertyNode.Name);

                    if (propertyInfo == null)
                    {
                        throw new XmlException(Resources.ValueObjectXmlMappingError.FormatCurrentCulture(propertyNode.Name, valueObjectType.FullName));
                    }

                    propertyInfo.SetValue(valueObject, propertyNode.InnerText, null);
                }

                newValueObjectList.Add(valueObject);
            }
            return newValueObjectList.OrderBy(valueObject => valueObject.Text).ToList();
        }


        #region IDisposable Members

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                ValueObjectCache.Clear();
                valueObjectCache = null;
            }
        }
        #endregion


        #endregion

        #region Private Properties
        /// <summary>
        /// Private cache of the value objects
        /// </summary>
        private IDictionary<Type, IList<ValueObject>> ValueObjectCache
        {
            get
            {
                if (valueObjectCache == null)
                {
                    valueObjectCache = new Dictionary<Type, IList<ValueObject>>();
                }
                return valueObjectCache;
            }
        }

        #endregion
    }
}
