﻿
namespace Airborne.Domain.Repository.QueryObjects
{
    /// <summary>
    /// Common filters used with the <see cref="SimpleFilter"/>
    /// </summary>
    public enum FilterType
    {

        /// <summary>
        /// Like filter.
        /// <remarks>
        /// Similiar to the like filter found in SQL
        /// </remarks>
        /// </summary>
        Like,

        /// <summary>
        /// Equal filter. 
        /// <remarks>
        /// The inverse of the <see cref="NotEqual"/> filter.
        /// </remarks>
        /// </summary>
        Equal,

        /// <summary>
        /// Not Equal filter
        /// <remarks>
        /// The inverse of the <see cref="Equal"/> filter.
        /// </remarks>
        /// </summary>
        NotEqual,

        /// <summary>
        /// Greater than filter
        /// </summary>
        GreaterThan,

        /// <summary>
        /// Greater than or equal to filter
        /// </summary>
        GreaterThanOrEqualTo,

        /// <summary>
        /// Less than filter
        /// </summary>
        LessThan,

        /// <summary>
        /// Less than or equal to filter
        /// </summary>
        LessThanOrEqualTo,

        /// <summary>
        /// Is null filter
        /// </summary>
        IsNull

    
    }
}
