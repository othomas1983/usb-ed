﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Airborne.Domain.Repository.QueryObjects
{
    /// <summary>
    /// Extension methods that applies to the <see cref="Criteria"/>
    /// </summary>
    public static class CriteriaExtensions
    {
        /// <summary>
        /// Performs a logical And on the <paramref name="criteria"/> filter and the <paramref name="filter"/>
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        /// <param name="filter">The filter.</param>
        /// <returns></returns>
        public static Criteria And(this Criteria criteria, Filter filter)
        {
            criteria.Filter = criteria.Filter & filter;
            return criteria;
        }

        /// <summary>
        /// Ands the specified criteria.
        /// </summary>
        public static Criteria And(this Criteria criteria, string property, FilterType @operator, object value)
        {
            return criteria.And(new SimpleFilter(property, @operator, value));
        }

        /// <summary>
        /// Performs a logical Or on the <paramref name="criteria"/> filter and the <paramref name="filter"/>
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        /// <param name="filter">The filter.</param>
        /// <returns></returns>
        public static Criteria Or(this Criteria criteria, Filter filter)
        {
            criteria.Filter = criteria.Filter | filter;
            return criteria;
        }

        /// <summary>
        /// Sets the Filter on <see cref="Criteria"/> to a <see cref="SimpleFilter"/>.
        /// </summary>
        /// <param name="criteria">The criteria.</param>
        /// <param name="property">The property.</param>
        /// <param name="operator">The @operator.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static Criteria Where(this Criteria criteria, string property, FilterType @operator, object value)
        {
            Guard.ArgumentNotEmpty(property, "property");
            criteria.Filter = new SimpleFilter(property, @operator, value);
            return criteria;
        }
    }
}
