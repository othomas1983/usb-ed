﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShortCourseApplicationForms.Controllers;
using USB_ED.Models;
using UnitTestProject.USBEDShortCourseApplication;
using USB_ED.Validation;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;

namespace UnitTestProject
{
    [TestClass]
    public class USBEDApplicationForm
    {
        #region ShortCourseOfferings
        [TestMethod]
        public void OfferingFound()
        {            
            var info = new ApplicationFormInfo();
            ApplicationFormInfo.Info = null;

            var model = new ApplicationForm()
            {
                DirectOfferingLink = true
            };
            
            info.LoadInfoMembers(model, null, "be430e33-7200-e511-8278-402cf4573df7");
            
            Assert.IsTrue(model.CourseName.IsNotNullOrEmpty());
        }

         [TestMethod]
        public void OfferingNotFound()
        {
            var info = new ApplicationFormInfo();
            ApplicationFormInfo.Info = null;

            var model = new ApplicationForm()
            {
                DirectOfferingLink = true
            };

            info.LoadInfoMembers(model, null, "00000000-0000-0000-0000-000000000000");

            Assert.IsTrue(model.CourseName.IsNullOrEmpty());
        }

        #endregion

        #region ShortCourses
        [TestMethod]
         public void ShortCourseFound()
         {
             var info = new ApplicationFormInfo();
             ApplicationFormInfo.Info = null;

             var model = new ApplicationForm()
             {
                 DirectOfferingLink = false
             };

             info.LoadInfoMembers(model, "74b0f7c8-72fd-e411-8276-402cf4573df7", null);

             Assert.IsTrue(info.ShortCourseOfferings.Count > 0);
         }

         [TestMethod]
        public void ShortCourseNotFound()
         {
             var info = new ApplicationFormInfo();
             ApplicationFormInfo.Info = null;

             var model = new ApplicationForm()
             {
                 DirectOfferingLink = false
             };

             info.LoadInfoMembers(model, "00000000-0000-0000-0000-000000000000", null);

             Assert.IsTrue(info.ShortCourseOfferings.Count == 0);
         }

         #endregion

         [TestMethod]
         public void SubmitApplicationFormFail()
         {
             var hasErrors = false;
             var model = new ApplicationForm();
             var info = new ApplicationFormInfo();
             ApplicationFormInfo.Info = null;
             model.DirectOfferingLink = false;
             using (ApplicationFormTest test = new ApplicationFormTest())
             {
                 info.LoadInfoMembers(model, "74b0f7c8-72fd-e411-8276-402cf4573df7", null);
                 model = test.EnvokeApplicationForm(model, true);
                 model.ValidationSection = "ApplicationForm";
             }

             if (info.ShortCourseOfferings.Count > 0)
             {
                 var controller = new ApplicationFormController();
                 
                 var result = controller.Index(model);
                 hasErrors = ((System.Web.Mvc.ViewResultBase)(result)).ViewData.ModelState.IsValid;
             }
             else
             {
                 throw new Exception("No Course details where found based on your selection.");
             }

             Assert.IsFalse(hasErrors);
         }

         [TestMethod]
         public void SubmitApplicationFormSuccess()
         {
             var hasErrors = false;
             var model = new ApplicationForm();
             var info = new ApplicationFormInfo();
             var courseId = "74b0f7c8-72fd-e411-8276-402cf4573df7";

             ApplicationFormInfo.Info = null;
             model.DirectOfferingLink = false;
             using (ApplicationFormTest test = new ApplicationFormTest())
             {
                 info.LoadInfoMembers(model, courseId, null);
                 model.Offering = courseId.AsGuid().GetValueOrDefault();
                 model = test.EnvokeApplicationForm(model, true);

                 var fileName = @"USBEDShortCourseApplication\Documents\ID Test.pdf";
                 var message = test.UploadFile(model,"id", fileName);
                 
                 fileName = @"USBEDShortCourseApplication\Documents\QualificationTest.pdf";
                 message = test.UploadFile(model, "certificate", fileName);

                 model.ValidationSection = "ApplicationForm";
             }

             if (info.ShortCourseOfferings.Count > 0)
             {
                 model.ShortCourseInfo = null;
                 var controller = new ApplicationFormController();
                 var result = controller.Index(model);
                 hasErrors = ((System.Web.Mvc.ViewResultBase)(result)).ViewData.ModelState.IsValid;
             }
             else
             {
                 throw new Exception("No Course details where found based on your selection.");
             }

             Assert.IsFalse(hasErrors);
         }

        #region Validation Checks

         [TestMethod]
         public void ValidateEmail()
         {
             var email = "test@appform";
             var validate = new ValidateEmailAddress();
             var result = validate.IsValid(email);

             Assert.IsFalse(result);

             email = "test@appform.cc";
             result = validate.IsValid(email);

             Assert.IsTrue(result);
         }

         [TestMethod]
         public void ValidateSAIdNumber()
         {
             var idNumber = "2112225007088";
             var info = new ApplicationFormInfo();
             ApplicationFormInfo.Info = new ApplicationFormInfo();
             var model = new ApplicationForm();

             using (ApplicationFormTest test = new ApplicationFormTest())
             {
                 WebClient client = new WebClient();
                 info.LoadNationalities(client, info);
                 info.LoadGenders(client, info);

                 var nationality = info.Nationalities.FirstOrDefault(c => c.Text == "South Africa");
                 model.Nationality = nationality.Value.AsGuid().GetValueOrDefault();
                 model.Gender = info.Genders.FirstOrDefault(c => c.Text == "Male").Value.AsGuid().GetValueOrDefault();
                 model.DateOfBirth = new DateTime(1921, 12, 22);

                 ApplicationFormInfo.Info.Nationalities = info.Nationalities;

                 //Pass
                 var context = new ValidationContext(model);
                 var result = new IdNumberValidation().GetValidationResult(idNumber, context);
                 Assert.IsNull(result);

                 //Fail - IDNumber vc DOB
                 model.DateOfBirth = new DateTime(1922, 12, 22);
                 result = new IdNumberValidation().GetValidationResult(idNumber, context);
                 Assert.IsNotNull(result);

                 //Fail - IDNumber vs Gender
                 model.Gender = info.Genders.FirstOrDefault(c => c.Text == "Female").Value.AsGuid().GetValueOrDefault();
                 result = new IdNumberValidation().GetValidationResult(idNumber, context);
                 Assert.IsNotNull(result);

                 //Fail - IDNumber format invalid
                 idNumber = "2154578021547";
                 result = new IdNumberValidation().GetValidationResult(idNumber, context);
                 Assert.IsNotNull(result);
             }
         }

         [TestMethod]
         public void ValidateInitialsAndFirstNames()
         {
             var model = new ApplicationForm();

             model.FirstNames = "R";
             model.Initials = "R";
             var context = new ValidationContext(model);

             var result = new InitialsAndFirstNamesValidationAttribute().GetValidationResult(null, context);
             Assert.AreEqual("Name and initials cannot be a single character", result.ErrorMessage);
             
             model.FirstNames = "Jan";
             model.Initials = "R";
             context = new ValidationContext(model);
             result = new InitialsAndFirstNamesValidationAttribute().GetValidationResult(null, context);
             Assert.AreEqual("Name(s) do not match initials", result.ErrorMessage);
         }

         [TestMethod]
         public void ValidatePaymentResponsibility()
         {
             var info = new ApplicationFormInfo();
             ApplicationFormInfo.Info = new ApplicationFormInfo();
             var model = new ApplicationForm();

             WebClient client = new WebClient();
             info.LoadPaymentResponsibility(client, info);
             
             ApplicationFormInfo.Info.PaymentResponsibility = info.PaymentResponsibility;
             model.PaymentResponsibility = info.PaymentResponsibility.First(m => m.Text == string.Empty).Value.AsGuid().GetValueOrDefault();
             
             var context = new ValidationContext(model);

             //Fail - Nothig selected
             var result = new PaymentResponsibilityValidation().GetValidationResult(model.PaymentResponsibility, context);
             Assert.AreEqual("Please select a party responsible for payment.", result.ErrorMessage);

             //Fail
             context.MemberName = "Payment Contact Person";
             model.PaymentResponsibility = info.PaymentResponsibility.First(m => m.Text == "Company").Value.AsGuid().GetValueOrDefault();
             result = new PaymentDetailsValidation().GetValidationResult(null, context);
             Assert.AreEqual("Payment Contact Person is a required field", result.ErrorMessage);
         }

        #endregion


    }
}
