﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using USB_ED.Models;
using System.Net.Http;

namespace UnitTestProject.USBEDShortCourseApplication
{
    public class ApplicationFormTest : IDisposable
    { 
        public ApplicationForm EnvokeApplicationForm(ApplicationForm model, bool AllSectionsValidated)
        {
            model.ApplicationKey = Guid.NewGuid().AsString();
            model.Surname = "TestSurname";
            model.FirstNames = "TestName";
            model.Initials = "T";
            model.NameOnCertificate = "T TestSurname";
            model.Title = ApplicationFormInfo.Info.Titles.Where(m => m.Text == "Mister").FirstOrDefault().Value.AsGuid().GetValueOrDefault();
            model.Gender = ApplicationFormInfo.Info.Genders.Where(m => m.Text == "Male").FirstOrDefault().Value.AsGuid().GetValueOrDefault();
            model.Nationality = ApplicationFormInfo.Info.Nationalities.Where(m => m.Text == "South Africa").FirstOrDefault().Value.AsGuid().GetValueOrDefault();
            model.Ethnicity = ApplicationFormInfo.Info.Ethnicities.Where(m => m.Text == "White").FirstOrDefault().Value.AsGuid().GetValueOrDefault();
            model.IdNumber = "2112225007088";            

            model.DateOfBirth = new DateTime(1921, 12, 22);
            model.DateOfBirthDay = "22";
            model.DateOfBirthMonth = "12";
            model.DateOfBirthYear = "1921";
            model.Language = ApplicationFormInfo.Info.Languages.Where(m => m.Value.IsNotNullOrEmpty()).FirstOrDefault().Value.AsGuid().GetValueOrDefault();
            model.DietaryRequirements = ApplicationFormInfo.Info.DietaryRequirements.Where(m => m.Value.IsNotNullOrEmpty()).FirstOrDefault().Value.AsGuid().GetValueOrDefault();
            model.Disability = ApplicationFormInfo.Info.Disabilities.Where(m => m.Value.IsNotNullOrEmpty()).FirstOrDefault().Value.AsGuid().GetValueOrDefault();
            model.UsesWheelchair = false;
            model.DisplayNameApproved = true;

            model.ContactNumber = "+27215554567";
            model.Email = "test@appform.co.za";
            model.PostalPostalCode = "8000";
            model.PostalAfrigis = "3cd6d57a-0b36-e511-80c8-005056b8008e".AsGuid();
            model.PostalCountry = ApplicationFormInfo.Info.AddressCountries.Where(m => m.Text == "South Africa").FirstOrDefault().Value.AsGuid().GetValueOrDefault();
            model.PostalStreet1 = "1ste Straat";
            model.PostalStreet2 = "Roggebaai";
            model.PostalSuburb = "Cape Town";
            model.PostalCity = "Cape Town";
            model.PostalPostalCode = "8000";

            model.Employed = true;
            model.Industry = ApplicationFormInfo.Info.Industries.Where(m => m.Value.IsNotNullOrEmpty()).FirstOrDefault().Value.AsGuid().GetValueOrDefault();
            model.WorkArea = ApplicationFormInfo.Info.WorkAreas.Where(m => m.Value.IsNotNullOrEmpty()).FirstOrDefault().Value.AsGuid().GetValueOrDefault();
            model.JobTitle = "Painter";
            model.Employer = "ABC Paints";
            model.EmployerContactNumber = "+27125557788";

            model.QualificationType = ApplicationFormInfo.Info.QualificationTypes.Where(m => m.Value.IsNotNullOrEmpty()).FirstOrDefault().Value.AsGuid().GetValueOrDefault();
            model.QualificationInstitution = "University of Stellenbosch";
            model.YearAchieved = "1999";

            model.MarketingSource = ApplicationFormInfo.Info.MarketingSources.Where(m => m.Value.IsNotNullOrEmpty()).FirstOrDefault().Value.AsInt().GetValueOrDefault();
            model.MarketingReason = ApplicationFormInfo.Info.MarketingReasons.Where(m => m.Value.IsNotNullOrEmpty()).FirstOrDefault().Value.AsInt().GetValueOrDefault();
            model.UsesTwitter = true;
            model.TwitterUsername = "appFormTwitterTest";
            model.UsesFacebook = true;
            model.UsesLinkedIn = true;
            model.SubscribeToNewsletter = true;

            model.PaymentResponsibility = ApplicationFormInfo.Info.PaymentResponsibility.Where(m => m.Value.IsNotNullOrEmpty()).FirstOrDefault().Value.AsGuid().GetValueOrDefault();
            model.PaymentContactPerson = "André Mouties";
            model.PaymentContactNumber = "+275553322";
            model.PaymentContactEmail = "payment@test.co.za";
            model.PaymentDebtorCode = "555666";

            //id doc
            //qualifications doc

            if (AllSectionsValidated)
            {
                model.Section1Validated = true;
                model.Section2Validated = true;
                model.Section3Validated = true;
                model.Section4Validated = true;
            }

            return model;
        }

        public HttpResponseMessage UploadFile(ApplicationForm model, string type, string savedFileName)
        {
            var result = new HttpResponseMessage();
            var message = new HttpRequestMessage();
            var content = new MultipartFormDataContent();
            var filestream = new FileStream(savedFileName, FileMode.Open, FileAccess.Read);
            var fileName = Path.GetFileName(savedFileName);

            var uploadKey = string.Empty;
            if (type == "id")
            {
                model.IdentificationDocumentUploadKey = Guid.NewGuid().ToString();
                uploadKey = model.IdentificationDocumentUploadKey;
            }
            else if (type == "certificate")
            {
                model.QualificationDocumentUploadKey = Guid.NewGuid().ToString();
                uploadKey = model.QualificationDocumentUploadKey;
            }

            content.Add(new StreamContent(filestream), "file", fileName);
            content.Add(new StringContent(uploadKey), "uploadKey");

            message.Method = HttpMethod.Post;
            message.Content = content;

            result = ApplicationForm.UploadFile(message);

            return result;
        }


        void IDisposable.Dispose()
        {

        }
    }
}
