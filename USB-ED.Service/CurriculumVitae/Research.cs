//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace USB_ED.Service.CurriculumVitae
{
    using System;
    using System.Collections.Generic;
    
    public partial class Research
    {
        public int ResearchID { get; set; }
        public int CVID { get; set; }
        public int ResearchCategoryID { get; set; }
        public Nullable<int> Year { get; set; }
        public string Description { get; set; }
        public string SupervisionOrAssessment { get; set; }
        public string Thesis { get; set; }
        public string Candidate { get; set; }
        public Nullable<System.DateTime> CreatedDT { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDT { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<bool> isActive { get; set; }
    }
}
