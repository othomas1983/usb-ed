//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace USB_ED.Service.CurriculumVitae
{
    using System;
    using System.Collections.Generic;
    
    public partial class Publication
    {
        public int PublicationID { get; set; }
        public int CVID { get; set; }
        public int PublicationCategoryID { get; set; }
        public string PublicationTitle { get; set; }
        public string Title { get; set; }
        public Nullable<int> Year { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public string SubjectArea { get; set; }
        public string Institution { get; set; }
        public string PublisherName { get; set; }
        public string PublicationCity { get; set; }
        public string NoOfPages { get; set; }
        public string CoAuthorName { get; set; }
        public string Promoter { get; set; }
        public string ResearchType { get; set; }
        public string VolumeNumber { get; set; }
        public Nullable<int> StartPage { get; set; }
        public Nullable<int> EndPage { get; set; }
        public string JournalTitle { get; set; }
        public string Author { get; set; }
        public string Editor { get; set; }
        public string BookTitle { get; set; }
        public Nullable<int> Month { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public string SeminarTitle { get; set; }
        public string Location { get; set; }
        public string ConferenceTitle { get; set; }
        public string Country { get; set; }
        public string CoPresenter { get; set; }
        public string CoOrganiser { get; set; }
        public Nullable<System.DateTime> CreatedDT { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDT { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<bool> isActive { get; set; }
    }
}
