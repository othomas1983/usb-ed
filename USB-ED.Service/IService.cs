﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace USB_ED.Service
{    
    [ServiceContract]
    public interface IService
    {
        [OperationContract]
        string UploadDocuments(int value);

        [OperationContract]
        string UploadShortCourseApplicationForm(string json);

        [OperationContract]
        string LoadStudentDetials(string studentNumber, string webTokenId);

        [OperationContract]
        string LoadVenue(string roomNumber);       

        [OperationContract]
        int SubmitBookingForm(VenueBooking.VenueBooking bookingContext);

        [OperationContract]
        string BookEvent(string json);
    }   
}
