﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
//using System.Data.Objects;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Web.Script.Serialization;

using SettingsMan = USB_ED.Service.Properties.Settings;
namespace USB_ED.Service
{   
    public class Service : IService
    {
        #region ApplicationForm
        
        public string UploadDocuments(int value)
        {
            throw new NotImplementedException();
        }

        public string UploadShortCourseApplicationForm(string json)
        {
            string submitUrl = SettingsMan.Default.SubmitAppFormUrl;

            try
            {
                using (WebClient webClient = new WebClient())
                {
                    webClient.Encoding = System.Text.Encoding.UTF8;
                    webClient.Headers.Add("Content-Type", "application/json");
                    return webClient.UploadString(submitUrl, json);
                }
            }
            catch (WebException wex)
            {
                string pageContent = new StreamReader(wex.Response.GetResponseStream()).ReadToEnd().ToString();
                return pageContent;
            }
        }

        public string LoadStudentDetials(string studentNumber, string webTokenId)
        {
            string loadStudentUrl = SettingsMan.Default.LoadStudentUrl;
        
            try
            {
                using (WebClient webClient = new WebClient())
                {
                    webClient.Headers.Add("Content-Type", "application/json");
                    webClient.QueryString.Add("usNumber",studentNumber);
                    webClient.QueryString.Add("webTokenId",webTokenId);
                    
                    return webClient.DownloadString(loadStudentUrl);
                }
            }
            catch (WebException wex)
            {
                string pageContent = new StreamReader(wex.Response.GetResponseStream()).ReadToEnd().ToString();
                return pageContent;
            }
        }

        #endregion

        #region VenueBooking

        public string LoadVenue(string roomNumber)
        {
            var venue = new za.ac.sun.belpark.servicebus.CampusVenues();
            var result = venue.GetVenueDetails(roomNumber);

            return JsonConvert.SerializeObject(result);
        }
        
        public int SubmitBookingForm(VenueBooking.VenueBooking bookingContext)
        {
            try
            {
                using (var entity = new VenueBooking.VenueBookingEntities())
                {
                    entity.VenueBookings.Add(bookingContext);
                    var rows = entity.SaveChanges();

                    return rows;
                }
            }
            catch
            {
                return -1;
            }
        }

        #endregion

        #region EventBooking

        public string BookEvent(string json)
        {
            string eventBookUrl = SettingsMan.Default.EventBookingUrl;

            try
            {
                using (WebClient webClient = new WebClient())
                {
                    webClient.Encoding = System.Text.Encoding.UTF8;
                    webClient.Headers.Add("Content-Type", "application/json");
                    return webClient.UploadString(eventBookUrl, json);
                }
            }
            catch (WebException wex)
            {
                string pageContent = new StreamReader(wex.Response.GetResponseStream()).ReadToEnd().ToString();
                return pageContent;
            }
        }

        #endregion

    }
}
