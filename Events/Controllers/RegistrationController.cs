﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using CaptchaMvc.HtmlHelpers;
using USB_ED.Models;
using Newtonsoft.Json;

namespace USB.ED.Events.Controllers
{
    public class RegistrationController : Controller
    {
        public ActionResult Index()
        {
            var eventModel = new USB_ED.Models.Events();
            var id = string.Empty;

    #if DEBUG
            id = "8d3a35dd-b74f-e511-80cb-005056b8008e";
    #else
            id = HttpContext.Request["iID"];
    #endif

            var campiagnId = new Guid();

            if (Guid.TryParse(id, out campiagnId))
            {
                eventModel.CampaignId = campiagnId;
            }
            
            EventsInfo info = new EventsInfo();
            info.LoadEvents(eventModel, id);

            return View(eventModel);
        }

        [HttpPost]
        public ActionResult Index(USB_ED.Models.Events model, string empty)
        {
            this.IsCaptchaValid("The verification code is not valid");

            if (ModelState.IsValid)
            {
                var json = JsonConvert.SerializeObject(model);
                EventsInfo info = new EventsInfo();
                info.BookEvent(json);

                return RedirectToAction("Index", "RegistationComplete", model);
            }

            return View(model);
        }
    }
}
