﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/master.Master" Inherits="System.Web.Mvc.ViewPage<USB_ED.Models.Events>" %>

<%@ Import namespace="USB_ED.Models" %>
<%@ Register Src="~/Views/Registration/Controls/Register.ascx" TagPrefix="uc1" TagName="Register" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

 <%: Html.ValidationSummary()%>

    <%:Html.Hidden("CampaignId", Model.CampaignId ,new {@id = "campaignId"}) %>
    <%:Html.Hidden("EventName", Model.EventName ,new {@id = "eventName"}) %>
    <%:Html.Hidden("EventLocation", Model.EventLocation ,new {@id = "eventLocation"}) %>

        <div id="Register">
            <uc1:Register runat="server" />
        </div>

        <div id="progressDialog">
           <strong class="message"></strong>
            <img class="progress" />
        </div>
</asp:Content>