﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace USB.ED.Events.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/content/style")
               .Include("~/Content/css/style.css",
               "~/Content/css/jquery-ui.css"));

            bundles.Add(new ScriptBundle("~/bundles/jquery")
                .Include("~/Content/js/jquery-*",
                "~/Content/js/events.js"));
        }
    }
}