﻿using System.ServiceModel;

namespace USB_ED.FacultyManagement.Contract
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISharePointConnectService" in both code and config file together.
    [ServiceContract]
    public interface ISharePointConnectService
    {

        [OperationContract]
        void UploadResearch(string filename, byte[] document);
    }
}
