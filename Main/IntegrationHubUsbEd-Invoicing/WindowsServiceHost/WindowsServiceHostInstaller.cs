﻿using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;


namespace StellenboschUniversity.UsbEd.Integration.WindowsServiceHost
{
    [RunInstaller(true)]
    public partial class WindowsServiceHostInstaller : Installer
    {
        #region Locals

        private ServiceProcessInstaller process;
        private ServiceInstaller service;

        #endregion

        #region Constructor

        public WindowsServiceHostInstaller()
        {
            InitializeComponent();
            InitializeInstaller();
        }

        #endregion

        #region Methods

        private void InitializeInstaller()
        {
            process = new ServiceProcessInstaller();
            process.Account = ServiceAccount.LocalSystem;
            service = new ServiceInstaller();
            service.ServiceName = "Integration Hub Service";
            service.Description = "USB-ED Integration Hub Service";
            Installers.Add(process);
            Installers.Add(service);
        }

        #endregion
    }
}
