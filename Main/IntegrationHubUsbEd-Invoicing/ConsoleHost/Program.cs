﻿using System;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Threading;
using StellenboschUniversity.UsbEd.Integration.Adapters.Crm;
using StellenboschUniversity.UsbEd.Integration.Adapters.Ods;
using StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Debtors;
using StellenboschUniversity.UsbEd.Integration.HubAgent;
using StellenboschUniversity.UsbEd.Integration.HubService;
using StellenboschUniversity.UsbEd.Integration.HubAgent.Debtors;
using StellenboschUniversity.UsbEd.Integration.HubAgent.Invoicing;


namespace StellenboschUniversity.UsbEd.Integration.ConsoleHost
{
    class Program
    {
        private static Agent agent;
        private static TimeSpan integrationInterval = new TimeSpan(0, 10, 0);
        private static TimeSpan importTime = new TimeSpan(1, 0, 0);
        
        static void Main(string[] args)
        {
            Initialize();
            //StartAgent();
            //Console.ReadLine();
            //HostService();
            //StopAgent();

            //Import();

            DebtorService debtorService = new DebtorService();
            debtorService.SendDebtorToAccpac("{B758EBC6-BC7D-E211-9DE5-005056B82153}");

            Console.WriteLine(Environment.NewLine);
            Console.WriteLine("Finished.");
            Console.ReadLine();
        }

        static void Import()
        {
            //SynchronizeGroupCodes.Poll();
            //SynchronizeGLAccounts.Poll();
            //SynchronizeTaxSettings.Poll();
            //SynchronizeDebtors.ImportPoll();
        }

        static void Initialize()
        {
            CrmAdapter.Initialize();
            OdsAdapter.Initialize();
        }

        static void StartAgent()
        {
            agent = new Agent(integrationInterval, importTime);
            agent.Start();

            Console.WriteLine("Agent started.");
        }

        static void StopAgent()
        {
            agent.ShutDown();

            while (agent.HasShutDown == false)
            {
                Console.WriteLine("Waiting for Agent to shut down...");
                Thread.Sleep(2000);
            }

            Console.WriteLine("Agent has shut down.");
        }

        static void HostService()
        {
            using (ServiceHost host = new ServiceHost(typeof(InvoicingService)))
            {
                host.Open();

                Console.WriteLine("The service is ready.");
                Console.WriteLine("Press <Enter> to stop the service.");
                Console.ReadLine();

                host.Close();
            }
        }
    }
}
