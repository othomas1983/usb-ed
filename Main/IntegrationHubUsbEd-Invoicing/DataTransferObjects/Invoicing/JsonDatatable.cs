﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Invoicing
{
    [DataContract]
    public class JsonDatatable
    {
        [DataMember]
        public int Id { get; set; }
        
        [DataMember]
        public IList<List<object>> aaData {get; set;}

        [DataMember]
        public IList<JsonDatacolumn> aoColumns { get; set; }

        public JsonDatatable() 
        {
            aaData = new List<List<object>>();
            aoColumns = new List<JsonDatacolumn>();
        }
 
    }
}
