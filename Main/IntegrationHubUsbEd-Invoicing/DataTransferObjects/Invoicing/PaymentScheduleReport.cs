﻿using System;

namespace StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Invoicing
{
    public class PaymentScheduleReport
    {
        public DateTime ScheduleDate { get; set; }

        public string ProgrammeOffering { get; set; }

        public string ProgrammeManager { get; set; }

        public string Debtor { get; set; }

        public decimal Amount { get; set; }

    }
}
