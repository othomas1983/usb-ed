﻿using System;
using System.Collections.Generic;

namespace StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Invoicing
{
    public class ProgrammeManager
    {
        public Guid Id { get; set; }

        public string Fullname { get; set; }

        public string Email { get; set; }
        
        public ProgrammeManager()
        { 
            
        }
    }
}
