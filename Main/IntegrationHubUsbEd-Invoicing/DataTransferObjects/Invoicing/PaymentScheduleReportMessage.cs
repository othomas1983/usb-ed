﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Invoicing
{
    public class PaymentScheduleReportMessage
    {
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }
    }
}
