﻿using System.Runtime.Serialization;

namespace StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Budget
{
    [DataContract]
    public class SharepointResult
    {
        [DataMember]
        public bool HasPassed { get; set; }
    }
}
