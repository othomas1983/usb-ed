﻿
namespace StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Enum
{
    /// <summary>
    /// Summary description for TransactionStatus
    /// </summary>
    public enum TransactionStatus
    {
        SubmittedByCrm = 0,
        Validated,
        Posted,
        Error,
        SubmittedByAccpac,
        DocumentCreatedUnposted,
        Cancelled,
        AwaitingApplyToNumber,
        GlAccountValidated,
        CustomerValidated
    }
}
