﻿using System;
using NLog;
using StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Budget;
using SPAdapter = StellenboschUniversity.UsbEd.Integration.Adapters.Sharepoint;

namespace StellenboschUniversity.UsbEd.Integration.HubService
{
    public class BudgetingService : IBudgetingService
    {
        #region Locals

        private static Logger logger = LogManager.GetCurrentClassLogger();

        #endregion

        public SharepointResult SendBudgetToOds(int sharepointId)
        { 
            logger.Log(LogLevel.Info, "Budgeting Service DeleteBudgetSet invoked with sharepointId of {1}...{0}", Environment.NewLine, sharepointId.ToString());

            SPAdapter.SharepointAdapter.Initialize();
            return SPAdapter.SharepointAdapter.SendBudgetToOds(sharepointId);
        }

        public void DeleteBudgetSet(int sharepointId)
        {
            logger.Log(LogLevel.Info, "Budgeting Service DeleteBudgetSet invoked with sharepointId of {1}...{0}", Environment.NewLine, sharepointId.ToString());
            
            SPAdapter.SharepointAdapter.Initialize();
            SPAdapter.SharepointAdapter.DeleteBudgetSet(sharepointId);
        }

        public string SendBudgetToAccpac(string message)
        {
            logger.Log(LogLevel.Info, "Budgeting Service SendBudgetToAccpac invoked with message of {1}...{0}", Environment.NewLine, message);
            
            SPAdapter.SharepointAdapter.Initialize();
            return SPAdapter.SharepointAdapter.SendBudgetToAccpac(message);
            
        }
        
    }
}
