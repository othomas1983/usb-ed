﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Enum;
using StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Debtors;
using StellenboschUniversity.UsbEd.Integration.Adapters.Ods;
using StellenboschUniversity.UsbEd.Integration.Adapters.Crm;

namespace StellenboschUniversity.UsbEd.Integration.HubService
{
    public class DebtorService : IDebtorService
    {
        public void SendDebtorToAccpac(string message)
        {
            Guid crmDebtorId = new Guid(message);

            CrmAdapter.Initialize();
            OdsAdapter.Initialize();
            
            var debtor = CrmAdapter.LoadDebtorFromCrm(crmDebtorId);
            OdsAdapter.SaveDebtorToOds(debtor, DebtorOrigin.Crm, TransactionStatus.SubmittedByCrm);
            CrmAdapter.UpdateDebtorAccpacIntegrationStatus(crmDebtorId, AccpacIntegrationStatus.SentToAccpac);
        }
    }
}
