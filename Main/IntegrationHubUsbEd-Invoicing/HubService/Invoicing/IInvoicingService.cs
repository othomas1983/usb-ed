﻿using System;
using System.ServiceModel;
using System.ServiceModel.Web;
using StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Invoicing;

namespace StellenboschUniversity.UsbEd.Integration.HubService
{
    [ServiceContract]
    public interface IInvoicingService
    {
        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare,
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "CreatePaymentSchedule?message={message}")]
        void CreatePaymentSchedule(string message);

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare,
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "UpdatePaymentSchedule?message={message}")]
        void UpdatePaymentSchedule(string message);

        [OperationContract]
        [WebGet(BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "GetPaymentSchedule?message={message}")]
        JsonDatatable GetPaymentSchedule(string message);

        [OperationContract]
        [WebGet(BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "GetPaymentScheduleTotal?message={message}")]
        string GetPaymentScheduleTotal(string message);

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare,
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "DeletePaymentSchedule?message={message}")]
        void DeletePaymentSchedule(string message);

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare,
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "SendInvoiceToAccpac?message={message}")]
        void SendInvoiceToAccpac(string message);

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare,
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "SendCreditNoteToAccpac?message={message}")]
        void SendCreditNoteToAccpac(string message);

        [OperationContract]
        [WebGet(BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "GetPaymentScheduleReport?message={message}")]
        JsonDatatable GetPaymentScheduleReport(string message);
    }
}
