﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StellenboschUniversity.UsbEd.Integration.Adapters.Ods;
using StellenboschUniversity.UsbEd.Integration.Adapters.Crm;
using StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Enum;

namespace StellenboschUniversity.UsbEd.Integration.HubAgent.Invoicing
{
    public static class CheckInvoices
    {
        public static void Poll()
        {
            List<Guid> postedInvoiceIds = OdsAdapter.GetPostedInvoiceIds();

            foreach (var postedInvoiceId in postedInvoiceIds)
            {
                try
                {
                    decimal? invoiceBalance = OdsAdapter.GetInvoiceBalance(postedInvoiceId);
                    string invoiceNumber = OdsAdapter.GetInvoiceNumber(postedInvoiceId);

                    if (invoiceBalance.HasValue == true)
                    {
                        CrmAdapter.UpdateInvoiceNumber(postedInvoiceId, invoiceNumber);
                        CrmAdapter.UpdateInvoiceBalance(postedInvoiceId, invoiceBalance.Value);
                        CrmAdapter.UpdateInvoiceAccpacIntegrationStatus(postedInvoiceId, AccpacIntegrationStatus.IntegratedWithAccpac);
                    }
                }
                catch { }
            }
        }
    }
}
