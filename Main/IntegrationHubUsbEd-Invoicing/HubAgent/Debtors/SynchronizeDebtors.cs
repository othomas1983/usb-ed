﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NLog;
using StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Enum;
using StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Debtors;
using StellenboschUniversity.UsbEd.Integration.Adapters.Crm;
using StellenboschUniversity.UsbEd.Integration.Adapters.Ods;

namespace StellenboschUniversity.UsbEd.Integration.HubAgent.Debtors
{
    public static class SynchronizeDebtors
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        
        public static void IntegrationPoll()
        {
            // Debtors originating from CRM created/updated successfully on Accpac

            List<Debtor> debtorsOriginCrm = OdsAdapter.GetDebtors(DebtorOrigin.Crm, TransactionStatus.Posted);

            foreach (var accpacDebtor in debtorsOriginCrm)
            {
                try
                {
                    if (CrmAdapter.DebtorExists(accpacDebtor.CrmId.Value))
                    {
                        CrmAdapter.UpdateDebtorCode(accpacDebtor.CrmId.Value, accpacDebtor.DebtorCode);
                        CrmAdapter.UpdateDebtorCurrencyInformation(accpacDebtor.CrmId.Value, accpacDebtor.CurrencyCode, accpacDebtor.CurrencySymbol);

                        // change transaction status of this debtor entry in the ODS
                        OdsAdapter.ChangeDebtorTransactionStatus(accpacDebtor.OdsId, TransactionStatus.Posted);
                        CrmAdapter.UpdateDebtorAccpacIntegrationStatus(accpacDebtor.CrmId.Value, AccpacIntegrationStatus.IntegratedWithAccpac);
                    }
                }
                catch { }
            }
        }

        public static void ImportPoll()
        {
            // Debtors originating from Accpac (can be either new or updated debtors; have to check each case individually)

            List<Debtor> debtorsOriginAccpac = OdsAdapter.GetDebtors(DebtorOrigin.Accpac, TransactionStatus.SubmittedByAccpac);

            foreach (var accpacDebtor in debtorsOriginAccpac)
            {
                try
                {
                    Debtor existingCrmDebtor = CrmAdapter.GetDebtor(accpacDebtor.DebtorCode);

                    if (existingCrmDebtor == null)
                    {
                        // this is a new debtor
                        CrmAdapter.SaveDebtorToCrm(accpacDebtor);
                        existingCrmDebtor = CrmAdapter.GetDebtor(accpacDebtor.DebtorCode);
                        accpacDebtor.CrmId = existingCrmDebtor.CrmId.Value;
                    }
                    else
                    {
                        // this is an existing debtor
                        accpacDebtor.CrmId = existingCrmDebtor.CrmId.Value;
                        CrmAdapter.SaveDebtorToCrm(accpacDebtor);
                    }

                    // change transaction status of this debtor entry in the ODS
                    OdsAdapter.ChangeDebtorTransactionStatus(accpacDebtor.OdsId, TransactionStatus.Posted);
                    CrmAdapter.UpdateDebtorAccpacIntegrationStatus(accpacDebtor.CrmId.Value, AccpacIntegrationStatus.IntegratedWithAccpac);
                }
                catch { }
            }
        }
    }
}
