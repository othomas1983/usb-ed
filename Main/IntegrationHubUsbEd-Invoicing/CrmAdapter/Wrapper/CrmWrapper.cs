﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Description;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using StellenboschUniversity.UsbEd.Integration.Adapters.Crm.Proxy;

namespace StellenboschUniversity.UsbEd.Integration.Adapters.Crm.Wrapper
{
    public static class CrmWrapper
    {
        private static CrmServiceContext crmServiceContext;
        private static OrganizationServiceContext organizationServiceContext;
        private static OrganizationServiceProxy organizationServiceProxy;
        private static bool initialized = false;
        
        public static void Initialize(Uri crmUri, ClientCredentials credentials)
        {
            organizationServiceProxy = new OrganizationServiceProxy(crmUri, null, credentials, null);
            organizationServiceProxy.EnableProxyTypes();
            IOrganizationService organizationService = (IOrganizationService)organizationServiceProxy;

            crmServiceContext = new CrmServiceContext(organizationService);
            organizationServiceContext = new OrganizationServiceContext(organizationService);

            initialized = true;
        }

        public static bool Initialized
        {
            get
            {
                return initialized;
            }
        }

        public static CrmServiceContext ServiceContext
        {
            get
            {
                return crmServiceContext;
            }
        }
    }
}
