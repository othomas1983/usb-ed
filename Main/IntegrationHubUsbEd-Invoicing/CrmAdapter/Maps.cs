﻿using System.Collections.Generic;
using System.Web.Script.Serialization;
using StellenboschUniversity.UsbEd.Integration.Adapters.Crm.Proxy;
using DtoInvoicingModel = StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Invoicing;
using DtoDebtorsModel = StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Debtors;
using System.Linq;

namespace StellenboschUniversity.UsbEd.Integration.Adapters.Crm
{
    public static class Maps
    {
        public static DtoInvoicingModel.PaymentSchedule ToDto(this string item)
        {
            var ser = new JavaScriptSerializer();

            return ser.Deserialize<DtoInvoicingModel.PaymentSchedule>(item);
        }

        public static T ToDto<T>(this string item)
        {
            var ser = new JavaScriptSerializer();

            return ser.Deserialize<T>(item);
        }

        public static string ToDto(this DtoInvoicingModel.PaymentScheduleJsonDto item)
        {
            var ser = new JavaScriptSerializer();

            return ser.Serialize(item);
        }

        public static DtoInvoicingModel.JsonDatatable ToDto(this IList<DtoInvoicingModel.PaymentSchedule> items)
        {
            var jsonTable = new DtoInvoicingModel.JsonDatatable();

            foreach (var item in items)
            {
                var row = new List<object>();

                row.Add(item.Date.ToShortDateString());
                //NOTE: replace is used for jQuery format currency plugin to function correctly
                row.Add(item.Amount.ToString("C").Replace(",", "."));
                row.Add(item.Description.ToString("P"));

                var paymentScheduleJsonDto = item.ToDto();

                row.Add(string.Format("<button id='edit{0}' class='edit'>Edit</button><input id='hiddenEdit{0}' type='hidden' value='{1}' />", item.Id.ToString(), paymentScheduleJsonDto.ToDto()));

                row.Add(string.Format("<button id='delete{0}' class='delete'>Delete</button><input id='hiddenDelete{0}' type='hidden' value='{0}' />", item.Id.ToString()));

                jsonTable.aaData.Add(row);
            }

            return jsonTable;
        }

        public static DtoInvoicingModel.JsonDatatable ToDto(this IList<DtoInvoicingModel.PaymentScheduleReport> items)
        {
            var jsonTable = new DtoInvoicingModel.JsonDatatable();

            foreach (var item in items)
            {
                var row = new List<object>();

                row.Add(item.ScheduleDate.ToShortDateString());
                row.Add(item.ProgrammeOffering);
                row.Add(item.ProgrammeManager);
                row.Add(item.Debtor);

                //NOTE: replace is used for jQuery format currency plugin to function correctly
                row.Add(item.Amount.ToString("C").Replace(",", "."));

                jsonTable.aaData.Add(row);
            }

            return jsonTable;
        }

        public static DtoInvoicingModel.PaymentScheduleJsonDto ToDto(this DtoInvoicingModel.PaymentSchedule item)
        {
            var paymentScheduleJsonDto = new DtoInvoicingModel.PaymentScheduleJsonDto();

            paymentScheduleJsonDto.Id = item.Id.ToString();
            //NOTE: hack - json serialiser does not handle date parsing
            paymentScheduleJsonDto.Date = item.Date.ToShortDateString().Replace("/", "##");
            //NOTE: replace is used for jQuery format currency plugin to function correctly
            paymentScheduleJsonDto.Amount = item.Amount.ToString("C").Replace(",", ".");
            paymentScheduleJsonDto.Description = item.Description.ToString("P");

            return paymentScheduleJsonDto;
        }

        public static DtoDebtorsModel.Debtor ToDto(this Account item)
        {
            var debtor = new DtoDebtorsModel.Debtor();

            debtor.CrmId = item.AccountId;
            debtor.Name = item.Name;
            
            return debtor;
        }

        public static DtoInvoicingModel.ProgrammeManager ToDto(this SystemUser item)
        {
            var programmeManager = new DtoInvoicingModel.ProgrammeManager();

            programmeManager.Id = item.Id;
            programmeManager.Fullname = item.FullName;
            programmeManager.Email = item.InternalEMailAddress;

            return programmeManager;
        }


    }
}
