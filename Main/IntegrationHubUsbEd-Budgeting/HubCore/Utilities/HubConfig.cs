﻿using System;
using System.Configuration;

namespace StellenboschUniversity.UsbEd.Integration.Core
{
    public static class HubConfig
    {
        #region Locals

        private const string _CONNECTIONSTRINGNAME = "OpsHubEntities";
        private const string _SHAREPOINTODATAURI = "SharePointODataUri";
        private const string _NETWORKDOMAIN = "NetworkDomain";
        private const string _SHAREPOINTUSER = "SharePointNetworkCredentialUser";
        private const string _SHAREPOINTPASSWORD = "SharePointNetworkCredentialPassword";
        private const string _CRMODATAURI = "CrmODataUri";
        private const string _CRMUSER = "CrmNetworkCredentialUser";
        private const string _CRMPASSWORD = "CrmNetworkCredentialPassword";
        private const string _BUDGETWORKSHEETNAME = "BudgetWorkSheetName";
        private const string _BUDGETTARGETCOLUMN = "BudgetTargetColumn";
        private const string _BUDGETENDMARKER = "BudgetEndMarker";
        private const string _BUDGETTEMPPATH = "BudgetTempPath";
        private const string _BUDGETDOWNLOADPROTOCOL = "BudgetDownloadProtocol";
        private const string _BUDGETVALIDATORCRMREPLACETEXT = "BudgetValidatorCrmReplaceText";
        private const string _BUDGETGLFORMATTEDCHAR = "BudgetGLFormattedChar";
        private const string _LOGGERLINESEPERATOR = "LoggerLineSeperator";
        private const string _PAYMENTSCHEDULEEMAILSUBJECT = "PaymentScheduleEmailSubject";
        private const string _INTEGRATIONPOLLINGINTERVAL = "IntegrationPollingInterval";
        private const string _IMPORTTIME = "ImportTime";

        #endregion

        #region Properties

        public static string OpsHubConnectionString
        {
            get 
            {
                return ConfigurationManager.ConnectionStrings[_CONNECTIONSTRINGNAME].ConnectionString;
            }
        }
        public static string SharePointODataUri
        {
            get
            {
                return ConfigurationManager.AppSettings[_SHAREPOINTODATAURI];
            }
        }
        public static string NetworkDomain
        {
            get
            {
                return ConfigurationManager.AppSettings[_NETWORKDOMAIN];
            }
        }
        public static string SharepointUser
        {
            get
            {
                return ConfigurationManager.AppSettings[_SHAREPOINTUSER];
            }
        }
        public static string SharepointPassword
        {
            get
            {
                return ConfigurationManager.AppSettings[_SHAREPOINTPASSWORD];
            }
        }
        public static string CrmODataUri
        {
            get
            {
                return ConfigurationManager.AppSettings[_CRMODATAURI];
            }
        }
        public static string CrmUser
        {
            get
            { 
                return ConfigurationManager.AppSettings[_CRMUSER];
            }
        }
        public static string CrmPassword
        {
            get
            {
                return ConfigurationManager.AppSettings[_CRMPASSWORD];
            }
        }
        public static string BudgetWorkSheetName
        {
            get
            {
                return ConfigurationManager.AppSettings[_BUDGETWORKSHEETNAME];
            }
        }
        public static string BudgetTargetColumn
        {
            get
            {
                return ConfigurationManager.AppSettings[_BUDGETTARGETCOLUMN];
            }
        }
        public static string BudgetEndMarker
        {
            get
            {
                return ConfigurationManager.AppSettings[_BUDGETENDMARKER];
            }
        }
        public static string BudgetTempPath
        {
            get
            {
                return ConfigurationManager.AppSettings[_BUDGETTEMPPATH];
            }
        }
        public static string BudgetDownloadProtocol
        {
            get
            {
                return ConfigurationManager.AppSettings[_BUDGETDOWNLOADPROTOCOL];
            }
        }
        public static string BudgetValidatorCrmReplaceText
        {
            get
            {
                return ConfigurationManager.AppSettings[_BUDGETVALIDATORCRMREPLACETEXT];
            }
        }
        public static string BudgetGLFormattedString
        {
            get
            {
                return ConfigurationManager.AppSettings[_BUDGETGLFORMATTEDCHAR];
            }
        }
        public static char BudgetGLFormattedChar
        {
            get
            {
                return ConfigurationManager.AppSettings[_BUDGETGLFORMATTEDCHAR].ToChar();
            }
        }
        public static string LoggerLineSeperator
        {
            get
            {
                return ConfigurationManager.AppSettings[_LOGGERLINESEPERATOR];
            }
        }
        public static string PaymentScheduleEmailSubject
        {
            get
            { 
                return ConfigurationManager.AppSettings[_PAYMENTSCHEDULEEMAILSUBJECT];
            }
        }
        public static TimeSpan IntegrationPollingInterval
        {
            get
            {
                return TimeSpan.Parse(ConfigurationManager.AppSettings[_INTEGRATIONPOLLINGINTERVAL]);
            }
        }
        public static TimeSpan ImportTime
        {
            get
            {
                return TimeSpan.Parse(ConfigurationManager.AppSettings[_IMPORTTIME]);
            }
        }

        #endregion
    }
}
