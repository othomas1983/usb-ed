﻿using System;
using System.ServiceModel;
using System.ServiceProcess;
using System.Threading;
using NLog;
using StellenboschUniversity.UsbEd.Integration.Adapters.Crm;
using StellenboschUniversity.UsbEd.Integration.Adapters.Ods;
using StellenboschUniversity.UsbEd.Integration.Core;
using StellenboschUniversity.UsbEd.Integration.HubAgent;

namespace StellenboschUniversity.UsbEd.Integration.WindowsServiceHost
{
    public class WindowsServiceHost : ServiceBase
    {
        #region Locals

        public ServiceHost serviceHostBudget = null;
        public ServiceHost serviceHostInvoice = null;
        public ServiceHost serviceHostDebtor = null;
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private static TimeSpan integrationInterval;
        private static TimeSpan importTime;
        private Agent agent;

        #endregion

        #region Constructor

        public WindowsServiceHost()
        {
            // Name the Windows Service
            ServiceName = "Integration Hub Service";
            EventLog.Log = "Application";

            integrationInterval = HubConfig.IntegrationPollingInterval;
            importTime = HubConfig.ImportTime;

            agent = new Agent(integrationInterval, importTime);
        }

        #endregion

        #region Events

        // Start the Windows service.
        protected override void OnStart(string[] args)
        {
            CrmAdapter.Initialize();
            OdsAdapter.Initialize();
            
            if (serviceHostBudget != null)
            {
                serviceHostBudget.Close();
            }

            if (serviceHostInvoice != null)
            {
                serviceHostInvoice.Close();
            }

            if (serviceHostDebtor != null)
            {
                serviceHostDebtor.Close();
            }

            serviceHostBudget = new ServiceHost(typeof(HubService.BudgetingService));
            serviceHostInvoice = new ServiceHost(typeof(HubService.InvoicingService));
            serviceHostDebtor = new ServiceHost(typeof(HubService.DebtorService));

            serviceHostBudget.Open();
            serviceHostInvoice.Open();
            serviceHostDebtor.Open();

            //starts the hub agent
            agent.Start();

            logger.Log(LogLevel.Info, "Integration Hub Service Started...{0}{1}", Environment.NewLine, HubConfig.LoggerLineSeperator);
        }

        protected override void OnPause()
        {
            logger.Log(LogLevel.Info, "Integration Hub Service Paused...{0}{1}", Environment.NewLine, HubConfig.LoggerLineSeperator);
        }

        protected override void OnContinue()
        {
            logger.Log(LogLevel.Info, "Integration Hub Service Unpaused...{0}{1}", Environment.NewLine, HubConfig.LoggerLineSeperator);
        }

        protected override void OnStop()
        {
            if (serviceHostBudget != null)
            {
                serviceHostBudget.Close();
                serviceHostBudget = null;
            }
            
            if (serviceHostInvoice != null)
            {
                serviceHostInvoice.Close();
                serviceHostInvoice = null;
            }

            if (serviceHostDebtor != null)
            {
                serviceHostDebtor.Close();
                serviceHostDebtor = null;
            }

            //stops the hub agent
            agent.ShutDown();

            Thread.Sleep(5000);

            //while (agent.HasShutDown == false)
            //{
            //    Thread.Sleep(2000);
            //}

            logger.Log(LogLevel.Info, "Integration Hub Service Stopped...{0}{1}", Environment.NewLine, HubConfig.LoggerLineSeperator);
        }

        #endregion

        #region Public Methods
        
        public static void Main(string[] args)
        {
            ServiceBase.Run(new WindowsServiceHost());
        }

        #endregion
    }
}
