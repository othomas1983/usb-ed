﻿using System;
using System.Web.Script.Serialization;
using SharepointAdapter.SharepointOData;
using StellenboschUniversity.UsbEd.Integration.Core;
using StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Budget;
using StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Enum;

namespace StellenboschUniversity.UsbEd.Integration.Adapters.Sharepoint
{
    public static class Maps
    {
        public static BudgetSet ToDto(this ProgrammeOfferingItem item)
        {
            var budgetSet = new BudgetSet();

            budgetSet.SharepointId = item.Id;
            budgetSet.CrmUserId = Guid.Empty;
            budgetSet.Set = item.BudgetSetNumber.ToInt();
            budgetSet.StartPeriod = item.StartingPeriod.ToInt();
            budgetSet.Title = item.Title;
            budgetSet.FileName = item.Name;
            budgetSet.FilePath = item.Path;
            budgetSet.DateCreated = item.Created.HasValue ? item.Created : null;
            budgetSet.UserCreatedId = item.CreatedById.HasValue ? item.CreatedById : null;
            budgetSet.DateModified = item.Modified.HasValue ? item.Modified : null;
            budgetSet.UserModifiedId = item.ModifiedById.HasValue ? item.ModifiedById : null;
            budgetSet.Status = item.BudgetStatusValue.IsNotNull() ? item.BudgetStatusValue.ToEnum<BudgetSetStatus>() : BudgetSetStatus.Draft;

            budgetSet.GeneralLedgerCodeCollection = SharepointWrapper.BuildGeneralLedgerCodes(item);

            return budgetSet;
        }

        public static BudgetMessage ToDto(this string item)
        {
            var ser = new JavaScriptSerializer();

            return ser.Deserialize<BudgetMessage>(item);
        }
    }
}
