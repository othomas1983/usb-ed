﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Debtors
{
    public class Debtor
    {
        public int OdsId { get; set; }

        public string DebtorCode { get; set; }

        public Guid? CrmId { get; set; }

        public string Name { get; set; }

        public string GroupCode { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string AddressLine3 { get; set; }

        public string AddressLine4 { get; set; }

        public string City { get; set; }

        public string StateProvince { get; set; }

        public string PostalCode { get; set; }

        public string Country { get; set; }

        public string ContactName { get; set; }

        public string Telephone { get; set; }

        public string Fax { get; set; }

        public int TaxGroup { get; set; }

        public string TaxRegistrationNumber { get; set; }

        public int TaxClass { get; set; }

        public string ContactEmail { get; set; }

        public string Email { get; set; }

        public string ContactTelephone { get; set; }

        public string ContactFax { get; set; }

        public string DebtorCodePrefix { get; set; }

        public string CurrencySymbol { get; set; }

        public string CurrencyCode { get; set; }
    }
}
