﻿using System;

namespace StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Invoicing
{
    public class PaymentSchedule
    {
        public int Id { get; set; }

        public Guid ProgrammeOfferingId { get; set; }

        public decimal Description { get; set; }

        public DateTime Date { get; set; }

        public decimal Amount { get; set; }

        public Guid TaskActivityId { get; set; }

        public PaymentSchedule()
        { 
        
        }
    }
}
