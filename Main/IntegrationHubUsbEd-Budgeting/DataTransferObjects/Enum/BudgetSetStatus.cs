﻿namespace StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Enum
{
    /// <summary>
    /// Summary description for BudgetSetStatus
    /// </summary>
    public enum BudgetSetStatus
    {
        Draft = 0,
        Approved = 1,
        Final = 2
    }
}