﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Enum
{
    public enum AccpacIntegrationStatus
    {
        NotYetSentToAccpac = 100000000,
        SentToAccpac,
        IntegratedWithAccpac
    }
}
