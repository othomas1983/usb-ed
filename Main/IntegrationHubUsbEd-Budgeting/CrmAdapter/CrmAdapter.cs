﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.ServiceModel.Description;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using NLog;
using StellenboschUniversity.UsbEd.Integration.Adapters.Crm.Proxy;
using StellenboschUniversity.UsbEd.Integration.Adapters.Crm.Wrapper;
using StellenboschUniversity.UsbEd.Integration.Adapters.Ods;
using StellenboschUniversity.UsbEd.Integration.Core;
using DtoDebtorsModel = StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Debtors;
using DtoInvoicingModel = StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Invoicing;
using StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Enum;


namespace StellenboschUniversity.UsbEd.Integration.Adapters.Crm
{
    public static class CrmAdapter
    {
        #region Locals

        private static Logger logger = LogManager.GetCurrentClassLogger();

        #endregion

        #region Initialize

        public static void Initialize(Uri crmUri, ClientCredentials credentials)
        {
            CrmWrapper.Initialize(crmUri, credentials);
        }

        public static void Initialize()
        {
            try
            {
                var credentials = new ClientCredentials();
                credentials.Windows.ClientCredential.Domain = HubConfig.NetworkDomain;
                credentials.Windows.ClientCredential.UserName = HubConfig.CrmUser;
                credentials.Windows.ClientCredential.Password = HubConfig.CrmPassword;

                CrmWrapper.Initialize(new Uri(HubConfig.CrmODataUri), credentials);
                OdsAdapter.Initialize();

            }
            catch (Exception ex)
            {
                logger.Log(LogLevel.Trace, "Message: {0}{3}Stack Trace: {1}{3}Inner Exception: {2}{3}", ex.Message, ex.StackTrace, ex.InnerException, Environment.NewLine);
            }
        }


        #endregion

        #region Budget

        public static Guid GetProgrammeOfferingId(string folderName)
        {
            var sharePointDocumentLocationSet = CrmWrapper.ServiceContext.SharePointDocumentLocationSet.Where(p => p.RelativeUrl == folderName).First();

            return sharePointDocumentLocationSet.RegardingObjectId.Id;
        }

        #endregion

        #region Debtors

        public static void SaveDebtorToCrm(DtoDebtorsModel.Debtor debtor)
        {
            Account crmDebtor = new Account();

            crmDebtor.AccountId = debtor.CrmId;
            crmDebtor.AccountNumber = debtor.DebtorCode;
            crmDebtor.Name = debtor.Name;
            crmDebtor.Address1_Line1 = debtor.AddressLine1;
            crmDebtor.Address1_Line2 = debtor.AddressLine2;
            crmDebtor.Address1_Line3 = debtor.AddressLine3;
            crmDebtor.LT_Address1_line4 = debtor.AddressLine4;
            crmDebtor.Address1_City = debtor.City;
            crmDebtor.Address1_StateOrProvince = debtor.StateProvince;
            crmDebtor.Address1_PostalCode = debtor.PostalCode;
            crmDebtor.Address1_Country = debtor.Country;
            crmDebtor.Address1_PrimaryContactName = debtor.ContactName;
            crmDebtor.Telephone1 = debtor.Telephone;
            crmDebtor.Fax = debtor.Fax;
            crmDebtor.EMailAddress1 = debtor.Email;
            crmDebtor.EMailAddress2 = debtor.ContactEmail;
            crmDebtor.Telephone2 = debtor.ContactTelephone;
            crmDebtor.Address1_Fax = debtor.ContactFax;
            crmDebtor.lt_debtorcodeprefix = debtor.DebtorCodePrefix;
            crmDebtor.lt_currencysymbol = debtor.CurrencySymbol;
            crmDebtor.lt_currencycode = debtor.CurrencyCode;

            if (crmDebtor.AccountId.HasValue == true)
            {
                CrmWrapper.ServiceContext.Update(crmDebtor);
            }
            else
            {
                CrmWrapper.ServiceContext.Create(crmDebtor);
            }
        }

        public static DtoDebtorsModel.Debtor LoadDebtorFromCrm(Guid crmId)
        {
            var debtor = new DtoDebtorsModel.Debtor();
            var crmDebtor = CrmWrapper.ServiceContext.AccountSet.Where(p => p.AccountId == crmId).First();

            debtor.CrmId = crmDebtor.AccountId;
            debtor.DebtorCode = crmDebtor.AccountNumber;
            debtor.Name = crmDebtor.Name;
            debtor.TaxRegistrationNumber = crmDebtor.LT_VATNumber;

            if (crmDebtor.lt_GroupCodeId != null)
            {
                debtor.GroupCode = GetGroupCode(crmDebtor.lt_GroupCodeId.Id);
            }

            debtor.AddressLine1 = crmDebtor.Address1_Line1;
            debtor.AddressLine2 = crmDebtor.Address1_Line2;
            debtor.AddressLine3 = crmDebtor.Address1_Line3;
            debtor.AddressLine4 = crmDebtor.LT_Address1_line4;
            debtor.City = crmDebtor.Address1_City;
            debtor.StateProvince = crmDebtor.Address1_StateOrProvince;
            debtor.PostalCode = crmDebtor.Address1_PostalCode;
            debtor.Country = crmDebtor.Address1_Country;

            debtor.ContactName = crmDebtor.Address1_PrimaryContactName;
            debtor.Telephone = crmDebtor.Telephone1;
            debtor.Fax = crmDebtor.Fax;
            debtor.Email = crmDebtor.EMailAddress1;
            debtor.ContactEmail = crmDebtor.EMailAddress2;
            debtor.ContactTelephone = crmDebtor.Telephone2;
            debtor.ContactFax = crmDebtor.Address1_Fax;

            debtor.DebtorCodePrefix = crmDebtor.lt_debtorcodeprefix;
            debtor.CurrencySymbol = crmDebtor.lt_currencysymbol;
            debtor.CurrencyCode = crmDebtor.lt_currencycode;

            return debtor;
        }

        public static void UpdateDebtorCode(Guid crmId, string debtorCode)
        {
            var crmDebtor = CrmWrapper.ServiceContext.AccountSet.Where(p => p.AccountId.Value == crmId).First();

            crmDebtor.AccountNumber = debtorCode;

            CrmWrapper.ServiceContext.UpdateObject(crmDebtor);
            CrmWrapper.ServiceContext.SaveChanges();
        }

        public static string GetDebtorCode(Guid crmId)
        {
            var crmDebtor = CrmWrapper.ServiceContext.AccountSet.Where(p => p.Id == crmId).First();

            return crmDebtor.AccountNumber;
        }

        public static void UpdateDebtorAccpacIntegrationStatus(Guid crmId, AccpacIntegrationStatus integrationStatus)
        {
            var crmDebtor = CrmWrapper.ServiceContext.AccountSet.Where(p => p.AccountId.Value == crmId).First();

            crmDebtor.lt_AccPacIntegrationStatus = (int)integrationStatus;

            CrmWrapper.ServiceContext.UpdateObject(crmDebtor);
            CrmWrapper.ServiceContext.SaveChanges();
        }

        public static void UpdateDebtorCurrencyInformation(Guid crmId, string currencyCode, string currencySymbol)
        {
            var crmDebtor = CrmWrapper.ServiceContext.AccountSet.Where(p => p.AccountId.Value == crmId).First();

            crmDebtor.lt_currencycode = currencyCode;
            crmDebtor.lt_currencysymbol = currencySymbol;

            CrmWrapper.ServiceContext.UpdateObject(crmDebtor);
            CrmWrapper.ServiceContext.SaveChanges();
        }

        public static DtoDebtorsModel.Debtor GetDebtor(string debtorCode)
        {
            var crmDebtors = CrmWrapper.ServiceContext.AccountSet.Where(p => p.AccountNumber == debtorCode).ToList();

            if (crmDebtors.Count == 0)
            {
                return null;
            }
            else
            {
                if (crmDebtors.Count == 1)
                {
                    return LoadDebtorFromCrm(crmDebtors[0].AccountId.Value);
                }
                else
                {
                    throw new Exception("More than one debtor with the same debtor code on CRM.");
                }
            }
        }

        public static IList<DtoDebtorsModel.Debtor> GetAllDebtorsForProgrammeOffering(Guid programmeOfferingId)
        {
            var debtors = (from a in CrmWrapper.ServiceContext.AccountSet
                           join s in CrmWrapper.ServiceContext.Lt_studentacademicrecordSet on a.AccountId equals s.lt_AccountNumber.Id
                           where s.lt_academicprogrammeofferingid.Id == programmeOfferingId
                           select a.AccountId).ToList();


            var results = new List<DtoDebtorsModel.Debtor>();

            foreach (var item in debtors)
            {
                results.Add(GetDebtor(item.Value));
            }

            return results;

        }

        public static DtoDebtorsModel.Debtor GetDebtor(Guid debtorId)
        {
            var debtor = CrmWrapper.ServiceContext.AccountSet.Where(p => p.AccountId == debtorId).First();

            return debtor.ToDto();
        }

        public static string GetGroupCode(Guid groupCodeId)
        {
            var groupCode = CrmWrapper.ServiceContext.lt_accountgroupcodeSet.Where(p => p.lt_accountgroupcodeId.Value == groupCodeId).First();

            return groupCode.lt_name;
        }

        public static bool DebtorExists(Guid debtorId)
        {
            var debtor = CrmWrapper.ServiceContext.AccountSet.Where(p => p.AccountId == debtorId).FirstOrDefault();

            if (debtor == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        #endregion

        #region Invoicing

        public static StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Invoicing.Invoice LoadInvoiceFromCrm(Guid crmId)
        {
            var invoice = new StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Invoicing.Invoice();
            var crmInvoice = CrmWrapper.ServiceContext.InvoiceSet.Where(p => p.InvoiceId.Value == crmId).First();
            var crmInvoiceLineItems = CrmWrapper.ServiceContext.InvoiceDetailSet.Where(p => p.InvoiceId.Id == crmId);

            invoice.CrmId = crmInvoice.InvoiceId.Value;
            invoice.Amount = crmInvoice.TotalLineItemAmount ?? 0;
            invoice.CreationDate = crmInvoice.CreatedOn.Value;
            invoice.DueDate = crmInvoice.DueDate;
            invoice.DebtorCode = GetDebtorCode(crmInvoice.CustomerId.Id);
            invoice.Description = crmInvoice.Description ?? "";
            invoice.Header1 = CrmWrapper.ServiceContext.lt_programmeuserSet.Where(p => p.lt_programmeuserId == crmInvoice.lt_lt_programmeuser_invoice_BDAquired.Id).First().lt_accpaccode;
            invoice.Header2 = CrmWrapper.ServiceContext.lt_programmeuserSet.Where(p => p.lt_programmeuserId == crmInvoice.lt_lt_programmeuser_invoice_BusinessDeveloper.Id).First().lt_accpaccode;
            invoice.Header3 = null;
            invoice.Header4 = null;
            invoice.Header5 = null;
            
            string programmeManagerCode = CrmWrapper.ServiceContext.lt_programmeuserSet.Where(p => p.lt_programmeuserId == crmInvoice.lt_lt_programmeuser_invoice_ProgrammeManager.Id).First().lt_accpaccode;

            foreach (var crmInvoiceLineItem in crmInvoiceLineItems)
            {
                invoice.LineItems.Add(LoadInvoiceLineItemFromCrm(crmInvoiceLineItem, programmeManagerCode));
            }

            return invoice;
        }

        public static StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Invoicing.InvoiceLineItem LoadInvoiceLineItemFromCrm(InvoiceDetail crmInvoiceLineItem, string programmeManagerCode)
        {
            var invoiceLineItem = new StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Invoicing.InvoiceLineItem();

            invoiceLineItem.Amount = crmInvoiceLineItem.BaseAmount ?? 0;
            invoiceLineItem.GlAccount = CrmWrapper.ServiceContext.lt_glaccountSet.Where(p => p.lt_glaccountId == crmInvoiceLineItem.lt_GLCodeId.Id).First().lt_name;
            invoiceLineItem.Comment = crmInvoiceLineItem.lt_CommentField ?? "";
            invoiceLineItem.Description = crmInvoiceLineItem.Description ?? "";
            invoiceLineItem.Taxable = crmInvoiceLineItem.lt_Taxable.Value;
            invoiceLineItem.AdditionalField1 = programmeManagerCode;
            invoiceLineItem.AdditionalField2 = null;
            invoiceLineItem.AdditionalField3 = null;
            invoiceLineItem.AdditionalField4 = null;
            invoiceLineItem.AdditionalField5 = null;

            return invoiceLineItem;
        }

        public static void UpdateInvoiceBalance(Guid crmId, decimal balance)
        {
            var crmInvoice = CrmWrapper.ServiceContext.InvoiceSet.Where(p => p.InvoiceId.Value == crmId).First();

            crmInvoice.lt_OutstandingBalance = balance;

            CrmWrapper.ServiceContext.UpdateObject(crmInvoice);
            CrmWrapper.ServiceContext.SaveChanges();
        }

        public static void UpdateInvoiceNumber(Guid crmId, string invoiceNumber)
        {
            var crmInvoice = CrmWrapper.ServiceContext.InvoiceSet.Where(p => p.InvoiceId.Value == crmId).First();

            crmInvoice.Name = invoiceNumber;

            CrmWrapper.ServiceContext.UpdateObject(crmInvoice);
            CrmWrapper.ServiceContext.SaveChanges();
        }

        public static void UpdateInvoiceAccpacIntegrationStatus(Guid crmId, AccpacIntegrationStatus integrationStatus)
        {
            var crmInvoice = CrmWrapper.ServiceContext.InvoiceSet.Where(p => p.InvoiceId.Value == crmId).First();

            crmInvoice.lt_AccPacIntegrationStatus = (int)integrationStatus;

            CrmWrapper.ServiceContext.UpdateObject(crmInvoice);
            CrmWrapper.ServiceContext.SaveChanges();
        }

        #endregion

        #region Credit Notes

        public static StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Invoicing.CreditNote LoadCreditNoteFromCrm(Guid crmId)
        {
            var creditNote = new StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Invoicing.CreditNote();
            var crmCreditNote = CrmWrapper.ServiceContext.lt_creditnoteSet.Where(p => p.lt_creditnoteId.Value == crmId).First();

            creditNote.Amount = crmCreditNote.lt_TotalDetailAmount ?? 0;
            creditNote.CrmId = crmCreditNote.lt_creditnoteId.Value;
            creditNote.Description = crmCreditNote.lt_Description ?? "";
            creditNote.Date = crmCreditNote.CreatedOn.Value;
            creditNote.Taxable = crmCreditNote.lt_Taxable.Value;
            creditNote.Header1 = null;
            creditNote.Header2 = null;
            creditNote.Header3 = null;             
            creditNote.Header4 = null;             
            creditNote.Header5 = null;
            creditNote.AdditionalField1 = null;
            creditNote.AdditionalField2 = null;
            creditNote.AdditionalField3 = null;
            creditNote.AdditionalField4 = null;
            creditNote.AdditionalField5 = null;

            if (crmCreditNote.lt_GlAccount.IsNotNull())
            {
                creditNote.GlAccount = CrmWrapper.ServiceContext.lt_glaccountSet.Where(p => p.lt_glaccountId == crmCreditNote.lt_GlAccount.Id).First().lt_name;
            }
            
            if (crmCreditNote.lt_CreditNoteType != null)
            {
                switch (crmCreditNote.lt_CreditNoteType.Value)
                {
                    case 100000000:
                        creditNote.Type = DataTransferObjects.Enum.CreditNoteType.Discount;
                        break;

                    case 100000001:
                        creditNote.Type = DataTransferObjects.Enum.CreditNoteType.Cancellation;
                        break;

                    case 100000002:
                        creditNote.Type = DataTransferObjects.Enum.CreditNoteType.Cancellation;
                        break;

                    default:
                        break;
                }
            }

            if (crmCreditNote.lt_InvoiceReference != null)
            {
                creditNote.CrmRelatedInvoiceId = crmCreditNote.lt_InvoiceReference.Id;
                
                StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Invoicing.Invoice invoice = LoadInvoiceFromCrm(crmCreditNote.lt_InvoiceReference.Id);
                creditNote.DebtorCode = invoice.DebtorCode;
            }

            return creditNote;
        }

        public static bool CreditNoteExists(string creditNoteNumber)
        {
            if (CrmWrapper.ServiceContext.lt_creditnoteSet.Where(p => p.lt_name == creditNoteNumber).ToList().Count() != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static void UpdateCreditNoteNumber(Guid crmId, string creditNoteNumber)
        {
            var crmCreditNote = CrmWrapper.ServiceContext.lt_creditnoteSet.Where(p => p.lt_creditnoteId.Value == crmId).First();

            crmCreditNote.lt_name = creditNoteNumber;

            CrmWrapper.ServiceContext.UpdateObject(crmCreditNote);
            CrmWrapper.ServiceContext.SaveChanges();
        }

        public static void UpdateCreditNoteAccpacIntegrationStatus(Guid crmId, AccpacIntegrationStatus integrationStatus)
        {
            var crmCreditNote = CrmWrapper.ServiceContext.lt_creditnoteSet.Where(p => p.lt_creditnoteId.Value == crmId).First();

            crmCreditNote.lt_AccPacIntegrationStatus = (int)integrationStatus;

            CrmWrapper.ServiceContext.UpdateObject(crmCreditNote);
            CrmWrapper.ServiceContext.SaveChanges();
        }

        public static void CreateDebitNote(string debitNoteNumber, Guid invoiceId, decimal amount, string glCode)
        {
            lt_creditnote debitNote = new lt_creditnote();

            debitNote.lt_name = debitNoteNumber;
            debitNote.lt_TotalDetailAmount = -amount;
            debitNote.lt_InvoiceReference = new Microsoft.Xrm.Client.CrmEntityReference("invoice", invoiceId);
            debitNote.lt_CreditNoteType = 100000003;        // "debit note" option set value
            debitNote.lt_AccPacIntegrationStatus = (int)AccpacIntegrationStatus.IntegratedWithAccpac;
            debitNote.lt_CreditNoteStatus = 100000001;      // "complete" option set value
            debitNote.lt_GlAccount = new Microsoft.Xrm.Client.CrmEntityReference("lt_creditnote", GetGLAccount(glCode));

            CrmWrapper.ServiceContext.AddObject(debitNote);
            CrmWrapper.ServiceContext.SaveChanges();
        }

        #endregion

        #region Payment schedule

        public static void CreatePaymentSchedule(string message)
        {
            try
            {
                var paymentScheduleItem = message.ToDto();

                paymentScheduleItem.TaskActivityId = CreateTaskActivity(paymentScheduleItem);

                OdsAdapter.CreatePaymentSchedule(paymentScheduleItem);

                OdsAdapter.RectifyPaymentSchedule(paymentScheduleItem.ProgrammeOfferingId);

            }
            catch (Exception ex)
            {
                logger.Log(LogLevel.Trace, "Message: {0}{3}Stack Trace: {1}{3}Inner Exception: {2}{3}", ex.Message, ex.StackTrace, ex.InnerException, Environment.NewLine);
            }
        }

        public static void UpdatePaymentSchedule(string message)
        {
            try
            {
                var paymentScheduleItem = message.ToDto();

                paymentScheduleItem = OdsAdapter.UpdatePaymentSchedule(paymentScheduleItem);

                OdsAdapter.RectifyPaymentSchedule(paymentScheduleItem.ProgrammeOfferingId);

                UpdateTaskActivity(paymentScheduleItem);

            }
            catch (Exception ex)
            {
                logger.Log(LogLevel.Trace, "Message: {0}{3}Stack Trace: {1}{3}Inner Exception: {2}{3}", ex.Message, ex.StackTrace, ex.InnerException, Environment.NewLine);
            }
        }

        public static DtoInvoicingModel.JsonDatatable GetPaymentSchedule(string message)
        {
            try
            {
                var paymentSchedule = OdsAdapter.GetPaymentSchedule(message.ToGuid());

                return paymentSchedule.ToDto();

            }
            catch (Exception ex)
            {
                logger.Log(LogLevel.Trace, "Message: {0}{3}Stack Trace: {1}{3}Inner Exception: {2}{3}", ex.Message, ex.StackTrace, ex.InnerException, Environment.NewLine);

                return null;
            }
        }

        public static string GetPaymentScheduleTotal(string message)
        {
            try
            {
                var totalAmount = OdsAdapter.GetPaymentScheduleTotal(message.ToGuid());

                //NOTE: replace is used for jQuery format currency plugin to function correctly
                return totalAmount.ToString("C").Replace(",", ".");

            }
            catch (Exception ex)
            {
                logger.Log(LogLevel.Trace, "Message: {0}{3}Stack Trace: {1}{3}Inner Exception: {2}{3}", ex.Message, ex.StackTrace, ex.InnerException, Environment.NewLine);

                return null;
            }
        }

        public static void DeletePaymentSchedule(string message)
        {
            try
            {
                var programmeOffering = OdsAdapter.DeletePaymentSchedule(message.ToInt());

                OdsAdapter.RectifyPaymentSchedule(programmeOffering.ProgrammeOfferingID.Value);

                DeleteTaskActivity(programmeOffering.TaskActivityID.Value);                
            }
            catch (Exception ex)
            {
                logger.Log(LogLevel.Trace, "Message: {0}{3}Stack Trace: {1}{3}Inner Exception: {2}{3}", ex.Message, ex.StackTrace, ex.InnerException, Environment.NewLine);
            }
        }

        public static DtoInvoicingModel.JsonDatatable GetPaymentScheduleReport(string message)
        {
            try
            {
                var paymentScheduleReportMessage = message.ToDto<DtoInvoicingModel.PaymentScheduleReportMessage>();

                var paymentScheduleItems = OdsAdapter.GetPaymentScheduleItems(paymentScheduleReportMessage.StartDate, paymentScheduleReportMessage.EndDate);

                //re-order payment scedule items by date
                paymentScheduleItems = paymentScheduleItems.OrderBy(p => p.Date).ToList();

                var programmeOfferingDistinctList = paymentScheduleItems.GroupBy(p => p.ProgrammeOfferingId).ToList();

                var reportList = new List<DtoInvoicingModel.PaymentScheduleReport>();

                foreach (var item in programmeOfferingDistinctList)
                {
                    //get all debtors/sar records for programme offering
                    var debtorList = CrmAdapter.GetAllDebtorsForProgrammeOffering(item.Key);

                    //get debtor list for programme offering
                    var debtorDistinctList = debtorList.GroupBy(p => p.CrmId).ToList();

                    //get payment schedule for programme offering
                    var paymentScheduleList = paymentScheduleItems.Where(p => p.ProgrammeOfferingId == item.Key).ToList();

                    BuildPaymentScheduleReport(ref reportList, debtorList, debtorDistinctList, paymentScheduleList);
                }

                return reportList.ToDto();
            }
            catch (Exception ex)
            {
                logger.Log(LogLevel.Trace, "Message: {0}{3}Stack Trace: {1}{3}Inner Exception: {2}{3}", ex.Message, ex.StackTrace, ex.InnerException, Environment.NewLine);

                return null;
            }
        }

        private static void BuildPaymentScheduleReport(ref List<DtoInvoicingModel.PaymentScheduleReport> reportList, IList<DtoDebtorsModel.Debtor> debtorList, List<IGrouping<Guid?, DtoDebtorsModel.Debtor>> debtorDistinctList, List<DtoInvoicingModel.PaymentSchedule> paymentScheduleList)
        {
            foreach (var schedule in paymentScheduleList)
            {
                //set programme manager and programme offering fields
                var programmeManager = CrmAdapter.GetProgrammeManager(schedule.ProgrammeOfferingId);
                var programmeOffering = CrmAdapter.GetProgrammeOfferingName(schedule.ProgrammeOfferingId);

                //build report list
                foreach (var debtor in debtorDistinctList)
                {
                    var reportItem = new DtoInvoicingModel.PaymentScheduleReport();
                    //calculate amount
                    var account = CrmAdapter.GetDebtor(debtor.Key.Value);
                    var delegateCount = debtorList.Where(p => p.CrmId == debtor.Key.Value).ToList().Count;
                    var pricePerDelegate = schedule.Amount / debtorList.Count;
                    var amount = delegateCount * pricePerDelegate;

                    reportItem.ScheduleDate = schedule.Date;
                    reportItem.Amount = amount;
                    reportItem.ProgrammeManager = programmeManager.Fullname;
                    reportItem.ProgrammeOffering = programmeOffering;
                    reportItem.Debtor = account.Name;

                    reportList.Add(reportItem);
                }
            }
        }

        private static Guid CreateTaskActivity(DtoInvoicingModel.PaymentSchedule paymentScheduleItem)
        {
                var requestCreate = new CreateRequest();
                var taskActivity = new Entity("task");
            
                taskActivity["subject"] = HubConfig.PaymentScheduleEmailSubject;

                var programmeManager = GetProgrammeManager(paymentScheduleItem.ProgrammeOfferingId);                
                
                taskActivity["description"] = BuildTaskDescription(paymentScheduleItem, programmeManager.Fullname);

                taskActivity["scheduledend"] = paymentScheduleItem.Date;

                var activityPartyTo = new Entity();
                var activityPartyFrom = new Entity();

                taskActivity["ownerid"] = new EntityReference("systemuser", programmeManager.Id);

                taskActivity["regardingobjectid"] = new EntityReference("lt_programmeoffering", paymentScheduleItem.ProgrammeOfferingId);

                requestCreate.Target = taskActivity;
                var resp = (CreateResponse)CrmWrapper.ServiceContext.Execute(requestCreate);

                return resp.id;
                
        }

        private static void UpdateTaskActivity(DtoInvoicingModel.PaymentSchedule paymentScheduleItem)
        {
            var attributes = new ColumnSet(new string[] { "description", "scheduledend" });

            var log = CrmWrapper.ServiceContext.EntityMapSet.Where(p => p.Id == paymentScheduleItem.TaskActivityId).FirstOrDefault();

            var taskActivity = CrmWrapper.ServiceContext.Retrieve("task", paymentScheduleItem.TaskActivityId, attributes);

            var programmeManager = GetProgrammeManager(paymentScheduleItem.ProgrammeOfferingId);
            taskActivity["description"] = BuildTaskDescription(paymentScheduleItem, programmeManager.Fullname);

            taskActivity["scheduledend"] = paymentScheduleItem.Date;

            CrmWrapper.ServiceContext.Update(taskActivity);
        }

        private static void DeleteTaskActivity(Guid taskActivityId)
        {
            CrmWrapper.ServiceContext.Delete("task", taskActivityId);

            CrmWrapper.ServiceContext.SaveChanges();
        }

        public static DtoInvoicingModel.ProgrammeManager GetProgrammeManager(Guid programmeOfferingId)
        {
            var programmeManager = CrmWrapper.ServiceContext.Lt_programmeofferingSet.Where(p => p.Lt_programmeofferingId == programmeOfferingId).Select(p => p.user_lt_programmeoffering).First();

            return programmeManager.ToDto();
        }

        public static string GetProgrammeOfferingName(Guid programmeOfferingId)
        {
            return CrmWrapper.ServiceContext.Lt_programmeofferingSet.Where(p => p.Lt_programmeofferingId == programmeOfferingId).Select(p => p.Lt_name).First();
        }

        private static string BuildTaskDescription(DtoInvoicingModel.PaymentSchedule paymentScheduleItem, string programmeManager)
        {
            var debtorList = CrmAdapter.GetAllDebtorsForProgrammeOffering(paymentScheduleItem.ProgrammeOfferingId);

            if (debtorList.Count == 0) return "Warning: There are no debtor accounts linked to this Programme Offering!";

            //get debtor list for programme offering
            var debtorDistinctList = debtorList.GroupBy(p => p.CrmId).ToList();

            var sbItem = new StringBuilder();

            foreach (var item in debtorDistinctList)
            {
                var delegateCount = debtorList.Where(p => p.CrmId == item.Key.Value).ToList().Count;
                var pricePerDelegate = paymentScheduleItem.Amount / debtorList.Count;
                var amount = delegateCount * pricePerDelegate;

                sbItem.AppendLine(string.Format("{0} {1}", "Date               :", paymentScheduleItem.Date.ToString("D", CultureInfo.CreateSpecificCulture("en-US"))));
                sbItem.AppendLine(string.Format("{0} {1}", "Programme Manager  :", programmeManager));
                sbItem.AppendLine(string.Format("{0} {1}", "Programme Offering :", GetProgrammeOfferingName(paymentScheduleItem.ProgrammeOfferingId)));
                sbItem.AppendLine(string.Format("{0} {1}", "Debtor             :", GetDebtor(item.Key.Value).Name));
                sbItem.AppendLine(string.Format("{0} {1}", "Amount             :", amount.ToString("C")));
                sbItem.AppendLine(string.Format("{0} {1}", "No. of Delegtes    :", delegateCount.ToString()));
                sbItem.AppendLine(Environment.NewLine);
            }   
     
            return sbItem.ToString();
        }


        #endregion

        #region Group codes

        public static bool GroupCodeExists(string groupCodeId)
        {
            if (CrmWrapper.ServiceContext.lt_accountgroupcodeSet.Where(p => p.lt_name == groupCodeId).ToList().Count() != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static void CreateGroupCode(DtoInvoicingModel.GroupCode groupCode)
        {
            lt_accountgroupcode crmGroupCode = new lt_accountgroupcode();

            crmGroupCode.lt_name = groupCode.Id;
            crmGroupCode.lt_AccPacDescription = groupCode.Description;

            CrmWrapper.ServiceContext.Create(crmGroupCode);
        }

        public static void UpdateGroupCode(DtoInvoicingModel.GroupCode groupCode)
        {
            try
            {
                var crmGroupCode = CrmWrapper.ServiceContext.lt_accountgroupcodeSet.Where(p => p.lt_name == groupCode.Id).First();

                crmGroupCode.lt_AccPacDescription = groupCode.Description;

                CrmWrapper.ServiceContext.UpdateObject(crmGroupCode);
                CrmWrapper.ServiceContext.SaveChanges();
            }
            catch { }
        }

        #endregion

        #region Tax settings

        public static bool TaxSettingExists(string taxSettingId, int classCode)
        {
            if (CrmWrapper.ServiceContext.lt_taxsettingSet.Where(p => p.lt_name == taxSettingId).Where(p => p.lt_Class == classCode).ToList().Count() != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static void CreateTaxSetting(DtoInvoicingModel.TaxSetting taxSetting)
        {
            lt_taxsetting crmTaxSetting = new lt_taxsetting();

            crmTaxSetting.lt_name = taxSetting.Id;
            crmTaxSetting.lt_Class = taxSetting.Class;
            crmTaxSetting.lt_Description = taxSetting.Description;
            crmTaxSetting.lt_ItemRate1 = taxSetting.ItemRate1;
            crmTaxSetting.lt_ItemRate2 = taxSetting.ItemRate2;
            crmTaxSetting.lt_ItemRate3 = taxSetting.ItemRate3;
            crmTaxSetting.lt_ItemRate4 = taxSetting.ItemRate4;
            crmTaxSetting.lt_ItemRate5 = taxSetting.ItemRate5;

            CrmWrapper.ServiceContext.Create(crmTaxSetting);
        }

        public static void UpdateTaxSetting(DtoInvoicingModel.TaxSetting taxSetting)
        {
            try
            {
                var crmTaxSetting = CrmWrapper.ServiceContext.lt_taxsettingSet.Where(p => p.lt_name == taxSetting.Id).Where(p => p.lt_Class == taxSetting.Class).First();

                crmTaxSetting.lt_Class = taxSetting.Class;
                crmTaxSetting.lt_Description = taxSetting.Description;
                crmTaxSetting.lt_ItemRate1 = taxSetting.ItemRate1;
                crmTaxSetting.lt_ItemRate2 = taxSetting.ItemRate2;
                crmTaxSetting.lt_ItemRate3 = taxSetting.ItemRate3;
                crmTaxSetting.lt_ItemRate4 = taxSetting.ItemRate4;
                crmTaxSetting.lt_ItemRate5 = taxSetting.ItemRate5;

                CrmWrapper.ServiceContext.UpdateObject(crmTaxSetting);
                CrmWrapper.ServiceContext.SaveChanges();
            }
            catch { }
        }

        #endregion

        #region GL codes

        public static bool GLAccountExists(string glCode)
        {
            if (CrmWrapper.ServiceContext.lt_glaccountSet.Where(p => p.lt_name == glCode).ToList().Count() != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static void CreateGLAccount(DtoInvoicingModel.GLAccount glAccount)
        {
            lt_glaccount crmGLAccount = new lt_glaccount();

            crmGLAccount.lt_name = glAccount.Id;
            crmGLAccount.lt_GLDescription = glAccount.Description;

            CrmWrapper.ServiceContext.Create(crmGLAccount);
        }

        public static void UpdateGLAccount(DtoInvoicingModel.GLAccount glAccount)
        {
            var crmGLAccount = CrmWrapper.ServiceContext.lt_glaccountSet.Where(p => p.lt_name == glAccount.Id).First();

            crmGLAccount.lt_GLDescription = glAccount.Description;

            CrmWrapper.ServiceContext.UpdateObject(crmGLAccount);
            CrmWrapper.ServiceContext.SaveChanges();
        }

        public static Guid GetGLAccount(string glCode)
        {
            var glAccount = CrmWrapper.ServiceContext.lt_glaccountSet.Where(p => p.lt_name == glCode).First();

            return glAccount.Id;
        }

        #endregion
    }
}
