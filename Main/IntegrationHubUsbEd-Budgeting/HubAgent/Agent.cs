﻿using System;
using System.Threading;
using NLog;
using StellenboschUniversity.UsbEd.Integration.HubAgent.Debtors;
using StellenboschUniversity.UsbEd.Integration.HubAgent.Invoicing;

namespace StellenboschUniversity.UsbEd.Integration.HubAgent
{
    public class Agent
    {
        private TimeSpan importTime;
        private TimeSpan integrationInterval;
        private TimeSpan loopInterval;
        private TimeSpan importTimeWindow = new TimeSpan(0, 30, 0);
        private object _lock;
        private bool shuttingDown;
        private bool hasShutDown;
        private Thread pollingThread;
        private static Logger logger = LogManager.GetCurrentClassLogger();
        
        public Agent(TimeSpan integrationInterval, TimeSpan importTime)
        {
            loopInterval = new TimeSpan(0, 0, 1);
            
            this.integrationInterval = integrationInterval;
            this.importTime = importTime;
            
            _lock = new object();

            shuttingDown = false;
            hasShutDown = false;
        }

        public void Start()
        {
            pollingThread = new Thread(new ThreadStart(PollingLoop));
            pollingThread.Start();

            logger.Log(LogLevel.Info, "Agent started...{0}{1}", Environment.NewLine, "=============================");
        }

        private void PollingLoop()
        {
            int tickCountAtLastIntegration = 0;
            int tickCountAtLastImport = 0;
            TimeSpan timeSinceLastIntegration = integrationInterval;
            TimeSpan timeSinceLastImport = new TimeSpan(0, 0, 0);
            
            while (true)
            {
                lock (_lock)
                {
                    if (shuttingDown == true)
                    {
                        break;
                    }
                }

                if (timeSinceLastIntegration >= integrationInterval)
                {
                    logger.Log(LogLevel.Info, "Starting Integration Poll...{0}{1}", Environment.NewLine, "=============================");
                    
                    IntegrationPoll();
                    tickCountAtLastIntegration = Environment.TickCount;

                    logger.Log(LogLevel.Info, "Finished Integration Poll...{0}{1}", Environment.NewLine, "=============================");
                }

                if ((DateTime.Now.TimeOfDay >= importTime) && (DateTime.Now.TimeOfDay < importTime.Add(importTimeWindow)))
                {
                    logger.Log(LogLevel.Info, "Starting Import Poll...{0}{1}", Environment.NewLine, "=============================");

                    ImportPoll();
                    tickCountAtLastImport = Environment.TickCount;

                    logger.Log(LogLevel.Info, "Finished Import Poll...{0}{1}", Environment.NewLine, "=============================");
                }

                Thread.Sleep(loopInterval);

                timeSinceLastIntegration = new TimeSpan(0, 0, 0, 0, Environment.TickCount - tickCountAtLastIntegration);
                timeSinceLastImport = new TimeSpan(0, 0, 0, 0, Environment.TickCount - tickCountAtLastImport);
            }

            lock (_lock)
            {
                hasShutDown = true;
            }
        }

        public void ShutDown()
        {
            lock (_lock)
            {
                shuttingDown = true;
            }
        }

        public bool HasShutDown
        {
            get
            {
                bool result;

                lock (_lock)
                {
                    result = hasShutDown;
                }

                return result;
            }
        }

        private void IntegrationPoll()
        {
            //SynchronizeDebtors.IntegrationPoll();
            //CheckInvoices.Poll();
            CheckCreditNotes.Poll();
        }

        private void ImportPoll()
        {
            SynchronizeDebtors.ImportPoll();
            SynchronizeGLAccounts.Poll();
            SynchronizeTaxSettings.Poll();
            SynchronizeGroupCodes.Poll();
        }
    }
}
