﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Debtors;
using StellenboschUniversity.UsbEd.Integration.Adapters.Crm;
using StellenboschUniversity.UsbEd.Integration.Adapters.Ods;

namespace StellenboschUniversity.UsbEd.Integration.HubAgent.Invoicing
{
    public static class SynchronizeTaxSettings
    {
        public static void Poll()
        {
            var accpacTaxSettings = OdsAdapter.GetTaxSettings();

            foreach (var accpacTaxSetting in accpacTaxSettings)
            {
                try
                {
                    if (CrmAdapter.TaxSettingExists(accpacTaxSetting.Id, accpacTaxSetting.Class) == false)
                    {
                        CrmAdapter.CreateTaxSetting(accpacTaxSetting);
                    }
                    else
                    {
                        CrmAdapter.UpdateTaxSetting(accpacTaxSetting);
                    }
                }
                catch { }
            }
        }
    }
}
