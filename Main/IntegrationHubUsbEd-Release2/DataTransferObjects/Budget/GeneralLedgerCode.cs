﻿using System.Collections.Generic;
namespace StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Budget
{
    /// <summary>
    /// Summary description for GenralLedgerCode
    /// </summary>
    public class GeneralLedgerCode
    {
        public string Code { get; set; }

        public string Account { get; set; }

        public string Market { get; set; }

        public string SubMarket { get; set; }

        public string ProgrammeCode { get; set; }

        public decimal TotalAmount { get; set; }

        public IList<BudgetSetYear> YearCollection { get; set; }

        public GeneralLedgerCode()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public GeneralLedgerCode(string generalLedgerCode, char splitChar)
        {
            string[] segments = generalLedgerCode.Split(splitChar);
            Account = segments[0];
            Market = segments[1];
            SubMarket = segments[2];
            ProgrammeCode = segments[3];
            Code = Account.ToString() + splitChar + Market + splitChar + SubMarket + splitChar + ProgrammeCode;
        }
    }
}