﻿namespace StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Budget
{
    /// <summary>
    /// Summary description for BudgetSetYear
    /// </summary>
    public struct BudgetSetYear
    {
        public int FiscalYear { get; set; }

        public decimal Amount { get; set; }
    }
}