﻿
namespace StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Invoicing
{
      public class PaymentScheduleJsonDto
    {
        public string Id { get; set; }

        public string ProgrammeOfferingId { get; set; }

        public string Date { get; set; }

        public string Amount { get; set; }

        public string Description { get; set; }
    }
}
