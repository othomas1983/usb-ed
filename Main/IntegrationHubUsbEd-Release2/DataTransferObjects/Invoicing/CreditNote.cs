﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Enum;

namespace StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Invoicing
{
    public class CreditNote
    {
        public Guid CrmId
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public decimal Amount
        {
            get;
            set;
        }

        public string DebtorCode
        {
            get;
            set;
        }

        public string GlAccount
        {
            get;
            set;
        }

        public DateTime Date
        {
            get;
            set;
        }

        public bool Taxable
        {
            get;
            set;
        }

        public Guid? CrmRelatedInvoiceId
        {
            get;
            set;
        }

        public CreditNoteType Type
        {
            get;
            set;
        }

        public string Header1
        {
            get;
            set;
        }

        public string Header2
        {
            get;
            set;
        }

        public string Header3
        {
            get;
            set;
        }

        public string Header4
        {
            get;
            set;
        }

        public string Header5
        {
            get;
            set;
        }

        public string AdditionalField1
        {
            get;
            set;
        }

        public string AdditionalField2
        {
            get;
            set;
        }

        public string AdditionalField3
        {
            get;
            set;
        }

        public string AdditionalField4
        {
            get;
            set;
        }

        public string AdditionalField5
        {
            get;
            set;
        }
    }
}
