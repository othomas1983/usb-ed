﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Invoicing
{
    public class GroupCode
    {
        public string Id
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }
    }
}
