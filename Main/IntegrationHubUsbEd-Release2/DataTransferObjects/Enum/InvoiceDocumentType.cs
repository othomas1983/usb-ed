﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Enum
{
    public enum InvoiceDocumentType
    {
        Invoice = 0,
        Cancellation,
        Discount,
        DebitNote
    }
}
