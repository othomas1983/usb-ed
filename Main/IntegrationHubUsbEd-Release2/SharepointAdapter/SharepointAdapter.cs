﻿using System;
using System.Net;
using NLog;
using StellenboschUniversity.UsbEd.Integration.Adapters.Ods;
using StellenboschUniversity.UsbEd.Integration.Core;
using StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Budget;


namespace StellenboschUniversity.UsbEd.Integration.Adapters.Sharepoint
{
    public static class SharepointAdapter
    {
        #region Locals

        private static Uri SharePointODataUri = new Uri(HubConfig.SharePointODataUri);
        private static NetworkCredential SharePointNetworkCredential = new NetworkCredential(HubConfig.SharepointUser, HubConfig.SharepointPassword,HubConfig.NetworkDomain);
        private static Logger logger = LogManager.GetCurrentClassLogger();

        #endregion

        #region Public Methods

        public static void Initialize()
        {
            try
            {
                SharepointWrapper.Initialize(SharePointODataUri, SharePointNetworkCredential);
                OdsAdapter.Initialize();
                logger.Log(LogLevel.Info, "SharePoint Adapter Initialize...{0}",Environment.NewLine);

            }
            catch (Exception ex)
            {
                logger.Log(LogLevel.Trace, "Message: {0}{3}Stack Trace: {1}{3}Inner Exception: {2}{3}", ex.Message, ex.StackTrace, ex.InnerException, Environment.NewLine);
            }
        }

        public static SharepointResult SendBudgetToOds(int sharepointId)
        {
            try
            {
                var result = new SharepointResult();

                var budgetSetItem = SharepointWrapper.GetBudgetSetItem(sharepointId);

                var budgetSet = budgetSetItem.ToDto();

                var isValid = SharepointWrapper.ValidateBudgetSet(budgetSet);

                var exists = SharepointWrapper.CheckIfExists(sharepointId);

                if (isValid)
                {

                    if (!exists)
                    {
                        OdsAdapter.CreateBudgetSet(budgetSet);
                    }
                    else
                    {
                        OdsAdapter.UpdateBudgetSet(budgetSet);
                    }
                }
                else
                {
                    if (exists)
                    {
                        OdsAdapter.MarkInvalidBudgetSet(sharepointId);
                    }
                }

                logger.Log(LogLevel.Info, "Send Budget To Ods OK{0}", Environment.NewLine);

                result.HasPassed = true;

                return result;
            }
            catch (Exception ex)
            {
                logger.Log(LogLevel.Trace, "Message: {0}{3}Stack Trace: {1}{3}Inner Exception: {2}{3}", ex.Message, ex.StackTrace, ex.InnerException, Environment.NewLine);

                var result = new SharepointResult();

                result.HasPassed = false;

                return result;
            }
        }

        public static void DeleteBudgetSet(int sharepointId)
        {
            try 
            { 
                OdsAdapter.DeleteBudgetSet(sharepointId);

                logger.Log(LogLevel.Info, "Delete Budget Set OK{0}", Environment.NewLine);
            }
            catch (Exception ex) 
            {
                logger.Log(LogLevel.Trace, "Message: {0}{3}Stack Trace: {1}{3}Inner Exception: {2}{3}", ex.Message, ex.StackTrace, ex.InnerException, Environment.NewLine);
            }            
        }

        public static string SendBudgetToAccpac(string message)
        {
            try
            {
                var budgetMessage = message.ToDto();

                var budgetSet = OdsAdapter.GetBudgetSet(Guid.Parse(budgetMessage.POID));

                OdsAdapter.CreateAccpacBudgetSet(budgetSet, Guid.Parse(budgetMessage.UserID));
                
                logger.Log(LogLevel.Info, "Send Budget To Accpac (in OpsHub) OK{0}", Environment.NewLine);

                //TODO: Get programme offering name for return message

                return string.Format("Budget posted successfully. The next AccPac run will take place at {0}", OdsAdapter.NextAccpacRunTime().ToString());

            }
            catch (Exception ex)
            {
                logger.Log(LogLevel.Trace, "Message: {0}{3}Stack Trace: {1}{3}Inner Exception: {2}{3}", ex.Message, ex.StackTrace, ex.InnerException, Environment.NewLine);

                return "Budget post failed. Please contact your system administrator.";
            }            
        }

        #endregion


    }
}
