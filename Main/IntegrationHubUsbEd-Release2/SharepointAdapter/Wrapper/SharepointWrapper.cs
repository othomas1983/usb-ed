﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel.Description;
using LinqToExcel;
using NLog;
using SharepointAdapter.SharepointOData;
using StellenboschUniversity.UsbEd.Integration.Adapters.Crm;
using StellenboschUniversity.UsbEd.Integration.Core;
using StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Budget;
using StellenboschUniversity.UsbEd.Integration.Adapters.Ods;


namespace StellenboschUniversity.UsbEd.Integration.Adapters.Sharepoint
{
    internal static class SharepointWrapper
    {
        #region Locals

        private static Uri crmUri = new Uri(HubConfig.CrmODataUri);
        private static ClientCredentials crmClientCredentials = new ClientCredentials();
        private static Logger logger = LogManager.GetCurrentClassLogger();

        #endregion

        #region Properties

        public static CRMDataContext DataContext { get; private set; }

        private static NetworkCredential SharePointNetworkCredential { get; set; }

        #endregion

        #region Public Methods

        /// <summary>
        /// This method intializes the wrapped object
        /// </summary>
        public static void Initialize(Uri sharePointODataUri, NetworkCredential sharePointNetworkCredential)
        {
            DataContext = new CRMDataContext(sharePointODataUri);
            DataContext.Credentials = sharePointNetworkCredential;
            SharePointNetworkCredential = sharePointNetworkCredential;
            logger.Log(LogLevel.Info, "SharePoint Wrapper Initialize...{0}", Environment.NewLine);

        }

        /// <summary>
        /// This method extracts the GeneralLedgerCode list from the excel file
        /// </summary>
        public static IList<GeneralLedgerCode> BuildGeneralLedgerCodes(ProgrammeOfferingItem item)
        {
            var codes = new List<GeneralLedgerCode>();

            //extracts file name and extension
            string fileName;
            string fileExtension;
            GetFileParts(item, out fileName, out fileExtension);

            //generates server path for temp storage
            string tempFolder;
            string serverPath;
            GetServerPath(fileName, fileExtension, out tempFolder, out serverPath);

            //creates directory if it does not exist
            Directory.CreateDirectory(tempFolder);

            //downloads excel file to temp storage location
            DownloadFile(item, serverPath);

            //set up linq query for data extraction
            var excel = new ExcelQueryFactory(serverPath);

            //verify worksheet name
            if (VerifyWorkbook(excel))
            {
                //determine target row and column index for budget area
                int columnIndex;
                Row headerRow;
                GetRowColumnIndex(excel, out columnIndex, out headerRow);

                //interogates excel file for glcodes, total and fiscal year to build budget set
                var glcodeColumn = GetGLCodeColumn(excel, columnIndex);

                var extractData = false;
                foreach (var cell in glcodeColumn)
                {
                    if (extractData)
                    {
                        var cellValue = cell[columnIndex].Value.ToString();

                        if (!(cellValue.ToLower() == HubConfig.BudgetEndMarker.ToLower()))
                        {
                            BuildGLCode(codes, columnIndex, headerRow, cell, cellValue);
                        }
                        else
                        {
                            break;
                        }
                    }
                    if (cell[columnIndex].Value.ToString().ToLower() == HubConfig.BudgetTargetColumn.ToLower())
                    {
                        extractData = true;
                    }
                }
            }

            //cleans up temp folder 
            File.Delete(serverPath);

            if (codes.Count > 0)
            {
                return codes;
            }
            else
            {
                throw new Exception("No General Ledger Accounts found");
            }            

        }

        /// <summary>
        /// This method validates the budget set
        /// </summary>
        public static bool ValidateBudgetSet(BudgetSet budgetSet)
        {
            var result = false;

            //try to assign, if the id does not exist
            if (budgetSet.ProgrammeOfferingId.IsEmpty())
            {
                crmClientCredentials.Windows.ClientCredential = SharePointNetworkCredential; 
                CrmAdapter.Initialize(crmUri, crmClientCredentials);
                budgetSet.ProgrammeOfferingId = CrmAdapter.GetProgrammeOfferingId(budgetSet.FilePath.Replace(HubConfig.BudgetValidatorCrmReplaceText, string.Empty));
            }

            //validate
            if (!budgetSet.ProgrammeOfferingId.IsEmpty())
            {
                result = true;
            }

            return result;
        }

        /// <summary>
        /// This method returns the Sharepoint budget item
        /// </summary>
        public static global::SharepointAdapter.SharepointOData.ProgrammeOfferingItem GetBudgetSetItem(int sharepointId)
        {
            var budgetSetItem = (from dc in DataContext.ProgrammeOffering
                                 where dc.Id == sharepointId
                                 select dc).FirstOrDefault();
            return budgetSetItem;
        }

        /// <summary>
        /// This method checks if the budget exists in the ods
        /// </summary>
        /// <param name="sharepointId"></param>
        public static bool CheckIfExists(int sharepointId)
        {
            var budgetSet = OdsAdapter.GetBudgetSet(sharepointId);
            return budgetSet.IsNotNull() ? true : false;
        }


        #endregion

        #region Private Methods

        private static void BuildGLCode(List<GeneralLedgerCode> codes, int columnIndex, Row headerRow, RowNoHeader cell, string cellValue)
        {
            var glCode = new GeneralLedgerCode(cellValue, HubConfig.BudgetGLFormattedChar);
            glCode.YearCollection = new List<BudgetSetYear>();

            BuildFiscalYear(columnIndex, headerRow, cell, glCode);
            decimal totalAmount;
            glCode.TotalAmount = decimal.TryParse(cell[columnIndex + 1].Value.ToString(), NumberStyles.Currency, CultureInfo.CurrentCulture.NumberFormat, out totalAmount) ? totalAmount : 0;

            codes.Add(glCode);
        }

        private static void BuildFiscalYear(int columnIndex, Row headerRow, RowNoHeader cell, GeneralLedgerCode glCode)
        {
            for (int i = columnIndex + 2; i < headerRow.Count; i++)
            {
                var fiscalYear = new BudgetSetYear();
                decimal amount;
                fiscalYear.Amount = decimal.TryParse(cell[i].Value.ToString(), NumberStyles.Currency, CultureInfo.CurrentCulture.NumberFormat, out amount) ? amount : 0; ;
                int year;
                fiscalYear.FiscalYear = int.TryParse(headerRow[i].Value.ToString(), NumberStyles.Currency, CultureInfo.CurrentCulture.NumberFormat, out year) ? year : 0;
                glCode.YearCollection.Add(fiscalYear);
            }
        }

        private static List<RowNoHeader> GetGLCodeColumn(ExcelQueryFactory excel, int columnIndex)
        {
            var glcodeColumn = (from c in excel.WorksheetNoHeader(HubConfig.BudgetWorkSheetName)
                                where c[columnIndex] != string.Empty
                                select c).ToList();
            return glcodeColumn;
        }

        private static void GetRowColumnIndex(ExcelQueryFactory excel, out int columnIndex, out Row headerRow)
        {
            var rows = from sheet in excel.Worksheet(HubConfig.BudgetWorkSheetName) select sheet;
            var innerBreak = false;
            var rowIndex = 0;
            columnIndex = 0;
            headerRow = new Row();
            foreach (var row in rows.ToList())
            {
                foreach (var cell in row.ToList())
                {
                    if (cell.Value.ToString().ToLower() == HubConfig.BudgetTargetColumn.ToLower())
                    {
                        headerRow = row;
                        innerBreak = true;
                        break;
                    }
                    columnIndex += 1;
                }
                if (innerBreak) break;
                columnIndex = 0;
                rowIndex += 1;
            }
        }

        private static bool VerifyWorkbook(ExcelQueryFactory excel)
        {
            bool validWorkSheet = false;
            var workSheetNames = excel.GetWorksheetNames().ToList();
            foreach (var sheet in workSheetNames)
            {
                if (sheet.ToLower() == HubConfig.BudgetWorkSheetName.ToLower())
                {
                    validWorkSheet = true;
                    break;
                }
            }
            return validWorkSheet;
        }

        private static void DownloadFile(ProgrammeOfferingItem item, string serverPath)
        {
            var sharePointFilePath = HubConfig.BudgetDownloadProtocol + DataContext.BaseUri.Host + item.Path + "/" + item.Name;
            var webClient = new WebClient();
            webClient.Credentials = SharePointNetworkCredential;
            webClient.DownloadFile(sharePointFilePath, serverPath);
        }

        private static void GetServerPath(string fileName, string fileExtension, out string tempFolder, out string serverPath)
        {
            tempFolder = HubConfig.BudgetTempPath;
            serverPath = tempFolder + fileName + "_" + Guid.NewGuid().ToString() + "." + fileExtension;
        }

        private static void GetFileParts(ProgrammeOfferingItem item, out string fileName, out string fileExtension)
        {
            string[] filePart = Path.GetFileName(item.Name).Split('.');
            fileName = filePart[0];
            fileExtension = filePart[1];
        }
            
      
        #endregion

    }
}
