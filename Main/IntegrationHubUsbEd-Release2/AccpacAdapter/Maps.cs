﻿using System.Data.Objects.DataClasses;
using StellenboschUniversity.UsbEd.Integration.Adapters.Accpac.Model;
using StellenboschUniversity.UsbEd.Integration.Core;
using StellenboschUniversity.UsbEd.Integration.DataTransferObjects;
using StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Enum;


namespace StellenboschUniversity.UsbEd.Integration.Adapters.Accpac
{
    public static class Maps
    {
        #region Locals

        //TODO: move to external settings file
        private const string _GLSPLITCHAR = "-";

        #endregion
        
        #region Extension Methods

        public static EntityCollection<GLBudget> ToAccpacModel(this BudgetSet item)
        {
            var budgetSet = new EntityCollection<GLBudget>();
            
            foreach (var glcode in item.GeneralLedgerCodeCollection)
            {
                foreach (var fiscalyear in glcode.YearCollection)
                {
                    budgetSet.Add(glcode.ToAccpacModel(fiscalyear, item));
                }
            }

            return budgetSet;
        }

        public static GLBudget ToAccpacModel(this GeneralLedgerCode item, BudgetSetYear year, BudgetSet budgetSet)
        {
            var glBudget = new GLBudget();

            glBudget.ACCTID = item.Code.Replace(_GLSPLITCHAR, string.Empty);
            glBudget.FSCSYR = year.FiscalYear.ToString();
            glBudget.AMOUNT = year.Amount;
            glBudget.FSCSDSG = budgetSet.Set;
            glBudget.STPERIOD = budgetSet.StartPeriod;
            glBudget.TRXSTATUS = TransactionStatus.Submitted.ToInt();

            return glBudget;
        }

        #endregion
    }
}
