﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Debtors;
using StellenboschUniversity.UsbEd.Integration.Adapters.Crm;
using StellenboschUniversity.UsbEd.Integration.Adapters.Ods;

namespace StellenboschUniversity.UsbEd.Integration.HubAgent.Invoicing
{
    public static class SynchronizeGroupCodes
    {
        public static void Poll()
        {
            var accpacGroupCodes = OdsAdapter.GetGroupCodes();

            foreach (var accpacGroupCode in accpacGroupCodes)
            {
                try
                {
                    if (CrmAdapter.GroupCodeExists(accpacGroupCode.Id) == false)
                    {
                        CrmAdapter.CreateGroupCode(accpacGroupCode);
                    }
                    else
                    {
                        CrmAdapter.UpdateGroupCode(accpacGroupCode);
                    }
                }
                catch { }
            }
        }
    }
}
