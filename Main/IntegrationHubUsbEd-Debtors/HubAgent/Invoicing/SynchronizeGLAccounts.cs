﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Debtors;
using StellenboschUniversity.UsbEd.Integration.Adapters.Crm;
using StellenboschUniversity.UsbEd.Integration.Adapters.Ods;

namespace StellenboschUniversity.UsbEd.Integration.HubAgent.Invoicing
{
    public static class SynchronizeGLAccounts
    {
        public static void Poll()
        {
            var accpacGLAccounts = OdsAdapter.GetGLAccounts();

            foreach (var accpacGLAccount in accpacGLAccounts)
            {
                try
                {
                    if (CrmAdapter.GLAccountExists(accpacGLAccount.Id) == false)
                    {
                        CrmAdapter.CreateGLAccount(accpacGLAccount);
                    }
                    else
                    {
                        CrmAdapter.UpdateGLAccount(accpacGLAccount);
                    }
                }
                catch { }
            }
        }
    }
}
