﻿using System;

namespace StellenboschUniversity.UsbEd.Integration.Core
{
    public static class ExtensionMethods
    {
        #region Enums

        public static T ToEnum<T>(this string value)
        {
            if (typeof(T).BaseType != typeof(Enum))
            {
                throw new InvalidCastException();
            }
            if (Enum.IsDefined(typeof(T), value) == false)
            {
                throw new InvalidCastException();
            }
            if (string.IsNullOrEmpty(value))
            {
                throw new ArgumentNullException("value");
            }

            return (T)Enum.Parse(typeof(T), value);
        }

        public static string GetName<T>(this Enum value)
        {
            if (typeof(T).BaseType != typeof(Enum))
            {
                throw new InvalidCastException();
            }
            if (Enum.IsDefined(typeof(T), value) == false)
            {
                throw new InvalidCastException();
            }


            return Enum.GetName(typeof(T), value.ToInt()); ;
        }

        #endregion

        #region Objects

        public static bool IsNotNull(this object value)
        {
            return value != null ? true : false;
        }

        public static bool IsNull(this object value)
        {
            return value == null ? true : false;
        }

        #endregion

        #region Int

        public static int ToInt(this string value)
        {
            int result;

            int.TryParse(value, out result);

            return result;
        }

        public static int ToInt(this object value)
        {
            return Convert.ToInt32(value);
        }

        #endregion

        #region Guid

        public static Guid ToGuid(this string value)
        {
            return string.IsNullOrEmpty(value) ? Guid.Empty : new Guid(value);
        }

        public static bool IsEmpty(this Guid value)
        {
            return value == Guid.Empty ? true : false;
        }

        #endregion

        #region Char

        public static char ToChar(this string value)
        {
            return Convert.ToChar(value);
        }

        #endregion
    }
}
