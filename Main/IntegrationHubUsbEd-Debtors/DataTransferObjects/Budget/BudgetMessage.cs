﻿
namespace StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Budget
{
    /// <summary>
    /// Summary description for BudgetMessage
    /// </summary>
    public class BudgetMessage
    {
        public string POID { get; set; }

        public string UserID { get; set; }
    }
}
