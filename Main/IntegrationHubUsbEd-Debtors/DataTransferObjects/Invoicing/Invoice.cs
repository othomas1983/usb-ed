﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Enum;

namespace StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Invoicing
{
    public class Invoice
    {
        public Invoice()
        {
            LineItems = new List<InvoiceLineItem>();
            CreditNotes = new List<CreditNote>();
        }
        
        public int OdsId
        {
            get;
            set;
        }

        public Guid CrmId
        {
            get;
            set;
        }

        public string DebtorCode
        {
            get;
            set;
        }

        public DateTime CreationDate
        {
            get;
            set;
        }

        public DateTime? DueDate
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public bool Taxable
        {
            get;
            set;
        }

        public string Header1
        {
            get;
            set;
        }

        public string Header2
        {
            get;
            set;
        }

        public string Header3
        {
            get;
            set;
        }

        public string Header4
        {
            get;
            set;
        }

        public string Header5
        {
            get;
            set;
        }

        public string DocumentNumber
        {
            get;
            set;
        }

        public decimal Balance
        {
            get;
            set;
        }

        public decimal Amount
        {
            get;
            set;
        }

        public List<InvoiceLineItem> LineItems
        {
            get;
            set;
        }

        public List<CreditNote> CreditNotes
        {
            get;
            set;
        }
    }
}
