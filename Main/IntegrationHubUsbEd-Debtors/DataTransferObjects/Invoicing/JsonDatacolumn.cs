﻿using System.Runtime.Serialization;

namespace StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Invoicing
{
    [DataContract]
    public class JsonDatacolumn
    {
        [DataMember]
        public string sTitle { get; set; }

        [DataMember]
        public string sClass { get; set; }
    }
}
