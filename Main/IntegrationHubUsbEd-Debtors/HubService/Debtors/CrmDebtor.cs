﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StellenboschUniversity.UsbEd.Integration.Adapters.Crm.Wrapper;

namespace StellenboschUniversity.UsbEd.Integration.HubService.Debtors
{
    public class CrmDebtor
    {
        private Guid crmId;
        
        public CrmDebtor(Guid crmId)
        {
            this.crmId = crmId;

            LoadFromCrm();
        }

        private void LoadFromCrm()
        {
            var crmDebtor = CrmWrapper.ServiceContext.Lt_debtorSet.Where(p => p.Lt_debtorId.Value == crmId).First();

            DebtorCode = crmDebtor.Lt_name;
            Name = crmDebtor.Lt_DebtorName;
            AddressLine1 = crmDebtor.lt_addressline1;
            AddressLine2 = crmDebtor.lt_addressline2;
            AddressLine3 = crmDebtor.lt_addressline3;
            AddressLine4 = crmDebtor.lt_addressline4;
            City = crmDebtor.lt_addresscity;
            StateProvince = crmDebtor.lt_addressstateprovince;
            PostalCode = crmDebtor.lt_addresspostalcode;
            Country = crmDebtor.lt_addresscountry;
            TelephoneNumber = crmDebtor.lt_telephonenumber;
            FaxNumber = crmDebtor.lt_faxnumber;
            ContactName = crmDebtor.lt_contactname;
            ContactEmail = crmDebtor.lt_contactemail;
            ContactPhone = crmDebtor.lt_contacttelephonenumber;
            ContactFax = crmDebtor.lt_contactfaxnumber;
        }

        #region Properties

        public Guid CrmId
        {
            get { return crmId; }
        }

        public string DebtorCode { get; set; }

        public string Name { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string AddressLine3 { get; set; }

        public string AddressLine4 { get; set; }

        public string City { get; set; }

        public string StateProvince { get; set; }

        public string PostalCode { get; set; }

        public string Country { get; set; }

        public string TelephoneNumber { get; set; }

        public string FaxNumber { get; set; }

        public string ContactName { get; set; }

        public string ContactEmail { get; set; }

        public string ContactPhone { get; set; }

        public string ContactFax { get; set; }

        #endregion
    }
}
