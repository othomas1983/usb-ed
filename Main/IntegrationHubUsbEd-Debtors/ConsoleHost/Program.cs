﻿using System;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Threading;
using StellenboschUniversity.UsbEd.Integration.Adapters.Crm;
using StellenboschUniversity.UsbEd.Integration.Adapters.Ods;
using StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Debtors;
using StellenboschUniversity.UsbEd.Integration.HubAgent;
using StellenboschUniversity.UsbEd.Integration.HubService;


namespace StellenboschUniversity.UsbEd.Integration.ConsoleHost
{
    class Program
    {
        private static Agent agent;
        private static TimeSpan integrationInterval = new TimeSpan(0, 10, 0);
        private static TimeSpan importInterval = new TimeSpan(1, 0, 0);
        
        static void Main(string[] args)
        {
            Initialize();
            StartAgent();
            //Test();
            Console.ReadLine();
            //HostService();
            StopAgent();

            Console.WriteLine(Environment.NewLine);
            Console.WriteLine("Finished.");
            Console.ReadLine();
        }

        static void Initialize()
        {
            CrmAdapter.Initialize();
            OdsAdapter.Initialize();
        }

        static void StartAgent()
        {
            agent = new Agent(integrationInterval, importInterval);
            agent.Start();

            Console.WriteLine("Agent started.");
        }

        static void StopAgent()
        {
            agent.ShutDown();

            while (agent.HasShutDown == false)
            {
                Console.WriteLine("Waiting for Agent to shut down...");
                Thread.Sleep(2000);
            }

            Console.WriteLine("Agent has shut down.");
        }

        static void HostService()
        {
            using (ServiceHost host = new ServiceHost(typeof(InvoicingService)))
            {
                host.Open();

                Console.WriteLine("The service is ready.");
                Console.WriteLine("Press <Enter> to stop the service.");
                Console.ReadLine();

                host.Close();
            }
        }
    }
}
