﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Invoicing
{
    public class InvoiceLineItem
    {
        public int OdsId
        {
            get;
            set;
        }

        public string GlAccount
        {
            get;
            set;
        }

        public decimal Amount
        {
            get;
            set;
        }

        public bool Taxable
        {
            get;
            set;
        }

        public string AdditionalField1
        {
            get;
            set;
        }

        public string AdditionalField2
        {
            get;
            set;
        }

        public string AdditionalField3
        {
            get;
            set;
        }

        public string AdditionalField4
        {
            get;
            set;
        }

        public string AdditionalField5
        {
            get;
            set;
        }

        public string Comment
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }
    }
}
