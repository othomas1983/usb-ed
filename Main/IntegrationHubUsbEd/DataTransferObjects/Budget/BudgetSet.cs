﻿using System;
using System.Collections.Generic;
using StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Enum;

namespace StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Budget
{
    /// <summary>
    /// Summary description for BudgetSet
    /// </summary>
    public class BudgetSet
    {
        public int Id { get; set; }
        
        public int SharepointId { get; set; }

        public Guid ProgrammeOfferingId { get; set; }

        public Guid CrmUserId { get; set; }

        public int Set { get; set; }

        public int StartPeriod { get; set; }

        public string Title { get; set; }

        public string FileName { get; set; }

        public string FilePath { get; set; }

        public DateTime? DateCreated { get; set; }

        public int? UserCreatedId { get; set; }

        public DateTime? DateModified { get; set; }

        public int? UserModifiedId { get; set; }

        public BudgetSetStatus Status { get; set; }

        public IList<GeneralLedgerCode> GeneralLedgerCodeCollection { get; set; }        

        public BudgetSet()
        {
            //
            // TODO: Add constructor logic here
            //
        }
    }
}