﻿using System;
using NLog;
using StellenboschUniversity.UsbEd.Integration.Adapters.Crm;
using StellenboschUniversity.UsbEd.Integration.Adapters.Ods;
using StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Invoicing;
using StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Enum;

namespace StellenboschUniversity.UsbEd.Integration.HubService
{
    public class InvoicingService : IInvoicingService
    {
        #region Locals

        private static Logger logger = LogManager.GetCurrentClassLogger();

        #endregion

        public void CreatePaymentSchedule(string message)
        {
            logger.Log(LogLevel.Info, "Invoicing Service CreatePaymentSchedule invoked with message of {1}...{0}", Environment.NewLine, message);

            CrmAdapter.Initialize();
            CrmAdapter.CreatePaymentSchedule(message);
        }

        public void UpdatePaymentSchedule(string message)
        {
            logger.Log(LogLevel.Info, "Invoicing Service UpdatePaymentSchedule invoked with message of {1}...{0}", Environment.NewLine, message);

            CrmAdapter.Initialize();
            CrmAdapter.UpdatePaymentSchedule(message);
        }

        public JsonDatatable GetPaymentSchedule(string message)
        {
            logger.Log(LogLevel.Info, "Invoicing Service GetPaymentSchedule invoked with message of {1}...{0}", Environment.NewLine, message);

            CrmAdapter.Initialize();

            return CrmAdapter.GetPaymentSchedule(message);
        }

        public string GetPaymentScheduleTotal(string message)
        {
            logger.Log(LogLevel.Info, "Invoicing Service GetPaymentSchedule invoked with message of {1}...{0}", Environment.NewLine, message);

            CrmAdapter.Initialize();

            return CrmAdapter.GetPaymentScheduleTotal(message);
        }

        public void DeletePaymentSchedule(string message)
        {
            logger.Log(LogLevel.Info, "Invoicing Service DeletePaymentSchedule invoked with message of {1}...{0}", Environment.NewLine, message);

            CrmAdapter.Initialize();
            CrmAdapter.DeletePaymentSchedule(message);
        }

        public void SendInvoiceToAccpac(string message)
        {
            logger.Log(LogLevel.Info, "Invoicing Service SendInvoiceToAccpac invoked with message of {1}...{0}", Environment.NewLine, message);

            Guid invoiceId = new Guid(message);

            CrmAdapter.Initialize();
            OdsAdapter.Initialize();

            Invoice invoice = CrmAdapter.LoadInvoiceFromCrm(invoiceId);
            OdsAdapter.SaveInvoiceToOds(invoice);
            CrmAdapter.UpdateInvoiceAccpacIntegrationStatus(invoiceId, AccpacIntegrationStatus.SentToAccpac);
        }

        public void SendCreditNoteToAccpac(string message)
        {
            logger.Log(LogLevel.Info, "Invoicing Service SendCreditNoteToAccpac invoked with message of {1}...{0}", Environment.NewLine, message);

            Guid creditNoteId = new Guid(message);

            CrmAdapter.Initialize();
            OdsAdapter.Initialize();

            CreditNote creditNote = CrmAdapter.LoadCreditNoteFromCrm(creditNoteId);
            OdsAdapter.SaveCreditNoteToOds(creditNote);
            CrmAdapter.UpdateCreditNoteAccpacIntegrationStatus(creditNoteId, AccpacIntegrationStatus.SentToAccpac);
        }

        public JsonDatatable GetPaymentScheduleReport(string message)
        {
            logger.Log(LogLevel.Info, "Invoicing Service GetPaymentScheduleReport invoked with message of {1}...{0}", Environment.NewLine, message);

            CrmAdapter.Initialize();

            return CrmAdapter.GetPaymentScheduleReport(message);
        }
    }
}
