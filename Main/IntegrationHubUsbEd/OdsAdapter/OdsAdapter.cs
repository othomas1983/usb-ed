﻿using System;
using System.Collections.Generic;
using System.Linq;
using NLog;
using StellenboschUniversity.UsbEd.Integration.Adapters.Ods.Model;
using StellenboschUniversity.UsbEd.Integration.Core;
using StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Invoicing;
using StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Debtors;
using StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Enum;
using DtoBudgetModel = StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Budget;
using DtoInvoicingModel = StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Invoicing;

namespace StellenboschUniversity.UsbEd.Integration.Adapters.Ods
{
    public static class OdsAdapter
    {
        #region Locals

        private static Logger logger = LogManager.GetCurrentClassLogger();

        #endregion

        #region Properties

        private static OpsHubEntities DataContext { get; set; }

        #endregion

        #region Methods

        public static void Initialize()
        {
            DataContext = new OpsHubEntities(HubConfig.OpsHubConnectionString);
            logger.Log(LogLevel.Info, "Ods Adapter Initialize...{0}", Environment.NewLine);
        }

        #endregion

        #region Budgeting

        public static void CreateBudgetSet(DtoBudgetModel.BudgetSet budgetSet)
        {
            SetBudgetStatusFlags(budgetSet);
            //create budget set
            DataContext.AddToBudgetSets(budgetSet.ToOdsDataModel());
            DataContext.SaveChanges();
        }

        private static void SetBudgetStatusFlags(DtoBudgetModel.BudgetSet budgetSet)
        {
            //set status flags in ods
            if (budgetSet.Status == BudgetSetStatus.Final)
            {
                var budgetSets = (from d in DataContext.BudgetSets where d.ProgrammeOfferingID == budgetSet.ProgrammeOfferingId select d).ToList();
                foreach (var item in budgetSets)
                {
                    item.Status = (BudgetSetStatus)item.Status.ToInt() == BudgetSetStatus.Final ? BudgetSetStatus.Approved.ToInt() : item.Status;
                }
            }
        }

        public static DtoBudgetModel.BudgetSet GetBudgetSet(Guid programmeOfferingIdd)
        {
            var budgetSet = (from d in DataContext.BudgetSets where d.ProgrammeOfferingID == programmeOfferingIdd && d.Status == (int)BudgetSetStatus.Final && (d.IsDeleted == false || d.IsDeleted == null) && d.IsValid == true select d).FirstOrDefault();
            return budgetSet.ToDto();
        }

        public static DtoBudgetModel.BudgetSet GetBudgetSet(int sharepointId)
        { 
            var budgetSet = (from d in DataContext.BudgetSets where d.SharepointID == sharepointId && (d.IsDeleted == false || d.IsDeleted == null) select d).FirstOrDefault();
            return budgetSet.IsNotNull() ? budgetSet.ToDto() : null;
        }

        public static void UpdateBudgetSet(DtoBudgetModel.BudgetSet budgetSet)
        {
            var budgetSetOds = (from d in DataContext.BudgetSets where d.SharepointID == budgetSet.SharepointId select d).FirstOrDefault();

            if (budgetSetOds.IsNotNull())
            {
                //performs a replace rather than an update
                DataContext.DeleteObject(budgetSetOds);

                DataContext.AddToBudgetSets(budgetSet.ToOdsDataModel());

                DataContext.SaveChanges();

            }

        }

        public static void DeleteBudgetSet(int sharepointId)
        {
            //soft delete
            var budgetSetOds = (from d in DataContext.BudgetSets where d.SharepointID == sharepointId select d).FirstOrDefault();

            if (budgetSetOds.IsNotNull())
            {
                budgetSetOds.IsDeleted = true;

                DataContext.SaveChanges();
            }
        }

        public static void CreateAccpacBudgetSet(DtoBudgetModel.BudgetSet budgetSet, Guid crmUserId)
        {
            var accpacBudgetSet = budgetSet.ToAccpacModel();

            foreach (var glBudget in accpacBudgetSet)
            {
                DataContext.AddToGLBudgets(glBudget);
            }

            //update SentToAccpac flag
            var budgetSetOds = (from d in DataContext.BudgetSets where d.SharepointID == budgetSet.SharepointId select d).FirstOrDefault();

            budgetSetOds.SentToAccpac = true;
            budgetSetOds.CrmUserID = crmUserId;

            DataContext.SaveChanges();

        }

        public static void MarkInvalidBudgetSet(int sharePointId)
        {
            var budgetSet = (from d in DataContext.BudgetSets where d.SharepointID == sharePointId select d).FirstOrDefault();

            budgetSet.IsValid = false;

            DataContext.SaveChanges();
        }

        public static DateTime NextAccpacRunTime()
        {
            var option = (from d in DataContext.IntOptions select d).FirstOrDefault();

            return option.NEXTRUNTIME.Value;
        }

        #endregion

        #region Debtors

        public static void SaveDebtorToOds(StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Debtors.Debtor debtor, DebtorOrigin origin, StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Enum.TransactionStatus status)
        {
            OpsHubEntities dataContext = new OpsHubEntities();
            var odsDebtor = new StellenboschUniversity.UsbEd.Integration.Adapters.Ods.Model.ARCustomer();

            odsDebtor.IDCUST = debtor.DebtorCode;
            odsDebtor.MSCUST = debtor.CrmId;
            odsDebtor.NAMECUST = debtor.Name;
            odsDebtor.IDGRP = debtor.GroupCode;
            odsDebtor.TEXTSTRE1 = debtor.AddressLine1;
            odsDebtor.TEXTSTRE2 = debtor.AddressLine2;
            odsDebtor.TEXTSTRE3 = debtor.AddressLine3;
            odsDebtor.TEXTSTRE4 = debtor.AddressLine4;
            odsDebtor.NAMECITY = debtor.City;
            odsDebtor.CODESTTE = debtor.StateProvince;
            odsDebtor.CODEPSTL = debtor.PostalCode;
            odsDebtor.CODECTRY = debtor.Country;
            odsDebtor.NAMECTAC = debtor.ContactName;
            odsDebtor.TEXTPHON1 = debtor.Telephone;
            odsDebtor.TEXTPHON2 = debtor.Fax;
            odsDebtor.IDTAXREGI1 = debtor.TaxRegistrationNumber;
            odsDebtor.EMAIL1 = debtor.Email;
            odsDebtor.EMAIL2 = debtor.ContactEmail;
            odsDebtor.CTACPHONE = debtor.ContactTelephone;
            odsDebtor.CTACFAX = debtor.ContactFax;
            odsDebtor.PREFIX = debtor.DebtorCodePrefix;
            odsDebtor.CURNSYMBOL = debtor.CurrencySymbol;
            odsDebtor.CURNCODE = debtor.CurrencyCode;
            
            odsDebtor.TRXSTATUS = (int)status;
            odsDebtor.TRXDATE = DateTime.Now;
            odsDebtor.ORIGIN = (int)origin;

            dataContext.AddToARCustomers(odsDebtor);
            dataContext.SaveChanges();
        }

        private static StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Debtors.Debtor LoadDebtorFromOdsObject(StellenboschUniversity.UsbEd.Integration.Adapters.Ods.Model.ARCustomer odsDebtor)
        {
            var debtor = new StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Debtors.Debtor();

            debtor.OdsId = odsDebtor.ARCUSTID;
            debtor.DebtorCode = odsDebtor.IDCUST == null ? null : odsDebtor.IDCUST.Trim();
            debtor.CrmId = odsDebtor.MSCUST;
            debtor.Name = odsDebtor.NAMECUST == null ? null : odsDebtor.NAMECUST.Trim();
            debtor.GroupCode = odsDebtor.IDGRP == null ? null : odsDebtor.IDGRP.Trim();
            debtor.AddressLine1 = odsDebtor.TEXTSTRE1 == null ? null : odsDebtor.TEXTSTRE1.Trim();
            debtor.AddressLine2 = odsDebtor.TEXTSTRE2 == null ? null : odsDebtor.TEXTSTRE2.Trim();
            debtor.AddressLine3 = odsDebtor.TEXTSTRE3 == null ? null : odsDebtor.TEXTSTRE3.Trim();
            debtor.AddressLine4 = odsDebtor.TEXTSTRE4 == null ? null : odsDebtor.TEXTSTRE4.Trim();
            debtor.City = odsDebtor.NAMECITY == null ? null : odsDebtor.NAMECITY.Trim();
            debtor.StateProvince = odsDebtor.CODESTTE == null ? null : odsDebtor.CODESTTE.Trim();
            debtor.PostalCode = odsDebtor.CODEPSTL == null ? null : odsDebtor.CODEPSTL.Trim();
            debtor.Country = odsDebtor.CODECTRY == null ? null : odsDebtor.CODECTRY.Trim();
            debtor.ContactName = odsDebtor.NAMECTAC == null ? null : odsDebtor.NAMECTAC.Trim();
            debtor.Telephone = odsDebtor.TEXTPHON1 == null ? null : odsDebtor.TEXTPHON1.Trim();
            debtor.Fax = odsDebtor.TEXTPHON2 == null ? null : odsDebtor.TEXTPHON2.Trim();
            debtor.TaxRegistrationNumber = odsDebtor.IDTAXREGI1 == null ? null : odsDebtor.IDTAXREGI1.Trim();
            debtor.ContactEmail = odsDebtor.EMAIL1 == null ? null : odsDebtor.EMAIL1.Trim();
            debtor.Email = odsDebtor.EMAIL2 == null ? null : odsDebtor.EMAIL2.Trim();
            debtor.ContactTelephone = odsDebtor.CTACPHONE == null ? null : odsDebtor.CTACPHONE.Trim();
            debtor.ContactFax = odsDebtor.CTACFAX == null ? null : odsDebtor.CTACFAX.Trim();
            debtor.DebtorCodePrefix = odsDebtor.PREFIX == null ? null : odsDebtor.PREFIX.Trim();
            debtor.CurrencySymbol = odsDebtor.CURNSYMBOL == null ? null : odsDebtor.CURNSYMBOL.Trim();
            debtor.CurrencyCode = odsDebtor.CURNCODE == null ? null : odsDebtor.CURNCODE.Trim();

            return debtor;
        }

        public static StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Debtors.Debtor LoadDebtorFromOds(int odsId)
        {
            OpsHubEntities dataContext = new OpsHubEntities();
            var odsDebtor = dataContext.ARCustomers.Where(p => p.ARCUSTID == odsId).First();

            return LoadDebtorFromOdsObject(odsDebtor);
        }

        public static StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Debtors.Debtor LoadDebtorFromOds(Guid crmId)
        {
            OpsHubEntities dataContext = new OpsHubEntities();
            var odsDebtor = dataContext.ARCustomers.Where(p => p.MSCUST == crmId).First();

            return LoadDebtorFromOdsObject(odsDebtor);
        }

        public static void DeleteDebtorFromOds(int odsId)
        {
            OpsHubEntities dataContext = new OpsHubEntities();
            var odsDebtor = dataContext.ARCustomers.Where(p => p.ARCUSTID == odsId).First();
            dataContext.DeleteObject(odsDebtor);
            dataContext.SaveChanges();
        }

        public static List<StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Debtors.Debtor> GetDebtors(DebtorOrigin origin, StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Enum.TransactionStatus transactionStatus)
        {
            var result = new List<StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Debtors.Debtor>();

            OpsHubEntities dataContext = new OpsHubEntities();

            var odsDebtors = dataContext.ARCustomers.
                                            Where(p => p.TRXSTATUS.Value == (int)transactionStatus).
                                            Where(p => p.ORIGIN == (int)origin);

            foreach (var odsDebtor in odsDebtors)
            {
                result.Add(LoadDebtorFromOds(odsDebtor.ARCUSTID));
            }

            return result;
        }

        public static void ChangeDebtorTransactionStatus(int odsId, TransactionStatus transactionStatus)
        {
            OpsHubEntities dataContext = new OpsHubEntities();

            var odsDebtor = dataContext.ARCustomers.Where(p => p.ARCUSTID == odsId).First();

            odsDebtor.TRXSTATUS = (int)transactionStatus;

            dataContext.SaveChanges();
        }

        #endregion

        #region Payment Schedule

        public static void RectifyPaymentSchedule(Guid programmeOfferingId)
        {
            var paymentScheduleItems = (from d in DataContext.PaymentSchedules where d.ProgrammeOfferingID == programmeOfferingId select d).ToList();

            var totalAmount = new decimal();

            foreach (var item in paymentScheduleItems)
            {
                totalAmount += item.Amount.Value;
            }

            foreach (var item in paymentScheduleItems)
            {
                var newItem = new StellenboschUniversity.UsbEd.Integration.Adapters.Ods.Model.PaymentSchedule();

                newItem.Description = Math.Round(item.Amount.Value / totalAmount, 2);

                newItem.Amount = item.Amount;

                newItem.Date = item.Date;

                newItem.ProgrammeOfferingID = item.ProgrammeOfferingID;

                newItem.TaskActivityID = item.TaskActivityID;

                DataContext.DeleteObject(item);

                DataContext.AddToPaymentSchedules(newItem);

                DataContext.SaveChanges();
            }
        }

        public static void CreatePaymentSchedule(DtoInvoicingModel.PaymentSchedule paymentSchedule)
        {
            DataContext.AddToPaymentSchedules(paymentSchedule.ToOdsDataModel());
           
            DataContext.SaveChanges();
        }

        public static DtoInvoicingModel.PaymentSchedule UpdatePaymentSchedule(DtoInvoicingModel.PaymentSchedule paymentSchedule)
        {
            //performs a replace rather than a delete                
            var item = (from d in DataContext.PaymentSchedules where d.ID == paymentSchedule.Id select d).FirstOrDefault();

            item.Date = paymentSchedule.Date;
            item.Amount = paymentSchedule.Amount;
            item.Description = paymentSchedule.Description;
           
            DataContext.SaveChanges();

            return item.ToDto();
        }

        public static Model.PaymentSchedule DeletePaymentSchedule(int paymentScheduleId)
        {
            var item = (from d in DataContext.PaymentSchedules where d.ID == paymentScheduleId select d).FirstOrDefault();

            DataContext.DeleteObject(item);

            DataContext.SaveChanges();

            return item;
        }

        public static IList<DtoInvoicingModel.PaymentSchedule> GetPaymentSchedule(Guid programmeOfferingId)
        {
            var paymentSchedule = (from d in DataContext.PaymentSchedules where d.ProgrammeOfferingID == programmeOfferingId orderby d.Date select d).ToList();

            return paymentSchedule.ToDto();
        }

        public static decimal GetPaymentScheduleTotal(Guid programmeOfferingId)
        {
            var paymentScheduleItems = (from d in DataContext.PaymentSchedules where d.ProgrammeOfferingID == programmeOfferingId select d).ToList();

            var result = new decimal();

            foreach (var item in paymentScheduleItems)
            {
                result += item.Amount.Value;
            }

            return result;

        }

        public static IList<DtoInvoicingModel.PaymentSchedule> GetPaymentScheduleItems(DateTime startDate, DateTime endDate)
        {
            var paymentSchedules = (from d in DataContext.PaymentSchedules where d.Date >= startDate && d.Date <= endDate select d).ToList();

            return paymentSchedules.ToDto();
        }

        #endregion

        #region Invoicing

        public static void SaveInvoiceToOds(DtoInvoicingModel.Invoice invoice)
        {
            OpsHubEntities dataContext = new OpsHubEntities();

            foreach (var invoiceLineItem in invoice.LineItems)
            {
                var odsInvoice = new StellenboschUniversity.UsbEd.Integration.Adapters.Ods.Model.ARInvoice();

                // invoice header
                odsInvoice.MSIDINVC = invoice.CrmId;
                odsInvoice.MSINVAPPTO = invoice.CrmId;
                odsInvoice.IDCUST = invoice.DebtorCode;
                odsInvoice.DOCTYPE = (int)InvoiceDocumentType.Invoice;
                odsInvoice.DATEINVC = invoice.CreationDate;
                odsInvoice.HEADOPT1 = invoice.Header1;
                odsInvoice.HEADOPT2 = invoice.Header2;
                odsInvoice.HEADOPT3 = invoice.Header3;
                odsInvoice.HEADOPT4 = invoice.Header4;
                odsInvoice.HEADOPT5 = invoice.Header5;

                if (invoice.DueDate.HasValue)
                {
                    odsInvoice.DATEDUE = invoice.DueDate.Value.AddHours(15);
                }
                else
                {
                    odsInvoice.DATEDUE = null;
                }
                
                // invoice line item
                odsInvoice.AMTNETTOT = invoiceLineItem.Amount;
                odsInvoice.GLACCOUNT = invoiceLineItem.GlAccount;
                odsInvoice.TAXCLASS = invoiceLineItem.Taxable ? 1 : 2;      // 1 = taxable, 2 = non-taxable
                odsInvoice.INVCDESC = invoiceLineItem.Description;
                odsInvoice.DETCOMMENT = invoiceLineItem.Comment;
                odsInvoice.DETOPT1 = invoiceLineItem.AdditionalField1;
                odsInvoice.DETOPT2 = invoiceLineItem.AdditionalField2;
                odsInvoice.DETOPT3 = invoiceLineItem.AdditionalField3;
                odsInvoice.DETOPT4 = invoiceLineItem.AdditionalField4;
                odsInvoice.DETOPT5 = invoiceLineItem.AdditionalField5;

                // status
                odsInvoice.TRXSTATUS = (int)TransactionStatus.SubmittedByCrm;

                dataContext.AddToARInvoices(odsInvoice);
                dataContext.SaveChanges();
            }
        }

        public static void CancelInvoice(DtoInvoicingModel.Invoice invoice)
        {
            OpsHubEntities dataContext = new OpsHubEntities();
            
            var cancelInvoice = new StellenboschUniversity.UsbEd.Integration.Adapters.Ods.Model.ARInvoice();

            cancelInvoice.INVCAPPLTO = invoice.DocumentNumber;
            cancelInvoice.DOCTYPE = (int)InvoiceDocumentType.Cancellation;

            // status
            cancelInvoice.TRXSTATUS = (int)TransactionStatus.SubmittedByCrm;

            dataContext.AddToARInvoices(cancelInvoice);
            dataContext.SaveChanges();
        }

        public static List<Guid> GetPostedInvoiceIds()
        {
            var uniqueCrmIds = new HashSet<Guid>();

            OpsHubEntities dataContext = new OpsHubEntities();

            var odsInvoices = dataContext.ARInvoices.
                                            Where(p => p.DOCTYPE.Value == (int)InvoiceDocumentType.Invoice).
                                            Where(p => p.TRXSTATUS.Value == (int)TransactionStatus.Posted);

            foreach (var odsInvoice in odsInvoices)
            {
                if (odsInvoice.MSIDINVC.HasValue == true)
                {
                    uniqueCrmIds.Add(odsInvoice.MSIDINVC.Value);
                }
            }

            return uniqueCrmIds.ToList();
        }

        public static decimal? GetInvoiceBalance(Guid invoiceId)
        {
            OpsHubEntities dataContext = new OpsHubEntities();

            var odsInvoice = dataContext.ARInvoices.
                                            Where(p => p.MSIDINVC == invoiceId).First();

            return odsInvoice.AMTBALDUE;
        }

        public static string GetInvoiceNumber(Guid invoiceId)
        {
            OpsHubEntities dataContext = new OpsHubEntities();

            var odsInvoice = dataContext.ARInvoices.
                                            Where(p => p.MSIDINVC == invoiceId).First();

            return odsInvoice.IDINVC.Trim();
        }

        #endregion
        
        #region Credit Notes

        public static void SaveCreditNoteToOds(DtoInvoicingModel.CreditNote creditNote)
        {
            OpsHubEntities dataContext = new OpsHubEntities();

            var odsCreditNote = new StellenboschUniversity.UsbEd.Integration.Adapters.Ods.Model.ARInvoice();

            odsCreditNote.MSIDINVC = creditNote.CrmId;
            odsCreditNote.MSINVAPPTO = creditNote.CrmRelatedInvoiceId;
            odsCreditNote.AMTNETTOT = creditNote.Amount;
            odsCreditNote.IDCUST = creditNote.DebtorCode;
            odsCreditNote.GLACCOUNT = creditNote.GlAccount;
            odsCreditNote.TAXCLASS = creditNote.Taxable ? 1 : 2;    // 1 = taxable, 2 = non-taxable
            
            odsCreditNote.DATEINVC = creditNote.Date;

            odsCreditNote.HEADOPT1 = creditNote.Header1;
            odsCreditNote.HEADOPT2 = creditNote.Header2;
            odsCreditNote.HEADOPT3 = creditNote.Header3;
            odsCreditNote.HEADOPT4 = creditNote.Header4;
            odsCreditNote.HEADOPT5 = creditNote.Header5;

            odsCreditNote.DETOPT1 = creditNote.AdditionalField1;
            odsCreditNote.DETOPT2 = creditNote.AdditionalField2;
            odsCreditNote.DETOPT3 = creditNote.AdditionalField3;
            odsCreditNote.DETOPT4 = creditNote.AdditionalField4;
            odsCreditNote.DETOPT5 = creditNote.AdditionalField5;

            odsCreditNote.INVCDESC = "";

            switch (creditNote.Type)
            {
                case CreditNoteType.Discount:
                    odsCreditNote.DOCTYPE = (int)InvoiceDocumentType.Discount;
                    break;

                case CreditNoteType.Cancellation:
                    odsCreditNote.DOCTYPE = (int)InvoiceDocumentType.Cancellation;
                    break;

                default:
                    break;
            }

            odsCreditNote.TRXSTATUS = (int)TransactionStatus.SubmittedByCrm;

            dataContext.AddToARInvoices(odsCreditNote);
            dataContext.SaveChanges();
        }

        public static List<Guid> GetPostedCreditNoteIds()
        {
            var uniqueCrmIds = new HashSet<Guid>();

            OpsHubEntities dataContext = new OpsHubEntities();

            var odsCreditNotes = dataContext.ARInvoices.
                                            Where(p => (p.DOCTYPE == (int)InvoiceDocumentType.Discount) || (p.DOCTYPE == (int)InvoiceDocumentType.Cancellation)).
                                            Where(p => p.TRXSTATUS.Value == (int)TransactionStatus.Posted);

            foreach (var odsCreditNote in odsCreditNotes)
            {
                if (odsCreditNote.MSIDINVC.HasValue == true)
                {
                    uniqueCrmIds.Add(odsCreditNote.MSIDINVC.Value);
                }
            }

            return uniqueCrmIds.ToList();
        }

        public static List<Tuple<string, Guid, decimal, string>> GetPostedDebitNoteIds()
        {
            var debitNotes = new List<Tuple<string, Guid, decimal, string>>();  // debit note number, invoice number, amount, GL Account code

            OpsHubEntities dataContext = new OpsHubEntities();

            var odsDebitNotes = dataContext.ARInvoices.
                                            Where(p => (p.DOCTYPE == (int)InvoiceDocumentType.DebitNote)).
                                            Where(p => p.TRXSTATUS.Value == (int)TransactionStatus.Posted);

            foreach (var odsDebitNote in odsDebitNotes)
            {
                debitNotes.Add(new Tuple<string, Guid, decimal, string>(odsDebitNote.IDINVC, odsDebitNote.MSINVAPPTO.Value, odsDebitNote.AMTNETTOT.Value, odsDebitNote.GLACCOUNT));
            }

            return debitNotes;
        }

        public static string GetCreditNoteNumber(Guid creditNoteId)
        {
            OpsHubEntities dataContext = new OpsHubEntities();

            var odsCreditNote = dataContext.ARInvoices.
                                            Where(p => p.MSIDINVC == creditNoteId).First();

            if (String.IsNullOrEmpty(odsCreditNote.IDINVC) == true)
            {
                return null;
            }
            else
            {
                return odsCreditNote.IDINVC.Trim();
            }
        }
        
        #endregion

        #region Group codes

        public static List<GroupCode> GetGroupCodes()
        {
            var result = new List<GroupCode>();

            OpsHubEntities dataContext = new OpsHubEntities();
            var odsGroupCodes = dataContext.vwARGROes.ToList();

            foreach (var groupCode in odsGroupCodes)
            {
                result.Add(new GroupCode()
                {
                    Id = groupCode.IDGRP.Trim(),
                    Description = groupCode.TEXTDESC.Trim()
                });
            }

            return result;
        }

        #endregion

        #region Tax settings

        public static List<TaxSetting> GetTaxSettings()
        {
            var result = new List<TaxSetting>();

            OpsHubEntities dataContext = new OpsHubEntities();
            var odsTaxSettings = dataContext.vwTaxSettings.ToList();

            foreach (var taxSetting in odsTaxSettings)
            {
                result.Add(new TaxSetting()
                {
                    Id = taxSetting.AUTHORITY.Trim(),
                    Class = taxSetting.CLASS,
                    Description = taxSetting.DESC.Trim(),
                    ItemRate1 = taxSetting.ITEMRATE1,
                    ItemRate2 = taxSetting.ITEMRATE2,
                    ItemRate3 = taxSetting.ITEMRATE3,
                    ItemRate4 = taxSetting.ITEMRATE4,
                    ItemRate5 = taxSetting.ITEMRATE5
                });
            }

            return result;
        }

        #endregion

        #region

        public static List<Tuple<string, string, string>> GetProgrammeUsers()
        {
            var result = new List<Tuple<string, string, string>>();

            OpsHubEntities dataContext = new OpsHubEntities();
            var odsProgrammeUsers = dataContext.vwOptionalFields.ToList();

            foreach (var odsProgrammeUser in odsProgrammeUsers)
            {
                result.Add(new Tuple<string, string, string>(
                    odsProgrammeUser.VALUE,
                    odsProgrammeUser.VDESC,
                    odsProgrammeUser.OPTFIELD
                ));
            }

            return result;
        }

        #endregion

        #region GL accounts

        public static List<GLAccount> GetGLAccounts()
        {
            var result = new List<GLAccount>();

            OpsHubEntities dataContext = new OpsHubEntities();
            var odsGLAccounts = dataContext.vwGLAccounts.ToList();

            foreach (var glAccount in odsGLAccounts)
            {
                result.Add(new GLAccount()
                {
                    Id = glAccount.ACCTID.Trim(),
                    Description = glAccount.ACCTDESC.Trim()
                });
            }

            return result;
        }

        #endregion
    }
}
