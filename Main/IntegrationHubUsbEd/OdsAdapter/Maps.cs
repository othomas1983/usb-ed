﻿using System;
using System.Collections.Generic;
using System.Data.Objects.DataClasses;
using StellenboschUniversity.UsbEd.Integration.Core;
using StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Enum;
using DtoModelBudget = StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Budget;
using DtoModelInvoice = StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Invoicing;
using OdsModel = StellenboschUniversity.UsbEd.Integration.Adapters.Ods.Model;

namespace StellenboschUniversity.UsbEd.Integration.Adapters.Ods
{
    public static class Maps
    {
        #region Extension Methods

        public static OdsModel.BudgetSet ToOdsDataModel(this DtoModelBudget.BudgetSet item)
        {
            var budgetSet = new OdsModel.BudgetSet();
            
            budgetSet.SharepointID = item.SharepointId;
            budgetSet.ProgrammeOfferingID = item.ProgrammeOfferingId;
            budgetSet.CrmUserID = item.CrmUserId;
            budgetSet.Set = item.Set;
            budgetSet.StartPeriod = item.StartPeriod;
            budgetSet.Title = item.Title;
            budgetSet.FileName = item.FileName;
            budgetSet.FilePath = item.FilePath;
            budgetSet.DateCreated = item.DateCreated.HasValue ? item.DateCreated : null;
            budgetSet.UserCreatedID = item.UserCreatedId.HasValue ? item.UserCreatedId : null;
            budgetSet.DateModified = item.DateModified.HasValue ? item.DateModified : null;
            budgetSet.UserModifiedID = item.UserModifiedId.HasValue ? item.UserModifiedId : null;
            budgetSet.Status = item.Status.ToInt();
            budgetSet.IsValid = true;

            budgetSet.GeneralLedgerCodes = item.GeneralLedgerCodeCollection.ToOdsDataModel();

            return budgetSet;
        }

        public static OdsModel.GeneralLedgerCode ToOdsDataModel(this DtoModelBudget.GeneralLedgerCode item)
        {
            var generalLedgerCode = new OdsModel.GeneralLedgerCode();

            generalLedgerCode.Code = item.Code;
            generalLedgerCode.Account = item.Account;
            generalLedgerCode.Market = item.Market;
            generalLedgerCode.SubMarket = item.SubMarket;
            generalLedgerCode.ProgrammeCode = item.ProgrammeCode;
            generalLedgerCode.TotalAmount = item.TotalAmount;

            generalLedgerCode.BudgetSetYears = item.YearCollection.ToOdsDataModel();
            
            return generalLedgerCode;
        }

        public static OdsModel.BudgetSetYear ToOdsDataModel(this DtoModelBudget.BudgetSetYear item)
        {
            var budgetSetYear = new OdsModel.BudgetSetYear();

            budgetSetYear.FiscalYear = item.FiscalYear;
            budgetSetYear.Amount = item.Amount;

            return budgetSetYear;
        }

        public static EntityCollection<OdsModel.GeneralLedgerCode> ToOdsDataModel(this IList<DtoModelBudget.GeneralLedgerCode> items)
        {
            var entityCollection = new EntityCollection<OdsModel.GeneralLedgerCode>();

            foreach (var item in items)
            {
                entityCollection.Add(item.ToOdsDataModel());
            }

            return entityCollection;
        }

        public static EntityCollection<OdsModel.BudgetSetYear> ToOdsDataModel(this IList<DtoModelBudget.BudgetSetYear> items)
        {
            var entityCollection = new EntityCollection<OdsModel.BudgetSetYear>();

            foreach (var item in items)
            {
                entityCollection.Add(item.ToOdsDataModel());
            }

            return entityCollection;
        }

        public static DtoModelBudget.BudgetSet ToDto(this OdsModel.BudgetSet item)
        {
            var budgetSet = new DtoModelBudget.BudgetSet();

            budgetSet.SharepointId = item.SharepointID.Value;
            budgetSet.ProgrammeOfferingId = item.ProgrammeOfferingID.HasValue ? item.ProgrammeOfferingID.Value : Guid.Empty;
            budgetSet.Set = item.Set.Value;
            budgetSet.StartPeriod = item.StartPeriod.Value;
            budgetSet.Title = item.Title;
            budgetSet.FileName = item.FileName;
            budgetSet.FilePath = item.FilePath;
            budgetSet.DateCreated = item.DateCreated.HasValue ? item.DateCreated : null;
            budgetSet.UserCreatedId = item.UserCreatedID.HasValue ? item.UserCreatedID : null;
            budgetSet.DateModified = item.DateModified.HasValue ? item.DateModified : null;
            budgetSet.UserModifiedId = item.UserModifiedID.HasValue ? item.UserModifiedID : null;
            budgetSet.Status = (StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Enum.BudgetSetStatus)item.Status.Value;

            budgetSet.GeneralLedgerCodeCollection = item.GeneralLedgerCodes.ToDto();

            return budgetSet;
        }

        public static IList<DtoModelBudget.GeneralLedgerCode> ToDto(this EntityCollection<OdsModel.GeneralLedgerCode> items)
        {
            var generalLedgerCodes = new List<DtoModelBudget.GeneralLedgerCode>();

            foreach(var item in items)
            {
                generalLedgerCodes.Add(item.ToDto());
            }

            return generalLedgerCodes;
        }

        public static DtoModelBudget.GeneralLedgerCode ToDto(this OdsModel.GeneralLedgerCode item)
        {
            var generalLedgerCode = new DtoModelBudget.GeneralLedgerCode();

            generalLedgerCode.Code = item.Code;
            generalLedgerCode.Account = item.Account;
            generalLedgerCode.Market = item.Market;
            generalLedgerCode.SubMarket = item.SubMarket;
            generalLedgerCode.ProgrammeCode = item.ProgrammeCode;
            generalLedgerCode.TotalAmount = item.TotalAmount.Value;

            generalLedgerCode.YearCollection = item.BudgetSetYears.ToDto();

            return generalLedgerCode;
        }

        public static IList<DtoModelBudget.BudgetSetYear> ToDto(this EntityCollection<OdsModel.BudgetSetYear> items)
        {
            var budgetSetYears = new List<DtoModelBudget.BudgetSetYear>();

            foreach (var item in items)
            {
                budgetSetYears.Add(item.ToDto());
            }

            return budgetSetYears;
        }

        public static DtoModelBudget.BudgetSetYear ToDto(this OdsModel.BudgetSetYear item)
        {
            var budgetSetYear = new DtoModelBudget.BudgetSetYear();

            budgetSetYear.FiscalYear = item.FiscalYear.Value;
            budgetSetYear.Amount = item.Amount.Value;

            return budgetSetYear;
        }

        public static EntityCollection<OdsModel.GLBudget> ToAccpacModel(this DtoModelBudget.BudgetSet item)
        {
            var budgetSet = new EntityCollection<OdsModel.GLBudget>();

            foreach (var glcode in item.GeneralLedgerCodeCollection)
            {
                var startPeriod = item.StartPeriod;

                foreach (var fiscalyear in glcode.YearCollection)
                {
                    budgetSet.Add(glcode.ToAccpacModel(fiscalyear, item, startPeriod));

                    //default start period to 1
                    startPeriod = 1;
                }
            }

            return budgetSet;
        }

        public static OdsModel.GLBudget ToAccpacModel(this DtoModelBudget.GeneralLedgerCode item, DtoModelBudget.BudgetSetYear year, DtoModelBudget.BudgetSet budgetSet, int startPeriod)
        {
            var glBudget = new OdsModel.GLBudget();

            glBudget.ACCTID = item.Code.Replace(HubConfig.BudgetGLFormattedString, string.Empty);
            glBudget.FSCSYR = year.FiscalYear.ToString();
            glBudget.AMOUNT = year.Amount;
            glBudget.FSCSDSG = budgetSet.Set;
            glBudget.STPERIOD = startPeriod;
            glBudget.TRXSTATUS = TransactionStatus.SubmittedByCrm.ToInt();

            return glBudget;
        }

        public static IList<OdsModel.PaymentSchedule> ToOdsDataModel(this IList<DtoModelInvoice.PaymentSchedule> items)
        {
            var paymentSchedule = new List<OdsModel.PaymentSchedule>();

            foreach (var item in items)
            {
                paymentSchedule.Add(item.ToOdsDataModel());
            }

            return paymentSchedule;
        }

        public static OdsModel.PaymentSchedule ToOdsDataModel(this DtoModelInvoice.PaymentSchedule item)
        {
            var paymentSchedule = new OdsModel.PaymentSchedule();

            paymentSchedule.ID = item.Id;
            paymentSchedule.ProgrammeOfferingID = item.ProgrammeOfferingId;
            paymentSchedule.Description = item.Description;
            paymentSchedule.Date = item.Date;
            paymentSchedule.Amount = item.Amount;
            paymentSchedule.TaskActivityID = item.TaskActivityId;

            return paymentSchedule;
        }

        public static IList<DtoModelInvoice.PaymentSchedule> ToDto(this IList<OdsModel.PaymentSchedule> items)
        {
            var list = new List<DtoModelInvoice.PaymentSchedule>();

            foreach (var item in items)
            {
                list.Add(item.ToDto());
            }

            return list;
        }

        public static DtoModelInvoice.PaymentSchedule ToDto(this OdsModel.PaymentSchedule item)
        {
            var paymentSchedule = new DtoModelInvoice.PaymentSchedule();

            paymentSchedule.Id = item.ID;
            paymentSchedule.ProgrammeOfferingId = item.ProgrammeOfferingID.Value;
            paymentSchedule.Description = item.Description.Value;
            paymentSchedule.Date = item.Date.Value;
            paymentSchedule.Amount = item.Amount.Value;
            paymentSchedule.TaskActivityId = item.TaskActivityID.Value;

            return paymentSchedule;
        }

        #endregion

    }
}
