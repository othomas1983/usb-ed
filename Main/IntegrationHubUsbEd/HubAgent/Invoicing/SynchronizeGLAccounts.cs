﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Debtors;
using StellenboschUniversity.UsbEd.Integration.Adapters.Crm;
using StellenboschUniversity.UsbEd.Integration.Adapters.Ods;
using NLog;

namespace StellenboschUniversity.UsbEd.Integration.HubAgent.Invoicing
{
    public static class SynchronizeGLAccounts
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        
        public static void Poll()
        {
            try
            {
                var accpacGLAccounts = OdsAdapter.GetGLAccounts();

                foreach (var accpacGLAccount in accpacGLAccounts)
                {
                    if (CrmAdapter.GLAccountExists(accpacGLAccount.Id) == false)
                    {
                        CrmAdapter.CreateGLAccount(accpacGLAccount);
                    }
                    else
                    {
                        CrmAdapter.UpdateGLAccount(accpacGLAccount);
                    }
                }
            }
            catch (Exception exception)
            {
                logger.Log(LogLevel.Error, "Exception in SynchronizeGLAccounts: {0}{1}{2}", exception.Message, Environment.NewLine, "=============================");
            }
        }
    }
}
