﻿
namespace StellenboschUniversity.UsbEd.Integration.Core
{
    public static class HTMLWrapper
    {
        #region Locals

        private const string _TABLE = "<table style='margin: 8px; border-collapse: collapse; font-family: Segoe UI, Tahoma, Arial;'>{0}</table>";
        private const string _TD = "<td style='padding: 7px; border-style: none none solid none; border-width: 2px; border-color: #990033; font-size:11px;'>{0}</td>";
        private const string _TR = "<tr>{0}</tr>";
        private const string _STRONG = "<strong>{0}</strong>";

        #endregion

        public static string WrapTd(this string item)
        {
            return string.Format(_TD, item);
        }
        public static string WrapTr(this string item)
        {
            return string.Format(_TR, item);
        }
        public static string WrapTable(this string item)
        {
            return string.Format(_TABLE, item);
        }
        public static string WrapStrong(this string item)
        {
            return string.Format(_STRONG, item);
        }
       
    }
}
