﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Invoicing
{
    public class TaxSetting
    {
        public string Id
        {
            get;
            set;
        }

        public int Class
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public decimal ItemRate1
        {
            get;
            set;
        }

        public decimal ItemRate2
        {
            get;
            set;
        }

        public decimal ItemRate3
        {
            get;
            set;
        }

        public decimal ItemRate4
        {
            get;
            set;
        }

        public decimal ItemRate5
        {
            get;
            set;
        }
    }
}
