﻿using System.Data.Objects.DataClasses;
using StellenboschUniversity.UsbEd.Integration.Adapters.Accpac.Model;

namespace StellenboschUniversity.UsbEd.Integration.Adapters.Accpac
{
    public class AccpacAdapter
    {
        #region Locals

        private const string _CONNECTIONSTRING = "metadata=res://*/AccpacModel.csdl|res://*/AccpacModel.ssdl|res://*/AccpacModel.msl;provider=System.Data.SqlClient;provider connection string='data source=bpcsqldev;initial catalog=EMWARE;user id=accpacdev;password=W@Gw00rd22722;multipleactiveresultsets=True;App=EntityFramework'";

        #endregion

        #region Properties

        private static EMWAREEntities DataContext { get; set; }

        #endregion

        #region Methods

        public static void Initialize()
        {
            DataContext = new EMWAREEntities(_CONNECTIONSTRING);
        }

        public static string CreateAccpacBudgetSet(EntityCollection<GLBudget> budgetSet)
        {
            string message;

            foreach (var glBudget in budgetSet)
            {
                DataContext.AddToGLBudgets(glBudget);
            }
            
            DataContext.SaveChanges();

            message = "Sent to AccPac successfully";


            return message;
        }

        #endregion
    }
}
