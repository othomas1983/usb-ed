﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Debtors;
using StellenboschUniversity.UsbEd.Integration.Adapters.Crm;
using StellenboschUniversity.UsbEd.Integration.Adapters.Ods;
using NLog;

namespace StellenboschUniversity.UsbEd.Integration.HubAgent.Invoicing
{
    public static class SynchronizeProgrammeUsers
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public static void Poll()
        {
            try
            {
                var accpacProgrammeUsers = OdsAdapter.GetProgrammeUsers();

                foreach (var accpacProgrammeUser in accpacProgrammeUsers)
                {
                    if (CrmAdapter.ProgrammeUserExists(accpacProgrammeUser.Item1, accpacProgrammeUser.Item2, accpacProgrammeUser.Item3) == false)
                    {
                        CrmAdapter.CreateProgrammeUser(accpacProgrammeUser.Item1, accpacProgrammeUser.Item2, accpacProgrammeUser.Item3);
                    }
                }
            }
            catch (Exception exception)
            {
                logger.Log(LogLevel.Error, "Exception in SynchronizeProgrammeUsers: {0}{1}{2}", exception.Message, Environment.NewLine, "=============================");
            }
        }
    }
}
