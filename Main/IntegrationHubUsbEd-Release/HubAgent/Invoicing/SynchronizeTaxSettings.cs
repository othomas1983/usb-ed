﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Debtors;
using StellenboschUniversity.UsbEd.Integration.Adapters.Crm;
using StellenboschUniversity.UsbEd.Integration.Adapters.Ods;
using NLog;

namespace StellenboschUniversity.UsbEd.Integration.HubAgent.Invoicing
{
    public static class SynchronizeTaxSettings
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        
        public static void Poll()
        {
            try
            {
                var accpacTaxSettings = OdsAdapter.GetTaxSettings();

                foreach (var accpacTaxSetting in accpacTaxSettings)
                {
                    if (CrmAdapter.TaxSettingExists(accpacTaxSetting.Id, accpacTaxSetting.Class) == false)
                    {
                        CrmAdapter.CreateTaxSetting(accpacTaxSetting);
                    }
                    else
                    {
                        CrmAdapter.UpdateTaxSetting(accpacTaxSetting);
                    }
                }
            }
            catch (Exception exception)
            {
                logger.Log(LogLevel.Error, "Exception in SynchronizeTaxSettings: {0}{1}{2}", exception.Message, Environment.NewLine, "=============================");
            }
        }
    }
}
