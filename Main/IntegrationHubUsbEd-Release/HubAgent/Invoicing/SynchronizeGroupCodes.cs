﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Debtors;
using StellenboschUniversity.UsbEd.Integration.Adapters.Crm;
using StellenboschUniversity.UsbEd.Integration.Adapters.Ods;
using NLog;

namespace StellenboschUniversity.UsbEd.Integration.HubAgent.Invoicing
{
    public static class SynchronizeGroupCodes
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public static void Poll()
        {
            try
            {
                var accpacGroupCodes = OdsAdapter.GetGroupCodes();

                foreach (var accpacGroupCode in accpacGroupCodes)
                {
                    if (CrmAdapter.GroupCodeExists(accpacGroupCode.Id) == false)
                    {
                        CrmAdapter.CreateGroupCode(accpacGroupCode);
                    }
                    else
                    {
                        CrmAdapter.UpdateGroupCode(accpacGroupCode);
                    }
                }
            }
            catch (Exception exception)
            {
                logger.Log(LogLevel.Error, "Exception in SynchronizeGroupCodes: {0}{1}{2}", exception.Message, Environment.NewLine, "=============================");
            }
        }
    }
}
