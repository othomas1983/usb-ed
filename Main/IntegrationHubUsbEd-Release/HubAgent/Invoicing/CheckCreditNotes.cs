﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StellenboschUniversity.UsbEd.Integration.Adapters.Ods;
using StellenboschUniversity.UsbEd.Integration.Adapters.Crm;
using StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Enum;

namespace StellenboschUniversity.UsbEd.Integration.HubAgent.Invoicing
{
    public class CheckCreditNotes
    {
        public static void Poll()
        {
            // discount and cancellation credit notes

            List<Guid> postedDiscountAndCancellationCreditNoteIds = OdsAdapter.GetPostedCreditNoteIds();

            foreach (var postedCreditNoteId in postedDiscountAndCancellationCreditNoteIds)
            {
                try
                {
                    string creditNoteNumber = OdsAdapter.GetCreditNoteNumber(postedCreditNoteId);

                    if (creditNoteNumber != null)
                    {
                        CrmAdapter.UpdateCreditNoteNumber(postedCreditNoteId, creditNoteNumber);
                        CrmAdapter.UpdateCreditNoteAccpacIntegrationStatus(postedCreditNoteId, AccpacIntegrationStatus.IntegratedWithAccpac);
                    }
                }
                catch { }
            }

            // debit notes

            List<Tuple<string, Guid, decimal, string>> postedDebitNotes = OdsAdapter.GetPostedDebitNoteIds();

            foreach (var postedDebitNote in postedDebitNotes)
            {
                try
                {
                    string debitNoteNumber = postedDebitNote.Item1;
                    Guid invoiceId = postedDebitNote.Item2;
                    decimal amount = postedDebitNote.Item3;
                    string glCode = postedDebitNote.Item4;

                    if (CrmAdapter.CreditNoteExists(debitNoteNumber) == false)
                    {
                        CrmAdapter.CreateDebitNote(debitNoteNumber, invoiceId, amount, glCode);
                    }
                }
                catch { }
            }
        }
    }
}
