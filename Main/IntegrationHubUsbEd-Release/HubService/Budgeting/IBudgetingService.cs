﻿using System.ServiceModel;
using System.ServiceModel.Web;
using StellenboschUniversity.UsbEd.Integration.DataTransferObjects.Budget;

namespace StellenboschUniversity.UsbEd.Integration.HubService
{
    [ServiceContract]
    public interface IBudgetingService
    {
        [OperationContract]
        SharepointResult SendBudgetToOds(int sharepointId);

        [OperationContract]
        void DeleteBudgetSet(int sharepointId);

        //Note: json format should be {"POID":"String content","UserID":"String content"}
        [OperationContract]
        [WebGet(BodyStyle = WebMessageBodyStyle.Bare,
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "SendBudgetToAccpac?message={message}")]
        string SendBudgetToAccpac(string message);

    }

}
