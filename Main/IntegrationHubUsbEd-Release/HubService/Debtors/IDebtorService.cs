﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace StellenboschUniversity.UsbEd.Integration.HubService
{
    [ServiceContract]
    public interface IDebtorService
    {
        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.Bare,
            Method = "POST",
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,
            UriTemplate = "SendDebtorToAccpac?message={message}")]
        void SendDebtorToAccpac(string message);
    }
}
