﻿function fileUploadHelper(config) {
    var instance = this;

    instance.canDragDropFiles = window.supportsDragAndDrop;
    
    instance.index = config.index;
    instance.advisorId = config.advisorId;

    instance.eventListener = (config.eventListener ? config.eventListener : instance.uploadTable);
    instance.control = config.control; //file input
    instance.form = instance.control.closest("form");
    instance.uploadRow = instance.control.closest("tr.fileUploadRow");
    instance.uploadTable = instance.uploadRow.closest("table");
    instance.uploadButton = instance.control.closest(".iconButton");
    instance.viewButton = $(".viewDocumentLink", instance.uploadRow);
    instance.deleteButton = $(".deleteDocumentLink", instance.uploadRow);
    instance.removeRowButton = $(".removeRow", instance.uploadRow);

    instance.supportsMultipleDocs = instance.uploadRow.hasAttr("uniquename") && instance.uploadRow.attr("uniquename") != "";
    instance.advisorId = config.advisorId;
    instance.documentId = instance.uploadRow.hasAttr("documentId") && instance.uploadRow.attr("documentId") != "" ? instance.uploadRow.attr("documentId") : false;
    instance.hasDocument = function () { return instance.uploadRow.hasAttr("documentId") && instance.uploadRow.attr("documentId") != "" ? true : false; }
    instance.downloadLink = config.downloadLink;

    instance.maxFileSize = 10485760; //10MB // 4194304; //4MB
    instance.sizeToMb = function (val) { return (val / 1048576).toFixed(2); };
    instance.maxFileSizeInMB = instance.sizeToMb(instance.maxFileSize);

    instance.allowedSelecttionTypes = ["application/pdf", "image/tiff", "image/jpg", "image/jpeg"]; //For file input controll (Filters file types in explorer, not supported in <= IE 9)
    instance.acceptedFileTypes = [".pdf", ".tif", ".tiff", ".jpg", ".jpeg"];
    instance.validationMessage = "<p>File must be less than " + instance.maxFileSizeInMB + "MB<br /><br />File must be in one of the following formats: <br />" + instance.acceptedFileTypes.toCSV() + "</p>";


    this.load = function () {
        instance.control.attr("accept", instance.allowedSelecttionTypes.toCSV());

        instance.deleteButton.on("click", function () {
            instance.confirmDeletion();
        });

        instance.deleteButton.on("forcedelete", function (e) {
            instance.deleteFile(function () {
                if (e.done) {
                    e.done()
                };
            });

        });

        instance.uploadRow.on("change keyup", ".additionalInfoFrom", function () {
            var val = $(this).val();
            $(".additionalInfoTo", instance.uploadRow).val(val);
            $(".additionalInfoSpan", instance.uploadRow).html(val);
        });

        if (instance.canDragDropFiles) {
            instance.dropZone = instance.uploadRow;
            instance.uploadRow.on("dragenter dragleave drop", function (e) {
                //e.stopPropagation();
                if (!instance.hasDocument()) {
                    if (e.type == "dragenter") {
                        instance.uploadRow.addClass("onDrag");
                    } else if (e.type == "dragleave" || e.type == "drop") {
                        instance.uploadRow.removeClass("onDrag");
                    }
                }
            });
        } else {
            instance.dropZone = false;
        }

        instance.uploadInstance = instance.control.fileupload({
            progressInterval: 5,
            dataType: 'json',
            singleFileUploads: true,
            autoUpload: true,
            maxFileSize: instance.maxFileSize,
            url: instance.form.attr("action"),
            //if chrome gives xmlUploadIssues
            //forceIframeTransport: true,
            dropZone: instance.dropZone,
            add: function (e, data) {
                if (instance.hasDocument()) {
                    view.confirm({
                        message: "This item already has a document linked to it, Do you want to remove it and upload a new one?",
                        yes: function () {
                            instance.deleteFile(function () {
                                if (instance.validateFiles(data.files)) {
                                    instance.showButtonMessage(instance.uploadButton, "Uploading file...");
                                    data.submit();
                                };
                            });
                        }
                    });
                } else if (instance.validateFiles(data.files)) {
                    instance.showButtonMessage(instance.uploadButton, "Uploading file...");
                    data.submit();
                };
                instance.uploadRow.removeClass("onDrag");
                instance.removeRowButton.addClass("hide");
            },
            beforeSend: function (xhr, data) {
                $(".additionalInfoFrom", instance.uploadRow).attr("readonly", true);
                data.formData = instance.form.serializeArray();
            },
            done: function (e, data) {
                instance.hideButtonMessage(instance.uploadButton);
                if (data.result.Errors && data.result.Errors.length > 0) {
                    var messages = data.result.Errors;
                    view.alert(messages.toHtmlList(function (item) { return item.Text; }));
                } else {
                    instance.documentId = data.result.Result;
                    instance.documentAdded();
                }
            },
            progress: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);

                instance.hideButtonMessage(instance.uploadButton);
                instance.showButtonMessage(instance.uploadButton, "Uploading file... (<span class='bold uploadProgressCounter'>" + progress + "</span> % complete)");
                if (progress == 100) {
                    instance.hideButtonMessage(instance.uploadButton);
                    instance.showButtonMessage(instance.uploadButton, "Upload Complete, processing file");
                }
            },
            fail: function (e, data) {
                if (data.jqXHR.status != 0) {
                    view.alert(instance.validationMessage);
                }
                instance.hideButtonMessage(instance.uploadButton);
            },
            always: function () {
                $(".additionalInfoFrom", instance.uploadRow).attr("readonly", false);
                instance.removeRowButton.removeClass("hide");
                instance.uploadRow.trigger("onUpdate");
                instance.updated("alwaysAfter");
            }
        });
    };

    this.validateFiles = function (files) {
        var msg = instance.validationMessage;
        var valid = true;

        msg += "<ul class=\"topSpace20\">";
        files.foreach(function (file) {
            var fileArr = file.name.split(".");
            var fileExtension = fileArr[fileArr.length - 1].toLowerCase();
            var fileName = fileArr[0];

            if (!instance.acceptedFileTypes.contains("." + fileExtension)) {
                msg += "<li>";
                msg += "Unsupported file format - <strong>" + fileName + ".<span style='color:red'>" + fileExtension + "</span></strong>";
                msg += "</li>";
                valid = false;
            }
            if (file.size > instance.maxFileSize) {
                msg += "<li>";
                msg += "File to large - <strong>" + file.name + " : <span style='color:red'>" + instance.sizeToMb(file.size) + "MB</span></strong>";
                msg += "</li>";
                valid = false;
            }
        });
        msg += "</ul>";

        if (!valid) {
            view.alert(msg);
            return false;
        } else {
            return true;
        }
    }

    this.confirmDeletion = function () {
        if (instance.documentId) {
            view.confirm({
                yes: function () {
                    instance.deleteFile();
                },
                message: "Are you sure you want to delete this document?"
            });
        } else {
            view.alert("no File to delete");
        }
    };

    this.deleteFile = function (done) {
        instance.showButtonMessage(instance.deleteButton, "Deleting...");

        var formData = {
            DocumentId: instance.documentId,
            DocumentType: $("documentType", instance.uploadRow).val(),
            AdvisorId: instance.advisorId,
            SourceId: $(".sourceId", instance.uploadRow).val(),
            SourceType: $(".sourceType", instance.uploadRow).val()
        };

        $.ajax({
            type: "POST",
            url: "/ClientDocuments/DeleteClientDocument",
            data: formData,
            success: function () {
                instance.hideButtonMessage(instance.deleteButton);
                instance.documentRemoved();
                if(done){
                    done();
                }
                instance.updated("documentDeleted");
            },
            dataType: "json"
        });
    }

    this.documentAdded = function () {
        instance.uploadRow.attr("documentid", instance.documentId);
        $(".fileUploadDocumentId", instance.uploadRow).val(instance.documentId);
        instance.viewButton.attr("href", instance.downloadLink + instance.documentId);
        instance.uploadRow.removeClass("noFile");
        instance.uploadRow.addClass("hasFile");
        instance.updated("documentAdded");
    };

    this.clearValues = function () {
        instance.uploadRow.attr("documentid", "");
        $(".fileUploadDocumentId", instance.uploadRow).val("");
        $(".additionalInfo", instance.uploadRow).val("").trigger("change");
        $(".additionalInfoFrom", instance.uploadRow).val("").trigger("change").prop("readonly", false);
        instance.viewButton.attr("href", "javascript:void();");
        instance.uploadRow.removeClass("hasFile");
        instance.uploadRow.addClass("noFile");
        instance.updated("valuesCleared");
    }
    this.documentRemoved = function () {
        instance.clearValues();
        instance.uploadRow;
        if (instance.supportsMultipleDocs) {
            if ($("tr[uniquename=\"" + instance.uploadRow.attr("uniquename") + "\"]", instance.uploadRow.closest("table")).length > 1) {
                instance.uploadRow.trigger("removerow");
            } else {
                instance.removeRowButton.addClass("hide");
            }
        }
        instance.updated("documentRemoved");
    };

    this.updated = function (trigger) {
        instance.eventListener.trigger({ type: "updated", uploadInstance: instance, trigger: trigger });
    }

    this.showButtonMessage = function (control, msg) {
        if (control.next().is("span.loader")) {
            control.next().replaceWith($.parseHtml(view.loader(msg)));
        } else {
            control.after(view.loader(msg));
            control.hide();
        }
    };

    this.hideButtonMessage = function (control) {
        control.next("span.loader").remove();
        control.show();
    };

    instance.load(instance.control);

    return this;
}
