﻿
<%@ Page Title="" Language="C#"  MasterPageFile="~/Views/Shared/Success.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <br />
     <%: Html.ValidationSummary()%>
    <br />

    <div class="headerImage"></div>

    <div style="height:600px">
        <h1 class="text-danger">Not Succesful</h1>
        <h2 class="text-danger">Sorry. We were unable to process your payment. Please contact your Programme Manager to discuss alternative payment options.</h2>
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>