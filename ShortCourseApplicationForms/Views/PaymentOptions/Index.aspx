﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<!DOCTYPE html>
 <%: System.Web.Optimization.Styles.Render("~/content/TermsAndConditions") %>  
<html>
<head runat="server">
    <meta name="viewport" content="width=device-width" />
    <title>Index</title>
</head>
<body>
    <div id="wrap">
         <form id="frmMain" runat="server">
       <div id="grey" class="head">
            <div class="left" style="width: auto; float: none; margin-right: 70px">              
               <div>
                    <img src="~/../Content/img/logo.jpg" />
                   <label class="headerTextSmall">Executive Development Programmes</label>
                    <label class="headerText">Application Form</label>
                </div> 
                <div>
                    <label class="terms">Payment Options</label>
                </div>
                <div>
                    <strong>Bank Transfer</strong>
                </div>

                <div>
                    <label>Bank:</label>
                    <span>FNB</span>
                </div>

                <div>
                    <label>Account name:</label>
                    <span>USB Executive Development LTD</span>
                </div>

                 <div>
                    <label>Account no.:</label>
                    <span>62206888309</span>
                </div>

                 <div>
                    <label>Branch:</label>
                    <span>Tygerberg</span>
                </div>
                <div>
                    <label>Code:</label>
                    <span>201410</span>
                </div>

                <div>
                    <label>Reference:</label>
                    <span>Programme name and students name or invoice number.</span>
                </div>  

                <p>Please email proof of payment to <a href="mailto:finance@usb-ed.com">finance@usb-ed.com</a> and cc the Programme Manager</p>
                <p>
                    <strong>Cheques </strong>
                    <br>
                    Made payable to USB Executive Development Ltd.
                </p>
                <p>
                    Please note that only electronic transfers, bank deposits and cheques are accepted.
                </p>
                    <p>
                        <strong>Payment</strong><br>
                        Should your company be liable for the programme fees, please provide us with written confirmation on a company letterhead, signed by your training/financial officer, whichever is applicable.<br>
                    </p>
                        <p>Please provide the following if your organisation is responsible for payment.</p>                        
                        <ul>
                            <li>Company full name</li>
                            <li>Company vat number</li>
                            <li>Company registration number</li>
                            <li>Company physical and postal address</li>
                            <li>Company contact and fax number</li>
                            <li>Email address of the department liable for payment</li>
                            <li>A clear copy of your id document/passport must accompany this registration form.</li>
                        </ul>
                    <p>
                        <span>Please note:</span><br>
                        Registration forms without supporting documentation as per the above requirements
            will not be accepted.<br>
                        Payment must be done no later than two weeks prior to commencement of programme.<br>
                    </p>

               
            </div>
        </div>
             </form>
    </div>
</body>
</html>
