﻿
<%@ Page Title="" Language="C#"  MasterPageFile="~/Views/Shared/Success.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <br />
     <%: Html.ValidationSummary()%>
    <br />

    <div class="headerImage"></div>

    <div style="height:600px">
        <h1 class="text-danger">An error occurred</h1>
        <h2 class="text-danger"><%: ViewBag.Error != null ? ViewBag.Error.ToString() : "Unknown" %></h2>
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>