﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ShortCourseApplicationForm.Domain.Models.ShortCourseApplicationForm.ApplicationForm>" %>


<div id="executiveDevelopmentSection">
   <br />
    <fieldset>
        <legend>How did you hear about USB Executive Development?</legend>
        <div>
            <span class="required">*</span>
             <%: Html.DropDownListFor(m => m.MarketingSource, (List<SelectListItem>)ViewBag.MarketingSources, new { @id = "marketingSource" }) %>
            <span class="info"> &nbsp; if other, please specify</span>
            <%: Html.TextBoxFor(m => m.MarketingSourceOther, new { @Value = Model.MarketingSourceOther }) %>
        </div>
  <br />
    <br />
        </fieldset>
    <fieldset>
        <legend>What made you decide on USB Executive Development for your studies?</legend>
        <div>
            <span class="required">*</span>
             <%: Html.DropDownListFor(m => m.MarketingReason, (List<SelectListItem>)ViewBag.MarketingReasons, new { @id = "marketingReason" }) %>
            <span class="info"> &nbsp; if other, please specify</span>
            <%: Html.TextBoxFor(m => m.MarketingReasonOther, new { @Value = Model.MarketingReasonOther }) %>
        </div>    
    </fieldset>
</div>
