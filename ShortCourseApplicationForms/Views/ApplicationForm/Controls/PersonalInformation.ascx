﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ShortCourseApplicationForm.Domain.Models.ShortCourseApplicationForm.ApplicationForm>" %>
<%@ Import Namespace="ShortCourseApplicationForm.Domain.Models.Enums" %>

<fieldset>
    <legend>Personal Information</legend>
    <div class="personalInformationSection">
        <br />
        <div><span id="ErrorDate" style="display:none;color:red;" >Please enter correct date format for year. eg 1900 or later</span></div>
        <div class="studentDetails fieldRow inline">
            <div>
                <%: Html.LabelFor(m => m.Email) %>
                <span class="required">*</span>
                <% if (Model.Email.IsNotNullOrEmpty())
                    { %>
                <%: Html.TextBoxFor(m => m.Email, new {@id = "email", @value = Model.Email, @readonly = "readonly"}) %>
                <% }
                    else
                    { %>
                <%: Html.TextBoxFor(m => m.Email, new { @id = "email", @value = Model.Email }) %>
                <% } %>
            </div>
            <div>
                <%: Html.LabelFor(m => m.Surname)%>
                <span class="required">*</span>
                <% if (Model.AccountType == AccountType.BasilRead || Model.AccountType == AccountType.GetSmarter)
                   { %>
                       <%: Html.TextBoxFor(m => m.Surname, new {@PlaceHolder = "As it appears in your ID or Passport", @Value = Model.Surname, data_bind = "value:surname", @readonly = "readonly" }) %>
                   <% }
                   else
                   { %>
                       <%: Html.TextBoxFor(m => m.Surname, new {@PlaceHolder = "As it appears in your ID or Passport", @Value = Model.Surname, data_bind = "value:surname" }) %>
                   <% }%>
            </div>

            <span class="infoBox">
                <label>
                    On completion of your studies, the following name will be displayed on your certificate of competence.
                 This is a read only display. Please make any changes in the Surname and First Names fields.</label>
                <%: Html.TextBoxFor(m => m.NameOnCertificate, new {@id="displayName", @tabindex="-1", @Readonly="readonly", data_bind="value:fullName"}) %>
                <label>Please confirm that this is correct or make the necessary changes.
                    <input type="checkbox" id="fullNameExcepted" /></label><a style="color: red">*</a>
                <%--<label>Please confirm that this is correct or make the necessary changes. <%: Html.CheckBoxFor(model => model.DisplayNameApproved, new { @id="fullNameExcepted", @Value = Model.DisplayNameApproved })%></label><a style="color: red">*</a>--%>
            </span>

            <div>
                <%: Html.LabelFor(m => m.FirstNames) %>
                <span class="required">*</span>
                <% if (Model.AccountType == AccountType.BasilRead || Model.AccountType == AccountType.GetSmarter)
                   { %>
                       <%: Html.TextBoxFor(m => m.FirstNames, new {@PlaceHolder = "As it appears in your ID or Passport", @Value = Model.FirstNames, data_bind = "value:firstNames", @readonly = "readonly"} ) %>
                   <% }
                   else
                   { %>
                       <%: Html.TextBoxFor(m => m.FirstNames, new {@PlaceHolder = "As it appears in your ID or Passport", @Value = Model.FirstNames, data_bind = "value:firstNames , event{ blur: initialsChanged} "} ) %>
                 <%--<%: Html.TextBoxFor(m => m.FirstNames, new {@PlaceHolder = "As it appears in your ID or Passport", @Value = Model.FirstNames, data_bind = "value:firstNames"} ) %>--%>
                   <% } %>
            </div>
             <div>
                <%: Html.LabelFor(m => m.NameCalledBy) %>
                <span class="required">*</span>                
                 <%: Html.TextBoxFor(m => m.NameCalledBy, new { @Value = Model.NameCalledBy, data_bind = "value:nameCalledBy"})%>
            </div>
            <div>
                <%: Html.LabelFor(m => m.Initials) %>
                <span class="required">*</span>                
                 <%: Html.TextBoxFor(m => m.Initials, new {@id="initials", @Value = Model.Initials, data_bind = "value:initials, event:{ change: initialsChanged}"})%>
            </div>

            <div>
                <%: Html.LabelFor(m => m.Title) %>
                <span class="required">*</span>
                <%: Html.DropDownListFor(m => m.Title, (List<SelectListItem>)ViewBag.Titles, new { @id = "title" }) %>
            </div>

            <div>
                <%: Html.LabelFor(m => m.Gender) %>
                <span class="required">*</span>
                <%: Html.DropDownListFor(m => m.Gender, (List<SelectListItem>)ViewBag.Genders, new { @id = "gender" }) %>
            </div>

            <div>
                <%: Html.LabelFor(m => m.Nationality) %>
                <span class="required">*</span>
                <%: Html.DropDownListFor(m => m.Nationality, (List<SelectListItem>)ViewBag.Nationalities, new { @id = "nationality" }) %>
            </div>

            <div class="personal-SA">
                <%: Html.LabelFor(m => m.Ethnicity) %>
                <span class="required">*</span>
                <%: Html.DropDownListFor(m => m.Ethnicity, (List<SelectListItem>)ViewBag.Ethnicities, new { @id = "ethnicity" }) %>
                <span class="info">&nbsp; Please select the ethnicity group you associate with.</span>
            </div>

            <div class="personal-nonSA">
                <%: Html.LabelFor(m => m.Ethnicity)%>
                <span class="required">*</span>
                <%: Html.DropDownList("EthnicityNonSA", (List<SelectListItem>)ViewBag.EthnicitiesNonSA, new { @id = "ethnicityNonSA" }) %>
                <span class="info">&nbsp; Please select the ethnicity group you associate with.</span>
            </div>

            <div class="personal-SA">
                <%: Html.LabelFor(m => m.IdNumber) %>
                <span class="required">*</span>
                <%: Html.TextBoxFor(m => m.IdNumber, new {@id="identityNumber", @Value = Model.IdNumber })%>
            </div>

            <%: Html.LabelFor(m => m.DateOfBirth) %>
            <span class="required">*</span>
            <%: Html.TextBoxFor(m => m.DateOfBirth, "{0:dd/MM/yyyy}", new { @id = "dateOfBirth" })%>
           

            <div class="personal-nonSA">
                <%: Html.LabelFor(m => m.ForeignIdType) %>
                <span class="required">*</span>
                <%: Html.DropDownListFor(m => m.ForeignIdType, (List<SelectListItem>)ViewBag.IdDocumentTypes,new { @id = "documentType" }) %>
            </div>

            <div class="personal-nonSA">
                <label id="idType"></label>
                <span class="required">*</span>
                <%:Html.TextBoxFor(m => m.ForeignIdNumber, new { @id = "foreignIdNumber" }) %>
                <%--<input id="foreignIdNumber" type="text" name="IdTypeNumber" data-bind="value:idTypeNumber" />--%>
                &nbsp; Expiry date:
                <%:Html.TextBoxFor(m => m.ForeignIdExpiryDate, "{0:dd/MM/yyyy}", new { @id = "idExpiryDate" }) %>
                <%--<input type="text" name="IdExpiryDate" id="idExpiryDate" data-bind="value:idTypeExpiryDate" />--%>
            </div>

            <div>
                <%: Html.LabelFor(m => m.Language) %>
                <span class="required">*</span>
                <%: Html.DropDownListFor(m => m.Language, (List<SelectListItem>)ViewBag.Languages, new { @id = "language" }) %>
            </div>
            <div>
                <%: Html.LabelFor(m => m.Disability) %>
                <span class="required">*</span>
                <%: Html.DropDownListFor(m => m.Disability, (List<SelectListItem>)ViewBag.DisabilityList, new { @id = "disability", @SelectedText = "None" })%>
            </div>

            <% if (!Model.IsMasterStart)
                {%>
            <div>
                <%: Html.LabelFor(m => m.DietaryRequirements) %>
                <span class="required">*</span>
                <%: Html.DropDownListFor(m => m.DietaryRequirements, (List<SelectListItem>)ViewBag.DietaryRequirementList, new { @id = "dietaryRequirements", @SelectedText = "None" })%>
                <span class="info">&nbsp; if other, please specify</span>
                <%: Html.TextBoxFor(m => m.DietaryRequirementsOther) %>
            </div>

            <div>
                <%: Html.LabelFor(m => m.UsesWheelchair) %>
                <span class="placeHolder"></span>
                <%: Html.DropDownListFor(m => m.UsesWheelchair, (List<SelectListItem>)ViewBag.YesNo, new { @id = "usesWheelchair" }) %>
            </div>

            <%} %>
        </div>

    </div>
</fieldset>

<input type="submit" class="navigateButton personalInfoButton" value="Next" data-bind="enable: displayNameApproved()" />
<span id="nameConfirmed" data-bind="visible: !displayNameApproved()">Please confirm the name that will be displayed on your certificate of competence.</span>