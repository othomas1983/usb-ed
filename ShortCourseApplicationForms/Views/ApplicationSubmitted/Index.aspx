﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Success.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="headerImage"></div>

    <div style="height: 600px; text-align: center; margin-top: 50px;">
        <h2>Your application has been submitted successfully.</h2>
    </div>
</asp:Content>
