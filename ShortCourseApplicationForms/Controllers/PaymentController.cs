﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ShortCourseApplicationForm.Domain.Controller;
using ShortCourseApplicationForm.Domain.Models.ShortCourseApplicationForm;

namespace ShortCourseApplicationForms.Controllers
{
    public class PaymentController : Controller
    {
        public ActionResult Succesful()
        {
            var enrolmentId = Session["Enrollment"].ToString();
            var usNumber = Session["UsNumber"].ToString();
            var shortCourseOffering = Session["ShortCourseOffering"].ToString();
            ApplicationSubmissionResult result;
            var controller = new ShortCourseApplicationFormsApiController();

            if (enrolmentId.IsNotNullOrEmpty() && usNumber.IsNotNullOrEmpty() && shortCourseOffering.IsNotNullOrEmpty())
            {
                try
                {
                    var model = new ApplicationForm
                    {
                        Enrolment = Guid.Parse(enrolmentId),
                        UsNumber = usNumber,
                        ShortCourseOfferingId = shortCourseOffering
                    };

                    result = controller.CreateOrUpdateContact(ValidateProgress.GatewayPaymentSuccesful, model);

                    return View();
                }
                catch (Exception ex)
                {
                    TempData["Error"] = ex.Message;
                    return Redirect("/Payment/Error");
                }
                
            }

            TempData["Error"] = "Unable to get your student details from the session.";
            return Redirect("/Payment/Error");
        }

        public ActionResult Error()
        {
            return View();
        }
    }
}