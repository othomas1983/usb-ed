﻿using System.Web.Mvc;

namespace ShortCourseApplicationForms.Controllers
{
    public class ErrorController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Error = TempData["Error"];
            return View();
        }

    }
}