﻿using Newtonsoft.Json;
using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Web;
using System.Web.Mvc;
using Domain.Helpers;
using Extensions;
using iTextSharp.text.pdf;
using ShortCourseApplicationForm.Domain.Controller;
using ShortCourseApplicationForm.Domain.Models.Enums;
using ShortCourseApplicationForm.Domain.Models.ShortCourseApplicationForm;
using ShortCourseApplicationForm.Models;
using ShortCourseApplicationForm.Domain.CRM;

//------------------------ PLEASE NOTE ------------------------
// The way the validation/submit is currently done needs to be re-designed. It makes navigation between tabs slow.
// The way it is done now where correct until the use of tabs where introduced just before going live. :( 
//-------------------------------------------------------------


namespace ShortCourseApplicationForms.Controllers
{
    public class ApplicationFormController : Controller
    {
        ApplicationFormInfo _applicationFormInfo;

        public ActionResult Index()
        {
            try
            {
                var accountId = string.Empty;
                var emailAddress = string.Empty;
                var firstName = string.Empty;
                var lastName = string.Empty;
                var id = string.Empty;
                var shortCourseOfferingId = string.Empty;
                var webTokenId = string.Empty;
                var applicationForm = new ApplicationForm();
                _applicationFormInfo = new ApplicationFormInfo();
                ApplicationFormInfo.Info = null;
                var controller = new ShortCourseApplicationFormsApiController();

#if DEBUG
                //oId= "03477aec-826e-e611-940c-00505695765f"; // Short Course Offering Id
                //id = "7bde7128-6808-e511-827a-402cf4573df7"; // Short Course Id

                //id = "6A8A693C-0836-E511-80C8-005056B8008E"; //"34A47630-0836-E511-80C8-005056B8008E"; //"FD6F79D2-0F36-E511-80C8-005056B8008E"; //"6B6F79D2-0F36-E511-80C8-005056B8008E"; //"caa37630-0836-e511-80c8-005056b8008e";  //"ee2427bb-1d10-e511-827b-402cf4573df7"; //"34A47630-0836-E511-80C8-005056B8008E"; //   "6bc0e7c2-500f-e511-827a-402cf4573df7"; //"73b8b892-9c0a-e511-827a-402cf4573df7"; "74b0f7c8-72fd-e411-8276-402cf4573df7";
                //oId = "be430e33-7200-e511-8278-402cf4573df7";
                //webTokenId = "472E2626-5447-E511-80C9-005056B8008E";

#else
//id = HttpContext.Request["iID"];
//oId = HttpContext.Request["oID"];
//webTokenId = HttpContext.Request["webTokenId"];
//aId = HttpContext.Request["aId"];
#endif

                id = HttpContext.Request["iID"];
                shortCourseOfferingId = HttpContext.Request["oID"];
                webTokenId = HttpContext.Request["webTokenId"];

                // GET from Partner Portal to resolve account and check if qualifying for voucher
                var portalToken = HttpContext.Request["token"];

                if (portalToken.IsNotNullOrEmpty())
                {
                    var decodedData = UrlSafeEncode.Decode(portalToken);
                    var decryptedData = Encryptor.Decrypt(decodedData);

                    if (decryptedData.IsNotNullOrEmpty())
                    {
                        dynamic result = JsonConvert.DeserializeObject<dynamic>(decryptedData);

                        if (result != null)
                        {
                            if (result.ExpiryDate == null)
                            {
                                accountId = result.AccountId;
                                shortCourseOfferingId = result.SelectedOffering;
                                applicationForm.ShortCourseOfferingId = shortCourseOfferingId;
                                applicationForm.Email = result.DelegateEmail;
                                applicationForm.FirstNames = result.DelegateName;
                                applicationForm.Surname = result.DelegateSurname;
                               // applicationForm.NameCalledBy = result.DelegateNameCalledBy;

                                var account = controller.GetAccount(Guid.Parse(accountId));

                                if (account.IsNotNull())
                                {
                                    if (account.AccountType == AccountType.None)
                                    {
                                        TempData["Error"] = "No Account Number or Debtor Code found for this Account.";
                                        return Redirect("/Error/Index");
                                    }

                                    applicationForm.AccountType = account.AccountType;

                                    if (applicationForm.Email.IsNotNullOrEmpty() &&
                                        applicationForm.FirstNames.IsNotNullOrEmpty() &&
                                        applicationForm.Surname.IsNotNullOrEmpty())
                                    {
                                        var contact = controller.Contact(applicationForm);

                                        if (contact.IsNotNull())
                                        {
                                            applicationForm = contact;

                                            var postalDistributionArea = new PostalDistributionArea();
                                            var areas =
                                                postalDistributionArea.LoadAreas(applicationForm.PostalPostalCode);

                                            if (areas.IsNotNull())
                                            {
                                                ViewBag.PostalAreas = areas;
                                            }
                                        }
                                    }

                                    applicationForm.AccountGuid = account.AccountId.GetValueOrDefault();
                                    applicationForm.EmailSectionValidated = true;
                                    applicationForm.QualifyForVoucher = true;
                                    applicationForm.ValidationSection = "PersonalInformation";
                                    applicationForm.SelectedTab = "personalInfoTab";
                                }
                                else
                                {
                                    applicationForm.QualifyForVoucher = false;
                                }
                            }
                            else
                            {
                                DateTime date = Convert.ToDateTime(result.ExpiryDate);

                                if (date > DateTime.Now)
                                {
                                    applicationForm.AccountGuid = result.AccountId;
                                    shortCourseOfferingId = result.SelectedOffering;
                                }
                            }
                        }
                    }
                }
                else
                {
                    applicationForm.SelectedTab = "failed";
                }
                //
               // applicationForm.ApplicationClosed = false;
                if (id.IsNullOrEmpty() && shortCourseOfferingId.IsNullOrEmpty())
                {
                    return Redirect("/NoOffering/Index");
                }
                //else
                //{
                //    //Check if application Closing date is > datenow
                //    bool isClosed = controller.ApplicationClosed(new Guid(HttpContext.Request["iID"]));
                //    if (isClosed)
                //    {
                //        applicationForm.ApplicationClosed = false;
                //    }
                //    else
                //    {
                //        applicationForm.ApplicationClosed = true;
                //    }

                //}


                applicationForm.DirectOfferingLink = false;
                applicationForm.InsideCRM = false;
                
                if (shortCourseOfferingId.IsNotNullOrEmpty())
                {
                    applicationForm.DirectOfferingLink = true;
                }

                if (webTokenId.IsNotNullOrEmpty())
                {
                    applicationForm.WebTokenId = webTokenId;
                    applicationForm.InsideCRM = true;
                    applicationForm.DirectOfferingLink = false;
                }

                applicationForm.ApplicationKey = Guid.NewGuid().AsString();
                applicationForm.ShortCourseId = id;
                applicationForm.ShortCourseOfferingId = shortCourseOfferingId;

                applicationForm.ValidationSection = string.Empty;
                
                _applicationFormInfo = _applicationFormInfo.LoadInfoMembers(applicationForm,
                    applicationForm.ShortCourseId,
                    applicationForm.ShortCourseOfferingId);

                ClearValidationFields(applicationForm);
                
                if (!applicationForm.HasListItems)
                {
                    TempData["Error"] = "No application data returned.";
                    return Redirect("/Error/Index");
                }

                applicationForm.CopyOfID = string.Empty;
                applicationForm.CopyOfCertificate = string.Empty;

                ResolveViewBag(applicationForm);
                ModelState.Clear();
                return View(applicationForm);
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.Message;

                return Redirect("/Error/Index");
            }
        }

        [HttpGet]
        public ActionResult LoadStudentDetails(ApplicationForm model, string studentNumber, string shortCourseOfferingId,
            string webTokenId)
        {
            ModelState.Clear();
            model.ShortCourseOfferingId = shortCourseOfferingId;
            model.WebTokenId = webTokenId;
            model.InsideCRM = true;
            _applicationFormInfo = new ApplicationFormInfo();
            ApplicationFormInfo.Info = null;
            var controller = new ShortCourseApplicationFormsApiController();

            _applicationFormInfo = _applicationFormInfo.LoadInfoMembers(model, model.ShortCourseId,
                model.ShortCourseOfferingId);

            var studentDetails = controller.GetContact(model, studentNumber);

            if (studentDetails.IsNull())
            {
                studentDetails = new ApplicationForm();
                ModelState.AddModelError("studentSearch",
                    "No student details where found for student number: '{0}' ".FormatInvariantCulture(studentNumber));
            }

            studentDetails.InsideCRM = true;
            studentDetails.ShortCourseOfferingId = shortCourseOfferingId;
            studentDetails.WebTokenId = webTokenId;
            studentDetails.Offering = model.Offering;
            studentDetails.CourseName = model.CourseName;
            //var newStartDate = model.CourseStartDate?.AddDays(1).ToString() ?? "";
            studentDetails.CourseStartDate = model.CourseStartDate;
            ResolveViewBag(model);

            return View("Index", studentDetails);
        }

        [HttpPost]
        public ActionResult Index(ApplicationForm model)
        {
            try
            {
                var postalDistributionArea = new PostalDistributionArea();
                var controller = new ShortCourseApplicationFormsApiController();
                var areas = postalDistributionArea.LoadAreas(model.PostalPostalCode);

                if (areas.IsNotNull() && areas.Count!=0)
                {
                    ViewBag.PostalAreas = areas;
                }

                var id = string.Empty;
                var certificate = string.Empty;
                var validationSection = string.Empty;
                var emailSectionValidated = string.Empty;
                var enrollment = string.Empty;
                ApplicationSubmissionResult result;

                if (Request.IsNotNull())
                {
                    id = Request.Form["CopyOfId"];
                    certificate = Request.Form["CopyOfCertificate"];
                    validationSection = Request.Form["ValidationSection"];
                    emailSectionValidated = Request.Form["EmailSectionValidated"];
                }

                if (validationSection == "EmailAddress")
                {
                    if (model.Email.IsNotNullOrEmpty() || model.RegisteredEmail.IsNotNullOrEmpty())
                    {
                        if (model.Email.IsNullOrEmpty())
                            model.Email = model.RegisteredEmail;
                        //var usNumber = controller.GetUsNumber(model.RegisteredEmail);

                        var contact = controller.Contact(model);

                        if (contact.IsNotNull())
                        {
                            model = contact;
                        }
                    }
                }

                if (validationSection == "PersonalInformation")
                {
                    model.EmailSectionValidated = bool.Parse(emailSectionValidated);
                }

                if (validationSection == "Qualifications")
                {
                    model.IdentificationDocumentUploadKey = Session["IdentificationDocumentUploadKey"].IsNull()
                        ? ""
                        : Session["IdentificationDocumentUploadKey"].ToString();
                    model.QualificationDocumentUploadKey = Session["QualificationDocumentUploadKey"].IsNull()
                        ? ""
                        : Session["QualificationDocumentUploadKey"].ToString();
                    model.CVDocumentUploadKey = Session["CVDocumentUploadKey"].IsNull()
                       ? ""
                       : Session["CVDocumentUploadKey"].ToString();

                    if (model.ApplicantStatus == ApplicantStatus.Qualified)
                    {
                        model.AcceptanceDocumentUploadKey = Session["AcceptanceDocumentUploadKey"].IsNull()
                            ? ""
                            : Session["AcceptanceDocumentUploadKey"].ToString();
                    }
                    
                }

                if (id.IsNotNullOrEmpty())
                {
                    model.CopyOfID = id;
                }

                if (certificate.IsNotNullOrEmpty())
                {
                    model.CopyOfCertificate = certificate;
                }

                if (validationSection.IsNotNullOrEmpty())
                {
                    model.ValidationSection = validationSection;
                }

                if (validationSection == "Qualifications")
                {
                    if (model.IdentificationDocumentUploadKey.IsNullOrEmpty())
                    {
                        ModelState.AddModelError("IdDocNotUploaded", "Copy of Identification document not uploaded");
                    }
                    if (model.QualificationDocumentUploadKey.IsNullOrEmpty())
                    {
                        ModelState.AddModelError("QualificationDocNotUploaded",
                            "Copy of Qualification document not uploaded");
                    }

                    var courseName = "";
                    if (model.CourseId.IsNotNullOrEmpty())
                    {
                        courseName = CrmQueries.LoadShortCourseNameByID(new Guid(model.ShortCourseId));
                    }
                    else
                    {
                        if(model.CourseName.IsNotNullOrEmpty())
                        courseName = model.CourseName;
                    }

                

                    if (courseName.IsNotNull())
                    {
                        if (courseName.Contains("Directors") || courseName.Contains("Africa Directors"))
                        {
                            if (model.CVDocumentUploadKey.IsNullOrEmpty())
                            {
                                ModelState.AddModelError("CVDocNotUploaded", "Copy of CV document not uploaded");
                            }
                        }
                    }

                    if (model.ApplicantStatus == ApplicantStatus.Qualified)
                    {
                        if (model.AcceptanceDocumentUploadKey.IsNullOrEmpty())
                        {
                            ModelState.AddModelError("AcceptanceDocument",
                                "Copy of Acceptance document is a required field");
                        }
                    }
                }

                MapFormRequest(model);

                if (ModelState.IsValid && model.AllSectionsValidated()
                    && model.ValidationSection == ValidateProgress.ApplicationForm.AsString())
                {
                    try
                    {
                        result = controller.CreateOrUpdateContact(ValidateProgress.ApplicationForm, model);

                        if (result.Successful)
                        {
                            return RedirectPermanent("ApplicationSubmitted/Index");
                        }

                        TempData["Error"] = result.Message;

                        return Redirect("/Error/Index");
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                }

                ValidateProgress validateProgress;
                if (Enum.TryParse(validationSection, out validateProgress))
                {
                    IgnoreValidation(model, validateProgress);

                    if (!ModelState.IsValid)
                    {
                        model.SelectedTab = "failed";

                        if (validateProgress.AsString() == ValidateProgress.PersonalInformation.AsString() ||
                            validateProgress.AsString() == ValidateProgress.EmailAddress.AsString())
                        {
                            model.ValidationSection = string.Empty;
                            model.Section1Validated = false;
                        }

                        if (validateProgress.AsString() == ValidateProgress.ContactInformation.AsString())
                        {
                            model.Section2Validated = false;
                        }

                        if (validateProgress.AsString() == ValidateProgress.Qualifications.AsString())
                        {
                            model.Section3Validated = false;
                        }

                        if (validateProgress.AsString() == ValidateProgress.Marketing.AsString())
                        {
                            model.Section4Validated = false;
                        }

                        if (validateProgress.AsString() == ValidateProgress.PaymentInfo.AsString())
                        {
                            model.Section5Validated = false;
                        }
                        if (validateProgress.AsString() == ValidateProgress.ApplicationForm.AsString())
                        {
                            model.ValidationSection = ValidateProgress.PaymentInfo.AsString();
                            model.Section5Validated = false;
                        }
                    }
                    else
                    {
                        if (validateProgress.AsString() == ValidateProgress.EmailAddress.AsString())
                        {
                            model.EmailSectionValidated = true;
                        }
                        if (validateProgress.AsString() == ValidateProgress.PersonalInformation.AsString())
                        {
                            model.ApplicationFormsBaseUrl = GetBaseUrl(HttpContext.Request);

                            result = controller.CreateOrUpdateContact(ValidateProgress.PersonalInformation, model);

                            if (result.Successful)
                            {
                                model.HasUploadedRequiredDocuments =
                                    controller.HasUploadedRequiredDocuments(result.EnrolmentId.GetValueOrDefault());
                                model.HasUploadedAcceptanceLetter =
                                    controller.HasUploadedAcceptanceForm(result.EnrolmentId.GetValueOrDefault());
                                model.HasCV =
                                    controller.HasUploadedCVForm(result.EnrolmentId.GetValueOrDefault());

                                Session["ShortCourseOffering"] = model.ShortCourseOfferingId;
                                Session["Enrollment"] = result.EnrolmentId;
                                Session["UsNumber"] = model.UsNumber;
                                model.Section1Validated = true;
                                model.DisplayNameApproved = true;
                                model.Enrolment = result.EnrolmentId;
                            }
                            else
                            {
                                TempData["Error"] = result.Message;
                                return Redirect("/Error/Index");
                            }
                        }

                        if (validateProgress.AsString() == ValidateProgress.ContactInformation.AsString())
                        {
                            result = controller.CreateOrUpdateContact(ValidateProgress.ContactInformation, model);

                            if (result.Successful)
                            {
                                Session["Enrollment"] = result.EnrolmentId;
                                Session["UsNumber"] = model.UsNumber;
                                model.Section2Validated = true;
                                model.DisplayNameApproved = true;
                                model.Enrolment = result.EnrolmentId;
                            }
                            else
                            {
                                TempData["Error"] = result.Message;
                                return Redirect("/Error/Index");
                            }
                        }

                        if (validateProgress.AsString() == ValidateProgress.Qualifications.AsString())
                        {
                            result = controller.CreateOrUpdateContact(ValidateProgress.Qualifications, model);

                            if (result.Successful)
                            {
                                if (model.AccountType == AccountType.MasterStart ||
                                    model.AccountType == AccountType.BasilRead)
                                {
                                    return RedirectPermanent("ApplicationSubmitted/Index");
                                }

                                Session["Enrollment"] = result.EnrolmentId;
                                Session["UsNumber"] = model.UsNumber;
                                model.Section3Validated = true;
                                model.DisplayNameApproved = true;
                                model.Enrolment = result.EnrolmentId;
                            }
                            else
                            {
                                TempData["Error"] = result.Message;
                                return Redirect("/Error/Index");
                            }
                        }

                        if (validateProgress.AsString() == ValidateProgress.Marketing.AsString())
                        {
                            result = controller.CreateOrUpdateContact(ValidateProgress.Marketing, model);

                            if (result.Successful)
                            {
                                if (model.ShortCourseOfferingType == ShortCourseOfferingType.TEL ||
                                    model.ShortCourseOfferingType == ShortCourseOfferingType.Partnership)
                                {
                                    return RedirectPermanent("ApplicationSubmitted/Index");
                                }

                                Session["Enrollment"] = result.EnrolmentId;
                                Session["UsNumber"] = model.UsNumber;
                                model.Section4Validated = true;
                                model.DisplayNameApproved = true;
                                model.Enrolment = result.EnrolmentId;
                            }
                            else
                            {
                                TempData["Error"] = result.Message;
                                return Redirect("/Error/Index");
                            }
                        }

                        if (validateProgress.AsString() == ValidateProgress.PaymentInfo.AsString() ||
                            validateProgress.AsString() == ValidateProgress.ApplicationForm.AsString())
                        {
                            result = controller.CreateOrUpdateContact(validateProgress, model);

                            if (result.Successful)
                            {
                                Session["Enrollment"] = result.EnrolmentId;
                                Session["UsNumber"] = model.UsNumber;
                                
                                Session["ShortCourseOffering"] = model.ShortCourseOfferingId;
                                model.Section5Validated = true;
                                model.DisplayNameApproved = true;
                                model.Enrolment = result.EnrolmentId;

                                var courseName = ShortCourseApplicationFormsApiController.LoadShortCourseOfferingNameByID(new Guid(model.ShortCourseOfferingId));
                                if (Request.Form["paymentResponsibilityType"].IsNotNullOrEmpty() &&
                                    Request.Form["paymentResponsibilityType"] == "Self")
                                {
                                    try
                                    {
                                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                                        using (var wb = new WebClient())
                                        {
                                            var data = new NameValueCollection
                                            {
                                                ["p1"] = ConfigurationManager.AppSettings["vcsAccountId"],
                                                ["p2"] = result.USNumber ?? "",
                                                ["p3"] = courseName,
                                                ["p4"] = result.OfferingAmount.GetValueOrDefault().ToString(),
                                                ["UrlsProvided"] = "Y",
                                                ["ApprovedUrl"] = ConfigurationManager.AppSettings["approvedUrl"],
                                                ["DeclinedUrl"] = ConfigurationManager.AppSettings["declinedUrl"]
                                            };

                                            var response = wb.UploadValues(
                                                ConfigurationManager.AppSettings["vcsPayUrl"],
                                                "POST", data);

                                            return new FileContentResult(response, "text/html");
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        TempData["Error"] = ex.Message;
                                        return Redirect("/Error/Index");
                                    }
                                }

                                return RedirectPermanent("ApplicationSubmitted/Index");
                            }

                            TempData["Error"] = result.Message;
                            return Redirect("/Error/Index");
                        }
                    }
                }

                _applicationFormInfo = new ApplicationFormInfo();
                _applicationFormInfo = _applicationFormInfo.LoadInfoMembers(model, model.ShortCourseId,
                    model.ShortCourseOfferingId);

                ResolveViewBag(model);

                return View(model);
            }
            catch (Exception ex)
            {
                TempData["Error"] = ex.Message;

                return Redirect("/Error/Index");
            }
        }

        [HttpPost]
        public ContentResult UploadFile(ApplicationForm model, string type)
        {
            var controller = new ShortCourseApplicationFormsApiController();
            var result = new ApplicationSubmissionResult();

            model.Enrolment = Session["Enrollment"].IsNotNull()
                ? Guid.Parse(Session["Enrollment"].ToString())
                : Guid.Empty;

            model.UsNumber = Session["UsNumber"].IsNotNull() ? Session["UsNumber"].ToString() : "";
            
            model.RequiredDocuments = controller.GetRequiredDocuments(model.Offering);

            if (!model.RequiredDocuments.Any())
            {
                var shortCourseId = controller.LoadShortCourseBy(model.Offering);

                model.RequiredDocuments = controller.GetRequiredDocuments(shortCourseId);
            }

            if (model.ApplicantStatus == ApplicantStatus.Qualified)
            {
                var acceptanceDocument = controller.GetRequiredDocuments(model.Offering, 3);
                model.RequiredDocuments.Add(acceptanceDocument.FirstOrDefault());
            }

            var requiredDocument = model.RequiredDocuments.Any()
                ? model.RequiredDocuments.FirstOrDefault(x => x.Description.ToLowerInvariant().Contains(type))
                : null;

            if (requiredDocument.IsNull())
            {
                var responseObjectError = new
                {
                    result = "ERROR",
                    message = "No required document found for type: " + type
                };

                var responseError = JsonConvert.SerializeObject(responseObjectError);

                return Content(responseError.AsString(), "application/json");
            }

            foreach (string file in Request.Files)
            {
                var uploadKey = string.Empty;

                switch (type)
                {
                    case "id":
                        model.IdentificationDocumentUploadKey = Guid.NewGuid().ToString();
                        uploadKey = model.IdentificationDocumentUploadKey;
                        Session["IdentificationDocumentUploadKey"] = model.IdentificationDocumentUploadKey;
                        break;
                    case "matric":
                        model.QualificationDocumentUploadKey = Guid.NewGuid().ToString();
                        uploadKey = model.QualificationDocumentUploadKey;
                        Session["QualificationDocumentUploadKey"] = model.QualificationDocumentUploadKey;
                        break;
                    case "acceptance":
                        model.AcceptanceDocumentUploadKey = Guid.NewGuid().ToString();
                        uploadKey = model.AcceptanceDocumentUploadKey;
                        model.HasUploadedAcceptanceLetter = true;
                        Session["AcceptanceDocumentUploadKey"] = model.AcceptanceDocumentUploadKey;
                        break;
                    case "cv":
                        model.CVDocumentUploadKey = Guid.NewGuid().ToString();
                        uploadKey = model.CVDocumentUploadKey;
                        model.HasCV = true;
                        Session["CVDocumentUploadKey"] = model.CVDocumentUploadKey;
                        break;
                }

                HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;

                if (hpf.ContentLength == 0)
                    continue;

                requiredDocument.FileName = hpf.FileName;
                byte[] documentBytes = hpf.InputStream.ToByteArray();

                result = controller.SubmitDocumentation(model.Enrolment.GetValueOrDefault(), model.UsNumber,
                    requiredDocument.FileName, requiredDocument.Level, requiredDocument.Description,
                    requiredDocument.DocumentId, documentBytes);

                if (result.IsNotNull())
                {
                    model.HasUploadedRequiredDocuments = controller.HasUploadedRequiredDocuments(model.Enrolment.GetValueOrDefault());
                }
            }

            var responseObject = new
            {
                result = result.Message,
                idDocumentUploadKey = model.IdentificationDocumentUploadKey,
                qualificationDocumentUploadKey = model.QualificationDocumentUploadKey,
                acceptanceDocumentUploadKey = model.AcceptanceDocumentUploadKey,
                cvDocumentUploadKey = model.CVDocumentUploadKey
            };

            var response = JsonConvert.SerializeObject(responseObject);

            return Content(response.AsString(), "application/json");
        }

       
        [HttpPost]
        public ContentResult UploadCompanyFile(ApplicationForm model, string type)
        {
            var controller = new ShortCourseApplicationFormsApiController();
            var result = new ApplicationSubmissionResult();

            model.Enrolment = Session["Enrollment"].IsNotNull()
                ? Guid.Parse(Session["Enrollment"].ToString())
                : Guid.Empty;

            model.UsNumber = Session["UsNumber"].IsNotNull() ? Session["UsNumber"].ToString() : "";

            foreach (string file in Request.Files)
            {
                HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;

                if (hpf.ContentLength == 0)
                    continue;

                var fileName = hpf.FileName;
                byte[] documentBytes = hpf.InputStream.ToByteArray();

                result = controller.SubmitDocumentation(model.Enrolment.GetValueOrDefault(), model.UsNumber, fileName, 1,
                    "Company Purchase Order Document", 0, documentBytes);

                if (result.IsNotNull())
                {
                    model.HasUploadedRequiredDocuments = controller.HasUploadedRequiredDocuments(model.Enrolment.GetValueOrDefault());
                }
            }

            var responseObject = new
            {
                result = result.Message,
                idDocumentUploadKey = model.IdentificationDocumentUploadKey,
                qualificationDocumentUploadKey = model.QualificationDocumentUploadKey,
                acceptanceDocumentUploadKey = model.AcceptanceDocumentUploadKey
            };

            var response = JsonConvert.SerializeObject(responseObject);

            return Content(response.AsString(), "application/json");
        }

        [HttpGet]
        public ContentResult UploadAcceptanceForm(string usNumber, string enrolment, string firstNames, string surname)
        {
            if (usNumber.IsNotNullOrEmpty())
            {
                var controller = new ShortCourseApplicationFormsApiController();
                var result = new ApplicationSubmissionResult();
                string acceptanceLetter =
                    HttpContext.Server.MapPath(@"~/Content/documents/TEL_Acceptance_Form.pdf");

                if (acceptanceLetter.IsNotNullOrEmpty())
                {
                    PdfReader reader = new PdfReader(acceptanceLetter);

                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (PdfStamper stamper = new PdfStamper(reader, ms, '\0', false))
                        {
                            AcroFields formFields = stamper.AcroFields;
                            formFields.SetField("Name and surname print", $"{(firstNames.IsNotNullOrEmpty() ? firstNames : "")} {(surname.IsNotNullOrEmpty() ? surname : "")}");
                            formFields.SetField("US Number  Student Number", usNumber.IsNotNullOrEmpty() ? usNumber : "");
                            formFields.SetField("Date", DateTime.Now.ToString("yyyy-MM-dd"));
                            stamper.FormFlattening = true;
                        }

                        result = controller.SubmitDocumentation(Guid.Parse(enrolment), usNumber,
                            "Acceptance Letter", 3, "Acceptance Letter",
                            0, ms.ToArray());

                        var responseObject = new
                        {
                            result = result.Message,
                            documentUrl = ms.ToArray()
                        };

                        var response = JsonConvert.SerializeObject(responseObject);
                        
                        Session["AcceptanceDocumentUploadKey"] = Guid.NewGuid().ToString(); ;

                        //return File(ms.ToArray(), MediaTypeNames.Application.Octet, "Payment_Notification.pdf");
                        return Content(response.AsString(), "application/json");
                    }
                }
            }

            return null;
        }

        public JsonResult GetDistributionAreas()
        {
            var query = HttpContext.Request["query"];

            var postalDistributionArea = new PostalDistributionArea();

            var areas = postalDistributionArea.LoadAreas(query);
            //Session["PostalAreas"] = areas;
            return Json(postalDistributionArea.Areas, JsonRequestBehavior.AllowGet);
        }

        private ActionResult NotFound()
        {
            return View("NotFound");
        }

        #region Private Methods

        private void MapFormRequest(ApplicationForm model)
        {
            var selectedTab = string.Empty;
            var ethnicityNonSA = string.Empty;

            if (Request.IsNotNull())
            {
                selectedTab = Request.Form["SelectedTab"];
                ethnicityNonSA = Request.Form["EthnicityNonSA"];

                model.DateOfBirthDay = Request.Form["DateOfBirthDay"];
                model.DateOfBirthMonth = Request.Form["DateOfBirthMonth"];
                model.DateOfBirthYear = Request.Form["DateOfBirthYear"];
            }

            if (ethnicityNonSA.IsNotNullOrEmpty())
            {
                Guid result = new Guid();
                Guid.TryParse(ethnicityNonSA, out result);

                model.Ethnicity = result;
            }

            model.SelectedTab = selectedTab;
        }

        private void IgnoreValidation(ApplicationForm model, ValidateProgress validateProgress)
        {
            if (ModelState["studentSearch"].IsNotNull())
            {
                ModelState["studentSearch"].Errors.Clear();
            }

            switch (validateProgress)
            {
                case ValidateProgress.EmailAddress:
                    if (ModelState["Email"].IsNotNull())
                    {
                        ModelState["Email"].Errors.Clear();
                    }
                    
                    if (ModelState["Surname"].IsNotNull())
                    {
                        ModelState["Surname"].Errors.Clear();
                    }

                    if (ModelState["FirstNames"].IsNotNull())
                    {
                        ModelState["FirstNames"].Errors.Clear();
                    }
                    if (ModelState["NameCalledBy"].IsNotNull())
                    {
                        ModelState["NameCalledBy"].Errors.Clear();
                    }

                    if (ModelState["Initials"].IsNotNull())
                    {
                        ModelState["Initials"].Errors.Clear();
                    }

                    if (ModelState["Title"].IsNotNull())
                    {
                        ModelState["Title"].Errors.Clear();
                    }

                    if (ModelState["Gender"].IsNotNull())
                    {
                        ModelState["Gender"].Errors.Clear();
                    }

                    if (ModelState["DateOfBirth"].IsNotNull())
                    {
                        ModelState["DateOfBirth"].Errors.Clear();
                    }

                    if (ModelState["Nationality"].IsNotNull())
                    {
                        ModelState["Nationality"].Errors.Clear();
                    }

                    if (ModelState["Language"].IsNotNull())
                    {
                        ModelState["Language"].Errors.Clear();
                    }

                    if (ModelState["ContactNumber"].IsNotNull())
                    {
                        ModelState["ContactNumber"].Errors.Clear();
                    }

                    if (ModelState["PostalCountry"].IsNotNull())
                    {
                        ModelState["PostalCountry"].Errors.Clear();
                    }

                    if (ModelState["Province"].IsNotNull())
                    {
                        ModelState["Province"].Errors.Clear();
                    }

                    if (ModelState["QualificationType"].IsNotNull())
                    {
                        ModelState["QualificationType"].Errors.Clear();
                    }

                    if (ModelState["PaymentResponsibility"].IsNotNull())
                    {
                        ModelState["PaymentResponsibility"].Errors.Clear();
                    }

                    if (ModelState["MarketingSource"].IsNotNull())
                    {
                        ModelState["MarketingSource"].Errors.Clear();
                    }

                    if (ModelState["MarketingReason"].IsNotNull())
                    {
                        ModelState["MarketingReason"].Errors.Clear();
                    }

                    if (ModelState["DietaryRequirements"].IsNotNull())
                    {
                        ModelState["DietaryRequirements"].Errors.Clear();
                    }

                    if (ModelState["DietaryRequirementsOther"].IsNotNull())
                    {
                        ModelState["DietaryRequirementsOther"].Errors.Clear();
                    }

                    if (ModelState["IDDocument"].IsNotNull())
                    {
                        ModelState["IDDocument"].Errors.Clear();
                    }

                    if (ModelState["QualificationDocument"].IsNotNull())
                    {
                        ModelState["QualificationDocument"].Errors.Clear();
                    }

                    if (ModelState["AcceptanceDocument"].IsNotNull())
                    {
                        ModelState["AcceptanceDocument"].Errors.Clear();
                    }

                    if (ModelState["Disability"].IsNotNull())
                    {
                        ModelState["Disability"].Errors.Clear();
                    }

                    if (ModelState["PaymentPostalAfrigis"].IsNotNull())
                    {
                        ModelState["PaymentPostalAfrigis"].Errors.Clear();
                    }
                    if (ModelState["PaymentPostalSuburb"].IsNotNull())
                    {
                        ModelState["PaymentPostalSuburb"].Errors.Clear();
                    }
                    if (ModelState["PaymentPostalCity"].IsNotNull())
                    {
                        ModelState["PaymentPostalCity"].Errors.Clear();
                    }
                    if (ModelState["PaymentPostalCountry"].IsNotNull())
                    {
                        ModelState["PaymentPostalCountry"].Errors.Clear();
                    }
                    if (ModelState["PaymentProvince"].IsNotNull())
                    {
                        ModelState["PaymentProvince"].Errors.Clear();
                    }

                    if (ModelState["PostalAfrigis"].IsNotNull())
                    {
                        ModelState["PostalAfrigis"].Errors.Clear();
                    }
                    ModelState["Consent"].Errors.Clear();
                    break;
                case ValidateProgress.PersonalInformation:

                    if (model.Offering.IsNotNull() && model.Offering == Guid.Empty)
                    {
                        ModelState.AddModelError("Offering", "Please select a short course.");
                    }
                    else
                    {
                        if (ModelState["Offering"].IsNotNull())
                        {
                            ModelState["Offering"].Errors.Clear();
                        }
                    }

                    if (ModelState["RegisteredEmail"].IsNotNull())
                    {
                        ModelState["RegisteredEmail"].Errors.Clear();
                    }

                    if (ModelState["Ethnicity"].IsNotNull())
                    {
                        ModelState["Ethnicity"].Errors.Clear();
                    }
                   
                    ModelState["ContactNumber"].Errors.Clear();

                    ModelState["PostalStreet1"].Errors.Clear();
                    ModelState["PostalSuburb"].Errors.Clear();
                    ModelState["PostalCity"].Errors.Clear();
                    ModelState["PostalPostalCode"].Errors.Clear();

                    if (ModelState["PostalCountry"].IsNotNull())
                    {
                        ModelState["PostalCountry"].Errors.Clear();
                    }

                    if (ModelState["Province"].IsNotNull())
                    {
                        ModelState["Province"].Errors.Clear();
                    }

                    if (ModelState["PostalAfrigis"].IsNotNull())
                    {
                        ModelState["PostalAfrigis"].Errors.Clear();
                    }

                    if (ModelState["MarketingReason"].IsNotNull())
                    {
                        ModelState["MarketingReason"].Errors.Clear();
                    }

                    if (ModelState["MarketingSource"].IsNotNull())
                    {
                        ModelState["MarketingSource"].Errors.Clear();
                    }
                   
                    if (ModelState["QualificationType"].IsNotNull())
                    {
                        ModelState["QualificationType"].Errors.Clear();
                    }

                    ModelState["PaymentResponsibility"].Errors.Clear();
                    ModelState["PaymentContactName"].Errors.Clear();
                    ModelState["PaymentContactSurname"].Errors.Clear();
                    ModelState["PaymentContactNumber"].Errors.Clear();
                    ModelState["PaymentContactEmail"].Errors.Clear();
                    ModelState["Consent"].Errors.Clear();
                    // ModelState["PaymentDebtorCode"].Errors.Clear();

                    if (ModelState["IDDocument"].IsNotNull())
                    {
                        ModelState["IDDocument"].Errors.Clear();
                    }

                    if (ModelState["QualificationDocument"].IsNotNull())
                    {
                        ModelState["QualificationDocument"].Errors.Clear();
                    }

                    if (ModelState["AcceptanceDocument"].IsNotNull())
                    {
                        ModelState["AcceptanceDocument"].Errors.Clear();
                    }

                    if (ModelState["QualificationDocNotUploaded"].IsNotNull())
                    {
                        ModelState["QualificationDocNotUploaded"].Errors.Clear();
                    }

                    if (model.AccountType == AccountType.MasterStart)
                    {
                        if (ModelState["DietaryRequirements"].IsNotNull())
                        {
                            ModelState["DietaryRequirements"].Errors.Clear();
                        }

                        if (ModelState["DietaryRequirementsOther"].IsNotNull())
                        {
                            ModelState["DietaryRequirementsOther"].Errors.Clear();
                        }
                    }

                    if (ModelState["PaymentPostalAfrigis"].IsNotNull())
                    {
                        ModelState["PaymentPostalAfrigis"].Errors.Clear();
                    }
                    if (ModelState["PaymentContactCompanyName"].IsNotNull())
                    {
                        ModelState["PaymentContactCompanyName"].Errors.Clear();
                    }
                    if (ModelState["PaymentPostalSuburb"].IsNotNull())
                    {
                        ModelState["PaymentPostalSuburb"].Errors.Clear();
                    }
                    if (ModelState["PaymentPostalCity"].IsNotNull())
                    {
                        ModelState["PaymentPostalCity"].Errors.Clear();
                    }
                    if (ModelState["PaymentPostalCountry"].IsNotNull())
                    {
                        ModelState["PaymentPostalCountry"].Errors.Clear();
                    }
                    if (ModelState["PaymentProvince"].IsNotNull())
                    {
                        ModelState["PaymentProvince"].Errors.Clear();
                    }
                    break;
                case ValidateProgress.ContactInformation:

                    if (ModelState["RegisteredEmail"].IsNotNull())
                    {
                        ModelState["RegisteredEmail"].Errors.Clear();
                    }
                    ModelState["Consent"].Errors.Clear();
                    ModelState["MarketingReason"].Errors.Clear();
                    ModelState["MarketingSource"].Errors.Clear();
                    ModelState["QualificationType"].Errors.Clear();

                    ModelState["PaymentResponsibility"].Errors.Clear();
                    ModelState["PaymentContactName"].Errors.Clear();
                    ModelState["PaymentContactSurname"].Errors.Clear();
                    ModelState["PaymentContactNumber"].Errors.Clear();
                    ModelState["PaymentContactEmail"].Errors.Clear();
                  //  ModelState["PaymentDebtorCode"].Errors.Clear();

                    if (ModelState["IDDocument"].IsNotNull())
                    {
                        ModelState["IDDocument"].Errors.Clear();
                    }
                    if (ModelState["Ethnicity"].IsNotNull())
                    {
                        ModelState["Ethnicity"].Errors.Clear();
                    }
                    if (ModelState["QualificationDocument"].IsNotNull())
                    {
                        ModelState["QualificationDocument"].Errors.Clear();
                    }

                    if (ModelState["AcceptanceDocument"].IsNotNull())
                    {
                        ModelState["AcceptanceDocument"].Errors.Clear();
                    }

                    if (ModelState["QualificationDocNotUploaded"].IsNotNull())
                    {
                        ModelState["QualificationDocNotUploaded"].Errors.Clear();
                    }

                    if (ModelState["DietaryRequirements"].IsNotNull())
                    {
                        ModelState["DietaryRequirements"].Errors.Clear();
                    }

                    if (ModelState["DietaryRequirementsOther"].IsNotNull())
                    {
                        ModelState["DietaryRequirementsOther"].Errors.Clear();
                    }
                    if (ModelState["Disability"].IsNotNull())
                    {
                        ModelState["Disability"].Errors.Clear();
                    }

                    if (model.SelectedCountry != "South Africa")
                    {
                        if (ModelState["Province"].IsNotNull())
                        {
                            ModelState["Province"].Errors.Clear();
                        }
                    }

                    if (ModelState["PaymentPostalAfrigis"].IsNotNull())
                    {
                        ModelState["PaymentPostalAfrigis"].Errors.Clear();
                    }
                    if (ModelState["PaymentContactCompanyName"].IsNotNull())
                    {
                        ModelState["PaymentContactCompanyName"].Errors.Clear();
                    }
                    if (ModelState["PaymentPostalSuburb"].IsNotNull())
                    {
                        ModelState["PaymentPostalSuburb"].Errors.Clear();
                    }
                    if (ModelState["PaymentPostalCity"].IsNotNull())
                    {
                        ModelState["PaymentPostalCity"].Errors.Clear();
                    }
                    if (ModelState["PaymentPostalCountry"].IsNotNull())
                    {
                        ModelState["PaymentPostalCountry"].Errors.Clear();
                    }
                    if (ModelState["PaymentProvince"].IsNotNull())
                    {
                        ModelState["PaymentProvince"].Errors.Clear();
                    }
                    break;
                case ValidateProgress.Qualifications:

                    if (ModelState["RegisteredEmail"].IsNotNull())
                    {
                        ModelState["RegisteredEmail"].Errors.Clear();
                    }

                    ModelState["Email"].Errors.Clear();
                    ModelState["Consent"].Errors.Clear();
                    ModelState["ContactNumber"].Errors.Clear();

                    ModelState["PostalStreet1"].Errors.Clear();
                    ModelState["PostalSuburb"].Errors.Clear();
                    ModelState["PostalCity"].Errors.Clear();
                    ModelState["PostalPostalCode"].Errors.Clear();

                    if (ModelState["PostalCountry"].IsNotNull())
                    {
                        ModelState["PostalCountry"].Errors.Clear();
                    }

                    if (ModelState["Province"].IsNotNull())
                    {
                        ModelState["Province"].Errors.Clear();
                    }

                    if (ModelState["PostalAfrigis"].IsNotNull())
                    {
                        ModelState["PostalAfrigis"].Errors.Clear();
                    }
                   
                    if (ModelState["Ethnicity"].IsNotNull())
                    {
                        ModelState["Ethnicity"].Errors.Clear();
                    }
                    ModelState["MarketingReason"].Errors.Clear();
                    ModelState["MarketingSource"].Errors.Clear();
                    ModelState["PaymentResponsibility"].Errors.Clear();
                    ModelState["PaymentContactName"].Errors.Clear();
                    ModelState["PaymentContactSurname"].Errors.Clear();

                    if (ModelState["DietaryRequirements"].IsNotNull())
                    {
                        ModelState["DietaryRequirements"].Errors.Clear();
                    }

                    if (ModelState["DietaryRequirementsOther"].IsNotNull())
                    {
                        ModelState["DietaryRequirementsOther"].Errors.Clear();
                    }
                    if (ModelState["Disability"].IsNotNull())
                    {
                        ModelState["Disability"].Errors.Clear();
                    }

                    if (ModelState["Nationality"].IsNotNull())
                    {
                        ModelState["Nationality"].Errors.Clear();
                    }

                    if (model.HasUploadedRequiredDocuments)
                    {
                        if (ModelState["IDDocument"].IsNotNull())
                        {
                            ModelState["IDDocument"].Errors.Clear();
                        }

                        if (ModelState["IdDocNotUploaded"].IsNotNull())
                        {
                            ModelState["IdDocNotUploaded"].Errors.Clear();
                        }

                        if (ModelState["QualificationDocument"].IsNotNull())
                        {
                            ModelState["QualificationDocument"].Errors.Clear();
                        }

                        if (ModelState["QualificationDocNotUploaded"].IsNotNull())
                        {
                            ModelState["QualificationDocNotUploaded"].Errors.Clear();
                        }
                    }

                    if (model.HasUploadedAcceptanceLetter)
                    {
                        if (ModelState["AcceptanceDocument"].IsNotNull())
                        {
                            ModelState["AcceptanceDocument"].Errors.Clear();
                        }
                    }

                    if (ModelState["PaymentPostalAfrigis"].IsNotNull())
                    {
                        ModelState["PaymentPostalAfrigis"].Errors.Clear();
                    }
                    if (ModelState["PaymentContactCompanyName"].IsNotNull())
                    {
                        ModelState["PaymentContactCompanyName"].Errors.Clear();
                    }
                    if (ModelState["PaymentPostalSuburb"].IsNotNull())
                    {
                        ModelState["PaymentPostalSuburb"].Errors.Clear();
                    }
                    if (ModelState["PaymentPostalCity"].IsNotNull())
                    {
                        ModelState["PaymentPostalCity"].Errors.Clear();
                    }
                    if (ModelState["PaymentPostalCountry"].IsNotNull())
                    {
                        ModelState["PaymentPostalCountry"].Errors.Clear();
                    }
                    if (ModelState["PaymentProvince"].IsNotNull())
                    {
                        ModelState["PaymentProvince"].Errors.Clear();
                    }
                    break;
                case ValidateProgress.Marketing:


                    if (ModelState["RegisteredEmail"].IsNotNull())
                    {
                        ModelState["RegisteredEmail"].Errors.Clear();
                    }
                    ModelState["Ethnicity"].Errors.Clear();

                    ModelState["Email"].Errors.Clear();
                    ModelState["ContactNumber"].Errors.Clear();
                    ModelState["Consent"].Errors.Clear();
                    ModelState["PostalStreet1"].Errors.Clear();
                    ModelState["PostalSuburb"].Errors.Clear();
                    ModelState["PostalCity"].Errors.Clear();
                    ModelState["PostalPostalCode"].Errors.Clear();

                    if (ModelState["PostalCountry"].IsNotNull())
                    {
                        ModelState["PostalCountry"].Errors.Clear();
                    }

                    if (ModelState["Province"].IsNotNull())
                    {
                        ModelState["Province"].Errors.Clear();
                    }

                    if (ModelState["PostalAfrigis"].IsNotNull())
                    {
                        ModelState["PostalAfrigis"].Errors.Clear();
                    }

                    ModelState["QualificationType"].Errors.Clear();

                    if (ModelState["IDDocument"].IsNotNull())
                    {
                        ModelState["IDDocument"].Errors.Clear();
                    }

                    if (ModelState["QualificationDocument"].IsNotNull())
                    {
                        ModelState["QualificationDocument"].Errors.Clear();
                    }

                    if (ModelState["AcceptanceDocument"].IsNotNull())
                    {
                        ModelState["AcceptanceDocument"].Errors.Clear();
                    }

                    if (ModelState["QualificationDocNotUploaded"].IsNotNull())
                    {
                        ModelState["QualificationDocNotUploaded"].Errors.Clear();
                    }

                    ModelState["PaymentResponsibility"].Errors.Clear();
                    ModelState["PaymentContactName"].Errors.Clear();
                    ModelState["PaymentContactSurname"].Errors.Clear();

                    if (ModelState["Disability"].IsNotNull())
                    {
                        ModelState["Disability"].Errors.Clear();
                    }
                    if (ModelState["DietaryRequirements"].IsNotNull())
                    {
                        ModelState["DietaryRequirements"].Errors.Clear();
                    }

                    if (ModelState["DietaryRequirementsOther"].IsNotNull())
                    {
                        ModelState["DietaryRequirementsOther"].Errors.Clear();
                    }

                    if (ModelState["Nationality"].IsNotNull())
                    {
                        ModelState["Nationality"].Errors.Clear();
                    }

                    if (ModelState["PaymentPostalAfrigis"].IsNotNull())
                    {
                        ModelState["PaymentPostalAfrigis"].Errors.Clear();
                    }
                    if (ModelState["PaymentContactCompanyName"].IsNotNull())
                    {
                        ModelState["PaymentContactCompanyName"].Errors.Clear();
                    }
                    if (ModelState["PaymentPostalSuburb"].IsNotNull())
                    {
                        ModelState["PaymentPostalSuburb"].Errors.Clear();
                    }
                    if (ModelState["PaymentPostalCity"].IsNotNull())
                    {
                        ModelState["PaymentPostalCity"].Errors.Clear();
                    }
                    if (ModelState["PaymentPostalCountry"].IsNotNull())
                    {
                        ModelState["PaymentPostalCountry"].Errors.Clear();
                    }
                    if (ModelState["PaymentProvince"].IsNotNull())
                    {
                        ModelState["PaymentProvince"].Errors.Clear();
                    }
                    break;
                case ValidateProgress.PaymentInfo:
                  
                    if (ModelState["RegisteredEmail"].IsNotNull())
                    {
                        ModelState["RegisteredEmail"].Errors.Clear();
                    }

                    ModelState["Email"].Errors.Clear();
                    ModelState["ContactNumber"].Errors.Clear();

                    ModelState["PostalStreet1"].Errors.Clear();
                    ModelState["PostalSuburb"].Errors.Clear();
                    ModelState["PostalCity"].Errors.Clear();
                    ModelState["PostalPostalCode"].Errors.Clear();

                    if (ModelState["PostalCountry"].IsNotNull())
                    {
                        ModelState["PostalCountry"].Errors.Clear();
                    }

                    if (ModelState["Province"].IsNotNull())
                    {
                        ModelState["Province"].Errors.Clear();
                    }

                    if (ModelState["PostalAfrigis"].IsNotNull())
                    {
                        ModelState["PostalAfrigis"].Errors.Clear();
                    }

                    ModelState["QualificationType"].Errors.Clear();

                    if (ModelState["IDDocument"].IsNotNull())
                    {
                        ModelState["IDDocument"].Errors.Clear();
                    }

                    if (ModelState["QualificationDocument"].IsNotNull())
                    {
                        ModelState["QualificationDocument"].Errors.Clear();
                    }

                    if (ModelState["AcceptanceDocument"].IsNotNull())
                    {
                        ModelState["AcceptanceDocument"].Errors.Clear();
                    }

                    if (ModelState["QualificationDocNotUploaded"].IsNotNull())
                    {
                        ModelState["QualificationDocNotUploaded"].Errors.Clear();
                    }

                    ModelState["MarketingReason"].Errors.Clear();
                    ModelState["MarketingSource"].Errors.Clear();

                    if (ModelState["Disability"].IsNotNull())
                    {
                        ModelState["Disability"].Errors.Clear();
                    }
                    if (ModelState["DietaryRequirements"].IsNotNull())
                    {
                        ModelState["DietaryRequirements"].Errors.Clear();
                    }

                    if (ModelState["DietaryRequirementsOther"].IsNotNull())
                    {
                        ModelState["DietaryRequirementsOther"].Errors.Clear();
                    }

                    if (ModelState["Nationality"].IsNotNull())
                    {
                        ModelState["Nationality"].Errors.Clear();
                    }

                    if (model.IsSelf)
                    {

                        ModelState["PaymentPostalAfrigis"].Errors.Clear();


                        ModelState["PaymentContactCompanyName"].Errors.Clear();


                        ModelState["PaymentPostalCity"].Errors.Clear();

                        ModelState["PaymentPostalCountry"].Errors.Clear();

                        ModelState["PaymentProvince"].Errors.Clear();

                        ModelState["PaymentPostalCode"].Errors.Clear();

                    }
                    else if (model.IsCompanyResponsibility)
                    {
                        if (ModelState["PaymentPostalAfrigis"].IsNotNull())
                        {
                            ModelState["PaymentPostalAfrigis"].Errors.Clear();
                        }
                        if (ModelState["PaymentContactCompanyName"].IsNotNull())
                        {
                            ModelState["PaymentContactCompanyName"].Errors.Clear();
                        }
                        if (ModelState["PaymentPostalSuburb"].IsNotNull())
                        {
                            ModelState["PaymentPostalSuburb"].Errors.Clear();
                        }
                        if (ModelState["PaymentPostalCity"].IsNotNull())
                        {
                            ModelState["PaymentPostalCity"].Errors.Clear();
                        }
                        if (ModelState["PaymentPostalCountry"].IsNotNull())
                        {
                            ModelState["PaymentPostalCountry"].Errors.Clear();
                        }
                        if (ModelState["PaymentProvince"].IsNotNull())
                        {
                            ModelState["PaymentProvince"].Errors.Clear();
                        }
                        if (ModelState["PaymentPostalCode"].IsNotNull())
                        {
                            ModelState["PaymentPostalCode"].Errors.Clear();
                        }
                        if (ModelState["Consent"].IsNotNull())
                        {
                            ModelState["Consent"].Errors.Clear();
                        }

                    }
                    break;
                case ValidateProgress.ApplicationForm:
                   

                    if (ModelState["Province"].IsNotNull())
                    {
                        ModelState["Province"].Errors.Clear();
                    }
                    ModelState["PostalStreet1"].Errors.Clear();
                    ModelState["PostalSuburb"].Errors.Clear();
                    ModelState["PostalCity"].Errors.Clear();
                    ModelState["PostalPostalCode"].Errors.Clear();
                    if (ModelState["PostalAfrigis"].IsNotNull())
                    {
                        ModelState["PostalAfrigis"].Errors.Clear();
                    }
                    if (model.IsSelf)
                    {
                        if (ModelState["PaymentPostalAfrigis"].IsNotNull())
                        {
                            ModelState["PaymentPostalAfrigis"].Errors.Clear();
                        }
                        if (ModelState["PaymentContactCompanyName"].IsNotNull())
                        {

                            ModelState["PaymentContactCompanyName"].Errors.Clear();
                        }
                        if (ModelState["PaymentPostalCity"].IsNotNull())
                        {

                            ModelState["PaymentPostalCity"].Errors.Clear();
                        }
                        if (ModelState["PaymentPostalCountry"].IsNotNull())
                        {
                            ModelState["PaymentPostalCountry"].Errors.Clear();
                        }
                        if (ModelState["PaymentPostalSuburb"].IsNotNull())
                        {
                            ModelState["PaymentPostalSuburb"].Errors.Clear();
                        }
                        if (ModelState["PaymentProvince"].IsNotNull())
                        {
                            ModelState["PaymentProvince"].Errors.Clear();
                        }
                        if (ModelState["PaymentPostalCode"].IsNotNull())
                        {
                            ModelState["PaymentPostalCode"].Errors.Clear();
                        }
                    }
                   else  if (model.IsCompanyResponsibility)
                    {
                        if (ModelState["PaymentPostalAfrigis"].IsNotNull())
                        {
                            ModelState["PaymentPostalAfrigis"].Errors.Clear();
                        }
                        if (ModelState["PaymentContactCompanyName"].IsNotNull())
                        {
                            ModelState["PaymentContactCompanyName"].Errors.Clear();
                        }
                        if (ModelState["PaymentPostalSuburb"].IsNotNull())
                        {
                            ModelState["PaymentPostalSuburb"].Errors.Clear();
                        }
                        if (ModelState["PaymentPostalCity"].IsNotNull())
                        {
                            ModelState["PaymentPostalCity"].Errors.Clear();
                        }
                        if (ModelState["PaymentPostalCountry"].IsNotNull())
                        {
                            ModelState["PaymentPostalCountry"].Errors.Clear();
                        }
                        if (ModelState["PaymentProvince"].IsNotNull())
                        {
                            ModelState["PaymentProvince"].Errors.Clear();
                        }
                        if (ModelState["PaymentPostalCode"].IsNotNull())
                        {
                            ModelState["PaymentPostalCode"].Errors.Clear();
                        }
                        if (ModelState["Consent"].Value == null)
                        {
                        }
                        else
                        {
                            ModelState["Consent"].Errors.Clear();
                        }
                    }
                    break;
            }
        }

        private void ResolveViewBag(ApplicationForm model)
        {
            ViewBag.ApplicationFormInfo = _applicationFormInfo;

            if (model.DirectOfferingLink)
            {
                ViewBag.OfferingName = model.CourseName;
                ViewBag.OfferingStartDate = model.CourseStartDate;
                ViewBag.ShortCourseOfferings = model.CourseName;
                ViewBag.CourseName = model.CourseName;
            }
            //else if(model.ApplicationClosed)
            //{
            //    ViewBag.OfferingName = model.CourseName;
            //    ViewBag.OfferingStartDate = model.CourseStartDate;
            //    ViewBag.ShortCourseOfferings = model.CourseName;
            //}
            else
            {
                if (model.InsideCRM)
                {
                    ViewBag.OfferingName = model.CourseName;
                    ViewBag.OfferingStartDate = model.CourseStartDate;
                    ViewBag.ShortCourseOfferings = model.CourseName;
                    ViewBag.CourseName = model.CourseName;
                }
                else
                {
                    ViewBag.OfferingName = _applicationFormInfo.ShortCourseOfferings.Any()
                        ? _applicationFormInfo.ShortCourseOfferings.FirstOrDefault().Text
                        : string.Empty;
                    ViewBag.OfferingStartDate = model.CourseStartDate;
                    ViewBag.ShortCourseOfferings = _applicationFormInfo.ShortCourseOfferings;
                    ViewBag.CourseName = model.CourseName;
                }
            }

            ViewBag.CopyOfID = model.CopyOfID;
            ViewBag.CopyOfCertificate = model.CopyOfCertificate;

            // Select list with selected values
            // Personal Details
            if (model.Gender != Guid.Empty)
            {
                foreach (var item in _applicationFormInfo.Genders)
                {
                    if (item.Value == model.Gender.ToString())
                    {
                        item.Selected = true;
                    }
                }
            }

            ViewBag.Genders = _applicationFormInfo.Genders;

            if (model.Nationality != Guid.Empty)
            {
                foreach (var item in _applicationFormInfo.Nationalities)
                {
                    if (item.Value == model.Nationality.ToString())
                    {
                        item.Selected = true;
                    }
                }
            }
            
            ViewBag.Nationalities = _applicationFormInfo.Nationalities;
            ViewBag.Languages = _applicationFormInfo.Languages;
            ViewBag.Titles = _applicationFormInfo.Titles;
            ViewBag.Ethnicities = _applicationFormInfo.Ethnicities;
            ViewBag.IdDocumentTypes = _applicationFormInfo.IdDocumentTypes;
            ViewBag.EthnicitiesNonSA = _applicationFormInfo.EthnicitiesNonSA;
            ViewBag.DietaryRequirementList = _applicationFormInfo.DietaryRequirements;
            ViewBag.DisabilityList = _applicationFormInfo.Disabilities;
            ViewBag.YesNo = _applicationFormInfo.YesNoList;
            //
            // Contact Information
            ViewBag.AddressCountries = _applicationFormInfo.AddressCountries;
            ViewBag.PostalProvinces = _applicationFormInfo.Provinces;
           

            ViewBag.Industries = _applicationFormInfo.Industries;
            ViewBag.WorkAreas = _applicationFormInfo.WorkAreas;

            ViewBag.PaymentResponsibility = _applicationFormInfo.PaymentResponsibility;
            ViewBag.PaymentProvince = _applicationFormInfo.Provinces;
            ViewBag.MarketingReasons = _applicationFormInfo.MarketingReasons;
            ViewBag.MarketingSources = _applicationFormInfo.MarketingSources;
            ViewBag.QualificationTypes = _applicationFormInfo.QualificationTypes;
            ViewBag.QualificationField = _applicationFormInfo.QualificationFields;
            
            ViewBag.Years = _applicationFormInfo.YearsList;
            ViewBag.DisplayNameApproved = model.DisplayNameApproved;

            var postalDistributionArea = new PostalDistributionArea();
            var controller = new ShortCourseApplicationFormsApiController();
            var areas = postalDistributionArea.LoadAreas(model.PostalPostalCode);
            var areasPayment = postalDistributionArea.LoadAreas(model.PaymentPostalCode);


            if (areas.IsNotNull() )
            {
                ViewBag.PostalAreas = areas;
            }
            if (areasPayment.IsNotNull())
            {
                ViewBag.PaymentPostalAreas = areas;
            }
        }

        private void ClearValidationFields(ApplicationForm model)
        {
            model.EmailSectionValidated = false;
            model.Section1Validated = false;
            model.Section2Validated = false;
            model.Section3Validated = false;
            model.Section4Validated = false;
            model.Section5Validated = false;
        }

        private string GetBaseUrl(HttpRequestBase request)
        {
            var appUrl = HttpRuntime.AppDomainAppVirtualPath;
            var baseUrl = $"{request.Url.Scheme}://{request.Url.Authority}{appUrl}";
            return baseUrl;
        }

        //[HttpPost]
        //public JsonResult GetShortCourseName(Guid ShortCourseId)
        //{
        //    var courseName = CrmQueries.LoadShortCourseNameByID(ShortCourseId);
        //    return Json(courseName, JsonRequestBehavior.AllowGet);

        //}


        public JsonResult GetShortCourseName()
        {
            Guid id = new Guid(HttpContext.Request["ShortCourseId"]);

            return Json(CrmQueries.LoadShortCourseNameByID(id), JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}
