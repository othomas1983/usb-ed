﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ShortCourseApplicationForms.Models
{
    public class ApplicationFormsTitles
    {
        protected static string titlesUrlBase = "http://bpccrmappdev.belpark.sun.ac.za/UsbEdApplicationFormApi/ApplicationFormApi/Titles/";

        private ApplicationFormsTitles()
        {            
        }

        private static ApplicationFormsTitles _ApplicationFormsTitles;

        public static List<SelectListItem> Titles { get; set; }

        public static ApplicationFormsTitles LoadTitles()
        {
            if (_ApplicationFormsTitles == null)
            {
                _ApplicationFormsTitles = new ApplicationFormsTitles();
                WebClient client = new WebClient();
                Titles = new List<SelectListItem>();
                Titles.Add(new SelectListItem { Text = "", Value = "" });
                try
                {
                    dynamic result = JsonConvert.DeserializeObject(client.DownloadString(titlesUrlBase));

                    foreach (dynamic item in result)
                    {
                        Titles.Add(new SelectListItem { Text = item.Name, Value = item.Value });
                    }
                }
                catch (Exception)
                {
                    //create exception message
                }
            }
            return _ApplicationFormsTitles;
        }
    }
}