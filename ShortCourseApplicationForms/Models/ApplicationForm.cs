﻿using Newtonsoft.Json;
using ShortCourseApplicationForms.Models.Rules;
//using ShortCourseApplicationForms.Models.Rules;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ShortCourseApplicationForms.Models
{
    public class ApplicationForm
    {
        public ApplicationForm()
        {
        }

        #region Properties

        #region Personal Information

        [Required]
        [Display(Name = "First Names")]
        public string FirstNames { get; set; }

        [Required]
        public string Surname { get; set; }
        [Required]
        public string NameCalledBy { get; set; }
        [Required]
        [InitialsAndFirstNamesValidationAttribute]
        public string Initials { get; set; }

        [Display(Name = "Given name")]
        public string GivenName { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string Gender { get; set; }

        [Required]
        public string Ethnicity { get; set; }

        [Required]
        public string Nationality { get; set; }

        [Required]
        [Display(Name = "Date of Birth")]
        public string DateOfBirth { get; set; }

        [Display(Name = "Type of identification document")]
        public string IdDocumentType { get; set; }

        [Display(Name = "Expiry date:")]
        public string IdExpiryDate { get; set; }

        [IdNumberValidation]
        [Display(Name = "Identity Number")]
        public string IdNumber { get; set; }

        [HomeLanguageValidation]
        [Display(Name = "Home Language")]
        public string HomeLanguage { get; set; }

        [HomeLanguageValidation]
        public string HomeLanguageOther { get; set; }

        [Required]
        [Display(Name = "Contact Number")]
        public string ContactNumber { get; set; }

        [Display(Name = "E-mail address")]
        [Required]
        [ValidateEmailAddress]
        public string Email { get; set; }

        [Display(Name = "Fax")]
        [Required]        
        public string Fax { get; set; }

        [DietaryValidation]
        [Display(Name = "Special Dietary requirements")]
        public string DietaryRequirements { get; set; }

        [DietaryValidation]
        public string OtherDietaryRequirements { get; set; }
      
        public string Company { get; set; }

        #endregion

        #region PostalAddress

        [PostalAddressValidation]
        [Display(Name = "Street / Postbox")]
        public string PostalStreet_1 { get; set; }
        
        [Display(Name = " ")]
        public string PostalStreet_2 { get; set; }

        [Display(Name = " ")]
        public string PostalStreet_3 { get; set; }

        [NonSAPostalAddressValidation]
        [Display(Name = "Suburb")]
        public string PostalSuburb { get; set; }

        [NonSAPostalAddressValidation]
        [Display(Name = "City / Town")]
        public string PostalCity { get; set; }

        [NonSAPostalAddressValidation]
        [Display(Name = "Postal Code")]
        public string PostalPostalCode { get; set; }

        [Display(Name = "Postal Code")]
        public string NonSAPostalCode { get; set; }

        [Display(Name = "Postal Code")]
        public string ForeignPostalPostalCode { get; set; }


        [SAPostalAddressValidation]
        [Display(Name = "Postal Code or Suburb or City/Town")]
        public string PostalAfrigis { get; set; }
       
        [SAPostalAddressValidation]        
        [Display(Name = "Select your Post Office distribution area")]
        public string PostalDistributionArea { get; set; }

        [Required]
        [Display(Name = "Country")]
        public string PostalCountry { get; set; }       

        #endregion

        #region Academic Qualifications

        [QualificationsValidation]
        public string Qualification1 { get; set; }
       
        public string QualificationMajor1 { get; set; }
       
        public string QualificationInstitution1 { get; set; }

        public string QualificationYearAchieved1 { get; set; }

        public string Qualification2 { get; set; }

        public string QualificationMajor2 { get; set; }

        public string QualificationInstitution2 { get; set; }

        public string QualificationYearAchieved2 { get; set; }

        public string Qualification3 { get; set; }

        public string QualificationMajor3 { get; set; }

        public string QualificationInstitution3 { get; set; }

        public string QualificationYearAchieved3 { get; set; }

        public string Qualification4 { get; set; }

        public string QualificationMajor4 { get; set; }

        public string QualificationInstitution4 { get; set; }

        public string QualificationYearAchieved4 { get; set; }

        #endregion

        #region PaymentInformation

        public string PaymentResponsibility { get; set; }

        [PaymentDetailsValidation]
        [Display(Name="Company Name")]
        public string PaymentCompanyName { get; set; }

        [PaymentDetailsValidation]
        [Display(Name = "Contact Person")]
        public string PaymentContactPerson { get; set; }

        [PaymentDetailsValidation]
        [Display(Name = "Contact Number")]
        public string PaymentContactNumber { get; set; }

        [PaymentDetailsValidation]
        [Display(Name = "Contact Email Address")]
        public string PaymentContactEmail { get; set; }

        #endregion

        #region WorkExperience

        [WorkExperienceValidation]
        public string WorkExperienceIndustryCurrent { get; set; }

        public string WorkExperienceIndustryPrevious1 { get; set; }

        public string WorkExperienceIndustryPrevious2 { get; set; }

        public string WorkExperienceAreaCurrent { get; set; }

        public string WorkExperienceAreaPrevious1 { get; set; }

        public string WorkExperienceAreaPrevious2 { get; set; }

        public string WorkExperienceJobTitleCurrent { get; set; }

        public string WorkExperienceJobTitlePrevious1 { get; set; }

        public string WorkExperienceJobTitlePrevious2 { get; set; }

        public string WorkExperienceStartDateCurrent { get; set; }

        public string WorkExperienceStartDatePrevious1 { get; set; }

        public string WorkExperienceStartDatePrevious2 { get; set; }

        public string WorkExperienceEndDateCurrent { get; set; }

        public string WorkExperienceEndDatePrevious1 { get; set; }

        public string WorkExperienceEndDatePrevious2 { get; set; }

        #endregion

        #region Marketing

        public string MarketingSource { get; set; }

        public string MarketingSourceOther { get; set; }

        public string MarketingReason { get; set; }

        public string MarketingReasonOther { get; set; }

        #endregion

        #endregion
    }
}
