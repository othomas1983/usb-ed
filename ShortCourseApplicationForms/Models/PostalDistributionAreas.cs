﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ShortCourseApplicationForms.Models
{
    public static class PostalDistributionArea
    {
        static string postalDistributionUrl = "http://bpccrmappdev.belpark.sun.ac.za/UsbEdApplicationFormApi/ApplicationFormApi/AfrigisSearch";

        //private PostalDistributionArea()
        //{
        //}

        #region Properties

        //private static PostalDistributionArea PostalDistributionArea;

        public static List<SelectListItem> Areas { get; set; }

        #endregion

        #region Public Methods

        public static void LoadAreas(string query)
        {
            if (!string.IsNullOrEmpty(query))
            {
                WebClient client = new WebClient();

                client.QueryString.Add("query", query.ToLower());

                Areas = new List<SelectListItem>();

                try
                {
                    dynamic result = JsonConvert.DeserializeObject(client.DownloadString(postalDistributionUrl));

                    foreach (dynamic item in result)
                    {
                        Areas.Add(new SelectListItem { Text = item.DisplayText, Value = item.Id });
                    }
                }
                catch 
                {
                }
            }
        }

        #endregion
    }
}