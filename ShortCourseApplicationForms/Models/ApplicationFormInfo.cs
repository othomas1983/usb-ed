﻿using Newtonsoft.Json;
using ShortCourseApplicationForms.Models.Rules;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ShortCourseApplicationForms.Models
{
    public class ApplicationFormInfo
    {
        protected static string gendersUrlBase = "http://bpccrmappdev.belpark.sun.ac.za/UsbEdApplicationFormApi/ApplicationFormApi/Genders/";

        protected static string titlesUrlBase = "http://bpccrmappdev.belpark.sun.ac.za/UsbEdApplicationFormApi/ApplicationFormApi/Titles/";

        protected static string ethnicitiesUrlBase = "http://bpccrmappdev.belpark.sun.ac.za/UsbEdApplicationFormApi/ApplicationFormApi/Ethnicities/";

        protected static string nationalitiesUrlBase = "http://bpccrmappdev.belpark.sun.ac.za/UsbEdApplicationFormApi/ApplicationFormApi/Nationalities/";

        protected static string languagesUrlBase = "http://bpccrmappdev.belpark.sun.ac.za/UsbEdApplicationFormApi/ApplicationFormApi/Languages/";

        protected static string dietaryRequirementsUrlBase = "http://bpccrmappdev.belpark.sun.ac.za/UsbEdApplicationFormApi/ApplicationFormApi/DietaryRequirements/";

        protected static string industriesUrlBase = "http://bpccrmappdev.belpark.sun.ac.za/UsbEdApplicationFormApi/ApplicationFormApi/Industries/";

        protected static string workAreasUrlBase = "http://bpccrmappdev.belpark.sun.ac.za/UsbEdApplicationFormApi/ApplicationFormApi/WorkAreas/";

        protected static string marketingReasonUrl = "http://bpccrmappdev.belpark.sun.ac.za/UsbEdApplicationFormApi/ApplicationFormApi/MarketingReasons";

        protected static string marketingSourcesUrl = "http://bpccrmappdev.belpark.sun.ac.za/UsbEdApplicationFormApi/ApplicationFormApi/MarketingSources";

        private ApplicationFormInfo()
        {
        }

        #region Properties
        
        private static ApplicationFormInfo Info;

        public static List<SelectListItem> Languages { get; set; }

        public static List<SelectListItem> Nationalities { get; set; }

        public static List<SelectListItem> Titles { get; set; }

        public static List<SelectListItem> Genders { get; set; }

        public static List<SelectListItem> Ethnicities { get; set; }

        public static List<SelectListItem> DietaryRequirements { get; set; }

        public static List<SelectListItem> Industries { get; set; }

        public static List<SelectListItem> WorkAreas { get; set; }
        
        public static List<SelectListItem> IdDocumentTypes { get; set; }

        public static List<SelectListItem> PaymentResponsibility { get; set; }

        public static List<SelectListItem> MarketingReasons { get; set; }

        public static List<SelectListItem> MarketingSources { get; set; }

        #endregion

        #region Public Methods

        public static ApplicationFormInfo LoadInfoMembers()
        {
            if (Info == null)
            {
                Info = new ApplicationFormInfo();
                WebClient client = new WebClient();

                try
                {
                    LoadTitles(client);

                    LoadLanguages(client);

                    LoadGenders(client);

                    LoadNationalities(client);

                    LoadDietaryRequirements(client);

                    LoadPaymentResponsibility(client);

                    LoadEthnicities(client);

                    LoadIndustries(client);

                    LoadWorkAreas(client);

                    LoadIdDocumentTypes(client);

                    LoadMarketingReasons(client);

                    LoadMarketingSources(client);
                }
                catch
                {
                    //create exception message
                }
            }

            return Info;
        }

        #endregion

        #region Private Methods

        public static void LoadTitles(WebClient client)
        {
            //Titles = new List<SelectListItem>();
            //Titles.Add(new SelectListItem { Text = "", Value = "" });
            //Titles.Add(new SelectListItem { Text = "Mr", Value = "af" });
            //Titles.Add(new SelectListItem { Text = "Mrs", Value = "en" });

            Titles = new List<SelectListItem>();
            Titles.Add(new SelectListItem { Text = "", Value = "" });
            dynamic result = JsonConvert.DeserializeObject(client.DownloadString(titlesUrlBase));

            foreach (dynamic item in result)
            {
                Titles.Add(new SelectListItem { Text = item.Name, Value = item.Value });
            }
        }

        public static void LoadLanguages(WebClient client)
        {
            //Languages = new List<SelectListItem>();
            //Languages.Add(new SelectListItem { Text = "", Value = "" });
            //Languages.Add(new SelectListItem { Text = "Afrikaans", Value = "af" });
            //Languages.Add(new SelectListItem { Text = "Engels", Value = "en" });

            Languages = new List<SelectListItem>();
            Languages.Add(new SelectListItem { Text = "", Value = "" });
            dynamic result = JsonConvert.DeserializeObject(client.DownloadString(languagesUrlBase));

            foreach (dynamic item in result)
            {
                Languages.Add(new SelectListItem { Text = item.Name, Value = item.Value });
            }
        }

        public static void LoadGenders(WebClient client)
        {
            //Genders = new List<SelectListItem>();
            //Genders.Add(new SelectListItem { Text = "", Value = "" });
            //Genders.Add(new SelectListItem { Text = "Male", Value = "af" });
            //Genders.Add(new SelectListItem { Text = "Femals", Value = "en" });

            Genders = new List<SelectListItem>();
            Genders.Add(new SelectListItem { Text = "", Value = "" });
            dynamic result = JsonConvert.DeserializeObject(client.DownloadString(gendersUrlBase));

            foreach (dynamic item in result)
            {
                Genders.Add(new SelectListItem { Text = item.Name, Value = item.Value });
            }
        }

        public static void LoadNationalities(WebClient client)
        {
            //Nationalities = new List<SelectListItem>();
            //Nationalities.Add(new SelectListItem { Text = "", Value = "" });
            //Nationalities.Add(new SelectListItem { Text = "South Africa", Value = "af" });
            //Nationalities.Add(new SelectListItem { Text = "Namibia", Value = "en" });

            Nationalities = new List<SelectListItem>();
            Nationalities.Add(new SelectListItem { Text = "", Value = "" });
            dynamic result = JsonConvert.DeserializeObject(client.DownloadString(nationalitiesUrlBase));
            var nationalities = new List<SelectListItem>();

            foreach (dynamic item in result)
            {
                Nationalities.Add(new SelectListItem { Text = item.Name, Value = item.Value });
            }
        }       

        public static void LoadPaymentResponsibility(WebClient client)
        {
            PaymentResponsibility = new List<SelectListItem>();
            PaymentResponsibility.Add(new SelectListItem { Text = "", Value = "" });
            PaymentResponsibility.Add(new SelectListItem { Text = "Self", Value = "s" });
            PaymentResponsibility.Add(new SelectListItem { Text = "Company", Value = "c" });

            //PaymentResponsibility = new List<SelectListItem>();
            //PaymentResponsibility.Add(new SelectListItem { Text = "", Value = "" });
            //dynamic result = JsonConvert.DeserializeObject(client.DownloadString(gendersUrlBase));

            //foreach (dynamic item in result)
            //{
            //    Genders.Add(new SelectListItem { Text = item.Name, Value = item.Value });
            //}
        }

        public static void LoadEthnicities(WebClient client)
        {
            //Ethnicities = new List<SelectListItem>();
            //Ethnicities.Add(new SelectListItem { Text = "", Value = "" });
            //Ethnicities.Add(new SelectListItem { Text = "White", Value = "af" });
            //Ethnicities.Add(new SelectListItem { Text = "Indian", Value = "en" });

            Ethnicities = new List<SelectListItem>();
            Ethnicities.Add(new SelectListItem { Text = "", Value = "" });
            dynamic result = JsonConvert.DeserializeObject(client.DownloadString(ethnicitiesUrlBase));

            foreach (dynamic item in result)
            {
                Ethnicities.Add(new SelectListItem { Text = item.Name, Value = item.Value });
            }
        }

        public static void LoadDietaryRequirements(WebClient client)
        {
            //DietaryRequirements = new List<SelectListItem>();
            //DietaryRequirements.Add(new SelectListItem { Text = "", Value = "" });
            //DietaryRequirements.Add(new SelectListItem { Text = "White", Value = "af" });
            //DietaryRequirements.Add(new SelectListItem { Text = "Indian", Value = "en" });

            DietaryRequirements = new List<SelectListItem>();
            DietaryRequirements.Add(new SelectListItem { Text = "", Value = "" });
            dynamic result = JsonConvert.DeserializeObject(client.DownloadString(dietaryRequirementsUrlBase));

            foreach (dynamic item in result)
            {
                DietaryRequirements.Add(new SelectListItem { Text = item.Name, Value = item.Value });
            }
        }

        public static void LoadIndustries(WebClient client)
        {
            //Industries = new List<SelectListItem>();
            //Industries.Add(new SelectListItem { Text = "", Value = "" });
            //Industries.Add(new SelectListItem { Text = "White", Value = "af" });
            //Industries.Add(new SelectListItem { Text = "Indian", Value = "en" });

            Industries = new List<SelectListItem>();
            Industries.Add(new SelectListItem { Text = "", Value = "" });
            dynamic result = JsonConvert.DeserializeObject(client.DownloadString(industriesUrlBase));

            foreach (dynamic item in result)
            {
                Industries.Add(new SelectListItem { Text = item.Name, Value = item.Value });
            }
        }

        public static void LoadWorkAreas(WebClient client)
        {
            //WorkAreas = new List<SelectListItem>();
            //WorkAreas.Add(new SelectListItem { Text = "", Value = "" });
            //WorkAreas.Add(new SelectListItem { Text = "White", Value = "af" });
            //WorkAreas.Add(new SelectListItem { Text = "Indian", Value = "en" });

            WorkAreas = new List<SelectListItem>();
            WorkAreas.Add(new SelectListItem { Text = "", Value = "" });
            dynamic result = JsonConvert.DeserializeObject(client.DownloadString(workAreasUrlBase));

            foreach (dynamic item in result)
            {
                WorkAreas.Add(new SelectListItem { Text = item.Name, Value = item.Value });
            }
        }

        public static void LoadIdDocumentTypes(WebClient client)
        {
            IdDocumentTypes = new List<SelectListItem>();
            IdDocumentTypes.Add(new SelectListItem { Text = "", Value = "" });
            IdDocumentTypes.Add(new SelectListItem { Text = "Passport", Value = "af" });
            IdDocumentTypes.Add(new SelectListItem { Text = "Foreign identification number", Value = "en" });

            //IdDocumentTypes = new List<SelectListItem>();
            //IdDocumentTypes.Add(new SelectListItem { Text = "", Value = "" });
            //dynamic result = JsonConvert.DeserializeObject(client.DownloadString(gendersUrlBase));

            //foreach (dynamic item in result)
            //{
            //    IdDocumentTypes.Add(new SelectListItem { Text = item.Name, Value = item.Value });
            //}
        }

        public static void LoadMarketingReasons(WebClient client)
        {
            //MarketingReasons = new List<SelectListItem>();
            //MarketingReasons.Add(new SelectListItem { Text = "", Value = "" });
            //MarketingReasons.Add(new SelectListItem { Text = "White", Value = "af" });
            //MarketingReasons.Add(new SelectListItem { Text = "Indian", Value = "en" });

            MarketingReasons = new List<SelectListItem>();
            MarketingReasons.Add(new SelectListItem { Text = "", Value = "" });
            dynamic result = JsonConvert.DeserializeObject(client.DownloadString(marketingReasonUrl));

            foreach (dynamic item in result)
            {
                MarketingReasons.Add(new SelectListItem { Text = item.Name, Value = item.Value });
            }
        }

        public static void LoadMarketingSources(WebClient client)
        {
            //MarketingSources = new List<SelectListItem>();
            //MarketingSources.Add(new SelectListItem { Text = "", Value = "" });
            //MarketingSources.Add(new SelectListItem { Text = "White", Value = "af" });
            //MarketingSources.Add(new SelectListItem { Text = "Indian", Value = "en" });

            MarketingSources = new List<SelectListItem>();
            MarketingSources.Add(new SelectListItem { Text = "", Value = "" });
            dynamic result = JsonConvert.DeserializeObject(client.DownloadString(marketingSourcesUrl));

            foreach (dynamic item in result)
            {
                MarketingSources.Add(new SelectListItem { Text = item.Name, Value = item.Value });
            }
        }
    }

        #endregion
}

