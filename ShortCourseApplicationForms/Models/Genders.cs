﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ShortCourseApplicationForms.Models
{
    public class ApplicationFormsGenders
    {
        protected static string gendersUrlBase = "http://bpccrmappdev.belpark.sun.ac.za/UsbEdApplicationFormApi/ApplicationFormApi/Genders/";

        private ApplicationFormsGenders()
        {            
        }

        private static ApplicationFormsGenders _ApplicationFormsGenders;

        public static List<SelectListItem> Genders { get; set; }

        public static ApplicationFormsGenders LoadGenders()
        {
            if (_ApplicationFormsGenders == null)
            {
                _ApplicationFormsGenders = new ApplicationFormsGenders();

                WebClient client = new WebClient();
                Genders = new List<SelectListItem>();
                Genders.Add(new SelectListItem { Text = "", Value = "" });
                try
                {
                    dynamic result = JsonConvert.DeserializeObject(client.DownloadString(gendersUrlBase));

                    foreach (dynamic item in result)
                    {
                        Genders.Add(new SelectListItem { Text = item.Name, Value = item.Value });
                    }
                }
                catch
                {
                    //create exception message
                }
            }

            return _ApplicationFormsGenders;            
        }
    }
}