﻿using ShortCourseApplicationForm.Domain.Controller;
using ShortCourseApplicationForm.Domain.CRM;
using ShortCourseApplicationForm.Domain.Models.ShortCourseApplicationForm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RestStudentService.Controllers
{
    public class StudentServiceController : ApiController
    {

        private ShortCourseApplicationFormsApiController apiController = new ShortCourseApplicationFormsApiController();
        /// <summary>
        /// GetStudentNumber
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public string GetStudentNumber(string email)
        {

            string result = null;

            result = CrmQueries.GetUsNumber(email);

            if (result == null || result == "")
            {
                return "Student number not found.";
            }
            return result;

        }

        /// <summary>
        /// CreateContactAndEnrollStudent
        /// </summary>
        /// <param name="ShortCourseOffering"></param>
        /// <param name="EmailAdress"></param>
        /// <param name="LastName"></param>
        /// <param name="FirstNames"></param>
        /// <param name="NameOnCertificate"></param>
        /// <param name="Initials"></param>
        /// <param name="Title"></param>
        /// <param name="Gender"></param>
        /// <param name="Nationality"></param>
        /// <param name="Ethnicity"></param>
        /// <param name="DateOfBirth"></param>
        /// <param name="ForeignIdNumber"></param>
        /// <param name="ForeignIdType"></param>
        /// <param name="ForeignIdExpiryDate"></param>
        /// <param name="PassportNumber"></param>
        /// <param name="PassportExpiryDate"></param>
        /// <param name="IdNumber"></param>
        /// <param name="DietaryRequirements"></param>
        /// <param name="DietaryRequirementsOther"></param>
        /// <param name="Language"></param>
        /// <param name="Disability"></param>
        /// <param name="DoYouUseAWheelchair"></param>
        /// <param name="Environment"></param>
        /// <param name="PostalCountry"></param>
        /// <param name="Province"></param>
        /// <param name="PostalSuburb"></param>
        /// <param name="PostalCity"></param>
        /// <param name="PostalStreet1"></param>
        /// <param name="PostalStreet2"></param>
        /// <param name="PostalPostalCode"></param>
        /// <param name="CellphoneNumber"></param>
        /// <returns></returns>
        public ApplicationSubmissionResult CreateContactAndEnrollStudent(
         Guid ShortCourseOffering,
         string EmailAdress,
         string LastName,
         string FirstNames,
         string NameOnCertificate,
         string Initials,
         Guid Title,
         Guid Gender,
         Guid Nationality,
         Guid Ethnicity,
         DateTime DateOfBirth,
         string ForeignIdNumber,
         Guid ForeignIdType,
         DateTime ForeignIdExpiryDate,
         string PassportNumber,
         DateTime PassportExpiryDate,
         string IdNumber,
         Guid DietaryRequirements,
         string DietaryRequirementsOther,
         Guid Language,
         Guid Disability,
         bool DoYouUseAWheelchair,
         string Environment,
         Guid PostalCountry,
         Guid Province,
         string PostalSuburb,
         string PostalCity,
         string PostalStreet1,
         string PostalStreet2,
         string PostalPostalCode,
         string CellphoneNumber
           )
        {

            Guid enrolmentId;
            Guid contactId;
            string UsNumber = CrmQueries.GetUsNumber(EmailAdress);


            string correspondingLanguageCode = CrmQueries.GetSisCorrespondingLanguageCode(Language);

            if (UsNumber == "" || UsNumber == null)
            {
                try
                {
                    UsNumber = ShortCourseApplicationForm.Domain.SIS.ContactMaintenanceInteractions.GetUSNumber(FirstNames, LastName, Initials, EmailAdress, DateOfBirth, Title,
                    Gender, CellphoneNumber, Ethnicity, Nationality, Language, IdNumber, PassportNumber, PassportExpiryDate, ForeignIdNumber,
                    correspondingLanguageCode);
                }
                catch (ApplicationException ae)
                {
                    return new ApplicationSubmissionResult { Success = false, Message = ae.Message };
                }
                catch (Exception ex)
                {
                    return new ApplicationSubmissionResult
                    {
                        Success = false,
                        Message = "Could not get US Number for applicant. " + ex.Message
                    };
                }
            }

            try
            {
                enrolmentId = CrmQueries.GetEnrolmentId(UsNumber, ShortCourseOffering);
                contactId = CrmQueries.GetContactId(UsNumber);

                CrmActions.CreateOrUpdateContact(ShortCourseOffering, EmailAdress, LastName, FirstNames,
                               NameOnCertificate, Initials, Title, Gender, Nationality, Ethnicity, DateOfBirth, ForeignIdNumber,
                               ForeignIdType, ForeignIdExpiryDate, PassportNumber, PassportExpiryDate, IdNumber, DietaryRequirements,
                               DietaryRequirementsOther, Language, Disability, DoYouUseAWheelchair, contactId, enrolmentId, UsNumber, Environment,
                               PostalCountry, Province, PostalSuburb, PostalCity, PostalStreet1, PostalStreet2, PostalPostalCode,CellphoneNumber);
            }
            catch (Exception ex)
            {
                return new ApplicationSubmissionResult
                {
                    Success = false,
                    Message = "Could not create or update contact. " + ex.Message
                };
            }

            return new ApplicationSubmissionResult
            {
                Success = true,
                Message = "",
                UsNumber = UsNumber,
                EnrolmentId = enrolmentId
            };
        }

        /// <summary>
        /// Upload Documents
        /// </summary>
        /// <param name="ShortCourseOffering"></param>
        /// <param name="EmailAdress"></param>
        /// <param name="documentBytes"></param>
        /// <param name="type"></param>
        /// <param name="documentName"></param>
        /// <param name="documentDescription"></param>
        /// <returns></returns>
        public ShortCourseApplicationForm.Domain.Models.ShortCourseApplicationForm.ApplicationSubmissionResult UploadRequiredDocuments(Guid ShortCourseOffering, string EmailAdress, byte[] documentBytes, string type, string documentName, string documentDescription)
        {
            var DocumentLevel = 0;
            ShortCourseApplicationForm.Domain.Models.ShortCourseApplicationForm.ApplicationSubmissionResult result = null;
            Guid enrolmentId;

            string UsNumber = CrmQueries.GetUsNumber(EmailAdress);
            enrolmentId = CrmQueries.GetEnrolmentId(UsNumber, ShortCourseOffering);

            var shortCourseId = CrmQueries.LoadShortCourseBy(ShortCourseOffering);
            List<ShortCourseApplicationForm.Domain.Models.Document> RequiredDocuments = CrmQueries.GetRequiredDocuments(shortCourseId, true);

            var requiredDocument = RequiredDocuments.FirstOrDefault();
            if (requiredDocument.Description == "ID" || requiredDocument.Description == "Matric")
            {
                DocumentLevel = 1;
            }
            else if (requiredDocument.Description == "Acceptance")
            {
                DocumentLevel = 3;
            }


            result = apiController.SubmitDocumentation(enrolmentId, UsNumber,
                         documentName, DocumentLevel, documentDescription,
                         requiredDocument.DocumentId, documentBytes);


            return result;
        }

        public class ApplicationSubmissionResult
        {
            bool success = true;
            string usNumber = "";
            string message = "";
            Guid enrollmentId = Guid.Empty;
           
            public bool Success
            {
                get { return success; }
                set { success = value; }
            }

           
            public Guid EnrolmentId
            {
                get { return enrollmentId; }
                set { enrollmentId = value; }
            }

        
            public string UsNumber
            {
                get { return usNumber; }
                set { usNumber = value; }
            }
           
            public string Message
            {
                get { return message; }
                set { message = value; }
            }
        }

    }
}
