﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Runtime.Caching;
namespace ShortCourseApplicationForm.Models
{
    public static class Cache
    {
        private static MemoryCache cache;

        static Cache()
        {
            cache = new MemoryCache("cache");
        }

        public static void Add(string key, object item)
        {
            cache.Set(key, item, new DateTimeOffset(DateTime.Now.AddHours(4)));
        }

        public static object GetCachedItem(string key)
        {
            return cache[key];
        }

        public static void RemoveItem(string key)
        {
            if (cache.Contains(key))
            {
                cache.Remove(key);
            }
        }

        public static bool Contains(string key)
        {
            return cache.Contains(key);
        }
    }
}
