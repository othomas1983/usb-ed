﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using ShortCourseApplicationForm.Domain.Models.Enums;
using ShortCourseApplicationForm.Domain.Validation;

namespace ShortCourseApplicationForm.Domain.Models.ShortCourseApplicationForm
{
    public enum ValidateProgress
    {
        EmailAddress,
        PersonalInformation,
        ContactInformation,
        Qualifications,
        WorkExperience,
        Marketing,
        PaymentInfo,
        ApplicationForm,
        GatewayPaymentSuccesful
    }

    public class ShortCourseInfo
    {
        public string OfferingName { get; set; }
        public string OfferingStartDate { get; set; }
        public string OfferingEndDate { get; set; }
        public string CourseAdministrator { get; set; }
        public string CourseId { get; set; }
    }

    public class ApplicationForm
    {
        #region Properties

        public ApplicantStatus ApplicantStatus { get; set; }

        public VoucherStatus VoucherStatus { get; set; }

    public ShortCourseOfferingType ShortCourseOfferingType { get; set; }

        [JsonIgnore]
        public List<ShortCourseInfo> ShortCourseInfo { get; set; }

        #region Personal Information

        private string contactNumber;

        private static string applicationKey;

        public string ApplicationKey
        {
            get { return applicationKey; }
            set { applicationKey = value; }
        }

        [JsonIgnore]
        public string DateOfBirthDay { get; set; }

        [JsonIgnore]
        public string DateOfBirthMonth { get; set; }

        [JsonIgnore]
        public string DateOfBirthYear { get; set; }

        [JsonIgnore]
        public bool HasListItems { get; set; }

        public string WebTokenId { get; set; }

        [JsonIgnore]
        public bool InsideCRM { get; set; }

        [JsonIgnore]
        public bool DirectOfferingLink { get; set; }

        [JsonIgnore]
        public bool ApplicationClosed { get; set; }

        [JsonIgnore]
        public bool DisplayNameApproved { get; set; }

        [JsonIgnore]
        public string CopyOfID { get; set; }

        [JsonIgnore]
        public string CopyOfCertificate { get; set; }

        [JsonIgnore]
        public string ValidationSection { get; set; }

        [JsonIgnore]
        public bool EmailSectionValidated { get; set; }

        [JsonIgnore]
        public AccountType AccountType { get; set; }

        [JsonIgnore]
        public bool IsMasterStart { get; set; }

        [JsonIgnore]
        public bool IsBasilRead { get; set; }

        [JsonIgnore]
        public bool QualifyForVoucher { get; set; }

        [JsonIgnore]
        public bool Section1Validated { get; set; }

        [JsonIgnore]
        public bool Section2Validated { get; set; }

        [JsonIgnore]
        public bool Section3Validated { get; set; }

        [JsonIgnore]
        public bool Section4Validated { get; set; }

        [JsonIgnore]
        public bool Section5Validated { get; set; }

        [JsonIgnore]
        public string SelectedTab { get; set; }

        private string courseStartDate;

        [JsonIgnore]
        public string CourseStartDate
        {
            get { return courseStartDate; }
            set { courseStartDate = ResolveCourseStartDate(value); }
        }

        [JsonIgnore]
        public string CourseAdministrator { get; set; }

        [JsonIgnore]
        public string CourseId { get; set; }

        [JsonIgnore]
        public string CourseName { get; set; }

        [JsonIgnore]
        public string ShortCourseId { get; set; }

        [JsonIgnore]
        public string ShortCourseOfferingId { get; set; }

        public string IdentificationDocumentUploadKey { get; set; }
        public string QualificationDocumentUploadKey { get; set; }
        public string AcceptanceDocumentUploadKey { get; set; }

        public string CVDocumentUploadKey { get; set; }
        public Guid? Enrolment { get; set; }

        [Required]
        public Guid Offering { get; set; }

        [Required]
        public string Surname { get; set; }

        [Required]
        [Display(Name = "First Names")]
        public string FirstNames { get; set; }

        [Required]
        public string Initials { get; set; }
        [Required]
        [Display(Name = "Name called by")]
        public string NameCalledBy { get; set; }

        [Display(Name = "Given name")]
        public string GivenName { get; set; }

        public string NameOnCertificate { get; set; }

        [Required]
        public Guid Title { get; set; }

        [Required]
        public Guid Gender { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        [Range(typeof(DateTime), "01/01/1900","01/01/2090", ErrorMessage = " Value for {0} must be greater than 1900/01/01.")]
        [Required]        
        [Display(Name = "Date of Birth")]
        public DateTime DateOfBirth { get; set; }

       [EthnicityValidation]
        public Guid? Ethnicity { get; set; }

        [Required]
        public Guid Nationality { get; set; }

        [IdNumberValidation]
        [Display(Name = "Identity Number")]
        public string IdNumber { get; set; }

        [Display(Name = "Type of identification:")]
        public Guid? ForeignIdType { get; set; }

        public string PassportNumber { get; set; }

        [Display(Name = "Expiry date:")]
        public DateTime? PassportExpiryDate { get; set; }

        [Display(Name = "Type of identification document")]
        public string ForeignIdNumber { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        [Range(typeof(DateTime), "01/01/1900", "01/01/2090", ErrorMessage = " Value for {0} must be greater than 1900/01/01.")] 
        [Display(Name = "Foreign Id Expiry Date")]
        public DateTime? ForeignIdExpiryDate { get; set; }

        [Display(Name = "Company name")]
        public Guid? Company { get; set; }

        [Display(Name = "Current occupation")]
        public string Occupation { get; set; }

        [Required]
        [Display(Name = "Home Language")]
        public Guid Language { get; set; }

        [DietaryValidation]
        [Display(Name = "Dietary requirements")]
        public Guid? DietaryRequirements { get; set; }

        [DietaryValidation]
        public string DietaryRequirementsOther { get; set; }

        [Required]
        public Guid? Disability { get; set; }

        [Display(Name = "Uses Wheelchair")]
        public bool UsesWheelchair { get; set; }

        [Display(Name = "E-mail address")]
        [Required]
        [ValidateEmailAddress]
        public string Email { get; set; }

        [Display(Name = "E-mail address")]
        [ValidateEmailAddress]
        public string RegisteredEmail { get; set; }

        [Required]
        [Display(Name = "Contact Number")]
        [DataType(DataType.PhoneNumber)]
        public string ContactNumber
        {
            get { return contactNumber; }
            set { contactNumber = value.IsNullOrEmpty() ? value : value.Replace(" ", ""); }
        }

        public string CellphoneNumber { get; set; }

        public string WorkNumber { get; set; }

        [Display(Name = "Fax")]
        public string FaxNumber { get; set; }

        [Display(Name = "Surname")]
        public string AssistantSurname { get; set; }

        public bool HasAssistant { get; set; } = false;

        [Display(Name = "First Names")]
        public string AssistantFirstnames { get; set; }

        [Display(Name = "Contact Number")]
        public string AssistantContactNumber { get; set; }

        [Display(Name = "Cellphone Number")]
        public string AssistantCellphoneNumber { get; set; }

        [Display(Name = "Email Address")]
        public string AssistantEmailAddress { get; set; }

        #endregion

        #region PostalAddress

        [PostalAddressValidation]
        public string PostalStreet1 { get; set; }

        public string PostalStreet2 { get; set; }

        [NonSAPostalAddressValidation]
        [Display(Name = "Suburb")]
        public string PostalSuburb { get; set; }

        [NonSAPostalAddressValidation]
        [Display(Name = "City / Town")]
        public string PostalCity { get; set; }

        //[NonSAPostalAddressValidation]
        [Display(Name = "Postal Code")]
        public string PostalPostalCode { get; set; }

        [Display(Name = "Postal Code")]
        public string NonSAPostalCode { get; set; }

        [SAPostalAddressValidation]
        [Display(Name = "Postal Code or Suburb or City/Town")]
        public Guid? PostalAfrigis { get; set; }

        [Required]
        [Display(Name = "Country")]
        public Guid PostalCountry { get; set; }

        [Display(Name = "Country")]
        public string SelectedCountry { get; set; }       

        //[Required]
        [Display(Name = "Province")]
        public Guid? Province { get; set; }

        #endregion

        #region Postal Courier

        public string CourierStreet { get; set; }

        public string CourierSuburb { get; set; }

        public string CourierCity { get; set; }

        public string CourierPostalCode { get; set; }

        public Guid CourierAfrigis { get; set; }

        public Guid CourierCountry { get; set; }

        #endregion

        #region Academic Qualifications

        public string Qualification { get; set; }

        public string QualificationTypeOther { get; set; }

        [Display(Name = "Qualification Type")]
        public Guid QualificationType { get; set; }

        [Display(Name = "Qualification Field")]
        public Guid? QualificationField { get; set; }

        [Display(Name = "Qualification Institution")]
        public string QualificationInstitution { get; set; }

        public string YearAchieved { get; set; }

        public List<Document> RequiredDocuments { get; set; }

        #endregion

        #region Employement

        public bool Employed { get; set; }

        public Guid? Industry { get; set; }

        public Guid? WorkArea { get; set; }

        public string JobTitle { get; set; }

        public string Employer { get; set; }

        public string EmployerContactNumber { get; set; }

        public Guid OccupationalCategory { get; set; }

        #endregion

        #region PaymentInformation

        [PaymentResponsibilityValidation]
        [Display(Name = "Payment Responsibility")]
        public Guid PaymentResponsibility { get; set; }

        [PaymentDetailsValidation]
        [Display(Name = "First name")]
        public string PaymentContactName { get; set; }

        [PaymentDetailsValidation]
        [Display(Name = "Surname")]
        public string PaymentContactSurname { get; set; }

        [PaymentDetailsValidation]
        [Display(Name = "Contact Number")]
        public string PaymentContactNumber { get; set; }

        [PaymentDetailsValidation]
        [ValidatePaymentDetailsEmailAddress]
        [Display(Name = "Contact Email")]
        public string PaymentContactEmail { get; set; }

        [PaymentDetailsValidation]
        [Display(Name = "Name of Company")]
        public string PaymentContactCompanyName { get; set; }

        //[Display(Name = "Payment Debtor Code")]
        //public string PaymentDebtorCode { get; set; }
        [Required]
        public bool? Consent { get; set; }

        public Document CompanyPurchaseOrderLetter { get; set; }

        [PostalAddressValidation]
        public string PaymentPostalStreet1 { get; set; }

        public string PaymentPostalStreet2 { get; set; }

        [NonSAPostalAddressValidation]
        [Display(Name = "Suburb")]
        public string PaymentPostalSuburb { get; set; }

        [NonSAPostalAddressValidation]
        [Display(Name = "City / Town")]
        public string PaymentPostalCity { get; set; }

        //[NonSAPostalAddressValidation]
        [Display(Name = "Postal Code")]
        public string PaymentPostalCode { get; set; }

        [SAPostalAddressValidation]
        [Display(Name = "Postal Code or Suburb or City/Town")]
        public Guid? PaymentPostalAfrigis { get; set; }

        [Required]
        [Display(Name = "Country")]
        public Guid PaymentPostalCountry { get; set; }

        [Display(Name = "Country")]
        public string PaymentSelectedCountry { get; set; }
        public Guid? PaymentProvince { get; set; }

        public bool IsSelf { get; set; }
        public bool IsCompanyResponsibility { get; set; }

        #endregion

        #region Marketing

        public int MarketingSource { get; set; }

        public string MarketingSourceOther { get; set; }

        public int MarketingReason { get; set; }

        public string MarketingReasonOther { get; set; }

        #endregion

        #region SocialMedia

        public bool UsesTwitter { get; set; }

        public bool UsesFacebook { get; set; }

        public bool UsesLinkedIn { get; set; }

        public string TwitterUsername { get; set; }

       // public bool SubscribeToNewsletter { get; set; }

        #endregion

        public string UsNumber { get; set; }

        public string ApplicationFormsBaseUrl { get; set; }

        public bool HasUploadedRequiredDocuments { get; set; }

        public bool HasUploadedAcceptanceLetter { get; set; }
        public bool HasCV { get; set; }

        public Guid? AccountGuid { get; set; }

        public bool HasVoucher { get; set; }

        #endregion

        #region public Methods

        public bool AllSectionsValidated()
        {
            return EmailSectionValidated
                   && Section1Validated
                   && Section2Validated
                   && Section3Validated
                   && Section4Validated
                   && Section5Validated;
        }

        public bool HasPAContactDetails()
        {
            return AssistantSurname.IsNotNullOrEmpty()
                   || AssistantFirstnames.IsNotNullOrEmpty()
                   || AssistantContactNumber.IsNotNullOrEmpty()
                   || AssistantCellphoneNumber.IsNotNullOrEmpty()
                   || AssistantEmailAddress.IsNotNullOrEmpty();
        }

        //public static HttpResponseMessage UploadFile(HttpRequestMessage message)
        //{
        //    message.RequestUri = new Uri(SettingsMan.Default.upLoadFileURL);

        //    var client = new HttpClient();
        //    return client.SendAsync(message).Result;
        //}

        #endregion

        #region Private Methods

        private string ResolveCourseStartDate(string value)
        {
            DateTime dateValue;

            if (DateTime.TryParse(value, out dateValue))
            {
               // var newStartDate = model.CourseStartDate?.AddDays(1).ToString() ?? "";
                return dateValue.AddDays(1).ToString("dd MMM yyyy");
            }
            else
            {
                return null;
            }
        }

        #endregion
    }
}

