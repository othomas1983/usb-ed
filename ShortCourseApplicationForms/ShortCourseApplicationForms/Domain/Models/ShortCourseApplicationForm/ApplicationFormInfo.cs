﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ShortCourseApplicationForm.Domain.Controller;
using ShortCourseApplicationForm.Models;

namespace ShortCourseApplicationForm.Domain.Models.ShortCourseApplicationForm
{
    public class ApplicationFormInfo
    {
        #region Properties

        //TODO: this is bad ....
        public static ApplicationFormInfo Info;

        public List<SelectListItem> ShortCourseOfferings { get; set; }
        public List<SelectListItem> Languages { get; set; }
        public List<SelectListItem> Nationalities { get; set; }
        public List<SelectListItem> AddressCountries { get; set; }
        public List<SelectListItem> Provinces { get; set; }
        public List<SelectListItem> Titles { get; set; }
        public List<SelectListItem> Genders { get; set; }
        public List<SelectListItem> Ethnicities { get; set; }
        public List<SelectListItem> EthnicitiesNonSA { get; set; }
        public List<SelectListItem> DietaryRequirements { get; set; }
        public List<SelectListItem> Industries { get; set; }
        public List<SelectListItem> WorkAreas { get; set; }
        public List<SelectListItem> OccupationalCategories { get; set; }
        public List<SelectListItem> IdDocumentTypes { get; set; }
        public List<SelectListItem> PaymentResponsibility { get; set; }
        public List<SelectListItem> MarketingReasons { get; set; }
        public List<SelectListItem> MarketingSources { get; set; }
        public List<SelectListItem> QualificationTypes { get; set; }
        public List<SelectListItem> QualificationFields { get; set; }
        public List<SelectListItem> Disabilities { get; set; }
        public List<SelectListItem> YesNoList { get; set; }
        public List<SelectListItem> YearsList { get; set; }

        #endregion

        #region Public Methods

        public ApplicationFormInfo LoadInfoMembers(ApplicationForm model, string shortCourseId, string shortCourseOfferingId)
        {
            try
            {
                var controller = new ShortCourseApplicationFormsApiController();


                Info = new ApplicationFormInfo
                {
                    // Load Short course / offering based on provided info
                    ShortCourseOfferings = controller.LoadShortCourseOfferings(model, shortCourseId, shortCourseOfferingId)
                };

                //Load ShortcourseOfferingType
                if (shortCourseOfferingId.IsNotNull())
                {
                    controller.SetShortCourseOfferingValues(model, new Guid(shortCourseOfferingId));
                }
                model.HasListItems = true;

                // Load titles
                controller.Titles(Info);

                // Load languages
                controller.Languages(Info);

                // Load genders
                controller.Genders(Info);

                // Load nationalities
                controller.Nationalities(Info);

                // Load countries
                controller.AddressCountries(Info);

                // Load provinces
                controller.Provinces(Info);

                // Load dietary requirements
                controller.DietaryRequirements(Info);

                // Load payment responsibility
                controller.PaymentResponsibilities(Info);

                // Load ethnicities
                controller.Ethnicities(Info);

                // Load ethnicities non SA
                controller.EthnicitiesNonSA(Info);

                // Load industries
                controller.Industries(Info);

                // Load work areas
                controller.WorkAreas(Info);

                // Load work occupation categories
                controller.OccupationalCategories(Info);

                // Load Id types
                controller.ForeignIdentificationTypes(Info);

                // Load marketing reasons // TODO: Get Marketing picklist from CRM and create ENUM to map
                controller.MarketingReasons(Info);

                // Load marketing sources // TODO: Get Marketing picklist from CRM and create ENUM to map
                controller.MarketingSources(Info);

                // Load qualification types
                controller.QualificationTypes(Info);

                // Load qualification fields
                controller.QualificationFields(Info);

                // Load disabilities
                controller.Disabilities(Info);

                // Load YesNo
                controller.YesNo(Info);

                // Load years
                controller.Years(Info);
                
                return Info;
            }

            catch (Exception ex)
            {
                throw ex;
                //model.HasListItems = false;
                //Info = null;
                //return Info;
            }
        }

        #endregion
    }
}

