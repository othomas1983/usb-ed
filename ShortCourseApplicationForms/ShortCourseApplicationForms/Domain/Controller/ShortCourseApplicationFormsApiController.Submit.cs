﻿using System;
using System.Linq;
using ShortCourseApplicationForm.Domain.CRM;
using ShortCourseApplicationForm.Domain.Mapper;
using ShortCourseApplicationForm.Domain.Models.Enums;
using ShortCourseApplicationForm.Domain.Models.ShortCourseApplicationForm;
using ShortCourseApplicationForm.Domain.SIS;
using StellenboschUniversity.UsbEd.Integration.Crm;

namespace ShortCourseApplicationForm.Domain.Controller
{
    public partial class ShortCourseApplicationFormsApiController
    {
        public ApplicationForm Contact(ApplicationForm model)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                var emailAddress = model.Email.IsNotNullOrEmpty() ? model.Email : model.RegisteredEmail;
                var contact = crmServiceContext.ContactSet.FirstOrDefault(c => c.EMailAddress1 == emailAddress);
                
                if (contact.IsNotNull())
                {
                    stb_enrollment enrolment = null;

                    if (model.Enrolment.IsNull() || model.Enrolment == Guid.Empty)
                    {
                        if (model.ShortCourseOfferingId.IsNotNullOrEmpty())
                        {
                            enrolment =
                                crmServiceContext.stb_enrollmentSet.FirstOrDefault(
                                    a =>
                                        a.stb_USNumber == contact.stb_USNumber &&
                                        a.stb_ShortCourseOfferingLookup.Id == Guid.Parse(model.ShortCourseOfferingId));
                        }
                        else
                        {
                            enrolment =
                                crmServiceContext.stb_enrollmentSet.FirstOrDefault(
                                    a =>
                                        a.stb_USNumber == contact.stb_USNumber);
                        }
                    }

                    return ContactMapper.MapContactToApplication(model, contact, enrolment);
                }

                return null;
            }
        }

        public ApplicationSubmissionResult CreateOrUpdateContact(ValidateProgress progressStep, ApplicationForm model)
        {
            Guid? enrolmentId;
            Guid? contactId;
            
            string correspondingLanguageCode = CrmQueries.GetSisCorrespondingLanguageCode(model.Language);

            if (model.UsNumber.IsNullOrEmpty())
            {
                try
                {
                    model.UsNumber = ContactMaintenanceInteractions.GetUSNumber(model, correspondingLanguageCode);
                }
                catch (ApplicationException ae)
                {
                    return new ApplicationSubmissionResult {Successful = false, Message = ae.Message};
                }
                catch (Exception ex)
                {
                    return new ApplicationSubmissionResult
                    {
                        Successful = false,
                        Message = "Could not get US Number for applicant. " + ex.Message
                    };
                }
            }

            try
            {
                enrolmentId = GetEnrolmentId(model.UsNumber, model.ShortCourseOfferingId);
                contactId = GetContactId(model.UsNumber);

                CrmActions.CreateOrUpdateContact(progressStep, model, contactId, enrolmentId);
            }
            catch (Exception ex)
            {
                return new ApplicationSubmissionResult
                {
                    Successful = false,
                    Message = "Could not create or update contact. " + ex.Message
                };
            }

            if (progressStep == ValidateProgress.PaymentInfo || progressStep == ValidateProgress.ApplicationForm)
            {
                return new ApplicationSubmissionResult
                {
                    Successful = true,
                    Message = "",
                    USNumber = model.UsNumber,
                    OfferingAmount = CrmQueries.GetShortCourseOfferingFee(Guid.Parse(model.ShortCourseOfferingId)),
                    OfferingName = model.CourseName,
                    EnrolmentId = enrolmentId
                };
            }

            return new ApplicationSubmissionResult
            {
                Successful = true,
                Message = "",
                USNumber = model.UsNumber,
                EnrolmentId = enrolmentId
            };
        }
    }
}
