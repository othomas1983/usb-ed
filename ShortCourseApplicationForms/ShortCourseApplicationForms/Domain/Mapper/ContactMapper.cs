﻿using System;
using System.Linq;
using ShortCourseApplicationForm.Domain.CRM;
using ShortCourseApplicationForm.Domain.Models.Enums;
using ShortCourseApplicationForm.Domain.Models.ShortCourseApplicationForm;
using ShortCourseApplicationForm.Models;
using StellenboschUniversity.UsbEd.Integration.Crm;

namespace ShortCourseApplicationForm.Domain.Mapper
{
    public class ContactMapper
    {
        public static ApplicationForm MapContactToApplication(ApplicationForm model, Contact contact,
            stb_enrollment enrolment)
        {
            #region Personal Details

            model.Email = contact.EMailAddress1;
            model.RegisteredEmail = contact.EMailAddress1;
            model.Surname = contact.LastName;
            model.FirstNames = contact.FirstName;
            if (enrolment.IsNotNull())
            {
                model.NameCalledBy = enrolment.stb_NameCalledBy;
            }
            else
            {
                model.NameCalledBy = "";
            }
            model.Initials = contact.stb_Initials;
            model.Title = contact.stb_TitleLookup.IsNotNull() ? contact.stb_TitleLookup.Id : new Guid();
            model.Gender = contact.stb_GenderLookup.IsNotNull() ? contact.stb_GenderLookup.Id : new Guid();
            model.Nationality = contact.stb_NationalityLookup.IsNotNull()
                ? contact.stb_NationalityLookup.Id
                : new Guid();
            model.Ethnicity = contact.stb_EthnicityLookup.IsNotNull()
                ? contact.stb_EthnicityLookup.Id
                : new Guid();
            model.DateOfBirth = contact.BirthDate ?? DateTime.MinValue;
            model.ForeignIdType = contact.stb_IdTypeLookup.IsNotNull()
                ? contact.stb_IdTypeLookup.Id
                : new Guid();

            if (contact.stb_ForeignId.IsNotNullOrEmpty())
            {
                model.ForeignIdNumber = contact.stb_ForeignId;
            }

            if (contact.stb_ForeignIDExpiryDate.HasValue)
            {
                model.ForeignIdExpiryDate = contact.stb_ForeignIDExpiryDate ?? DateTime.MinValue;
            }

            if (contact.stb_PassportExpiryDate.HasValue)
            {
                model.PassportExpiryDate = contact.stb_PassportExpiryDate ?? DateTime.MinValue;
            }

            model.IdNumber = contact.GovernmentId;
            model.PassportNumber = contact.stb_PassportNumber;
            model.Language = contact.stb_HomeLanguage_LanguageLookup.IsNotNull()
                ? contact.stb_HomeLanguage_LanguageLookup.Id
                : new Guid();
            model.DietaryRequirements = contact.stb_SpecialDietaryRequirements.IsNotNull()
                ? contact.stb_SpecialDietaryRequirements.Id
                : new Guid();
            model.DietaryRequirementsOther = contact.stb_OtherDietaryRequirements;
            model.Disability = contact.stb_DisabilityLookup.IsNotNull()
                ? contact.stb_DisabilityLookup.Id
                : new Guid();
            model.UsesWheelchair = contact.stb_DoYouUseAWheelchair ?? false;

            #endregion

            #region Contact Information

            model.ContactNumber = contact.Address1_Telephone1;
            model.PostalCountry = contact.stb_address1_CountryLookup.IsNotNull()
                ? contact.stb_address1_CountryLookup.Id
                : new Guid();
            model.SelectedCountry = contact.stb_address1_CountryLookup.IsNotNull()
                ? contact.stb_address1_CountryLookup.Name
                : "";
            //model.Province = contact.stb_address1_AfrigisLookup.IsNotNull()
            //    ? contact.stb_address1_AfrigisLookup.Id
            //    : new Guid();
            
            //Get id of province based on name
            model.Province = CrmActions.GetAfrigisProvinceId(contact.stb_contactprovince);

            model.PostalSuburb = contact.stb_address1_Suburb;
            model.PostalCity = contact.stb_address1_City;
            model.PostalStreet1 = contact.Address1_Line1;
            model.PostalStreet2 = contact.Address1_Line2;
            model.PostalPostalCode = contact.stb_address1_PostalCode;

            if (contact.stb_address1_CountryLookup.IsNotNull())
            {
                if(contact.stb_address1_CountryLookup.Name == "South Africa")
                {
                    if (contact.stb_address1_PostalCode.IsNotNullOrEmpty())
                    {
                        var postalDistributionArea = new PostalDistributionArea();
                        var postalAreas = postalDistributionArea.LoadAreas(model.PostalPostalCode);

                        if (postalAreas.IsNotNull())
                        {
                            model.PostalAfrigis =
                                postalAreas.FirstOrDefault(x => x.Text.Contains(model.PostalPostalCode)).Value.AsGuid();
                        }
                    }
                    model.PostalPostalCode = contact.stb_address1_PostalCode;
                    //model.Province = contact.stb_contactprovince;
                }
                else
                {
                    model.NonSAPostalCode = contact.stb_address1_PostalCode;
                }
            }

          

            model.AssistantSurname = contact.stb_AssistantLastName;
            model.AssistantFirstnames = contact.AssistantName;
            model.AssistantCellphoneNumber = contact.stb_AssistantMobile;
            model.AssistantContactNumber = contact.AssistantPhone;
            model.AssistantEmailAddress = contact.EMailAddress2;

            #endregion

            #region Qualifications

            if (enrolment.IsNotNull())
            {
                model.ApplicantStatus = (ApplicantStatus) enrolment.statuscode.GetValueOrDefault();

                model.UsNumber = enrolment.stb_USNumber;
                model.VoucherStatus = CrmQueries.GetVoucherStatus(enrolment.Id);
                model.HasVoucher = enrolment.stb_VoucherGeneratedTwoOptions ?? false;

                if (enrolment.stb_CurrentlyEmployedTwoOptions.HasValue &&
                    enrolment.stb_CurrentlyEmployedTwoOptions.Value)
                {
                    model.Employed = enrolment.stb_CurrentlyEmployedTwoOptions.Value;
                    model.Industry = enrolment.stb_IndustryLookup.IsNotNull()
                        ? enrolment.stb_IndustryLookup.Id
                        : new Guid();
                    model.WorkArea = enrolment.stb_WorkAreaLookup.IsNotNull()
                        ? enrolment.stb_WorkAreaLookup.Id
                        : new Guid();
                    model.JobTitle = enrolment.stb_JobTitle;
                    model.Employer = enrolment.stb_CurrentEmployer;
                    model.EmployerContactNumber = enrolment.stb_EmployerContactNumber;
                }

                model.QualificationType = enrolment.stb_QualificationTypeLookup.IsNotNull()
                    ? enrolment.stb_QualificationTypeLookup.Id
                    : new Guid();
                model.QualificationTypeOther = enrolment.stb_QualificationTypeOther;
                model.QualificationField = enrolment.stb_FieldofStudyLookup.IsNotNull()
                    ? enrolment.stb_FieldofStudyLookup.Id
                    : new Guid();
                model.QualificationInstitution = enrolment.stb_AcademicInstitution;
                model.YearAchieved = enrolment.stb_YearAchieved?.Year.ToString() ?? string.Empty;

                //applicationContact.IdentificationDocumentUploadKey = contact.IdentificationDocumentUploadKey;
                //applicationContact.QualificationDocumentUploadKey = contact.QualificationDocumentUploadKey;

                #endregion

                #region Marketing

                model.MarketingSource = enrolment.stb_LeadSource.GetValueOrDefault();
                model.MarketingReason = enrolment.stb_MarketingReason.GetValueOrDefault();
                model.MarketingSourceOther = enrolment.stb_LeadSourceOther;
                model.MarketingReasonOther = enrolment.stb_MarketingReasonOther;
            }

            model.UsesTwitter = contact.stb_UseTwitter ?? contact.stb_UseTwitter.GetValueOrDefault();
            model.TwitterUsername = contact.stb_TwitterAccount;
            model.UsesFacebook = contact.stb_UseFacebook ?? contact.stb_UseFacebook.GetValueOrDefault();
            model.UsesLinkedIn = contact.stb_UseLinkedin ?? contact.stb_UseLinkedin.GetValueOrDefault();

            //model.SubscribeToNewsletter = contact.stb_SendNewsletterTwoOptions ??
            //                              contact.stb_SendNewsletterTwoOptions.GetValueOrDefault();

            #endregion

            #region Payment

            model.PaymentResponsibility = enrolment.IsNotNull() && enrolment.stb_PaymentResponsibilityLookup.IsNotNull() ? enrolment.stb_PaymentResponsibilityLookup.Id : Guid.Empty;
            model.Consent = enrolment.stb_CompanyPOPIconsent;
            model.PaymentContactCompanyName = contact.stb_ParentSponsorCompanyName;
            model.PaymentContactNumber = contact.stb_ParentContactNumber;
            model.PaymentContactEmail = contact.stb_ParentEmailAddress;
            model.PaymentContactName = contact.stb_ParentFirstName;
            model.PaymentContactSurname = contact.stb_ParentLastName;
            model.PaymentPostalStreet1 = contact.stb_ParentStreet1;
            model.PaymentPostalStreet2 = contact.stb_ParentStreet2;
            model.PaymentPostalCode = contact.stb_ParentPostalCode;
            model.PaymentPostalCity = contact.stb_ParentCityTown;
            model.PaymentPostalSuburb = contact.stb_ParentSuburb;           
            model.PaymentPostalCountry = contact.stb_ParentCountryLookup.IsNotNull() ? contact.stb_ParentCountryLookup.Id :  new Guid(); 
           // model.PaymentDebtorCode = enrolment.IsNotNull() ? enrolment.stb_PayeeDebtorNumber : null;
            


            if (contact.stb_ParentPostalCode.IsNotNullOrEmpty())
            {
                var PaymentPostalDistributionArea = new PostalDistributionArea();
                var PaymentPostalAreas = PaymentPostalDistributionArea.LoadAreas(model.PaymentPostalCode);

                if (PaymentPostalAreas.IsNotNull())
                {
                    model.PaymentPostalAfrigis =
                        PaymentPostalAreas.FirstOrDefault(x => x.Text.Contains(model.PaymentPostalCode)).Value.AsGuid();
                }

            }
            #endregion

            return model;
        }
    }
}
