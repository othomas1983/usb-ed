﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ShortCourseApplicationForm.Domain.Tasks
{
    public class RemoveSpecialCharacters
    { 
        public static string RemoveInvalidCharactersFromFileName(string fileName)
        {
            var pattern = "[\\$^~@#%&*{}()/:<>?|\"-]";
            //var oldFileName = "Policy^Numb|er@:<>?/.pdf";

            Regex regEx = new Regex(pattern);
            var newFileName = Regex.Replace(regEx.Replace(fileName, ""), @"\s+", " ");

            return newFileName;
        }
    }
}
