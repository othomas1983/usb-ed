﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using ShortCourseApplicationForm.Domain.Controller;
using ShortCourseApplicationForm.Domain.Models.ShortCourseApplicationForm;

namespace ShortCourseApplicationForm.Domain.Validation
{
    #region InitialsAndFirstNamesValidation

    public sealed class InitialsAndFirstNamesValidationAttribute : ValidationAttribute
    {
        public override string FormatErrorMessage(string message)
        {
            return string.Format(message);
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var context = validationContext.ObjectInstance as ApplicationForm;

            if (context.FirstNames.IsNotNull() && context.Initials.IsNotNull())
            {
                if (context.FirstNames.Trim().Length == 1 && context.Initials.Trim().Length == 1)
                {
                    return new ValidationResult(
                        FormatErrorMessage("Name and initials cannot be a single character"));
                }

                var firstNames = context.FirstNames.Split(' ').Where(s => s.Trim(' ').IsNotNull()).ToArray();
                
                if (firstNames.Length != context.Initials.Length)
                {
                    return new ValidationResult(
                        FormatErrorMessage("Name(s) do not match initials"));
                }
                else
                {
                    var initials = new List<string>();

                    for (var i = 0; i < context.Initials.Length; i++)
                    {
                        var currentInitial = context.Initials[i].ToString();
                        if (!string.IsNullOrWhiteSpace(currentInitial))
                        {
                            initials.Add(currentInitial);
                        }
                    }

                    if (firstNames.Length != initials.Count)
                    {
                        return new ValidationResult(
                            FormatErrorMessage("Name(s) do not match initials"));
                    }
                    else
                    {
                        for (var i = 0; i < initials.Count; i++)
                        {
                            if (initials[i].ToUpperInvariant() != firstNames[i][0].ToString().ToUpperInvariant())
                            {
                                return new ValidationResult(
                                    FormatErrorMessage("Name(s) do not match initials"));
                            }
                        }
                    }
                }
            }
            return ValidationResult.Success;
        }
    }

    #endregion

    #region PaymentDetailsValidation

    public sealed class PaymentResponsibilityValidation : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var context = validationContext.ObjectInstance as ApplicationForm;

            if (ApplicationFormInfo.Info.IsNull() || ApplicationFormInfo.Info.PaymentResponsibility.IsNull())
            {
                using (WebClient client = new WebClient())
                {
                    ApplicationFormInfo.Info = new ApplicationFormInfo();
                    ApplicationFormInfo.Info.LoadInfoMembers(context, context.ShortCourseId, context.Offering.AsString());
                }
            }

            var paymentResponsibility =  ApplicationFormInfo.Info.PaymentResponsibility.FirstOrDefault(p => p.Value == context.PaymentResponsibility.AsString());
            
            if (paymentResponsibility.IsNull())
            {
                return new ValidationResult(
                    FormatErrorMessage("Please select a party responsible for payment."));
            }
            return ValidationResult.Success;
        }

        public override string FormatErrorMessage(string message)
        {
            return string.Format(message);
        }
    }


    public sealed class PaymentDetailsValidation : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var context = validationContext.ObjectInstance as ApplicationForm;

            if (ApplicationFormInfo.Info.IsNull() || ApplicationFormInfo.Info.PaymentResponsibility.IsNull())
            {
                using (WebClient client = new WebClient())
                {
                    ApplicationFormInfo.Info = new ApplicationFormInfo();
                    ApplicationFormInfo.Info.LoadInfoMembers(context, context.ShortCourseId, context.Offering.AsString());
                }
            }

            var paymentResponsibility = ApplicationFormInfo.Info.PaymentResponsibility.FirstOrDefault(p => p.Value == context.PaymentResponsibility.AsString());
                        
            if (paymentResponsibility.IsNotNull())
            {
                if ((paymentResponsibility.Text == "Company" || paymentResponsibility.Text == "Department" || paymentResponsibility.Text == "Sponsored") && value.IsNull())
                {
                    return new ValidationResult(
                        FormatErrorMessage(validationContext.DisplayName));
                }
            }
            return ValidationResult.Success;
        }

        public override string FormatErrorMessage(string name)
        {
            return string.Format("{0} is a required field",name);
        }
    }

    #endregion

    #region EthnicityValidation

    public sealed class EthnicityValidation : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var context = validationContext.ObjectInstance as ApplicationForm;

            if (ApplicationFormInfo.Info.IsNull() || ApplicationFormInfo.Info.Nationalities.IsNull())
            {
                using (WebClient client = new WebClient())
                {
                    ApplicationFormInfo.Info = new ApplicationFormInfo();
                    ApplicationFormInfo.Info.LoadInfoMembers(context, context.ShortCourseId, context.Offering.AsString());
                }
            }

            var country = ApplicationFormInfo.Info.Nationalities.FirstOrDefault(c => c.Value == context.Nationality.AsString());            

            if (country.IsNotNull())
            {
                if (country.Text == "South Africa")
                {
                    if (value.IsNull())
                    {
                        return new ValidationResult(FormatErrorMessage(validationContext.DisplayName)); 
                    }
                }
            }

            return ValidationResult.Success;
        }

        public override string FormatErrorMessage(string name)
        {
            return "{0} is a required field".FormatInvariantCulture(name);
        }
    }

    #endregion

    #region IdNumberValidation

    public sealed class IdNumberValidation : ValidationAttribute
    {       
        private const string IdPattern = @"(?<Year>[0-9][0-9])(?<Month>([0][1-9])|([1][0-2]))(?<Day>([0-2][0-9])|([3][0-1]))(?<Gender>[0-9])(?<Series>[0-9]{3})(?<Citizenship>[0-9])(?<Uniform>[0-9])(?<Control>[0-9])";


        #region Enums

        private enum PersonGender
        {
            Female = 0,
            Male = 5
        }

        public enum GenderType
        {
            Male,
            Female,
            NonBinary
        }

        #endregion

        #region Properties

        private Match IdNumberMatch { get; set; }

        private GenderType? Gender { get; set; }

        private DateTime? DateOfBirth { get; set; }

        #endregion

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {            
            var context = validationContext.ObjectInstance as ApplicationForm;
            var controller = new ShortCourseApplicationFormsApiController();

            if (ApplicationFormInfo.Info.IsNull() || ApplicationFormInfo.Info.Nationalities.IsNull())
            {
                using (WebClient client = new WebClient())
                {
                    ApplicationFormInfo.Info = new ApplicationFormInfo();
                    ApplicationFormInfo.Info.LoadInfoMembers(context, context.ShortCourseId, context.Offering.AsString());
                }
            }

            var country = ApplicationFormInfo.Info.Nationalities.FirstOrDefault(c => c.Value == context.Nationality.AsString());

            if (country.IsNotNull())
            {
                if (country.Text == "South Africa")
                {
                    if (value.IsNotNull())
                    {
                        var idNumber = value.ToString();
                        var dateOfBirth = context.DateOfBirth;

                            controller.Genders(ApplicationFormInfo.Info);

                        var selectedGender = ApplicationFormInfo.Info.Genders.FirstOrDefault(g => g.Value == context.Gender.AsString());
                        
                        var gender = new object();
                        if (selectedGender.IsNotNull())
                        {
                            var trimmedGender = selectedGender.Text.Replace(" ", "");

                            gender = Enum.Parse(typeof(GenderType), trimmedGender);
                            Gender = (GenderType)Enum.Parse(typeof(GenderType), gender.ToString());
                        }

                        if (idNumber.IsNotNull() && idNumber.Length != 13)
                        {
                            FormatErrorMessage("Id number must be 13 characers long.");
                        }

                        IdNumberMatch = Regex.Match(idNumber, IdPattern, RegexOptions.Compiled | RegexOptions.Singleline);

                        DateOfBirth = dateOfBirth;

                        if (!IsValid())
                        {
                            return new ValidationResult(FormatErrorMessage("Id number format is invalid"));
                        }
                        else if (dateOfBirth.IsNotNull()
                            && !IsValidForDateOfBirth())
                        {
                            return new ValidationResult(FormatErrorMessage("Date of birth, '{0:dd/MM/yyyy}', does not match with Id Number '{1}'".FormatInvariantCulture(dateOfBirth, idNumber)));
                        }
                        else if (Gender.HasValue
                            && !IsValidForGender())
                        {
                            return new ValidationResult(FormatErrorMessage("'{0}' gender does not match with Id Number '{0}'".FormatInvariantCulture(gender.ToString(), idNumber)));
                        }
                    }
                    else
                    {
                        return new ValidationResult(FormatErrorMessage("Id Number field is required"));
                    }
                }
            }
            
            return ValidationResult.Success;
        }

        public override string FormatErrorMessage(string errorMessage)
        {
            return string.Format(errorMessage);
        }

        #region Private Methods

        private bool IsValid()
        {
            if (IdNumberMatch.Success)
            {
                // Calculate total A by adding the figures in the odd positions i.e. the first, third, fifth,
                // seventh, ninth and eleventh digits.
                int a = int.Parse(IdNumberMatch.Value.Substring(0, 1)) +
                    int.Parse(IdNumberMatch.Value.Substring(2, 1)) +
                    int.Parse(IdNumberMatch.Value.Substring(4, 1)) +
                    int.Parse(IdNumberMatch.Value.Substring(6, 1)) +
                    int.Parse(IdNumberMatch.Value.Substring(8, 1)) +
                    int.Parse(IdNumberMatch.Value.Substring(10, 1));

                // Calculate total B by taking the even figures of the number as a whole number, and then
                // multiplying that number by 2, and then add the individual figures together.
                int b = int.Parse(IdNumberMatch.Value.Substring(1, 1) +
                    IdNumberMatch.Value.Substring(3, 1) +
                    IdNumberMatch.Value.Substring(5, 1) +
                    IdNumberMatch.Value.Substring(7, 1) +
                    IdNumberMatch.Value.Substring(9, 1) +
                    IdNumberMatch.Value.Substring(11, 1));

                b *= 2;
                string bString = b.ToString();
                b = 0;
                for (int index = 0; index < bString.Length; index++)
                {
                    b += int.Parse(bString.Substring(index, 1));
                }

                // Calculate total C by adding total A to total B.
                int c = a + b;

                // The control-figure can now be determined by subtracting the ones in figure C from 10.
                string cString = c.ToString();
                cString = cString.Substring(cString.Length - 1, 1);
                int control = 0;

                // Where the total C is a multiple of 10, the control figure will be 0.
                if (cString != "0")
                {
                    control = 10 - int.Parse(cString.Substring(cString.Length - 1, 1));
                }

                if (IdNumberMatch.Groups["Control"].Value == control.ToString())
                {
                    return true;
                }
            }

            return false;
        }

        private bool IsValidForDateOfBirth()
        {
            int year = Convert.ToInt16(IdNumberMatch.Groups["Year"].Value);
                        
            int currentCentury = Convert.ToInt16(DateTime.Now.Year.ToString().Substring(0, 2) + "00");
            int lastCentury = currentCentury - 100;
            int currentYear = Convert.ToInt16(DateTime.Now.Year.ToString().Substring(2, 2));

            // If the year is after or at the current YY, then add last century to it, otherwise add
            // this century.
            // TODO: YY -> YYYY logic needs thinking about
            if (year > currentYear)
            {
                year += lastCentury;
            }
            else
            {
                year += currentCentury;
            }

            var date = new DateTime(year, Convert.ToInt16(IdNumberMatch.Groups["Month"].Value), Convert.ToInt16(IdNumberMatch.Groups["Day"].Value));

            return DateOfBirth.Value.Date == date;
        }

        public bool IsValidForGender()
        {
            int gender = Convert.ToInt16(IdNumberMatch.Groups["Gender"].Value);

            if (gender < (int)PersonGender.Male)
            {
                return Gender.Value == GenderType.Female;
            }

            return Gender.Value == GenderType.Male;
        }

        #endregion
    }

    #endregion

    #region PostalAddressValidation

    public sealed class SAPaymentPostalAddressValidation : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var context = validationContext.ObjectInstance as ApplicationForm;
            if (!string.IsNullOrWhiteSpace(context.PaymentPostalCountry.AsString()))
            {
                if (ApplicationFormInfo.Info.IsNull() || ApplicationFormInfo.Info.AddressCountries.IsNull())
                {
                    using (WebClient client = new WebClient())
                    {
                        ApplicationFormInfo.Info = new ApplicationFormInfo();
                        ApplicationFormInfo.Info.LoadInfoMembers(context, context.ShortCourseId, context.Offering.AsString());
                    }
                }

                var country = ApplicationFormInfo.Info.AddressCountries.FirstOrDefault(c => c.Value == context.PaymentPostalCountry.AsString());

                if (country.IsNotNull())
                {
                    if (country.Text == "South Africa")
                    {
                        if (value.IsNull() || string.IsNullOrWhiteSpace(value.ToString()))
                        {
                            return new ValidationResult(
                                FormatErrorMessage(validationContext.DisplayName));
                        }
                    }
                }
            }
            return ValidationResult.Success;
        }

        public override string FormatErrorMessage(string name)
        {
            return "{0} is a required field".FormatInvariantCulture(name);
        }
    }

    public sealed class PostalAddressValidation : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var context = validationContext.ObjectInstance as ApplicationForm;
            if (!string.IsNullOrWhiteSpace(context.PostalCountry.AsString()))
            {
                if (ApplicationFormInfo.Info.IsNull() || ApplicationFormInfo.Info.AddressCountries.IsNull())
                {
                    using (WebClient client = new WebClient())
                    {
                        ApplicationFormInfo.Info = new ApplicationFormInfo();
                        ApplicationFormInfo.Info.LoadInfoMembers(context, context.ShortCourseId, context.Offering.AsString());
                    }
                }

                var country = ApplicationFormInfo.Info.AddressCountries.Where(c => c.Value == context.PostalCountry.AsString());

                if (country.Any())
                {
                    if (country.FirstOrDefault().Text == "South Africa" && validationContext.DisplayName == "PostalStreet1")
                    {
                        if (value.IsNull() || string.IsNullOrWhiteSpace(value.ToString()))
                        {
                            return new ValidationResult(
                                FormatErrorMessage(validationContext.DisplayName));
                        }
                    }
                }
            }
            return ValidationResult.Success;
        }

        public override string FormatErrorMessage(string name)
        {
            return "{0} is a required field".FormatInvariantCulture(name);
        }
    }

    public sealed class SAPostalAddressValidation : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var context = validationContext.ObjectInstance as ApplicationForm;
            if (!string.IsNullOrWhiteSpace(context.PostalCountry.AsString()))
            {
                if (ApplicationFormInfo.Info.IsNull() || ApplicationFormInfo.Info.AddressCountries.IsNull())
                {
                    using (WebClient client = new WebClient())
                    {
                        ApplicationFormInfo.Info = new ApplicationFormInfo();
                        ApplicationFormInfo.Info.LoadInfoMembers(context, context.ShortCourseId, context.Offering.AsString());
                    }
                }

                var country = ApplicationFormInfo.Info.AddressCountries.FirstOrDefault(c => c.Value == context.PostalCountry.AsString());

                if (country.IsNotNull())
                {
                    if (country.Text == "South Africa")
                    {
                        if (value.IsNull() || string.IsNullOrWhiteSpace(value.ToString()))
                        {
                            return new ValidationResult(
                                FormatErrorMessage(validationContext.DisplayName));
                        }
                    }
                }
            }
            return ValidationResult.Success;
        }

        public override string FormatErrorMessage(string name)
        {
            return "{0} is a required field".FormatInvariantCulture(name);
        }
    }

    public sealed class NonSAPostalAddressValidation : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var context = validationContext.ObjectInstance as ApplicationForm;

            if (!string.IsNullOrWhiteSpace(context.PostalCountry.AsString()))
            {
                if (ApplicationFormInfo.Info.IsNull() || ApplicationFormInfo.Info.AddressCountries.IsNull())
                {
                    using (WebClient client = new WebClient())
                    {
                        ApplicationFormInfo.Info = new ApplicationFormInfo();
                        ApplicationFormInfo.Info.LoadInfoMembers(context, context.ShortCourseId, context.Offering.AsString());
                    }
                }

                var country = ApplicationFormInfo.Info.AddressCountries.FirstOrDefault(c => c.Value == context.PostalCountry.AsString());

                if (country.IsNotNull())
                {
                    if (country.Text != "South Africa")
                    {
                        if (value.IsNull() || string.IsNullOrWhiteSpace(value.ToString()))
                        {
                            return new ValidationResult(
                                FormatErrorMessage(validationContext.DisplayName));
                        }
                    }
                }
            }
            return ValidationResult.Success;
        }

        public override string FormatErrorMessage(string name)
        {
            return "{0} is a required field".FormatInvariantCulture(name);
        }
    }

    #endregion

    #region AcademicQualificationValidation
    
    //public sealed class QualificationsValidation : ValidationAttribute
    //{
    //    protected override ValidationResult IsValid(object value, ValidationContext validationContext)
    //    {
    //        var context = validationContext.ObjectInstance as ApplicationForm;

    //        var hasAcadamicHistory = context.qu.Any(a => a.Field.IsNotNull()
    //            || a.Institution.IsNotNull()
    //            || a.Qualification.IsNotNull()
    //            || a.YearAchieved != 0);

    //        if (!hasAcadamicHistory)
    //        {
    //            return new ValidationResult(
    //                    FormatErrorMessage(null));                
    //        }
    //        return ValidationResult.Success;
    //    }

    //    public override string FormatErrorMessage(string name)
    //    {
    //        return "Please fill in your Academic Qualifications".FormatInvariantCulture();
    //    }
    //}

    #endregion

    #region WorkExperienceValidation

    public sealed class WorkExperienceValidation : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var context = validationContext.ObjectInstance as ApplicationForm;

            if (context.Employed)
            {
                if (context.Industry.IsNull())
                {
                    return new ValidationResult(
                        FormatErrorMessage(validationContext.DisplayName));
                }

                if (context.WorkArea.IsNull())
                {
                    return new ValidationResult(
                        FormatErrorMessage(validationContext.DisplayName));
                }
            }
            return ValidationResult.Success;
        }

        public override string FormatErrorMessage(string name)
        {
            return "{0} is a required field".FormatInvariantCulture(name);
        }
    }

    #endregion

    #region DietaryValidation

    public sealed class DietaryValidation : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var context = validationContext.ObjectInstance as ApplicationForm;
            if (string.IsNullOrWhiteSpace(context.DietaryRequirements.AsString()) && string.IsNullOrWhiteSpace(context.DietaryRequirementsOther))
            {
                return new ValidationResult(
                    FormatErrorMessage("Dietary Requirement"));
            }

            return ValidationResult.Success;
        }

        public override string FormatErrorMessage(string name)
        {
            return "Please supply your {0} ".FormatInvariantCulture(name);
        }
    }
    
    #endregion
       
    #region EmailValidation

    public sealed class ValidateEmailAddress : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value.IsNotNull())
            {
                var emailExpression = @"^(\w+([-+.']\w+)*)@([\w-]+\.)+[\w-]+$".AsString();
                var valid = Regex.Match(value.ToString().ToLowerInvariant(), emailExpression).Success;

                if (!valid)
                {
                    return new ValidationResult(
                        FormatErrorMessage(value.ToString()));
                }
            }

            return ValidationResult.Success;
        }

        public override string FormatErrorMessage(string name)
        {
            return "{0} is an invalid email address".FormatInvariantCulture(name);
        }
    }

    public sealed class ValidatePaymentDetailsEmailAddress : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value.IsNotNull())
            {
                var context = validationContext.ObjectInstance as ApplicationForm;
                if (ApplicationFormInfo.Info.IsNull() || ApplicationFormInfo.Info.PaymentResponsibility.IsNull())
                {
                    using (WebClient client = new WebClient())
                    {
                        ApplicationFormInfo.Info = new ApplicationFormInfo();
                        ApplicationFormInfo.Info.LoadInfoMembers(context, context.ShortCourseId, context.Offering.AsString());
                    }
                }

                var paymentResponsibility = ApplicationFormInfo.Info.PaymentResponsibility.FirstOrDefault(p => p.Value == context.PaymentResponsibility.AsString());
                if (paymentResponsibility.IsNotNull())
                {
                    if ((paymentResponsibility.Text == "Company" || paymentResponsibility.Text == "Department"))
                    {
                        var emailExpression = @"^(\w+([-+.']\w+)*)@([\w-]+\.)+[\w-]+$";
                        
                        var valid = Regex.Match(value.AsString().Trim(), emailExpression).Success;

                        if (!valid)
                        {
                            return new ValidationResult(
                                FormatErrorMessage(value.ToString()));
                        }
                    }
                }
            }
            return ValidationResult.Success;           
        }

        public override string FormatErrorMessage(string name)
        {
            return "{0} is an invalid email address".FormatInvariantCulture(name);
        }
    }

    #endregion
}