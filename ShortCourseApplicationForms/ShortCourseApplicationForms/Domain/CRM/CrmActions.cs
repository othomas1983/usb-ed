﻿using System;
using System.Linq;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk;
using ShortCourseApplicationForm.Domain.Models.Enums;
using ShortCourseApplicationForm.Domain.Models.ShortCourseApplicationForm;
using ShortCourseApplicationForm.Domain.Properties;
using ShortCourseApplicationForm.Domain.Sharepoint;
using StellenboschUniversity.UsbEd.Integration.Crm;
using System.Configuration;

namespace ShortCourseApplicationForm.Domain.CRM
{
    public static class CrmActions
    {

        //Webservice integration for Partners 
        public static void CreateOrUpdateContact(Guid ShortCourseOffering,string EmailAdress, string LastName,string FirstNames, 
            string NameOnCertificate,string Initials, Guid Title,Guid Gender, Guid Nationality,Guid Ethnicity, DateTime DateOfBirth,
            string ForeignIdNumber,Guid ForeignIdType, DateTime? ForeignIdExpiryDate,string PassportNumber, DateTime? PassportExpiryDate,
            string IdNumber, Guid DietaryRequirements, string DietaryRequirementsOther,Guid Language,Guid Disability, bool DoYouUseAWheelchair,
            Guid? contactId, Guid? enrolmentId, string UsNumber, string Environment,Guid PostalCountry,Guid? Province,string PostalSuburb,
            string PostalCity, string PostalStreet1, string PostalStreet2,string PostalPostalCode,string CellphoneNumber)
        {
            var contact = new Contact();
            var enrolment = new stb_enrollment();

            contact.Id = contactId.Value;
            contact.ContactId = contact.Id;
            contact.stb_USNumber = UsNumber;

            enrolment.Id = enrolmentId.Value;
            enrolment.stb_enrollmentId = enrolment.Id;
            enrolment.stb_USNumber = UsNumber;


            var offeringId = ShortCourseOffering;

            enrolment.stb_ShortCourseOfferingLookup =
                            new CrmEntityReference(stb_shortcourseoffering.EntityLogicalName,offeringId);


            enrolment.stb_StudentContact_ContactLookup = new CrmEntityReference(Contact.EntityLogicalName,
                contactId.GetValueOrDefault());

            string ApplicationFormsBaseUrl =  Settings.Default.ApplicationFormsBaseUrl;

            enrolment.stb_AppFormURL = ApplicationFormsBaseUrl + "?oID=" +
                                             enrolment.stb_ShortCourseOfferingLookup.Id;
          

            contact.EMailAddress1 = EmailAdress;
            contact.LastName = LastName;
            contact.FirstName = FirstNames;
            enrolment.stb_nameoncertificate = $"{FirstNames} {LastName}";
            contact.stb_Initials = Initials;
            contact.stb_TitleLookup = new CrmEntityReference(stb_title.EntityLogicalName, Title);
            contact.stb_GenderLookup = new CrmEntityReference(stb_gender.EntityLogicalName,
            Gender);
            contact.stb_NationalityLookup = new CrmEntityReference(stb_nationality.EntityLogicalName,
            Nationality);
            contact.stb_EthnicityLookup = new CrmEntityReference(stb_ethnicity.EntityLogicalName,
            Ethnicity);
            contact.BirthDate = DateOfBirth;

            contact.stb_DOB = DateOfBirth.ToString("yyyyMMdd");

            contact.stb_IdTypeLookup = new CrmEntityReference(stb_Idtype.EntityLogicalName,
                ForeignIdType);

            if (ForeignIdNumber.IsNotNullOrEmpty())
            {
                contact.stb_ForeignId = ForeignIdNumber;
            }

            if (!ForeignIdExpiryDate.ToString().Contains("0001"))
            {
                contact.stb_ForeignIDExpiryDate = ForeignIdExpiryDate;
            }

            if (!PassportExpiryDate.ToString().Contains("0001"))
            {
                contact.stb_PassportExpiryDate = PassportExpiryDate;
            }

            contact.GovernmentId = IdNumber;

            if (Nationality.IsNotNull())
            {
                if (IdNumber.IsNotNullOrEmpty())
                {
                    var idType = Guid.Parse(CrmQueries.GetSaIdType(Nationality));
                    contact.stb_IdTypeLookup = new CrmEntityReference(stb_Idtype.EntityLogicalName, idType);
                }
                else
                {
                    ForeignIdType = Guid.Parse(CrmQueries.GetSaIdType(Nationality));
                    contact.stb_IdTypeLookup = new CrmEntityReference(stb_Idtype.EntityLogicalName,
                        ForeignIdType);
                }
            }

            contact.stb_PassportNumber = PassportNumber;
            contact.stb_HomeLanguage_LanguageLookup = new CrmEntityReference(stb_language.EntityLogicalName,
                Language);

            if (DietaryRequirements.IsNotNull())
            {
                contact.stb_SpecialDietaryRequirements =
                    new CrmEntityReference(stb_specialdietaryrequirements.EntityLogicalName,
                        DietaryRequirements);
                contact.stb_OtherDietaryRequirements = DietaryRequirementsOther;
            }

            contact.stb_DisabilityLookup = new CrmEntityReference(stb_disability.EntityLogicalName,
                Disability);
            contact.stb_DoYouUseAWheelchair = DoYouUseAWheelchair;

            //Contact Adress  
            contact.Address1_Telephone1 = CellphoneNumber;
            contact.stb_address1_CountryLookup = new CrmEntityReference(stb_country.EntityLogicalName,
                PostalCountry);

            if (Province != Guid.Parse("{00000000-0000-0000-0000-000000000000}"))
            {
                contact.stb_address1_AfrigisLookup = new CrmEntityReference(stb_afrigis.EntityLogicalName,
                    Province.GetValueOrDefault());
            }
            
            contact.stb_address1_Suburb = PostalSuburb;
            contact.stb_address1_City = PostalCity;
            contact.Address1_Line1 = PostalStreet1;
            contact.Address1_Line2 = PostalStreet2;
            contact.stb_address1_PostalCode = PostalPostalCode;

            
            enrolment.stb_SharePointLocation =
                       $"{Settings.Default.SharepointUrl}/{Settings.Default.SharepointLibrary}/{enrolment.stb_enrollmentId}";

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                try
                {
                    crmServiceContext.Update(enrolment);
                    crmServiceContext.Update(contact);
                }
                catch (Exception e)
                {
                    
                }
                finally
                {
                    crmServiceContext.Dispose();
                }
             
                if (ShortCourseOffering != Guid.Empty)
                {
                    offeringId = ShortCourseOffering;
                       

                    Guid offeringOwner =
                        CrmQueries.GetCourseOfferingOwner(offeringId);
                    CrmActions.SetOwnershipOfEnrollment(enrolment.Id, offeringOwner);
                }
            }

        }

            public static ApplicationForm CreateOrUpdateContact(ValidateProgress progressStep,
            ApplicationForm contactDetails, Guid? contactId, Guid? enrolmentId)
        {
            var contact = new Contact();
            var enrolment = new stb_enrollment();

            contact.Id = contactId.Value;
            contact.ContactId = contact.Id;
            contact.stb_USNumber = contactDetails.UsNumber;

            enrolment.Id = enrolmentId.Value;
            enrolment.stb_enrollmentId = enrolment.Id;
            enrolment.stb_USNumber = contactDetails.UsNumber;

            if (contactDetails.ShortCourseOfferingId.IsNotNullOrEmpty() ||
                contactDetails.Offering.IsNotNull() && contactDetails.Offering != Guid.Empty)
            {
                var offeringId = contactDetails.ShortCourseOfferingId.IsNotNullOrEmpty()
                    ? contactDetails.ShortCourseOfferingId
                    : contactDetails.Offering.ToString();

                enrolment.stb_ShortCourseOfferingLookup =
                    new CrmEntityReference(stb_shortcourseoffering.EntityLogicalName, Guid.Parse(offeringId));
            }

            enrolment.stb_StudentContact_ContactLookup = new CrmEntityReference(Contact.EntityLogicalName,
                contactId.GetValueOrDefault());


            switch (progressStep)
            {
                case ValidateProgress.PersonalInformation:

                    #region Personal Information
                    var ApplicationFormsBaseUrl = Settings.Default.ApplicationFormsBaseUrl;
                    enrolment.stb_AppFormURL = ApplicationFormsBaseUrl + "?oID=" +
                                               enrolment.stb_ShortCourseOfferingLookup.Id;

                    contact.EMailAddress1 = contactDetails.Email.IsNotNullOrEmpty()
                        ? contactDetails.Email
                        : contactDetails.RegisteredEmail;
                    contact.LastName = contactDetails.Surname;
                    contact.FirstName = contactDetails.FirstNames;
                    enrolment.stb_NameCalledBy = contactDetails.NameCalledBy;
                    enrolment.stb_nameoncertificate = $"{contactDetails.FirstNames} {contactDetails.Surname}";
                    contact.stb_Initials = contactDetails.Initials;
                    contact.stb_TitleLookup = new CrmEntityReference(stb_title.EntityLogicalName, contactDetails.Title);
                    contact.stb_GenderLookup = new CrmEntityReference(stb_gender.EntityLogicalName,
                        contactDetails.Gender);
                    contact.stb_NationalityLookup = new CrmEntityReference(stb_nationality.EntityLogicalName,
                        contactDetails.Nationality);
                    contact.stb_EthnicityLookup = new CrmEntityReference(stb_ethnicity.EntityLogicalName,
                        contactDetails.Ethnicity.GetValueOrDefault());
                    contact.BirthDate = contactDetails.DateOfBirth;

                    contact.stb_DOB = contactDetails.DateOfBirth.ToString("yyyyMMdd");

                    //contact.stb_IdTypeLookup = new CrmEntityReference(stb_Idtype.EntityLogicalName,
                    //    contactDetails.ForeignIdType.GetValueOrDefault());

                    if (contactDetails.ForeignIdNumber.IsNotNullOrEmpty())
                    {
                        contact.stb_ForeignId = contactDetails.ForeignIdNumber;
                    }

                    if (contactDetails.ForeignIdExpiryDate.HasValue)
                    {
                        contact.stb_ForeignIDExpiryDate = contactDetails.ForeignIdExpiryDate;
                    }

                    if (contactDetails.PassportExpiryDate.HasValue)
                    {
                        contact.stb_PassportExpiryDate = contactDetails.PassportExpiryDate;
                    }

                    contact.GovernmentId = contactDetails.IdNumber;

                    if (contactDetails.Nationality.IsNotNull() && contactDetails.Nationality != Guid.Empty)
                    {
                        if (contactDetails.IdNumber.IsNotNullOrEmpty())
                        {
                            var idType = Guid.Parse(CrmQueries.GetSaIdType(contactDetails.Nationality));
                            contact.stb_IdTypeLookup = new CrmEntityReference(stb_Idtype.EntityLogicalName, idType);
                        }
                        else
                        {
                            //contactDetails.ForeignIdType = Guid.Parse(CrmQueries.GetSaIdType(contactDetails.Nationality));
                            contact.stb_IdTypeLookup = new CrmEntityReference(stb_Idtype.EntityLogicalName,
                                contactDetails.ForeignIdType.GetValueOrDefault());
                        }
                    }

                    contact.stb_PassportNumber = contactDetails.PassportNumber;
                    contact.stb_HomeLanguage_LanguageLookup = new CrmEntityReference(stb_language.EntityLogicalName,
                        contactDetails.Language);

                    if (contactDetails.DietaryRequirements.IsNotNull() &&
                        contactDetails.DietaryRequirements != Guid.Empty)
                    {
                        contact.stb_SpecialDietaryRequirements =
                            new CrmEntityReference(stb_specialdietaryrequirements.EntityLogicalName,
                                contactDetails.DietaryRequirements.GetValueOrDefault());
                        contact.stb_OtherDietaryRequirements = contactDetails.DietaryRequirementsOther;
                    }

                    contact.stb_DisabilityLookup = new CrmEntityReference(stb_disability.EntityLogicalName,
                        contactDetails.Disability.GetValueOrDefault());
                    contact.stb_DoYouUseAWheelchair = contactDetails.UsesWheelchair;

                    if (contactDetails.AccountGuid.IsNotNull() && contactDetails.AccountGuid != Guid.Empty &&
                        contactDetails.QualifyForVoucher)
                    {
                        VoucherStatus voucherStatus = CrmQueries.GetVoucherStatus(enrolmentId);

                        if (voucherStatus == VoucherStatus.Redeemable &&
                            (contactDetails.ShortCourseOfferingType == ShortCourseOfferingType.Partnership ||
                             contactDetails.ShortCourseOfferingType == ShortCourseOfferingType.TEL))
                        {
                            if (!contactDetails.HasVoucher)
                            {
                                CreateOrUpdateEnrollmentVoucher(contactDetails, enrolmentId);

                                //if (contactDetails.IsBasilRead)
                                //{
                                //    UpdateEnrollmentVoucherTotals(Guid.Parse(contactDetails.ShortCourseOfferingId));
                                //}

                                enrolment.stb_VoucherGeneratedTwoOptions = true;

                                CrmQueries.SetDelegateStatus(contactDetails.AccountGuid.GetValueOrDefault());
                            }
                        }
                        //else if (contactDetails.HasUploadedAcceptanceLetter)
                        //{
                        //    UpdateEnrollmentVoucherTotals(Guid.Parse(contactDetails.ShortCourseOfferingId));
                        //}
                    }

                    #endregion

                    break;
                case ValidateProgress.ContactInformation:

                    #region Contact Information

                    contact.Address1_Telephone1 = contactDetails.ContactNumber;
                    contact.stb_address1_CountryLookup = new CrmEntityReference(stb_country.EntityLogicalName,
                        contactDetails.PostalCountry);
                  
                  
                     if (contactDetails.PostalCountry == new Guid("ff24b8f9-0736-e511-80c8-005056b8008e"))
                    {                       
                        var afrigisVal = contactDetails.PostalAfrigis.ToString();                       

                        contact.stb_address1_AfrigisLookup = new CrmEntityReference(stb_afrigis.EntityLogicalName,
                            new Guid(afrigisVal));
                        
                    }
                     else
                    {//todo set afrigis lookup to clear
                        //contact.stb_address1_AfrigisLookup.Id = 
                    }

                    if (contact.stb_address1_CountryLookup.Id == new Guid("ff24b8f9-0736-e511-80c8-005056b8008e"))
                    {
                        var provinceVal = contactDetails.Province.ToString();
                        contact.stb_address1_PostalCode = contactDetails.PostalPostalCode;

                        //Lookup province value based on guid in stb_province afrigis
                        contact.stb_contactprovince= GetAfrigisProvinceName(new Guid(provinceVal));                      

                    }
                    else
                    {
                        contact.stb_address1_PostalCode = contactDetails.NonSAPostalCode;
                        contact.stb_contactprovince = "";
                    }

                    contact.stb_address1_Suburb = contactDetails.PostalSuburb;
                    contact.stb_address1_City = contactDetails.PostalCity;
                    contact.Address1_Line1 = contactDetails.PostalStreet1;
                    contact.Address1_Line2 = contactDetails.PostalStreet2;

                    // PA
                    if (contactDetails.HasPAContactDetails())
                    {
                        contact.stb_AssistantRequiredTwoOptions = true;

                        contact.AssistantName = contactDetails.AssistantFirstnames;
                        contact.stb_AssistantLastName = contactDetails.AssistantSurname;
                        contact.stb_AssistantMobile = contactDetails.AssistantCellphoneNumber;
                        contact.AssistantPhone = contactDetails.AssistantContactNumber;
                        contact.EMailAddress2 = contactDetails.AssistantEmailAddress;
                        enrolment.stb_EmailAddress2 = contactDetails.AssistantEmailAddress;
                    }

                    #endregion

                    break;
                case ValidateProgress.Qualifications:

                    #region Qualifications

                    contactDetails.HasUploadedRequiredDocuments =
                        SharepointActions.HasUploadedRequiredDocuments(enrolmentId.GetValueOrDefault());
                    contactDetails.HasCV =
                        SharepointActions.HasUploadedCVForm(enrolmentId.GetValueOrDefault());
                    contactDetails.HasUploadedAcceptanceLetter =
                        SharepointActions.HasUploadedAcceptanceForm(enrolmentId.GetValueOrDefault());

                    if (contactDetails.HasUploadedAcceptanceLetter)
                    {
                        if (contactDetails.VoucherStatus == VoucherStatus.Redeemable && contactDetails.HasVoucher)
                        {
                            enrolment.stb_AcceptanceFormTwoOptions = true;
                            CreateOrUpdateEnrollmentVoucher(contactDetails, enrolmentId);
                        }

                        enrolment.stb_PaymentIndicatorTwoOptions = true;
                    }
                    if (contactDetails.HasCV)
                    {
                        enrolment.stb_CVReceived = true;
                    }



                    if (contactDetails.HasUploadedRequiredDocuments &&
                        contactDetails.VoucherStatus == VoucherStatus.Redeemable)
                    {
                        enrolment.stb_DocumentsCompletedTwoOptions = true;
                    }

                    if (contactDetails.Employed)
                    {
                        enrolment.stb_CurrentlyEmployedTwoOptions = true;
                        enrolment.stb_JobTitle = contactDetails.JobTitle;
                        enrolment.stb_IndustryLookup = new CrmEntityReference(stb_industry.EntityLogicalName,
                            contactDetails.Industry.GetValueOrDefault());
                        enrolment.stb_WorkAreaLookup = new CrmEntityReference(stb_workarea.EntityLogicalName,
                            contactDetails.WorkArea.GetValueOrDefault());
                        enrolment.stb_CurrentEmployer = contactDetails.Employer;
                        enrolment.stb_EmployerContactNumber = contactDetails.EmployerContactNumber;
                    }

                    enrolment.stb_QualificationTypeLookup =
                        new CrmEntityReference(stb_QualificationType.EntityLogicalName,
                            contactDetails.QualificationType);
                    enrolment.stb_QualificationTypeOther = contactDetails.QualificationTypeOther;

                    if (contactDetails.QualificationField.IsNotNull() && contactDetails.QualificationField != Guid.Empty)
                    {
                        enrolment.stb_FieldofStudyLookup = new CrmEntityReference(stb_fieldofstudy.EntityLogicalName,
                            contactDetails.QualificationField.GetValueOrDefault());
                    }

                    enrolment.stb_AcademicInstitution = contactDetails.QualificationInstitution;
                    enrolment.stb_YearAchieved = contactDetails.YearAchieved.IsNotNullOrEmpty()
                        ? new DateTime(int.Parse(contactDetails.YearAchieved), 1, 1)
                        : new DateTime(DateTime.Now.Year, 1, 1);

                    enrolment.stb_SharePointLocation =
                        $"{Settings.Default.SharepointUrl}/{Settings.Default.SharepointLibrary}/{enrolment.stb_enrollmentId}";

                    #endregion

                    break;
                case ValidateProgress.Marketing:

                    #region Marketing

                    enrolment.stb_LeadSource = contactDetails.MarketingSource;
                    enrolment.stb_MarketingReason = contactDetails.MarketingReason;
                    enrolment.stb_LeadSourceOther = contactDetails.MarketingSourceOther;
                    enrolment.stb_MarketingReasonOther = contactDetails.MarketingReasonOther;

                    contact.stb_UseTwitter = contactDetails.UsesTwitter;
                    contact.stb_TwitterAccount = contactDetails.TwitterUsername;
                    contact.stb_UseFacebook = contactDetails.UsesFacebook;
                    contact.stb_UseLinkedin = contactDetails.UsesLinkedIn;
                   // contact.stb_SendNewsletterTwoOptions = contactDetails.SubscribeToNewsletter;

                    #endregion

                    break;
                case ValidateProgress.PaymentInfo:
                case ValidateProgress.ApplicationForm:

                    #region Payment Info

                    contact.stb_ParentSponsorCompanyName = contactDetails.IsCompanyResponsibility
                        ? contactDetails.PaymentContactCompanyName
                        : null;
                    contact.stb_ParentContactNumber = contactDetails.IsCompanyResponsibility
                        ? contactDetails.PaymentContactNumber
                        : null;
                    contact.stb_ParentEmailAddress = contactDetails.IsCompanyResponsibility
                        ? contactDetails.PaymentContactEmail
                        : null;

                    //Need to save to email2 field as CRM workflow requirement
                    enrolment.stb_EmailAddress2 = contactDetails.IsCompanyResponsibility
                        ? contactDetails.PaymentContactEmail
                        : null;
                    
                        enrolment.stb_CompanyPOPIconsent = contactDetails.Consent;
                   
                    contact.stb_ParentFirstName = contactDetails.IsCompanyResponsibility
                        ? contactDetails.PaymentContactName
                        : null;
                    contact.stb_ParentLastName = contactDetails.IsCompanyResponsibility
                        ? contactDetails.PaymentContactSurname
                        : null;
                    contact.stb_ParentStreet1 = contactDetails.IsCompanyResponsibility
                        ? contactDetails.PaymentPostalStreet1
                        : null;
                    contact.stb_ParentStreet2 = contactDetails.IsCompanyResponsibility
                        ? contactDetails.PaymentPostalStreet2
                        : null;
                    contact.stb_ParentPostalCode = contactDetails.IsCompanyResponsibility
                        ? contactDetails.PaymentPostalCode
                        : null;
                    contact.stb_ParentCityTown = contactDetails.IsCompanyResponsibility
                        ? contactDetails.PaymentPostalCity
                        : null;
                    contact.stb_ParentSuburb = contactDetails.IsCompanyResponsibility
                        ? contactDetails.PaymentPostalSuburb
                        : null;

                    if (contactDetails.PaymentPostalCountry.IsNotNull() &&
                        contactDetails.PaymentPostalCountry != Guid.Empty)
                        contact.stb_ParentCountryLookup = new CrmEntityReference(stb_country.EntityLogicalName,
                            contactDetails.PaymentPostalCountry);

                    ///TO DO///
//                    contact.stb_ParentProvince = contactDetails.PaymentProvince;


                    enrolment.stb_PayeeContactPerson = contactDetails.IsCompanyResponsibility
                        ? $"{contactDetails.PaymentContactName} {contactDetails.PaymentContactSurname}"
                        : null;
                    enrolment.stb_PayeeContactNumber = contactDetails.IsCompanyResponsibility
                        ? contactDetails.PaymentContactNumber
                        : null;
                    enrolment.stb_PayeeContactEmailAddress = contactDetails.IsCompanyResponsibility
                        ? contactDetails.PaymentContactEmail
                        : null;
                    //enrolment.stb_PayeeDebtorNumber = contactDetails.IsCompanyResponsibility
                    //    ? contactDetails.PaymentDebtorCode
                    //    : null;

                    if (contactDetails.PaymentResponsibility.IsNotNull() &&
                        contactDetails.PaymentResponsibility != Guid.Empty)
                        enrolment.stb_PaymentResponsibilityLookup =
                            new CrmEntityReference(stb_paymentresponsibility.EntityLogicalName,
                                contactDetails.PaymentResponsibility);

                    #endregion

                    break;
                    case ValidateProgress.GatewayPaymentSuccesful:

                    #region Payment Succesful

                    enrolment.Id = enrolmentId.Value;
                    enrolment.stb_enrollmentId = enrolment.Id;
                    enrolment.stb_USNumber = contactDetails.UsNumber;

                    enrolment.stb_PaymentIndicatorTwoOptions = true;
                    enrolment.stb_PaymentReferenceNumber = contactDetails.UsNumber;
                    enrolment.stb_PaymentDate = DateTime.Now;

                    #endregion
                    break;
            }

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                try
                {
                    crmServiceContext.Update(enrolment);
                    crmServiceContext.Update(contact);
                }
                finally
                {
                    crmServiceContext.Dispose();
                }
                if (contactDetails.ShortCourseOfferingId.IsNullOrEmpty() ||
                    contactDetails.Offering.IsNotNull() && contactDetails.Offering != Guid.Empty)
                {
                    var offeringId = contactDetails.ShortCourseOfferingId.IsNotNullOrEmpty()
                        ? contactDetails.ShortCourseOfferingId
                        : contactDetails.Offering.ToString();

                    Guid offeringOwner =
                        CrmQueries.GetCourseOfferingOwner(Guid.Parse(offeringId));
                    CrmActions.SetOwnershipOfEnrollment(enrolment.Id, offeringOwner);
                }
            }
            return contactDetails;
        }

        public static string GetAfrigisProvinceName(Guid provinceId)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                try
                {
                    var provinceName = (from c in crmServiceContext.stb_afrigisSet
                                        where c.stb_afrigisId == provinceId
                                        select c.stb_Province).FirstOrDefault();

                    if (provinceName.IsNotNull())
                    {
                        return provinceName;
                    }
                }
                finally
                {
                    crmServiceContext.Dispose();
                }
            }

            return string.Empty;
        }
        public static Guid? GetAfrigisProvinceId(string provinceName)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                try
                {
                    var provinceId = (from c in crmServiceContext.stb_afrigisSet
                                        where c.stb_Province == provinceName
                                        select c.stb_afrigisId).FirstOrDefault();

                    if (provinceId.IsNotNull())
                    {
                        return provinceId;
                    }
                }
                finally
                {
                    crmServiceContext.Dispose();
                }
            }

            return null;
        }

        public static void CreateOrUpdateEnrollmentVoucher(ApplicationForm contactDetails, Guid? enrollmentGuid)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                Guid? voucherGuid;

                try
                {
                    var existingVoucher =
                        crmServiceContext.stb_voucherSet.FirstOrDefault(
                            x => x.stb_EnrollmentLookup.Id == enrollmentGuid.GetValueOrDefault());

                    if (contactDetails.VoucherStatus == VoucherStatus.Redeemable && existingVoucher.IsNotNull())
                    {
                        existingVoucher.statuscode = (int)VoucherStatus.Redeemed;

                        crmServiceContext.UpdateObject(existingVoucher);
                        crmServiceContext.SaveChanges();

                        UpdateEnrollmentVoucherTotals(Guid.Parse(contactDetails.ShortCourseOfferingId));
                    }
                    else
                    {
                        var voucher = new stb_voucher
                        {
                            stb_EnrollmentLookup = new CrmEntityReference(stb_enrollment.EntityLogicalName,
                                enrollmentGuid.GetValueOrDefault()),
                            stb_AccountLookup = new CrmEntityReference(Account.EntityLogicalName,
                                contactDetails.AccountGuid.GetValueOrDefault()),
                            stb_ValidFrom = DateTime.Now.Date,
                            stb_ValidUntil = DateTime.Now.Date.AddYears(1),
                            stb_ShortCourseOfferingLookup =
                                new CrmEntityReference(stb_shortcourseoffering.EntityLogicalName,
                                    Guid.Parse(contactDetails.ShortCourseOfferingId)),
                            statuscode = (int)VoucherStatus.Redeemable
                        };

                        voucherGuid = crmServiceContext.Create(voucher);

                        Guid offeringOwner =
                            CrmQueries.GetCourseOfferingOwner(Guid.Parse(contactDetails.ShortCourseOfferingId));
                        SetOwnershipOfVoucher(voucherGuid.GetValueOrDefault(), offeringOwner);
                    }
                }
                finally
                {
                    crmServiceContext.Dispose();
                }
            }
        }

        private static void UpdateEnrollmentVoucherTotals(Guid? offeringId)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                try
                {
                    var offering =
                        crmServiceContext.stb_shortcourseofferingSet.FirstOrDefault(x => x.stb_shortcourseofferingId == offeringId);

                    if (offering.IsNotNull())
                    {
                        if (offering.stb_VoucherTotal.IsNotNull())
                        {
                            if (offering.stb_VouchersAvailable.IsNullOrEmpty())
                            {
                                offering.stb_VouchersAvailable = offering.stb_VoucherTotal.ToString();
                            }

                            int availableVouchers = int.Parse(offering.stb_VouchersAvailable) - 1;
                            int totalVouchersRedeemed = offering.stb_VoucherTotal.GetValueOrDefault() - availableVouchers;

                            offering.stb_VouchersAvailable = availableVouchers.ToString();
                            offering.stb_VouchersRedeemed = totalVouchersRedeemed.ToString();
                        }

                        crmServiceContext.UpdateObject(offering);
                        crmServiceContext.SaveChanges();
                    }
                }
                finally
                {
                    crmServiceContext.Dispose();
                }
            }
        }

        private static void SetOwnershipOfEnrollment(Guid enrollmentId, Guid offeringOwner)
        {
            using (var organizationService = new OrganizationService(CrmServer.CrmConnection))
            //using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                var assignRequest = new AssignRequest()
                {
                    Assignee = new EntityReference(SystemUser.EntityLogicalName, offeringOwner),
                    Target = new EntityReference(stb_enrollment.EntityLogicalName, enrollmentId)
                };

                try
                {
                    organizationService.Execute(assignRequest);
                }
                finally
                {
                    organizationService.Dispose();
                }
            }
        }

        private static void SetOwnershipOfVoucher(Guid voucherGuid, Guid offeringGuid)
        {
           // using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            using (var organizationService = new OrganizationService(CrmServer.CrmConnection))
            {
                var assignRequest = new AssignRequest
                {
                    Assignee = new EntityReference(SystemUser.EntityLogicalName, offeringGuid),
                    Target = new EntityReference(stb_voucher.EntityLogicalName, voucherGuid)
                };

                try
                {
                    organizationService.Execute(assignRequest);
                }
                finally
                {
                    organizationService.Dispose();
                }
            }
        }
    }
}
