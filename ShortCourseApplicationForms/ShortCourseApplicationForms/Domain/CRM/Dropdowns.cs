﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using ShortCourseApplicationForm.Domain.Models.ShortCourseApplicationForm;
using ShortCourseApplicationForm.Models;
using StellenboschUniversity.UsbEd.Integration.Crm;

namespace ShortCourseApplicationForm.Domain.CRM
{
    public static class Dropdowns
    {
        public static void DietaryRequirements(ApplicationFormInfo info)
        {
            var exist = Cache.Contains("DietaryRequirements");

            if (exist)
            {
                info.DietaryRequirements = (List<SelectListItem>)Cache.GetCachedItem("DietaryRequirements");
                return;
            }

            info.DietaryRequirements = new List<SelectListItem> {new SelectListItem {Text = "", Value = ""}};
            
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<stb_specialdietaryrequirements> crmResults = null;

                try
                {
                    crmResults = from a in crmServiceContext.stb_specialdietaryrequirementsSet
                        orderby a.stb_NameEnglish
                        select a;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                foreach (var item in crmResults)
                {
                    if (item.stb_NameEnglish == "None")
                    {
                        info.DietaryRequirements.Add(new SelectListItem
                        {
                            Text = item.stb_NameEnglish,
                            Value = item.Id.ToString(),
                            Selected = true
                        });
                    }
                    else
                    {
                        info.DietaryRequirements.Add(new SelectListItem
                        {
                            Text = item.stb_NameEnglish,
                            Value = item.Id.ToString()
                        });
                    }
                }
            }

            Cache.Add("DietaryRequirements", info.DietaryRequirements);
        }

        public static void Ethnicities(ApplicationFormInfo info)
        {
            var exist = Cache.Contains("Ethnicities");

            if (exist)
            {
                info.Ethnicities = (List<SelectListItem>)Cache.GetCachedItem("Ethnicities");
                return;
            }

            info.Ethnicities = new List<SelectListItem>() ;// {new SelectListItem {Text = "", Value = ""}};
            
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<stb_ethnicity> crmResults = null;

                try
                {
                    crmResults = from a in crmServiceContext.stb_ethnicitySet
                                 orderby a.stb_NameEnglish
                                 select a;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                foreach (var item in crmResults)
                {
                    info.Ethnicities.Add(new SelectListItem
                    {
                        Text = item.stb_NameEnglish,
                        Value = item.Id.ToString()
                    });
                }
            }

            Cache.Add("Ethnicities", info.Ethnicities);
        }

        public static void EthnicitiesNonSA(ApplicationFormInfo info)
        {
            var exist = Cache.Contains("EthnicitiesNonSA");

            if (exist)
            {
                info.EthnicitiesNonSA = (List<SelectListItem>) Cache.GetCachedItem("EthnicitiesNonSA");
                return;
            }

            info.EthnicitiesNonSA = new List<SelectListItem>();

            var africanGuid = info.Ethnicities.FirstOrDefault(e => e.Text == "African").Value;
            var asianGuid = info.Ethnicities.FirstOrDefault(e => e.Text == "Indian").Value;
            var caucasianGuid = info.Ethnicities.FirstOrDefault(e => e.Text == "White").Value;
            var mixedGuid = info.Ethnicities.FirstOrDefault(e => e.Text == "Coloured").Value;
            var otherGuid = info.Ethnicities.FirstOrDefault(e => e.Text == "African").Value;

           //info.EthnicitiesNonSA.Add(new SelectListItem {Text = "", Value = "", Selected = true});
            info.EthnicitiesNonSA.Add(new SelectListItem {Text = "African", Value = africanGuid});
            info.EthnicitiesNonSA.Add(new SelectListItem {Text = "Asian", Value = asianGuid});
            info.EthnicitiesNonSA.Add(new SelectListItem {Text = "Caucasian", Value = caucasianGuid});
            info.EthnicitiesNonSA.Add(new SelectListItem {Text = "Mixed", Value = mixedGuid});
            info.EthnicitiesNonSA.Add(new SelectListItem {Text = "Other", Value = otherGuid});

            Cache.Add("EthnicitiesNonSA", info.EthnicitiesNonSA);
        }

        public static void ForeignIdentificationTypes(ApplicationFormInfo info)
        {
            var exist = Cache.Contains("IdDocumentTypes");

            if (exist)
            {
                info.IdDocumentTypes = (List<SelectListItem>)Cache.GetCachedItem("IdDocumentTypes");
                return;
            }

            info.IdDocumentTypes = new List<SelectListItem> {new SelectListItem {Text = "", Value = ""}};
            
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<stb_Idtype> crmResults = null;

                try
                {
                    crmResults = from a in crmServiceContext.stb_IdtypeSet
                                 orderby a.stb_NameEnglish
                                 select a;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                foreach (var item in crmResults)
                {
                    info.IdDocumentTypes.Add(new SelectListItem
                    {
                        Text = item.stb_NameEnglish,
                        Value = item.Id.ToString()
                    });
                }
            }

            Cache.Add("IdDocumentTypes", info.IdDocumentTypes);
        }

        public static void Genders(ApplicationFormInfo info)
        {
            var exist = Cache.Contains("Genders");

            if (exist)
            {
                info.Genders = (List<SelectListItem>)Cache.GetCachedItem("Genders");
                return;
            }

            info.Genders = new List<SelectListItem> {new SelectListItem {Text = "", Value = ""}};
            
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<stb_gender> crmResults = null;

                try
                {
                    crmResults = from a in crmServiceContext.stb_genderSet
                                 orderby a.stb_NameEnglish
                                 select a;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                foreach (var item in crmResults)
                {
                    info.Genders.Add(new SelectListItem
                    {
                        Text = item.stb_NameEnglish,
                        Value = item.Id.ToString()
                    });
                }
            }

            Cache.Add("Genders", info.Genders);
        }

        public static void Industries(ApplicationFormInfo info)
        {
            var exist = Cache.Contains("Industries");

            if (exist)
            {
                info.Industries = (List<SelectListItem>)Cache.GetCachedItem("Industries");
                return;
            }

            info.Industries = new List<SelectListItem> {new SelectListItem {Text = "", Value = ""}};
            
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<stb_industry> crmResults = null;

                try
                {
                    crmResults = from a in crmServiceContext.stb_industrySet
                                 orderby a.stb_NameEnglish
                                 select a;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                foreach (var item in crmResults)
                {
                    info.Industries.Add(new SelectListItem
                    {
                        Text = item.stb_NameEnglish,
                        Value = item.Id.ToString()
                    });
                }
            }

            Cache.Add("Industries", info.Industries);
        }

        public static void Languages(ApplicationFormInfo info)
        {
            var exist = Cache.Contains("Languages");

            if (exist)
            {
                info.Languages = (List<SelectListItem>)Cache.GetCachedItem("Languages");
                return;
            }

            info.Languages = new List<SelectListItem> {new SelectListItem {Text = "", Value = ""}};
            
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<stb_language> crmResults = null;

                try
                {
                    crmResults = from a in crmServiceContext.stb_languageSet
                                 orderby a.stb_NameEnglish
                                 select a;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                foreach (var item in crmResults)
                {
                    info.Languages.Add(new SelectListItem
                    {
                        Text = item.stb_NameEnglish,
                        Value = item.Id.ToString()
                    });
                }
            }

            Cache.Add("Languages", info.Languages);
        }

        public static void MarketingReasons(ApplicationFormInfo info)
        {
            var result = CrmQueries.GetOptionSetValues(stb_enrollment.EntityLogicalName, "Stb_MarketingReason");

            var exist = Cache.Contains("MarketingReasons");

            if (exist)
            {
                info.MarketingReasons = (List<SelectListItem>)Cache.GetCachedItem("MarketingReasons");
                return;
            }

            info.MarketingReasons = new List<SelectListItem> { new SelectListItem { Text = "", Value = "" } };

            if (result.IsNotNull())
            {
                foreach (var item in result)
                {
                    info.MarketingReasons.Add(new SelectListItem
                    {
                        Text = item.Key,
                        Value = item.Value.ToString()
                    });
                }
            }

            Cache.Add("MarketingReasons", info.MarketingReasons);
        }

        public static void MarketingSources(ApplicationFormInfo info)
        {
            var result = CrmQueries.GetOptionSetValues(stb_enrollment.EntityLogicalName, "stb_LeadSource");

            var exist = Cache.Contains("MarketingSources");

            if (exist)
            {
                info.MarketingSources = (List<SelectListItem>)Cache.GetCachedItem("MarketingSources");
                return;
            }

            info.MarketingSources = new List<SelectListItem> { new SelectListItem { Text = "", Value = "" } };

            if (result.IsNotNull())
            {
                foreach (var item in result)
                {
                    info.MarketingSources.Add(new SelectListItem
                    {
                        Text = item.Key,
                        Value = item.Value.ToString()
                    });
                }
            }

            Cache.Add("MarketingSources", info.MarketingSources);
        }

        public static void Nationalities(ApplicationFormInfo info)
        {
            var exist = Cache.Contains("Nationalities");

            if (exist)
            {
                info.Nationalities = (List<SelectListItem>) Cache.GetCachedItem("Nationalities");
                return;
            }

            info.Nationalities = new List<SelectListItem> {new SelectListItem {Text = "", Value = ""}};

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<stb_nationality> crmResults = null;

                try
                {
                    crmResults = from a in crmServiceContext.stb_nationalitySet
                        orderby a.stb_NameEnglish
                        select a;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                foreach (var item in crmResults)
                {
                    info.Nationalities.Add(new SelectListItem
                    {
                        Text = item.stb_NameEnglish,
                        Value = item.Id.ToString()
                    });
                }

                if (info.Nationalities.Count > 0)
                {
                    var country = info.Nationalities.FirstOrDefault(x => x.Text == "South Africa");
                    info.Nationalities.Remove(country);
                    info.Nationalities.Insert(1, country);
                }
            }

            Cache.Add("Nationalities", info.Nationalities);
        }

        public static void OccupationalCategories(ApplicationFormInfo info)
        {
            var exist = Cache.Contains("OccupationalCategories");

            if (exist)
            {
                info.OccupationalCategories = (List<SelectListItem>)Cache.GetCachedItem("OccupationalCategories");
                return;
            }

            info.OccupationalCategories = new List<SelectListItem> {new SelectListItem {Text = "", Value = ""}};
            
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<stb_occupationalcategory> crmResults = null;

                try
                {
                    crmResults = from a in crmServiceContext.stb_occupationalcategorySet
                                 orderby a.stb_NameEnglish
                                 select a;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                foreach (var item in crmResults)
                {
                    info.OccupationalCategories.Add(new SelectListItem
                    {
                        Text = item.stb_NameEnglish,
                        Value = item.Id.ToString()
                    });
                }
            }

            Cache.Add("OccupationalCategories", info.OccupationalCategories);
        }

        public static void PaymentResponsibilities(ApplicationFormInfo info)
        {
            var exist = Cache.Contains("PaymentResponsibility");

            if (exist)
            {
                info.PaymentResponsibility = (List<SelectListItem>)Cache.GetCachedItem("PaymentResponsibility");
                return;
            }

            info.PaymentResponsibility = new List<SelectListItem> {new SelectListItem {Text = "", Value = ""}};

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<stb_paymentresponsibility> crmResults = null;

                try
                {
                    crmResults = from a in crmServiceContext.stb_paymentresponsibilitySet
                                 orderby a.stb_NameEnglish
                                 select a;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                foreach (var item in crmResults)
                {
                    info.PaymentResponsibility.Add(new SelectListItem
                    {
                        Text = item.stb_NameEnglish,
                        Value = item.Id.ToString()
                    });
                }
            }

            Cache.Add("PaymentResponsibility", info.PaymentResponsibility);
        }

        public static void QualificationFields(ApplicationFormInfo info)
        {
            var exist = Cache.Contains("QualificationFields");

            if (exist)
            {
                info.QualificationFields = (List<SelectListItem>)Cache.GetCachedItem("QualificationFields");
                return;
            }

            info.QualificationFields = new List<SelectListItem> { new SelectListItem { Text = "", Value = "" } };

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<stb_fieldofstudy> crmResults = null;

                try
                {
                    crmResults = from a in crmServiceContext.stb_fieldofstudySet
                                 orderby a.stb_NameEnglish
                                 select a;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                foreach (var item in crmResults)
                {
                    info.QualificationFields.Add(new SelectListItem
                    {
                        Text = item.stb_NameEnglish,
                        Value = item.Id.ToString()
                    });
                }
            }

            Cache.Add("QualificationFields", info.QualificationFields);
        }

        public static void QualificationTypes(ApplicationFormInfo info)
        {
            var exist = Cache.Contains("QualificationTypes");

            if (exist)
            {
                info.QualificationTypes = (List<SelectListItem>)Cache.GetCachedItem("QualificationTypes");
                return;
            }

            info.QualificationTypes = new List<SelectListItem> {new SelectListItem {Text = "", Value = ""}};
            
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<stb_QualificationType> crmResults = null;

                try
                {
                    crmResults = from a in crmServiceContext.stb_QualificationTypeSet
                                 orderby a.stb_NameEnglish
                                 select a;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                foreach (var item in crmResults)
                {
                    info.QualificationTypes.Add(new SelectListItem
                    {
                        Text = item.stb_NameEnglish,
                        Value = item.Id.ToString()
                    });
                }
            }

            Cache.Add("QualificationTypes", info.QualificationTypes);
        }

        public static void Titles(ApplicationFormInfo info)
        {
            var exist = Cache.Contains("Titles");
            
            if (exist)
            {
                info.Titles = (List<SelectListItem>)Cache.GetCachedItem("Titles");
                return;
            }

            info.Titles = new List<SelectListItem> {new SelectListItem {Text = "", Value = ""}};

            // TODO: Filter and add the following items on top of list
            var firstTitles = new List<string>() { "Ms", "Mx", "Miss", "Misses", "Mister", "Professor", "Doctor/Docter" };

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<stb_title> crmResults = null;

                try
                {
                    crmResults = from a in crmServiceContext.stb_titleSet
                        orderby a.stb_NameEnglish
                        select a;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }
                foreach (var item in crmResults)
                {
                    info.Titles.Add(new SelectListItem
                    {
                        Text = item.stb_NameEnglish,
                        Value = item.Id.ToString()
                    });
                }

                if (info.Titles.Count > 0)
                {
                    var doctor = info.Titles.FirstOrDefault(x => x.Text == "Doctor/Docter");
                    info.Titles.Remove(doctor);
                    info.Titles.Insert(0, doctor);

                    var prof = info.Titles.FirstOrDefault(x => x.Text == "Professor");
                    info.Titles.Remove(prof);
                    info.Titles.Insert(0, prof);

                    var mister = info.Titles.FirstOrDefault(x => x.Text == "Mister");
                    info.Titles.Remove(mister);
                    info.Titles.Insert(0, mister);

                    var misses = info.Titles.FirstOrDefault(x => x.Text == "Misses");
                    info.Titles.Remove(misses);
                    info.Titles.Insert(0, misses);

                    var miss = info.Titles.FirstOrDefault(x => x.Text == "Miss");
                    info.Titles.Remove(miss);
                    info.Titles.Insert(0, miss);

                    var mx = info.Titles.FirstOrDefault(x => x.Text == "Mx");
                    info.Titles.Remove(mx);
                    info.Titles.Insert(0, mx);

                    var ms = info.Titles.FirstOrDefault(x => x.Text == "Ms");
                    info.Titles.Remove(ms);
                    info.Titles.Insert(0, ms);

                    var blank = info.Titles.FirstOrDefault(x => x.Text == "");
                    info.Titles.Remove(blank);
                    info.Titles.Insert(0, blank);
                }
            }

            Cache.Add("Titles", info.Titles);
        }

        public static void WorkAreas(ApplicationFormInfo info)
        {
            var exist = Cache.Contains("WorkAreas");

            if (exist)
            {
                info.WorkAreas = (List<SelectListItem>)Cache.GetCachedItem("WorkAreas");
                return;
            }

            info.WorkAreas = new List<SelectListItem> {new SelectListItem {Text = "", Value = ""}};
            
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<stb_workarea> crmResults = null;

                try
                {
                    crmResults = from a in crmServiceContext.stb_workareaSet
                                 orderby a.stb_NameEnglish
                                 select a;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                foreach (var item in crmResults)
                {
                    info.WorkAreas.Add(new SelectListItem
                    {
                        Text = item.stb_NameEnglish,
                        Value = item.Id.ToString()
                    });
                }
            }

            Cache.Add("WorkAreas", info.WorkAreas);
        }

        public static void AddressCountries(ApplicationFormInfo info)
        {
            var exist = Cache.Contains("AddressCountries");

            if (exist)
            {
                info.AddressCountries = (List<SelectListItem>)Cache.GetCachedItem("AddressCountries");
                return;
            }

            info.AddressCountries = new List<SelectListItem> {new SelectListItem {Text = "", Value = ""}};

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<stb_country> crmResults = null;

                try
                {
                    crmResults = from a in crmServiceContext.stb_countrySet
                        orderby a.stb_name
                        select a;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                foreach (var item in crmResults)
                {
                    info.AddressCountries.Add(new SelectListItem
                    {
                        Text = item.stb_name,
                        Value = item.Id.ToString()
                    });
                }

                if (info.AddressCountries.Count > 0)
                {
                    var country = info.AddressCountries.FirstOrDefault(x => x.Text == "South Africa");
                    info.AddressCountries.Remove(country);
                    info.AddressCountries.Insert(1, country);
                }
            }

            Cache.Add("AddressCountries", info.AddressCountries);
        }

        public static void Provinces(ApplicationFormInfo info)
        {
            var exist = Cache.Contains("Provinces");

            if (exist)
            {
                info.Provinces = (List<SelectListItem>)Cache.GetCachedItem("Provinces");
                return;
            }

            info.Provinces = new List<SelectListItem> { new SelectListItem { Text = "", Value = "" } };

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<stb_afrigis> crmResults = null;

                try
                {
                    crmResults = from a in crmServiceContext.stb_afrigisSet
                                 orderby a.stb_Province
                                 where a.stb_Province != null
                                 select a;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (crmResults.IsNotNull())
                {
                    var easternCape = crmResults.FirstOrDefault(a => a.stb_Province == "Eastern Cape");

                    if (easternCape.IsNotNull())
                        info.Provinces.Add(new SelectListItem { Text = easternCape.stb_Province, Value = easternCape.Id.ToString() });

                    var freeState = crmResults.FirstOrDefault(a => a.stb_Province == "Free State");

                    if (freeState.IsNotNull())
                        info.Provinces.Add(new SelectListItem { Text = freeState.stb_Province, Value = freeState.Id.ToString() });

                    var gauteng = crmResults.FirstOrDefault(a => a.stb_Province == "Gauteng");

                    if (gauteng.IsNotNull())
                        info.Provinces.Add(new SelectListItem { Text = gauteng.stb_Province, Value = gauteng.Id.ToString() });

                    var kwazuluNatal = crmResults.FirstOrDefault(a => a.stb_Province == "KwaZulu-Natal");

                    if (kwazuluNatal.IsNotNull())
                        info.Provinces.Add(new SelectListItem { Text = kwazuluNatal.stb_Province, Value = kwazuluNatal.Id.ToString() });

                    var limpopo = crmResults.FirstOrDefault(a => a.stb_Province == "Limpopo");

                    if (limpopo.IsNotNull())
                        info.Provinces.Add(new SelectListItem { Text = limpopo.stb_Province, Value = limpopo.Id.ToString() });

                    var mpumalanga = crmResults.FirstOrDefault(a => a.stb_Province == "Mpumalanga");

                    if (mpumalanga.IsNotNull())
                        info.Provinces.Add(new SelectListItem { Text = mpumalanga.stb_Province, Value = mpumalanga.Id.ToString() });

                    var northernCape = crmResults.FirstOrDefault(a => a.stb_Province == "Northern Cape");

                    if (northernCape.IsNotNull())
                        info.Provinces.Add(new SelectListItem { Text = northernCape.stb_Province, Value = northernCape.Id.ToString() });

                    var northWest = crmResults.FirstOrDefault(a => a.stb_Province == "North West");

                    if (northWest.IsNotNull())
                        info.Provinces.Add(new SelectListItem { Text = northWest.stb_Province, Value = northWest.Id.ToString() });

                    var westernCape = crmResults.FirstOrDefault(a => a.stb_Province == "Western Cape");

                    if (westernCape.IsNotNull())
                        info.Provinces.Add(new SelectListItem { Text = westernCape.stb_Province, Value = westernCape.Id.ToString() });
                }
            }

            Cache.Add("Provinces", info.Provinces);
        }

        public static void Disabilities(ApplicationFormInfo info)
        {
            var exist = Cache.Contains("Disabilities");

            if (exist)
            {
                info.Disabilities = (List<SelectListItem>)Cache.GetCachedItem("Disabilities");
                return;
            }

            info.Disabilities = new List<SelectListItem> {new SelectListItem {Text = "", Value = ""}};
            
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<stb_disability> crmResults = null;

                try
                {
                    crmResults = from a in crmServiceContext.stb_disabilitySet
                        orderby a.stb_NameEnglish
                        select a;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                foreach (var item in crmResults)
                {
                    if (item.stb_NameEnglish == "None")
                    {
                        info.Disabilities.Add(new SelectListItem
                        {
                            Text = item.stb_NameEnglish,
                            Value = item.Id.ToString(),
                            Selected = true
                        });
                    }
                    else
                    {
                        info.Disabilities.Add(new SelectListItem
                        {
                            Text = item.stb_NameEnglish,
                            Value = item.Id.ToString()
                        });
                    }
                }
            }

            Cache.Add("Disabilities", info.Disabilities);
        }

        public static void YesNo(ApplicationFormInfo info)
        {
            var exist = Cache.Contains("YesNoList");

            if (exist)
            {
                info.YesNoList = (List<SelectListItem>)Cache.GetCachedItem("YesNoList");
                return;
            }

            info.YesNoList = new List<SelectListItem>
            {
                new SelectListItem {Text = "Yes", Value = "true"},
                new SelectListItem {Text = "No", Value = "false", Selected = true}
            };

            Cache.Add("YesNoList", info.YesNoList);
        }

        public static void Years(ApplicationFormInfo info)
        {
            var exist = Cache.Contains("YearsList");

            if (exist)
            {
                info.YearsList = (List<SelectListItem>)Cache.GetCachedItem("YearsList");
                return;
            }

            info.YearsList = new List<SelectListItem>();

            var start = DateTime.Now.AddYears(-80).Year;
            var end = DateTime.Now.Year;

            info.YearsList.Add(new SelectListItem { Text = "", Value = "" });

            for (var year = start; year <= end; year++)
            {
                info.YearsList.Add(new SelectListItem { Text = year.AsString(), Value = year.AsString() });
            }

            Cache.Add("YearsList", info.YearsList);
        }
    }
}
