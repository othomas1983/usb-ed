﻿using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using ShortCourseApplicationForm.Domain.Mapper;
using ShortCourseApplicationForm.Domain.Models;
using ShortCourseApplicationForm.Domain.Models.Enums;
using ShortCourseApplicationForm.Domain.Models.ShortCourseApplicationForm;
using StellenboschUniversity.UsbEd.Integration.Crm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AppSettings = ShortCourseApplicationForm.Domain.Properties.Settings;

namespace ShortCourseApplicationForm.Domain.CRM
{
    public static class CrmQueries
    {
        private static readonly string _MasterStart = AppSettings.Default.MasterStart;
        private static readonly string _BasilRead = AppSettings.Default.BasilRead;
        private static readonly string _GetSmarter = AppSettings.Default.GetSmarter;

        #region Public Methods

        public static string GetSisCorrespondingLanguageCode(Guid? crmHomeLanguage)
        {
            if (crmHomeLanguage.IsNotNull() && crmHomeLanguage != Guid.Empty)
            {
                if (GetHomeLanguageName(crmHomeLanguage.Value)
                    .Equals("Afrikaans", StringComparison.InvariantCultureIgnoreCase))
                {
                    return "9";
                }
            }

            return "1";
        }

        public static string GetHomeLanguageName(Guid homeLanguageId)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                stb_language language = null;

                try
                {
                    language = (from a in crmServiceContext.stb_languageSet
                                where a.stb_languageId.GetValueOrDefault() == homeLanguageId
                                select a).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (language.IsNull())
                {
                    throw new Exception("No home language found.");
                }
                else
                {
                    return language.stb_NameEnglish;
                }
            }
        }

        public static Guid GetContactId(string usNumber)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                Contact contact = null;

                try
                {
                    contact = (from c in crmServiceContext.ContactSet
                               where c.stb_USNumber == usNumber
                               select c).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (contact.IsNull())
                {
                    var newContact = new Contact
                    {
                        stb_USNumber = usNumber
                    };

                    return crmServiceContext.Create(newContact);
                }

                return contact.Id;
            }
        }

        public static Guid GetEnrolmentId(string usNumber, string shortCourseOffering)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                stb_enrollment enrolment = null;

                try
                {
                    if (shortCourseOffering.IsNotNullOrEmpty())
                    {
                        enrolment = (from a in crmServiceContext.stb_enrollmentSet
                                     where
                                     a.stb_USNumber == usNumber && a.stb_ShortCourseOfferingLookup.Id == Guid.Parse(shortCourseOffering)
                                     select a).FirstOrDefault();
                    }
                    else
                    {
                        enrolment = (from a in crmServiceContext.stb_enrollmentSet
                                     where a.stb_USNumber == usNumber
                                     select a).FirstOrDefault();
                    }
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (enrolment.IsNull())
                {
                    var enrollment = new stb_enrollment
                    {
                        stb_USNumber = usNumber
                    };

                    return crmServiceContext.Create(enrollment);
                }

                return enrolment.Id;
            }
        }
        //Get Enrolment by ShortCourseOffering Guid
        public static Guid GetEnrolmentId(string usNumber, Guid shortCourseOffering)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                stb_enrollment enrolment = null;

                try
                {
                    if (shortCourseOffering != Guid.Empty)
                    {
                        enrolment = (from a in crmServiceContext.stb_enrollmentSet
                                     where
                                     a.stb_USNumber == usNumber && a.stb_ShortCourseOfferingLookup.Id == shortCourseOffering
                                     select a).FirstOrDefault();
                    }
                    else
                    {
                        enrolment = (from a in crmServiceContext.stb_enrollmentSet
                                     where a.stb_USNumber == usNumber
                                     select a).FirstOrDefault();
                    }
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (enrolment.IsNull())
                {
                    var enrollment = new stb_enrollment
                    {
                        stb_USNumber = usNumber
                    };

                    return crmServiceContext.Create(enrollment);
                }

                return enrolment.Id;
            }
        }
        public static Guid LoadShortCourseBy(Guid shortCourseOffering)
        {
            Guid shortCourseOfferingId = Guid.Empty;

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                try
                {
                    var result = (from a in crmServiceContext.stb_shortcourseofferingSet
                                  join b in crmServiceContext.stb_shortcourseSet
                                  on a.stb_ShortCourseLookup.Id equals b.stb_shortcourseId
                                  where a.stb_shortcourseofferingId == shortCourseOffering
                                  select b.stb_shortcourseId).FirstOrDefault();

                    if (result.IsNotNull() && result != Guid.Empty)
                    {
                        shortCourseOfferingId = result.GetValueOrDefault();
                    }
                }
                finally
                {
                    crmServiceContext.Dispose();
                }
            }

            return shortCourseOfferingId;
        }

        public static string LoadShortCourseOfferingNameByID(Guid ShortCourseOfferingId)
        {
            string courseName = "";

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                try
                {
                    var result = (from a in crmServiceContext.stb_shortcourseofferingSet
                                  join b in crmServiceContext.stb_shortcourseSet
                                  on a.stb_ShortCourseLookup.Id equals b.stb_shortcourseId
                                  where a.stb_shortcourseofferingId == ShortCourseOfferingId
                                  select b.stb_ShortCourseName).FirstOrDefault();


                    if (result.IsNotNull())
                    {
                        courseName = result;
                    }
                }

                finally
                {
                    crmServiceContext.Dispose();
                }
            }

            return courseName;
        }


        public static string LoadShortCourseNameByID(Guid ShortCourseId)
        {
            string courseName = "";

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                try
                {
                    var result = (from b in crmServiceContext.stb_shortcourseSet                                                                
                                  where b.stb_shortcourseId == ShortCourseId
                                  select b.stb_ShortCourseName ).FirstOrDefault();


                    if (result.IsNotNull())
                    {
                        courseName = result;
                    }
                }

                finally
                {
                    crmServiceContext.Dispose();
                }
            }

            return courseName;
        }

        public static List<SelectListItem> LoadShortCourseOfferings(ApplicationForm model, string shortCourseId,
            string shortCourseOfferingId)
        {
            var shortCourseOfferings = new List<SelectListItem>();





            if (model.DirectOfferingLink || model.InsideCRM)
            {
                //if (model.ApplicationClosed)
                //{
                //    model.ShortCourseInfo = new List<ShortCourseInfo>();
                //    shortCourseOfferings.Add(new SelectListItem { Text = "< Select Course >", Value = "none" });

                //    using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
                //    {
                //        IQueryable<stb_shortcourseoffering> crmShortCourseOfferings = null;

                //        try
                //        {
                //            crmShortCourseOfferings = from a in crmServiceContext.stb_shortcourseofferingSet
                //                                      where
                //                                      a.stb_ShortCourseLookup.Id == Guid.Parse(shortCourseId) &&
                //                                      a.statuscode == (int)stb_shortcourseoffering_statuscode.OpenforApplication
                //                                       && a.stb_ApplicationClosingDate >= DateTime.Now
                //                                      orderby a.stb_StartDate descending
                //                                      select a;
                //        }
                //        finally
                //        {
                //            crmServiceContext.Dispose();
                //        }

                //        foreach (var item in crmShortCourseOfferings)
                //        {
                //            shortCourseOfferings.Add(new SelectListItem
                //            {
                //                Text = item.stb_OfferingNameEnglish,
                //                Value = item.Id.ToString()
                //            });
                //            model.ShortCourseInfo.Add(new ShortCourseInfo
                //            {
                //                OfferingName = item.stb_OfferingNameEnglish,
                //                CourseId = item.Id.ToString(),
                //                //CourseAdministrator = item.stb_AdministratorName,
                //                OfferingStartDate = ResolveCourseStartDate(item.stb_StartDate.Value.AddDays(1).ToString())
                //            });



                //            model.CourseStartDate = ResolveCourseStartDate(item.stb_StartDate.ToString() ?? "");
                //            model.ShortCourseOfferingType =
                //                (ShortCourseOfferingType)
                //                Enum.ToObject(typeof(ShortCourseOfferingType),
                //                    item.stb_OfferingTypeOptionSet.GetValueOrDefault());

                //            //model.CourseAdministrator = item.stb_AdministratorName;
                //        }
                //    }
                //    return shortCourseOfferings;
                //}
                //else
                //{
                    using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
                    {
                        try
                        {
                            var results = (from a in crmServiceContext.stb_shortcourseSet
                                           join b in crmServiceContext.stb_shortcourseofferingSet
                                           on a.stb_shortcourseId equals b.stb_ShortCourseLookup.Id
                                           where b.stb_shortcourseofferingId == Guid.Parse(shortCourseOfferingId)
                                           orderby a.stb_ShortCourseName

                                           select new
                                           {
                                               a.Id,
                                               b.stb_OfferingNameEnglish,
                                               b.stb_OfferingTypeOptionSet,
                                               b.stb_StartDate,
                                               a.stb_SCRSAdministrator,
                                               b.stb_VoucherTotal,
                                               b.stb_VouchersRedeemed,
                                               a.stb_ShortCourseName
                                           }).FirstOrDefault();
                            //}).ToList().OrderByDescending(b =>b.stb_StartDate);

                            if (results.IsNotNull())
                            {
                                // var result = results.FirstOrDefault() ;
                                var result = results;
                                model.Offering = result.Id;
                            // model.CourseName = result.stb_OfferingNameEnglish;
                            model.CourseName = result.stb_ShortCourseName;
                                model.ShortCourseOfferingType =
                                    (ShortCourseOfferingType)
                                    Enum.ToObject(typeof(ShortCourseOfferingType),
                                        result.stb_OfferingTypeOptionSet.GetValueOrDefault());



                                model.CourseStartDate = result.stb_StartDate.Value.AddDays(1).ToString() ?? "";
                                model.CourseAdministrator = result.stb_SCRSAdministrator;

                                if (result.stb_VoucherTotal.IsNotNull() && result.stb_VouchersRedeemed.IsNotNullOrEmpty())
                                {
                                    if (result.stb_VoucherTotal == int.Parse(result.stb_VouchersRedeemed))
                                    {
                                        throw new Exception("All vouchers redeemed for this Short Course Offering.");
                                    }
                                }
                            }
                        }
                        finally
                        {
                            crmServiceContext.Dispose();
                        }
                    }
                }
           // }
            else
            {
                //if (model.ApplicationClosed)
                //{
                //    model.ShortCourseInfo = new List<ShortCourseInfo>();
                //    shortCourseOfferings.Add(new SelectListItem { Text = "< Select Course >", Value = "none" });

                //    using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
                //    {
                //        IQueryable<stb_shortcourseoffering> crmShortCourseOfferings = null;

                //        try
                //        {
                //            crmShortCourseOfferings = from a in crmServiceContext.stb_shortcourseofferingSet
                //                                      where
                //                                      a.stb_ShortCourseLookup.Id == Guid.Parse(shortCourseId) &&
                //                                      a.statuscode == (int)stb_shortcourseoffering_statuscode.OpenforApplication
                //                                       && a.stb_ApplicationClosingDate >= DateTime.Now
                //                                      orderby a.stb_StartDate descending
                //                                      select a;
                //        }
                //        finally
                //        {
                //            crmServiceContext.Dispose();
                //        }

                //        foreach (var item in crmShortCourseOfferings)
                //        {
                //            shortCourseOfferings.Add(new SelectListItem
                //            {
                //                Text = item.stb_OfferingNameEnglish,
                //                Value = item.Id.ToString()
                //            });
                //            model.ShortCourseInfo.Add(new ShortCourseInfo
                //            {
                //                OfferingName = item.stb_OfferingNameEnglish,
                //                CourseId = item.Id.ToString(),
                //                //CourseAdministrator = item.stb_AdministratorName,
                //                OfferingStartDate = ResolveCourseStartDate(item.stb_StartDate.Value.AddDays(1).ToString())
                //            });



                //            model.CourseStartDate = ResolveCourseStartDate(item.stb_StartDate.ToString() ?? "");
                //            model.ShortCourseOfferingType =
                //                (ShortCourseOfferingType)
                //                Enum.ToObject(typeof(ShortCourseOfferingType),
                //                    item.stb_OfferingTypeOptionSet.GetValueOrDefault());

                //            //model.CourseAdministrator = item.stb_AdministratorName;
                //        }
                //    }
                //    return shortCourseOfferings;
                //}
                //else
                //{
                    model.ShortCourseInfo = new List<ShortCourseInfo>();
                    shortCourseOfferings.Add(new SelectListItem { Text = "< Select Course >", Value = "none" });

                    using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
                    {
                        IQueryable<stb_shortcourseoffering> crmShortCourseOfferings = null;

                        try
                        {
                            crmShortCourseOfferings = from a in crmServiceContext.stb_shortcourseofferingSet
                                                      where
                                                      a.stb_ShortCourseLookup.Id == Guid.Parse(shortCourseId) &&
                                                      a.statuscode == (int)stb_shortcourseoffering_statuscode.OpenforApplication
                                                      // && a.stb_ApplicationClosingDate >= DateTime.Now
                                                      orderby a.stb_StartDate descending
                                                      select a;
                        }
                        finally
                        {
                            crmServiceContext.Dispose();
                        }

                        foreach (var item in crmShortCourseOfferings)
                        {
                            shortCourseOfferings.Add(new SelectListItem
                            {
                                Text = item.stb_OfferingNameEnglish,
                                Value = item.Id.ToString()
                            });
                            model.ShortCourseInfo.Add(new ShortCourseInfo
                            {
                                OfferingName = item.stb_OfferingNameEnglish,
                                CourseId = item.Id.ToString(),
                                //CourseAdministrator = item.stb_AdministratorName,
                                OfferingStartDate = ResolveCourseStartDate(item.stb_StartDate.Value.AddDays(1).ToString())
                            });


                          //  model.CourseName = item.stb_OfferingNameEnglish;
                            model.CourseStartDate = ResolveCourseStartDate(item.stb_StartDate.ToString() ?? "");
                            model.ShortCourseOfferingType =
                                (ShortCourseOfferingType)
                                Enum.ToObject(typeof(ShortCourseOfferingType),
                                    item.stb_OfferingTypeOptionSet.GetValueOrDefault());

                            //model.CourseAdministrator = item.stb_AdministratorName;
                        }
                    }
                }
           // }

            return shortCourseOfferings;
        }

        public static void SetShortCourseOfferingValues(ApplicationForm model, Guid shortCourseOfferingId)
        {

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<stb_shortcourseoffering> crmShortCourseOfferings = null;

                try
                {
                    crmShortCourseOfferings = from a in crmServiceContext.stb_shortcourseofferingSet
                                              where
                                              a.stb_shortcourseofferingId == shortCourseOfferingId &&
                                              //a.stb_ShortCourseLookup.Id == Guid.Parse(shortCourseId) &&
                                              a.statuscode == (int)stb_shortcourseoffering_statuscode.OpenforApplication                                              
                                              select a;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                foreach (var item in crmShortCourseOfferings)
                {
                    model.CourseStartDate = ResolveCourseStartDate(item.stb_StartDate.ToString());
                    model.ShortCourseOfferingType =
                        (ShortCourseOfferingType)Enum.ToObject(typeof(ShortCourseOfferingType), item.stb_OfferingTypeOptionSet.GetValueOrDefault());
                }
            }
             
        }

        public static Dictionary<string, int> GetOptionSetValues(string entityName, string fieldName)
        {
            var result = new Dictionary<string, int>();

            using (var organizationService = new OrganizationService(CrmServer.CrmConnection))
            {
                RetrieveAttributeRequest newAttributeRequest = new RetrieveAttributeRequest
                {
                    EntityLogicalName = entityName,
                    LogicalName = fieldName.ToLower(),
                    RetrieveAsIfPublished = true
                };

                RetrieveAttributeResponse picklistResponse = null;
                try
                {
                    picklistResponse = (organizationService.Execute(newAttributeRequest) as RetrieveAttributeResponse);
                }
                finally
                {
                    organizationService.Dispose();
                }

                if (picklistResponse != null)
                {
                    var picklistMetaDetails = (picklistResponse.AttributeMetadata as PicklistAttributeMetadata);

                    if (picklistMetaDetails != null)
                    {
                        var optionsList = picklistMetaDetails.OptionSet.Options.ToArray();

                        if (optionsList != null)
                        {
                            foreach (var option in optionsList)
                            {
                                result.Add(option.Label.UserLocalizedLabel.Label, option.Value.Value);
                            }
                        }
                    }
                }
            }

            return result;
        }

        public static ApplicationForm GetContact(ApplicationForm model, string studentNumber)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                var contact = crmServiceContext.ContactSet.FirstOrDefault(c => c.stb_USNumber == studentNumber);

                if (contact.IsNotNull())
                {
                    stb_enrollment enrolment = null;

                    if (model.Enrolment == Guid.Empty)
                    {
                        enrolment =
                            crmServiceContext.stb_enrollmentSet.FirstOrDefault(a => a.stb_USNumber == studentNumber);
                    }

                    return ContactMapper.MapContactToApplication(model, contact, enrolment);
                }

                return model;
            }
        }

        public static List<Document> GetRequiredDocuments(Guid shortCourseId,bool Webservice)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                List<Document> requiredDocuments = null;

                try
                {
                    requiredDocuments = (from reqDocs in crmServiceContext.stb_requireddocumentSet
                                         join docType in crmServiceContext.stb_documenttypeSet
                                         on reqDocs.stb_DocumentTypeLookup.Id equals docType.stb_documenttypeId
                                         where reqDocs.stb_ShortCourseLookup.Id == shortCourseId
                                         select new Document
                                         {
                                             DocumentTypeId = docType.stb_documenttypeId,
                                             Description = docType.stb_name,
                                             Level = reqDocs.stb_RequirementLevelOptionSet.GetValueOrDefault()
                                         }).ToList();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (requiredDocuments.IsNull())
                {
                    throw new Exception();
                }

                return requiredDocuments;
            }
        }
        public static List<Document> GetRequiredDocuments(Guid shortCourseId, int level = 1)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {

                List<Document> requiredDocuments = null;

                var programmeOfferingName = from a in crmServiceContext.stb_shortcourseSet
                                            where a.stb_ShortCourseName == "Africa Directors Programme"
                                            && a.stb_shortcourseId == shortCourseId
                                            select a;

                //if (programmeOfferingName.IsNotNull())
                //{
                //    try
                //    {
                //        requiredDocuments = (from reqDocs in crmServiceContext.stb_requireddocumentSet
                //                             join docType in crmServiceContext.stb_documenttypeSet
                //                             on reqDocs.stb_DocumentTypeLookup.Id equals docType.stb_documenttypeId
                //                             where reqDocs.stb_ShortCourseLookup.Id == shortCourseId &&
                //                                   reqDocs.stb_RequirementLevelOptionSet == level
                //                             select new Document
                //                             {
                //                                 DocumentTypeId = docType.stb_documenttypeId,
                //                                 Description = docType.stb_name,
                //                                 Level = reqDocs.stb_RequirementLevelOptionSet.GetValueOrDefault()
                //                             }).ToList();
                //    }
                //    finally
                //    {
                //        crmServiceContext.Dispose();
                //    }

                //    if (requiredDocuments.IsNull())
                //    {
                //        throw new Exception();
                //    }

                //    return requiredDocuments;
                //}
                //else
                //{


                    try
                    {
                        requiredDocuments = (from reqDocs in crmServiceContext.stb_requireddocumentSet
                                             join docType in crmServiceContext.stb_documenttypeSet
                                             on reqDocs.stb_DocumentTypeLookup.Id equals docType.stb_documenttypeId
                                             where reqDocs.stb_ShortCourseLookup.Id == shortCourseId &&
                                                   reqDocs.stb_RequirementLevelOptionSet == level
                                             select new Document
                                             {
                                                 DocumentTypeId = docType.stb_documenttypeId,
                                                 Description = docType.stb_name,
                                                 Level = reqDocs.stb_RequirementLevelOptionSet.GetValueOrDefault()
                                             }).ToList();
                    }
                    finally
                    {
                        crmServiceContext.Dispose();
                    }

                    if (requiredDocuments.IsNull())
                    {
                        throw new Exception();
                    }
                //}
                return requiredDocuments;
            }
        }

        public static string GetUsNumber(string email)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                try
                {
                    var contact = (from c in crmServiceContext.ContactSet
                        where c.EMailAddress1 == email
                        select c).FirstOrDefault();

                    if (contact.IsNotNull())
                    {
                        return contact.stb_USNumber;
                    }
                }
                finally
                {
                    crmServiceContext.Dispose();
                }
            }

            return string.Empty;
        }

        public static string GetSaIdType(Guid nationalityGuid)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                try
                {
                    var selectedNationality = (from a in crmServiceContext.stb_nationalitySet
                        where a.stb_nationalityId == nationalityGuid
                        select a.stb_NameEnglish).FirstOrDefault();

                    if (selectedNationality.IsNotNullOrEmpty())
                    {
                        var selectedIdType = (from a in crmServiceContext.stb_IdtypeSet
                            where a.stb_NameEnglish == "ID Number"
                            select a.stb_IdtypeId).FirstOrDefault();

                        if (selectedIdType.IsNotNull())
                        {
                            return selectedIdType.ToString();
                        }
                    }

                    return string.Empty;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }
            }
        }

        public static Guid GetCourseOfferingOwner(Guid contactDetailsOffering)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                stb_shortcourseoffering crmOffering = null;
                try
                {
                    crmOffering = (from x in crmServiceContext.stb_shortcourseofferingSet
                        where x.Id == contactDetailsOffering
                        select x).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (crmOffering == null)
                {
                    throw new Exception();
                }
                else
                {
                    return crmOffering.OwnerId.Id;
                }
            }
        }

        public static AccountDetails GetAccount(Guid? accountId)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                try
                {
                    var accountDetails = (from a in crmServiceContext.AccountSet
                        where a.AccountId == accountId
                        select new AccountDetails
                        {
                            AccountId = a.Id,
                            Name = a.Name,
                            AccountNumber = a.AccountNumber
                        }).FirstOrDefault();

                    if (!accountDetails.IsNotNull()) return accountDetails;

                    if (accountDetails.AccountNumber.IsNotNullOrEmpty())
                    {
                        if(accountDetails.AccountNumber == _BasilRead)
                            accountDetails.AccountType = AccountType.BasilRead;
                        if (accountDetails.AccountNumber == _GetSmarter)
                            accountDetails.AccountType = AccountType.GetSmarter;
                        if (accountDetails.AccountNumber == _MasterStart)
                            accountDetails.AccountType = AccountType.MasterStart;
                    }
                    else
                    {
                        accountDetails.AccountType = AccountType.None;
                    }

                    return accountDetails;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }
            }
        }

        public static VoucherStatus GetVoucherStatus(Guid? enrollmentGuid)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                VoucherStatus voucherStatus = VoucherStatus.Redeemable;

                try
                {
                    var voucher = (from a in crmServiceContext.stb_voucherSet
                        where a.stb_EnrollmentLookup.Id == enrollmentGuid.GetValueOrDefault()
                        select a).FirstOrDefault();

                    if (voucher.IsNotNull())
                    {
                        voucherStatus =
                            (VoucherStatus) Enum.ToObject(typeof(VoucherStatus), voucher.statuscode.GetValueOrDefault());
                    }
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                return voucherStatus;
            }
        }

        public static void SetDelegateStatus(Guid accountId)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                try
                {
                    var delegateDetails = (from a in crmServiceContext.stb_delegateSet
                        where a.stb_AccountLookup.Id == accountId
                        select a).FirstOrDefault();

                    if (delegateDetails.IsNotNull())
                    {
                        delegateDetails.statuscode = (int) DelegateStatus.Applied;

                        crmServiceContext.UpdateObject(delegateDetails);
                        crmServiceContext.SaveChanges();
                    }
                }
                finally
                {
                    crmServiceContext.Dispose();
                }
            }
        }

        public static decimal GetShortCourseOfferingFee(Guid shortCourseOfferingId)
        {
            decimal feeAmount = 0;

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                try
                {
                    var shortCourse = (from x in crmServiceContext.stb_shortcourseofferingSet
                        where x.stb_shortcourseofferingId == shortCourseOfferingId
                        select x).FirstOrDefault();

                    if (shortCourse.IsNotNull())
                    {
                        System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
                        customCulture.NumberFormat.NumberDecimalSeparator = ".";

                        System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

                        var shortCourseOfferingFee = string.Format(customCulture, shortCourse.stb_ShortCourseOfferingFee.GetValueOrDefault().ToString("F2"));

                        feeAmount = decimal.Parse(shortCourseOfferingFee);
                    }

                    //Shortcoursfee code
                    //var shortCourse = (from a in crmServiceContext.stb_shortcourseofferingSet
                    //                   join b in crmServiceContext.stb_shortcourseSet
                    //                   on a.stb_ShortCourseLookup.Id equals b.stb_shortcourseId
                    //                   where a.stb_shortcourseofferingId == shortCourseOfferingId
                    //                   select b).FirstOrDefault();



                    //if (shortCourse.IsNotNull())
                    //{
                    //    System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
                    //    customCulture.NumberFormat.NumberDecimalSeparator = ".";

                    //    System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

                    //    var shortCourseOfferingFee = string.Format(customCulture, shortCourse.stb_ShortCourseFee.GetValueOrDefault().ToString("F2"));

                    //    feeAmount = decimal.Parse(shortCourseOfferingFee);
                    //}
                }
                finally
                {
                    crmServiceContext.Dispose();
                }
            }

            return feeAmount;
        }

        public static bool ApplicationClosed(Guid offeringId)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                IQueryable<stb_shortcourseoffering> crmShortCourseOfferings = null;

                try
                {
                    crmShortCourseOfferings = from a in crmServiceContext.stb_shortcourseofferingSet
                                              where
                                              a.stb_shortcourseofferingId == offeringId &&
                                              a.stb_ApplicationClosingDate >= DateTime.Now &&
                                              a.statuscode == (int)stb_shortcourseoffering_statuscode.OpenforApplication
                                              select a;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if(crmShortCourseOfferings.IsNotNull())
                {
                    //Application is open
                    return false;
                }
                else
                { return true; }              
            }
        }
       



        #endregion

        #region Private Methods

        private static string ResolveCourseStartDate(string value)
        {
            DateTime dateValue;

            return DateTime.TryParse(value, out dateValue) ? dateValue.ToString("dd MMM yyyy") : null;
        }


        #endregion
    }
}
