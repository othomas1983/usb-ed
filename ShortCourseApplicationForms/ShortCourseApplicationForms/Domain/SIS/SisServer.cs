﻿using System.Configuration;
using SettingsMan = ShortCourseApplicationForm.Domain.Properties.Settings;

namespace ShortCourseApplicationForm.Domain.SIS
{
    public static class SisServer
    {
        private const string key = "SISEndpoint";

        public static string SisConnection
        {
            get
            {
                var setting = ConfigurationManager.AppSettings[key] ?? SettingsMan.Default.SISEndpoint;

                return setting;
            }
        }
    }
}
