﻿/// <reference path="../knockout/knockout-3.2.0.js" />
/// <reference path="../knockout/knockout.validation.min.js" />
/// <reference path="../knockout/knockoutCommon.js" />

var knockoutValidationSettings = {
    insertMessages: true,
    decorateElement: true,
    errorElementClass: 'input-validation-error',
    messagesOnModified: true,
    decorateElementOnModified: true,
    decorateInputElement: true
};

function ApplicationFormViewModel() {
    var self = this;

    $(document)
        .ready(function () {
            $("input:text,form").attr("autocomplete", "off");
            $("#dialog").hide();
            $("#progressDialog").hide();
            $(".paymentInfoButton").hide();
            $("#dateOfBirth")
                .datepicker({
                    dateFormat: "dd/mm/yy",
                    changeMonth: true,
                    changeYear: true,
                    yearRange: "-100:+0"
                });
            $("#idExpiryDate")
                .datepicker({
                    dateFormat: "dd/mm/yy",
                    changeMonth: true,
                    changeYear: true,
                    yearRange: "+0:c+30"
                });
        });

    //self.MobileNumber = ko.observable();

    //self.MobileNumber.extend({
    //    required: true,
    //    pattern: {
    //        message: "Invalid phone number.",
    //        params: /^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]*$/g
    //    }
    //});

    $("#MobileNumberId").change(function () {

        if ($("#MobileNumberId").val() != undefined) {
            $("#MobileNumberId").intlTelInput("setNumber", $("#MobileNumberId").val());
            var intlNumber = $("#MobileNumberId").intlTelInput("getNumber");

            $("#MobileNumberId").val(intlNumber);

            if ($('#MobileNumberId').attr('placeholder') != undefined && intlNumber !== "") {
                var numberLength = $('#MobileNumberId').attr('placeholder').replace(/\s/g, "").length;
                if ($("#MobileNumberId").val().length < numberLength) {
                    alert("Please enter at least " + numberLength + " characters");
                    //self.MobileNumber.setError("Please enter at least " + numberLength + " characters.");
                    //self.MobileNumber.isModified(true);
                } else if (!$("#MobileNumberId").intlTelInput("isValidNumber")) {
                    alert("Invalid number for selected country");
                    //self.MobileNumber.setError("Invalid number for selected country.");
                    //self.MobileNumber.isModified(true);
                } else {
                    //self.MobileNumber.clearError();
                }
            }
        }

    });

    $("#MobileNumberId")
            .intlTelInput({
                allowExtensions: true,
                autoHideDialCode: true,
                autoPlaceholder: true,
                nationalMode: false,
                preferredCountries: ["za"],
                utilsScript: "/Content/js/utils.js"
            });

    $("#MobileNumberId")
        .on("countrychange",
            function (e, countryData) {
                var extension = $("#MobileNumberId").intlTelInput("getExtension");
                $("#MobileNumberId").intlTelInput("setNumber", "+" + extension);
            });

    $("#input-id")
        .on("change keyup paste click",
            function() {
                $('.input-validation-error').removeClass('input-validation-error');
            });

    $("#frmApplication :input")
        .change(function() {
            var control = $(this);
            control.removeClass('input-validation-error');
        });

    self.termsAndConditionSelected = ko.observable(false);
   
    resolveNationality = function (data) {
        var selectedText = $("#nationality option:selected").text();
        if (selectedText == "South Africa") {
            $(".personal-nonSA").hide();
            $(".personal-SA").show();
            $('#dateOfBirth').prop('readonly', true);
        }
        else if (selectedText != "") {
            $(".personal-nonSA").show();
            $(".personal-SA").hide();
            $('#dateOfBirth').prop('readonly', false);
            if (data) {                
                $("#ethnicityNonSA").val(data.Ethnicity);
            }
        }
        else {
            $(".personal-nonSA").hide();
            $(".personal-SA").hide();
            $('#dateOfBirth').prop('readonly', false);
            $('#dateOfBirth').val('');
        }
    };

    resolveQualification = function () {
        var selectedText = $("#qualificationType option:selected").text();
        $("#qualificationTypeOther").val('');
        if (selectedText == "" || selectedText == "Other" || selectedText == "Grade 12") {
            $("#qualificationField").hide(500);
        }
        else {
            $("#qualificationField").show(500);
        }
    };

    resolveCountries = function () {
        var selectedText = $("#countries option:selected").text();
        if (selectedText == "South Africa") {
            $("#postalContainer").show(500);
            $(".postal-nonSA").hide();
            $(".postal-SA").show();
            $("#postalSuburb").attr("readonly", true);
            $("#postalCity").attr("readonly", true);
            $("#postalCode").attr("readonly", true);
        } else if (selectedText != "") {
            //$("#nationality").val(selectedText);
            $("#postalContainer").show(500);
            $(".postal-nonSA").show();
            $(".postal-SA").hide();
            $("#postalSuburb").attr("readonly", false);
            $("#postalCity").attr("readonly", false);
            $("#postalCode").attr("readonly", false);
        } else {
            $("#postalContainer").hide(500);
        }

        $("#country").val(selectedText);
        $("#selectedCountry").val(selectedText);
    };

    resolveEmployment = function() {
        var isEmployed = document.getElementById("currentlyEmployeed").checked;
        if (isEmployed) {
            $("#workExperienceContainer").show();
        } else {
            $("#workExperienceContainer").hide();
        }
    };

    resolvePayment = function() {
        var selectedText = $("#payment option:selected").text();
        if (selectedText == "Company") {
            $(".companyContainer").show(500);
            $(".paymentDebtorCode").show();
        } else if (selectedText == "Department") {
            $(".companyContainer").show(500);
            $(".paymentDebtorCode").hide();
        } else if (selectedText == "Sponsored") {
            $(".companyContainer").show(500);
            $(".paymentDebtorCode").hide();
        } else {
            $(".companyContainer").hide(500);
        }
    };

    ResolveSection = function() {
        tab = $("#selectedTab").val();
        if (!tab || tab == "failed") {
            section = $("#validationSection").val();
            if (section == "PersonalInformation") {
                showPersonalInfoTab();
                return;
            }
            if (section == "ContactInformation") {
                showContactInfoTab();
                return;
            }
            if (section == "Qualifications") {
                showAcademicTab();
                return;
            }
            if (section == "Marketing") {
                showMarketingTab();
                return;
            }
       
            if (section == "PaymentInfo") {
                showPaymentInfoTab();
                return;
            }
            if (section == "ApplicationForm") {
                showPaymentInfoTab();
            }
        } else {

            if (tab == "personalInfoTab") {
                showPersonalInfoTab();
                return;
            }
            if (tab == "contactInfoTab") {
                showContactInfoTab();
                return;
            }
            if (tab == "academicTab") {
                showAcademicTab();
                return;
            }
            if (tab == "marketingTab") {
                showMarketingTab();
                return;
            }
            if (tab == "paymentInfoTab") {
                showPaymentInfoTab();
                return;
            }
        }
    };

    ResolveValidatedSections = function() {
        if ($("#section1Validated").val() == 'true') {
            $("#step1").addClass("validated");
            $("#step1").removeClass("notValidated");
        } else {
            $("#step1").removeClass("validated");
            $("#step1").addClass("notValidated");
        }

        if ($("#section2Validated").val() == 'true') {
            $("#step2").addClass("validated");
            $("#step2").removeClass("notValidated");
        } else {
            $("#step2").removeClass("validated");
            $("#step2").addClass("notValidated");
        }

        if ($("#section3Validated").val() == 'true') {
            $("#step3").addClass("validated");
            $("#step3").removeClass("notValidated");
        } else {
            $("#step3").removeClass("validated");
            $("#step3").addClass("notValidated");
        }

        if ($("#section4Validated").val() == 'true') {
            $("#step4").addClass("validated");
            $("#step4").removeClass("notValidated");
        } else {
            $("#step4").removeClass("validated");
            $("#step4").addClass("notValidated");
        }

        if ($("#section5Validated").val() == 'true') {
            $("#step5").addClass("validated");
            $("#step5").removeClass("notValidated");
        } else {
            $("#step5").removeClass("validated");
            $("#step5").addClass("notValidated");
        }
    };

    IsDisplayNameApproved = function () {
        if ($("#displayNameApproved").val() === "false") {
            alert("Please confirm the name that will be displayed on your certificate of competence");
            return false;
        }
        return true;
    };

    CourseSelected = function() {
        var selectedOffering = $("#shortCourseOfferings option:selected").val();

        if (selectedOffering == "none") {
            alert("Please select a valid course.");
            return false;
        }
        return true;
    };

    this.load = function (model) {
        $("#selectedTab").val(model.SelectedTab);

        var offeringGuid = model.Offering;

        ResolveDocumentsUploaded = function() {
            if (model.IdentificationDocumentUploadKey) {
                return false;
            }

            if (model.QualificationDocumentUploadKey) {
                return false;
            }
        };

        ResolveOffering = function() {
            var selectedOfferingId = model.Offering;

            var result = $(model.ShortCourseInfo)
                .filter(function(idx) {
                    return model.ShortCourseInfo[idx].CourseId === selectedOfferingId;
                });

            if (result.length > 0) {
                $("#offeringStartDate").text(result[0].OfferingStartDate);
                $("#shortCourseOfferings").val(selectedOfferingId);
            }
        };

        ResolveOffering();        

        if (model.DirectOfferingLink || model.InsideCRM ) {
            $("#offeringStartDate").html(model.CourseStartDate);
        }

        if (model.DisplayNameApproved) {
            $("#fullNameExcepted").prop("checked", true);
        }

        if (model.Email != null) {

            //if (model.Email != undefined || model.Email !== null) {
            //    $("#email").prop("readonly", true);
            //} else {
            //    $("#email").prop("readonly", false);
            //}

            if (model.SelectedTab === "personalInfoTab") {
                // Personal Info
                $("#email").val(model.Email);
                $("#title").val(model.Title);
                $("#gender").val(model.Gender);
                $("#nationality").val(model.Nationality);
                $("#ethnicity").val(model.Ethnicity);
                $("#documentType").val(model.ForeignIdType);
                $("#language").val(model.Language);
                $("#disability").val(model.Disability);
                $("#dietaryRequirements").val(model.DietaryRequirements);
                // Contact Info

                $("#countries").val(model.PostalCountry);
                $("#provinces").val(model.Province);

                if (model.PostalAfrigis != null || model.PostalAfrigis !== "") {
                    $("#distributionAreaList").val(model.PostalAfrigis);
                }

                // Work Experience
                $("#currentlyEmployeed").prop("checked", model.Employed);
                $("#currentlyNotEmployeed").prop("checked", !model.Employed);
                $("#industry").val(model.Industry);
                $("#workArea").val(model.WorkArea);
                // Qualification
                $("#qualificationType").val(model.QualificationType);
                $("#qualificationFields").val(model.QualificationField);
                $("#yearAchieved").val(model.YearAchieved);
                // Marketing
                $("#marketingSource").val(model.MarketingSource);
                $("#marketingReason").val(model.MarketingReason);
                //Payment
                $("#payment").val(model.PaymentResponsibility);
                $("#paymentCountries").val(model.PaymentPostalCountry);
            }
        }

        $("#displayNameApproved").val(model.DisplayNameApproved);

        if (model.DisplayNameApproved) {
            $("#fullNameExcepted").prop("checked", true);
        } else {
            $("#fullNameExcepted").prop("checked", false);
        }

        $("#emailSectionValidated").val(model.EmailSectionValidated);
        $("#section1Validated").val(model.Section1Validated);
        $("#section2Validated").val(model.Section2Validated);
        $("#section3Validated").val(model.Section3Validated);
        $("#section4Validated").val(model.Section4Validated);
        $("#section5Validated").val(model.Section5Validated);
        //$("#fullNameExcepted").prop("checked", model.DisplayNameApproved);

        $("#identificationDocumentUploadKey").val(model.IdentificationDocumentUploadKey);
        $("#qualificationDocumentUploadKey").val(model.QualificationDocumentUploadKey);
        $("#insideCRM").val(model.InsideCRM);
        $("#directOfferingLink").val(model.DirectOfferingLink);
        $("#shortCourseId").val(model.ShortCourseId);
        $("#shortCourseOfferingId").val(model.ShortCourseOfferingId);
        $("#webTokenId").val(model.WebTokenId);
        

        //var activeTab = "PersonalInformation";
        //var selectedTab = "PersonalInformation";
        ///* Tabs */
        //$("#personalInformation").show();
        //$("#personalInfoTab").addClass("active");

        $("#emailAddress").show();
        $("#personalInformation").hide();
        $("#contactInformation").hide();
        $("#qualifications").hide();
        $("#workExperience").hide();
        $("#marketing").hide();
        $("#paymentInfo").hide();

        ResolveValidatedSections();

        //section 1
        showPersonalInfoTab = function () {

            activeTab = "PersonalInformation";
            $("#personalInformation").show();
            $("#personalInfoTab").addClass("active");
            $("#contactInfoTab").removeClass("active");
            $("#academicTab").removeClass("active");
            $("#marketingTab").removeClass("active");
            $("#paymentInfoTab").removeClass("active");

            $("#emailAddress").hide();
            $("#contactInformation").hide();
            $("#qualifications").hide();
            $("#workExperience").hide();
            $("#marketing").hide();
            $("#paymentInfo").hide();
        };
        //section 2
        showContactInfoTab = function () {

            activeTab = "ContactInformation";

            $("#sectionOneValidated").val("true");
            $("#contactInformation").show();

            $("#contactInfoTab").addClass("active");
            $("#personalInfoTab").removeClass("active");
            $("#academicTab").removeClass("active");
            $("#marketingTab").removeClass("active");
            $("#paymentInfoTab").removeClass("active");

            $("#emailAddress").hide();
            $("#personalInformation").hide();
            $("#qualifications").hide();
            $("#workExperience").hide();
            $("#marketing").hide();
            $("#paymentInfo").hide();
        };
        //section 3
        showAcademicTab = function () {

            activeTab = "Qualifications";

            $("#sectionTwoValidated").val("true");
            $("#qualifications").show();
            $("#workExperience").show();

            //if (model.HasUploadedAcceptanceLetter) {
            //    document.getElementById("qualificationBtn").style.visibility = "visible";
            //} else {
            //    document.getElementById("qualificationBtn").style.visibility = "hidden";
            //}

            $("#academicTab").addClass("active");
            $("#personalInfoTab").removeClass("active");
            $("#contactInfoTab").removeClass("active");
            $("#marketingTab").removeClass("active");
            $("#paymentInfoTab").removeClass("active");

            $("#emailAddress").hide();
            $("#personalInformation").hide();
            $("#contactInformation").hide();
            $("#marketing").hide();
            $("#paymentInfo").hide();
        };
        //section 4
        showMarketingTab = function () {

            activeTab = "Marketing";

            $("#sectionThreeValidated").val("true");
            $("#marketing").show();

            $("#marketingTab").addClass("active");
            $("#academicTab").removeClass("active");
            $("#personalInfoTab").removeClass("active");
            $("#contactInfoTab").removeClass("active");
            $("#paymentInfoTab").removeClass("active");

            $("#emailAddress").hide();
            $("#qualifications").hide();
            $("#personalInformation").hide();
            $("#contactInformation").hide();
            $("#workExperience").hide();
            $("#paymentInfo").hide();
        };
        //section 5
        showPaymentInfoTab = function () {

            activeTab = "PaymentInfo";

            $("#sectionFourValidated").val("true");
            $("#paymentInfo").show();

            $("#paymentInfoTab").addClass("active");
            $("#academicTab").removeClass("active");
            $("#personalInfoTab").removeClass("active");
            $("#contactInfoTab").removeClass("active");
            $("#marketingTab").removeClass("active");

            $("#emailAddress").hide();
            $("#qualifications").hide();
            $("#personalInformation").hide();
            $("#contactInformation").hide();
            $("#workExperience").hide();
            $("#marketing").hide();
        };

        ResolveSelectedTab = function(activeTab, tab) {
            $("#validationSection").val(activeTab);
            $("#selectedTab").val(tab);
            $(".paymentInfoButton").click();
        };

        $("#shortCourseOfferings").change(function () {
            var selectedOffering = $("#shortCourseOfferings option:selected").text();
            
            var offering = $(model.ShortCourseInfo).filter(function (idx) {
                return model.ShortCourseInfo[idx].OfferingName === selectedOffering;
            });
            
            if (offering) {
                if (offering.length > 0) {
                    $("#offeringStartDate").html(offering[0].OfferingStartDate);
                    $("#offering").val(offering[0].CourseId);
                }
            }
        });

        

        $("#personalInfoTab").click(function () {
            if (!IsDisplayNameApproved()) {
                return;
            }

            if (!CourseSelected()) {
                return;
            }
            selectedTab = "personalInfoTab";
            ResolveSelectedTab(activeTab, selectedTab);
        });

        $("#contactInfoTab").click(function () {
            if (!IsDisplayNameApproved()) {
                return;
            }

            if (!CourseSelected()) {
                return;
            }

            selectedTab = "contactInfoTab";
            ResolveSelectedTab(activeTab, selectedTab);
        });

        $("#academicTab").click(function () {
            if (!IsDisplayNameApproved()) {
                return;
            }

            if (!CourseSelected()) {
                return;
            }

            selectedTab = "academicTab";
            ResolveSelectedTab(activeTab, selectedTab);
        });

        $("#marketingTab").click(function () {
            if (!IsDisplayNameApproved()) {
                return;
            }

            if (!CourseSelected()) {
                return;
            }
            selectedTab = "marketingTab";
            ResolveSelectedTab(activeTab, selectedTab);
        });

        $("#paymentInfoTab").click(function () {
            if (!IsDisplayNameApproved()) {
                return;
            }

            if (!CourseSelected()) {
                return;
            }
            selectedTab = "paymentInfoTab";
            ResolveSelectedTab(activeTab, selectedTab);
        });

        if ($("#validationSection"))
        {
           ResolveSection();
        }

        if ($("#currentlyEmployeed")) {
            if($("#currentlyEmployeed").is(':checked')){
                $("#workExperienceContainer").show();
            }
            else {
                $("#workExperienceContainer").hide();
            }
        }

        if ($("#nationality")) {
            if (model.IdNumber) {
                var dob = GetDateOfBirthFromIdNumber(model.IdNumber);
                var year = Number(dob.year);
                var month = Number(dob.month) - 1;
                var day = Number(dob.day);
                var dobFormat = new Date(year, month, day);
                $("#dateOfBirth").datepicker("setDate", dobFormat);
            }
            else {
                $("#dateOfBirth").datepicker('setDate', $("#dateOfBirthYear").val() + "-" + $("#dateOfBirthMonth").val() + "-" + $("#dateOfBirthDay").val());
            }

            resolveNationality(model);
        }

        resolveEmployment();

        if ($("#qualificationType")) {
            resolveQualification();
        }

        if ($("#countries")) {
            resolveCountries();
        }

        if ($("#payment")) {
            resolvePayment();
        }

        $("#identityNumber").blur(function () {
            var dateOfBirth = GetDateOfBirthFromIdNumber($("#identityNumber").val());
            var year = Number(dateOfBirth.year);
            var month = Number(dateOfBirth.month) - 1;
            var day = Number(dateOfBirth.day);
            var dobFormat = new Date(year, month, day);
            $("#dateOfBirth").datepicker("setDate", dobFormat);
        });

        if ($("#foreignIdNumber")) {
            if (model.ForeignIdNumber) {
                $("#foreignIdNumber").val(model.ForeignIdNumber);
          }  
        };

        if ($("#dateOfBirth")) {
            if (model.DateOfBirth) {
                var dateOfBirth = parseJsonDate(model.DateOfBirth);
                $("#dateOfBirth").datepicker("setDate", dateOfBirth);
            }
        };

        if ($("#idExpiryDate")) {
            if (model.ForeignIdExpiryDate) {
                var expiryDate = parseJsonDate(model.ForeignIdExpiryDate);
                $("#idExpiryDate").datepicker("setDate", expiryDate);
            }
        };

        function parseJsonDate(jsonDateString) {
            return new Date(parseInt(jsonDateString.replace('/Date(', '')));
        }

        if ($("#documentType")) {
            var selectedText = $("#documentType option:selected").text();
                $("#idType").html(selectedText);
        };

        function GetDateOfBirthFromIdNumber(idNumber) {

            var date = {};
            var year = parseInt(idNumber.toString().substring(0, 2));
            var currentCentury = parseInt(new Date().getFullYear().toString().substring(0, 2) + "00");
            var lastCentury = currentCentury - 100;
            var currentYear = parseInt(new Date().getFullYear().toString().substring(2, 4));

            if (year > currentYear) {
                year += lastCentury;
            }
            else {
                year += currentCentury;
            }

            date.year = year;
            date.month = idNumber.substring(2, 4);
            date.day = idNumber.substring(4, 6);

            return date;
        }

        var viewModel = function() {

            var self = this;

            self.displayNameApproved = ko.observable();
            self.termsAndConditionsRead = ko.observable();
            self.idTypeNumber = ko.observable();
            self.idTypeExpiryDate = ko.observable();
            self.selectedPaymentResponsibility = ko.observable();
            //self.selectedQualificationYearAchieved = ko.observable();
            self.titleText = ko.observable();

            self.Enrolment = ko.observable(model.Enrolment ? model.Enrolment : "");
            self.firstNames = ko.observable(model.FirstNames ? model.FirstNames : "");
            self.surname = ko.observable(model.Surname ? model.Surname : "");
            self.initials = ko.observable(model.Initials ? model.Initials : "");

            self.hasOpenedAcceptanceLetter = ko.observable(false);

            self.enableSubmitButton = function() {
                    var checked = $("#fullNameExcepted").is(":checked");
                    self.displayNameApproved(checked);
                    return self.termsAndConditionsRead() && checked;
                },
                self.fullName = ko.computed(function() {
                        return self.firstNames() + " " + self.surname();
                    },
                    viewModel);

            self.initials = ko.computed(function() {
                    var names = self.firstNames().split(" ");
                    var result = "";

                    if (names.length > 0) {
                        for (var i = 0; i < names.length; i++) {
                            result += names[i].substring(0, 1);
                        }
                    }
                    return result;
                },
                viewModel);

            initialsChanged = function() {
                var initials = $("#initials").val();
                $("#displayName").val(initials + " " + self.surname());
            }

            $("#fullNameExcepted")
                .change(function () {
                    var result = $("#fullNameExcepted").is(":checked") && self.termsAndConditionsRead();
                    $("#submit").prop("disabled", !result);
                    self.displayNameApproved($("#fullNameExcepted").is(":checked"));
                });

            self.enableSubmitButton();

            return self;
        };

        $("#documentType")
            .change(function() {
                var selectedText = $("#documentType option:selected").text();
                $("#idType").html(selectedText);
            });

        $("#nationality").change(function () {
            resolveNationality();
            $("#identityNumber").val("");
            $("#foreignIdNumber").val("");
        });

        $("#qualificationType").change(function () {
            resolveQualification();

            $("#QualificationField").val("");
            $("#QualificationInstitution").val("");
            $("#YearAchieved").val("");
        });

        $("#countries").change(function () {
            resolveCountries();
            
            //$("#postalAfrigis").val("");
            //$("#distributionAreaList").val("");
            $("#PostalStreet1").val("");
            $("#PostalStreet2").val("");
            //$("#postalSuburb").val("");
            //$("#postalCity").val("");
            //$("#postalCode").val("");
            //$("#PostalSuburb").val("");
            //$("#PostalCity").val("");
            //$("#PostalPostalCode").val("");

        });

        $(".emailAddressButton")
            .click(function() {
                $("#validationSection").val("EmailAddress");
                $("#selectedTab").val("personalInfoTab");
            });

        $(".personalInfoButton").click(function () {
            $("#validationSection").val("PersonalInformation");
            $("#selectedTab").val("contactInfoTab");
        });

        $(".contactInfoButton").click(function () {
            $("#validationSection").val("ContactInformation");
            $("#selectedTab").val("academicTab");
        });

        $(".qualificationButton")
            .click(function() {
                $("#validationSection").val("Qualifications");
                $("#selectedTab").val("marketingTab");
            });

        $(".marketingButton").click(function () {
            $("#validationSection").val("Marketing");
            $("#selectedTab").val("paymentInfoTab");
        });

        $(".paymentInfoButton").click(function () {
            if (activeTab) {
                $("#validationSection").val(activeTab);
                return;
            }
            $("#validationSection").val("PaymentInfo");
        });

        $("#mainSubmitButton").click(function() {
            $("#validationSection").val("ApplicationForm");
        });

        $("#payment").change(function () {
            resolvePayment();

            $("#PaymentContactPerson").val("");
            $("#PaymentContactNumber").val("");
            $("#PaymentContactEmail").val("");
            $("#PaymentDebtorCode").val("");
        });

        submitForm = function (e) {
            var form = $("#frmApplication");
           
            $.ajax({
                type: "POST",
                cache: true,
                url: "ApplicationForm/Index",
                data: JSON.stringify(model, "Koos"),
                dataType: "json",
                success: function (data) {
                   form.submit();
                }
            });
        };

        ko.applyBindings(viewModel, document.getElementById("applicationForm"));

        $(function () {
            var controls = $('#workExperience').find("input[id^='workDate_']");
            for (var i = 0; i < controls.length; i++) {
                if (controls[i]) {
                    $(controls[i]).datepicker({
                        showButtonPanel: true,
                        changeMonth: true,
                        changeYear: true,                        
                        minDate: "-100Y",
                        maxDate: 0,
                        yearRange: "1900: new Date()",
                        dateFormat: 'dd/mm/yyyy',
                        beforeShow: function (input, inst) {
                            var control = $('#workExperience').find("input[id=" + inst.id + "]");
                            control.addClass("calendar-off");
                        },
                        onClose: function (dateText, inst) {
                            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                            var control = $('#workExperience').find("input[id=" + inst.id + "]");
                            control.datepicker('setDate', new Date(year, month, 1));
                        },
                    });
                }
            }
        });

        $(function() {
            $('#dateOfBirth').datepicker({
                dateFormat: "dd/mm/yy",
                changeMonth: true,
                changeYear: true,
                yearRange: "-100:+0"
            });
        });

        $(function () {
            $('#idExpiryDate').datepicker({
                dateFormat: "dd/mm/yy",
                changeMonth: true,
                changeYear: true,
                yearRange: "+0:c+30"
            });
        });

        $("#currentlyEmployeed").change(function () {
            if ($(this).is(':checked')) {
                $("#workExperienceContainer").show(500);
            }
            else {
                $("#workExperienceContainer").hide(500);
            }
        });

        $("#currentlyNotEmployeed").change(function () {
            if ($(this).is(':checked')) {
                $("#workExperienceContainer").hide(500);
            }
            else {
                $("#workExperienceContainer").show(500);
            }
        });

        $("#studentSearch").click(function () {           
            showProgressDialog();
            searchCriteria = $("#studentNumber").val();

            var url = $(this).data('request-url') + "?studentNumber=" + searchCriteria + "&shortCourseOfferingId=" + model.ShortCourseOfferingId + "&webTokenId=" + model.WebTokenId;
           
            location.href = url;
        });

        $("#postalDistribution").click(function () {
            searchCriteria = $("#postalAfrigisId").val();
            if (!searchCriteria) {
                alert("Please enter Postal Code or Suburb or City/Town");
                return;
            }
            $.ajax({                
                type: "POST",
                dataType: "json",        
                url:"ApplicationForm/GetDistributionAreas?query=" + searchCriteria,
                data: {"query" : searchCriteria },
                contentType: 'application/json; charset=utf-8',
                error:function(x,e){
                    alert("An error occurred while trying to load the data. Please try again later or contact your service administrator.");
                },
                success: function (data) {
                    populateDistributionAreas(data);
                }
            });
        });
        
        $("#distributionAreaList").change(function () {
            var selectedArea = $("#distributionAreaList option:selected").text();

            var area = selectedArea.split(',');
            if (area.length === 1) {
                return false;
            }

            $("#postalSuburb").val(area[0]);
            $("#postalCity").val(area[1]);
            $("#postalCode").val(area[2]);
        });

        populateDistributionAreas = function (areas) {
            if (areas.length > 0) {
                if ($('#distributionAreaList').length >= 0) {
                    $('#distributionAreaList')[0].options.length = 0;
                }
                var options = $("#distributionAreaList");
                $.each(areas, function () {
                    options.append(new Option(this.Text, this.Value));
                });
            }
            else {
                alert("The address search returned no results for your query. Check the spelling and try filling in your town or city which will return the broadest search. Remember to fill in either the one or the other, for instance, do not supply BOTH suburb AND city.");                
            }
        };
            
        $("#frmApplication").submit(function (e) {
            showProgressDialog();
        });

        function showProgressDialog() {
            $.LoadingOverlay("show");
        };

        function showDialog(message) {
            $(".message").html(message);
            $("#dialog").dialog(
                    {              
                        modal: true,
                        draggable: false,
                        resizable: false,
                        width: 470,
                        dialogClass: 'no-close success-dialog',
                        buttons: {
                            "OK": function () {                                
                                $(this).dialog("close");
                                ClearForm();
                            }
                        }
                    });
        };

        function ClearForm() {           
            document.location.reload();
        };

        $("#acceptanceLetterButton")
            .click(function () {

                hasOpenedAcceptanceLetter(false);

                $.ajax({
                    url: "ApplicationForm/UploadAcceptanceForm?usNumber=" + model.UsNumber + "&enrolment=" + model.Enrolment + "&firstNames=" + model.FirstNames + "&surname=" + model.Surname,
                    type: "GET",
                    //data: JSON.stringify(model),
                    contentType: "application/json",
                    success: function(data) {
                        if (data != undefined && data.result === "OK") {
                            var dataUri = "data:application/pdf;base64," + data.documentUrl;
                            window.open(dataUri, "_blank");
                            hasOpenedAcceptanceLetter(true);
                            $("#hasUploadedAcceptanceLetter").val(true);
                        }
                    },
                    error: function(e) {

                    }
                });

            });

        $('img#captcha-refresh').click(function () {

            change_captcha();
        });

        function change_captcha() {
            document.getElementById('captcha').src = "get_captcha.php?rnd=" + Math.random();
        }
    }
}
