﻿$(document)
    .ready(function() {

        $("#idUploadProgress").hide();
        $("#certificateUploadProgress").hide();
        $("#acceptanceFormUploadProgress").hide();
        $("#upload_Id").val('');
        $("#upload_Certificate").val('');
        $("#upload_AcceptanceForm").val('');


        $("#uploadBtn_Id")
            .click(function() {
                document.getElementById("idUpload").disabled = true;
            });

        $("#uploadBtn_Cert")
            .click(function() {
                document.getElementById("certificateUpload").disabled = true;
            });

        document.getElementById("fileuploadId").onchange = function() {
            var filename = this.value.substring(this.value.lastIndexOf('\\') + 1);
            document.getElementById("upload_Id").value = filename;
            document.getElementById("fileuploadId").disabled = false;

        };

        document.getElementById("fileuploadCert").onchange = function() {
            var filename = this.value.substring(this.value.lastIndexOf('\\') + 1);
            document.getElementById("upload_Certificate").value = filename;
            document.getElementById("fileuploadCert").disabled = false;
        };

        document.getElementById("fileuploadAcceptanceForm").onchange = function () {
            var filename = this.value.substring(this.value.lastIndexOf('\\') + 1);
            document.getElementById("upload_AcceptanceForm").value = filename;
            document.getElementById("fileuploadAcceptanceForm").disabled = false;
        };
    });