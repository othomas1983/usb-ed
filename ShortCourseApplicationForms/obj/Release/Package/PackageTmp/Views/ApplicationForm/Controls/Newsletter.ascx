﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ShortCourseApplicationForm.Domain.Models.ShortCourseApplicationForm.ApplicationForm>" %>


<div id="newsLetterSection">
     <br />
    <fieldset>
    <legend>Newsletter</legend>
        <label for="checkboxNewsletter">Subscribe to the Thought Thursday email newsletter</label>
        &nbsp;
        <input type="checkbox" name="SubscribeToNewsletter" value="true" checked="checked" />
        </fieldset>


     <div class="contactInfoNavigateButton">
        <input type="submit" class="navigateButton marketingButton" value="Next"/>
    </div>
</div>