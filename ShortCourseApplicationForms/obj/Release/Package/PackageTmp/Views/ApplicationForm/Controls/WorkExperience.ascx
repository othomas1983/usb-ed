﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ShortCourseApplicationForm.Domain.Models.ShortCourseApplicationForm.ApplicationForm>" %>

<fieldset id="workExperienceSection">
    <legend>Current Employment</legend>

    <div>
        <label>Currently Employed</label>       
        <%: Html.RadioButtonFor(m => m.Employed, true, new { @id = "currentlyEmployeed" })%>
    </div>
    <div>
        <br />
        <label>Currently Not Employed</label>
        <%: Html.RadioButtonFor(m => m.Employed, false, new { @id = "currentlyNotEmployeed"})%>
    </div>
    <br />
    <div id="workExperienceContainer">
        <div>
            <label>Industry</label>
            <%: Html.DropDownListFor(m => m.Industry, (List<SelectListItem>)ViewBag.Industries, new { @id = "industry" }) %>
        </div>
        <div>
            <label>Work Area</label>
            <%: Html.DropDownListFor(m => m.WorkArea, (List<SelectListItem>)ViewBag.WorkAreas, new { @id = "workArea" }) %>
        </div>
        <div>
            <label>Job Title</label>
            <%: Html.TextBoxFor(m => m.JobTitle, new { @Value = Model.JobTitle }) %>
        </div>
        <div>
            <label>Current Employer</label>
            <%: Html.TextBoxFor(m => m.Employer, new { @Value = Model.Employer }) %>
        </div>
        <div>
            <label>Employer Contact Number</label>
            <%: Html.TextBoxFor(m => m.EmployerContactNumber, new { @Value = Model.EmployerContactNumber })%>
        </div>
    </div>
</fieldset>
