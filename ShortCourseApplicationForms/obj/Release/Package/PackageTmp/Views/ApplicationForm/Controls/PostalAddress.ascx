﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ShortCourseApplicationForm.Domain.Models.ShortCourseApplicationForm.ApplicationForm>" %>
<%@ Import Namespace="ShortCourseApplicationForm.Domain.Models.Enums" %>

<div>
    <br />
    <fieldset>
       <%-- <legend><%: (Model.ShortCourseOfferingType == ShortCourseOfferingType.Partnership || Model.ShortCourseOfferingType == ShortCourseOfferingType.TEL || Model.ShortCourseOfferingType == ShortCourseOfferingType.General || Model.ShortCourseOfferingType == ShortCourseOfferingType.Gap || Model.ShortCourseOfferingType == ShortCourseOfferingType.Public) ? "Contact Information" : "Participants Contact Information" %></legend>--%>
        <div>
            <%: Html.LabelFor(m => m.ContactNumber) %>
            <span class="required">*</span>
        <%--    <%: Html.TextBoxFor(m => m.ContactNumber, new { @id = "MobileNumberId", @Value = Model.ContactNumber }) %>--%>
              <%: Html.TextBoxFor(m => m.ContactNumber, new { @Value = Model.ContactNumber })%>
            <span class="info">&nbsp; (e.g +27219184488)</span>
        </div>
    </fieldset>
</div>

<div>
    <br />
    <fieldset>
        <legend><%: (Model.ShortCourseOfferingType == ShortCourseOfferingType.Partnership || Model.ShortCourseOfferingType == ShortCourseOfferingType.TEL || Model.ShortCourseOfferingType == ShortCourseOfferingType.General || Model.ShortCourseOfferingType == ShortCourseOfferingType.Gap || Model.ShortCourseOfferingType == ShortCourseOfferingType.Public) ? "Postal Address" : "Participants Address" %></legend>

        Please complete this section by first selecting the country.
            <div class="fieldRow inline">
                <label>Country</label>
                <span class="placeHolder"></span>
                <%: Html.DropDownListFor(m => m.PostalCountry, (List<SelectListItem>)ViewBag.AddressCountries,new { @id = "countries" }) %>

                <div id="postalContainer">
                    <div id="street-postbox">

                        <div class="postal-SA">
                            <label class="postal_sub_city">Postal Code or Suburb or City/Town</label>
                            <label></label>
                            <span class="required">*</span>
                            <%: Html.TextBoxFor(m => m.PostalPostalCode, new {@id="postalAfrigisId", @class="meduimText", @Value = Model.PostalPostalCode })%>
                            <input id="postalDistribution" type="button" value="Search" />

                            <div>
                                <label></label>
                                <span class="required">*</span>
                                <% if (Model.PostalAfrigis.IsNotNull() && Model.PostalAfrigis != Guid.Empty)
                                    { %>
                                <%: Html.DropDownListFor(m => m.PostalAfrigis, (List<SelectListItem>)ViewBag.PostalAreas, new {@id="distributionAreaList", @Value = Model.PostalAfrigis })%>
                                <% }
                                    else
                                    { %>
                                <select name="PostalAfrigis" id="distributionAreaList"></select>
                                <% } %>
                            </div>
                            <div>
                                <label>Province</label>
                                <span class="placeHolder"></span>
                                <%: Html.DropDownListFor(m => m.Province, (List<SelectListItem>)ViewBag.PostalProvinces, new { @id = "provinces" })%>
                            </div>
                            <div>
                                <label>Street / Post Box</label>
                                <span class="required">*</span>
                                <%: Html.TextBoxFor(m => m.PostalStreet1, new { @Value = Model.PostalStreet1 }) %>
                            </div>
                            <div>
                                <label></label>
                                <span class="placeHolder"></span>
                                <%: Html.TextBoxFor(m => m.PostalStreet2, new { @Value = Model.PostalStreet2 }) %>
                            </div>
                        </div>

                        <div>
                            <label>Suburb</label>
                            <span class="placeHolder"></span>
                            <%: Html.TextBoxFor(m => m.PostalSuburb, new { @readonly="readonly", @id = "postalSuburb", @Value = Model.PostalSuburb } )%>
                        </div>
                        <div>
                            <label>City / Town</label>
                            <span class="placeHolder"></span>
                            <%: Html.TextBoxFor(m => m.PostalCity, new { @readonly="readonly", @id = "postalCity", @Value = Model.PostalCity } ) %>
                        </div>
                        <div>
                            <label>Postal Code</label>
                            <span class="placeHolder"></span>
                            <%: Html.TextBoxFor(m => m.PostalPostalCode, new { @readonly="readonly", @id = "postalCode", @Value = Model.PostalPostalCode } ) %>
                        </div>


                        <div class="postal-nonSA">
                            <span class="placeHolder"></span>
                            <div>
                                <%: Html.LabelFor(m => m.SelectedCountry)%>
                                <%: Html.TextBoxFor(m => m.SelectedCountry, new {@readonly="readonly", @id="country", @Value = Model.SelectedCountry })%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </fieldset>
</div>

<% if (!(Model.ShortCourseOfferingType == ShortCourseOfferingType.Partnership || Model.ShortCourseOfferingType == ShortCourseOfferingType.TEL || Model.ShortCourseOfferingType == ShortCourseOfferingType.General || Model.ShortCourseOfferingType == ShortCourseOfferingType.Gap || Model.ShortCourseOfferingType == ShortCourseOfferingType.Public))
    { %>
<div id="divPA">
    <br />
    <fieldset>
        <legend>PA's Contact Information</legend>
        If you would like your PA to handle any further correspondence, please complete this section. The below fields are optional
        <div>
            <%: Html.LabelFor(m => m.AssistantSurname) %>
            <span class="placeHolder"></span>
            <%: Html.TextBoxFor(m => m.AssistantSurname, new {@Value = Model.AssistantSurname}) %>
        </div>
        <div>
            <%: Html.LabelFor(m => m.AssistantFirstnames) %>
            <span class="placeHolder"></span>
            <%: Html.TextBoxFor(m => m.AssistantFirstnames, new {@Value = Model.AssistantFirstnames}) %>
        </div>
        <div>
            <%: Html.LabelFor(m => m.AssistantContactNumber) %>
            <span class="placeHolder"></span>
            <%: Html.TextBoxFor(m => m.AssistantContactNumber, new {@Value = Model.AssistantContactNumber}) %>
        </div>
        <div>
            <%: Html.LabelFor(m => m.AssistantCellphoneNumber) %>
            <span class="placeHolder"></span>
            <%: Html.TextBoxFor(m => m.AssistantCellphoneNumber, new {@Value = Model.AssistantCellphoneNumber}) %>
        </div>
        <div>
            <%: Html.LabelFor(m => m.AssistantEmailAddress) %>
            <span class="placeHolder"></span>
            <%: Html.TextBoxFor(m => m.AssistantEmailAddress, new {@Value = Model.AssistantEmailAddress}) %>
        </div>
    </fieldset>
</div>
<% } %>

<div class="contactInfoNavigateButton">
    <input type="submit" class="navigateButton contactInfoButton" value="Next" />
</div>
