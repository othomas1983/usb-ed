﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ShortCourseApplicationForm.Domain.Models.ShortCourseApplicationForm.ApplicationForm>" %>
<%@ Import Namespace="ShortCourseApplicationForm.Domain.Models.Enums" %>

<div>
    <%--<link href="../../../Content/css/font-awsome-min.css" rel="stylesheet" />
    <link href="../../../Content/css/fileuploader.css" rel="stylesheet" />
    <script src="../../../Content/js/BlueImp/jquery.fileupload.js"></script>
    <script src="../../../Content/js/BlueImp/jquery.iframe-transport.js"></script>
    <script src="../../../Content/js/jquery.ui.widget.js"></script>
    <script src="../../../Content/js/BlueImp/blueImp.js"></script>--%>

    <!--#region dropdowns-->

    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    
    <script>
        
        var correctCaptcha = function(response) {
            if (response != undefined && response.length > 0) {
                document.getElementById("qualificationBtn").style.visibility = "visible";
            }
        }

    </script>

    <fieldset>
        <legend>Academic qualifications and employment</legend>
        <br />
        <label class="field-label">Highest Academic Qualification</label>
        <div id="academicContainer">
            <div>
                <%: Html.LabelFor(m => m.QualificationType) %>
                <%: Html.DropDownListFor(m => m.QualificationType, (List<SelectListItem>)ViewBag.QualificationTypes, new { @id="qualificationType"})%>
                <span class="info">&nbsp; if other, please specify</span>
                <%: Html.TextBoxFor(m => m.QualificationTypeOther, new { @id="qualificationTypeOther", @Value = Model.QualificationTypeOther })%>
            </div>

            <div id="qualificationField">
                <%: Html.LabelFor(m => m.QualificationField) %>
                <%: Html.DropDownListFor(m => m.QualificationField, (List<SelectListItem>)ViewBag.QualificationFields, new { @id = "qualificationFields" }) %>
            </div>
            <div>
                <%: Html.LabelFor(m => m.QualificationInstitution) %>
                <%: Html.TextBoxFor(m => m.QualificationInstitution, new { @Value = Model.QualificationInstitution }) %>
            </div>
            <div>
                <label>Year Achieved</label>
                <%: Html.DropDownListFor(m => m.YearAchieved, (List<SelectListItem>)ViewBag.Years, new { @id = "yearAchieved" }) %>
            </div>
        </div>
        <!--#endregion-->
        <%--<!--#region upload-->--%>
        <br />

        <% if (!Model.HasUploadedRequiredDocuments)
            { %>
        <label class="field-label">Copy of Qualifications</label>
        <div class="uploadDocuments">
            <%--Copy of Id doc--%>
            <label>Copy of Identity document</label>
            <div>
                <label for="fileuploadId" class="custom-file-upload">Select file...</label>
                <input id="fileuploadId" type="file" name="files[]" />
                <span style="vertical-align: top" id="upload_Id"></span>&nbsp;
                <progress style="display: none" class="progress-bar" id="idUploadProgress"></progress>
                <span style="vertical-align: top" id="idUploadResult"></span>
            </div>

            <%--Matric Certificate--%>
            <label class="uploadLabel">Matric or equivalent NQF level 4 certificate.</label>
            <div>
                <label for="fileuploadCert" class="custom-file-upload">Select file...</label>
                <input id="fileuploadCert" type="file" name="files[]" />
                <span style="vertical-align: top" id="upload_Certificate"></span>&nbsp;
                <progress style="display: none" class="progress-bar" id="certificateUploadProgress"></progress>
                <span style="vertical-align: top" id="certUploadResult"></span>
            </div>
            
            <% if (!Model.HasUploadedAcceptanceLetter && Model.ShortCourseOfferingType == ShortCourseOfferingType.AdpEdp && Model.ApplicantStatus == ApplicantStatus.Qualified)
               { %>
            <%--Acceptance Form--%>
            <label class="uploadLabel">Acceptance Form</label>
            <div>
                <label for="fileuploadAcceptanceForm" class="custom-file-upload">Select file...</label>
                <input id="fileuploadAcceptanceForm" type="file" name="files[]" />
                <span style="vertical-align: top" id="upload_AcceptanceForm"></span>&nbsp;
                <progress style="display: none" class="progress-bar" id="acceptanceFormUploadProgress"></progress>
                <span style="vertical-align: top" id="acceptanceFormUploadResult"></span>
            </div>
            <% } %>
        </div>
        <% }
            if (Model.HasUploadedRequiredDocuments && !Model.HasUploadedAcceptanceLetter && (Model.ShortCourseOfferingType == ShortCourseOfferingType.TEL || Model.ShortCourseOfferingType == ShortCourseOfferingType.General || Model.ShortCourseOfferingType == ShortCourseOfferingType.Gap || Model.ShortCourseOfferingType == ShortCourseOfferingType.Public))
            { %>
        <%--Acceptance Letter--%>
        <label class="uploadLabel">Acceptance Letter</label>
        <div>
            <input id="acceptanceLetterButton" type="button" class="custom-file-upload" value="View Acceptance Letter" title="View your Acceptance Letter" style="width: 150px !important;" />

            <div data-bind="visible: hasOpenedAcceptanceLetter()">
                <div class="g-recaptcha" data-sitekey="6Leh1g4UAAAAAH4j9WI-bIkVdtk2im7jJ21obFx9" data-callback="correctCaptcha"></div>

                <div class="row">
                    <div class="col-md-12">
                        <form>
                            <div class="g-recaptcha" data-sitekey="6LfiS_8SAAAAABvF6ixcyP5MtsevE5RZ9dTorUWr" data-callback="correctCaptcha"></div>
                        </form>
                    </div>

                </div>
            </div>
        </div>

        <% } %>
    </fieldset>
    <!--#endregion-->
    <div class="qualificationNavigateButton">
        <input type="submit" id="qualificationBtn" class="navigateButton qualificationButton" value="Next" />
    </div>
</div>
