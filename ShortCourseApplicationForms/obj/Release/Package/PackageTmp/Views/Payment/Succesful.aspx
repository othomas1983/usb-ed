﻿
<%@ Page Title="" Language="C#"  MasterPageFile="~/Views/Shared/Success.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <br />
     <%: Html.ValidationSummary()%>
    <br />

    <div class="headerImage"></div>

    <div style="height:600px">
        <h1 class="text-danger">Succesful</h1>
        <h2 class="text-danger">Your payment has succesfully been processed.</h2>
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>