﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <h3>Terms & Conditions</h3>
    <div>
        <label>Registration on programme subject to approval.</label>
        <br />
        <br />
        <strong>Tuition Fees</strong>
        <label>Tuition fees for open programmes cover programme fees, programme material, stationery, lunch and refreshments, and are payable before the commencement of the programme. Please note that programme fees and dates are subject to change.</label>
    </div>
    <br />
    <div>
        <strong>Cancellations</strong>
        <br />
        <label>It is of utmost importance that USB-ED be formally notified of any cancellation 14 days prior to the commencement date of the programme.</label>
        <label>A cancellation fee of 10% will be payable for cancellation fewer than 14 days prior to the commencement of the programme.</label>
        <label>USB-ED reserves the right to cancel or postpone programmes.</label>
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
