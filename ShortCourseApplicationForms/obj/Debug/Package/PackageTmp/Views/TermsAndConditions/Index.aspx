﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<!DOCTYPE html>
 <%: System.Web.Optimization.Styles.Render("~/content/TermsAndConditions") %>   
<html>
<head runat="server">
    <meta name="viewport" content="width=device-width" />
    <title>Terms and Conditions</title>
</head>
<body>
    <div id="wrap">
    <form id="frmMain" runat="server">
    	<div id="grey" class="head">
            <div class="left" style="width: auto; float: none; margin-right: 70px">
                <div>
                    <img src="~/../Content/img/logo.jpg" />
                    <label class="headerTextSmall">Executive Development Programmes</label>
                    <label class="headerText">Application Form</label>
                </div> 
                
                <div>
                    <label class="terms">Terms & Conditions</label>
                </div>

                <div>
                    <label>Registration on programme subject to approval.</label>
                    <br />
                    <br />
                    <strong>Tuition Fees</strong>
                    <label>Tuition fees for open programmes cover programme fees, programme material, stationery, lunch and refreshments, and are payable before the commencement of the programme. Please note that programme fees and dates are subject to change.</label>
                </div>
                <br />
                <div>
                    <strong>Cancellations</strong>
                    <br />
                    <label>It is of utmost importance that USB-ED be formally notified of any cancellation 14 days prior to the commencement date of the programme.</label>
                    <label>A cancellation fee of 10% will be payable for cancellation fewer than 14 days prior to the commencement of the programme.</label>
                    <label>USB-ED reserves the right to cancel or postpone programmes.</label>
                </div>

               <%-- <div class="corner_LT">
                </div>
                <div class="corner_LB">
                </div>
                <div class="corner_RT">
                </div>
                <div class="corner_RB">
                </div>--%>

            </div>
        </div>
    </form>
        </div>
</body>
</html>
