﻿<%@ Page Title="" Language="C#"  MasterPageFile="~/Views/Shared/Success.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <br />
     <%: Html.ValidationSummary()%>
    <br />

    <div class="headerImage"></div>

    <div style="height:600px">
        <h4>This course has already run for this academic year.</h4>
        <h4>Please contact <a href="mailto:info@usb-ed.com">info@usb-ed.com</a> should you wish to enquire about the next years offering and enrolment thereof.</h4>
        <%--<h4>Please select a valid course or try again later.</h4>--%>
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>