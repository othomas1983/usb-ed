﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<ShortCourseApplicationForm.Domain.Models.ShortCourseApplicationForm.ApplicationForm>" %>

<%@ Register Src="~/Views/ApplicationForm/Controls/EmailAddress.ascx" TagName="EmailAddress" TagPrefix="Controls" %>
<%@ Register Src="~/Views/ApplicationForm/Controls/PersonalInformation.ascx" TagName="PersonalInformation" TagPrefix="Controls" %>
<%@ Register Src="~/Views/ApplicationForm/Controls/ExecutiveDevelopment.ascx" TagName="ExecutiveDevelopment" TagPrefix="Controls" %>
<%@ Register Src="~/Views/ApplicationForm/Controls/Newsletter.ascx" TagName="Newsletter" TagPrefix="Controls" %>
<%@ Register Src="~/Views/ApplicationForm/Controls/PaymentInformation.ascx" TagName="PaymentInformation" TagPrefix="Controls" %>
<%@ Register Src="~/Views/ApplicationForm/Controls/PostalAddress.ascx" TagName="PostalAddress" TagPrefix="Controls" %>
<%@ Register Src="~/Views/ApplicationForm/Controls/Qualifications.ascx" TagName="Qualifications" TagPrefix="Controls" %>
<%@ Register Src="~/Views/ApplicationForm/Controls/SocialMedia.ascx" TagName="SocialMedia" TagPrefix="Controls" %>
<%@ Register Src="~/Views/ApplicationForm/Controls/WorkExperience.ascx" TagName="WorkExperience" TagPrefix="Controls" %>
<%@ Import Namespace="ShortCourseApplicationForm.Models" %>
<%@ Import Namespace="ShortCourseApplicationForm.Domain.Models.Enums" %>

<asp:Content ID="applicationForm" ContentPlaceHolderID="MainContent" runat="server">

    <% using (Html.BeginForm("Index", "ApplicationForm", FormMethod.Post, new { id = "frmApplication" }))
        { %>
    
    <%: Html.Hidden("UsNumber", Model.UsNumber, new { @id = "usNumber", @Value = Model.UsNumber }) %>
    <%: Html.Hidden("VoucherStatus", Model.VoucherStatus, new { @id = "voucherStatus", @Value = Model.VoucherStatus }) %>
    <%: Html.Hidden("HasVoucher", Model.HasVoucher, new { @id=  "hasVoucher", @Value = Model.HasVoucher }) %>
    <%: Html.Hidden("Enrolment", Model.Enrolment, new { @id = "enrolment", @Value = Model.Enrolment }) %>
    <%--<%: Html.Hidden("FirstNames", Model.FirstNames, new { @id = "firstNames", @Value = Model.FirstNames }) %>
    <%: Html.Hidden("Surname", Model.Surname, new { @id = "surname", @Value = Model.Surname }) %>--%>

    <%:Html.Hidden("ValidationSection", Model.ValidationSection, new { @id = "validationSection" }) %>
    <%:Html.Hidden("SelectedTab", Model.SelectedTab, new { @id = "selectedTab", Value = Model.SelectedTab }) %>
    <%: Html.Hidden("AccountGuid", Model.AccountGuid, new { @id = "accountGuid", @Value = Model.AccountGuid }) %>

    <%: Html.Hidden("ShortCourseOfferingType", Model.ShortCourseOfferingType, new { @id = "shortCourseOfferingType", @Value = Model.ShortCourseOfferingType }) %>
    <%: Html.Hidden("AccountType", Model.AccountType, new { @id = "accountType", @Value = Model.AccountType }) %>
    

    <%--TODO: Remove these properties when account type tested--%>
    <%: Html.Hidden("IsMasterStart", Model.IsMasterStart, new { @id = "isMasterStart", @Value = Model.IsMasterStart }) %>
    <%: Html.Hidden("IsBasilRead", Model.IsBasilRead, new { @id = "isBasilRead", @Value = Model.IsBasilRead }) %>
    <%--  --%>
    

    <%: Html.Hidden("QualifyForVoucher", Model.QualifyForVoucher, new { @ID = "QualifyForVoucher", @Value = Model.QualifyForVoucher }) %>
    
    <%: Html.Hidden("HasUploadedRequiredDocuments", Model.HasUploadedRequiredDocuments, new { @id = "hasUploadedRequiredDocuments", @Value = Model.HasUploadedRequiredDocuments }) %>
    <%: Html.Hidden("HasUploadedAcceptanceLetter", Model.HasUploadedAcceptanceLetter, new { @id = "hasUploadedAcceptanceLetter", @Value = Model.HasUploadedAcceptanceLetter }) %>
    <%: Html.Hidden("ApplicantStatus", Model.ApplicantStatus, new { @id = "applicantStatus", @Value = Model.ApplicantStatus }) %>

    <%:Html.Hidden("EmailSectionValidated", Model.EmailSectionValidated, new { @id = "emailSectionValidated", @Value = Model.EmailSectionValidated }) %>
    <%:Html.Hidden("DisplayNameApproved", Model.DisplayNameApproved, new { @id = "displayNameApproved", @Value = Model.DisplayNameApproved })%>
    <%:Html.Hidden("Section1Validated", Model.Section1Validated, new { @id = "section1Validated" }) %>
    <%:Html.Hidden("Section2Validated", Model.Section2Validated, new { @id = "section2Validated" }) %>
    <%:Html.Hidden("Section3Validated", Model.Section3Validated, new { @id = "section3Validated" }) %>
    <%:Html.Hidden("Section4Validated", Model.Section4Validated, new { @id = "section4Validated" }) %>
    <%:Html.Hidden("Section5Validated", Model.Section5Validated, new { @id = "section5Validated" }) %>
    
    <%: Html.Hidden("SelectedCountry", Model.SelectedCountry, new { @id = "selectedCountry", @Value = Model.SelectedCountry }) %>
    <%--<%: Html.Hidden("PostalAfrigis", Model.PostalAfrigis, new { @id = "postalAfrigis", @Value = Model.PostalAfrigis }) %>--%>

    <%:Html.Hidden("DateOfBirthDay", Model.DateOfBirthDay, new { @id = "dateOfBirthDay" }) %>
    <%:Html.Hidden("DateOfBirthMonth", Model.DateOfBirthMonth, new { @id = "dateOfBirthMonth" }) %>
    <%:Html.Hidden("DateOfBirthYear", Model.DateOfBirthYear, new { @id = "dateOfBirthYear" }) %>

    <%:Html.Hidden("CourseName", Model.CourseName, new { @id = "courseName" }) %>
    <%:Html.Hidden("CourseStartDate", Model.CourseStartDate, new { @id = "courseStartDate" }) %>
    <%:Html.Hidden("DirectOfferingLink", Model.DirectOfferingLink, new { @id = "directOfferingLink" }) %>
    <%:Html.Hidden("ApplicationClosed", Model.ApplicationClosed, new { @id = "applicationClosed" }) %>    
    <%:Html.Hidden("ShortCourseId", Model.ShortCourseId, new { @id = "shortCourseId" }) %>
    <%:Html.Hidden("ShortCourseOfferingId", Model.ShortCourseOfferingId, new { @id = "shortCourseOfferingId" }) %>
    <%:Html.Hidden("WebTokenId", Model.WebTokenId, new { @id = "webTokenId" }) %>
    <%:Html.Hidden("Offering", Model.Offering, new { @id = "offering" }) %>


    <%:Html.Hidden("IdentificationDocumentUploadKey", Model.IdentificationDocumentUploadKey, new { @id = "identificationDocumentUploadKey" }) %>
    <%:Html.Hidden("QualificationDocumentUploadKey", Model.QualificationDocumentUploadKey, new { @id = "qualificationDocumentUploadKey" }) %>
    <%:Html.Hidden("CVDocumentUploadKey", Model.CVDocumentUploadKey, new { @id = "cvDocumentUploadKey" }) %>

    <%:Html.Hidden("InsideCRM", Model.InsideCRM, new { @id = "insideCRM" }) %>
    <%:Html.Hidden("DirectOfferingLink", Model.DirectOfferingLink, new { @id = "directOfferingLink" }) %>

    <div class="main">
        <div class="wrap">
            <div class="left" style="width: auto; float: none; margin-right: 70px">
                <div class="headerContainer">
                    <div class="mainHeading">
                        <label>Application Form</label>
                    </div>
                    <div class="headerImage"></div>

                    <% if (Model.EmailSectionValidated || Model.Email.IsNotNullOrEmpty())
                        { %>
                    <div id="menu-outer">
                        <ul id="progress-list">
                            <li id="personalInfoTab" class="menu_1">&nbsp Personal information<span id="step1" class="notValidated"></span><span class="menuDivider"></span> </li>
                            <li id="contactInfoTab" class="menu_2">&nbsp Contact information<span id="step2" class="notValidated"></span><span class="menuDivider"></span> </li>
                            <li id="academicTab" class="menu_3">&nbsp Academic qualifications and employment<span id="step3" class="notValidated"></span>
                            <% if (!(Model.AccountType == AccountType.MasterStart || Model.AccountType == AccountType.BasilRead))
                               { %>
                                <span class="menuDivider"></span> </li>
                            <li id="marketingTab" class="menu_4">&nbsp Marketing<span id="step4" class="notValidated"></span>
                            <% if (!(Model.ShortCourseOfferingType == ShortCourseOfferingType.Partnership || Model.ShortCourseOfferingType == ShortCourseOfferingType.TEL))
                               { %>
                                <span class="menuDivider"></span> </li>
                            <li id="paymentInfoTab" class="menu_5">&nbsp Payment information<span id="step5" class="notValidated"></span></li>
                            <% }
                               } %>
                        </ul>
                    </div>
                    <% } %>


                    <%: Html.ValidationSummary()%>
                </div>

                <div id="emailAddress">
                    <Controls:EmailAddress runat="server" />
                </div>

                <div class="corner_LT">
                </div>
                <div class="corner_LB">
                </div>
                <div class="corner_RT">
                </div>
                <div class="corner_RB">
                </div>

                <div id="qualifications">
                    <Controls:WorkExperience runat="server" />
                    <Controls:Qualifications runat="server" />
                </div>

                <div id="personalInformation">

                    <fieldset>
                        <legend>Field of Study</legend>
                        <div class="marginLeft20">
                            <label for="course"><strong>Course</strong></label>

                            <%if (Model.DirectOfferingLink)
                                {%>
                            <%: Html.LabelFor(m => m.Offering, (string)ViewBag.OfferingName)%>
                            <% }
                                else
                                { %>
                            <% if (Model.InsideCRM)
                                { %>
                            <%: Html.Label(Model.CourseName)%>
                            <% }
                                else
                                { %>
                            <%: Html.DropDownListFor(m => m.Offering, (List<SelectListItem>)ViewBag.ShortCourseOfferings, new {@id="shortCourseOfferings" })%>
                            <% } %>
                            <% } %>

                            <div>
                                <label><strong>Course start date:</strong></label>
                                <label id="offeringStartDate"></label>
                            </div>
                        </div>

                        <% var canSearch = System.Configuration.ConfigurationManager.AppSettings["StudentSearchEnabled"]; %>
                        <% if (Model.InsideCRM)
                            { %>
                        <div class="search_field marginLeft20">
                            <br />
                            <label><strong>Student Number</strong> </label>
                            <%:Html.TextBoxFor(m => m.UsNumber, new {@id="studentNumber", @class="searchButton" })%>

                            <a class="button" id="studentSearch" data-request-url='<%=Url.Action("LoadStudentDetails", "ApplicationForm")%>'>Search</a>

                        </div>
                        <% } %>
                    </fieldset>

                    <Controls:PersonalInformation runat="server" />
                </div>

                <div id="contactInformation">
                    <Controls:PostalAddress runat="server" />
                </div>

                <div id="marketing">

                    <div id="executiveDevelopment">
                        <Controls:ExecutiveDevelopment runat="server" />
                    </div>

                    <div id="socialMedia">
                        <Controls:SocialMedia runat="server" />
                    </div>

                    <div id="newsletter">
                        <Controls:Newsletter runat="server" />
                    </div>
                </div>
                <div id="paymentInfo">
                    <Controls:PaymentInformation runat="server" />
                </div>
            </div>

            <div id="dialog">
                <strong class="message"></strong>
            </div>

            <div id="progressDialog">
                <strong class="message"></strong>
                <img class="progress" />
            </div>

        </div>
    </div>
    <%--<div class="termsContainer">
        <label>I accept the <a href="<%=Url.Action("Index", "TermsAndConditions")%>" target="_blank">Terms and Conditions</a></label>
        <input type="checkbox" id="checkBoxTerms" data-bind="checked: termsAndConditionsRead"/>
        <label class="noBold">* required</label>
        <input id="submit" type="submit" value="Submit" data-bind="enable: enableSubmitButton()"/>

    </div>
    <span id="nameConfirmed" data-bind="visible: !displayNameApproved()">Please confirm the name that will be displayed on your certificate of competence.</span>--%>

    <p class="" style="font-style: italic; color: #ba3636; width: 500px; margin: 0 auto;">
        This site is best viewed using Chrome, Firefox, Safari or Internet Explorer 9 or higher.
    </p>

    <script type="text/javascript">
        var applicationFormViewModel = new ApplicationFormViewModel();
        var data = <%: Html.Raw(Json.Encode(Model))%>;
        $(document).ready(function () {
                applicationFormViewModel.load(data);
        });

    </script>

    <%--<input id="submit" type="submit" class="navigateButton paymentInfoButton" value="Submit"/>  --%>
    <% } %>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
