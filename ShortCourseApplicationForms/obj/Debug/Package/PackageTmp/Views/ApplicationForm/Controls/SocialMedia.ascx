﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ShortCourseApplicationForm.Domain.Models.ShortCourseApplicationForm.ApplicationForm>" %>

<div id="socialMediaSection">
    <fieldset>
        <legend>Which of the following social media networks do you use?</legend>

        <div>
            <label for="usesTwitter">Twitter</label>
            <%--<%: Html.CheckBoxFor(m => m.UsesTwitter, new { @id = "usesTwitter", @checked = "checked" }) %>--%>
            <input type="checkbox" id="usesTwitter" name="usesTwitter" value="true"  <%: @Html.Raw( Model.UsesTwitter ? "checked" : "") %> />
            <span class="info"> &nbsp;Twitter username:  &nbsp; @</span>
            <%: Html.TextBoxFor(m => m.TwitterUsername, new { @Value = Model.TwitterUsername }) %>
        </div>

        <div>
            <label for="usesFacebook">Facebook</label>
            <%--<%: Html.CheckBoxFor(m => m.UsesFacebook, new { @id = "usesFacebook", @checked = Model.UsesFacebook }) %>--%>
            <input type="checkbox" id="usesFacebook" name="usesFacebook" value="true"  <%: @Html.Raw( Model.UsesFacebook ? "checked" : "") %> />
        </div>

        <div>
            <label for="usesLinkedIn">LinkedIn</label>
            <%--<%: Html.CheckBoxFor(m => m.UsesLinkedIn, new { @id = "usesLinkedIn", @checked = Model.UsesLinkedIn }) %>--%>
            <input type="checkbox" id="usesLinkedIn" name="usesLinkedIn" value="true"  <%: @Html.Raw( Model.UsesLinkedIn ? "checked" : "") %> />
        </div>
    </fieldset>
</div>