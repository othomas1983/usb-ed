﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ShortCourseApplicationForm.Domain.Models.ShortCourseApplicationForm.ApplicationForm>" %>

<div id="emailAddressSection">
    <fieldset>
        <legend>Have you studied at USB-ED before or saved an incomplete application?</legend>
        
        <div style="text-align: center;">
            <span>Enter your email address to search for a previously added/saved application. </span>
        </div>
        <div style="text-align: center; margin-top: 10px;">
            <%: Html.LabelFor(m => m.RegisteredEmail) %>
            <%: Html.TextBoxFor(m => m.RegisteredEmail) %>
        </div>
         <div style="text-align: center;">
            <span>
                Clicking continue without entering an email address will create a new application
            </span>
        </div>
        <div style="text-align: center;">
            <input type="submit" id="emailAddressBtn" class="navigateButton emailAddressButton" value="Continue" style="float: none;" />
        </div>
       
    </fieldset>
</div>