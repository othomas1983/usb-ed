﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<ShortCourseApplicationForm.Domain.Models.ShortCourseApplicationForm.ApplicationForm>" %>

<div id="paymentInfoSection">
    <br />
    <fieldset>
        <legend>Payment Responsibility</legend>
        <div>
            <%: Html.Hidden("paymentResponsibilityType", null, new { @id = "paymentResponsibilityType" }) %>
            <span class="required">*</span>
            <%: Html.DropDownListFor(m => m.PaymentResponsibility, (List<SelectListItem>)ViewBag.PaymentResponsibility, new { @id = "payment" }) %>

            <span>For more information about other payment options, please click <a href="<%=Url.Action("Index", "PaymentOptions")%>" target="_blank">here.</a></span>
        </div>

        <div class="companyContainer">
            <%: Html.HiddenFor(m => m.IsCompanyResponsibility, new { @id = "isCompanyResponsibility" }) %>
            <%: Html.HiddenFor(m => m.IsSelf, new { @id = "isSelf" }) %>
            <h1 id="paymentCompanyHeading">Company:</h1>

            <label>Copy of purchase order or letter from sponsor</label>
            <div>
                <label for="fileuploadCompanyPurchaseOrder" class="custom-file-upload">Select file...</label>
                <input id="fileuploadCompanyPurchaseOrder" type="file" name="files[]" />
                <span style="vertical-align: top" id="upload_CompanyPurchaseOrder"></span>&nbsp;
                <progress style="display: none" class="progress-bar" id="companyPurchaseOrderUploadProgress"></progress>
                <span style="vertical-align: top" id="companyPurchaseOrderUploadResult"></span>
            </div>

            <table>
                <tr>
                    <td><%: Html.LabelFor(m => m.PaymentContactName) %><span class="required">*</span> </td>
                    <td><%: Html.TextBoxFor(m => m.PaymentContactName, new { @Value = Model.PaymentContactName }) %></td>
                </tr>
                <tr>
                    <td><%: Html.LabelFor(m => m.PaymentContactSurname) %><span class="required">*</span> </td>
                    <td><%: Html.TextBoxFor(m => m.PaymentContactSurname, new { @Value = Model.PaymentContactSurname }) %></td>
                </tr>
                <tr>
                    <td><%: Html.LabelFor(m => m.PaymentContactNumber) %><span class="required">*</span></td>
                    <td><%: Html.TextBoxFor(m => m.PaymentContactNumber, new { @Value = Model.PaymentContactNumber }) %></td>
                </tr>
                <tr>
                    <td><%: Html.LabelFor(m => m.PaymentContactEmail) %><span class="required">*</span></td>
                    <td><%: Html.TextBoxFor(m => m.PaymentContactEmail, new { @Value = Model.PaymentContactEmail }) %></td>
                </tr>
                <tr>
                    <td><label id="paymentCompanyLabel">Name of Company</label><span class="required">*</span></td>
                    <td><%: Html.TextBoxFor(m => m.PaymentContactCompanyName, new { @Value = Model.PaymentContactCompanyName }) %></td>
                </tr>
             
            </table>
             <table border="0"  id="companyConsent" style="display:none;">
                <tr> 
                    <td colspan="3">
                        <span class="required">*</span>
                        <span><b>Consent for Release of Information (PoPi Act)</b><br /></span>
                        <span>I give my consent that information regarding my enrolment, academic records and/or awards may be released to my Company.</span><br />
                        <span>I understand that the purpose of the disclosure of the information is to assist with the monitoring of my progress and the implementation of the necessary actions to support my development.</span>
                    </td>
                </tr>
                <tr>
                     <td>
				        <table>
                             <tr>
                                <td colspan="2"> <%= Html.RadioButtonFor(model => model.Consent, "True") %> Yes</td>                    
                            </tr>
                            <tr>
                                <td colspan="2"><%= Html.RadioButtonFor(model => model.Consent, "False") %> No</td>                   
                            </tr>
                        </table>
                     </td>
                     <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>             
                </table>
              
            <h1 id="paymentCompanyAddressHeading">Company Address:</h1>
            <table>
                <tr>
                    <td><label>Country</label><span class="required">*</span></td>
                    <td><%: Html.DropDownListFor(m => m.PaymentPostalCountry, (List<SelectListItem>)ViewBag.AddressCountries,new { @id = "paymentCountries" }) %></td>
                </tr>
            </table>
            
            <div id="paymentPostalContainer">

            <table>
                <tr class="paymentPostal-SA">
                    <td><label></label><span class="required">*</span></td>
                    <td><%: Html.TextBoxFor(m => m.PaymentPostalCode, new {@id="paymentPostalAfrigisId", @class="meduimText", @Value = Model.PaymentPostalCode })%></td>
                    <td><input id="paymentPostalDistribution" type="button" value="Search" /></td>
                </tr>
                <tr class="paymentPostal-SA">
                    <td></td>
                    <td>
                        <% if (Model.PaymentPostalAfrigis.IsNotNull() && Model.PaymentPostalAfrigis != Guid.Empty)
                                    { %>
                                <%: Html.DropDownListFor(m => m.PaymentPostalAfrigis, (List<SelectListItem>)ViewBag.PaymentPostalAreas, new {@id="paymentDistributionAreaList", @Value = Model.PaymentPostalAfrigis })%>
                                <% }
                                    else
                                    { %>
                                <select name="PaymentPostalAfrigis" id="paymentDistributionAreaList"></select>
                                <% } %>
                    </td>
                </tr>
               <%-- <tr class="paymentPostal-SA">
                    <td><label>Province</label></td>
                    <td><%: Html.DropDownListFor(m => m.PaymentProvince, (List<SelectListItem>)ViewBag.PaymentProvince, new { @id = "paymentProvinces" })%></td>
                </tr>--%>
                <tr class="paymentPostal-SA">
                    <td><label>Province</label></td>
                    <td><%: Html.DropDownListFor(m => m.PaymentProvince, (List<SelectListItem>)ViewBag.PaymentProvince, new { @id = "paymentProvinces" }) %></td>
                </tr>
                <tr class="paymentPostal-SA">
                    <td><label>Street / Post Box</label><span class="required">*</span></td>
                    <td><%: Html.TextBoxFor(m => m.PaymentPostalStreet1, new { @Value = Model.PaymentPostalStreet1 }) %></td>
                </tr>
                <tr class="paymentPostal-SA">
                    <td><label></label></td>
                    <td><%: Html.TextBoxFor(m => m.PaymentPostalStreet2, new { @Value = Model.PaymentPostalStreet2 }) %></td>
                </tr>
                <tr>
                    <td><%: Html.LabelFor(m => m.PaymentPostalSuburb) %></td>
                    <td><%: Html.TextBoxFor(m => m.PaymentPostalSuburb, new { @id="paymentPostalSuburb", @readonly="readonly", @Value = Model.PaymentPostalSuburb }) %></td>
                </tr>
                <tr>
                    <td><%: Html.LabelFor(m => m.PaymentPostalCity) %></td>
                    <td><%: Html.TextBoxFor(m => m.PaymentPostalCity, new { @id="paymentPostalCity", @readonly="readonly", @Value = Model.PaymentPostalCity }) %></td>
                </tr>
                <%--ForeignPostalPostalCode--%>
                 <tr>
                    <td><%: Html.LabelFor(m => m.PaymentPostalCode) %></td>
                    <td><%: Html.TextBoxFor(m => m.PaymentPostalCode, new { @id="paymentPostalCode", @readonly="readonly", @Value = Model.PaymentPostalCode }) %></td>
                </tr>
               <%-- <tr>
                    <td><%: Html.LabelFor(m => m.PaymentPostalCode) %></td>
                    <td><%: Html.TextBoxFor(m => m.PaymentPostalCode, new { @id="paymentPostalCode", @readonly="readonly", @Value = Model.PaymentPostalCode }) %></td>
                </tr>--%>
            </table>
            </div>
            <div class="paymentPostal-nonSA">
                <table>
                    <tr>
                        <td><%: Html.LabelFor(m => m.PaymentSelectedCountry)%></td>
                        <td><%: Html.TextBoxFor(m => m.PaymentSelectedCountry, new {@readonly="readonly", @id="paymentCountry", @Value = Model.PaymentSelectedCountry })%></td>
                    </tr>
                </table>
            </div>
        </div>
    </fieldset>

    <input id="navigateButton" type="submit" class="navigateButton paymentInfoButton" value="Submit" />
    <input id="mainSubmitButton" class="navigateButton" type="submit" value="Submit" />
</div>

<script type="text/javascript"> 
    
     var paymentVal = $("#payment option:selected").text();
            //alert(paymentVal);

            if (paymentVal == "Company") {
                $('#companyConsent').show();
            }
            else {
                $('#companyConsent').hide();
            }
           


    $(function () {
        $("#paymentResponsibilityType").val($("#payment>option:selected").text());
        if ($("#paymentCountries")) {
            resolvePaymentCountries();
        }

        $("#paymentCountries").change(function() {
            resolvePaymentCountries();
            //set province to empty string if international
            //if ($("#payment>option:selected").text() !== "South Africa" || $("#payment>option:selected").text() !== "")
            //{
            //    //Internation Company address
            //    paymentProvinces
            //    $("#payment>option:selected").va('0cb9d57a-0b36-e511-80c8-005056b8908e');
            //}
        });
      
        if ($("#payment>option:selected").text() != undefined && $("#payment>option:selected").text() == "Self") {
            $("#paymentResponsibilityType").val($("#payment>option:selected").text());
            $("#isCompanyResponsibility").val(false);
            $("#isSelf").val(true);
        }
        $("#payment").change(function () {
            var paymentVal = $("#payment option:selected").text();
            //alert(paymentVal);

            if (paymentVal == "Company") {
                $('#companyConsent').show();
            }
            else {
                $('#companyConsent').hide();
            }
           
            $("#paymentResponsibilityType").val("");
            if ($("#payment>option:selected").text() != undefined && $("#payment>option:selected").text() == "Self") {
                $("#paymentResponsibilityType").val($("#payment>option:selected").text());
                $("#isCompanyResponsibility").val(false);
                $("#isSelf").val(true);
            }else if ($("#payment>option:selected").text() != undefined &&
                $("#payment>option:selected").text() == "Company") {
                $("#paymentCompanyLabel").text("Name of Company");
                $("#paymentCompanyHeading").text("Company:");
                $("#paymentCompanyAddressHeading").text("Company Address:");
                $("#isCompanyResponsibility").val(true);
                $("#isSelf").val(false);
            }else if ($("#payment>option:selected").text() != undefined &&
                $("#payment>option:selected").text() == "Department") {
                $("#paymentCompanyHeading").text("Department:");
                $("#paymentCompanyAddressHeading").text("Department Address:");
                $("#isCompanyResponsibility").val(true);
                $("#isSelf").val(false);
            }else if ($("#payment>option:selected").text() != undefined &&
                $("#payment>option:selected").text() == "Sponsored") {
                $("#paymentCompanyLabel").text("Name of Sponsor");
                $("#paymentCompanyHeading").text("Sponsor:");
                $("#paymentCompanyAddressHeading").text("Sponsor Address:");
                $("#isCompanyResponsibility").val(true);
                $("#isSelf").val(false);
            }
        });

        $("#paymentPostalDistribution").click(function () {
            var searchCriteria = $("#paymentPostalAfrigisId").val();
            if (!searchCriteria) {
                alert("Please enter Postal Code");
                return;
            }
            $.ajax({
                type: "POST",
                dataType: "json",
                url: "ApplicationForm/GetDistributionAreas?query=" + searchCriteria,
                data: { "query": searchCriteria },
                contentType: 'application/json; charset=utf-8',
                error: function (x, e) {
                    alert("An error occurred while trying to load the data. Please try again later or contact your service administrator.");
                },
                success: function (data) {
                    populatePaymentDistributionAreas(data);
                }
            });
        });

        $("#paymentDistributionAreaList").change(function () {
            var selectedArea = $("#paymentDistributionAreaList option:selected").text();

            var area = selectedArea.split(',');
            if (area.length === 1) {
                return false;
            }

            $("#paymentPostalSuburb").val(area[0]);
            $("#paymentPostalCity").val(area[1]);
            $("#paymentPostalCode").val(area[2]);
        });
    });

    resolvePaymentCountries = function () {
        var selectedText = $("#paymentCountries option:selected").text();

        if (selectedText == "South Africa") {
            $("#paymentPostalContainer").show(500);
            $(".paymentPostal-nonSA").hide();
            $(".paymentPostal-SA").show();
            $("#paymentPostalSuburb").attr("readonly", true);
            $("#paymentPostalCity").attr("readonly", true);
            $("#paymentPostalCode").attr("readonly", true);
        } else if (selectedText != "") {
            //$("#nationality").val(selectedText);
            $("#paymentPostalContainer").show(500);
            $(".paymentPostal-nonSA").show();
            $(".paymentPostal-SA").hide();
            $("#paymentPostalSuburb").attr("readonly", false);
            $("#paymentPostalCity").attr("readonly", false);
            $("#paymentPostalCode").attr("readonly", false);
        } else {
            $("#paymentPostalContainer").hide(500);
        }

        $("#paymentCountry").val(selectedText);
        $("#paymentSelectedCountry").val(selectedText);
    };

    populatePaymentDistributionAreas = function (areas) {
        if (areas.length > 0) {
            if ($('#paymentDistributionAreaList').length >= 0) {
                $('#paymentDistributionAreaList')[0].options.length = 0;
            }
            var options = $("#paymentDistributionAreaList");
            $.each(areas, function () {
                options.append(new Option(this.Text, this.Value));
            });
        }
        else {
            alert("The address search returned no results for your query. Check the spelling and try filling in your town or city which will return the broadest search. Remember to fill in either the one or the other, for instance, do not supply BOTH suburb AND city.");
        }
    };
     var paymentVal = $("#payment option:selected").text();
            //alert(paymentVal);

            if (paymentVal == "Company") {
                $('#companyConsent').show();
            }
            else {
                $('#companyConsent').hide();
            }
           
</script>
