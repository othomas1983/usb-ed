﻿function KnockoutObject() {

    var self = this;

    self.Active = ko.observable(false);

    self.ErrorMessages = ko.observableArray();
    self.hasErrors = ko.computed(function () {
        return self.ErrorMessages().length > 0;
    });
    self.ControlIds = new Array();
    self.addControlId = function (properyName, controlId) {
        self.ControlIds.push({ Property: properyName, ControlId: controlId });
    };

    self.getPropertyControl = function (properyName) {
        var control = self.ControlIds.first(function (t) { return t.Property == properyName; });
        if (control) {
            return control.ControlId;
        }
        return "";
    };

    self.getErrorSummaryControl = function () {
        var control = self.ControlIds.first(function (t) { return t.Property == "ErrorMessages"; });
        if (control) {
            return control.ControlId;
        }
        return "";
    };

    self.ObjectSummary = ko.observable("");
    self.scrollToObject = function () {
        var control = self.ControlIds.first(function (t) { return t.Property == "ObjectSummary"; });
        if (control) {
            var targetControl = $("#" + control.ControlId);
            if (targetControl) {
                var parentControl = targetControl.parent();
                parentControl.scrollTo();

            }
        }
    };

    self.SuccessMessage = ko.observable("");

    self.cancelAll = function () {
    };


    self.headerRowClass = ko.computed(function () {
        var active = self.Active();
        if (active) {
            return "";
        }
        return "hide";
    });

}

function KnockOutCommon() {

    var instance;

    this.load = function () {
        instance = this;
        instance.registerExtenders();
        instance.registerHandlers();
    }

    this.clearErrorMessages = function (targetObject) {
        targetObject.ErrorMessages.removeAll();
    }

    this.addErrorMessages = function (targetObject, errors) {

        instance.clearErrorMessages(targetObject);

        for (var i = 0; i < errors.length; i++) {
            var error = errors[i];
            targetObject.ErrorMessages.push({ Text: error.Text });

            if (error.PropertyName) {
                var controlId = targetObject.getPropertyControl(error.PropertyName);
                if (controlId) {
                    var control = $("#" + controlId);
                    if (control) {
                        control.addClass("input-validation-error");
                        control.attr("title", error.Text);
                    }
                }

            }
        }

        targetObject.scrollToObject();
    }

    this.registerExtenders = function () {

        ko.validation.init({
            registerExtenders: true,
            messagesOnModified: false,
            insertMessages: false,
            decorateInputElement: true,
            parseInputAttributes: true,
            errorsAsTitle: true,
            errorMessageClass: "input-validation-error",
            errorElementClass: "input-validation-error error toolTip",
            errorClass: "error",
            messageTemplate: null,
            grouping: {
                deep: true,
                observable: true,
                live: true
            }
        });

        ko.validation.rules['mustEqual'] = {
            validator: function (val, otherVal) {
                return val === otherVal;
            },
            message: 'The field must equal {0}'
        };

        ko.validation.rules['atLeastOne'] = {
            validator: function (array, predicate) {
                var self = this;
                self.predicate = predicate;
                return ko.utils.arrayFirst(array, function (item) {
                    return self.predicate.call(item);
                }) != null;
            },
            message: 'The array must contain at least one valid element.'
        };

        ko.validation.registerExtenders();
    }
    this.registerHandlers = function () {

        ko.observableArray.fn.pushAll = function (valuesToPush) {
            var underlyingArray = this();
            this.valueWillMutate();
            ko.utils.arrayPushAll(underlyingArray, valuesToPush);
            this.valueHasMutated();
            return this;
        };
        
        ko.observableArray.fn.stringSort = function (a, b, isAscending) {
            this.sort(function (x, y) {
                var xValue = a(x);
                var yValue = b(y);

                aResult = xValue == null ? null : xValue.toLowerCase();
                bResult = yValue == null ? null : yValue.toLowerCase();

                if (isAscending) {
                    return ((aResult < bResult) ? -1 : (aResult > bResult) ? 1 : 0);
                }
                return ((aResult < bResult) ? 1 : (aResult > bResult) ? -1 : 0);
            });
        };

        ko.bindingHandlers.uniqueId = {
            init: function (element, valueAccessor, allBindingsAccessor) {
                var value = valueAccessor();
                var idMod = 'id-' + (allBindingsAccessor().uniqueMod || 0);
                value[idMod] = value[idMod] || ko.bindingHandlers.uniqueId.prefix + (++ko.bindingHandlers.uniqueId.counter);
                element.id = value[idMod];
            },
            counter: 0,
            prefix: "unique"
        };

        ko.bindingHandlers.uniqueFor = {
            init: function (element, valueAccessor, allBindingsAccessor) {
                var value = valueAccessor();
                var idMod = 'id-' + (allBindingsAccessor().uniqueMod || 0);
                value[idMod] = value[idMod] || ko.bindingHandlers.uniqueId.prefix + (++ko.bindingHandlers.uniqueId.counter);

                element.setAttribute("for", value[idMod]);
            }
        };

        ko.protectedObservable = function (initialValue) {
            //private variables
            var _actualValue = ko.observable(initialValue);
            var _tempValue = ko.observable(initialValue);

            //computed observable that we will return
            var result = ko.computed({
                //always return the actual value
                read: function () {
                    return _actualValue();
                },
                //stored in a temporary spot until commit
                write: function (newValue) {
                    _tempValue(newValue);
                }
            });

            //if different, commit temp value
            result.commit = function () {
                if (_tempValue() !== _actualValue()) {
                    _actualValue(_tempValue());
                }
            };

            result.current = function () {
                return _tempValue();
            }

            //force subscribers to take original
            result.reset = function () {
                _actualValue.valueHasMutated();
                _tempValue(_actualValue());   //reset temp value
            };

            return result;
        };

        ko.bindingHandlers.datepicker = {
            init: function (element, valueAccessor, allBindingsAccessor) {
                //initialize datepicker with some optional options
                var options = datePickerOptions(allBindingsAccessor().datepickerOptions || {});
                $(element).datepicker(options);

                //handle the field changing
                ko.utils.registerEventHandler(element, "change", function () {
                    var observable = valueAccessor();
                    observable($(element).val());
                });

                //handle disposal (if KO removes by the template binding)
                ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
                    $(element).datepicker("destroy");
                });

            },
            update: function (element, valueAccessor) {
                var value = ko.utils.unwrapObservable(valueAccessor());

                //handle date data coming via json from Microsoft
                if (String(value).indexOf('/Date(') == 0) {
                    value = new Date(parseInt(value.replace(/\/Date\((.*?)\)\//gi, "$1")));
                }

                var current = $(element).datepicker("getDate");
                
                if (value - current !== 0) {
                    $(element).datepicker("setDate", value);
                }
            }
        };

        ko.bindingHandlers.checkedRadioToBool = {
            init: function (element, valueAccessor, allBindingsAccessor) {
                var observable = valueAccessor(),
            interceptor = ko.computed({
                read: function () {
                    return observable().toString();
                },
                write: function (newValue) {
                    observable(newValue === "true");
                },
                owner: this
            });
                ko.applyBindingsToNode(element, { checked: interceptor });
            }
        };

    }
}

function CustomKnockOutGrid(settings) {
    var instance = this;

    var defaultSettings = {
        pageSize: 10
    };

    var config = $.fn.extend(defaultSettings, settings)

    this.loading = ko.observable(true);
    this.list = ko.observableArray();
    this.pageList = ko.observableArray([]);
    this.pageSize = config.pageSize;
    this.currentPage = ko.observable(1);
    this.totalPages = ko.pureComputed(function () { return Math.ceil(instance.list().length / instance.pageSize) });
    this.disableNextPage = ko.pureComputed(function () { return instance.currentPage() >= instance.totalPages() }, this);
    this.disablePrevPage = ko.pureComputed(function () { return instance.currentPage() <= 1 }, this);
    this.totalListSize = ko.pureComputed(function () { return instance.list().length; }, this);

    this.goToPage = function (pageNumber) {
        var gridList = instance.list().slice((instance.pageSize * (pageNumber - 1)), (instance.pageSize * pageNumber));
        instance.pageList(gridList);
        instance.currentPage(pageNumber);
    };

    this.nextPage = function () {
        if (!instance.disableNextPage()) {
            instance.goToPage(instance.currentPage() + 1);
        }
    };

    this.prevPage = function () {
        if (!instance.disablePrevPage()) {
            instance.goToPage(instance.currentPage() - 1);
        }
    };

    this.firstPage = function () {
        if (!instance.disablePrevPage()) {
            instance.goToPage(1);
        }
    };

    this.lastPage = function () {
        if (!instance.disableNextPage()) {
            instance.goToPage(instance.totalPages());
        }
    };

    this.onPagerClick = function (page) {
        instance.goToPage(page.value);
    }

    this.reload = function (filteredResults) {
        instance.list(filteredResults);
        instance.goToPage(1);
    };

    this.pageNumbers = ko.pureComputed(function () {
        var pages = [];
        for (var i = 0; i < instance.totalPages(); i++) {
            pages.push({ value: i + 1 });
        }
        return pages;
    }, this);
}