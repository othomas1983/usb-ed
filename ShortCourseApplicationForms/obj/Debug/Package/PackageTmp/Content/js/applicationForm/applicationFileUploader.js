﻿$(document)
    .ready(function() {

        $("#idUploadProgress").hide();
        $("#certificateUploadProgress").hide();
        $("#cvUploadProgress").hide();
        $("#acceptanceFormUploadProgress").hide();
        $("#upload_Id").val('');
        $("#upload_Certificate").val('');
        $("#upload_CV").val('');
        $("#upload_AcceptanceForm").val('');


        $("#uploadBtn_Id")
            .click(function() {
                document.getElementById("idUpload").disabled = true;
            });

        $("#uploadBtn_Cert")
            .click(function() {
                document.getElementById("certificateUpload").disabled = true;
            });

         $("#uploadBtn_CV")
            .click(function() {
                document.getElementById("cvUpload").disabled = true;
            });

        document.getElementById("fileuploadId").onchange = function() {
            var filename = this.value.substring(this.value.lastIndexOf('\\') + 1);
            document.getElementById("upload_Id").value = filename;
            document.getElementById("fileuploadId").disabled = false;

        };

        document.getElementById("fileuploadCert").onchange = function() {
            var filename = this.value.substring(this.value.lastIndexOf('\\') + 1);
            document.getElementById("upload_Certificate").value = filename;
            document.getElementById("fileuploadCert").disabled = false;
        };

         document.getElementById("fileuploadCV").onchange = function() {
            var filename = this.value.substring(this.value.lastIndexOf('\\') + 1);
            document.getElementById("upload_CV").value = filename;
            document.getElementById("fileuploadCV").disabled = false;

        };
        
        document.getElementById("fileuploadAcceptanceForm").onchange = function () {
            var filename = this.value.substring(this.value.lastIndexOf('\\') + 1);
            document.getElementById("upload_AcceptanceForm").value = filename;
            document.getElementById("fileuploadAcceptanceForm").disabled = false;
        };
    });