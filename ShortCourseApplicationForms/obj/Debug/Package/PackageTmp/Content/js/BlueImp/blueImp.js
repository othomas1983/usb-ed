﻿var idGuid = "";
var certGuid = "";
var acceptanceGuid = "";
var cvGuid = "";

$(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    var idUrl = 'ApplicationForm/UploadFile?type=id';

    $('#fileuploadId').fileupload({
        url: idUrl,
        dataType: 'json',
        start: function (e) {
            $("#idUploadProgress").show();
            $("#idUploadResult").hide();
       
        },
        done: function (e, data) {
        //    $.each(data.result.files, function (index, file) {
        //        $('<p/>').text(file.name).appendTo('#files');
            //    });
            $("#idUploadProgress").hide();
            if (e.result == "OK" || data._response.result.result == "OK") {
                $("#qualificationDocumentUploadKey").val(e.qualificationDocumentUploadKey);               
                $("#idUploadResult").text('Successfully Uploaded.');
                $("#idUploadResult").show();

                var filename = data.files[0].name //this.value.substring(this.value.lastIndexOf('\\') + 1);
                $("#upload_Id").text(filename);
                $("#upload_Id").attr('display', 'true');
                $("#fileuploadId").disabled = false;
                $("#idUploadResult").css('color','green');
            } else if (data._response.result.result == "ERROR") {
                $("#idUploadResult").text(data._response.result.message);
                $("#idUploadResult").css('color', 'red');
                $("#idUploadResult").show();
            }
            else {
                $("#idUploadResult").text(data._response.result.result);
                $("#idUploadResult").css('color', 'red');
                $("#idUploadResult").show();
            }

            $('#idUploadProgress .progress-bar').attr('display', 'none');

            idGuid = data;
        },
        fail: function (e, data) {
            var msg = "";
            $.each(data.messages, function (index, error) {
                msg += error + "\n";
            });

            var filename = data.files[0].name;
            $("#upload_Id").text(filename);

            $("#idUploadResult").text(msg);
            $("#idUploadResult").css('color', 'red');
            $("#idUploadResult").show(msg);
            $("#idUploadProgress").hide();
        },
        progressall: function (e, data) {
            $('#idUploadProgress .progress-bar').attr('display', 'true');
        },
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});

$(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    var certificateUrl = 'ApplicationForm/UploadFile?type=matric';
    $('#fileuploadCert').fileupload({       
        url: certificateUrl,
        dataType: 'json',
        start: function (e) {
            $("#certificateUploadProgress").show();
            $("#certUploadResult").hide();
        },
        done: function (e, data) {
          //  $.each(data.result.files, function (index, file) {
          //      $('<p/>').text(file.name).appendTo('#files');
            //  });
            $("#certificateUploadProgress").hide();
            if (e.result == "OK" || data._response.result.result == "OK") {
                $("#qualificationDocumentUploadKey").val(e.qualificationDocumentUploadKey);              
                $("#certUploadResult").text('Successfully Uploaded.');
                $("#certUploadResult").show();

                var filename = data.files[0].name; //this.value.substring(this.value.lastIndexOf('\\') + 1);
                $("#upload_Certificate").text(filename);
                $("#fileuploadCert").disabled = false;
                $("#certUploadResult").css('color','green');
            } else if (data._response.result.result == "ERROR") {
                $("#certUploadResult").text(data._response.result.message);
                $("#certUploadResult").css('color', 'red');
                $("#certUploadResult").show();
            }
            else {
                $("#certUploadResult").text(data._response.result.result);
                $("#certUploadResult").css('color', 'red');
                $("#certUploadResult").show();
            }

            $('#certificateUploadProgress .progress-bar').attr('display', 'none');

            certGuid = data;
        },
        fail: function (e, data) {
            var msg = "";
            $.each(data.messages, function (index, error) {
                msg += error + "\n";
            });

            var filename = data.files[0].name;
            $("#upload_Certificate").text(filename);

            $("#certUploadResult").text(msg);
            $("#certUploadResult").css('color', 'red');
            $("#certUploadResult").show(msg);
            $("#certificateUploadProgress").hide();
        },
        progressall: function (e, data) {
            $('#certificateUploadProgress .progress-bar').attr('display', 'true');
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});

//CV Added
$(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    var CVUrl = 'ApplicationForm/UploadFile?type=cv';
    $('#fileuploadCV').fileupload({
        maxFileSize: 104857600,
        url: CVUrl,
        //dataType: 'json',
        start: function (e) {
            $("#cvUploadProgress").show();
            $("#cvUploadResult").hide();
        },
        done: function (e, data) {
            //  $.each(data.result.files, function (index, file) {
            //      $('<p/>').text(file.name).appendTo('#files');
            //  });
            $("#cvUploadProgress").hide();
            if (e.result == "OK" || data._response.result.result == "OK") {
                $("#cvDocumentUploadKey").val(e.cvDocumentUploadKey);
                $("#cvUploadResult").text('Successfully Uploaded.');
                $("#cvUploadResult").show();

                var filename = data.files[0].name; //this.value.substring(this.value.lastIndexOf('\\') + 1);
                $("#upload_CV").text(filename);
                $("#fileuploadCV").disabled = false;
                $("#cvUploadResult").css('color', 'green');
            } else if (data._response.result.result == "ERROR") {
                $("#cvUploadResult").text(data._response.result.message);
                $("#cvUploadResult").css('color', 'red');
                $("#cvUploadResult").show();
            }
            else {
                $("#cvUploadResult").text(data._response.result.result);
                $("#cvUploadResult").css('color', 'red');
                $("#cvUploadResult").show();
            }

            $('#cvUploadProgress .progress-bar').attr('display', 'none');

            cvGuid = data;
        },
        fail: function (e, data) {
            var msg = "";
            $.each(data.messages, function (index, error) {
                msg += error + "\n";
            });

            var filename = data.files[0].name;
            $("#upload_CV").text(filename);

            $("#cvUploadResult").text(msg);
            $("#cvUploadResult").css('color', 'red');
            $("#cvUploadResult").show(msg);
            $("#cvUploadProgress").hide();
        },
        progressall: function (e, data) {
            $('#cvUploadProgress .progress-bar').attr('display', 'true');
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});

$(function () {
    'use strict';
    var acceptanceUrl = 'ApplicationForm/UploadFile?type=acceptance';
    $('#fileuploadAcceptanceForm').fileupload({
        maxFileSize: 10485760,
        url: acceptanceUrl,
        //dataType: 'json',
        start: function (e) {
            $("#acceptanceFormUploadProgress").show();
            $("#acceptanceFormUploadResult").hide();
        },
        done: function (e, data) {
            $("#acceptanceFormUploadProgress").hide();
            if (e.result == "OK" || data._response.result.result == "OK") {
                $("#acceptanceDocumentUploadKey").val(e.acceptanceDocumentUploadKey);
                $("#acceptanceFormUploadResult").text('Successfully Uploaded.');
                $("#acceptanceFormUploadResult").show();

                var filename = data.files[0].name;
                $("#upload_AcceptanceForm").text(filename);
                $("#fileuploadAcceptanceForm").disabled = false;
                $("#acceptanceFormUploadResult").css('color', 'green');
            } else if (data._response.result.result == "ERROR") {
                $("#acceptanceFormUploadResult").text(data._response.result.message);
                $("#acceptanceFormUploadResult").css('color', 'red');
                $("#acceptanceFormUploadResult").show();
            }
            else {
                $("#acceptanceFormUploadResult").text(data._response.result.result);
                $("#acceptanceFormUploadResult").css('color', 'red');
                $("#acceptanceFormUploadResult").show();
            }

            $('#acceptanceFormUploadProgress .progress-bar').attr('display', 'none');

            acceptanceGuid = data;
        },
        fail: function (e, data) {
            var msg = "";
            $.each(data.messages, function (index, error) {
                msg += error + "\n";
            });

            var filename = data.files[0].name;
            $("#upload_AcceptanceForm").text(filename);

            $("#acceptanceFormUploadResult").text(msg);
            $("#acceptanceFormUploadResult").css('color', 'red');
            $("#acceptanceFormUploadResult").show(msg);
            $("#acceptanceFormUploadProgress").hide();
        },
        progressall: function (e, data) {
            $('#acceptanceFormUploadProgress .progress-bar').attr('display', 'true');
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});

$(function () {
    'use strict';
    var acceptanceUrl = 'ApplicationForm/UploadCompanyFile?type=companyPO';
    $('#fileuploadCompanyPurchaseOrder').fileupload({
        maxFileSize: 10485760,
        url: acceptanceUrl,
        //dataType: 'json',
        start: function (e) {
            $("#companyPurchaseOrderUploadProgress").show();
            $("#companyPurchaseOrderUploadResult").hide();
        },
        done: function (e, data) {
            $("#companyPurchaseOrderUploadProgress").hide();
            if (e.result == "OK" || data._response.result.result == "OK") {
                //$("#acceptanceDocumentUploadKey").val(e.acceptanceDocumentUploadKey);
                $("#companyPurchaseOrderUploadResult").text('Successfully Uploaded.');
                $("#companyPurchaseOrderUploadResult").show();

                var filename = data.files[0].name;
                $("#upload_CompanyPurchaseOrder").text(filename);
                $("#fileuploadCompanyPurchaseOrder").disabled = false;
                $("#companyPurchaseOrderUploadResult").css('color', 'green');
            } else if (data._response.result.result == "ERROR") {
                $("#companyPurchaseOrderUploadResult").text(data._response.result.message);
                $("#companyPurchaseOrderUploadResult").css('color', 'red');
                $("#companyPurchaseOrderUploadResult").show();
            }
            else {
                $("#companyPurchaseOrderUploadResult").text(data._response.result.result);
                $("#companyPurchaseOrderUploadResult").css('color', 'red');
                $("#companyPurchaseOrderUploadResult").show();
            }

            $('#companyPurchaseOrderUploadProgress .progress-bar').attr('display', 'none');

            acceptanceGuid = data;
        },
        fail: function (e, data) {
            var msg = "";
            $.each(data.messages, function (index, error) {
                msg += error + "\n";
            });

            var filename = data.files[0].name;
            $("#upload_CompanyPurchaseOrder").text(filename);

            $("#companyPurchaseOrderUploadResult").text(msg);
            $("#companyPurchaseOrderUploadResult").css('color', 'red');
            $("#companyPurchaseOrderUploadResult").show(msg);
            $("#companyPurchaseOrderUploadProgress").hide();
        },
        progressall: function (e, data) {
            $('#companyPurchaseOrderUploadProgress .progress-bar').attr('display', 'true');
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});