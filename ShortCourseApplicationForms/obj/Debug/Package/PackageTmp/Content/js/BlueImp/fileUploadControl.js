﻿var fileUploadHelpers = [];
function uploadControl(config) {

    var container = $("#" + config.divContainerId);
    var downloadLink = "/ClientDocuments/GetClientDocument?AdvisorId=" + config.advisorId + "&DocumentId="

    container.on("click", ".deleteSelectedFiles", function () {
        var toggleAllFilesInput = $(".toggleAllDocumentsDelete", container);
        var checkedItemsForDelete = $(".deleteFileCheck:checked", container);
        view.confirm({
            message: "Are you sure you want to delete the <strong>" + checkedItemsForDelete.length + "</strong> selected document(s)?",
            yes: function () {
                checkedItemsForDelete.each(function (i, el) {
                    var element = $(el);
                    var row = element.closest("tr");
                    $(".deleteDocumentLink:visible", row).trigger("forcedelete");
                    element.prop("checked", false);
                });
            }
        });
        toggleAllFilesInput.prop("checked", false).trigger("change");
    });

    container.on("click", ".addRow", function () {
        var row = $(this).closest("tr");
        row.trigger("copyrow");
    });

    container.on("click", ".removeRow", function () {
        var row = $(this).closest("tr");
        if (row.hasAttr("uniquename") && $("tr[uniquename=\"" + row.attr("uniquename") + "\"]", row.closest("table")).length == 1) {
            $(this).addClass("hide");
        } else {
            row.trigger("removerow");
        }
    });

    container.on("change", "input.toggleAllDocumentsDelete", function () {
        var checkbox = $(this);
        if (!checkbox.prop("checked")) {
            $("input.deleteFileCheck:visible", container).prop("checked", false);
            checkbox.attr("title", "Select All");
        }
        else {
            $("input.deleteFileCheck:visible", container).prop("checked", true);
            checkbox.attr("title", "Deselect All");
        }
    });

    container.on("copyrow", "table.uploadFileTable tr", function (event) {
        var row = $(event.target);
        var rowTemplate = row.clone();
        var control = $(".uploadFileInputAttach", rowTemplate);
        $(".removeRow", rowTemplate).removeClass("hide");
        row.after(rowTemplate);
        fileUploadHelpers.push({
            helper: newHelper(control).clearValues()
        });

        $("td", rowTemplate).stop().animate({ backgroundColor: "#eaeaea" }, 200, false, function () {
            $("td", rowTemplate).animate({ backgroundColor: "#fafafa" }, 2000);
        })

        row.closest("table").trigger("resetindex");
    });

    container.on("removerow", "table.uploadFileTable tr", function (event) {
        var row = $(event.target);
        var table = row.closest("table");
        if (row.hasAttr("documentid") && row.attr("documentid") != "") {
            view.confirm({
                message: "Deleting this row will delete the attached document as well. Are you sure?",
                yes: function () {
                    $(".deleteDocumentLink", row).trigger({
                        type: "forcedelete",
                        done: function () {
                            row.remove();
                            table.trigger("resetindex");
                        }
                    });
                }
            });
        } else {
            row.remove();
            table.trigger("resetindex");
        }
    });

    container.on("resetindex", "table.uploadFileTable", function (event) {
        var table = $("table:first", container);
        var indexCells = $("tr td:visible:first-child", table);
        indexCells.each(function (i, indexCell) {
            $(indexCell).html(i + 1);
        });
        table.trigger("updated");
    });

    $(document).ready(function () {

        $(document).bind('drop dragover', function (e) {
            e.preventDefault();
        });

        loadUserControl(config.divContainerId, view.loader("loading documents please wait...", "big"), config.getDocumentsUrl, { SourceType: config.sourceType, SourceId: config.sourceId, AdvisorId: config.advisorId }, function () {
            $(".uploadFileInputAttach", container).each(function (i, el) {
                var control = $(el);
                fileUploadHelpers.push({
                    helper: new newHelper(control)
                });
            });
            afterControlLoad();
            if (config.done) { config.done(container); }
        }, { actionType: "POST", async: false });

    });

    function afterControlLoad() {
        if (config.isSubmitted) {
            $(".deleteDocumentLink", container).remove();
            $(".hide_onFile", container).remove();
            $(".deleteFileCheck", container).remove();
            $(".deleteSelectedFiles", container).remove();
            $(".toggleAllDocumentsDelete", container).remove();
        }
    };

    function newHelper(control) {
        return new fileUploadHelper({ control: control, downloadLink: downloadLink, eventListener: container, advisorId: config.advisorId });
    }

};

