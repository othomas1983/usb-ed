﻿using System.Web;
using System.Web.Optimization;

namespace ShortCourseApplicationForms
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            BundleTable.EnableOptimizations = false;

            bundles.Add(new StyleBundle("~/content/style")
                .Include("~/Content/themes/base/jquery-ui.css",
                "~/Content/themes/base/datepicker.css",
                "~/Content/css/containers.css",
                "~/Content/css/Global.css",
                "~/Content/css/font-awsome-min.css",                
                "~/Content/css/fileuploader.css",
                "~/Content/css/intlTelInput.css"));

            bundles.Add(new StyleBundle("~/content/TermsAndConditions")
                .Include("~/Content/css/TermsAndConditions.css"));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Content/js/applicationForm/fileuploader.js",
                "~/Scripts/jquery-{version}.js",
                "~/Scripts/jquery-ui-{version}.js",
                "~/Content/js/jquery.unobstrusive-ajax.js",
                "~/Content/js/applicationForm/applicationForm.js",
                "~/Content/js/knockout/knockout-3.2.0.js",
                "~/Content/js/knockout/knockout.validation.min.js",
                "~/Content/js/intlTelInput.js",
                "~/Content/js/loadingoverlay.js"));

            bundles.Add(new ScriptBundle("~/bundles/fileUpload").Include(
                //"~/Content/js/BlueImp/fileUploadControl.js",
                //"~/Content/js/BlueImp/fileUploadHelper.js",
                "~/Content/js/BlueImp/jquery.fileupload.js",
                "~/Content/js/BlueImp/jquery.iframe-transport.js",
                //"~/Content/js/applicationForm/fileUpload.js"
                "~/Content/js/BlueImp/blueImp.js"
                ));
        }
    }
}