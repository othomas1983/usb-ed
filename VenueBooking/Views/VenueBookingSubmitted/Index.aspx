﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Success.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    <br />
    <div style="height:600px">
        <h2>The booking application has been successfully submitted.</h2>
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
