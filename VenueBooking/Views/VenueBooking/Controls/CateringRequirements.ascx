﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VenueBooking.Models.VenueBookingForm>" %>

-<fieldset>
    <legend>Catering Requirements</legend>
    <div>
        <h3 class="margin-top-small">Select a package below or choose a combination of the requirements</h3>
    </div>
    
    <div class="cateringPackage" id="package_a"> 
        <div>
            <span class="cateringHeading">Catering Package A</span>
            <input type="checkbox" data-bind="checked: package_A" />
        </div>
        <ul>
            <li>Tea & Coffee on arrival.</li>
            <li>Mid Morning Tea & Coffee served with a biscuit.</li>
            <li>Meal of the day with a glass of juice OR cans.</li>
            <li>Tea & Coffee.</li>
            <li data-bind="visible: package_A" ><span style="float:right"><a style="text-decoration:none" href="javascript:void(0)" data-bind="click: showPackageADetails">view</a></span></li>
        </ul>
    </div>

    <div class="cateringPackageDialog" id="cateringPackageADialog">
        <span>Please select the times you require the following items.</span>
        <div>
            <span><strong>Tea / Coffee</strong></span>
        </div>
        <div style="margin-left:25px;">
            <div>
                <label>Arrival Time</label>
                <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: cateringPackageATeaCoffeeArrivalTime"></select></td>
            </div>
             <div>            
                <label>Midmorning Time</label>
                 <select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: cateringPackageATeaCoffeeMidmorningTime"></select>
            </div>

             <div>            
                <label>Afternoon Time</label>
                 <select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: cateringPackageATeaCoffeeAfternoonTime"></select>
            </div>
        </div>
        <div>
            <span><strong>Lunch</strong></span>
            <div style="margin-left:25px;">
                <div>
                    <label>Lunch Time</label>
                    <select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: cateringPackageALunchTime"></select>
                </div>
            </div>
        </div>
    </div>

    <div class="cateringPackageDialog" id="cateringPackageBDialog">
        <span>Please select the times you require the following items.</span>
        <div>
            <span><strong>Tea / Coffee</strong></span>
        </div>
        <div style="margin-left:25px;">
            <div>
                <label>Arrival Time</label>
                <select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: cateringPackageBTeaCoffeeArrivalTime"></select>
            </div>
             <div>            
                <label>Midmorning Time</label>
                 <select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: cateringPackageBTeaCoffeeMidmorningTime"></select>
            </div>

             <div>            
                <label>Afternoon Time</label>
                 <select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: cateringPackageBTeaCoffeeAfternoonTime"></select>
            </div>
        </div>
        <div>
            <span><strong>Lunch</strong></span>
            <div style="margin-left:25px;">
                <div>
                    <label>Lunch Time</label>
                    <select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: cateringPackageBLunchTime"></select>
                </div>
            </div>
        </div>
    </div>

    <div class="cateringPackageDialog" id="cateringPackageCDialog">
        <span>Please select the times you require the following items.</span>
        <div>
            <span><strong>Tea / Coffee</strong></span>
        </div>
        <div style="margin-left:25px;">
            <div>
                <label>Arrival Time</label>
                <select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: cateringPackageCTeaCoffeeArrivalTime"></select>
            </div>
             <div>            
                <label>Midmorning Time</label>
                 <select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: cateringPackageCTeaCoffeeMidmorningTime"></select>
            </div>

             <div>            
                <label>Afternoon Time</label>
                 <select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: cateringPackageCTeaCoffeeAfternoonTime"></select>
            </div>
        </div>
        <div>
            <span><strong>Lunch</strong></span>
            <div style="margin-left:25px;">
                <div>
                    <label>Lunch Time</label>
                    <select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: cateringPackageCLunchTime"></select>
                </div>
            </div>
        </div>
    </div>

    <div class="cateringPackage" id="package_b">
        <div>   
            <span class="cateringHeading">Catering Package B</span>
            <input type="checkbox" data-bind="checked: package_B"/>
        </div>
        <ul>
            <li>Tea & Coffee on arrival.</li>
            <li>Mid Morning Tea & Coffee served with Tartlets.</li>
            <li>Buffet lunch with a glass of juice OR cans.</li>
            <li>Afternoon Tea & Coffee with biscuits.</li>
            <li data-bind="visible: package_B" ><span style="float:right; text-decoration:none"><a style="text-decoration:none" href="javascript:void(0)" data-bind="    click: showPackageBDetails">view</a></span></li>
        </ul>
    </div>

    <div class="cateringPackage" id="package_c">
        <div>
             <span class="cateringHeading">Catering Package C</span>
           <input type="checkbox" data-bind="checked: package_C"/>
        </div>
        <ul>
            <li>Tea & Coffee on arrival.</li>
            <li>Mid Morning Tea & Coffee served with a tea snack.</li>
            <li>Buffet lunch with a glass of juice OR cans.</li>
            <li>Tea & Cofee with biscuits.</li>
            <li data-bind="visible: package_C" ><span style="float:right; text-decoration:none"><a style="text-decoration:none" href="javascript:void(0)" data-bind="click: showPackageCDetails">view</a></span></li>
        </ul>
    </div>

    <table class="bookingForm">
        <thead>
            <tr>
                <th>Options</th>
                <th style="width:25px">Select</th>
                <th>Number of people</th>
                <th class="error" data-bind='visible: teaCoffeeCount.hasError, text: teaCoffeeCount.validationMessage'></th>
                <th>Morning time</th>
                <th>Midmorning time</th>
                <th>Lunch time</th>
                <th>Afternoon time</th>
                <th>Evening time</th>
            </tr>
        </thead>
        <tr aria-disabled="true">
            <td>Tea / Coffee</td>
            <td><input type="checkbox" data-bind="checked: teaCoffee" /> </td>
            <td><input type="number" data-bind="enable: teaCoffee, value: teaCoffeeCount" /></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', value: teaCoffeeMorningTime, enable: teaCoffee"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: teaCoffeeMidMorningTime, enable: teaCoffee"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: teaCoffeeLunchTime, enable: teaCoffee"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: teaCoffeeAfternoonTime, enable: teaCoffee"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: teaCoffeeEveningTime, enable: teaCoffee"></select></td>
        </tr>
        
        <tr>
            <td>Tea Snacks (Scones/Muffins/Sandwiches)</td>
            <td><input type="checkbox" data-bind="checked: teaSnack" /> </td>
            <td><input type="number" data-bind="enable: teaSnack, value: teaSnackCount" /></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: teaSnackMorningTime, enable: teaSnackCount"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: teaSnackMidMorningTime, enable: teaSnackCount"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: teaSnackLunchTime, enable: teaSnackCount"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: teaSnackAfternoonTime, enable: teaSnackCount"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: teaSnackEveningTime, enable: teaSnackCount"></select></td>
        </tr>


        <tr>
            <td>Tartlets</td>
            <td><input type="checkbox" data-bind="checked: tartlets" /> </td>
            <td><input type="number" data-bind="enable: tartlets, value: tartletsCount" /></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: tartletsMorningTime, enable: tartlets"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: tartletsMidMorningTime, enable: tartlets"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: tartletsLunchTime, enable: tartlets"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: tartletsAfternoonTime, enable: tartlets"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: tartletsEveningTime, enable: tartlets"></select></td>
        </tr>
        
        <tr>
            <td>Biscuits</td>
            <td><input type="checkbox" data-bind="checked: biscuits" /> </td>
            <td><input type="number" data-bind="enable: biscuits, value: biscuitsCount" /></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: biscuitsMorningTime, enable: biscuits"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: biscuitsMidMorningTime, enable: biscuits"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: biscuitsLunchTime, enable: biscuits"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: biscuitsAfternoonTime, enable: biscuits"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: biscuitsEveningTime, enable: biscuits"></select></td>
        </tr>
       
        <tr>
            <td>Meal of the day</td>
            <td><input type="checkbox" data-bind="checked: mealOfTheDay" /> </td>
            <td><input type="number" data-bind="enable: mealOfTheDay, value: mealOfTheDayCount" /></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: mealOfTheDayMorningTime, enable: mealOfTheDay"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: mealOfTheDayMidMorningTime, enable: mealOfTheDay"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: mealOfTheDayLunchTime, enable: mealOfTheDay"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: mealOfTheDayAfternoonTime, enable: mealOfTheDay"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: mealOfTheDayEveningTime, enable: mealOfTheDay"></select></td>
        </tr> 
       
        <tr>
            <td>Buffet Lunch</td>
            <td><input type="checkbox" data-bind="checked: buffetLunch" /> </td>
            <td><input type="number" data-bind="enable: buffetLunch, value: buffetLunchCount" /></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: buffetLunchMorningTime, enable: buffetLunch"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: buffetLunchMidMorningTime, enable: buffetLunch"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: buffetLunchLunchTime, enable: buffetLunch"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: buffetLunchAfternoonTime, enable: buffetLunch"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: buffetLunchEveningTime, enable: buffetLunch"></select></td>
        </tr>
        
       <tr>
            <td>Upmarket Lunch</td>
            <td><input type="checkbox" data-bind="checked: upmarketLunch" /> </td>
            <td><input type="number" data-bind="enable: upmarketLunch, value: upmarketLunchCount" /></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: upmarketLunchMorningTime, enable: upmarketLunch"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: upmarketLunchMidMorningTime, enable: upmarketLunch"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: upmarketLunchLunchTime, enable: upmarketLunch"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: upmarketLunchAfternoonTime, enable: upmarketLunch"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: upmarketLunchEveningTime, enable: upmarketLunch"></select></td>
        </tr>
       
        <tr>
            <td>Dessert</td>
            <td><input type="checkbox" data-bind="checked: dessert" /> </td>
            <td><input type="number" data-bind="enable: dessert, value: dessertCount" /></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: dessertMorningTime, enable: dessert"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: dessertMidMorningTime, enable: dessert"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: dessertLunchTime, enable: dessert"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: dessertAfternoonTime, enable: dessert"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: dessertEveningTime, enable: dessert"></select></td>
        </tr>

       <tr>
            <td>Dinner</td>
            <td><input type="checkbox" data-bind="checked: dinner" /> </td>
            <td><input type="number" data-bind="enable: dinner, value: dinnerCount" /></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: dinnerMorningTime, enable: dinner"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: dinnerMidMorningTime, enable: dinner"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: dinnerLunchTime, enable: dinner"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: dinnerAfternoonTime, enable: dinner"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: dinnerEveningTime, enable: dinner"></select></td>
        </tr>

       <tr>
            <td>Cans</td>
            <td><input type="checkbox" data-bind="checked: cans" /> </td>
            <td><input type="number" data-bind="enable: cans, value: cansCount" /></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: cansMorningTime, enable: cans"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: cansMidMorningTime, enable: cans"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: cansLunchTime, enable: cans"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: cansAfternoonTime, enable: cans"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: cansEveningTime, enable: cans"></select></td>
        </tr>

       <tr>
            <td>Mineral Water (500ml)</td>
            <td><input type="checkbox" data-bind="checked: mineralWater" /> </td>
            <td><input type="number" data-bind="enable: mineralWater, value: mineralWaterCount" /></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: mineralWaterMorningTime, enable: mineralWater"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: mineralWaterMidMorningTime, enable: mineralWater"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: mineralWaterLunchTime, enable: mineralWater"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: mineralWaterAfternoonTime, enable: mineralWater"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: mineralWaterEveningTime, enable: mineralWater"></select></td>
        </tr>
       
        <tr>
            <td>Juice per class</td>
            <td><input type="checkbox" data-bind="checked: juicePerGlass" /> </td>
            <td><input type="number" data-bind="enable: juicePerGlass, value: juicePerGlassCount" /></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: juicePerGlassMorningTime, enable: juicePerGlass"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: juicePerGlassMidMorningTime, enable: juicePerGlass"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: juicePerGlassLunchTime, enable: juicePerGlass"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: juicePerGlassAfternoonTime, enable: juicePerGlass"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: juicePerGlassEveningTime, enable: juicePerGlass"></select></td>
        </tr>

        <tr>
            <td>Jugs of juice</td>
            <td><input type="checkbox" data-bind="checked: jugsOfJuice" /> </td>
            <td><input type="number" data-bind="enable: jugsOfJuice, value: jugsOfJuiceCount" /></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: jugsOfJuiceMorningTime, enable: jugsOfJuice"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: jugsOfJuiceMidMorningTime, enable: jugsOfJuice"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: jugsOfJuiceLunchTime, enable: jugsOfJuice"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: jugsOfJuiceAfternoonTime, enable: jugsOfJuice"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: jugsOfJuiceEveningTime, enable: jugsOfJuice"></select></td>
        </tr>
       
       <tr>
            <td>Braai - 2 meats</td>
            <td><input type="checkbox" data-bind="checked: braai2Meats" /> </td>
            <td><input type="number" data-bind="enable: braai2Meats, value: braai2MeatsCount" /></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: braai2MeatsMorningTime, enable: braai2Meats"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: braai2MeatsMidMorningTime, enable: braai2Meats"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: braai2MeatsLunchTime, enable: braai2Meats"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: braai2MeatsAfternoonTime, enable: braai2Meats"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: braai2MeatsEveningTime, enable: braai2Meats"></select></td>
        </tr>

        <tr>
            <td>Braai - 3 meats</td>
            <td><input type="checkbox" data-bind="checked: braai3Meats" /> </td>
            <td><input type="number" data-bind="enable: braai3Meats, value: braai3MeatsCount" /></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: braai3MeatsMorningTime, enable: braai3Meats"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: braai3MeatsMidMorningTime, enable: braai3Meats"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: braai3MeatsLunchTime, enable: braai3Meats"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: braai3MeatsAfternoonTime, enable: braai3Meats"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: braai3MeatsEveningTime, enable: braai3Meats"></select></td>
        </tr>

        <tr>
            <td>Spit braai (over 40 pax)</td>
            <td><input type="checkbox" data-bind="checked: spitBraai" /> </td>
            <td><input type="number" data-bind="enable: spitBraai, value: spitBraaiCount" /></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: spitBraaiMorningTime, enable: spitBraai"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: spitBraaiMidMorningTime, enable: spitBraai"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: spitBraaiLunchTime, enable: spitBraai"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: spitBraaiAfternoonTime, enable: spitBraai"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: spitBraaiEveningTime, enable: spitBraai"></select></td>
        </tr>

        <tr>
            <td>Potjie x 2</td>
            <td><input type="checkbox" data-bind="checked: potjie" /> </td>
            <td><input type="number" data-bind="enable: potjie, value: potjieCount" /></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: potjieMorningTime, enable: potjie"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: potjieMidMorningTime, enable: potjie"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: potjieLunchTime, enable: potjie"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: potjieAfternoonTime, enable: potjie"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: potjieEveningTime, enable: potjie"></select></td>
        </tr>
       
        <tr>
            <td>Cocktail Half</td>
            <td><input type="checkbox" data-bind="checked: cockTailHalf" /> </td>
            <td><input type="number" data-bind="enable: cockTailHalf, value: cockTailHalfCount" /></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: cockTailHalfMorningTime, enable: cockTailHalf"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: cockTailHalfMidMorningTime, enable: cockTailHalf"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: cockTailHalfLunchTime, enable: cockTailHalf"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: cockTailHalfAfternoonTime, enable: cockTailHalf"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: cockTailHalfEveningTime, enable: cockTailHalf"></select></td>
        </tr>
       
        <tr>
            <td>Cocktail Full</td>
            <td><input type="checkbox" data-bind="checked: cockTailFull" /> </td>
            <td><input type="number" data-bind="enable: cockTailFull, value: cockTailFullCount" /></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: cockTailFullMorningTime, enable: cockTailFull"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: cockTailFullMidMorningTime, enable: cockTailFull"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: cockTailFullLunchTime, enable: cockTailFull"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: cockTailFullAfternoonTime, enable: cockTailFull"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: cockTailFullEveningTime, enable: cockTailFull"></select></td>
        </tr>
       
       <tr>
            <td>Bowl Foods</td>
            <td><input type="checkbox" data-bind="checked: bowlFoods" /> </td>
            <td><input type="number" data-bind="enable: bowlFoods, value: bowlFoodsCount" /></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: bowlFoodsMorningTime, enable: bowlFoods"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: bowlFoodsMidMorningTime, enable: bowlFoods"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: bowlFoodsLunchTime, enable: bowlFoods"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: bowlFoodsAfternoonTime, enable: bowlFoods"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: bowlFoodsEveningTime, enable: bowlFoods"></select></td>
        </tr>

        <tr>
            <td>Buffet Breakfast</td>
            <td><input type="checkbox" data-bind="checked: buffetBreakfast" /> </td>
            <td><input type="number" data-bind="enable: buffetBreakfast, value: buffetBreakfastCount" /></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: buffetBreakfastMorningTime, enable: buffetBreakfast"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: buffetBreakfastMidMorningTime, enable: buffetBreakfast"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: buffetBreakfastLunchTime, enable: buffetBreakfast"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: buffetBreakfastAfternoonTime, enable: buffetBreakfast"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: buffetBreakfastEveningTime, enable: buffetBreakfast"></select></td>
        </tr>

       <tr>
            <td>Finger Breakfast</td>
            <td><input type="checkbox" data-bind="checked: fingerBreakfast" /> </td>
            <td><input type="number" data-bind="enable: fingerBreakfast, value: fingerBreakfastCount" /></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: fingerBreakfastMorningTime, enable: fingerBreakfast"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: fingerBreakfastMidMorningTime, enable: fingerBreakfast"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: fingerBreakfastLunchTime, enable: fingerBreakfast"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: fingerBreakfastAfternoonTime, enable: fingerBreakfast"></select></td>
            <td><select class="timeOption select-sm" data-bind="options: cateringTimes, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: fingerBreakfastEveningTime, enable: fingerBreakfast"></select></td>
        </tr>
       
        <tr>
            <td>Corkage</td>
            <td><input type="checkbox" data-bind="checked: corkage" /> </td>
            <td><input type="number" data-bind="enable: corkage, value: corkageCount" /></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </table>
</fieldset>
