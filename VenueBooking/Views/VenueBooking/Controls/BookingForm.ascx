﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<VenueBooking.Models.VenueBookingForm>" %>

<%@ Import Namespace="USB_ED.Models" %>

<fieldset>
    <legend>Event Information</legend>
    
    <div>
        <label style="width:206px">Event venue<span class="requiredIndicator">*</span></label>
        <input type="text" data-bind="value:venueName, enable:false, readonly:true" />

    </div>
    <div>
        <label style="width:206px">Name of event:<span class="requiredIndicator">*</span></label>
        <input type="text" data-bind="value:eventName"/>
    </div>

    <div>
        <div class="infoBox">
           <label>Once booking has been received, a booking confirmation, subject to availablity, will be issued within 48 hours in respect of the venue booked and sent to the email address provided.</label>
        </div>
    </div>  

     <div>
       <label>Number of people expected to attend: (if unsure, please estimate):<span class="requiredIndicator">*</span></label>
         <input class="input-sm" type="number" data-bind="value: bookingNumberOfPeopleExpected" />
    </div>
    
    <div style="margin-bottom:10px">
        <label style="min-width: 115px;">Single day booking?</label>
        <label style="min-width: 0;"><input type="radio" name="singleDay" data-bind="checked: singleDay, value: true"/>Yes</label>
        <label style="min-width: 0;"><input type="radio" name="singleDay" data-bind="checked: singleDay, value: false" />No</label>
    </div>

     <%--<div style="margin-bottom:10px">
       <span>Full day:</span>
         <input type="radio" name="fullDay" data-bind="checked: fullDay, value: true" />
        <span class="marginLeft40">Half day:</span>
         <input type="radio" name="fullDay" data-bind="checked: fullDay, value: false" />
    </div>--%>
    

    <div>
       <label class="smallLabel">Event start date:<span class="requiredIndicator">*</span></label>
        <input id="venueStartDate" class="dateTableHeader input-sm" type="text" data-bind="value: bookingStartDate, bookingStartDateFormat: bookingStartDate" />
        <label class="smallLabel" data-bind="attr: { 'style': singleDay() ? 'display: none !important' : 'display: inline !important' }">Event end date:<span class="requiredIndicator">*</span></label>
        <input id="venueEndDate" class="dateTableHeader input-sm" type="text" data-bind="visible: !singleDay(), enable: bookingStartDate() != undefined ? bookingStartDate().length > 0 : false, value: bookingEndDate, bookingEndDateFormat: bookingEndDate" />
        <label class="smallLabel">Time in:</label>
        <select class="input-sm" data-bind="enable: bookingStartDate() != undefined ? bookingStartDate().length > 0 : false, options: venueTimes, optionsText: 'Text', optionsValue: 'Value', value: bookingTimeIn, valueAllowUnset: true"></select>
        <label class="smallLabel">Time out:</label>
        <select class="input-sm" data-bind="options: availableBookingTimeOut, optionsText: 'Text', optionsValue: 'Value', value: bookingTimeOut, valueAllowUnset: true, enable: availableBookingTimeOut().length > 0"></select>
        <input class="btn btn-primary" type="button" data-bind="enable: bookingStartDate() != undefined ? bookingStartDate().length > 0 && availableBookingTimeOut().length > 0 : false, click: LoadVenueAvailibility" value ="Check availability" />
    </div>    
    <div style="text-align: center;">NOTE: A grid for the selected venue will be displayed showing all available times for the week in which the selected day falls</div>
    
    <div style="margin-top: 10px;" class="textAreaContainer" data-bind="visible: !singleDay()">
        <label style="width: 206px;">Event Booking Notes:</label>
        <textarea cols="50" rows="4" data-bind="value: eventBookingNotes"></textarea>
    </div>

</fieldset>

<fieldset id="personalInformation">
    <legend>Client & Billing Information</legend>
    <div>
       <label>Person requesting venue:<span class="requiredIndicator">*</span></label>
        <input type="text" data-bind="value: personRequestingVenue"/>
        <span class="error" data-bind='visible: personRequestingVenue.hasError, text: personRequestingVenue.validationMessage'></span>
    </div>

    <div>
       <label>Contact number:<span class="requiredIndicator">*</span></label>
        <input type="text" data-bind="value: contactNumber"/>
    </div>

    <div>
       <label>E-Mail:<span class="requiredIndicator">*</span></label>
        <input type="text" data-bind="value: eMail"/>
    </div>

    <div>
        <label>Business unit:<span class="requiredIndicator">*</span></label>
        <select id="ddBusinessUnits" class="select-md" data-bind="options: businessUnits, optionsText: 'Text', optionsValue: 'Value', valueAllowUnset: true, value: businessUnitId"></select>&nbsp Select 'Private' if you have no affiliation with the University.
    </div>

    <div data-bind="visible: !isPrivateBusinessUnit()">
        <label>Cost centre</label>
        <input type="text" data-bind="value: costCentre" />
    </div>

    
    <!-- ko if: isPrivateBusinessUnit -->
    <div>
       <label>Company name:</label>
        <input type="text" data-bind="value: companyName"/>
    </div>

    <div class="textAreaContainer">
       <label>Billing address:<span class="requiredIndicator">*</span></label>
        <textarea rows="7" cols="50" data-bind="value: billingAddress"></textarea>
    </div>

    <div>
       <label>VAT no:</label>
        <input type="text" data-bind="value: vatNumber"/>
    </div>

    <div>
       <label>VAT registration no:</label>
        <input type="text" data-bind="value: vatRegistrationNumber"/>
    </div>

    <div>
       <label>Person to be billed:<span class="requiredIndicator">*</span></label>
        <input type="text" data-bind="value: personToBeBilled"/>
    </div>
    <!-- /ko -->
</fieldset>

<fieldset id="venueRequirements">
    <legend>Venue Requirements</legend>   

    <div>
        <label>Data projector / Proxima when hiring breakaway</label>
        <input type="checkbox" data-bind="checked:dataProjector" />
    </div>

    <div>
        <label>Microphone</label>
        <input type="checkbox" data-bind="checked: microPhone" />
    </div>

    <div>
        <label>Laser pointer</label>
        <input type="checkbox" data-bind="checked: laserPointer" />
    </div>

    <div>
        <label>Pen & paper</label>
        <input type="checkbox" data-bind="checked: penAndPaper" />
    </div>

    <div>
        <label>Still bottled mineral water</label>
        <input type="checkbox" data-bind="checked: mineralWater" />
    </div>

    <div>
        <label>Flowers (minimum of R200)</label>
        <input type="checkbox" data-bind="checked: flowers" />
    </div>

    <div class="textAreaContainer">
        <label>Special requirements</label>
        <textarea cols="50" rows="4" data-bind="value: specialRequirements"></textarea>
    </div>

    <div>
        <label style="width: 167px;">Is catering required?</label>
        <label style="min-width: 0; width: 42px;"><input type="radio" name="careting" data-bind="checked: cateringRequired, value: true"/>Yes</label>
        <label style="min-width: 0; width: 42px;"><input type="radio" name="careting" data-bind="checked: cateringRequired, value: false" />No</label>
    </div>
    
    <div data-bind="attr: { 'style': cateringRequired() ? 'display: inline !important' : 'display: none !important' }">
        <span>For catering services please call Fedics at +27 (21) 9184333 or email your catering request to <a href="mailto: fedics@belpark.sun.ac.za"> fedics@belpark.sun.ac.za</a></span>
    </div>

</fieldset>

