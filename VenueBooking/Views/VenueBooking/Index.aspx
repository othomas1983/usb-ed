﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<VenueBooking.Models.VenueBookingForm>" %>

<%@ Register Src="~/Views/VenueBooking/Controls/BookingForm.ascx" TagPrefix="uc1" TagName="BookingForm" %>
<%@ Register Src="~/Views/VenueBooking/Controls/CateringRequirements.ascx" TagPrefix="uc1" TagName="CateringRequirements" %>


<%@ Import Namespace="VenueBooking.Models" %>
<%@ Import Namespace="System.Web.Script.Serialization" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <%: System.Web.Optimization.Scripts.Render("~/bundles/jquery") %>
    <%: System.Web.Optimization.Scripts.Render("~/bundles/venueBooking") %>
    <%: System.Web.Optimization.Styles.Render("~/bundles/calendar") %>
    <%: System.Web.Optimization.Scripts.Render("~/bundles/calendar") %>


    <form method="post" id="frmVenueBooking">

        <label class="requiredIndicator">* indicates a compulsory field</label>

        <div id="bookingAlert" class="alert alert-danger" style="display: none">
            <a href="#" class="alert-close close" aria-label="close">&times;</a>
            <strong>Error!</strong> Please make sure that all required fields are filled in correctly.
        </div>
        
        <div id="noVenueAlert" class="alert alert-danger" data-bind="visible:!venueFound()">
            <strong>Error!</strong> <label data-bind="text:venueResultDetails()"></label>
        </div>
        

        <div data-bind="enable:venueFound">
            <uc1:BookingForm runat="server" ID="BookingForm" />
        </div>

        <%--<div data-bind="visible: cateringRequired, enable:venueFound()">
            <uc1:CateringRequirements runat="server" ID="CateringRequirements" />
        </div>--%>

        <div id="progressDialog">
            <strong class="message"></strong>
            <img src="./Content/img/progress.gif" class="progressSpinner" />
        </div>

        <div class="submitContainer">
            <input type="button" value="Submit booking" data-bind="click: submitBooking, enable:venueFound" />
        </div>

        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModal-label" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h2 class="modal-title" id="myModal-label" data-bind="text: availabilityVenueName"></h2>
                        This grid shows available times for the selected Venue for the week in which your selected date falls.
                    </div>
                    
                    <div class="modal-body">
                        <div id='calendar'></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>


        <footer></footer>

        <script type="text/javascript">
            var model = <%:Html.Raw(Newtonsoft.Json.JsonConvert.SerializeObject(this.Model)) %>
            var view = new View();
            
            $(document).ready(function () {
                var venueBooking = VenueBooking(model);
                ko.applyBindings(venueBooking);
         });
        </script>



    </form>
</asp:Content>






<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
