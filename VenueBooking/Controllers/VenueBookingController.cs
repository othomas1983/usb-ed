﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VenueBooking.Models;

namespace VenueBooking.Controllers
{
    public class VenueBookingController : Controller
    {
        public ActionResult Index()
        {
            var roomNumber = string.Empty;
            var iID = string.Empty;
#if DEBUG
            roomNumber = "210";
#else
            roomNumber = HttpContext.Request["roomNumber"];
            iID = HttpContext.Request["iID"];
#endif

            //roomNumber = HttpContext.Request["roomNumber"];
            iID = HttpContext.Request["iID"];

            var venueBooking = new VenueBookingForm();
            VenueBookingFormInfo.LoadInfoMembers();

            var task = new VenueBooking.Models.VenueBookingTasks();
            var venue = new Venue();

            if (roomNumber.IsNotNullOrEmpty())
            {
                venueBooking.Venue = task.LoadVenue(roomNumber);
                venueBooking.VenueId = task.LoadVenueId(roomNumber);
                venueBooking.RoomNumber = roomNumber;
                venueBooking.BookingStartDate = null;
                venueBooking.BookingEndDate = null;
                venueBooking.Venue.VenueFound = true;

                if (venueBooking.Venue.Result == "Error")
                {
                    venueBooking.Venue.VenueFound = false;
                }
            }

            if (iID.IsNotNullOrEmpty())
            {
                venueBooking = task.LoadEvent(Convert.ToInt32(iID));
                if (venueBooking.Venue.IsNotNull())
                {
                    venueBooking.Venue.VenueFound = true;
                }
            }

            if (venueBooking.Venue.IsNull())
            {
                venueBooking.Venue = new Venue()
                {
                    Name = "",
                    Number = "0",
                    Result = "Error",
                    ResultDetails = "No venues were found matching the venue number. {0}".FormatInvariantCulture(roomNumber),
                    Seats = "0",
                    Site = "No Site",
                    Type = "No Type",
                    VenueFound = false
                };
            }

            return View(venueBooking);
        }

        [HttpPost]
        public ActionResult Index(VenueBookingForm model)
        {
            var venueBooking = new VenueBookingForm();

            ResolveViewBag(model);

            //temp testing
            //if (model.CateringRequirements.IsNotNull())
            //{
            //    var htmlToPDf = new HtmlToPdf();
            //    var stream = htmlToPDf.Convert("", model);

            //    //var mail = new USB_ED.eMail();
            //    //mail.SendVenueBookingMail(model.eMail, "Confirmation", "Check this out", "VenueBooking.pdf", stream);
            //}
            try
            {
                if (model.Venue.IsNotNull())
                {
                    if (model.Venue.Result == "Error")
                    {
                        ModelState.AddModelError("", model.Venue.ResultDetails);
                    }
                }

                if (ModelState.IsValid)
                {
                    var mail = new Email();
                    mail.SendVenueBookingMail(model);

                    var task = new VenueBooking.Models.VenueBookingTasks();
                    var result = task.SubmitBookingForm(model);
                    ModelState.Clear();
                    if (Convert.ToInt32(result) > 0)
                    {
                        return Redirect("VenueBookingSubmitted/Index");
                    }
                    else
                    {
                        ModelState.AddModelError("Application", result);
                    }
                }
                return View(model);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Application", "Unable to submit the booking. Please try again later. " + ex.Message);
                return View();
            }
        }

        public ActionResult SaveBooking(VenueBookingForm venueBooking)
        {
            //venueBooking.CateringRequirements = cateringRequirements;

            var mail = new Email();
            mail.SendVenueBookingMail(venueBooking);

            var task = new VenueBooking.Models.VenueBookingTasks();
            var result = task.SubmitBookingForm(venueBooking);

            ModelState.Clear();
            if (Convert.ToInt32(result) > 0)
            {
                return Json(new { Success = true, Message = "Successfully saved" });
            }
            else
            {
                return Json(new { Success = false, Message = "Booking not saved" });
            }
        }

        public ActionResult LoadCateringRequirementTimes()
        {
            var task = new VenueBookingTasks();

            var times = task.LoadCateringTimes();
            return Json(new { CateringTimes = times }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadVenueTimes()
        {
            var task = new VenueBookingTasks();

            var times = task.LoadVenueTimes();
            return Json(new { VenueTimes = times }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadBusinessUnits()
        {
            var task = new VenueBookingTasks();

            var times = task.LoadBusinessUnits();
            return Json(new { BusinessUnits = times }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadVenueAvailability(VenueBookingForm venueBooking)
        {
            var task = new VenueBookingTasks();
            var result = new List<VenueAvailabiliy>();

            if (venueBooking.IsNotNull())
            {
                result = venueBooking.BookingEndDate.HasValue
                    ? task.LoadVenueAvailability(venueBooking.BookingStartDate.GetValueOrDefault(),
                        venueBooking.BookingEndDate.GetValueOrDefault(), venueBooking.VenueName)
                    : task.LoadVenueAvailability(venueBooking.BookingStartDate.GetValueOrDefault(), null,
                        venueBooking.VenueName);
            }
            
           return Json(new { BookedList = result },JsonRequestBehavior.AllowGet);
        }

        private void ResolveViewBag(VenueBookingForm model)
        {
            if (VenueBookingFormInfo.VenueTimeInOutList.IsNull()
                || VenueBookingFormInfo.CateringRequirementsTime.IsNull())
            {
                VenueBookingFormInfo.LoadInfoMembers();
            }

            if (model.BusinessUnits.IsNull())
            {
                var task = new VenueBooking.Models.VenueBookingTasks();
                var businessUnits = task.LoadBusinessUnits();
                ViewBag.BusinessUnits = businessUnits;
            }
            else
            {
                ViewBag.BusinessUnits = model.BusinessUnits;
            }
            
            ViewBag.VenueTimes = VenueBookingFormInfo.VenueTimeInOutList;
        }
    }
}
