﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using VenueBooking.Models;

namespace VenueBooking.Models
{
    public class VenueBookingTasks
    {
        #region Public Methods

        public Venue LoadVenue(string roomNumber)
        {
            var campusVenues = new VenueBookingDomian.CampusVenuesWS.CampusVenues(); //za.ac.sun.belpark.servicebus.CampusVenues();
            var result = campusVenues.GetVenueDetails(roomNumber);

            var venueJson = JsonConvert.SerializeObject(result);
            var venue = JsonConvert.DeserializeObject<Venue>(venueJson);

            return venue;
        }

        public List<SelectListItem> LoadBusinessUnits()
        {
            using (var entity = new VenueBookingDomian.EntityFrameworkModel.BookingsEntities())
            {
                var list = entity.sp_BusinessUnitRead().ToList();
                var business = ResolveBusinessUnits(list);

                return business;
            }
        }

        public List<SelectListItem> LoadCateringTimes()
        {
            var list = new List<SelectListItem>();

            var times = ",07:30,08:00,08:30,09:00,09:30,10:00,10:30,11:00,11:30,12:00,12:30,13:00,13:30,14:00,14:30,15:00,15:30,16:00,16:30,17:00,17:30,18:00,18:30,19:00,19:30,20:00".Split(',');
            
            foreach (var item in times)
            {
                list.Add(new SelectListItem { Text = item, Value = item });
            }

            return list;
        }

        public List<SelectListItem> LoadVenueTimes()
        {
            var list = new List<SelectListItem>();

            var times = ",07:00,08:00,09:00,10:00,11:00,12:00,13:00,14:00,15:00,16:00,17:00,18:00,19:00,20:00,21:00".Split(',');

            foreach (var item in times)
            {
                list.Add(new SelectListItem { Text = item, Value = item });
            }

            return list;
        }

        public VenueBookingForm LoadEvent(int venueId)
        {
            var model = new VenueBookingForm();

            using (var entity = new VenueBookingDomian.EntityFrameworkModel.BookingsEntities())
            {
                var venueBooking = new VenueBookingForm();

                var booking = entity.sp_VenueBookingRead(venueId).FirstOrDefault();
                if (booking.IsNotNull())
                {
                    model = ResolveBooking(booking, venueBooking);

                    var catering = entity.sp_CateringInfoRead(venueId).ToList();
                    ResolveCateringRequirements(catering, model);
                }
            }

            return model;
        }

        public List<VenueAvailabiliy> LoadVenueAvailability(DateTime venueStartDate, DateTime? venueEndDate, string venueName)
        {
            var bookedList = new List<VenueAvailabiliy>();

            //int delta = DayOfWeek.Monday - venueStartDate.DayOfWeek;
            //if(delta > 0)
            //    delta -= 7;

            //DateTime monday = venueStartDate.AddDays(delta);
            
            var startDate = venueStartDate.Date;
            var endDate = venueEndDate.HasValue ? venueEndDate.GetValueOrDefault() : startDate.AddHours(23);

            using (var entity = new VenueBookingDomian.EntityFrameworkModel.BookingsEntities())
            {
                var booking = entity.sp_VenueAvailabiliyRead(startDate, endDate, venueName).ToList();
                if (booking.IsNotNull())
                {
                    bookedList = ResolveVenueAvailibility(booking);
                }

                return bookedList;
            }
        }

        public string SubmitBookingForm(VenueBookingForm model)
        {
            try
            {
                using (var entity = new VenueBookingDomian.EntityFrameworkModel.BookingsEntities())
                {                    
                    model.VenueId = LoadVenueId(model.RoomNumber);

                    var timeIn = new System.TimeSpan();
                    var timeOut = new System.TimeSpan();

                    System.TimeSpan.TryParse(model.BookingTimeIn, out timeIn);
                    System.TimeSpan.TryParse(model.BookingTimeOut, out timeOut);

                    var output = new SqlParameter("ResultList", SqlDbType.NVarChar, 4000);
                    output.Direction = ParameterDirection.Output;

                    if (model.VenueBookingID > 0)
                    {
                        entity.sp_VenueBookingUpdate(model.VenueBookingID, model.EventName, model.CompanyName, model.PersonRequestingVenue, null,
                            model.ContactNumber, model.eMail, null, model.VATNumber, model.VATRegistrationNumber, model.BillingAddress,
                            model.PersonToBeBilled, null, model.ContactNumber, model.eMail, null, model.BookingNumberOfPeopleExpected,
                            model.VenueId, null, null, timeIn, timeOut, null, model.BookingStartDate, Convert.ToInt32(model.BusinessUnitId),
                            model.DataProjector, model.MicroPhone, model.LaserPointer, model.PenAndPaper, model.MineralWater, model.Flowers,
                            model.SpecialRequirements, model.CostCentre, null, model.BookingEndDate, model.BookingNotes, model.SingleDay);

                        //if (model.CateringRequirements.IsNotNull())
                        //{
                        //    PopulateTVP(model, model.VenueBookingID, entity);
                        //}
                    }
                    else
                    {
                        var venueBookingId = new System.Data.Entity.Core.Objects.ObjectParameter("VenueBookingID", SqlDbType.Int);

                        var result = entity.sp_VenueBookingCreate(model.EventName, model.CompanyName, model.PersonRequestingVenue, null,
                            model.ContactNumber, model.eMail, null, model.VATNumber, model.VATRegistrationNumber, model.BillingAddress,
                            model.PersonToBeBilled, null, model.ContactNumber, model.eMail, null, model.BookingNumberOfPeopleExpected,
                            model.VenueId, null, null, timeIn, timeOut, null, model.BookingStartDate, Convert.ToInt32(model.BusinessUnitId),
                            model.DataProjector, model.MicroPhone, model.LaserPointer, model.PenAndPaper, model.MineralWater, model.Flowers,
                            model.SpecialRequirements, model.CostCentre, null, venueBookingId, model.BookingEndDate, model.BookingNotes, model.SingleDay);

                        //if (model.CateringRequirements.IsNotNull())
                        //{
                        //    PopulateTVP(model, Convert.ToInt32(venueBookingId.Value), entity);
                        //}
                    }

                    //TODO: change 
                    //return result.AsString();
                    return "1";
                }
            }
            catch (Exception ex)
            {
                return "Unable to process your request at this time. Please try again later or contact the University of Stellenbosch Belville Park Campus to finalise your booking." + Environment.NewLine +
                    ex.Message;
            }
        }

        public int LoadVenueId(string roomNumber)
        {
            using (var entity = new VenueBookingDomian.EntityFrameworkModel.BookingsEntities())
            {
                var venue = entity.sp_VenueRead(roomNumber).FirstOrDefault();
                return venue.VenueID;
            }
        }

        #endregion

        #region Private Methods

        private List<VenueBookingDomian.EntityFrameworkModel.sp_CateringInfoRead_Result> LoadCateringInfo(int venueBookingId)
        {
            using (var entity = new VenueBookingDomian.EntityFrameworkModel.BookingsEntities())
            {
                var cateringInfo = entity.sp_CateringInfoRead(venueBookingId).ToList();

                return cateringInfo;
            }
        }

        private VenueBookingForm ResolveBooking(VenueBookingDomian.EntityFrameworkModel.sp_VenueBookingRead_Result result, VenueBookingForm venueBooking)
        {
            var startHour = result.TimeIn.Value.Hours.AsString().PadLeft(2, '0');
            var startMinutes = result.TimeIn.Value.Minutes.AsString().PadRight(2, '0');
            var endHour = result.TimeOut.Value.Hours.AsString().PadRight(2, '0');
            var endMinutes = result.TimeOut.Value.Minutes.AsString().PadRight(2, '0');

            venueBooking.BillingAddress = result.BillingAddress;
            venueBooking.BookingStartDate = result.EventDate.GetValueOrDefault();
            venueBooking.BookingEndDate = result.EventEndDate.GetValueOrDefault();
            venueBooking.BookingNotes = result.EventNotes;
            venueBooking.SingleDay = result.SingleDayBooking.GetValueOrDefault();
            venueBooking.BookingNumberOfPeopleExpected = result.NoOfGuests.GetValueOrDefault();
            venueBooking.BookingTimeIn = startHour.AsString() + ":" + startMinutes.AsString();
            venueBooking.BookingTimeOut = endHour.AsString() + ":" + endMinutes.AsString();
            
            venueBooking.BusinessUnitId = result.BusinessUnitID.AsString();
            venueBooking.CompanyName = result.CompanyName;
            venueBooking.CostCentre = result.CostCentre;
            venueBooking.ContactNumber = result.ContactNumber;
            venueBooking.DataProjector = result.DataProjector.GetValueOrDefault();
            venueBooking.eMail = result.EmailAddress;
            venueBooking.EventName = result.EventName;
            venueBooking.Flowers = result.Flowers.GetValueOrDefault();
            
            venueBooking.LaserPointer = result.LaserPointer.GetValueOrDefault();
            venueBooking.MicroPhone = result.Microphone.GetValueOrDefault();
            venueBooking.MineralWater = result.StillBottleMineralWater.GetValueOrDefault();
            venueBooking.PenAndPaper = result.PenAndPaper.GetValueOrDefault();
            venueBooking.PersonRequestingVenue = result.ContactPersonName + " " + result.ContactPersonSurname;
            venueBooking.PersonToBeBilled = result.ContactPersonName + " " + result.ContactPersonSurname;
            venueBooking.RoomNumber = result.VenueNo;
            venueBooking.SpecialRequirements = result.SpecialRequirements;
            venueBooking.VATNumber = result.VATNo;
            venueBooking.VATRegistrationNumber = result.VATRegNo;

            venueBooking.Venue = new Venue()
            {
                Name = result.VenueName,
                Number = result.VenueNo
            };

            venueBooking.VenueBookingID = result.VenueBookingID;
            venueBooking.VenueId = result.VenueID.GetValueOrDefault();

            return venueBooking;
        }

        private void ResolveCateringRequirements(List<VenueBookingDomian.EntityFrameworkModel.sp_CateringInfoRead_Result> catering, VenueBookingForm model)
        {
            model.CateringRequired = false;
            model.CateringRequirements = new CateringRequirements();

            foreach (var item in catering.Where(c => c.CateringPlan == "A"))
            {
                model.CateringRequirements.CateringPackageA = true;
                model.CateringRequired = true;
                SetCateringPackageATimes(item, model);
            }

            foreach (var item in catering.Where(c => c.CateringPlan == "B"))
            {
                model.CateringRequirements.CateringPackageB = true;
                model.CateringRequired = true;
                SetCateringPackageBTimes(item, model);
            }

            foreach (var item in catering.Where(c => c.CateringPlan == "C"))
            {
                model.CateringRequirements.CateringPackageC = true;
                model.CateringRequired = true;
                SetCateringPackageCTimes(item, model);
            }

            foreach (var item in catering.Where(c => c.CateringPlan.IsNullOrEmpty()))
            {
                var propertyInfo = model.CateringRequirements.GetType().GetProperty(item.Item);

                if (propertyInfo.IsNotNull())
                {
                    model.CateringRequired = true;
                    propertyInfo.SetValue(model.CateringRequirements, Convert.ChangeType(true, propertyInfo.PropertyType), null);

                    var cateringInfoId = model.CateringRequirements.GetType().GetProperty(item.Item + "CateringInfoID");
                    cateringInfoId.SetValue(model.CateringRequirements, Convert.ChangeType(item.CateringInfoID, typeof(int), null));

                    var noOfPeople = model.CateringRequirements.GetType().GetProperty(item.Item + "Count");
                    noOfPeople.SetValue(model.CateringRequirements, Convert.ChangeType(item.NoOfPeople, typeof(int), null));

                    var hour = item.Times.Value.Hours.AsString().PadLeft(2, '0');
                    var min = item.Times.Value.Minutes.AsString().PadRight(2, '0');
                    var time = hour + ":" + min;

                    switch (item.TimeOption)
                    {
                        case "MorningTime":
                            {
                                var morningTimes = model.CateringRequirements.GetType().GetProperty(item.Item + "MorningTime");
                                if (morningTimes.IsNotNull())
                                {
                                    morningTimes.SetValue(model.CateringRequirements, time);
                                }
                                break;
                            }
                        case "MidMorningTime":
                            {
                                var midMorningTimes = model.CateringRequirements.GetType().GetProperty(item.Item + "MidMorningTime");
                                if (midMorningTimes.IsNotNull())
                                {
                                    midMorningTimes.SetValue(model.CateringRequirements, time);
                                }
                                break;
                            }
                        case "LunchTime":
                            {
                                var lunchTime = model.CateringRequirements.GetType().GetProperty(item.Item + "LunchTime");
                                if (lunchTime.IsNotNull())
                                {
                                    lunchTime.SetValue(model.CateringRequirements, time);
                                }
                                break;
                            }
                        case "AfternoonTime":
                            {
                                var afternoonTime = model.CateringRequirements.GetType().GetProperty(item.Item + "AfternoonTime");
                                if (afternoonTime.IsNotNull())
                                {
                                    afternoonTime.SetValue(model.CateringRequirements, time);
                                }

                                break;
                            }
                        case "EveningTime":
                            {
                                var eveningTime = model.CateringRequirements.GetType().GetProperty(item.Item + "EveningTime");
                                if (eveningTime.IsNotNull())
                                {
                                    eveningTime.SetValue(model.CateringRequirements, time);
                                }
                                break;
                            }

                        default:
                            break;

                    }
                }
            }
        }

        private void SetCateringPackageATimes(VenueBookingDomian.EntityFrameworkModel.sp_CateringInfoRead_Result item, VenueBookingForm model)
        {
            var cateringTime = new CateringPackageTimes();
            model.CateringRequirements.CateringPackageACateringInfoID = item.CateringInfoID;

            var timeOption = Enum.TryParse(item.TimeOption, out cateringTime);
            switch (cateringTime)
            {
                case CateringPackageTimes.ArrivalTeaTime:
                    {
                        model.CateringRequirements.CateringPackageATeaCoffeeArrivalTime = formatDate(item.Times);
                        break;
                    }
                case CateringPackageTimes.MidmorningTeaTime:
                    {
                        model.CateringRequirements.CateringPackageATeaCoffeeMidmorningTime = formatDate(item.Times);
                        break;
                    }
                case CateringPackageTimes.AfternoonTeaTime:
                    {
                        model.CateringRequirements.CateringPackageATeaCoffeeAfternoonTime = formatDate(item.Times);
                        break;
                    }
                case CateringPackageTimes.LunchTime:
                    {
                        model.CateringRequirements.CateringPackageALunchTime = formatDate(item.Times);
                        break;
                    }
                default:
                    break;
            }
        }

        private void SetCateringPackageBTimes(VenueBookingDomian.EntityFrameworkModel.sp_CateringInfoRead_Result item, VenueBookingForm model)
        {
            var cateringTime = new CateringPackageTimes();
            model.CateringRequirements.CateringPackageBCateringInfoID = item.CateringInfoID;

            var timeOption = Enum.TryParse(item.TimeOption, out cateringTime);
            switch (cateringTime)
            {
                case CateringPackageTimes.ArrivalTeaTime:
                    {
                        model.CateringRequirements.CateringPackageBTeaCoffeeArrivalTime = formatDate(item.Times);
                        break;
                    }
                case CateringPackageTimes.MidmorningTeaTime:
                    {
                        model.CateringRequirements.CateringPackageBTeaCoffeeMidmorningTime = formatDate(item.Times);
                        break;
                    }
                case CateringPackageTimes.AfternoonTeaTime:
                    {
                        model.CateringRequirements.CateringPackageBTeaCoffeeAfternoonTime = formatDate(item.Times);
                        break;
                    }
                case CateringPackageTimes.LunchTime:
                    {
                        model.CateringRequirements.CateringPackageBLunchTime = formatDate(item.Times);
                        break;
                    }
                default:
                    break;
            }
        }

        private void SetCateringPackageCTimes(VenueBookingDomian.EntityFrameworkModel.sp_CateringInfoRead_Result item, VenueBookingForm model)
        {
            var cateringTime = new CateringPackageTimes();
            model.CateringRequirements.CateringPackageCCateringInfoID = item.CateringInfoID;

            var timeOption = Enum.TryParse(item.TimeOption, out cateringTime);
            switch (cateringTime)
            {
                case CateringPackageTimes.ArrivalTeaTime:
                    {
                        model.CateringRequirements.CateringPackageCTeaCoffeeArrivalTime = formatDate(item.Times);
                        break;
                    }
                case CateringPackageTimes.MidmorningTeaTime:
                    {
                        model.CateringRequirements.CateringPackageCTeaCoffeeMidmorningTime = formatDate(item.Times);
                        break;
                    }
                case CateringPackageTimes.AfternoonTeaTime:
                    {
                        model.CateringRequirements.CateringPackageCTeaCoffeeAfternoonTime = formatDate(item.Times);
                        break;
                    }
                case CateringPackageTimes.LunchTime:
                    {
                        model.CateringRequirements.CateringPackageCLunchTime = formatDate(item.Times);
                        break;
                    }
                default:
                    break;
            }
        }

        private string formatDate(TimeSpan? itemTime)
        {
            var hour = itemTime.Value.Hours.AsString().PadLeft(2, '0');
            var min = itemTime.Value.Minutes.AsString().PadRight(2, '0');
            var time = hour + ":" + min;

            return time;
        }

        private List<VenueAvailabiliy> ResolveVenueAvailibility(List<VenueBookingDomian.EntityFrameworkModel.sp_VenueAvailabiliyRead_Result> result)
        {
            var venueAvailabiliy = new List<VenueAvailabiliy>();

            foreach (var item in result)
            {
                var start = new DateTime(item.EventDate.Value.Year,
                    item.EventDate.Value.Month, 
                    item.EventDate.Value.Day, item.TimeIn.Value.Hours, item.TimeIn.Value.Minutes,0).AddHours(2);

                var end = new DateTime(item.EventDate.Value.Year,
                    item.EventDate.Value.Month,
                    item.EventDate.Value.Day, item.TimeOut.Value.Hours, item.TimeOut.Value.Minutes, 0).AddHours(2);

                venueAvailabiliy.Add(new VenueAvailabiliy()
                {
                    start = start,
                    end = end,
                    title = "Venue booked"
                });
            }

            return venueAvailabiliy;
        }

        #region Save New Booking

        private void PopulateTVP(VenueBookingForm model, int venueBookingId, VenueBookingDomian.EntityFrameworkModel.BookingsEntities entity)
        {
            var cateringInfo = LoadCateringInfo(model.VenueBookingID);
            var dataTable = new DataTable();

            dataTable.Columns.Add("VenueBookingID");
            dataTable.Columns.Add("Item");
            dataTable.Columns.Add("NoOfPeople");
            dataTable.Columns.Add("Times", typeof(TimeSpan));
            dataTable.Columns.Add("TimeOption");
            dataTable.Columns.Add("CateringPlan");
            dataTable.Columns.Add("CateringInfoID");
            dataTable.Columns.Add("isActive");

            var packageACateringInfo = cateringInfo.Where(c => c.CateringPlan == "A").ToList();
            var packageBCateringInfo = cateringInfo.Where(c => c.CateringPlan == "B").ToList();
            var packageCCateringInfo = cateringInfo.Where(c => c.CateringPlan == "C").ToList();

            dataTable = ResolveCateringPackageA(dataTable, model, venueBookingId, packageACateringInfo);
            dataTable = ResolveCateringPackageB(dataTable, model, venueBookingId, packageBCateringInfo);
            dataTable = ResolveCateringPackageC(dataTable, model, venueBookingId, packageCCateringInfo);

            var props = model.CateringRequirements.GetType().GetProperties().Where(
                       prop => Attribute.IsDefined(prop, typeof(RequirementAttribute)));

            foreach (var item in props)
            {
                var newRow = dataTable.NewRow();
                var selected = model.CateringRequirements.GetType().GetProperty(item.Name).GetValue(model.CateringRequirements).AsBool().GetValueOrDefault();

                object[] attribute = item.GetCustomAttributes(typeof(RequirementAttribute), true);
                var displayName = (attribute[0] as RequirementAttribute).DisplayName;
                var hasTimes = (attribute[0] as RequirementAttribute).HasTimes;
                var count = 0;
                if (model.CateringRequirements.GetType().GetProperty(item.Name + "Count").GetValue(model.CateringRequirements).IsNotNull())
                {
                    count = (int)model.CateringRequirements.GetType().GetProperty(item.Name + "Count").GetValue(model.CateringRequirements);
                }

                if (item.Name == "Corkage")
                {
                    var Id = CateringInfoId(cateringInfo, "Corkage", null);

                    dataTable.Rows.Add(AddNewRow(newRow, venueBookingId, displayName, count, null, null, null, Id, selected));
                }

                if (hasTimes)
                {
                    var morningTime = model.CateringRequirements.GetType().GetProperty(item.Name + CateringTimes.MorningTime).GetValue(model.CateringRequirements).AsString();
                    if (morningTime.IsNotNullOrEmpty())
                    {
                        newRow = dataTable.NewRow();
                        var Id = CateringInfoId(cateringInfo, item.Name, CateringTimes.MorningTime.AsString());
                        dataTable.Rows.Add(AddNewRow(newRow, venueBookingId, displayName, count, morningTime, CateringTimes.MorningTime.AsString(), null, Id, selected));
                    }

                    var midMorningTime = model.CateringRequirements.GetType().GetProperty(item.Name + CateringTimes.MidMorningTime.AsString()).GetValue(model.CateringRequirements).AsString();
                    if (midMorningTime.IsNotNullOrEmpty())
                    {
                        newRow = dataTable.NewRow();
                        var Id = CateringInfoId(cateringInfo, item.Name, CateringTimes.MidMorningTime.AsString());
                        dataTable.Rows.Add(AddNewRow(newRow, venueBookingId, displayName, count, midMorningTime, CateringTimes.MidMorningTime.AsString(), null, Id, selected));
                    }

                    var lunchTime = model.CateringRequirements.GetType().GetProperty(item.Name + CateringTimes.LunchTime.AsString()).GetValue(model.CateringRequirements).AsString();
                    if (lunchTime.IsNotNullOrEmpty())
                    {
                        newRow = dataTable.NewRow();
                        var Id = CateringInfoId(cateringInfo, item.Name, CateringTimes.LunchTime.AsString());
                        dataTable.Rows.Add(AddNewRow(newRow, venueBookingId, displayName, count, lunchTime, CateringTimes.LunchTime.AsString(), null, Id, selected));
                    }

                    var AfternoonTime = model.CateringRequirements.GetType().GetProperty(item.Name + CateringTimes.AfternoonTime.AsString()).GetValue(model.CateringRequirements).AsString();
                    if (AfternoonTime.IsNotNullOrEmpty())
                    {
                        newRow = dataTable.NewRow();
                        var Id = CateringInfoId(cateringInfo, item.Name, CateringTimes.AfternoonTime.AsString());
                        dataTable.Rows.Add(AddNewRow(newRow, venueBookingId, displayName, count, AfternoonTime, CateringTimes.AfternoonTime.AsString(), null, Id, selected));
                    }

                    var eveningTime = model.CateringRequirements.GetType().GetProperty(item.Name + CateringTimes.EveningTime.AsString()).GetValue(model.CateringRequirements).AsString();
                    if (eveningTime.IsNotNullOrEmpty())
                    {
                        newRow = dataTable.NewRow();
                        var Id = CateringInfoId(cateringInfo, item.Name, CateringTimes.EveningTime.AsString());
                        dataTable.Rows.Add(AddNewRow(newRow, venueBookingId, displayName, count, eveningTime, CateringTimes.EveningTime.AsString(), null, Id, selected));
                    }
                }
            }
            CreateCateringInfo(dataTable, entity);
            UpdateCateringInfo(dataTable, entity);
            DeleteCateringInfo(dataTable, entity);
        }        

        private int? CateringInfoId(List<VenueBookingDomian.EntityFrameworkModel.sp_CateringInfoRead_Result> cateringInfo, string itemName, string timeOption)
        {
            int? id = null;
            var cateringItem = cateringInfo.Where(c => c.Item == itemName && c.TimeOption == timeOption).FirstOrDefault();

            if (cateringItem.IsNotNull())
            {
                id = cateringItem.CateringInfoID;
            }

            return id;
        }

        private DataRow AddNewRow(DataRow newRow, int venueBookingId, string displayName, int count, string time, string timeOption, string cateringPlan, int? cateringInfoId, bool isActive)
        {
            newRow["VenueBookingID"] = venueBookingId;
            newRow["Item"] = displayName;
            newRow["NoOfPeople"] = count;

            var timeRequired = new TimeSpan();
            TimeSpan.TryParse(time, out timeRequired);
            if (timeRequired == null)
            {
                newRow["Times"] = DBNull.Value;
            }
            else
            {
                newRow["Times"] = timeRequired;
            }

            newRow["TimeOption"] = timeOption;
            newRow["CateringPlan"] = cateringPlan;
            newRow["CateringInfoID"] = cateringInfoId;
            newRow["isActive"] = isActive;

            return newRow;
        }

        private void CreateCateringInfo(DataTable dataTable, VenueBookingDomian.EntityFrameworkModel.BookingsEntities entity)
        {
            var tvpTable = new SqlParameter("tvpCateringInfoTable", SqlDbType.Structured);
            tvpTable.Value = dataTable;
            tvpTable.TypeName = "CateringInfoTable";

            var result = entity.Database.ExecuteSqlCommand("EXEC sp_CateringInfoCreate @tvpCateringInfoTable", tvpTable);
        }

        private void UpdateCateringInfo(DataTable dataTable, VenueBookingDomian.EntityFrameworkModel.BookingsEntities entity)
        {
            var tvpTable = new SqlParameter("tvpCateringInfoTable", SqlDbType.Structured);
            tvpTable.Value = dataTable;
            tvpTable.TypeName = "CateringInfoTable";

            var result = entity.Database.ExecuteSqlCommand("EXEC sp_CateringInfoUpdate @tvpCateringInfoTable", tvpTable);
        }

        private void DeleteCateringInfo(DataTable dataTable, VenueBookingDomian.EntityFrameworkModel.BookingsEntities entity)
        {
            var tvpTable = new SqlParameter("tvpCateringInfoTable", SqlDbType.Structured);
            tvpTable.Value = dataTable;
            tvpTable.TypeName = "CateringInfoTable";

            var result = entity.Database.ExecuteSqlCommand("EXEC sp_CateringInfoDelete @tvpCateringInfoTable", tvpTable);
        }

        private List<SelectListItem> ResolveBusinessUnits(List<VenueBookingDomian.EntityFrameworkModel.sp_BusinessUnitRead_Result> list)
        {
            var businessUnits = new List<SelectListItem>();
            businessUnits.Add(new SelectListItem() { Value = "", Text = "" });
            foreach (var item in list)
            {
                businessUnits.Add(new SelectListItem()
                {
                    Value = item.BusinessUnitID.AsString(),
                    Text = item.BusinessUnitName
                });
            }

            return businessUnits;
        }

        private VenueBookingForm ResolveVenue(VenueBookingDomian.EntityFrameworkModel.sp_VenueRead_Result venue, VenueBookingForm venueBooking)
        {
            venueBooking.RoomNumber = venue.VenueNo;

            return venueBooking;
        }

        private DataTable ResolveCateringPackageA(DataTable dataTable, VenueBookingForm model, int venueBookingId, List<VenueBookingDomian.EntityFrameworkModel.sp_CateringInfoRead_Result> packageACateringInfo)
        {
            var peopleCount = model.BookingNumberOfPeopleExpected;
            if (model.CateringRequirements.CateringPackageA.IsNotNull())
            {
                var newRow = dataTable.NewRow();
                var arrivalTeaTime = model.CateringRequirements.CateringPackageATeaCoffeeArrivalTime;
                var midmorningTeaTime = model.CateringRequirements.CateringPackageATeaCoffeeMidmorningTime;
                var afternoonTeaTime = model.CateringRequirements.CateringPackageATeaCoffeeAfternoonTime;
                var lunchTime = model.CateringRequirements.CateringPackageALunchTime;

                if (arrivalTeaTime.IsNotNullOrEmpty())
                {
                    var cateringInfoId = ResolveCateringInfoId(packageACateringInfo, CateringPackageTimes.ArrivalTeaTime.AsString());

                    newRow = dataTable.NewRow();
                    dataTable.Rows.Add(AddNewRow(newRow, venueBookingId, "Tea/Coffee", peopleCount, arrivalTeaTime, CateringPackageTimes.ArrivalTeaTime.AsString(), "A",
                        cateringInfoId, model.CateringRequirements.CateringPackageA));
                }
                if (midmorningTeaTime.IsNotNullOrEmpty())
                {
                    var cateringInfoId = ResolveCateringInfoId(packageACateringInfo, CateringPackageTimes.MidmorningTeaTime.AsString());

                    newRow = dataTable.NewRow();
                    dataTable.Rows.Add(AddNewRow(newRow, venueBookingId, "Tea/Coffee", peopleCount, midmorningTeaTime, CateringPackageTimes.MidmorningTeaTime.AsString(), "A",
                        cateringInfoId, model.CateringRequirements.CateringPackageA));
                }
                if (afternoonTeaTime.IsNotNullOrEmpty())
                {
                    var cateringInfoId = ResolveCateringInfoId(packageACateringInfo, CateringPackageTimes.AfternoonTeaTime.AsString());
                    
                    newRow = dataTable.NewRow();
                    dataTable.Rows.Add(AddNewRow(newRow, venueBookingId, "Tea/Coffee", peopleCount, afternoonTeaTime, CateringPackageTimes.AfternoonTeaTime.AsString(), "A",
                        cateringInfoId, model.CateringRequirements.CateringPackageA));
                }
                if (lunchTime.IsNotNullOrEmpty())
                {
                    var cateringInfoId = ResolveCateringInfoId(packageACateringInfo, CateringPackageTimes.LunchTime.AsString());
                    
                    newRow = dataTable.NewRow();
                    dataTable.Rows.Add(AddNewRow(newRow, venueBookingId, "Lunch", peopleCount, lunchTime, CateringPackageTimes.LunchTime.AsString(), "A",
                        cateringInfoId, model.CateringRequirements.CateringPackageA));
                }
            }

            return dataTable;
        }

        private DataTable ResolveCateringPackageB(DataTable dataTable, VenueBookingForm model, int venueBookingId, List<VenueBookingDomian.EntityFrameworkModel.sp_CateringInfoRead_Result> packageBCateringInfo)
        {
            var peopleCount = model.BookingNumberOfPeopleExpected;
            if (model.CateringRequirements.CateringPackageB.IsNotNull())
            {
                var newRow = dataTable.NewRow();
                var arrivalTeaTime = model.CateringRequirements.CateringPackageBTeaCoffeeArrivalTime;
                var midmorningTeaTime = model.CateringRequirements.CateringPackageBTeaCoffeeMidmorningTime;
                var afternoonTeaTime = model.CateringRequirements.CateringPackageBTeaCoffeeAfternoonTime;
                var lunchTime = model.CateringRequirements.CateringPackageBLunchTime;

                if (arrivalTeaTime.IsNotNullOrEmpty())
                {
                    var cateringInfoId = ResolveCateringInfoId(packageBCateringInfo, CateringPackageTimes.ArrivalTeaTime.AsString());

                    newRow = dataTable.NewRow();
                    dataTable.Rows.Add(AddNewRow(newRow, venueBookingId, "Tea/Coffee", peopleCount, arrivalTeaTime, CateringPackageTimes.ArrivalTeaTime.AsString(), "B",
                        cateringInfoId, model.CateringRequirements.CateringPackageB));
                }
                if (midmorningTeaTime.IsNotNullOrEmpty())
                {
                    var cateringInfoId = ResolveCateringInfoId(packageBCateringInfo, CateringPackageTimes.MidmorningTeaTime.AsString());

                    newRow = dataTable.NewRow();
                    dataTable.Rows.Add(AddNewRow(newRow, venueBookingId, "Tea/Coffee", peopleCount, midmorningTeaTime, CateringPackageTimes.MidmorningTeaTime.AsString(), "B",
                        cateringInfoId, model.CateringRequirements.CateringPackageB));
                }
                if (afternoonTeaTime.IsNotNullOrEmpty())
                {
                    var cateringInfoId = ResolveCateringInfoId(packageBCateringInfo, CateringPackageTimes.AfternoonTeaTime.AsString());
                    
                    newRow = dataTable.NewRow();
                    dataTable.Rows.Add(AddNewRow(newRow, venueBookingId, "Tea/Coffee", peopleCount, afternoonTeaTime, CateringPackageTimes.AfternoonTeaTime.AsString(), "B",
                        cateringInfoId, model.CateringRequirements.CateringPackageB));
                }
                if (lunchTime.IsNotNullOrEmpty())
                {
                    var cateringInfoId = ResolveCateringInfoId(packageBCateringInfo, CateringPackageTimes.LunchTime.AsString());
                    
                    newRow = dataTable.NewRow();
                    dataTable.Rows.Add(AddNewRow(newRow, venueBookingId, "Lunch", peopleCount, lunchTime, CateringPackageTimes.LunchTime.AsString(), "B",
                        cateringInfoId, model.CateringRequirements.CateringPackageB));
                }
            }
            return dataTable;
        }

        private DataTable ResolveCateringPackageC(DataTable dataTable, VenueBookingForm model, int venueBookingId, List<VenueBookingDomian.EntityFrameworkModel.sp_CateringInfoRead_Result> packageCCateringInfo)
        {
            var peopleCount = model.BookingNumberOfPeopleExpected;
            if (model.CateringRequirements.CateringPackageC.IsNotNull())
            {
                var newRow = dataTable.NewRow();
                var arrivalTeaTime = model.CateringRequirements.CateringPackageCTeaCoffeeArrivalTime;
                var midmorningTeaTime = model.CateringRequirements.CateringPackageCTeaCoffeeMidmorningTime;
                var afternoonTeaTime = model.CateringRequirements.CateringPackageCTeaCoffeeAfternoonTime;
                var lunchTime = model.CateringRequirements.CateringPackageCLunchTime;

                if (arrivalTeaTime.IsNotNullOrEmpty())
                {
                    var cateringInfoId = ResolveCateringInfoId(packageCCateringInfo, CateringPackageTimes.ArrivalTeaTime.AsString());

                    newRow = dataTable.NewRow();
                    dataTable.Rows.Add(AddNewRow(newRow, venueBookingId, "Tea/Coffee", peopleCount, arrivalTeaTime, CateringPackageTimes.ArrivalTeaTime.AsString(), "C",
                        cateringInfoId, model.CateringRequirements.CateringPackageC));
                }
                if (midmorningTeaTime.IsNotNullOrEmpty())
                {
                    var cateringInfoId = ResolveCateringInfoId(packageCCateringInfo, CateringPackageTimes.MidmorningTeaTime.AsString());

                    newRow = dataTable.NewRow();
                    dataTable.Rows.Add(AddNewRow(newRow, venueBookingId, "Tea/Coffee", peopleCount, midmorningTeaTime, CateringPackageTimes.MidmorningTeaTime.AsString(), "C",
                        cateringInfoId, model.CateringRequirements.CateringPackageC));
                }
                if (afternoonTeaTime.IsNotNullOrEmpty())
                {
                    var cateringInfoId = ResolveCateringInfoId(packageCCateringInfo, CateringPackageTimes.AfternoonTeaTime.AsString());

                    newRow = dataTable.NewRow();
                    dataTable.Rows.Add(AddNewRow(newRow, venueBookingId, "Tea/Coffee", peopleCount, afternoonTeaTime, CateringPackageTimes.AfternoonTeaTime.AsString(), "C",
                        cateringInfoId, model.CateringRequirements.CateringPackageC));
                }
                if (lunchTime.IsNotNullOrEmpty())
                {
                    var cateringInfoId = ResolveCateringInfoId(packageCCateringInfo, CateringPackageTimes.LunchTime.AsString());

                    newRow = dataTable.NewRow();
                    dataTable.Rows.Add(AddNewRow(newRow, venueBookingId, "Lunch", peopleCount, lunchTime, CateringPackageTimes.LunchTime.AsString(), "C",
                        cateringInfoId, model.CateringRequirements.CateringPackageC));
                }
            }

            return dataTable;
        }




        private int? ResolveCateringInfoId(List<VenueBookingDomian.EntityFrameworkModel.sp_CateringInfoRead_Result> packageACateringInfo, string time)
        {
            int? cateringInfoId = null;
            var cateringInfo = packageACateringInfo.Where(value => value.TimeOption == time).FirstOrDefault();

            if (cateringInfo.IsNull())
            {
                cateringInfoId = null;
            }
            else
            {
                cateringInfoId = cateringInfo.CateringInfoID;
            }

            return cateringInfoId;
        }

        #endregion

        #endregion
    }
}
