﻿using System;
using System.Linq;
using System.Net.Mail;
using System.Text;
using VenueBooking.Models;
using SettingManager = VenueBookingDomian.Properties.Settings;

namespace VenueBooking
{
    public class Email
    {
        public MailMessage Mail = new MailMessage();
        public SmtpClient SmtpServer = new SmtpClient();

        public void SendVenueBookingMail(VenueBookingForm model)
        {
            var smptClient = SettingManager.Default.SMTPClient;
            var portNumber = SettingManager.Default.MailPortNumber;

            SmtpServer = new SmtpClient(smptClient)
            {
                Port = portNumber,
                UseDefaultCredentials = false
            };

            SendMailToClient(model);

            SendMailToCatering(model);
        }

        private void SendMailToClient(VenueBookingForm model)
        {
            try
            {
                Mail.To.Add(model.eMail);
                Mail.From = new MailAddress(SettingManager.Default.MailFrom);
                Mail.Subject = SettingManager.Default.ClientMailSubject;
                Mail.IsBodyHtml = true;
                Mail.Body = BodyHeading() + Environment.NewLine + VenueBookingBody(model);

                SmtpServer.Send(Mail);
            }
            catch (Exception ex)
            {
                var exceptionMessage = ex.Message;
                //do nothing
            }
        }

        private static string BodyHeading()
        {
            var heading = new StringBuilder();

            heading.Append("Thank you for choosing to host your event with us.");
            heading.AppendLine("<br/>");
            heading.Append(
                "Your booking will be confirmed, subject to availability, within 48 hours and sent to the e- mail address or mobile number provided.");
            heading.AppendLine("<br/>");
            heading.Append("We look forward to the pleasure of having you as our guest.");
            heading.AppendLine("<br/>");
            heading.AppendLine("<br/>");
            heading.Append("Sincerely");
            heading.AppendLine("<br/>");
            heading.AppendLine("<br/>");
            heading.Append("University of Stellenbosch");
            heading.AppendLine("<br/>");
            heading.AppendLine("<br/>");
            heading.Append("Bellville Park Campus");
            heading.AppendLine("<br/>");
            heading.AppendLine("<br/>");
            heading.Append("E-mail: venues@belpark.sun.ac.za");
            heading.AppendLine("<br/>");
            heading.AppendLine("<br/>");
            heading.AppendLine("<br/>");

            return heading.ToString();
        }

        private string VenueBookingBody(VenueBookingForm model)
        {
            var result = model.SingleDay
                ? "Single Day Event"
                : $"Multiple Day Event ({(model.BookingEndDate.GetValueOrDefault() - model.BookingStartDate.GetValueOrDefault()).TotalDays} days)";

            var mailBody = new StringBuilder();

            mailBody.Append("<h2>Venue Booking</h2>");
            mailBody.AppendLine("<br/>");

            // Event Information
            mailBody.Append("<b>Event Information - " + result + "</b>");
            mailBody.AppendLine("<br/>");
            mailBody.AppendLine("<br/>");
            mailBody.Append("Event Venue: " + (model.VenueName.IsNotNullOrEmpty() ? model.VenueName : "N/A"));
            mailBody.AppendLine("<br/>");
            mailBody.Append("Event Name: " + (model.EventName.IsNotNullOrEmpty() ? model.EventName : "N/A"));
            mailBody.AppendLine("<br/>");
            mailBody.Append("Number of people expected: " + model.BookingNumberOfPeopleExpected);
            mailBody.AppendLine("<br/>");

            if (model.SingleDay)
            {
                mailBody.Append("Booking Date: " + (model.BookingStartDate?.ToString("yyyy-MM-dd") ?? "N/A"));
            }
            else
            {
                mailBody.Append("Booking Dates: " +
                                (model.BookingStartDate.HasValue && model.BookingEndDate.HasValue
                                    ? $"{model.BookingStartDate.GetValueOrDefault():yyyy-MM-dd} - {model.BookingEndDate.GetValueOrDefault():yyyy-MM-dd}"
                                    : "N/A"));
            }
            
            mailBody.AppendLine("<br/>");
            mailBody.Append("Time In: " + (model.BookingTimeIn.IsNotNullOrEmpty() ? model.BookingTimeIn : "N/A"));
            mailBody.AppendLine("<br/>");
            mailBody.Append("Time Out: " + (model.BookingTimeOut.IsNotNullOrEmpty() ? model.BookingTimeOut : "N/A"));
            mailBody.AppendLine("<br/>");
            mailBody.AppendLine("<br/>");

            // Billing Information
            mailBody.Append("<b>Billing Information</b>");
            mailBody.AppendLine("<br/>");
            mailBody.AppendLine("<br/>");
            mailBody.Append("Person Requesting Venue: " + (model.PersonRequestingVenue.IsNotNullOrEmpty() ? model.PersonRequestingVenue : "N/A"));
            mailBody.AppendLine("<br/>");
            mailBody.Append("Contact Number: " + (model.ContactNumber.IsNotNullOrEmpty() ? model.ContactNumber : "N/A"));
            mailBody.AppendLine("<br/>");
            mailBody.Append("E-Mail: " + (model.eMail.IsNotNullOrEmpty() ? model.eMail : "N/A"));
            mailBody.AppendLine("<br/>");
            mailBody.Append("Company Name: " + (model.CompanyName.IsNotNullOrEmpty() ? model.CompanyName : "N/A"));
            mailBody.AppendLine("<br/>");
            mailBody.Append("Billing Address: " + (model.BillingAddress.IsNotNullOrEmpty() ? model.BillingAddress : "N/A"));
            mailBody.AppendLine("<br/>");
            mailBody.Append("VAT Number: " + (model.VATNumber.IsNotNullOrEmpty() ? model.VATNumber : "N/A"));
            mailBody.AppendLine("<br/>");
            mailBody.Append("VAT Registration Number: " + (model.VATRegistrationNumber.IsNotNullOrEmpty() ? model.VATRegistrationNumber : "N/A"));
            mailBody.AppendLine("<br/>");
            mailBody.Append("Person To Be Billed: " + (model.PersonToBeBilled.IsNotNullOrEmpty() ? model.PersonToBeBilled : "N/A"));
            mailBody.AppendLine("<br/>");
            mailBody.AppendLine("<br/>");

            // Venue Requirements
            mailBody.Append("<b>Venue Requirements</b>");
            mailBody.AppendLine("<br/>");
            mailBody.AppendLine("<br/>");
            mailBody.Append("Data projector / Proxima when hiring breakaway: " + (model.DataProjector ? "Yes" : "No"));
            mailBody.AppendLine("<br/>");
            mailBody.Append("Microphone: " + (model.MicroPhone ? "Yes" : "No"));
            mailBody.AppendLine("<br/>");
            mailBody.Append("Laser pointer: " + (model.LaserPointer ? "Yes" : "No"));
            mailBody.AppendLine("<br/>");
            mailBody.Append("Pen & paper: " + (model.PenAndPaper ? "Yes" : "No"));
            mailBody.AppendLine("<br/>");
            mailBody.Append("Still bottled mineral water: " + (model.MineralWater ? "Yes" : "No"));
            mailBody.AppendLine("<br/>");
            mailBody.Append("Flowers (minimum of R200): " + (model.Flowers ? "Yes" : "No"));
            mailBody.AppendLine("<br/>");
            mailBody.Append("Special requirements: " +
                            (model.SpecialRequirements.IsNotNullOrEmpty() ? model.SpecialRequirements : "None"));
            mailBody.AppendLine("<br/>");
            mailBody.Append("Catering required: " + (model.CateringRequired ? "Yes" : "No"));
            mailBody.AppendLine("<br/>");

            // Not used anymore
            // Catering Requirements
            //if (model.CateringRequirements.CateringPackageA || model.CateringRequirements.CateringPackageB ||
            //    model.CateringRequirements.CateringPackageC)
            //{
            //    mailBody.AppendLine("<br/>");
            //    mailBody.Append("<b>Catering Requirements</b>");
            //    mailBody.AppendLine("<br/>");
            //    mailBody.AppendLine("<br/>");
            //}

            // Catering Package A
            //if (model.CateringRequirements.CateringPackageA)
            //{
            //    mailBody.Append(" Package A");
            //    mailBody.AppendLine("<br/>");
            //    mailBody.Append("Tea & Coffee on arrival.");
            //    mailBody.AppendLine("<br/>");
            //    mailBody.Append("Mid Morning Tea & Coffee served with a biscuit.");
            //    mailBody.AppendLine("<br/>");
            //    mailBody.Append("Meal of the day with a glass of juice OR cans.");
            //    mailBody.AppendLine("<br/>");
            //    mailBody.Append("Tea & Coffee.");
            //    mailBody.AppendLine("<br/>");
            //    mailBody.Append("Arrival Time" +
            //                    (model.CateringRequirements.CateringPackageATeaCoffeeArrivalTime.IsNotNullOrEmpty()
            //                        ? model.CateringRequirements.CateringPackageATeaCoffeeArrivalTime
            //                        : "N/A"));
            //    mailBody.AppendLine("<br/>");
            //    mailBody.Append("Midmorning Time" +
            //                    (model.CateringRequirements.CateringPackageATeaCoffeeMidmorningTime.IsNotNullOrEmpty()
            //                        ? model.CateringRequirements.CateringPackageATeaCoffeeMidmorningTime
            //                        : "N/A"));
            //    mailBody.AppendLine("<br/>");
            //    mailBody.Append("Afternoon Time" +
            //                    (model.CateringRequirements.CateringPackageATeaCoffeeAfternoonTime.IsNotNullOrEmpty()
            //                        ? model.CateringRequirements.CateringPackageATeaCoffeeAfternoonTime
            //                        : "N/A"));
            //    mailBody.AppendLine("<br/>");
            //    mailBody.Append("Lunch Time" +
            //                    (model.CateringRequirements.CateringPackageALunchTime.IsNotNullOrEmpty()
            //                        ? model.CateringRequirements.CateringPackageALunchTime
            //                        : "N/A"));
            //    mailBody.AppendLine("<br/>");
            //}

            // Catering Package B
            //if (model.CateringRequirements.CateringPackageB)
            //{
            //    mailBody.Append(" Package B");
            //    mailBody.AppendLine("<br/>");
            //    mailBody.Append("Tea & Coffee on arrival.");
            //    mailBody.AppendLine("<br/>");
            //    mailBody.Append("Mid Morning Tea & Coffee served with Tartlets.");
            //    mailBody.AppendLine("<br/>");
            //    mailBody.Append("Buffet lunch with a glass of juice OR cans.");
            //    mailBody.AppendLine("<br/>");
            //    mailBody.Append("Afternoon Tea & Coffee with biscuits.");
            //    mailBody.AppendLine("<br/>");
            //    mailBody.Append("Arrival Time" +
            //                    (model.CateringRequirements.CateringPackageBTeaCoffeeArrivalTime.IsNotNullOrEmpty()
            //                        ? model.CateringRequirements.CateringPackageBTeaCoffeeArrivalTime
            //                        : "N/A"));
            //    mailBody.AppendLine("<br/>");
            //    mailBody.Append("Midmorning Time" +
            //                    (model.CateringRequirements.CateringPackageBTeaCoffeeMidmorningTime.IsNotNullOrEmpty()
            //                        ? model.CateringRequirements.CateringPackageBTeaCoffeeMidmorningTime
            //                        : "N/A"));
            //    mailBody.AppendLine("<br/>");
            //    mailBody.Append("Afternoon Time" +
            //                    (model.CateringRequirements.CateringPackageBTeaCoffeeAfternoonTime.IsNotNullOrEmpty()
            //                        ? model.CateringRequirements.CateringPackageBTeaCoffeeAfternoonTime
            //                        : "N/A"));
            //    mailBody.AppendLine("<br/>");
            //    mailBody.Append("Lunch Time" +
            //                    (model.CateringRequirements.CateringPackageBLunchTime.IsNotNullOrEmpty()
            //                        ? model.CateringRequirements.CateringPackageBLunchTime
            //                        : "N/A"));
            //    mailBody.AppendLine("<br/>");
            //}

            // Catering Package C
            //if (model.CateringRequirements.CateringPackageC)
            //{
            //    mailBody.Append(" Package C");
            //    mailBody.AppendLine("<br/>");
            //    mailBody.Append("Tea & Coffee on arrival.");
            //    mailBody.AppendLine("<br/>");
            //    mailBody.Append("Mid Morning Tea & Coffee served with a tea snack.");
            //    mailBody.AppendLine("<br/>");
            //    mailBody.Append("Buffet lunch with a glass of juice OR cans.");
            //    mailBody.AppendLine("<br/>");
            //    mailBody.Append("Tea & Cofee with biscuits.");
            //    mailBody.AppendLine("<br/>");
            //    mailBody.Append("Arrival Time " +
            //                    (model.CateringRequirements.CateringPackageCTeaCoffeeArrivalTime.IsNotNullOrEmpty()
            //                        ? model.CateringRequirements.CateringPackageCTeaCoffeeArrivalTime
            //                        : "N/A"));
            //    mailBody.AppendLine("<br/>");
            //    mailBody.Append("Midmorning Time " +
            //                    (model.CateringRequirements.CateringPackageCTeaCoffeeMidmorningTime.IsNotNullOrEmpty()
            //                        ? model.CateringRequirements.CateringPackageCTeaCoffeeMidmorningTime
            //                        : "N/A"));
            //    mailBody.AppendLine("<br/>");
            //    mailBody.Append("Afternoon Time " +
            //                    (model.CateringRequirements.CateringPackageCTeaCoffeeAfternoonTime.IsNotNullOrEmpty()
            //                        ? model.CateringRequirements.CateringPackageCTeaCoffeeAfternoonTime
            //                        : "N/A"));
            //    mailBody.AppendLine("<br/>");
            //    mailBody.Append("Lunch Time " +
            //                    (model.CateringRequirements.CateringPackageCLunchTime.IsNotNullOrEmpty()
            //                        ? model.CateringRequirements.CateringPackageCLunchTime
            //                        : "N/A"));
            //    mailBody.AppendLine("<br/>");
            //}

            //var props = model.CateringRequirements.GetType().GetProperties().Where(
            //    prop => Attribute.IsDefined(prop, typeof(RequirementAttribute)));

            //foreach (var item in props)
            //{
            //    var selected =
            //        model.CateringRequirements.GetType()
            //            .GetProperty(item.Name)
            //            .GetValue(model.CateringRequirements)
            //            .AsBool()
            //            .GetValueOrDefault();

            //    if (selected)
            //    {
            //        object[] attribute = item.GetCustomAttributes(typeof(RequirementAttribute), true);
            //        var displayName = (attribute[0] as RequirementAttribute).DisplayName;
            //        var hasTimes = (attribute[0] as RequirementAttribute).HasTimes;
            //        var count = 0;
            //        if (
            //            model.CateringRequirements.GetType()
            //                .GetProperty(item.Name + "Count")
            //                .GetValue(model.CateringRequirements)
            //                .IsNotNull())
            //        {
            //            count =
            //                (int)
            //                model.CateringRequirements.GetType()
            //                    .GetProperty(item.Name + "Count")
            //                    .GetValue(model.CateringRequirements);
            //        }

            //        if (hasTimes)
            //        {
            //            var morningTime =
            //                model.CateringRequirements.GetType()
            //                    .GetProperty(item.Name + "MorningTime")
            //                    .GetValue(model.CateringRequirements)
            //                    .AsString();
            //            var midMorningTime =
            //                model.CateringRequirements.GetType()
            //                    .GetProperty(item.Name + "MidMorningTime")
            //                    .GetValue(model.CateringRequirements)
            //                    .AsString();
            //            var lunchTime =
            //                model.CateringRequirements.GetType()
            //                    .GetProperty(item.Name + "LunchTime")
            //                    .GetValue(model.CateringRequirements)
            //                    .AsString();
            //            var afternoonTime =
            //                model.CateringRequirements.GetType()
            //                    .GetProperty(item.Name + "AfternoonTime")
            //                    .GetValue(model.CateringRequirements)
            //                    .AsString();
            //            var eveningTime =
            //                model.CateringRequirements.GetType()
            //                    .GetProperty(item.Name + "EveningTime")
            //                    .GetValue(model.CateringRequirements)
            //                    .AsString();

            //            mailBody.Append(CateringRequirements(displayName.AddSpaceBeforeCapital(),
            //                count,
            //                morningTime,
            //                midMorningTime,
            //                lunchTime,
            //                afternoonTime,
            //                eveningTime));
            //        }
            //        else
            //        {
            //            mailBody.Append(CateringRequirements(displayName, count));
            //        }
            //    }
            //}

            return mailBody.ToString();
        }

        private void SendMailToCatering(VenueBookingForm model)
        {
            try
            {
                Mail = new MailMessage();

                Mail.To.Add(SettingManager.Default.CateringMailAddress);
                Mail.From = new MailAddress(SettingManager.Default.CateringMailAddress);
                Mail.Subject = SettingManager.Default.CateringMailSubject;
                Mail.Body = SettingManager.Default.CateringMailBody + VenueBookingBody(model);
                Mail.IsBodyHtml = true;

                SmtpServer.Send(Mail);
            }
            catch (Exception ex)
            {
                var exceptionMessage = ex.Message;
                //do nothing
            }
        }

        private string CateringPackageItems(string itemName, string itemTime)
        {
            return $"{itemName} :{itemTime}<br/>";
        }

        public string CateringRequirements(string requirement, int? count)
        {
            var requirements = new StringBuilder();

            requirements.Append("<b>" + requirement + "</b>");
            requirements.AppendLine();
            requirements.Append("Number: " + count);
            requirements.AppendLine();

            return requirement;
        }

        private string CateringRequirements(string requirement, int? count, string morningTime,
            string midMorningTime, string lunchTime, string afternoonTime, string eveningTime)
        {
            var requirements = new StringBuilder();

            var timeMorning = morningTime != string.Empty ? morningTime : "N/A";
            var timeMidMorning = midMorningTime != string.Empty ? midMorningTime : "N/A";
            var timeLunch = lunchTime != string.Empty ? lunchTime : "N/A";
            var timeAfterNoon = afternoonTime != string.Empty ? afternoonTime : "N/A";
            var timeEvening = eveningTime != string.Empty ? eveningTime : "N/A";

            requirements.Append("<b>" + requirement + "</b>");
            requirements.AppendLine("<br/>");
            requirements.Append("Number of people: " + count);
            requirements.AppendLine("<br/>");
            requirements.Append("Morning Time: " + timeMorning);
            requirements.AppendLine("<br/>");
            requirements.Append("Mid Morning Time: " + timeMidMorning);
            requirements.AppendLine("<br/>");
            requirements.Append("Lunch Time: " + timeLunch);
            requirements.AppendLine("<br/>");
            requirements.Append("After Noon Time: " + timeAfterNoon);
            requirements.AppendLine("<br/>");
            requirements.Append("Evening Time: " + timeEvening);
            requirements.AppendLine("<br/>");

            return requirements.ToString();
        }
    }
}