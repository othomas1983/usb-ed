﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VenueBooking.Models
{
    public class Venue
    {
        public string Number { get; set; }
        public string Site { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public string Seats { get; set; }
        public string Result { get; set; }
        public string ResultDetails { get; set; }
        public bool VenueFound { get; set; }
    }
}
