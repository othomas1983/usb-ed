﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VenueBooking.Models
{
    public class VenueAvailabiliy
    {
        public Nullable<System.DateTime> EventDate { get; set; }
        public Nullable<System.DateTime> start { get; set; }
        public Nullable<System.DateTime> end { get; set; }
        public string title { get; set; }
        public string VenueName { get; set; }
        public int VenueID { get; set; }
        public int VenueBookingID { get; set; }
        public string EventName { get; set; }
    }
}
