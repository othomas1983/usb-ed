﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using VenueBooking.Validation;

namespace VenueBooking.Models
{
    public class VenueBookingForm
    {
        #region Event Information
        
        public string RoomNumber { get; set; }

        public int VenueId { get; set; }
        public int VenueBookingID { get; set; }

        public Venue Venue { get; set; }
        public string VenueName { get; set; }
        public CateringRequirements CateringRequirements { get; set; }
        public bool CateringRequired { get; set; }


        [Display(Name = "Event Name")]
        [Required]
        public string EventName { get; set; }

        //[EventDurationValidation]
        public bool SingleDay { get; set; }
        
        [Required]
        [Display(Name = "Number of people expected")]
        public int BookingNumberOfPeopleExpected { get; set; }

        [Display(Name = "Booking Venue")]
        public string BookingVenue { get; set; }

        [Display(Name = "Event start date")]
        public DateTime? BookingStartDate { get; set; }

        [Display(Name = "Event end date")]
        public DateTime? BookingEndDate { get; set; }

        public string BookingNotes { get; set; }

        public string BookingTimeIn { get; set; }

        public string BookingTimeOut { get; set; }

        #endregion

        #region Client & Billing Information

        [Display(Name = "Person Requesting Venue")]
        [Required]
        public string PersonRequestingVenue { get; set; }

        [Display(Name = "Contact Number")]
        [Required]
        public string ContactNumber { get; set; }

        [Required]
        [ValidateEmailAddress]
        public string eMail { get; set; }

        public string CompanyName { get; set; }
        public string CostCentre { get; set; }
        public List<SelectListItem> BusinessUnits { get; set; }
        public List<SelectListItem> CateringTimes { get; set; }
        
        [Required]
        [Display(Name = "Business Unit")]
        public string BusinessUnitId { get; set; }
        [Display(Name = "Billing Address")]
        [Required]
        public string BillingAddress { get; set; }

        public string VATNumber { get; set; }

        public string VATRegistrationNumber { get; set; }

        [Display(Name = "Person to be billed")]
        [Required]
        public string PersonToBeBilled { get; set; }

        #endregion

        #region Venue Requirements

         public bool DataProjector { get; set; }

         public bool MicroPhone { get; set; }

         public bool LaserPointer { get; set; }

         public bool PenAndPaper { get; set; }

         public bool MineralWater { get; set; }

         public bool Flowers { get; set; }

         public string SpecialRequirements { get; set; }

        #endregion
    }
}
