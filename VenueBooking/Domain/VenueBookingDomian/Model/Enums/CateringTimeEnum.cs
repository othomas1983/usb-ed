﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VenueBooking.Models
{
    enum CateringPackageTimes
    {
        ArrivalTeaTime,
        MidmorningTeaTime,
        AfternoonTeaTime,
        LunchTime
    }

    enum CateringTimes
    {
        MorningTime,
        MidMorningTime,
        LunchTime,
        AfternoonTime,
        EveningTime
    }
}