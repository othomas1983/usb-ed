﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using VenueBooking.Models;

namespace VenueBooking.Validation
{   
    #region EventDurationValidation

    public sealed class CateringRequirementsCountValidation : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var context = validationContext.ObjectInstance as CateringRequirements;

            //var morningTime = model.CateringRequirements.GetType().GetProperty(item.Name + "MorningTime").GetValue(model.CateringRequirements).AsString();

            var isChecked = context.GetType().GetProperty(validationContext.DisplayName.Replace("Count",string.Empty)).GetValue(context).AsBool();

            if (isChecked.GetValueOrDefault())
            {
                if (value.IsNull() || (int)value == 0)
                {
                    return new ValidationResult("Please enter a valid value for {0}".FormatInvariantCulture(validationContext.DisplayName));
                }
            }
            return ValidationResult.Success;
        }
    }

    #endregion    

    #region EmailValidation

    public sealed class ValidateEmailAddress : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value.IsNotNull())
            {
                var emailExpression = @"^(\w+([-+.']\w+)*)@([\w-]+\.)+[\w-]+$".AsString();
                var valid = Regex.Match(value.ToString(), emailExpression).Success;

                if (!valid)
                {
                    return new ValidationResult(
                        FormatErrorMessage(value.ToString()));
                }
            }

            return ValidationResult.Success;
        }

        public override string FormatErrorMessage(string name)
        {
            return "{0} is an invalid email address".FormatInvariantCulture(name);
        }
    }


    #endregion
}
