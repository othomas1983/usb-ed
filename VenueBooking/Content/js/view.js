﻿function View() {
    var instance;

    this.load = function () {
        instance = this;

        this.proccesingDialogID = "serverProccesingDialog";

        //$("body").append("<div class=\"infoDialog noClose\" id=\"" + instance.proccesingDialogID + "\">" + instance.loader("Processing Request, Please Wait...", "big") + "</div>");
        instance.customConfirmLoad();
    }

    this.iconButton = function (icon, text, id, buttonClass, url) {
        var iconButtonClass = buttonClass || "";
        var iconButtonID = id || "";
        var iconButtonIcon = icon || "";
        var iconButtonUrl = url || "javascript:void(0)";
        var iconButtonText = "";
        if (text) {
            iconButtonText = "<span class=\"text\">" + text + "</span>";
        }
        return "<a href=\"" + iconButtonUrl + "\" class=\"iconButton " + iconButtonClass + "\" id=\"" + iconButtonID + "\">" + iconButtonText + "<span class=\"iconBg\"><span class=\"icon " + iconButtonIcon + "\"></span></span></a>";
    }

    this.confirm = function (settings) {
        var instance = this;
        var defaultSettings = {
            yes: function () { },
            no: function () { },
            close: function () { },
            message: "Are you sure you want to Continue",
            showContactDetails: false,
            type: "normal", //or warning
            title: "Continue?",
            heading: "Continue?",
            yesText: "Yes",
            noText: "No", // False To hide it,
            width: 480
        };

        this.config = $.fn.extend(defaultSettings, settings);

        if (instance.config.heading) {
            instance.config.message = "<h3>" + instance.config.heading + "</h3> " + instance.config.message;
        }
        instance.config.buttons = view.iconButton("tick-mark", instance.config.yesText, "custom_confirm_acceptButton", instance.config.yesText + " rightSpace15");

        if (instance.config.noText) {
            instance.config.buttons += view.iconButton("cross", instance.config.noText, "custom_confirm_declineButton", instance.config.noText);
        }

        $('#custom_confirm_dialog').dialog('option', 'open', function () {
            var dialog = $(this);

            var dialogMsg = instance.config.message;
            dialog.removeClass("normal warning");
            dialog.addClass(instance.config.type);
            $(".ui-widget-overlay").css("opacity", 0.8);
            $(".ui-dialog-titlebar-close", dialog.parent()).remove();
            $('.ui-dialog-titlebar').html(instance.config.title);
            //$('.ui-dialog-titlebar').after('<h3 style="position:absolute;top:-5px;left:15px;color:white;">' + instance.config.title + '</h3>');


            $("#custom_confirm_message", dialog).html(dialogMsg);
            $("#custom_confirm_dialog_buttonsContainer", dialog).html(instance.config.buttons);

            dialog.dialog("option", "position", "center");

            $(this).off("click", "#custom_confirm_acceptButton");
            $(this).on("click", "#custom_confirm_acceptButton", function () {
                if (instance.config.yes) {
                    instance.config.yes();
                }
                dialog.dialog("close");
            });
            $(this).off("click", "#custom_confirm_declineButton");
            $(this).on("click", "#custom_confirm_declineButton", function () {
                if (instance.config.no) {
                    instance.config.no();
                }
                dialog.dialog("close");
            });
        });
        $('#custom_confirm_dialog').dialog('option', 'close', function () {
            instance.config.close();
        });
        $('#custom_confirm_dialog').dialog('option', 'width', instance.config.width);
        $('#custom_confirm_dialog').dialog('open');
    }

    this.customConfirmLoad = function () {
        var customConfirmDialog = $("<div id='custom_confirm_dialog' class='customConfirmDialog'><div id='custom_confirm_message'></div><div id='custom_confirm_dialog_buttonsContainer' style='padding-top:20px'></div></div>").dialog({
            autoOpen: false,
            closeOnEscape: false,
            modal: true,
            width: 480
        });
    }

    this.registerDialog = function (dialog, contentDiv, dialogSettings) {
        var defaultSettings = {
            width: 500,
            close: true,
            fullscreen: false
        };

        var config = $.fn.extend(defaultSettings, dialogSettings);

        var parentContainer = contentDiv || $("body");
        var hasClose = config.close;
        var dialogWidth = config.width;
        var fullscreen = config.fullscreen;

        $(dialog).each(function () {
            var dialogId = this.id;

            if ($("[id='" + dialogId + "']").length > 1) {
                $(".ui-dialog-content[id='" + dialogId + "']").dialog("destroy").remove();
            }
            $("#" + dialogId, parentContainer).dialog({
                autoOpen: false,
                closeOnEscape: true,
                modal: true,
                width: dialogWidth,
                height: "auto",
                open: function () {
                    // set overlay opacity
                    $(".ui-widget-overlay").css("opacity", 0.8);
                    if (!hasClose || $(this).hasClass("noClose")) {
                        $(".ui-dialog-titlebar-close", $(this).parent()).remove();
                        $(this).dialog("option", "closeOnEscape", false)
                    }
                    if (fullscreen || $(this).hasClass("fullscreen")) {
                        enableFullsreenDialog($(this));
                    }
                    var tinyMCEContainer = $(this).data("tinymcecontainer");
                    if (tinyMCEContainer && $(this).hasClass("tinyMCEDialog")) {
                        $(tinyMCEContainer).trigger({
                            type: "registerTinyMCE"
                        });
                        $(this).removeClass("tinyMCEDialog");
                    }
                }
            });

        });

    }
}