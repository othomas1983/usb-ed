﻿/// <reference path="knockout/knockout-3.2.0.js" />
/// <reference path="knockout/knockout.validation.min.js" />
/// <reference path="knockout/knockoutCommon.js" />
var VenueBooking = function (model) {

    var self = this;

    view.load();

    var emailPattern = new RegExp(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/);

    $("#progressDialog").hide();
    $("#cateringPackageADialog").hide();
    $("#cateringPackageBDialog").hide();
    $("#cateringPackageCDialog").hide();
    
    self.isPrivateBusinessUnit = ko.observable(false);

    self.roomNumber = ko.observable(model ? model.RoomNumber : null);
    self.venueId = ko.observable(model ? model.VenueId : null);
    self.venueBookingID = ko.observable(model ? model.VenueBookingID : null);
    self.venueNumber = ko.observable(model ? model.Venue.Number : null);
    self.venueFound = ko.observable(model ? model.Venue.VenueFound : null);
    self.venueSite = ko.observable(model ? model.Venue.Site : null);
    self.venueType = ko.observable(model ? model.Venue.Type : null);
    self.venueSeats = ko.observable(model ? model.Venue.Seats : null);
    self.venueResult = ko.observable(model ? model.Venue.Result : null);
    self.venueResultDetails = ko.observable(model ? model.Venue.ResultDetails : null);    
    self.venueName = ko.observable(model ? model.Venue.Name : null);

    //self.availabilityVenueName = ko.observable();
    self.cateringTimes = ko.observableArray();
    self.venueTimes = ko.observableArray();
    self.businessUnits = ko.observableArray();
    self.bookedList = ko.observableArray();
    
    self.eventName = ko.observable(model ? model.EventName : null).extend({ required: true });
    self.singleDay = ko.observable(true);
    self.bookingNumberOfPeopleExpected = ko.observable(model ? model.BookingNumberOfPeopleExpected : null).extend({ required: { message: "*" }, min: {param:1, message: "Value must be greater than 0" }});
    self.bookingVenue = ko.observable(model ? model.BookingVenue : null);
    self.bookingStartDate = ko.observable(model ? model.BookingStartDate : null).extend({ required: true });
    self.bookingEndDate = ko.observable(model ? model.BookingEndDate : null).extend({
        required: {
            onlyIf: function() {
                return self.singleDay();
            }
        }
    });
    self.eventBookingNotes = ko.observable(model ? model.BookingNotes : null).extend({
        required: {
            onlyIf: function() {
                return self.singleDay();
            }
        }
    });
    self.bookingTimeIn = ko.observable(model ? model.BookingTimeIn : null);
    self.bookingTimeOut = ko.observable(model ? model.BookingTimeOut : null);
    self.personRequestingVenue = ko.observable(model ? model.PersonRequestingVenue : null).extend({ required: true });
    self.contactNumber = ko.observable(model ? model.ContactNumber : null).extend({ required: true });
    self.eMail = ko.observable(model ? model.eMail : null).extend({ required: true }).extend({ pattern: { message: ' Please enter a valid email address', params: emailPattern } });
    self.companyName = ko.observable(model ? model.CompanyName : null);
    self.costCentre = ko.observable(model ? model.CostCentre : null);
    self.businessUnitId = ko.observable(model ? model.BusinessUnitId : null).extend({ required: true });
    self.billingAddress = ko.observable(model ? model.BillingAddress : null).extend({ required: { onlyIf: function () { return self.isPrivateBusinessUnit() === true; } } });
    self.vatNumber = ko.observable(model ? model.VatNumber : null);
    self.vatRegistrationNumber = ko.observablemodel ? model.VatRegistrationNumber : null;
    self.personToBeBilled = ko.observable(model ? model.PersonToBeBilled : null).extend({ required: { onlyIf: function () { return self.isPrivateBusinessUnit() === true; } } });
    self.dataProjector = ko.observable(model ? model.DataProjector : false);
    self.microPhone = ko.observable(model ? model.MicroPhone : false);
    self.laserPointer = ko.observable(model ? model.LaserPointer : false);
    self.penAndPaper = ko.observable(model ? model.PenAndPaper : false);
    self.mineralWater = ko.observable(model ? model.MineralWater : false);
    self.flowers = ko.observable(model ? model.Flowers : false);
    self.specialRequirements = ko.observable(model ? model.SpecialRequirements : null);
    self.cateringRequired = ko.observable(model ? model.CateringRequired : null);

    //self.package_A = ko.observable(model.CateringRequirements ? model.CateringRequirements.CateringPackageA : null);
    //self.cateringPackageATeaCoffeeArrivalTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.CateringPackageATeaCoffeeArrivalTime : null);
    //self.cateringPackageATeaCoffeeMidmorningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.CateringPackageATeaCoffeeMidmorningTime : null);
    //self.cateringPackageATeaCoffeeAfternoonTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.CateringPackageATeaCoffeeAfternoonTime : null);
    //self.cateringPackageALunchTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.CateringPackageALunchTime : null);

    //self.package_B = ko.observable(model.CateringRequirements ? model.CateringRequirements.CateringPackageB : null);
    //self.cateringPackageBTeaCoffeeArrivalTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.CateringPackageBTeaCoffeeArrivalTime : null);
    //self.cateringPackageBTeaCoffeeMidmorningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.CateringPackageBTeaCoffeeMidmorningTime : null);
    //self.cateringPackageBTeaCoffeeAfternoonTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.CateringPackageBTeaCoffeeAfternoonTime : null);
    //self.cateringPackageBLunchTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.CateringPackageBLunchTime : null);

    //self.package_C = ko.observable(model.CateringRequirements ? model.CateringRequirements.CateringPackageC : null);
    //self.cateringPackageCTeaCoffeeArrivalTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.CateringPackageCTeaCoffeeArrivalTime : null);
    //self.cateringPackageCTeaCoffeeMidmorningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.CateringPackageCTeaCoffeeMidmorningTime : null);
    //self.cateringPackageCTeaCoffeeAfternoonTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.CateringPackageCTeaCoffeeAfternoonTime : null);
    //self.cateringPackageCLunchTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.CateringPackageCLunchTime : null);

    //self.teaCoffee = ko.observable(model.CateringRequirements ? model.CateringRequirements.TeaCoffee : null);
    //self.teaCoffeeCount = ko.observable(model.CateringRequirements ? model.CateringRequirements.TeaCoffeeCount : null).extend({ min: 1 }),
    //self.teaCoffeeMorningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.TeaCoffeeMorningTime : null),
    //self.teaCoffeeMidMorningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.TeaCoffeeMidMorningTime : null),
    //self.teaCoffeeLunchTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.TeaCoffeeLunchTime : null),
    //self.teaCoffeeAfternoonTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.TeaCoffeeAfternoonTime : null),
    //self.teaCoffeeEveningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.TeaCoffeeEveningTime : null),

    //self.teaSnack = ko.observable(model.CateringRequirements ? model.CateringRequirements.TeaSnack : null);
    //self.teaSnackCount = ko.observable(model.CateringRequirements ? model.CateringRequirements.TeaSnackCount : null),
    //self.teaSnackMorningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.TeaSnackMorningTime : null),
    //self.teaSnackMidMorningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.TeaSnackMidMorningTime : null),
    //self.teaSnackLunchTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.TeaSnackLunchTime : null),
    //self.teaSnackAfternoonTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.TeaSnackAfternoonTime : null),
    //self.teaSnackEveningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.TeaSnackEveningTime : null),

    //self.tartlets = ko.observable(model.CateringRequirements ? model.CateringRequirements.Tartlets : null);
    //self.tartletsCount = ko.observable(model.CateringRequirements ? model.CateringRequirements.TartletsCount : null),
    //self.tartletsMorningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.TartletsMorningTime : null),
    //self.tartletsMidMorningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.TartletsMidMorningTime : null),
    //self.tartletsLunchTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.TartletsLunchTime : null),
    //self.tartletsAfternoonTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.TartletsAfternoonTime : null),
    //self.tartletsEveningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.TartletsEveningTime : null),

    //self.biscuits = ko.observable(model.CateringRequirements ? model.CateringRequirements.Biscuits : null);
    //self.biscuitsCount = ko.observable(model.CateringRequirements ? model.CateringRequirements.BiscuitsCount : null),
    //self.biscuitsMorningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.BiscuitsMorningTime : null),
    //self.biscuitsMidMorningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.BiscuitsMidMorningTime : null),
    //self.biscuitsLunchTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.BiscuitsLunchTime : null),
    //self.biscuitsAfternoonTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.BiscuitsAfternoonTime : null),
    //self.biscuitsEveningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.BiscuitsEveningTime : null),

    //self.mealOfTheDay = ko.observable(model.CateringRequirements ? model.CateringRequirements.MealOfTheDay : null);
    //self.mealOfTheDayCount = ko.observable(model.CateringRequirements ? model.CateringRequirements.MealOfTheDayCount : null),
    //self.mealOfTheDayMorningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.MealOfTheDayMorningTime : null),
    //self.mealOfTheDayMidMorningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.MealOfTheDayMidMorningTime : null),
    //self.mealOfTheDayLunchTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.MealOfTheDayLunchTime : null),
    //self.mealOfTheDayAfternoonTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.MealOfTheDayAfternoonTime : null),
    //self.mealOfTheDayEveningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.MealOfTheDayEveningTime : null),

    //self.buffetLunch = ko.observable(model.CateringRequirements ? model.CateringRequirements.BuffetLunch : null);
    //self.buffetLunchCount = ko.observable(model.CateringRequirements ? model.CateringRequirements.BuffetLunchCount : null),
    //self.buffetLunchMorningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.BuffetLunchMorningTime : null),
    //self.buffetLunchMidMorningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.BuffetLunchMidMorningTime : null),
    //self.buffetLunchLunchTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.BuffetLunchLunchTime : null),
    //self.buffetLunchAfternoonTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.BuffetLunchAfternoonTime : null),
    //self.buffetLunchEveningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.BuffetLunchEveningTime : null),

    //self.upmarketLunch = ko.observable(model.CateringRequirements ? model.CateringRequirements.UpmarketLunch : null);
    //self.upmarketLunchCount = ko.observable(model.CateringRequirements ? model.CateringRequirements.UpmarketLunchCount : null),
    //self.upmarketLunchMorningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.UpmarketLunchMorningTime : null),
    //self.upmarketLunchMidMorningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.UpmarketLunchMidMorningTime : null),
    //self.upmarketLunchLunchTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.UpmarketLunchLunchTime : null),
    //self.upmarketLunchAfternoonTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.UpmarketLunchAfternoonTime : null),
    //self.upmarketLunchEveningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.UpmarketLunchEveningTime : null),

    //self.dessert = ko.observable(model.CateringRequirements ? model.CateringRequirements.Dessert : null);
    //self.dessertCount = ko.observable(model.CateringRequirements ? model.CateringRequirements.DessertCount : null),
    //self.dessertMorningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.DessertMorningTime : null),
    //self.dessertMidMorningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.DessertMidMorningTime : null),
    //self.dessertLunchTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.DessertLunchTime : null),
    //self.dessertAfternoonTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.DessertAfternoonTime : null),
    //self.dessertEveningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.DessertEveningTime : null),

    //self.dinner = ko.observable(model.CateringRequirements ? model.CateringRequirements.Dinner : null);
    //self.dinnerCount = ko.observable(model.CateringRequirements ? model.CateringRequirements.DinnerCount : null),
    //self.dinnerMorningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.DinnerMorningTime : null),
    //self.dinnerMidMorningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.DinnerMidMorningTime : null),
    //self.dinnerLunchTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.DinnerLunchTime : null),
    //self.dinnerAfternoonTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.DinnerAfternoonTime : null),
    //self.dinnerEveningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.DinnerEveningTime : null),

    //self.cans = ko.observable(model.CateringRequirements ? model.CateringRequirements.Cans : null);
    //self.cansCount = ko.observable(model.CateringRequirements ? model.CateringRequirements.CansCount : null),
    //self.cansMorningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.CansMorningTime : null),
    //self.cansMidMorningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.CansMidMorningTime : null),
    //self.cansLunchTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.CansLunchTime : null),
    //self.cansAfternoonTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.CansAfternoonTime : null),
    //self.cansEveningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.CansEveningTime : null),

    //self.mineralWater = ko.observable(model.CateringRequirements ? model.CateringRequirements.MineralWater : null);
    //self.mineralWaterCount = ko.observable(model.CateringRequirements ? model.CateringRequirements.MineralWaterCount : null),
    //self.mineralWaterMorningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.MineralWaterMorningTime : null),
    //self.mineralWaterMidMorningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.MineralWaterMidMorningTime : null),
    //self.mineralWaterLunchTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.MineralWaterLunchTime : null),
    //self.mineralWaterAfternoonTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.MineralWaterAfternoonTime : null),
    //self.mineralWaterEveningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.MineralWaterEveningTime : null),

    //self.juicePerGlass = ko.observable(model.CateringRequirements ? model.CateringRequirements.JuicePerGlass : null);
    //self.juicePerGlassCount = ko.observable(model.CateringRequirements ? model.CateringRequirements.JuicePerGlassCount : null),
    //self.juicePerGlassMorningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.JuicePerGlassMorningTime : null),
    //self.juicePerGlassMidMorningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.JuicePerGlassMidMorningTime : null),
    //self.juicePerGlassLunchTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.JuicePerGlassLunchTime : null),
    //self.juicePerGlassAfternoonTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.JuicePerGlassAfternoonTime : null),
    //self.juicePerGlassEveningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.JuicePerGlassEveningTime : null),

    //self.jugsOfJuice = ko.observable(model.CateringRequirements ? model.CateringRequirements.JugsOfJuice : null);
    //self.jugsOfJuiceCount = ko.observable(model.CateringRequirements ? model.CateringRequirements.JugsOfJuiceCount : null),
    //self.jugsOfJuiceMorningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.JugsOfJuiceMorningTime : null),
    //self.jugsOfJuiceMidMorningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.JugsOfJuiceMidMorningTime : null),
    //self.jugsOfJuiceLunchTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.JugsOfJuiceLunchTime : null),
    //self.jugsOfJuiceAfternoonTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.JugsOfJuiceAfternoonTime : null),
    //self.jugsOfJuiceEveningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.JugsOfJuiceEveningTime : null),

    //self.braai2Meats = ko.observable(model.CateringRequirements ? model.CateringRequirements.Braai2Meats : null);
    //self.braai2MeatsCount = ko.observable(model.CateringRequirements ? model.CateringRequirements.Braai2MeatsCount : null),
    //self.braai2MeatsMorningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.Braai2MeatsMorningTime : null),
    //self.braai2MeatsMidMorningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.Braai2MeatsMidMorningTime : null),
    //self.braai2MeatsLunchTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.Braai2MeatsLunchTime : null),
    //self.braai2MeatsAfternoonTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.Braai2MeatsAfternoonTime : null),
    //self.braai2MeatsEveningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.Braai2MeatsEveningTime : null),

    //self.braai3Meats = ko.observable(model.CateringRequirements ? model.CateringRequirements.Braai3Meats : null);
    //self.braai3MeatsCount = ko.observable(model.CateringRequirements ? model.CateringRequirements.Braai3MeatsCount : null),
    //self.braai3MeatsMorningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.Braai3MeatsMorningTime : null),
    //self.braai3MeatsMidMorningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.Braai3MeatsMidMorningTime : null),
    //self.braai3MeatsLunchTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.Braai3MeatsLunchTime : null),
    //self.braai3MeatsAfternoonTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.Braai3MeatsAfternoonTime : null),
    //self.braai3MeatsEveningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.Braai3MeatsEveningTime : null),

    //self.spitBraai = ko.observable(model.CateringRequirements ? model.CateringRequirements.SpitBraai : null);
    //self.spitBraaiCount = ko.observable(model.CateringRequirements ? model.CateringRequirements.SpitBraaiCount : null),
    //self.spitBraaiMorningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.SpitBraaiMorningTime : null),
    //self.spitBraaiMidMorningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.SpitBraaiMidMorningTime : null),
    //self.spitBraaiLunchTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.SpitBraaiLunchTime : null),
    //self.spitBraaiAfternoonTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.SpitBraaiAfternoonTime : null),
    //self.spitBraaiEveningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.SpitBraaiEveningTime : null),

    //self.potjie = ko.observable(model.CateringRequirements ? model.CateringRequirements.Potjie : null);
    //self.potjieCount = ko.observable(model.CateringRequirements ? model.CateringRequirements.PotjieCount : null),
    //self.potjieMorningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.PotjieMorningTime : null),
    //self.potjieMidMorningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.PotjieMidMorningTime : null),
    //self.potjieLunchTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.PotjieLunchTime : null),
    //self.potjieAfternoonTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.PotjieAfternoonTime : null),
    //self.potjieEveningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.PotjieEveningTime : null),

    //self.cockTailHalf = ko.observable(model.CateringRequirements ? model.CateringRequirements.CockTailHalf : null);
    //self.cockTailHalfCount = ko.observable(model.CateringRequirements ? model.CateringRequirements.CockTailHalfCount : null),
    //self.cockTailHalfMorningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.CockTailHalfMorningTime : null),
    //self.cockTailHalfMidMorningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.CockTailHalfMidMorningTime : null),
    //self.cockTailHalfLunchTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.CockTailHalfLunchTime : null),
    //self.cockTailHalfAfternoonTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.CockTailHalfAfternoonTime : null),
    //self.cockTailHalfEveningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.CockTailHalfEveningTime : null),

    //self.cockTailFull = ko.observable(model.CateringRequirements ? model.CateringRequirements.CockTailFull : null);
    //self.cockTailFullCount = ko.observable(model.CateringRequirements ? model.CateringRequirements.CockTailFullCount : null),
    //self.cockTailFullMorningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.CockTailFullMorningTime : null),
    //self.cockTailFullMidMorningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.CockTailFullMidMorningTime : null),
    //self.cockTailFullLunchTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.CockTailFullLunchTime : null),
    //self.cockTailFullAfternoonTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.CockTailFullAfternoonTime : null),
    //self.cockTailFullEveningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.CockTailFullEveningTime : null),

    //self.bowlFoods = ko.observable(model.CateringRequirements ? model.CateringRequirements.BowlFoods : null);
    //self.bowlFoodsCount = ko.observable(model.CateringRequirements ? model.CateringRequirements.BowlFoodsCount : null),
    //self.bowlFoodsMorningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.BowlFoodsMorningTime : null),
    //self.bowlFoodsMidMorningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.BowFoodsMidMorningTime : null),
    //self.bowlFoodsLunchTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.BowlFoodsLunchTime : null),
    //self.bowlFoodsAfternoonTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.BowlFoodsAfternoonTime : null),
    //self.bowlFoodsEveningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.BowlFoodsEveningTime : null),

    //self.buffetBreakfast = ko.observable(model.CateringRequirements ? model.CateringRequirements.BuffetBreakfast : null);
    //self.buffetBreakfastCount = ko.observable(model.CateringRequirements ? model.CateringRequirements.BuffetBreakfastCount : null),
    //self.buffetBreakfastMorningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.BuffetBreakfastMorningTime : null),
    //self.buffetBreakfastMidMorningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.BuffetBreakfastMidMorningTime : null),
    //self.buffetBreakfastLunchTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.BuffetBreakfastLunchTime : null),
    //self.buffetBreakfastAfternoonTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.BuffetBreakfastAfternoonTime : null),
    //self.buffetBreakfastEveningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.BuffetBreakfastEveningTime : null),

    //self.fingerBreakfast = ko.observable(model.CateringRequirements ? model.CateringRequirements.FingerBreakfast : null);
    //self.fingerBreakfastCount = ko.observable(model.CateringRequirements ? model.CateringRequirements.fingerBreakfastCount : null),
    //self.fingerBreakfastMorningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.FingerBreakfastMorningTime : null),
    //self.fingerBreakfastMidMorningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.FingerBreakfastMidMorningTime : null),
    //self.fingerBreakfastLunchTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.FingerBreakfastLunchTime : null),
    //self.fingerBreakfastAfternoonTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.FingerBreakfastAfternoonTime : null),
    //self.fingerBreakfastEveningTime = ko.observable(model.CateringRequirements ? model.CateringRequirements.FingerBreakfastEveningTime : null),

    //self.corkage = ko.observable(model.CateringRequirements ? model.CateringRequirements.Corkage : null);
    //self.corkageCount = ko.observable(model.CateringRequirements ? model.CateringRequirements.CorkageCount : null);

    self.availableBookingTimeOut = ko.observableArray();

    LoadCateringRequirementTimes();
    LoadVenueTimes();
    LoadBusinessUnits();

    var VenueBookingDetails = {
        RoomNumber: self.roomNumber,
        VenueId: self.venueId,
        VenueBookingID: self.venueBookingID,
        EventName: self.eventName,
        SingleDay: self.singleDay,
        BookingNumberOfPeopleExpected: self.bookingNumberOfPeopleExpected,
        BookingVenue: self.bookingVenue,
        BookingStartDate: self.bookingStartDate,
        BookingEndDate: self.bookingEndDate,
        BookingNotes: self.eventBookingNotes,
        BookingTimeIn: self.bookingTimeIn,
        BookingTimeOut: self.bookingTimeOut,
        PersonRequestingVenue: self.personRequestingVenue,
        ContactNumber: self.contactNumber,
        EMail: self.eMail,
        CompanyName: self.companyName,
        BusinessUnitId: self.businessId,
        CostCentre: self.costCentre,
        BusinessUnitId: self.businessUnitId,
        BillingAddress: self.billingAddress,
        VatNumber: self.vatNumber,
        VatRegistrationNumber: self.vatRegistrationNumber,
        PersonToBeBilled: self.personToBeBilled,
        DataProjector: self.dataProjector,
        MicroPhone: self.microPhone,
        LaserPointer: self.laserPointer,
        PenAndPaper: self.penAndPaper,
        MineralWater: self.mineralWater,
        Flowers: self.flowers,
        SpecialRequirements: self.specialRequirements,
        VenueName: self.venueName,
        CateringRequired: self.cateringRequired
    }

    var CateringRequirements = {
        CateringPackageA: self.package_A,
        CateringPackageATeaCoffeeArrivalTime: self.cateringPackageATeaCoffeeArrivalTime,
        CateringPackageATeaCoffeeMidmorningTime: self.cateringPackageATeaCoffeeMidmorningTime,
        CateringPackageATeaCoffeeAfternoonTime: self.cateringPackageATeaCoffeeAfternoonTime,
        CateringPackageALunchTime: self.cateringPackageALunchTime,

        CateringPackageB: self.package_B,
        CateringPackageBTeaCoffeeArrivalTime: self.cateringPackageBTeaCoffeeArrivalTime,
        CateringPackageBTeaCoffeeMidmorningTime: self.cateringPackageBTeaCoffeeMidmorningTime,
        CateringPackageBTeaCoffeeAfternoonTime: self.cateringPackageBTeaCoffeeAfternoonTime,
        CateringPackageBLunchTime: self.cateringPackageBLunchTime,

        CateringPackageC: self.package_C,
        CateringPackageCTeaCoffeeArrivalTime: self.cateringPackageCTeaCoffeeArrivalTime,
        CateringPackageCTeaCoffeeMidmorningTime: self.cateringPackageCTeaCoffeeMidmorningTime,
        CateringPackageCTeaCoffeeAfternoonTime: self.cateringPackageCTeaCoffeeAfternoonTime,
        CateringPackageCLunchTime: self.cateringPackageCLunchTime,

        TeaCoffee: self.teaCoffee,
        TeaCoffeeCount: self.teaCoffeeCount,
        TeaCoffeeMorningTime: self.teaCoffeeMorningTime,
        TeaCoffeeMidMorningTime: self.teaCoffeeMidMorningTime,
        TeaCoffeeLunchTime: self.teaCoffeeLunchTime,
        TeaCoffeeAfternoonTime: self.teaCoffeeAfternoonTime,
        TeaCoffeeEveningTime: self.teaCoffeeEveningTime,

        TeaSnack: self.teaSnack,
        TeaSnackCount: self.teaSnackCount,
        TeaSnackMorningTime: self.teaSnackMorningTime,
        TeaSnackMidMorningTime: self.teaSnackMidMorningTime,
        TeaSnackLunchTime: self.teaSnackLunchTime,
        TeaSnackAfternoonTime: self.teaSnackAfternoonTime,
        TeaSnackEveningTime: self.teaSnackEveningTime,

        Tartlets: self.tartlets,
        TartletsCount: self.tartletsCount,
        TartletsMorningTime: self.tartletsMorningTime,
        TartletsMidMorningTime: self.tartletsMidMorningTime,
        TartletsLunchTime: self.tartletsLunchTime,
        TartletsAfternoonTime: self.tartletsAfternoonTime,
        TartletsEveningTime: self.tartletsEveningTime,

        Biscuits: self.biscuits,
        BiscuitsCount: self.biscuitsCount,
        BiscuitsMorningTime: self.biscuitsMorningTime,
        BiscuitsMidMorningTime: self.biscuitsMidMorningTime,
        BiscuitsLunchTime: self.biscuitsLunchTime,
        BiscuitsAfternoonTime: self.biscuitsAfternoonTime,
        BiscuitsEveningTime: self.biscuitsEveningTime,

        MealOfTheDay: self.mealOfTheDay,
        MealOfTheDayCount: self.mealOfTheDayCount,
        MealOfTheDayMorningTime: self.mealOfTheDayMorningTime,
        MealOfTheDayMidMorningTime: self.mealOfTheDayMidMorningTime,
        MealOfTheDayLunchTime: self.mealOfTheDayLunchTime,
        MealOfTheDayAfternoonTime: self.mealOfTheDayAfternoonTime,
        MealOfTheDayEveningTime: self.mealOfTheDayEveningTime,

        BuffetLunch: self.buffetLunch,
        BuffetLunchCount: self.buffetLunchCount,
        BuffetLunchMorningTime: self.buffetLunchMorningTime,
        BuffetLunchMidMorningTime: self.buffetLunchMidMorningTime,
        BuffetLunchLunchTime: self.buffetLunchLunchTime,
        BuffetLunchAfternoonTime: self.buffetLunchAfternoonTime,
        BuffetLunchEveningTime: self.buffetLunchEveningTime,

        UpmarketLunch: self.upmarketLunch,
        UpmarketLunchCount: self.upmarketLunchCount,
        UpmarketLunchMorningTime: self.upmarketLunchMorningTime,
        UpmarketLunchMidMorningTime: self.upmarketLunchMidMorningTime,
        UpmarketLunchLunchTime: self.upmarketLunchLunchTime,
        UpmarketLunchAfternoonTime: self.upmarketLunchAfternoonTime,
        UpmarketLunchEveningTime: self.upmarketLunchEveningTime,

        Dessert: self.dessert,
        DessertCount: self.dessertCount,
        DessertMorningTime: self.dessertMorningTime,
        DessertMidMorningTime: self.dessertMidMorningTime,
        DessertLunchTime: self.dessertLunchTime,
        DessertAfternoonTime: self.dessertAfternoonTime,
        DessertEveningTime: self.dessertEveningTime,

        Dinner: self.dinner,
        DinnerCount: self.dinnerCount,
        DinnerMorningTime: self.dinnerMorningTime,
        DinnerMidMorningTime: self.dinnerMidMorningTime,
        DinnerLunchTime: self.dinnerLunchTime,
        DinnerAfternoonTime: self.dinnerAfternoonTime,
        DinnerEveningTime: self.dinnerEveningTime,

        Cans: self.cans,
        CansCount: self.cansCount,
        CansMorningTime: self.cansMorningTime,
        CansMidMorningTime: self.cansMidMorningTime,
        CansLunchTime: self.cansLunchTime,
        CansAfternoonTime: self.cansAfternoonTime,
        CansEveningTime: self.cansEveningTime,

        MineralWater: self.mineralWater,
        MineralWaterCount: self.mineralWaterCount,
        MineralWaterMorningTime: self.mineralWaterMorningTime,
        MineralWaterMidMorningTime: self.mineralWaterMidMorningTime,
        MineralWaterLunchTime: self.mineralWaterLunchTime,
        MineralWaterAfternoonTime: self.mineralWaterAfternoonTime,
        MineralWaterEveningTime: self.mineralWaterEveningTime,

        JuicePerGlass: self.juicePerGlass,
        JuicePerGlassCount: self.juicePerGlassCount,
        JuicePerGlassMorningTime: self.juicePerGlassMorningTime,
        JuicePerGlassMidMorningTime: self.juicePerGlassMidMorningTime,
        JuicePerGlassLunchTime: self.juicePerGlassLunchTime,
        JuicePerGlassAfternoonTime: self.juicePerGlassAfternoonTime,
        JuicePerGlassEveningTime: self.juicePerGlassEveningTime,

        JugsOfJuice: self.jugsOfJuice,
        JugsOfJuiceCount: self.jugsOfJuiceCount,
        JugsOfJuiceMorningTime: self.jugsOfJuiceMorningTime,
        JugsOfJuiceMidMorningTime: self.jugsOfJuiceMidMorningTime,
        JugsOfJuiceLunchTime: self.jugsOfJuiceLunchTime,
        JugsOfJuiceAfternoonTime: self.jugsOfJuiceAfternoonTime,
        JugsOfJuiceEveningTime: self.jugsOfJuiceEveningTime,

        Braai2Meats: self.braai2Meats,
        Braai2MeatsCount: self.braai2MeatsCount,
        Braai2MeatsMorningTime: self.braai2MeatsMorningTime,
        Braai2MeatsMidMorningTime: self.braai2MeatsMidMorningTime,
        Braai2MeatsLunchTime: self.braai2MeatsLunchTime,
        Braai2MeatsAfternoonTime: self.braai2MeatsAfternoonTime,
        Braai2MeatsEveningTime: self.braai2MeatsEveningTime,

        Braai3Meats: self.braai3Meats,
        Braai3MeatsCount: self.braai3MeatsCount,
        Braai3MeatsMorningTime: self.braai3MeatsMorningTime,
        Braai3MeatsMidMorningTime: self.braai3MeatsMidMorningTime,
        Braai3MeatsLunchTime: self.braai3MeatsLunchTime,
        Braai3MeatsAfternoonTime: self.braai3MeatsAfternoonTime,
        Braai3MeatsEveningTime: self.braai3MeatsEveningTime,

        SpitBraai: self.spitBraai,
        SpitBraaiCount: self.spitBraaiCount,
        SpitBraaiMorningTime: self.spitBraaiMorningTime,
        SpitBraaiMidMorningTime: self.spitBraaiMidMorningTime,
        SpitBraaiLunchTime: self.spitBraaiLunchTime,
        SpitBraaiAfternoonTime: self.spitBraaiAfternoonTime,
        SpitBraaiEveningTime: self.spitBraaiEveningTime,

        Potjie: self.potjie,
        PotjieCount: self.potjieCount,
        PotjieMorningTime: self.potjieMorningTime,
        PotjieMidMorningTime: self.potjieMidMorningTime,
        PotjieLunchTime: self.potjieLunchTime,
        PotjieAfternoonTime: self.potjieAfternoonTime,
        PotjieEveningTime: self.potjieEveningTime,

        CockTailHalf: self.cockTailHalf,
        CockTailHalfCount: self.cockTailHalfCount,
        CockTailHalfMorningTime: self.cockTailHalfMorningTime,
        CockTailHalfMidMorningTime: self.cockTailHalfMidMorningTime,
        CockTailHalfLunchTime: self.cockTailHalfLunchTime,
        CockTailHalfAfternoonTime: self.cockTailHalfAfternoonTime,
        CockTailHalfEveningTime: self.cockTailHalfEveningTime,

        CockTailFull: self.cockTailFull,
        CockTailFullCount: self.cockTailFullCount,
        CockTailFullMorningTime: self.cockTailFullMorningTime,
        CockTailFullMidMorningTime: self.cockTailFullMidMorningTime,
        CockTailFullLunchTime: self.cockTailFullLunchTime,
        CockTailFullAfternoonTime: self.cockTailFullAfternoonTime,
        CockTailFullEveningTime: self.cockTailFullEveningTime,

        BowlFoods: self.bowlFoods,
        BowlFoodsCount: self.bowlFoodsCount,
        BowlFoodsMorningTime: self.bowlFoodsMorningTime,
        BowlFoodsMidMorningTime: self.bowlFoodsMidMorningTime,
        BowlFoodsLunchTime: self.bowlFoodsLunchTime,
        BowlFoodsAfternoonTime: self.bowlFoodsAfternoonTime,
        BowlFoodsEveningTime: self.bowlFoodsEveningTime,

        BuffetBreakfast: self.buffetBreakfastbuffetBreakfast,
        BuffetBreakfastCount: self.buffetBreakfastCount,
        BuffetBreakfastMorningTime: self.buffetBreakfastMorningTime,
        BuffetBreakfastMidMorningTime: self.buffetBreakfastMidMorningTime,
        BuffetBreakfastLunchTime: self.buffetBreakfastLunchTime,
        BuffetBreakfastAfternoonTime: self.buffetBreakfastAfternoonTime,
        BuffetBreakfastEveningTime: self.buffetBreakfastEveningTime,

        FingerBreakfast: self.fingerBreakfast,
        FingerBreakfastCount: self.fingerBreakfastCount,
        FingerBreakfastMorningTime: self.fingerBreakfastMorningTime,
        FingerBreakfastMidMorningTime: self.fingerBreakfastMidMorningTime,
        FingerBreakfastLunchTime: self.fingerBreakfastLunchTime,
        FingerBreakfastAfternoonTime: self.fingerBreakfastAfternoonTime,
        FingerBreakfastEveningTime: self.fingerBreakfastEveningTime,

        Corkage: self.corkage,
        CorkageCount: self.corkageCount
    }

    VenueBookingDetails.errors = ko.validation.group(VenueBookingDetails);
    //CateringRequirements.errors = ko.validation.group(CateringRequirements);

    ko.bindingHandlers.bookingStartDateFormat = {
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

            var value = valueAccessor();
            var newValueAccessor = ko.unwrap(value);
            var dt = new Date(newValueAccessor);

            if (newValueAccessor) {
                var formattedDate = formatBookingDate(dt);
                self.bookingStartDate(formattedDate);
            }
        }
    };

    ko.bindingHandlers.bookingEndDateFormat = {
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

            var value = valueAccessor();
            var newValueAccessor = ko.unwrap(value);
            var dt = new Date(newValueAccessor);

            if (newValueAccessor) {
                var formattedDate = formatBookingDate(dt);
                self.bookingEndDate(formattedDate);
            }
        }
    };

    self.availabilityVenueName = ko.computed(function() {
        return "Availability for: " + self.venueName();
    });

    self.singleDay.subscribe(function (selected) {
        if (selected != undefined && selected) {
            self.bookingEndDate(null);
        }
    });

    //self.cateringRequired.subscribe(function () {
         
    //    if (self.cateringRequired() == false) {
    //        ClearCateringOptions();
    //    }
    //});

    self.businessUnitId.subscribe(function() {
        var selectedValue = $("#ddBusinessUnits option:selected").val();
        var selectPrivate = $('#ddBusinessUnits option').filter(function() { return $(this).html() == "Private"; })
            .val();

        if (selectedValue === selectPrivate) {
            self.isPrivateBusinessUnit(true);
        } else {
            self.isPrivateBusinessUnit(false);
        }
    });

    self.submitBooking = function()
    {
        if (VenueBookingDetails.errors().length > 0) {
            VenueBookingDetails.errors.showAllMessages(true);
            //CateringRequirements.errors.showAllMessages(true);
            $("#bookingAlert").show();
            window.scrollTo(0, 0);
            return;
        }

        //if (CateringRequirementsSelected() &&
        //            ($("#cateringPackageA").prop("checked") == true ||
        //            $("#cateringPackageB").prop("checked") == true ||
        //            $("#cateringPackageC").prop("checked") == true)) {
        //    alert("You have Catering Requirements and a Package selected. Please make sure that you do not have any Catering Requirements selected together with a Package.");
        //    return false;
        //}
        $("#bookingAlert").hide();
        showProgressDialog("Busy uploading your application. Please wait...");

        var venue = ko.toJSON(VenueBookingDetails);
        //var catering = ko.toJSON(CateringRequirements);      

        $.ajax({
            data: '{ venueBooking :' +  venue  + '}',
            type: "POST",
            contentType: "application/json",
            cache: false,
            url: "VenueBooking/SaveBooking",
            async: false,
            success: function (j) {
                if (j.Success == true) {
                    window.location.href = "VenueBookingSubmitted/Index";
                }
                else {
                    alert(j.Message);
                }
            },
            error: function (e, x, r) {
                alert(e);
                hideProgressDialog();
            }
        }).always = function () {
            hideProgressDialog();
        }
    }

    //self.package_A.subscribe(function () {
    //    if (self.package_A() == true) {
    //        self.package_B(false);
    //        self.package_C(false);
    //        if (CateringRequirementsSelected()) {
    //            ConfirmSelection($("#cateringPackageA"), $("#cateringPackageADialog"), 'Catering Package A');
    //        }
    //        else {
    //            ShowCateringPackageDialog($("#cateringPackageADialog"), 'Catering Package A');
    //        }
    //    }
    //});

    //self.package_B.subscribe(function () {
    //    if (self.package_B() == true) {
    //        self.package_A(false);
    //        self.package_C(false);
    //        if (CateringRequirementsSelected()) {
    //            ConfirmSelection($("#cateringPackageB"), $("#cateringPackageBDialog"), 'Catering Package B');
    //        }
    //        else {
    //            ShowCateringPackageDialog($("#cateringPackageBDialog"), 'Catering Package B');
    //        }
    //    }
    //});

    //self.package_C.subscribe(function () {
    //    if (self.package_C() == true) {
    //        self.package_A(false);
    //        self.package_B(false);
    //        if (CateringRequirementsSelected()) {
    //            ConfirmSelection($("#cateringPackageC"), $("#cateringPackageCDialog"), 'Catering Package C');
    //        }
    //        else {
    //            ShowCateringPackageDialog($("#cateringPackageCDialog"), 'Catering Package C');
    //        }
    //    }
    //});

    self.showPackageADetails = function () {
        ShowCateringPackageDialog($("#cateringPackageADialog"), 'Catering Package A');
    }

    self.showPackageBDetails = function () {
        ShowCateringPackageDialog($("#cateringPackageBDialog"), 'Catering Package B');
    }

    self.showPackageCDetails = function () {
        ShowCateringPackageDialog($("#cateringPackageCDialog"), 'Catering Package C');
    }

    self.bookingTimeIn.subscribe(function (value) {

        self.availableBookingTimeOut([]);

        if (value != undefined && value !== "") {
            var availableTimeSlots = JSLINQ(self.venueTimes())
                .Where(function(item) {
                    return item.Value > value;
                });

            self.availableBookingTimeOut(availableTimeSlots.items);
        }
    });

    formatBookingDate = function (date) {
        try {
            //var date = new Date(parseInt(/^\/Date\((.*?)\)\/$/.exec(date)[1], 10));
            var day = String("00" + date.getDate()).slice(-2);
            var month = String("00" + (date.getMonth() + 1)).slice(-2);
            var year = date.getFullYear();

            var formatedDate = year + "-" + month + "-" + day;

            return formatedDate;
        } catch (e) {
            return jsonDate
        }
    }

    function LoadCateringRequirementTimes() {
        $.ajax({
            url: "VenueBooking/LoadCateringRequirementTimes",
            type: 'GET',
            contentType: "application/json",
            cache: true,
            success: function (j) {
                self.cateringTimes(j.CateringTimes);
            },
            error: function (error) {
            }
        });
    }

    function LoadVenueTimes() {
        $.ajax({
            url: "VenueBooking/LoadVenueTimes",
            type: 'GET',
            contentType: "application/json",
            cache: true,
            success: function (j) {
                self.venueTimes(j.VenueTimes);
            },
            error: function (error) {
            }
        });
    }

    function LoadBusinessUnits() {
        $.ajax({
            url: "VenueBooking/LoadBusinessUnits",
            type: 'GET',
            contentType: "application/json",
            cache: true,
            success: function (j) {
                self.businessUnits(j.BusinessUnits);
            },
            error: function (error) {
            }
        });
    }

    self.LoadVenueAvailibility = function() {
        $('#myModal').modal('show');

        var venue = ko.toJSON(VenueBookingDetails);
        $.ajax({
            url: "VenueBooking/LoadVenueAvailability",
            data: '{ venueBooking :' + venue + '}',
            type: 'POST',
            contentType: "application/json",
            cache: false,
            async: false,
            success: function(j) {
                $('#calendar').fullCalendar('destroy');
                self.bookedList(j.BookedList);
            },
            error: function(error) {
            }
        });


        $('#calendar').fullCalendar({
            header: {
                left: self.roomNumber(),
                center: 'title',
                right: null
            },
            minTime: self.bookingTimeIn() != undefined ? self.bookingTimeIn() : "00:00:00",
            maxTime: self.bookingTimeOut() != undefined ? self.bookingTimeOut() : "23:59:59",
            defaultDate: self.bookingStartDate(),
            defaultView: 'agendaWeek',
            events: self.bookedList()
        });

        $('#myModal').on('shown.bs.modal',
            function() {
                $("#calendar").fullCalendar('render');
            });

    };

    $(".dialogClose").click(function() {
        $("#cateringPackageADialog").dialog("close");
    });

    $(".alert-close").click(function() {
        $("#bookingAlert").hide();
    });

    function ConfirmSelection(control, dialog, title) {
        view.confirm({
            message: "You have other catering options selected in the list. If you continue the list will be cleared. Are you sure you would like to continue?",
            title: "Catering Requirements",
            type: "warning",
            yes: function () {
                ClearCateringOptions();
                ShowCateringPackageDialog(dialog, title);
            },
            no: function () {
                control.prop("checked", false);
            }
        });
    };

    function ShowCateringPackageDialog(control, title) {
        control.dialog(
        {
            modal: true,
            draggable: false,
            resizable: false,
            width: 600,
            height: 320,
            dialogClass: 'no-close success-dialog',
            buttons: {
                "Save & Close": function () {
                    $(this).dialog("close");
                }
            },
            open: function (event, ui) {
                $('.ui-dialog-titlebar').html(title);
            }
        });
    };

    function ShowAvailibilityDialog() {
        $("#availibilityModal").dialog(
        {
            modal: true,
            draggable: false,
            resizable: false,
            width: 1200,
            height: 1000,
            dialogClass: 'no-close success-dialog',
            buttons: {
                "Close": function () {
                    $(this).dialog("close");
                }
            },
            open: function (event, ui) {
                $('.ui-dialog-titlebar').html("TEST");
            }
        });
    };

    function ClearCateringOptions() {
        self.teaCoffee(false);
        self.teaSnack(false);
        self.tartlets(false);
        self.biscuits(false);
        self.mealOfTheDay(false);
        self.buffetLunch(false);
        self.upmarketLunch(false);
        self.dessert(false);
        self.dinner(false);
        self.cans(false);
        self.mineralWater(false);
        self.juicePerGlass(false);
        self.jugsOfJuice(false);
        self.braai2Meats(false);
        self.braai3Meats(false);
        self.spitBraai(false);
        self.potjie(false);
        self.cockTailHalf(false);
        self.cockTailFull(false);
        self.bowlFoods(false);
        self.buffetBreakfast(false);
        self.fingerBreakfast(false);
        self.corkage(false);

        $(".timeOption").each(function () {
            $(this).prop('selectedIndex', 0);
        })
    };

    $("#frmVenueBooking").submit(function () {

        //if (CateringRequirementsSelected() &&
        //    ($("#cateringPackageA").prop("checked") == true ||
        //    $("#cateringPackageB").prop("checked") == true ||
        //    $("#cateringPackageC").prop("checked") == true)) {
        //    alert("You have Catering Requirements and a Package selected. Please make sure that you do not have any Catering Requirements selected together with a Package.");
        //    return false;
        //}

        showProgressDialog("Busy uploading your application. Please wait...");

        var data = ko.toJSON(VenueBookingDetails);

        $.ajax({
            data: data,
            type: "POST",
            contentType: "application/json",
            cache: false,
            url: "VenueBooking/Index",
            async: false,
            success: function (j) {
                return false;
            },
            error: function (e, x, r) {
                alert(e);
            },
            done: function () {
                hideProgressDialog();
            }
        });
    });

    $("#saveBooking").click(function () {
        var catering = ko.toJSON(cateringRequirements);

        $("#frmVenueBooking").submit();

        $.ajax({
            type: "POST",
            contentType: "application/json",
            cache: false,
            url: "VenueBooking/Index",
            data: catering
        });
    });

    function CateringRequirementsSelected() {

        return (self.teaCoffee() == true ||
        self.teaSnack() == true ||
        self.tartlets() == true ||
        self.biscuits() == true ||
        self.mealOfTheDay() == true ||
        self.buffetLunch() == true ||
        self.upmarketLunch() == true ||
        self.dessert() == true ||
        self.dinner() == true ||
        self.cans() == true ||
        self.mineralWater() == true ||
        self.juicePerGlass() == true ||
        self.jugsOfJuice() == true ||
        self.braai2Meats() == true ||
        self.braai3Meats() == true ||
        self.spitBraai() == true ||
        self.potjie() == true ||
        self.cockTailHalf() == true ||
        self.cockTailFull() == true ||
        self.bowlFoods() == true ||
        self.buffetBreakfast() == true ||
        self.fingerBreakfast() == true ||
        self.corkage() == true);
    };


    $("#frmVenueBooking :input").change(function() {
        var control = $(this);
        control.removeClass('input-validation-error');
    });

    $(function () {

        $('#venueEndDate').datepicker({
            defaultDate: new Date(),
            showButtonPanel: true,
            changeMonth: true,
            changeYear: true,
            changeDay: true,
            maxDate: "+5y",
            yearRange: "+0:+5",
            dateFormat: 'yy-mm-dd',
            onClose: function (dateText, inst) {
                var getdate = $('#venueEndDate').datepicker('getDate');
                if (getdate) {
                    var day = getdate.getDate();
                    var month = getdate.getMonth();
                    var year = getdate.getFullYear();
                    $('#venueEndDate').datepicker('setDate', new Date(year, month, day));
                }
            }
        });

        $('#venueStartDate').datepicker({
            showButtonPanel: true,
            changeMonth: true,
            changeYear: true,
            changeDay: true,
            minDate: +0,
            maxDate: "+5y",
            yearRange: "+0:+5",
            dateFormat: 'yy-mm-dd',
            onClose: function(dateText, inst) {
                var getdate = $('#venueStartDate').datepicker('getDate');
                if (getdate) {
                    var day = getdate.getDate();
                    var month = getdate.getMonth();
                    var year = getdate.getFullYear();
                    $('#venueStartDate').datepicker('setDate', new Date(year, month, day));
                    $('#venueEndDate').datepicker('option', 'minDate', new Date(year, month, day));
                }
            }
        });
    });

    function showProgressDialog(message) {
        $(".message").html(message);
        $("#progressDialog").dialog(
            {
                modal: true,
                draggable: false,
                resizable: false,
                width: 470,
                dialogClass: 'no-close success-dialog',
            });
    }

    function hideProgressDialog(message) {
        $("#progressDialog").dialog("close");
    }

    function ClearForm() {
        document.location.reload();
    }    
}

$(document).ready(function () {
    $.ajaxSetup({
        contentType: 'application/json'
    });

    ko.validation.configure({
        insertMessages: false,
        decorateElement: true,
        errorElementClass: 'error',
        messagesOnModified: false
    });

    //ko.validation.init({
    //    registerExtenders: true,
    //    messagesOnModified: false,
    //    insertMessages: true,
    //    decorateInputElement: true,
    //    errorElementClass: 'error'
    //});

    //vm = new VenueBooking();
    //ko.applyBindings(vm);
});