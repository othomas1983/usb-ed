﻿/// <reference path="knockout/knockout-3.2.0.js" />
/// <reference path="knockout/knockout.validation.min.js" />
/// <reference path="knockout/knockoutCommon.js" />

var cateringRequirements = function () {
    var self = this;

    var items = ["", "07:30", "07:45", "08:00", "08:15", "08:30", "08:45", "09:00", "09:15", "09:30", "09:45", "10:00", "10:15", "10:30", "10:45", "11:00", "11:15", "11:30", "11:45", "12:00", "12:15", "12:30", "12:45", "13:00", "13:15", "13:30", "13:45", "14:00", "14:15", "14:30", "14:45", "15:00", "15:15", "15:30", "15:45", "16:00", "16:15", "16:30", "16:45", "17:00", "17:15", "17:30", "17:45", "18:00", "18:15", "18:30", "18:45", "19:00", "19:15", "19:30", "19:45", "20:00"]

    self.Package_A = ko.observable();
    self.Package_B = ko.observable();
    self.Package_C = ko.observable();

    self.cateringTimes = ko.observableArray(items);

    self.teaCoffeeSelected = ko.observable();
    self.teaSnackSelected = ko.observable();
    self.tartletsSelected = ko.observable();
    self.biscuitsSelected = ko.observable();
    self.mealOfTheDaySelected = ko.observable();
    self.buffetLunchSelected = ko.observable();
    self.upmarketLunchSelected = ko.observable();
    self.dessertSelected = ko.observable();
    self.dinnerSelected = ko.observable();
    self.cansSelected = ko.observable();
    self.mineralWaterSelected = ko.observable();
    self.juicePerGlassSelected = ko.observable();
    self.jugsOfJuiceSelected = ko.observable();
    self.braai2MeatsSelected = ko.observable();
    self.braai3MeatsSelected = ko.observable();
    self.spitBraaiSelected = ko.observable();
    self.potjieSelected = ko.observable();
    self.cockTailHalfSelected = ko.observable();
    self.cockTailFullSelected = ko.observable();
    self.bowlFoodsSelected = ko.observable();
    self.buffetBreakfastSelected = ko.observable();
    self.fingerBreakfastSelected = ko.observable();
    self.corkageSelected = ko.observable();

    $("#package_a").click(function () {
        $("#package_a input:radio").prop("checked", true);
    });

    $("#package_b").click(function () {
        $("#package_b input:radio").prop("checked", true);
    });

    $("#package_c").click(function () {
        $("#package_c input:radio").prop("checked", true);
    });
}

ko.applyBindings(cateringRequirements());
