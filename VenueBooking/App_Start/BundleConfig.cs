﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace VenueBooking.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            BundleTable.EnableOptimizations = false;

            bundles.Add(new StyleBundle("~/content/style")
                   .Include("~/Content/css/jquery-ui.css",
                   "~/Content/css/containers.css",
                   "~/Content/css/dialog.css",
                   "~/Content/css/fonts/fonts.css",
                   "~/Content/css/Global.css",
                   "~/Content/css/alert.css",
                   "~/Content/css/fullcalendar.css"));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Content/js/jquery-*",
                "~/Content/js/JSLINQ.js",
                "~/Content/js/knockout/knockout-3.2.0.js",
                "~/Content/js/knockout/knockoutCommon.js",
                "~/Content/js/knockout/knockout.validation.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/venueBooking").Include(
                "~/Content/js/view.js",                
                "~/Content/js/venueBookingForm.js"));

            bundles.Add(new ScriptBundle("~/bundles/calendar").Include(
                "~/Content/js/bootstrap.js",
               "~/Content/js/calendar/moment.min.js",
               "~/Content/js/calendar/fullcalendar.js"));
        }
    }
}