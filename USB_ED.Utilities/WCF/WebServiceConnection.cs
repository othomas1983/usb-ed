﻿using System;
using System.ServiceModel;
using System.ServiceModel.Channels;


namespace USB_ED.Utilities.WCF
{
    public class WebServiceConnection<T> : IDisposable where T:class
    {
        public EndpointConfiguration EndpointConfiguration { get; set; }

        public WebServiceConnection(EndpointAddress address, Binding binding)
        {
            Address = address;
            Binding = binding;
        }
       
        
        public WebServiceConnection(string address)
        {
            Binding = new BasicHttpBinding();
            Address = new EndpointAddress(address);
        } 
 

        public WebServiceConnection(EndpointConfiguration endpointConfiguration)
        {
            EndpointConfiguration = endpointConfiguration;
        }


        public WebServiceConnection(string address, Binding binding)
        {
            Binding = binding;
            Address = new EndpointAddress(address);
        }

        public T Proxy
        {
            get { return _proxy ?? (_proxy = GetProxy()); }
        }

        public Binding  Binding { get; private set; }
        public EndpointAddress  Address { get; private set; }
        private ChannelFactory<T> _channelFactory;
        private T _proxy;

        public T GetProxy()
        {
            _channelFactory = EndpointConfiguration != null ? new ChannelFactory<T>(EndpointConfiguration.Name)  : new ChannelFactory<T>(Binding, Address);
            _proxy = _channelFactory.CreateChannel();
            return _proxy;
        }

        public void Dispose()
        {
            if (_channelFactory == null) return;

            _channelFactory.Close();
        }
    }
}