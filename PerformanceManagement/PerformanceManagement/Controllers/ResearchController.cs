﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PerformanceManagement.Application.Common;
using PerformanceManagement.Application.Research;
using PerformanceManagement.Application.Research.ViewModels;
using PerformanceManagement.Core.Cache;
using PerformanceManagement.Domain.Model.Common;
using PerformanceManagement.Domain.Model.Enums;
using PerformanceManagement.Domain.Model.Research;
using PerformanceManagement.Infrastructure.Utilities;
using PerformanceManagement.Infrastructure.Utilities.ExtensionMethods;
using PerformanceManagement.Web.ActionResults;
using PerformanceManagement.Web.Infrastructure.Filters;

namespace PerformanceManagement.Web.Controllers
{

    public class ResearchController : ControllerBase
    {
        public ResearchController( IResearchService service ) : base( service )
        {
        }


        public ActionResult Index()
        {
            return View();
        }

        #region Research Activity

        // GET: Research Activities
        public ActionResult GetResearchActivities()
        {
            var model = Service.GetViewModel( CurrentPerson ) as ResearchVm;

            return new JsonNetResult( model );
        }

        public ActionResult CreateResearch()
        {
            var model = new ResearchHistoryCreateEditVm( CurrentPerson.CVid );

            return View( model );
        }

        [HttpPost]
        public ActionResult SaveResearch( ResearchHistoryCreateEditVm model )
        {
            bool success = Service.SaveChanges( model, CurrentPerson, CurrentUser );

            return new JsonNetResult( new { success } );
        }

        // Post : AddInternalContributor
        public ActionResult AddInternalContributor( string selectedUsernameAndCvid )
        {
            string username;
            string cVid;
            FacultyHelper.SplitUserDetails( selectedUsernameAndCvid, out cVid, out username );

            var model = ( (IResearchService) Service ).GetFacultyUserDetails( cVid.AsInteger() );

            return new JsonNetResult( model );
        }

        // Post : AddExternalContributor
        public ActionResult AddExternalContributor(string selectedUsernameAndCvid)
        {
            string username;
            string cVid;
            FacultyHelper.SplitUserDetails(selectedUsernameAndCvid, out cVid, out username);

            var model = ((IResearchService)Service).GetFacultyUserDetails(cVid.AsInteger());

            return new JsonNetResult(model);
        }

        public ActionResult Edit( int id )
        {
            var model = ( (IResearchService) Service ).GetResearchWithContributors( id, CurrentPerson );

            return View( "EditResearch", model );
        }

        public ActionResult GetResearch( int id )
        {
            var model = ( (IResearchService) Service ).GetResearchWithContributors( id, CurrentPerson );

            return new JsonNetResult( model );
        }

        #endregion

        #region Drop downs

        // GET: DropDowns : ResearchOutputCategories
        public ActionResult GetResearchDropDowns()
        {
            var model = new
            {
                ResearchCategories = DropDownsCache.Read<ResearchOutputCategory>( DropDownType.ResearchOutputCategory ),
                ResearchContributionType = DropDownsCache.Read<ResearchContributionType>( DropDownType.ResearchContributionType ),
                Locus = DropDownsCache.Read<Locus>( DropDownType.Locus ),
                ABSRatingStatus = DropDownsCache.Read<ABSRating>( DropDownType.ABSRating ),
                DHETStatus = DropDownsCache.Read<DHETStatus>( DropDownType.DHETStatus ),
                ResearchStatus = DropDownsCache.Read<ResearchStatus>( DropDownType.ResearchStatus ),
                Countries = DropDownsCache.Read<Nationality>( DropDownType.Nationalities ),
            };

            return new JsonNetResult( model );
        }

        public ActionResult GetPersonRolesDropDown()
        {
            var model = new
            {
                Roles = DropDownsCache.Read<PersonRole>( DropDownType.PersonRole ),
            };

            return new JsonNetResult( model );
        }

        public ActionResult GetExternalContributorDropDowns()
        {
            var model = new
            {
                Affiliations = DropDownsCache.Read<Affiliation>( DropDownType.Affiliations ),
                Roles = DropDownsCache.Read<PersonRole>( DropDownType.PersonRole ),
                Nationalities = DropDownsCache.Read<Nationality>( DropDownType.Nationalities ),
                Genders = new[]
                {
                    new {Id = "Male", Description = "Male"},
                    new {Id = "Female", Description = "Female"}
                }
            };

            return new JsonNetResult( model );
        }

        #endregion

        #region Delete

        // GET: ManagementLoads/Delete/5
        public ActionResult Delete( int id )
        {
            return PartialView( "_Delete", id );
        }

        // POST: ManagementLoads/Delete/5
        [HttpPost]
        [ActionName( "Delete" )]
        public ActionResult DeleteConfirmed( int id )
        {
            bool success = Service.Delete<ResearchHistory>( id, CurrentPerson, CurrentUser );

            return Json( new { success } );
        }
        #endregion

        #region Private Methods

        #endregion
    }
}