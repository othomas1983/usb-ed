﻿using System.Web.Mvc;
using PerformanceManagement.Application.TeachingLoads;
using PerformanceManagement.Application.TeachingLoads.ViewModels;

namespace PerformanceManagement.Web.Controllers
{
    public class TeachingLoadController : ControllerBase
    {
        public TeachingLoadController( ITeachingLoadService service ) : base( service )
        {
        }

        // GET: TeachingLoad
        public ActionResult Index( int? year = null )
        {
            // Filter by Year
            var model = ( (ITeachingLoadService) Service ).FilterViewModel( CurrentPerson, year );
          
            return View( model );
        }

        
        // GET: TeachingLoad/Create
        public ActionResult Create()
        {
            var model = new TeachingLoadItemCreateVm( this.CVid );

            return PartialView( "_Create", model );
        }

        // POST: TeachingLoad/Create
        [HttpPost]
        public ActionResult Create( TeachingLoadItemCreateVm model )
        {
            bool success = Service.SaveChanges( model, CurrentPerson, CurrentUser );
             
            return Json( new { success } );
        }

        // GET: TeachingLoad/Edit/5
        public ActionResult Edit( int id )
        {
            var model = Service.GetItem<TeachingLoadItemEditVm>( id, CurrentPerson );

            return PartialView( "_Edit", model );
        }

        // POST: TeachingLoad/Edit/5
        [HttpPost]
        public ActionResult Edit( int id, FormCollection collection )
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction( "Index" );
            }
            catch
            {
                return View();
            }
        }

        // GET: TeachingLoad/Delete/5
        public ActionResult Delete( int id )
        {
            return View();
        }

        // POST: TeachingLoad/Delete/5
        [HttpPost]
        public ActionResult Delete( int id, FormCollection collection )
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction( "Index" );
            }
            catch
            {
                return View();
            }
        }
    }
}
