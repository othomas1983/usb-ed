﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PerformanceManagement.Application.PerformanceManagement.SupervisorReviewed;
using PerformanceManagement.Application.PerformanceManagement.SupervisorReviewed.ViewModels;
using PerformanceManagement.Domain.Model.PerformanceManagement;
using PerformanceManagement.Web.ActionResults;
using PerformanceManagement.Application.PerformanceManagement.SupervisorEvaluation.ViewModels;
using PerformanceManagement.Core.Security.Identity;
using PerformanceManagement.Infrastructure.Utilities.ExtensionMethods;
using PerformanceManagement.Application.PerformanceManagement.SupervisorEvaluation;

namespace PerformanceManagement.Web.Controllers
{
    public class SupervisorReviewedController : ControllerBase
    {

        protected ISupervisorEvaluationService Service2 { get; set; }
        
        public SupervisorReviewedController( ISupervisorReviewedService service, ISupervisorEvaluationService _service ) : base( service )
        {
            _service =Service2;
        }

        // GET: MyCommitment
        public ActionResult Index()
        {
            var model = Service.GetViewModel(CurrentPerson) as SupervisorReviewedVm;
            return PartialView(model);

        }


        public ActionResult Edit(int cvid, string username)
        {

            var model = Service.GetViewModel(CurrentPerson, CurrentUser) as SupervisorEvaluationVm;

            return PartialView(model);
        }
    
    }
}