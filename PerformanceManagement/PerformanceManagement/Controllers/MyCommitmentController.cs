﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PerformanceManagement.Application.PerformanceManagement.MyCommitment;
using PerformanceManagement.Application.PerformanceManagement.MyCommitment.ViewModels;
using PerformanceManagement.Domain.Model.PerformanceManagement;
using PerformanceManagement.Web.ActionResults;

namespace PerformanceManagement.Web.Controllers
{
    public class MyCommitmentController : ControllerBase
    {
        public MyCommitmentController( IMyCommitmentService service ) : base( service )
        {
        }

        // GET: MyCommitment
        public ActionResult Index()
        {
            var model = Service.GetViewModel( CurrentPerson ) as MyCommitmentVm;

            return View( model );
        }

        // GET: MyCommitment/Create
        public ActionResult Create()
        {
            var model = new MyCommitmentCreateItemVm( this.CVid );

            return PartialView( "_Create", model );
        }

        // POST: MyCommitment/Create
        [HttpPost]
        public ActionResult Create( MyCommitmentCreateItemVm model )
        {
            if ( !ModelState.IsValid ) return Json( new { success = false, errorMessage = this.ErrorMessages }, JsonRequestBehavior.AllowGet );

            bool success = Service.SaveChanges( model, CurrentPerson, CurrentUser );

            // This will send back partial page to be re-rendered to keep user where they were
            if ( success )
            {
                return RedirectToAction( "Index" );
            }

            return new HttpStatusCodeResult( HttpStatusCode.BadRequest );  // 400
        }

        // GET: MyCommitment/Edit/5
        public ActionResult Edit( int id )
        {
            var model = Service.GetItem<MyCommitmenteEditItemVm>( id, CurrentPerson );

            return PartialView( "_Edit", model );
        }

        // POST: MyCommitment/Edit/5
        [HttpPost]
        public ActionResult Edit( MyCommitmenteEditItemVm model )
        {
            if ( !ModelState.IsValid ) return Json( new { success = false, errorMessage = this.ErrorMessages }, JsonRequestBehavior.AllowGet );

            bool success = Service.SaveChanges( model, CurrentPerson, CurrentUser );

            // This will send back partial page to be re-rendered to keep user where they were
            if ( success )
            {
                return RedirectToAction( "Index" );
            }

            return new HttpStatusCodeResult( HttpStatusCode.BadRequest );  // 400
        }

        #region Delete

        // GET: SupervisorEvaluation/Delete/5
        public ActionResult Delete( int id )
        {
            return PartialView( "_Delete", id );
        }

        // POST: SupervisorEvaluation/Delete/5
        [HttpPost]
        [ActionName( "Delete" )]
        public ActionResult DeleteConfirmed( int id )
        {
            bool success = Service.Delete<Commitment>( id, CurrentPerson, CurrentUser );

            // This will send back partial page to be re-rendered to keep user where they were
            if ( success )
            {
                return RedirectToAction( "Index" );
            }

            return new HttpStatusCodeResult( HttpStatusCode.BadRequest );  // 400
        }
        #endregion




        // Json : Get performance categories
        public ActionResult GetPerformanceCategories()
        {
            var model = ( (IMyCommitmentService) Service ).GetPerformanceCategories();

            return new JsonNetResult( model );
        }
    }
}