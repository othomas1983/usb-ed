﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PerformanceManagement.Application.PerformanceManagement.PeerReviewed;
using PerformanceManagement.Application.PerformanceManagement.PeerReviewed.ViewModels;
using PerformanceManagement.Domain.Model.PerformanceManagement;
using PerformanceManagement.Web.ActionResults;
using PerformanceManagement.Application.PerformanceManagement.PeerEvaluation.ViewModels;
using PerformanceManagement.Core.Security.Identity;
using PerformanceManagement.Infrastructure.Utilities.ExtensionMethods;
using PerformanceManagement.Application.PerformanceManagement.PeerEvaluation;

namespace PerformanceManagement.Web.Controllers
{
    public class PeerReviewedController : ControllerBase
    {

       
        
        public PeerReviewedController( IPeerReviewedService service  ) : base( service )
        {
           
        }

        // GET: MyCommitment
        public ActionResult Index()
        {
            var model = Service.GetViewModel(CurrentPerson) as PeerReviewedVm;
            return PartialView(model);

        }

        
        public ActionResult Edit(int cvid, string username)
        {

            var model = Service.GetViewModel(CurrentPerson, CurrentUser) as PeerEvaluationVm;

            return PartialView(model);
        }
    
    }
}