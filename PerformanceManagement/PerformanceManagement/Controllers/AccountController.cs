﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PerformanceManagement.Application.Authentication;
using PerformanceManagement.Application.Authentication.ViewModels;
using PerformanceManagement.Application.Common;
using PerformanceManagement.Infrastructure;
using ControllerBase = PerformanceManagement.Web.Controllers.ControllerBase;
using Microsoft.Owin.Security;
using NLog;

namespace PerformanceManagement.Web.Controllers
{


    public class AccountController : ControllerBase
    {       
        private readonly IAuthService _authService;
        
        public AccountController( IAuthService authService )
        {
            _authService = authService;
        }
        // GET: Account
        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login( LoginVm model, string returnUrl )
        {
            if ( !ModelState.IsValid ) return View( model );

            string errorMessage;
            bool authenticated = _authService.Authenticate( model, out errorMessage );

            if ( authenticated )
            {
                // we are in!
                return RedirectToLocal( returnUrl );
            }

            ModelState.AddModelError( "", errorMessage );
            return View( model );
        }

        public ActionResult LogOut()
        {
            Logger.Info( $"Username:{this.CurrentUser.Username}" );

            Request.GetOwinContext().Authentication.SignOut();
            return Redirect("~/");
        }


        private ActionResult RedirectToLocal( string returnUrl )
        {
            if ( Url.IsLocalUrl( returnUrl ) )
            {
                return Redirect( returnUrl );
            }
            return Redirect( "/" );
        }
    }
}