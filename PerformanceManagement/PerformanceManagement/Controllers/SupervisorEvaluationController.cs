﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Services.Description;
using PerformanceManagement.Application.PerformanceManagement.SupervisorEvaluation.ViewModels;
using PerformanceManagement.Application.PerformanceManagement.SupervisorEvaluation;
using PerformanceManagement.Core.Security.Identity;
using PerformanceManagement.Domain.Model.PerformanceManagement;
using PerformanceManagement.Infrastructure.Utilities.ExtensionMethods;
using System.Web.Script.Serialization;

namespace PerformanceManagement.Web.Controllers
{
    public class SupervisorEvaluationController : ControllerBase
    {
        public SupervisorEvaluationController(ISupervisorEvaluationService service) : base(service)
        {
        }

        public ActionResult SelectUser()
        {
            return PartialView("_SelectUser");
        }

        public ActionResult SelectUserPage()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult SelectUser(string selectedUsernameAndCvid)
        {
            string username;
            string cVid;
            SplitUserDetails(selectedUsernameAndCvid, out cVid, out username);

            // Create Peer Person and save to session
            var supervisorPerson = new CurrentPerson(cVid.AsInteger(), username);
            this.SetSupervisorPerson(supervisorPerson);

            return RedirectToAction("Index/" + cVid);
        }

        // GET: SupervisorEvaluation
        public ActionResult Index(int id, string username)
        {

            if (id == 0 && username == null)
            {
                //var supervisorPerson = this.GetSupervisorPerson();

                //if (supervisorPerson == null) return RedirectToAction("SelectUserPage");

                //var model = Service.GetViewModel(supervisorPerson, CurrentUser) as SupervisorEvaluationVm;

                //return PartialView(model);
                return null;
            }
            else
            {
                var supervisorPerson = new CurrentPerson(id, username);

                this.SetSupervisorPerson(supervisorPerson);
                var supervisorGetPerson = this.GetSupervisorPerson();

                var model = Service.GetViewModel(supervisorGetPerson, CurrentUser) as SupervisorEvaluationVm;

                return PartialView(model);
            }
        }


        // GET: SupervisorEvaluation/Create
        public ActionResult Create(string category)
        {
            var supervisorPerson = this.GetSupervisorPerson();

            var model = ((ISupervisorEvaluationService)Service).GetCreateVm(category, supervisorPerson, CurrentUser);

            return PartialView("_Create", model);
        }

        // POST: SupervisorEvaluation/Create
        [HttpPost]
        public ActionResult Create(SupervisorEvaluationCreateItemVm model)
        {
            var supervisorPerson = this.GetSupervisorPerson();

            bool success = Service.SaveChanges(model, supervisorPerson, CurrentUser);

            // This will send back partial page to be re-rendered to keep user where they were
            if (success)
            {
                //Need to refresh the data or flush it
                return JavaScript("window.close();");

                // return RedirectToAction("../SupervisorReviewed/Index/"+model.CVid);
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);  // 400

        }


        // GET: SupervisorEvaluation/Edit/5
        public ActionResult Edit(int id)
        {
            var supervisorPerson = this.GetSupervisorPerson();

            var model = Service.GetItem<SupervisorEvaluationEditItemVm>(id, supervisorPerson, CurrentUser);

            return PartialView("_Edit", model);
        }

        // POST: SupervisorEvaluation/Edit/5
        [HttpPost]
        public ActionResult Edit(SupervisorEvaluationEditItemVm model)
        {
            var supervisorPerson = this.GetSupervisorPerson();

            bool success = Service.SaveChanges(model, supervisorPerson, CurrentUser);

            //// This will send back partial page to be re-rendered to keep user where they were
            if (success)
            {
                return JavaScript("window.close();");
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);  // 400
        }

        #region Delete

        // GET: SupervisorEvaluation/Delete/5
        public ActionResult Delete(int id)
        {
            return PartialView("_Delete", id);
        }

        // POST: SupervisorEvaluation/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            var supervisorPerson = this.GetSupervisorPerson();

            bool success = Service.Delete<SupervisorEvaluation>(id, supervisorPerson, CurrentUser);

            // This will send back partial page to be re-rendered to keep user where they were
            if (success)
            {
                return JavaScript("window.close();");
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);  // 400
        }
        #endregion

        #region Private Methods
        private void SplitUserDetails(string selectedUsernameAndCvid, out string cVid, out string username)
        {
            var usernameAndCvid = selectedUsernameAndCvid.Split(':');
            username = usernameAndCvid[0];
            cVid = usernameAndCvid[1];
        }


        /// <summary>
        /// Sets the peer person in session
        /// </summary>
        /// <param name="peerPerson">The peer person.</param>
        private void SetSupervisorPerson(CurrentPerson supervisorPerson)
        {
            Session.SetDataInSession<IUser<CurrentPerson>>("_SupervisorPerson", supervisorPerson);
        }
        /// <summary>
        /// Gets the peer person from session.
        /// </summary>
        /// <returns></returns>
        private IUser<CurrentPerson> GetSupervisorPerson()
        {
            return Session.GetDataFromSession<IUser<CurrentPerson>>("_SupervisorPerson");
        }



        #endregion
    }
}