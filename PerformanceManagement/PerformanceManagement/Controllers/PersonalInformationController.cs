﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FacultyManagement.Application.Common;
using FacultyManagement.Application.PersonalInformation;
using FacultyManagement.Application.PersonalInformation.ViewModels;
using FacultyManagement.Core.Security.Identity;
using FacultyManagement.Infrastructure;
using FacultyManagement.Web.Areas.Admin.Controllers;
using FacultyManagement.Web.Infrastructure.Alerts;
using ControllerBase = FacultyManagement.Web.Controllers.ControllerBase;
using System.Net;

namespace FacultyManagement.Web.Controllers
{   
    public class PersonalInformationController : ControllerBase
    {

        public PersonalInformationController( IPersonalInformationService service ) : base( service )
        {            
        }

        // GET: PersonalInformation
        /// Summary: Will Load the CurrentUser or the User that was Loaded from Admin
        /// <seealso cref="UsersController.LoadUser" />
        public ActionResult Index()
        {
            PersonalInformationVm model = null;
            try
            {
                model = Service.GetViewModel( CurrentPerson ) as PersonalInformationVm;
            }
            catch ( SqlException )
            {
                ModelState.AddModelError( "", Messages.DbConnectionError );
            }
            catch ( ObjectNotFoundException )
            {
                ModelState.AddModelError( "", Messages.NoDataError );
            }

            return View( model );
        }


        // GET: PersonalInformation/Edit/5
        //[NationalitySelectListPopulator]    
        public ActionResult Edit()
        {
            PersonalInformationEditVm model = null;
            try
            {
                model = ((IPersonalInformationService) Service).GetEditModel( CurrentPerson ) as PersonalInformationEditVm;
               
              
            }
            catch ( SqlException )
            {
                ModelState.AddModelError( "", Messages.DbConnectionError );
            }
            catch ( ObjectNotFoundException )
            {
                    ModelState.AddModelError( "", Messages.NoDataError );
            }
            ModelState.Clear();
            model.Biography = WebUtility.HtmlEncode(model.Biography);
            return View( model );
        }

        // POST: PersonalInformation/Edit/5
        [HttpPost]       
        public ActionResult Save( PersonalInformationEditVm model )
        {
            try
            {
                if ( !ModelState.IsValid ) return View( "Edit", model );
                //ModelState.Clear();
                //model.Biography = WebUtility.HtmlDecode(model.Biography);
             
                bool success = Service.SaveChanges( model, CurrentPerson, CurrentUser );

                if ( success )
                {
                    return RedirectToAction( "Index" ).WithSuccess( Messages.SavingSuccess );
                }

                return RedirectToAction( "Index" ).WithError( Messages.SavingError );
            }

            catch
            {
                return View( "Edit", model ).WithError( Messages.SavingError );
            }
        }

    }
}
