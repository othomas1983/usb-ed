﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FacultyManagement.Application.Common;
using FacultyManagement.Application.ProfessionalAndPersonalDevelopment;
using FacultyManagement.Application.ProfessionalAndPersonalDevelopment.ViewModels;
using FacultyManagement.Application.ProfessionalAndPersonalDevelopment.ViewModels.ContinuingEducation;
using FacultyManagement.Application.ProfessionalAndPersonalDevelopment.ViewModels.Membership;
using FacultyManagement.Application.ProfessionalAndPersonalDevelopment.ViewModels.NonAcademic;
using FacultyManagement.Domain.Model.ProfessionalAndPersonalDevelopment;
using FacultyManagement.Infrastructure;
using ControllerBase = FacultyManagement.Web.Controllers.ControllerBase;

namespace FacultyManagement.Web.Controllers
{
    public class ProfessionalAndPersonalDevelopmentController : ControllerBase
    {
        public ProfessionalAndPersonalDevelopmentController( IProfessionalAndPersonalDevelopmentService service ) : base( service )
        {
        }

        // GET: ProfessionalAndPersonalDevelopment
        public ActionResult Index()
        {
            ProfessionalAndPersonalDevelopmentVm model = null;
            try
            {
                model = Service.GetViewModel( CurrentPerson ) as ProfessionalAndPersonalDevelopmentVm;
            }
            catch ( ObjectNotFoundException )
            {
                ModelState.AddModelError( "", Messages.NoDataError );
            }

            return View( model );
        }


        #region Membership
        // GET: ProfessionalAndPersonalDevelopment/Create
        public ActionResult CreateMembership()
        {
            var model = new MembershipCreateVm( this.CVid );

            return PartialView( "_CreateMembership", model );
        }

        // POST: ProfessionalAndPersonalDevelopment/Create
        [HttpPost]
        public ActionResult CreateMembership( MembershipCreateVm model )
        {
            if ( !ModelState.IsValid ) return Json( new { success = false, errorMessage = this.ErrorMessages }, JsonRequestBehavior.AllowGet );

            bool success = Service.SaveChanges( model, CurrentPerson, CurrentUser );

            return Json( new { success } );
        }

        // GET: ProfessionalAndPersonalDevelopment/Edit/5
        public ActionResult EditMembership( int id )
        {
            var model = Service.GetItem<MembershipEditVm>( id, CurrentPerson );

            return PartialView( "_EditMembership", model );
        }

        // POST: ProfessionalAndPersonalDevelopment/Edit/5
        [HttpPost]
        public ActionResult EditMembership( MembershipEditVm model )
        {
            if ( !ModelState.IsValid ) return Json( new { success = false, errorMessage = this.ErrorMessages }, JsonRequestBehavior.AllowGet );

            bool success = Service.SaveChanges( model, CurrentPerson, CurrentUser );

            return Json( new { success } );
        }
        #endregion

        #region Continuing Education
        // GET: ProfessionalAndPersonalDevelopment/Create
        public ActionResult CreateContinuingEducation()
        {
            var model = new ContinuingEducationCreateVm( this.CVid );

            return PartialView( "_CreateContinuingEducation", model );
        }

        // POST: ProfessionalAndPersonalDevelopment/Create
        [HttpPost]
        public ActionResult CreateContinuingEducation( ContinuingEducationCreateVm model )
        {
            if ( !ModelState.IsValid ) return Json( new { success = false, errorMessage = this.ErrorMessages }, JsonRequestBehavior.AllowGet );

            bool success = Service.SaveChanges( model, CurrentPerson, CurrentUser );

            return Json( new { success } );
        }

        // GET: ProfessionalAndPersonalDevelopment/Edit/5
        public ActionResult EditContinuingEducation( int id )
        {
            var model = Service.GetItem<ContinuingEducationEditVm>( id, CurrentPerson );

            return PartialView( "_EditContinuingEducation", model );
        }

        // POST: ProfessionalAndPersonalDevelopment/Edit/5
        [HttpPost]
        public ActionResult EditContinuingEducation( ContinuingEducationEditVm model )
        {
            if ( !ModelState.IsValid ) return Json( new { success = false, errorMessage = this.ErrorMessages }, JsonRequestBehavior.AllowGet );

            bool success = Service.SaveChanges( model, CurrentPerson, CurrentUser );

            return Json( new { success } );
        }
        #endregion

        #region Non Academic
        // GET: ProfessionalAndPersonalDevelopment/Create
        public ActionResult CreateNonAcademic()
        {
            var model = new NonAcademicCreateVm( this.CVid );

            return PartialView( "_CreateNonAcademic", model );
        }

        // POST: ProfessionalAndPersonalDevelopment/Create
        [HttpPost]
        public ActionResult CreateNonAcademic( NonAcademicCreateVm model )
        {
            if ( !ModelState.IsValid ) return Json( new { success = false, errorMessage = this.ErrorMessages }, JsonRequestBehavior.AllowGet );

            bool success = Service.SaveChanges( model, CurrentPerson, CurrentUser );

            return Json( new { success } );
        }

        // GET: ProfessionalAndPersonalDevelopment/Edit/5
        public ActionResult EditNonAcademic( int id )
        {
            var model = Service.GetItem<NonAcademicEditVm>( id, CurrentPerson );

            return PartialView( "_EditNonAcademic", model );
        }

        // POST: ProfessionalAndPersonalDevelopment/Edit/5
        [HttpPost]
        public ActionResult EditNonAcademic( NonAcademicEditVm model )
        {
            if ( !ModelState.IsValid ) return Json( new { success = false, errorMessage = this.ErrorMessages }, JsonRequestBehavior.AllowGet );

            bool success = Service.SaveChanges( model, CurrentPerson, CurrentUser );

            return Json( new { success } );
        }
        #endregion

        // GET: ProfessionalAndPersonalDevelopment/Delete/5
        public ActionResult Delete( int id )
        {
            return PartialView( "_Delete", id );
        }

        // POST: ProfessionalAndPersonalDevelopment/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeleteConfirmed( int id )
        {
            bool success = Service.Delete<Development>( id, CurrentPerson, CurrentUser );

            return Json( new { success } );
        }
    }
}
