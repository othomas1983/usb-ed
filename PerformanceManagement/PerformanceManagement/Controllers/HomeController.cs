﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Web;
using System.Web.Mvc;

namespace PerformanceManagement.Web.Controllers
{
    public class HomeController : Controller
    {
        [Authorize]
        public ActionResult PerformanceManagementMenu()
        {
            return View();
        }

        public ActionResult Dashboard()
        {
            return View();
        }

        public ActionResult Index()
        {
            

             return RedirectToAction("Dashboard");

         

            
        }

    }
}