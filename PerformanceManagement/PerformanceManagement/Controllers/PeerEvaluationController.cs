﻿using System.Net;
using System.Web.Helpers;
using System.Web.Mvc;
using PerformanceManagement.Application.PerformanceManagement.PeerEvaluation;
using PerformanceManagement.Application.PerformanceManagement.PeerEvaluation.ViewModels;
using PerformanceManagement.Infrastructure.Utilities.ExtensionMethods;
using PerformanceManagement.Core.Security.Identity;
using PerformanceManagement.Domain.Model.PerformanceManagement;
using PerformanceManagement.Web.ActionResults;

namespace PerformanceManagement.Web.Controllers
{
    public class PeerEvaluationController : ControllerBase
    {
        public PeerEvaluationController( IPeerEvaluationService service ) : base( service )
        {
        }

        public ActionResult SelectUser()
        {
            return PartialView( "_SelectUser" );
        }

        public ActionResult SelectUserPage()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult SelectUser( string selectedUsernameAndCvid )
        {
            string username;
            string cVid;
            SplitUserDetails( selectedUsernameAndCvid, out cVid, out username );

            // Create Peer Person and save to session
            var peerPerson = new CurrentPerson( cVid.AsInteger(), username );
            this.SetPeerPerson( peerPerson );

            return RedirectToAction("Index/" + cVid);
        }

        // GET: PeerEvaluation
        public ActionResult Index(int id, string username)
        {
            if (id == 0 && username == null)
            {
                //var supervisorPerson = this.GetSupervisorPerson();

                //if (supervisorPerson == null) return RedirectToAction("SelectUserPage");

                //var model = Service.GetViewModel(supervisorPerson, CurrentUser) as SupervisorEvaluationVm;

                //return PartialView(model);
                return null;
            }
            else
            {

                var peerPerson = new CurrentPerson(id, username);

                this.SetPeerPerson(peerPerson);
                var peerGetPerson = this.GetPeerPerson();             

                if (peerPerson == null) return RedirectToAction("SelectUserPage");

                var model = Service.GetViewModel(peerGetPerson, CurrentUser) as PeerEvaluationVm;

                return PartialView(model);
            }
        }


        // GET: PeerEvaluation/Create
        public ActionResult Create( string category )
        {
            var peerPerson = this.GetPeerPerson();

            var model = ( (IPeerEvaluationService) Service ).GetCreateVm( category, peerPerson, CurrentUser );

            return PartialView( "_Create", model );
        }

        // POST: PeerEvaluation/Create
        [HttpPost]
        public ActionResult Create( PeerEvaluationCreateItemVm model )
        {
            var peerPerson = this.GetPeerPerson();

            bool success = Service.SaveChanges( model, peerPerson, CurrentUser );

            // This will send back partial page to be re-rendered to keep user where they were
            if ( success )
            {
               // return RedirectToAction( "Index" );
                return JavaScript("window.close();");
            }

            return new HttpStatusCodeResult( HttpStatusCode.BadRequest );  // 400
        }

        // GET: PeerEvaluation/Edit/5
        public ActionResult Edit( int id )
        {
            var peerPerson = this.GetPeerPerson();

            var model = Service.GetItem<PeerEvaluationEditItemVm>( id, peerPerson, CurrentUser );

            return PartialView( "_Edit", model );
        }

        // POST: PeerEvaluation/Edit/5
        [HttpPost]
        public ActionResult Edit( PeerEvaluationEditItemVm model )
        {
            var peerPerson = this.GetPeerPerson();

            bool success = Service.SaveChanges( model, peerPerson, CurrentUser );

            // This will send back partial page to be re-rendered to keep user where they were
            if ( success )
            {
                return JavaScript("window.close();");
            }

            return new HttpStatusCodeResult( HttpStatusCode.BadRequest );  // 400
        }

        #region Delete

        // GET: PeerEvaluation/Delete/5
        public ActionResult Delete( int id )
        {
            return PartialView( "_Delete", id );
        }

        // POST: ManagementLoads/Delete/5
        [HttpPost]
        [ActionName( "Delete" )]
        public ActionResult DeleteConfirmed( int id )
        {
            var peerPerson = this.GetPeerPerson();

            bool success = Service.Delete<PeerEvaluation>( id, peerPerson, CurrentUser );

            // This will send back partial page to be re-rendered to keep user where they were
            if ( success )
            {
                return JavaScript("window.close();");
            }

            return new HttpStatusCodeResult( HttpStatusCode.BadRequest );  // 400
        }
        #endregion

        #region Private Methods
        private void SplitUserDetails( string selectedUsernameAndCvid, out string cVid, out string username )
        {
            var usernameAndCvid = selectedUsernameAndCvid.Split( ':' );
            username = usernameAndCvid[0];
            cVid = usernameAndCvid[1];
        }


        /// <summary>
        /// Sets the peer person in session
        /// </summary>
        /// <param name="peerPerson">The peer person.</param>
        private void SetPeerPerson( CurrentPerson peerPerson )
        {
            Session.SetDataInSession<IUser<CurrentPerson>>( "_PeerPerson", peerPerson );
        }
        /// <summary>
        /// Gets the peer person from session.
        /// </summary>
        /// <returns></returns>
        private IUser<CurrentPerson> GetPeerPerson()
        {
            return Session.GetDataFromSession<IUser<CurrentPerson>>( "_PeerPerson" );
        }

        #endregion
    }
}
