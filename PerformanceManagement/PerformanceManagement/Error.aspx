﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Error.aspx.cs" Inherits="PerformanceManagement.Web.Error" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Performance Management - Error</title>

    <link rel="stylesheet" href="<%= Page.ResolveUrl("~/Content/font-awesome.min.css") %>" />
    <link rel="stylesheet" href="<%= Page.ResolveUrl("~/Content/bootstrap.yeti.css") %>" />
    <link rel="stylesheet" href="<%= Page.ResolveUrl("~/Content/Site.css") %>" />

    <script src="<%= ResolveUrl("~/Scripts/jquery-3.1.1.min.js") %>"> </script>
</head>

<body>
    <form id="form1" runat="server">
        <div id="content">

            <div id="content-box">
                <div class="row">
                    <div class="col-md-12">

                        <asp:Panel ID="pnlSecurity" runat="server" Visible="false">
                            <div class="error-wrap">
                                <h1>Hey...</h1>
                                <h3>We can't let you view this page...</h3>

                                <p class="error-icon danger">
                                    <i class="fa fa-lock"></i>
                                </p>

                                <p>
                                    Unfortunately, you are not authorized to view the page you requested. Please contact
                                    your Rock administrator if you need access to this resource.
                               
                                </p>

                                <p><a onclick="history.go(-1);" class="btn btn-sm btn-primary">Go Back</a></p>
                            </div>
                        </asp:Panel>

                        <asp:Panel ID="pnlException" runat="server" Visible="true">
                            <div class="error-wrap">
                                <h1>That Wasn't Supposed To Happen... </h1>

                                <p class="error-icon warning">
                                    <i class="fa fa-exclamation-triangle"></i>
                                </p>

                                <p>
                                    An error has occurred while processing your request.  Your organization's administrators have 
                                been notified of this problem.
                                </p>

                                <asp:Panel ID="btnHtml" runat="server" Visible="True">
                                    <p><a onclick="history.go(-1);" class="btn btn-sm btn-primary">Go Back</a></p>
                                </asp:Panel>

                                <asp:Panel ID="btnAjax" runat="server" Visible="False">
                                    <p><a onclick="location.reload();" class="btn btn-sm btn-primary">Go Back</a></p>
                                </asp:Panel>

                            </div>

                            <div class="error-details">
                                <asp:Literal ID="lErrorInfo" runat="server"></asp:Literal>
                            </div>
                        </asp:Panel>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
