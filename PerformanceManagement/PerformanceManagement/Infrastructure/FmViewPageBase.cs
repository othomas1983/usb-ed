﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PerformanceManagement.Application.Admin;
using PerformanceManagement.Core.Security.Identity;
using PerformanceManagement.Data.Services;
using PerformanceManagement.Infrastructure.Utilities.ExtensionMethods;
using PerformanceManagement.Web.App_Start;
using StructureMap.Attributes;

namespace PerformanceManagement.Web.Infrastructure
{
    /// <summary>
    /// Adds Faculty Management Properties to the be accessed in views
    /// </summary>
    /// <seealso cref="System.Web.Mvc.WebViewPage" />
    public class FmViewPageBase<T> : WebViewPage<T>
    {
        public IUserService UserService { get; set; }

        public CurrentUser CurrentUser { get; set; }
        public LoadedUser LoadedUser { get; set; }
        public string Username => LoadedUser?.Username ?? CurrentUser.Username;
        public int CVid => LoadedUser?.CVid ?? CurrentUser.CVid;
        public CurrentPerson CurrentPerson
        {
            get
            {
                // Returns the active user
                if ( _currentPerson != null && _currentPerson.CVid == this.CVid )
                {
                    return _currentPerson;
                }

                // Current Person is not yet set
                if ( _currentPerson == null )
                {
                    _currentPerson = Session.GetDataFromSession<CurrentPerson>( "_CurrentPerson" );
                    // Updated Current Person  to the active user
                    if ( _currentPerson?.CVid != this.CVid )
                    {
                        string fullName = UserService.GetFacultyUser( this.CVid ).GetFullName();
                        _currentPerson = new CurrentPerson( this.CVid, this.Username, fullName );
                    }
                    return _currentPerson;
                }

                return null;
            }
            set
            {
                Session.SetDataInSession<CurrentPerson>( "_CurrentPerson", null );

                _currentPerson = value;
                if ( _currentPerson != null )
                {
                    Session.SetDataInSession<CurrentPerson>( "_CurrentPerson", value );
                }
            }
        }
        private CurrentPerson _currentPerson;

        public override void InitHelpers()
        {
            base.InitHelpers();

            // Container is disposed automatically by structureMap            
            var container = StructuremapMvc.StructureMapDependencyScope.Container;

            CurrentUser = container.GetInstance<IUser<CurrentUser>>() as CurrentUser;
            LoadedUser = container.GetInstance<IUser<LoadedUser>>() as LoadedUser;

            UserService = container.GetInstance<IUserService>();
        }


        public override void Execute()
        {
            throw new NotImplementedException();
        }
    }
}