﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace PerformanceManagement.Web.Infrastructure.Alerts
{
    /// <summary>
    /// Extensions to support Alert functionality
    /// </summary>
    public static class AlertExtensions
    {
        const string Alerts = "_Alerts";

        /// <summary>
        /// Gets existing alerts from TempData or creates a new list of alerts if it doesnt exist.
        /// </summary>
        /// <param name="tempData">The temporary data.</param>
        /// <returns></returns>
        public static List<Alert> GetAlerts( this TempDataDictionary tempData )
        {
            if ( !tempData.ContainsKey( Alerts ) )
            {
                tempData[Alerts] = new List<Alert>();
            }

            return (List<Alert>) tempData[Alerts];
        }

        public static ActionResult WithSuccess( this ActionResult result, string message )
        {
            return new AlertDecoratorResult( result, "alert-success", message );
        }

        public static ActionResult WithInfo( this ActionResult result, string message )
        {
            return new AlertDecoratorResult( result, "alert-info", message );
        }

        public static ActionResult WithWarning( this ActionResult result, string message )
        {
            return new AlertDecoratorResult( result, "alert-warning", message );
        }

        public static ActionResult WithError( this ActionResult result, string message )
        {
            return new AlertDecoratorResult( result, "alert-danger", message );
        }
    }
}