﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PerformanceManagement.Web.Infrastructure.ModelMetadata.Filters
{
    public class DateTimeByNameFilter : IModelMetadataFilter
    {
        public void TransformMetadata( System.Web.Mvc.ModelMetadata metadata,
            IEnumerable<Attribute> attributes )
        {
            if ( !string.IsNullOrEmpty( metadata.PropertyName ) && metadata.PropertyName.ToLower().Contains( "Date" ) )
            {
                metadata.DataTypeName = "DateTime";
            }
        }
    }
}