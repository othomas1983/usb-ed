﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace PerformanceManagement.Web.Infrastructure.ModelMetadata.Filters
{
    public class LabelConventionFilter : IModelMetadataFilter
    {
        public void TransformMetadata( System.Web.Mvc.ModelMetadata metadata,
            IEnumerable<Attribute> attributes )
        {
            if ( !string.IsNullOrEmpty( metadata.PropertyName ) &&
                 string.IsNullOrEmpty( metadata.DisplayName ) )
            {
                string stringWithSpaces = GetStringWithSpaces( metadata.PropertyName );
                var splitStringList = stringWithSpaces.Split( ' ' );

                var sb = new StringBuilder();
                for ( int i = 0; i < stringWithSpaces.Split( ' ' ).Length; i++ )
                {
                    // First word has Title case
                    if ( i == 0 )
                    {
                        sb.Append( splitStringList[i] );
                        continue;
                    }

                    // lower case all remaining words
                    if ( i > 0 )
                    {
                        sb.Append( $" {splitStringList[i].ToLower()}" );
                    }
                }

                metadata.DisplayName = sb.ToString();
            }
        }

        private string GetStringWithSpaces( string input )
        {
            return Regex.Replace(
                input,
                "(?<!^)" +
                "(" +
                "  [A-Z][a-z] |" +
                "  (?<=[a-z])[A-Z] |" +
                "  (?<![A-Z])[A-Z]$" +
                ")",
                " $1",
                RegexOptions.IgnorePatternWhitespace );
        }
    }
}