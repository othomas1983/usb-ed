﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace PerformanceManagement.Web.Helpers
{
    public static class BootstrapHelpers
    {
        /// <summary>
        /// Bootstraps the label for.
        /// </summary>
        /// <typeparam name="TModel">The type of the model.</typeparam>
        /// <typeparam name="TProp">The type of the property.</typeparam>
        /// <param name="helper">The helper.</param>
        /// <param name="property">The property.</param>
        /// <param name="columns"></param>
        /// <param name="required">if set to <c>true</c> [required].</param>
        /// <returns></returns>
        public static IHtmlString BootstrapLabelFor<TModel, TProp>( this HtmlHelper<TModel> helper,
            Expression<Func<TModel, TProp>> property, int? columns = null, bool required = false )
        {
            string css = "control-label";

            if ( columns.HasValue )
            {
                css = css + $" col-md-{columns.Value}";
            }

            if ( required )
            {
                return helper.LabelFor( property, new {@class = css + " required"} );
            }

            return helper.LabelFor( property, new {@class = css} );
        }

        public static IHtmlString BootstrapLabel( this HtmlHelper helper, string propertyName, int? columns = null,
            bool required = false )
        {
            string css = "control-label";


            if ( columns.HasValue )
            {
                css = css + $" col-md-{columns.Value}";
            }

            if ( required )
            {
                return helper.Label( propertyName, new {@class = css + " required"} );
            }

            return helper.Label( propertyName, new {@class = css} );
        }


        public static IHtmlString BootstrapValidationMessageFor<TModel, TProp>( this HtmlHelper<TModel> helper,
            Expression<Func<TModel, TProp>> property, string message = "" )
        {
            return helper.ValidationMessageFor( property, message, new {@class = "text-danger"} );
        }
    }
}