﻿using System.Web;
using System.Web.Mvc;
using PerformanceManagement.Web.Infrastructure.Filters;

namespace PerformanceManagement.Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters( GlobalFilterCollection filters )
        {
            filters.Add( new AuthorizeAttribute() );
            filters.Add( new AjaxExceptionFilter() );
        }
    }
}
