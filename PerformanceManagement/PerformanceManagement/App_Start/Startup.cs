﻿using PerformanceManagement.Core.Security.Authentication;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(PerformanceManagement.Startup))]

namespace PerformanceManagement
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=316888
            AuthSetup.ConfigureAuth(app);
        }
    }
}
