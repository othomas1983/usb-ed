﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PerformanceManagement.Infrastructure.Utilities;
using StructureMap;


namespace PerformanceManagement.Web.DependencyResolution
{
    public class StartupRegistry : Registry
    {
        public StartupRegistry()
        {
            Scan( scan =>
            {
                scan.AssembliesFromApplicationBaseDirectory(
                    a => a.FullName.StartsWith( "PerformanceManagement" ) );                
                scan.AddAllTypesOf<IRunAtStartup>();
            } );
        }

       
    }
}