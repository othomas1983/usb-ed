﻿using System.Web;
using PerformanceManagement.Web.App_Start;
using StructureMap;

namespace PerformanceManagement.Web.DependencyResolution
{
    public static class ContainerPerRequestExtensions
    {
        public static IContainer GetNestedContainer( this HttpContextBase context )
        {
            return StructuremapMvc.StructureMapDependencyScope.CurrentNestedContainer;
        }
    }
}