﻿(function() {
    "use strict";

    angular
        .module( "PerformanceManagement" )
        .controller("InternalContributorCreateModalCtrl",  internalContributorCreateModalCtrl);

    function internalContributorCreateModalCtrl( $http, $uibModalInstance, items ) {
        var vmodal = this;
        var dataService = $http;

        vmodal.items = items;
        vmodal.contributor = {};

        console.log(items);             

        vmodal.selectedRole = {
            id: 0,
            description: ""
        };
        vmodal.Roles = [];
        vmodal.SelectedUser = "";
        vmodal.Sequence = "";
        vmodal.Credit = "";
        vmodal.selectedRole = null;
        vmodal.contributor = {};

        // return a value
        vmodal.ok = function () {

            console.log(vmodal.SelectedUser);

            var facultyUser = null;

             facultyUser = dataService.get("/PerformanceManagement/Research/AddInternalContributor/?selectedUsernameAndCvid=" + vmodal.SelectedUser)
                .then(function (result) {
                    console.log(result.data);
                    // Faculty Management User is returned
                    facultyUser = result.data;  

                    // Construct contributor detail object which will be passed back
                    vmodal.contributor = {                        
                        cvid: facultyUser.cVid,
                        author: facultyUser.fullName,
                        sequence: vmodal.Sequence,
                        dhetCredit: vmodal.Credit,
                        institution: facultyUser.institution !== null ? facultyUser.institution : "",
                        role: vmodal.selectedRole.description,
                        roleId: vmodal.selectedRole.id,
                        type : "Internal"
                    };   

                    console.log( vmodal.contributor );
                    // Send details back to be displayed in parent view
                    $uibModalInstance.close({ contributor: vmodal.contributor });
                });                                                               
        };

        vmodal.cancel = function() {
            $uibModalInstance.dismiss( "cancel" );
        };


        populateDropDowns();

        function populateDropDowns() {
            dataService.get("/PerformanceManagement/Research/GetPersonRolesDropDown")
                .then(function (response) {

                    console.log(response.data);

                    vmodal.Roles = response.data.roles;
                });
        }                    

    }

}());