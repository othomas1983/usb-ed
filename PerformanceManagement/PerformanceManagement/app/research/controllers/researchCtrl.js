﻿(function () {
    "use strict";

    angular
        .module("PerformanceManagement")
        .controller("ResearchCtrl", researchCtrl);


    function researchCtrl($http, NgTableParams) {
        var vm = this;
        var dataService = $http;
     
        // Variables
        vm.Type = "";
        vm.Title = "";
        vm.PublicationName = "";
        vm.PublicationStanding = "";
        vm.Status = "";
        vm.StatusDate = "";
        vm.Credit = "";

        vm.activities = [];

        vm.activitiesTotal = 0;

        vm.editResearch = function( id ) {
            dataService.get("/PerformanceManagement/GetResearchActivities/Research/Edit/?id=" + id);
        };

        getResearchActivities();   
     //   calculateTotal();

        // Functions
        function getResearchActivities() {
            dataService.get("/PerformanceManagement/Research/GetResearchActivities")
                .then(function (response) {
                    
                    vm.activities = response.data.activities;

                    vm.tableParams = new NgTableParams({page:1, count:vm.activities.length }, { dataset: vm.activities });

                    angular.forEach(vm.activities, function (activity) {
                        vm.activitiesTotal += activity.credit;
                    });

                    console.log(vm.activities);
                });
        }
        
        //function calculateTotal() {
        //    dataService.get("/PerformanceManagement/Research/GetResearchActivities")
        //        .then(function (response) {

        //            vm.activities = response.data.activities;

        //            var total = 0;
        //            for (var i = 0; i < vm.activities.length; i++)
        //            {
        //                total += vm.activities[i].credit;
        //            }  
        //            alert(total);
        //            return total;
                

        //        });

        //}

    }


}());