﻿// The autocomplete element
$(".faculty-user-search").autocomplete({
    minLength: 3,
    source: function (request, response) {
        var url = "/PerformanceManagement/Admin/Users/AutoComplete";
        $.getJSON(url, { term: request.term }, function (data) {
            response(data);
        });
    },
    // The the element we want to update
    appendTo: $("#searchresults"),
    // This will override the label
    // and set custom input to be sent to controller with another id
    select: function (e, ui) {
        e.preventDefault() // <--- Prevent the value from being inserted.
        $("#selected_user").val(ui.item.value); // <--- custom input - we set another value

        $(this).val(ui.item.label); // <---  the search input box display text
    }
});