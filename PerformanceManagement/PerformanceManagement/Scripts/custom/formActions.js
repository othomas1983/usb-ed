﻿// Handles form submission
$("form").submit(function () {

    ResetValidationSummary();

    var validated = ValidateDates();

    if (!validated) {
        $("#MessageToClient").html("&bull;  Start Date must be before the End Date.");
        return false;
    }

    if ($("form").valid()) {
        $.ajax({
            url: this.action,
            type: this.method,
            data: $(this).serialize(),
            success: function (result) {
                if (result.success) {
                    $("#modal-container").modal("hide");
                    location.reload();
                } else {
                    if (result.errorMessage) {
                        $("#MessageToClient").text(result.errorMessage);
                    } else {
                        $("#MessageToClient").html("<div class='alert alert-danger'> <i class='fa fa-exclamation-triangle fa-2x'> </i> " +
                            " An error has occurred while processing your request. <p> Your organization's administrators have been notified of this problem.</p></div>");
                    }
                }
            },
            error: function () {
                $("#MessageToClient").html("<div class='alert alert-danger'> <i class='fa fa-exclamation-triangle fa-2x'> </i> " +
                    " An error has occurred while processing your request. <p> Your organization's administrators have been notified of this problem. </p></div>");
            }
        });
        return false;
    }

});

function ValidateDates() {
    var startDate = new Date($('#StartDate').val());
    var endDate = new Date($('#EndDate').val());

    if (startDate != null && endDate != null) {
        if (endDate < startDate) {
            return false;
        }
    }
    return true;
}

function ResetValidationSummary() {
    // Clears validation summary
    $('.validation-summary-errors').each(function () {
        $(this).html("<ul><li style='display:none'></li></ul>");
    })
} 