﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace PerformanceManagement.Web.ActionResults
{
    /// <summary>
    /// Json.NET implementation of Json Action Result with camel casing for javascript
    /// </summary>    
    public class JsonNetResult : ActionResult
    {

        readonly JsonSerializerSettings _jsonSerializerSettings = new JsonSerializerSettings
        {
            ContractResolver = new CamelCasePropertyNamesContractResolver()
        };

        #region Properties

        public object Data { get; set; } 

        #endregion

        public JsonNetResult( object data )
        {
            Data = data;
        }

        /// <summary>
        /// Enables processing of the result of an action method by a custom type that inherits from the <see cref="T:System.Web.Mvc.ActionResult" /> class.
        /// </summary>
        /// <param name="context">The context in which the result is executed. The context information includes the controller, HTTP content, request context, and route data.</param>
        public override void ExecuteResult( ControllerContext context )
        {
            var response = context.HttpContext.Response;

            response.ContentType = "application/json";
            
            response.Write( JsonConvert.SerializeObject( Data, _jsonSerializerSettings ) );
        }
    }
}