﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using PerformanceManagement.Application.Admin;
using PerformanceManagement.Application.Admin.ViewModels;
using PerformanceManagement.Core.Security.Authentication;
using PerformanceManagement.Core.Security.Identity;
using PerformanceManagement.Infrastructure.Utilities;
using PerformanceManagement.Infrastructure.Utilities.ExtensionMethods;
using PerformanceManagement.Web.ActionResults;
using PerformanceManagement.Infrastructure;
using ControllerBase = PerformanceManagement.Web.Controllers.ControllerBase;
using PerformanceManagement.Web.Infrastructure;

namespace PerformanceManagement.Web.Areas.Admin.Controllers
{
    public class UsersController : Web.Controllers.ControllerBase
    {
        private readonly IAdminService _service;
        private readonly IAuthentication _authenticator;

        public UsersController( IAdminService service, IAuthentication authenticator )
        {
            _service = service;
            _authenticator = authenticator;
        }

        // GET: Admin/Users
        public ActionResult Autocomplete( string term , bool includeCurrentUser = false )
        {
            var model = _service.NameAutoComplete( term, CurrentUser, includeCurrentUser );

            return Json( model, JsonRequestBehavior.AllowGet );
        }

        #region Load User
        public ActionResult LoadUser()
        {
            return PartialView( "_LoadUsers" );
        }

        // POST: Admin/Users/LoadUser/5
        [HttpPost]
        public ActionResult LoadUser( string selectedUsernameAndCvid )
        {
            string username;
            string cVid;
            FacultyHelper.SplitUserDetails( selectedUsernameAndCvid, out cVid, out username );

            SetLoadedUser( cVid, username );

            return Json( new { success = true } );
        } 
        #endregion

        // GET: Admin/Users/ResetUser
        public ActionResult ResetUser()
        {
            Session["_LoadedUser"] = null;

            return RedirectToAction( "Index", "PeerEvaluation", new { area = "" } );
        }

        // GET: Admin/Users/AddUser
        #region Add User
        public ActionResult AddUser()
        {
            var model = _service.GetAddUserViewModel( CurrentUser );

            return PartialView( "_AddUsers", model );
        }
        // POST: Admin/Users/Create
        [HttpPost]
        public ActionResult AddUser( List<string> selectedEmployeeIds )
        {
            AddUsersResult result = _service.AddUsers( selectedEmployeeIds, CurrentPerson );

            // Set the loaded user so we can redirect to their profile later
            if ( result.Success && result.FacultyUser != null )
            {
                SetLoadedUser( result.FacultyUser.CVid.ToString(), result.FacultyUser.SamAccountName );
            }

            return new JsonNetResult( new { success = result.Success } );
        } 
        #endregion

        #region Add / Remove Manager
        public ActionResult AddManager()
        {
            var model = _service.GetAddManagerViewModel( CurrentUser ) as AddRemoveManagerVm;

            return PartialView( "_AddRemoveManager", model );
        }

        [HttpPost]
        public ActionResult AddManager( AddRemoveManagerVm model )
        {
            if ( !ModelState.IsValid ) return Json( new { success = false, errorMessage = this.ErrorMessages }, JsonRequestBehavior.AllowGet );

            try
            {
                _service.AddManagers( model.SelectedSamAccountNames );
            }
            catch ( Exception )
            {
                return new JsonNetResult( new { success = false } );
            }

            return new JsonNetResult( new { success = true } );
        }

        public ActionResult RemoveManager()
        {
            var model = _service.GetRemoveManagerViewModel( CurrentUser ) as AddRemoveManagerVm;

            return PartialView( "_AddRemoveManager", model );
        }

        [HttpPost]
        public ActionResult RemoveManager( AddRemoveManagerVm model )
        {
            if ( !ModelState.IsValid ) return Json( new { success = false, errorMessage = this.ErrorMessages }, JsonRequestBehavior.AllowGet );

            try
            {
                _service.RemoveManagers( model.SelectedSamAccountNames );
            }
            catch ( Exception )
            {
                return new JsonNetResult( new { success = false } );
            }

            return new JsonNetResult( new { success = true } );
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Creates and Sets the loaded user in session.
        /// </summary>
        /// <param name="cvId">The cv identifier.</param>
        /// <param name="username">The username.</param>
        private void SetLoadedUser( string cvId, string username )
        {
            IPrincipal principal = _authenticator.CreatePrincipal( cvId, username );
            // Create loaded user and save to session
            var loadedUser = new LoadedUser( principal );
            Session.SetDataInSession<IUser<LoadedUser>>( "_LoadedUser", loadedUser );
        }

        #endregion
    }
}
