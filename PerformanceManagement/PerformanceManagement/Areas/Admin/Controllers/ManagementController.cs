﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using PerformanceManagement.Application.Admin;
using PerformanceManagement.Application.Admin.ViewModels;
using PerformanceManagement.Application.Admin.ViewModels.Common;
using PerformanceManagement.Core.Security.Identity;
using PerformanceManagement.Domain.ExtensionMethods;
using PerformanceManagement.Infrastructure.Utilities;
using PerformanceManagement.Infrastructure.Utilities.ExtensionMethods;
using PerformanceManagement.Infrastructure;
using ControllerBase = PerformanceManagement.Web.Controllers.ControllerBase;
using System.Net;

namespace PerformanceManagement.Web.Areas.Admin.Controllers
{
    [RouteArea("Admin")]
    public class ManagementController : ControllerBase
    {

        public ManagementController(IAdminService service) : base(service)
        {
        }
        public ActionResult Index()
        {


            return PartialView("_Classifications");
        }

        #region Departments

        // GET: Admin/Departments
        public ActionResult AddOrganisation()
        {
            var model = ((IAdminService)Service).GetSelectUserViewModel() as UserSelectBaseVm;

            return View(model);
        }

        [HttpPost]
        public ActionResult LoadUserOrganisations(string selectedUsernameAndCvid)
        {
            string username;
            string cVid;
            FacultyHelper.SplitUserDetails(selectedUsernameAndCvid, out cVid, out username);

            //Stop partial view page repeating
            if (Request.IsAjaxRequest())
            {
                var model = ((IAdminService)Service).GetOrganisations(cVid.AsInteger());

                return PartialView("_Organisations", model);
            }


            return Json(new { success = true });
        }

        [HttpPost]
        public ActionResult CreateUserOrganisations(OrganisationAndRolesVm model)
        {
            if (!ModelState.IsValid) return PartialView("_Organisations", model);

            bool success = Service.SaveChanges(model, new CurrentPerson(model.CVid.Value, model.Username), CurrentUser);

            return Json(new { success });
        }
        #endregion

        #region Classification & Sufficiency

        // GET: Admin/AddClassification
        public ActionResult AddClassification()
        {
            var model = ((IAdminService)Service).GetSelectUserViewModel() as UserSelectBaseVm;

            return View(model);
        }


        [HttpPost]
        public ActionResult LoadUserClassifications(CurrentPerson current)
        {

            var model = ((IAdminService)Service).GetClassificationsAndSufficiencies(current.CVid, 0);



            return PartialView("_Classifications", model);

        }

        [HttpPost]
        public ActionResult CreateUserClassifications(int classificationId, string type)
        {
            if (type == "load")
            {
                var model = ((IAdminService)Service).GetClassificationsAndSufficiencies(CurrentPerson.CVid, 0);


                return Json(new { model.ClassificationId });

            }
            else if (type == "save")
            {
                var model = ((IAdminService)Service).GetClassificationsAndSufficiencies(CurrentPerson.CVid, classificationId);
                bool success = true;
                return Json(new { success });
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);  // 400
        }



        #endregion

        #region Executive profile 

        public ActionResult EditExecutiveProfile()
        {
            var model = ((IAdminService)Service).GetExecutiveProfile(CurrentPerson);

            return PartialView("_EditExecutiveProfile", model);
        }

        [HttpPost]
        public ActionResult EditExecutiveProfile(ExecutiveProfileVm model)
        {
            bool success = Service.SaveChanges(model, CurrentPerson, CurrentUser);

            return Json(new { success });
        }

        public ActionResult LogOut()
        {
            Logger.Info($"Username:{this.CurrentUser.Username}");

            Request.GetOwinContext().Authentication.SignOut();
            return Redirect("/PerformanceManagement");
        }


        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return Redirect("/");
        }
        #endregion
    }
}

