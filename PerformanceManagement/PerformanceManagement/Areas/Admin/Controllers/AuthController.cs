﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PerformanceManagement.Application.Admin;
using PerformanceManagement.Application.Admin.ViewModels.Common;
using PerformanceManagement.Infrastructure.Utilities;
using PerformanceManagement.Infrastructure.Utilities.ExtensionMethods;
using ControllerBase = PerformanceManagement.Web.Controllers.ControllerBase;

namespace PerformanceManagement.Web.Areas.Admin.Controllers
{
    public class AuthController : ControllerBase
    {
        public AuthController( IAdminService service ) : base( service )
        {
        }

        // GET : AddAuth
        public ActionResult AddAuth()
        {
            var model = ( (IAdminService) Service ).GetSelectUserViewModel() as UserSelectBaseVm;

            return View( model );
        }

        [HttpPost]
        public ActionResult LoadUserAuths( string selectedUsernameAndCvid )
        {
            string username;
            string cVid;
            FacultyHelper.SplitUserDetails( selectedUsernameAndCvid, out cVid, out username );

            //Stop partial view page repeating
            if ( Request.IsAjaxRequest() )
            {
                var model = ( (IAdminService) Service ).GetAuths( cVid.AsInteger() );

                return PartialView( "_Auths", model );
            }


            return Json( new { success = true } );
        }

    }
}