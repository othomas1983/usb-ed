﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PerformanceManagement.Infrastructure.Utilities.ExtensionMethods;
using NLog;

namespace PerformanceManagement.Web
{
    public partial class Error : System.Web.UI.Page
    {
        /// <summary>
        /// Handles the Init event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void Page_Init( object sender, EventArgs e )
        {

        }

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        protected void Page_Load( object sender, EventArgs e )
        {
            try
            {
                string errorType = Request["type"];
                if ( string.IsNullOrWhiteSpace( errorType ) )
                {
                    errorType = "exception";
                }

                if ( errorType.Equals( "security", StringComparison.OrdinalIgnoreCase ) )
                {
                    ShowSecurityError();
                }
                else
                {
                    ShowException();
                }
            }
            catch
            {
                // intentionally ignore exception
            }
        }

        /// <summary>
        /// Shows the security error.
        /// </summary>
        private void ShowSecurityError()
        {
            pnlSecurity.Visible = true;
            pnlException.Visible = false;
        }

        /// <summary>
        /// Shows the exception.
        /// </summary>
        private void ShowException()
        {
            Exception ex = GetSavedValue( "FMLastException" ) as Exception;
            if ( ex != null )
            {
                pnlSecurity.Visible = false;
                pnlException.Visible = true;


                ClearSavedValue( "FMLastException" );

                bool showDetails = false;

                try
                {
                    // check to see if the user is an admin, if so allow them to view the error details                  
                    showDetails = User != null && User.IsInRole( "Admin" );
                }
                catch
                {
                    // ignore
                }


                if ( ex is HttpRequestValidationException )
                {
                    lErrorInfo.Text = "<h3>Exception Log:</h3>";
                    lErrorInfo.Text += "<div class=\"alert alert-danger\">";
                    lErrorInfo.Text += "<h4>Invalid Content</h4>";
                    lErrorInfo.Text += "<p>One or more of the fields contained invalid characters. Please make sure that your entries do not contain any angle brackets like &lt; or &gt;.";
                    lErrorInfo.Text += "</div>";
                }
                else
                {
                    if ( showDetails )
                    {
                        lErrorInfo.Text = "<h3>Exception Log:</h3>";
                        ProcessException( ex, " " );
                    }
                }

                if ( Request.Headers["X-Requested-With"] == "XMLHttpRequest" )
                {
                    btnHtml.Visible = false;
                    btnAjax.Visible = true;
                    Response.Clear();
                    Response.StatusCode = HttpStatusCode.BadRequest.ConvertToInt();
                }   

            }
        }

        /// <summary>
        /// Processes the exception.
        /// </summary>
        /// <param name="ex">The ex.</param>
        /// <param name="exLevel">The ex level.</param>
        private void ProcessException( Exception ex, string exLevel )
        {
            lErrorInfo.Text += "<div class=\"alert alert-danger\">";
            lErrorInfo.Text += "<h4>" + exLevel + ex.GetType().Name + " in " + HttpUtility.HtmlEncode( ex.Source ) + "</h4>";
            lErrorInfo.Text += "<p><strong>Message</strong><br>" + HttpUtility.HtmlEncode( ex.Message ) + "</p>";
            lErrorInfo.Text += "<p><strong>Stack Trace</strong><br><pre>" + HttpUtility.HtmlEncode( ex.StackTrace ) + "</pre></p>";
            lErrorInfo.Text += "</div>";

            // check for inner exception
            if ( ex.InnerException != null )
            {
                ProcessException( ex.InnerException, "-" + exLevel );
            }
        }

        /// <summary>
        /// Gets the saved value.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        private object GetSavedValue( string key )
        {
            object item = null;
            if ( Context.Session != null )
            {
                item = Context.Session[key];
            }

            if ( item == null )
            {
                item = Context.Cache[key];
            }

            return item;
        }

        /// <summary>
        /// Clears the saved value.
        /// </summary>
        /// <param name="key">The key.</param>
        private void ClearSavedValue( string key )
        {
            if ( Context.Session != null )
            {
                Context.Session.Remove( key );
            }

            Context.Cache.Remove( key );
        }
    }
}