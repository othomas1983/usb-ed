﻿using System;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;
using PerformanceManagement.Infrastructure.Utilities.ExtensionMethods;

namespace PerformanceManagement.Core.Security.Identity
{
   
    /// <summary>
    /// The Current Logged In User
    /// </summary>
    public class CurrentUser : User, IUser<CurrentUser>
    {
        /// <summary>
        /// IPrincipal is injected from HttpContext.Current.User
        /// </summary>
        /// <param name="principal">The principal (injected).</param>
        public CurrentUser( IPrincipal principal )
        {            
            Principal = principal;
        }
    }
}
