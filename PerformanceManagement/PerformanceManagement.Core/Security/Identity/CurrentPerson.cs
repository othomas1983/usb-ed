﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceManagement.Core.Security.Identity
{
    /// <summary>
    /// The Currently Active user which is either the Loaded User or the Current User.
    /// </summary>    
    public class CurrentPerson : IUser<CurrentPerson>
    {
        public string Username { get; }
        public int CVid { get; }
        public string FullName { get; }

        public CurrentPerson(int cVid, string username)
        {
            CVid = cVid;
            Username = username;
        }

        public CurrentPerson( int cVid, string username, string fullname )
        {
            CVid = cVid;
            Username = username;
            FullName = fullname;
        }
    }
}
