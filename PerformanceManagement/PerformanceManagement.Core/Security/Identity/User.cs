﻿using System.Security.Claims;
using System.Security.Principal;
using PerformanceManagement.Infrastructure.Utilities.ExtensionMethods;

namespace PerformanceManagement.Core.Security.Identity
{
    /// <summary>
    /// Base User class with function implementations
    /// </summary>
    public abstract class User
    {
        protected IPrincipal Principal;
        private ClaimsPrincipal ClaimsPrincipal => Principal as ClaimsPrincipal;

        public virtual string Username
        {
            get
            {
                string username = ClaimsPrincipal?.FindFirst( ClaimTypes.Name )?.Value;
                return username;
            }
        }

        public virtual int CVid
        {
            get
            {
                int? cvid = ClaimsPrincipal?.FindFirst( CustomClaimTypes.CVid )?.Value.AsInteger();

                return cvid.Value;
            }
        }
    }

}
