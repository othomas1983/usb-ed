﻿using System;
using System.Configuration;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using PerformanceManagement.Core.Security.Identity;
using PerformanceManagement.Data.Interfaces;
using PerformanceManagement.Domain;
using PerformanceManagement.Domain.Model;
using PerformanceManagement.Infrastructure;
using Microsoft.Owin.Security;
using NLog;

namespace PerformanceManagement.Core.Security.Authentication
{
    /// <summary>
    /// Active Directory implementation of IAuthentication
    /// IAuthenticationManager - Owin AuthManager (injected)
    /// </summary>
    /// <seealso cref="PerformanceManagement.Core.Security.Authentication.IAuthentication" />
    public class ActiveDirectory : IAuthentication
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly IAuthenticationManager _authenticationManager;
        private readonly IAuthDbService _dbService;

        #region Properties
        public string Username { get; private set; }
        public string Domain { get; private set; } = ConfigurationManager.AppSettings["ActiveDirectoryDomain"];
        public string Server { get; set; } = ConfigurationManager.AppSettings["ActiveDirectoryServer"];
        #endregion

        public ActiveDirectory( IAuthenticationManager authenticationManager, IAuthDbService dbService )
        {
            _authenticationManager = authenticationManager;
            _dbService = dbService;
        }

        /// <summary>
        /// Authenticates the specified user.
        /// </summary>
        /// <returns></returns>
        public bool Authenticate( string username, string password, out string errorMessage )
        {
            PrincipalContext context;
            bool isValidated = false;
            bool isAdmin = false;
            errorMessage = Messages.LoginError;

            Username = username;
            if ( username.Contains( @"\" ) )
            {
                Domain = username.Split( '\\' )[0];
                Username = username.Split( '\\' )[1];
            }

            /* context = new PrincipalContext( ContextType.Machine );
              isValidated = context.ValidateCredentials( username, password );
              isAdmin = true;*/

            try
            {
                context = new PrincipalContext( ContextType.Domain, Server, username, password );

                using ( context )
                {
                    isValidated = context.ValidateCredentials( Username, password );
                    if ( isValidated )
                    {
                        var userPrincipal = UserPrincipal.FindByIdentity( context, IdentityType.SamAccountName, username );
                        var groups = userPrincipal?.GetGroups();

                        if ( groups != null && groups.Any( g => g.Name.Equals( ADGroups.Admin ) ) )
                        {
                            isAdmin = true;
                        }
                    }
                }
            }
            catch ( Exception ex )
            {
                Logger.Error( $"Login Exception:{ex.StackTrace} for Username:{username}" );
                return false;
            }

            string domainAndUsername = Username;
            if ( !string.IsNullOrWhiteSpace( Domain ) )
            {
                domainAndUsername = $@"{Domain}\{Username}";
            }

            int cVid;
            bool userExists = _dbService.CheckUserExists( domainAndUsername, out cVid );

            Logger.Info( userExists ? $"Username:{username} found in FM, CVid:{cVid}" : $"Username:{username} not found in FM" );


            if ( isValidated && !userExists )
            {
                errorMessage = Messages.UserNotExists;
                return false;
            }


            bool isAuthenticated = isValidated && userExists;
            if ( isAuthenticated )
            {
                //var userPrincipal = UserPrincipal.FindByIdentity( context, username );
                var identity = CreateIdentity( domainAndUsername, cVid, isAdmin );

                _authenticationManager.SignOut( CustomAuthentication.ApplicationCookie );
                _authenticationManager.SignIn( new AuthenticationProperties { IsPersistent = true }, identity );
            }

            Logger.Info( $"Authenticated:{isAuthenticated}, Username:{domainAndUsername}" );

            return isAuthenticated;
        }

        /// <summary>
        /// Changes the password.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="oldPassword">The old password.</param>
        /// <param name="newPassword">The new password.</param>
        /// <param name="warningMessage">The warning message.</param>
        /// <returns></returns>
        public bool ChangePassword( UserLogin user, string oldPassword, string newPassword, out string warningMessage )
        {
            warningMessage = null;
            string username = user.Username;
            if ( !string.IsNullOrWhiteSpace( Domain ) )
            {
                username = $@"{Domain}\{user.Username}";
            }

            var context = new PrincipalContext( ContextType.Domain, Server );
            using ( context )
            {
                var userPrincipal = UserPrincipal.FindByIdentity( context, username );
                if ( userPrincipal != null )
                {
                    try
                    {
                        userPrincipal.ChangePassword( oldPassword, newPassword );
                        return true;
                    }
                    catch ( PasswordException pex )
                    {
                        warningMessage = pex.Message;
                        return false;
                    }
                }
            }

            return false;
        }

        public IPrincipal CreatePrincipal( string cVid, string username )
        {
            var identity = new ClaimsIdentity();
            identity.AddClaim( new Claim( CustomClaimTypes.CVid, cVid ) );
            identity.AddClaim( new Claim( ClaimTypes.Name, username ) );

            var principal = new ClaimsPrincipal( identity );

            return principal;
        }


        /// <summary>
        /// Creates the claims identity.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="cVid"></param>
        /// <returns></returns>
        private ClaimsIdentity CreateIdentity( string username, int cVid, bool isAdmin )
        {
            var identity = new ClaimsIdentity( CustomAuthentication.ApplicationCookie, ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType );
            identity.AddClaim( new Claim( "http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider", "Active Directory" ) );
            identity.AddClaim( new Claim( ClaimTypes.Name, username ) );
            identity.AddClaim( new Claim( Identity.CustomClaimTypes.CVid, cVid.ToString() ) );
            // add your own claims if you need to add more information stored on the cookie
            if ( isAdmin )
            {
                identity.AddClaim( new Claim( ClaimTypes.Role, "Admin" ) );
            }

            return identity;
        }
    }


}
