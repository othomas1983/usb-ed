﻿using System.Configuration;
using System.IO;
using PerformanceManagement.Data.Services;
using PerformanceManagement.Infrastructure.Utilities;
using PerformanceManagement.Infrastructure.Utilities.ExtensionMethods;
using NLog;

namespace PerformanceManagement.Core.Startup
{
    public class AddUsernameToFmDbSql : IRunAtStartup
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly string _domain = ConfigurationManager.AppSettings["ActiveDirectoryDomain"];
        private readonly string _username = ConfigurationManager.AppSettings["DeveloperUsername"];
        
        public int StartupOrder { get; } = 2;


        public void Execute()
        {
            if ( System.Web.Hosting.HostingEnvironment.IsDevelopmentEnvironment )
            {
                string query = $@"SELECT Count(*) FROM CV WHERE Samaccountname LIKE '%{_username}%'";

                int? count = DbService.ExecuteScaler( query ).ToString().AsIntegerOrNull();

                if ( count == 0 )
                {
                    string command = $@"UPDATE CV SET Samaccountname='{_domain}\{_username}' WHERE CVID = 77";
                    DbService.ExecuteCommand( command );
                    Logger.Info( $"Username {_username} updated in database" );
                }
            }
        }
    }
}