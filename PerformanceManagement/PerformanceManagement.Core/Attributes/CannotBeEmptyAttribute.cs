﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceManagement.Core.Attributes
{
    /// <summary>
    /// Ensure IEnumerable is not null and has at least 1 element
    /// </summary>    
    [AttributeUsage( AttributeTargets.Property )]
    public sealed class CannotBeEmptyAttribute : ValidationAttribute
    {
        private const string DefaultError = "Please select at least one option.";
        public CannotBeEmptyAttribute() : base( DefaultError )
        {
        }

        protected override ValidationResult IsValid( object value, ValidationContext validationContext )
        {
            var list = value as IEnumerable;
            if ( list != null && list.GetEnumerator().MoveNext() )
            {
                return ValidationResult.Success;
            }
            ;
            return new ValidationResult( DefaultError );
        }

        public override string FormatErrorMessage( string name )
        {
            return String.Format( this.ErrorMessageString, name );
        }
    }
}
