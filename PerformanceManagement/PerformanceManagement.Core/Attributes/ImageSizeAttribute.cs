﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace PerformanceManagement.Core.Attributes
{
    /// <summary>
    /// Validates image filetype and does not exceed max size in bytes
    /// </summary>
    /// <seealso cref="System.ComponentModel.DataAnnotations.ValidationAttribute" />
    public class ImageSizeAttribute : ValidationAttribute
    {
        private readonly int _maxSize;

        public ImageSizeAttribute( int maxSize )
        {
            _maxSize = maxSize;
        }

        /*public override bool IsValid( object value )
        {
            // Because image is not required
            if ( value == null ) return true;

            if ( value is HttpPostedFileBase )
            {
                string contentType = ((HttpPostedFileBase) value).ContentType;
                if ( !contentType.Contains( "image" ) )
                {
                    ErrorMessage = "Please select valid jpg, png, gif or jpeg image.";
                    return false;
                }

                int contentLength = ((HttpPostedFileBase) value).ContentLength;
                if ( contentLength > _maxSize )
                {
                    ErrorMessage = "Please select image not greater than 50 KB in size.";
                    return false;
                }
            }

            // All validations passed
            return true;
        }*/

        protected override ValidationResult IsValid( object value, ValidationContext validationContext )
        {
            // Because image is not required
            if ( value == null ) return ValidationResult.Success;

            if ( value is HttpPostedFileBase )
            {
                string contentType = ((HttpPostedFileBase) value).ContentType;
                if ( !contentType.Contains( "image" ) )
                {
                    ErrorMessage = "Please select valid jpg, png, gif or jpeg image.";
                    return new ValidationResult( ErrorMessage );
                }

                int contentLength = ((HttpPostedFileBase) value).ContentLength;
                if ( contentLength > _maxSize )
                {
                    ErrorMessage = $"Please select image not greater than {_maxSize/1000} KB in size.";
                    return new ValidationResult( ErrorMessage );
                }
            }

            // All validations passed
            return ValidationResult.Success;
        }
    }
}
