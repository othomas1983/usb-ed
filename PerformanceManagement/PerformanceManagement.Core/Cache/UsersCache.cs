﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceManagement.Core.Cache
{
    public class UsersCache : CacheBase
    {
        private static string CacheKey( Type type )
        {
            return $"FM:Users:{type.Name}";
        }

        /// <summary>
        /// Gets the existing or a new list of users from the cache using default policy
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="valueFactory">The value factory.</param>
        /// <returns></returns>
        public static List<T> Read<T>( Func<List<T>> valueFactory ) 
        {
            var key = CacheKey( typeof( T ) );

            return GetOrAddExisting( key, valueFactory, new CacheItemPolicy() );
        }

        /// <summary>
        /// Gets the existing or a new list of users from the cache. Allows setting a custom cache policy
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="valueFactory">The value factory.</param>
        /// <param name="policy"></param>
        /// <returns></returns>
        public static List<T> Read<T>( Func<List<T>> valueFactory, CacheItemPolicy policy )
        {
            var key = CacheKey( typeof( T ) );

            return GetOrAddExisting( key, valueFactory, policy );
        }

        /// <summary>
        /// Removes the specified users from cache.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cVid">The c vid.</param>
        public static void Flush<T>() 
        {
            var key = CacheKey( typeof( T ) );

            if ( CacheContainsKey( key ) )
            {
                FlushCache( key );
            }
        }         
    }
}
