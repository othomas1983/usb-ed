﻿using System;
using System.Runtime.Caching;

namespace PerformanceManagement.Core.Cache
{
    public abstract class CacheBase
    {
        /// <summary>
        /// Gets the existing or a new item from cache
        /// </summary>
        /// <typeparam name="TT">The type of the t.</typeparam>
        /// <param name="key">The key.</param>
        /// <param name="valueFactory">The value factory.</param>
        /// <param name="policy"></param>
        /// <returns></returns>
        protected static TT GetOrAddExisting<TT>( string key, Func<TT> valueFactory , CacheItemPolicy policy )
        {
            FmMemoryCache cache = FmMemoryCache.Default;

            object cacheValue = cache.Get( key );
            if ( cacheValue != null )
            {
                return (TT) cacheValue;
            }

            TT value = valueFactory();
            if ( value != null )
            {
               cache.Set( key, value, policy );
            }

            return value;
        }

        /// <summary>
        /// Sets the cache.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="item">The item.</param>
        /// <param name="policy">The policy.</param>
        protected static void SetCache( string key, object item, CacheItemPolicy policy )
        {
            FmMemoryCache cache = FmMemoryCache.Default;
            cache.Set( key, item, policy );
        }

        /// <summary>
        /// Caches the contains key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        protected static bool CacheContainsKey( string key )
        {
            FmMemoryCache cache = FmMemoryCache.Default;
            return cache.Contains( key );
        }

        /// <summary>
        /// Flushes the cache.
        /// </summary>
        /// <param name="key">The key.</param>
        protected static void FlushCache( string key )
        {
            FmMemoryCache cache = FmMemoryCache.Default;
            cache.Remove( key );
        }
    }
}
