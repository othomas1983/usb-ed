﻿using System;
using System.Runtime.Caching;
using PerformanceManagement.Core.ViewModels.Interfaces;

namespace PerformanceManagement.Core.Cache
{
    /// <summary>
    /// Cache implementations for View Models
    /// </summary>
    public class ViewModelCache : CacheBase
    {
        private static string CacheKey( Type type, int id )
        {
            return $"FM:ViewModel:{type.Name}:{id}";
        }

        /// <summary>
        /// Gets the existing or a new view model from cache for the specific user
        /// </summary>
        /// <typeparam name="T">The type of the t.</typeparam>
        /// <param name="cVid"></param>
        /// <param name="valueFactory">The value factory.</param>
        /// <returns></returns>
        public static T Read<T>( int cVid, Func<T> valueFactory , bool loadFromCache = false) where T : class, IViewModel
        {
            if (loadFromCache)
            {
                var key = CacheKey(typeof(T), cVid);

                var cacheItemPolicy = new CacheItemPolicy
                {
                    AbsoluteExpiration = DateTime.Now.AddMinutes(30)
                };

                return GetOrAddExisting(key, valueFactory, cacheItemPolicy);
            }

            return valueFactory();
        }

        /// <summary>
        /// Removes the specified view model from cache.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cVid">The c vid.</param>
        public static void Flush<T>( int cVid ) where T : class, IViewModel
        {
            var key = CacheKey( typeof( T ), cVid );

            if ( CacheContainsKey( key ) )
            {
                FlushCache( key );
            }
        }

    }
}
