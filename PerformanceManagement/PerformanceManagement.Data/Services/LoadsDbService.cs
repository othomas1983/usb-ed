﻿using PerformanceManagement.Data.Interfaces;
using PerformanceManagement.Data.Models;
using PerformanceManagement.Domain.ExtensionMethods;
using PerformanceManagement.Domain.Model.Admin;
using PerformanceManagement.Domain.Model.Loads;
using System;
using System.Collections.Generic;
using System.Data;

namespace PerformanceManagement.Data.Services
{
    public class LoadsDbService : ILoadsDbService
    {
        #region Store Procedure
        public DataSet GetWebUsers(int cVid)
        {
            return DbService.GetDataSet("Select cvid, surname, [name], webdisplay from cv", CommandType.Text, null, 300);
        }
        /// <summary>
        /// Gets the teaching loads.
        /// </summary>
        /// <param name="cVid">The c vid.</param>
        /// <returns></returns>
        public DataSet GetTeachingLoads(int cVid)
        {
            var parameters = new Dictionary<string, object>
            {
                {"@CVID", cVid}
            };

            return DbService.GetDataSet("sp_PMTeachingLoadRead", CommandType.StoredProcedure, parameters, 300);
        }

        /// <summary>
        /// Gets the management loads.
        /// </summary>
        /// <param name="cVid">The c vid.</param>
        /// <returns></returns>
        public DataSet GetManagementLoads(int cVid)
        {
            var parameters = new Dictionary<string, object>
            {
                {"@CVID", cVid}
            };

            return DbService.GetDataSet("sp_PMManagementLoadRead", CommandType.StoredProcedure, parameters, 300);
        }

        /// <summary>
        /// Gets the supervision loads.
        /// </summary>
        /// <param name="cVid">The c vid.</param>
        /// <returns></returns>
        public DataSet GetSupervisionLoads(int cVid)
        {
            var parameters = new Dictionary<string, object>
            {
                {"@CVID", cVid}
            };

            return DbService.GetDataSet("sp_PMSupervisionRead", CommandType.StoredProcedure, parameters, 300);
        }
        #endregion

        #region IDbService Methods

        /// <summary>
        /// Saves the changes made to the model to the database.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="username">The username.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public bool SaveChanges(ISaveModel model, string username)
        {
           
            return false;
        }

        /// <summary>
        /// Deletes the specified database entity by its identifier.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id">The identifier.</param>
        /// <param name="username">The username.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public bool Delete<T>(int id, string username) //TODO: add constraint to generic T
        {
            string query = string.Empty;

            if (typeof(T) == typeof(ManagementLoad))
            {
                query = $"UPDATE  ManagementLoad SET isActive = 0, ModifiedBy = @username WHERE ManagementLoadID={id}";
            }

            if (typeof(T) == typeof(SupervisionLoad))
            {
                query = $"UPDATE  [dbo].[pma.Supervision] SET isActive = 0, ModifiedBy = @username WHERE SupervisionID={id}";
            }

            if (typeof(T) == typeof(TeachingLoad))
            {
                query = $"UPDATE  TeachingLoad SET isActive = 0, ModifiedBy = @username WHERE TeachingLoadID={id}";
            }

            if (!string.IsNullOrEmpty(query))
            {
                var parameters = new Dictionary<string, object>()
                {
                    {"@username", username}
                };

                var rowsDeleted = DbService.ExecuteCommand(query, CommandType.Text, parameters);

                return rowsDeleted != 0;
            }

            return false;
        }

        #endregion


        #region Save Methods
      

        /// <summary>
        /// Saves the data for Supervision Loads
        /// </summary>
        /// <param name="model">The model.</param>s
        /// <param name="username">The username.</param>
        /// <returns></returns>
      
      

        #endregion
    }
}