﻿using PerformanceManagement.Data.Interfaces;
using PerformanceManagement.Data.Models;
using PerformanceManagement.Domain.ExtensionMethods;
using PerformanceManagement.Domain.Model.Admin;
using PerformanceManagement.Domain.Model.Common;
using PerformanceManagement.Domain.Model.Loads;

using System;
using System.Collections.Generic;
using System.Data;

namespace PerformanceManagement.Data.Services
{
    public class UserWebService : IWebUserDbService
    {
        #region Store Procedure

        /// <summary>
        /// Gets the teaching loads.
        /// </summary>
        /// <param name="cVid">The c vid.</param>
        /// <returns></returns>
        public DataSet GetWebUsers(int cVid)
        {
            return DbService.GetDataSet("Select cvid, surname, [name], webdisplay from cv", CommandType.Text, null, 300);
        }
        #endregion

        #region IDbService Methods

        /// <summary>
        /// Saves the changes made to the model to the database.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="username">The username.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public bool SaveChanges(ISaveModel model, string username)
        {
            if (model is SaveWebUser)
            {
                return SaveData((SaveWebUser)model, username);
            }       

            return false;
        }

        /// <summary>
        /// Deletes the specified database entity by its identifier.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id">The identifier.</param>
        /// <param name="username">The username.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public bool Delete<T>(int id, string username) //TODO: add constraint to generic T
        {
            //string query = string.Empty;

            //if (typeof(T) == typeof(WebUser))
            //{
            //    query = $"UPDATE  ManagementLoad SET isActive = 0, ModifiedBy = @username WHERE ManagementLoadID={id}";
            //}



            //if (!string.IsNullOrEmpty(query))
            //{
            //    var parameters = new Dictionary<string, object>()
            //    {
            //        {"@username", username}
            //    };

            //    var rowsDeleted = DbService.ExecuteCommand(query, CommandType.Text, parameters);

            //    return rowsDeleted != 0;
            //}

            return false;
        }

        #endregion


        #region Save Methods
        /// <summary>
        /// Saves the data for Educations.
        /// </summary>
        /// <param name="model">The model.</param>s
        /// <param name="username">The username.</param>
        /// <returns></returns>
        private bool SaveData(SaveWebUser model, string username)
        {
            var parameters = new Dictionary<string, object>
            {                
                {"@CVID", model.CVid },
                {"@WebDisplay", model.WebDisplay}
            };

            int rowsUpdated = DbService.ExecuteCommand("", CommandType.StoredProcedure, parameters, 300);

            return rowsUpdated != 0;
        }
       
        private DataTable MapToDataTable(WebUser model)
        {
            var dataTable = new DataTable();

            
            dataTable.Columns.Add("CVID");
            dataTable.Columns.Add("WebDisplay");
  

            var newRow = dataTable.NewRow();
            dataTable.Rows.Add(newRow);

          
            newRow["CVID"] = model.CVID;           
            newRow["WebDisplay"] = model.WebDisplay;

            return dataTable;
        }

        public DataSet GetWebUsers(int cVid, bool displayOnWeb)
        {
            throw new NotImplementedException();
        }

        public void UpdateUserWebDisplayState(IEnumerable<int> cvIds, bool displayOnWeb)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
