﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using PerformanceManagement.Data.Properties;

namespace PerformanceManagement.Data.Services
{
    /// <summary>
    ///  Helper class to provide native SQL methods 
    /// </summary>
    public class DbService
    {
        #region Methods

        /// <summary>
        /// Gets a data reader.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="commandType">Type of the command.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        public static IDataReader GetDataReader( string query, CommandType commandType, Dictionary<string, object> parameters )
        {
            string connectionString = GetConnectionString();
            if ( !string.IsNullOrWhiteSpace( connectionString ) )
            {
                SqlConnection con = new SqlConnection( connectionString );

                if ( con.State == ConnectionState.Closed )
                {
                    con.Open();
                }

                SqlCommand sqlCommand = new SqlCommand( query, con );
                sqlCommand.CommandType = commandType;

                if ( parameters != null )
                {
                    foreach ( var parameter in parameters )
                    {
                        SqlParameter sqlParam = new SqlParameter();
                        sqlParam.ParameterName = parameter.Key.StartsWith( "@" ) ? parameter.Key : "@" + parameter.Key;
                        sqlParam.Value = parameter.Value;
                        sqlCommand.Parameters.Add( sqlParam );
                    }
                }

                return sqlCommand.ExecuteReader( CommandBehavior.CloseConnection );
            }

            return null;
        }

        /// <summary>
        /// Gets a data table.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="commandType">Type of the command.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        public static DataTable GetDataTable( string query, CommandType commandType, Dictionary<string, object> parameters )
        {
            DataSet dataSet = DbService.GetDataSet( query, commandType, parameters );
            if ( dataSet.Tables.Count > 0 )
            {
                return dataSet.Tables[0];
            }

            return null;
        }

        /// <summary>
        /// Gets a data set.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="commandType">Type of the command.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="timeOut">The time out in seconds.</param>
        /// <returns></returns>
        public static DataSet GetDataSet( string query, CommandType commandType, Dictionary<string, object> parameters, int? timeOut = null )
        {
            string connectionString = GetConnectionString();

            if ( !string.IsNullOrWhiteSpace( connectionString ) )
            {
                try
                {
                    using (SqlConnection con = new SqlConnection(connectionString))
                    {
                        if (con.State == ConnectionState.Closed)
                        {
                            con.Open();
                        }

                        using (SqlCommand sqlCommand = new SqlCommand(query, con))
                        {
                            if (timeOut.HasValue)
                            {
                                sqlCommand.CommandTimeout = timeOut.Value;
                            }
                            sqlCommand.CommandType = commandType;

                            if (parameters != null)
                            {
                                foreach (var parameter in parameters)
                                {
                                    SqlParameter sqlParam = new SqlParameter();
                                    sqlParam.ParameterName = parameter.Key.StartsWith("@") ? parameter.Key : "@" + parameter.Key;
                                    sqlParam.Value = parameter.Value;
                                    sqlCommand.Parameters.Add(sqlParam);
                                }
                            }

                            SqlDataAdapter adapter = new SqlDataAdapter(sqlCommand);
                            DataSet dataSet = new DataSet("facultyDs");
                            adapter.Fill(dataSet);
                            return dataSet;
                        }
                    }
                }
                catch (Exception ex)
                {

                }
                
            }

            return null;
        }

        /// <summary>
        /// Executes the query, and returns number of rows affected
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="commandType">Type of the command.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="commandTimeout">The command timeout (seconds)</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public static int ExecuteCommand( string query, CommandType commandType = CommandType.Text, Dictionary<string, object> parameters = null, int? commandTimeout = null )
        {
            string connectionString = GetConnectionString();
            if ( !string.IsNullOrWhiteSpace( connectionString ) )
            {
                using ( SqlConnection con = new SqlConnection( connectionString ) )
                {
                    if ( con.State == ConnectionState.Closed )
                    {
                        con.Open();
                    }

                    using ( SqlCommand sqlCommand = new SqlCommand( query, con ) )
                    {
                        sqlCommand.CommandType = commandType;

                        if ( parameters != null )
                        {
                            foreach ( var parameter in parameters )
                            {
                                SqlParameter sqlParam = new SqlParameter();
                                sqlParam.ParameterName = parameter.Key.StartsWith( "@" ) ? parameter.Key : "@" + parameter.Key;
                                sqlParam.Value = parameter.Value;
                                sqlCommand.Parameters.Add( sqlParam );
                            }
                        }

                        if ( commandTimeout.HasValue )
                        {
                            sqlCommand.CommandTimeout = commandTimeout.Value;
                        }

                        return sqlCommand.ExecuteNonQuery();
                    }
                }
            }

            return 0;
        }


        /// <summary>
        /// Executes the query, and returns number of rows affected.
        /// Provides ability to define parameters using SqlParameterInfo
        /// <seealso cref="SqlParameterInfo"/>
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="commandType">Type of the command.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="commandTimeout">The command timeout.</param>
        /// <returns></returns>
        public static int ExecuteOutCommand( string query, Dictionary<string, SqlParameterInfo> parameters, ref SqlParameter outParameter, CommandType commandType = CommandType.Text, int? commandTimeout = null )
        {
            string connectionString = GetConnectionString();
            if ( !string.IsNullOrWhiteSpace( connectionString ) )
            {
                using ( SqlConnection con = new SqlConnection( connectionString ) )
                {
                    if ( con.State == ConnectionState.Closed )
                    {
                        con.Open();
                    }

                    using ( SqlCommand sqlCommand = new SqlCommand( query, con ) )
                    {
                        sqlCommand.CommandType = commandType;

                        if ( parameters != null )
                        {
                            foreach ( var parameter in parameters )
                            {
                                SqlParameter sqlParam = new SqlParameter();
                                sqlParam.ParameterName = parameter.Key.StartsWith( "@" ) ? parameter.Key : "@" + parameter.Key;
                                sqlParam.Value = parameter.Value.ParameterValue;
                                // Specific parameter checks
                                if ( parameter.Value.DbType == SqlDbType.Structured )
                                {
                                    sqlParam.TypeName = parameter.Value.TypeName;
                                    sqlParam.SqlDbType = SqlDbType.Structured;
                                }

                                sqlCommand.Parameters.Add( sqlParam );
                            }
                            // Add OutPut Parameter
                            sqlCommand.Parameters.Add( outParameter );
                        }

                        if ( commandTimeout.HasValue )
                        {
                            sqlCommand.CommandTimeout = commandTimeout.Value;
                        }

                        return sqlCommand.ExecuteNonQuery();
                    }
                }
            }

            return 0;
        }


        /// <summary>
        /// Executes the query, and returns the first column of the first row in the
        /// result set returned by the query. Additional columns or rows are ignored.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="commandType">Type of the command.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        public static object ExecuteScaler( string query, CommandType commandType = CommandType.Text, Dictionary<string, object> parameters = null )
        {
            string connectionString = GetConnectionString();
            if ( !string.IsNullOrWhiteSpace( connectionString ) )
            {
                using ( SqlConnection con = new SqlConnection( connectionString ) )
                {
                    if ( con.State == ConnectionState.Closed )
                    {
                        con.Open();
                    }

                    using ( SqlCommand sqlCommand = new SqlCommand( query, con ) )
                    {
                        sqlCommand.CommandType = commandType;

                        if ( parameters != null )
                        {
                            foreach ( var parameter in parameters )
                            {
                                SqlParameter sqlParam = new SqlParameter();
                                sqlParam.ParameterName = parameter.Key.StartsWith( "@" ) ? parameter.Key : "@" + parameter.Key;
                                sqlParam.Value = parameter.Value;
                                sqlCommand.Parameters.Add( sqlParam );
                            }
                        }

                        return sqlCommand.ExecuteScalar();
                    }
                }
            }

            return null;
        }

        private static string GetConnectionString()
        {
            // Library setting file : used for unit and intergration tests
            string testingConnectionString = Settings.Default.FacultyContext;
            // Load application setting
            var connectionString = ConfigurationManager.ConnectionStrings["FacultyContext"];
            // Always prefer application setting
            return connectionString != null ? connectionString.ConnectionString : testingConnectionString;
        }

        #endregion
    }

    /// <summary>
    /// Allows Sql parameters to have extra definitions added to them which is required by some d
    /// </summary>
    public class SqlParameterInfo
    {
        public string TypeName { get; set; }
        public SqlDbType DbType { get; set; }
        public ParameterDirection Direction { get; set; }
        public object ParameterValue { get; set; }

        public SqlParameterInfo( string typeName, SqlDbType dbType, ParameterDirection direction, object parameterValue )
        {
            TypeName = typeName;
            DbType = dbType;
            Direction = direction;
            ParameterValue = parameterValue;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SqlParameterInfo"/> class.
        /// SqlDbType.VarChar, ParameterDirection.Input
        /// </summary>
        /// <param name="parameterValue">The parameter value.</param>
        public SqlParameterInfo( object parameterValue ) : this( string.Empty, SqlDbType.VarChar, ParameterDirection.Input, parameterValue )
        {
        }
    }
}
