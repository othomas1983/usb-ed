﻿using PerformanceManagement.Data.Interfaces;
using PerformanceManagement.Data.Models;
using PerformanceManagement.Domain.ExtensionMethods;

using PerformanceManagement.Infrastructure.Utilities.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace PerformanceManagement.Data.Services
{
    public class ResearchDbService : IResearchDbService
    {
        /// <summary>
        /// Gets the research.
        /// </summary>
        /// <param name="cVid">The cvid.</param>
        /// <returns></returns>
        public DataSet GetResearch(int cVid)
        {
            var parameters = new Dictionary<string, object>
            {
                {"@CVID", cVid}
            };
            //Get Performance management
            return DbService.GetDataSet("sp_PerformanceManagementResearchHistoryRead", CommandType.StoredProcedure, parameters, 300);
        }

        /// <summary>
        /// Gets the contributors.
        /// </summary>
        /// <param name="researchId">The research identifier.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public DataSet GetContributors(int researchId)
        {
            var parameters = new Dictionary<string, object>
            {
                {"@ResearchID", researchId}
            };

            return DbService.GetDataSet("sp_PeopleinResearchRead", CommandType.StoredProcedure, parameters, 300);
        }

        public bool SaveChanges(ISaveModel model, string username)
        {
            throw new NotImplementedException();
        }

        public bool Delete<T>(int id, string username)
        {
            throw new NotImplementedException();
        }
    }
}
