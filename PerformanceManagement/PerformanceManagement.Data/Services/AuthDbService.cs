using PerformanceManagement.Data.Interfaces;
using System.Collections.Generic;
using System.Data;

namespace PerformanceManagement.Data.Services
{
    /// <summary>
    /// Provides DB access functions for authentication and authorization
    /// </summary>
    /// <seealso cref="IAuthDbService" />
    public sealed class AuthDbService : IAuthDbService
    {
        /// <summary>
        /// Checks if the users exists in the database. Returns the CVid of the user
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="cVid">The c vid.</param>
        /// <returns></returns>
        public bool CheckUserExists(string username, out int cVid)
        {
            cVid = 0; //TODO magic number. remove this ...

            //TODO: check logic what if the user is inactive?

            var query = "SELECT CVID FROM CV WHERE [Samaccountname] = @username";

            var parameters = new Dictionary<string, object>
            {
                {"@username", username}
            };

            var table = DbService.GetDataTable(query, CommandType.Text, parameters);

            // No records found
            if (table.Rows.Count == 0)
            {
                return false;
            }

            var row = table.Rows[0];
            cVid = (int)row.ItemArray[0];

            return true;
        }

        /// <summary>
        /// Gets the auths.
        /// </summary>
        /// <returns></returns>
        public DataSet GetAuths()
        {
            return DbService.GetDataSet("sp_AuthRead", CommandType.StoredProcedure, null, 300);
        }
    }
}
