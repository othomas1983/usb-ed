﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceManagement.Data.Models
{
    public class SaveWebUser : ISaveModel
    {
        #region Properties

        public int CVid { get; set; }
        public bool WebDisplay { get; set; }     

       
        #endregion
    }
}
