﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceManagement.Data.Models
{
    public class SaveEducation : ISaveModel
    {
        #region Properties

        public int? Id { get; set; }
        public int Year { get; set; }
        public string Degree { get; set; }
        public string FieldOfStudy { get; set; }
        public string University { get; set; }
        public string Country { get; set; }
        public int IsActive { get; set; }
        public int CVid { get; set; }
        #endregion
    }
}
