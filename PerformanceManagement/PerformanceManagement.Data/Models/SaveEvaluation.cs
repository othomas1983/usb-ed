﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceManagement.Data.Models
{
    public class SaveEvaluation : ISaveModel
    {
        #region Properties

        public int? Id { get; set; }
        public int Year { get; set; }
        public int CVid { get; set; }
        public string ValueCommitment { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }

        // Peer Evaluations
        public int? PeerCVid { get; set; }

        // Supervisor Evaluations
        public int? SupervisorCVid { get; set; }

        #endregion
    }
}
