﻿using System.Collections.Generic;
using System.Data;
using PerformanceManagement.Domain.Model;



namespace PerformanceManagement.Data.Interfaces
{
    public interface IAdminDbService : IDbService
    {     
        /// <summary>
        /// Adds the Active Directory users to Faculty Management
        /// </summary>
        /// <param name="users">The users.</param>
        /// <param name="username"></param>
        /// <returns></returns>
        bool AddUsers( IEnumerable<ActiveDirectoryUser> users, string username );

      
        /// <summary>
        /// Gets the department categories.
        /// </summary>
        /// <param name="cvId">The cv identifier.</param>
        /// <returns></returns>
        DataSet GetFacultyDepartments( int cvId );

        /// <summary>
        /// Gets the classifications and sufficiencies.
        /// </summary>
        /// <param name="cvId">The cv identifier.</param>
        /// <returns></returns>
        DataSet GetClassificationsAndSufficiencies( int cvId, int? classificationId );

        DataSet GetWebUsers(int cVid);
    }
}
