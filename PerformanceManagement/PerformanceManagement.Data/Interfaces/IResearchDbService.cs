﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceManagement.Data.Interfaces
{
    public interface IResearchDbService : IDbService
    {
        /// <summary>
        /// Gets the research.
        /// </summary>
        /// <param name="cVid">The cvid.</param>
        /// <returns></returns>
        DataSet GetResearch( int cVid );

        /// <summary>
        /// Gets the contributors.
        /// </summary>
        /// <param name="researchId">The research identifier.</param>
        /// <returns></returns>
        DataSet GetContributors( int researchId );
    }
}
