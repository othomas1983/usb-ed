﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceManagement.Data.Interfaces
{
    public interface IFacultyUserDbService
    {
        /// <summary>
        /// Gets all faculty users.
        /// </summary>
        /// <returns></returns>
        DataSet GetAllFacultyUsers();

    }
}
