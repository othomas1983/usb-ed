﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceManagement.Data.Interfaces
{
    public interface ILoadsDbService : IDbService
    {
        /// <summary>
        /// Gets the teaching loads.
        /// </summary>
        /// <param name="cVid">The c vid.</param>
        /// <returns></returns>
        DataSet GetTeachingLoads( int cVid );

              /// <summary>
        /// Gets the management loads.
        /// </summary>
        /// <param name="cVid">The c vid.</param>
        /// <returns></returns>
        DataSet GetManagementLoads( int cVid );

        /// <summary>
        /// Gets the supervision loads.
        /// </summary>
        /// <param name="cVid">The c vid.</param>
        /// <returns></returns>
        DataSet GetSupervisionLoads( int cVid );
    }
}
