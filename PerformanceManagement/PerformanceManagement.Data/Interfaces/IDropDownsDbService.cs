using System.Data;
using PerformanceManagement.Domain.Model.Enums;

namespace PerformanceManagement.Data.Interfaces
{
    public interface IDropDownsDbService
    {
        /// <summary>
        /// Factory method to get the appropriate data.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        DataSet GetData( DropDownType type );
    }
}