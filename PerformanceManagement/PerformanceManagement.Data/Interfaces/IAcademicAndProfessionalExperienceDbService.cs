﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceManagement.Data.Interfaces
{
    public interface IAcademicAndProfessionalExperienceDbService : IDbService
    {
        /// <summary>
        /// Gets the experiences.
        /// </summary>
        /// <param name="cVid">The c vid.</param>
        /// <returns></returns>
        DataSet GetExperiences( int cVid );
    }
}
