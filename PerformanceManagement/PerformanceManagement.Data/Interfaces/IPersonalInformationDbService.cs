using System.Data;

namespace PerformanceManagement.Data.Interfaces
{
    public interface IPersonalInformationDbService : IDbService
    {
        /// <summary>
        /// Gets the personal information.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <returns></returns>
        DataSet GetPersonalInformation( string username );

        /// <summary>
        /// Gets the user image.
        /// </summary>
        /// <param name="employeeId">The employee identifier.</param>
        /// <returns></returns>
        DataSet GetUserImage( string employeeId );
   }
}