﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PerformanceManagement.Domain.Common;
using PerformanceManagement.Domain.ExtensionMethods;

namespace PerformanceManagement.Domain.Model.Admin
{
    /// <summary>
    /// Records for Faculty Department : [sp_FacultyDepartmentRead]
    /// </summary>
    public class Users : DataRowModel
    {

        public string UserName { get; set; }
        public string FullName { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public Users()
        {
        }

        public Users(DataRow row)
        {
            MapFromDataRow(row);
        }

 

        public override void MapFromDataRow( DataRow row )
        {
            UserName = row["UserName"].ValueOrDefault<string>();
            FirstName = row["FirstName"].ValueOrDefault<string>();
            LastName = row["LastName"].ValueOrDefault<string>();
        }
    }
}
