﻿using PerformanceManagement.Core.ViewModels.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceManagement.Application.PerformanceManagement.SupervisorReviewed.ViewModels
{
    public class ReviewedVm : IViewModel
    {
        /// <summary>
        /// Gets the full name.
        /// </summary>
        /// <value>
        /// The full name.
        /// </value>
        // public string FullName => $"{Name}, {Lastname}"; // Surname, FirstName
        public string Name { get; set; }
        public string Lastname { get; set; }
        public string Username { get; set; }
        public int ReviewerCVID { get; set; }

    }
}
