﻿using System.Collections.Generic;
using System.Linq;
using PerformanceManagement.Application.PerformanceManagement.Common;
using PerformanceManagement.Core.ViewModels.Interfaces;
using PerformanceManagement.Domain.Model.Common;
using PerformanceManagement.Domain.Model.Enums;
using PerformanceManagement.Infrastructure.Utilities.ExtensionMethods;

namespace PerformanceManagement.Application.PerformanceManagement.SupervisorReviewed.ViewModels
{
   
        public class SupervisorReviewedVm : IViewModel
        {
            public IEnumerable<ReviewedVm> Reviewed { get; }

            public SupervisorReviewedVm(IEnumerable<ReviewedVm> reviewed)
            {
            Reviewed = reviewed;
            }
        }
    }

