﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PerformanceManagement.Application.Common;
using PerformanceManagement.Application.PerformanceManagement.SupervisorReviewed.ViewModels;
using PerformanceManagement.Core.Cache;
using PerformanceManagement.Core.Security.Identity;
using PerformanceManagement.Core.ViewModels.Interfaces;
using PerformanceManagement.Data.Interfaces;
using PerformanceManagement.Domain.Factories;
using PerformanceManagement.Domain.Model.Enums;
using PerformanceManagement.Domain.Model.Loads.Common;
using PerformanceManagement.Domain.Model.PerformanceManagement;
using PerformanceManagement.Data.Models;

namespace PerformanceManagement.Application.PerformanceManagement.SupervisorReviewed
{
    public class SupervisorReviewedService : ApplicationService<SupervisorReviewedVm>, ISupervisorReviewedService
    {
        private readonly IPerformanceManagementDbService _dbService;

        public SupervisorReviewedService( IPerformanceManagementDbService dbService )
        {
            _dbService = dbService;
        }



        #region Override Methods

        public override IViewModel GetViewModel( IUser<CurrentPerson> currentPerson )
        {
            return CreateViewModel( currentPerson.CVid );
        }

        public override T GetItem<T>( int itemId, IUser<CurrentPerson> person )
        {
            var model = default( T );
            //object item = null;

            //var viewModel = ViewModelCache.Read( person.CVid, () => CreateViewModel( person.CVid ) );

            //if ( typeof( T ) == typeof( MyCommitmenteEditItemVm ) )
            //{
            //    item = viewModel.Commitments.Single( e => e.Id == itemId );
            //}

            //model = Mapper.Map<T>( item );                       

            return model;
        }

        public override bool SaveChanges(ICreateEditVm model, IUser<CurrentPerson> person, IUser<CurrentUser> user)
        {
            //ISaveModel dbModel = Mapper.Map<SaveCommitment>( model );

            //bool success = _dbService.SaveChanges( dbModel, user.Username );

            //if ( success )
            //{
            //    FlushViewModel( person );
            //}

            //return success;
            return true;
        }

        public override bool Delete<T>( int id, IUser<CurrentPerson> person, IUser<CurrentUser> user )
        {
            bool success = _dbService.Delete<T>( id, user.Username );

            if ( success )
            {
                FlushViewModel( person );
            }

            return success;
        }

        #endregion

        #region protected Methods

        protected override SupervisorReviewedVm CreateViewModel( int cvId )
        {
            var dataSet = _dbService.GetAllReviewedSupervisors( cvId );
            var reviewees = ModelFactory.CreateList<Reviewed>( dataSet )                                            
                                            .ToList();

            var reviewedVm = Mapper.Map<IList<ReviewedVm>>(reviewees);

            return new SupervisorReviewedVm(reviewedVm);
        }

        #endregion

        #region IMyCommitmentService Methods

    

        #endregion
    }
}
