﻿using PerformanceManagement.Application.Common;
using PerformanceManagement.Core.Security.Identity;

namespace PerformanceManagement.Application.PerformanceManagement.PeerEvaluation
{
    public interface IPeerEvaluationService : IApplicationService
    {
        /// <summary>
        /// Gets the create view model.
        /// </summary>
        /// <param name="category">The category.</param>
        /// <param name="person">The person.</param>
        /// <param name="user"></param>
        /// <returns></returns>
        ICreateEditVm GetCreateVm( string category, IUser<CurrentPerson> person, IUser<CurrentUser> user );
    }
}
