﻿using PerformanceManagement.Application.PerformanceManagement.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceManagement.Application.PerformanceManagement.PeerEvaluation.ViewModels
{
    public class PeerEvaluationItemVm : EvaluationBaseVm
    {
        #region Properties

        public int PeerCVid { get; set; }

        #endregion
    }
}
