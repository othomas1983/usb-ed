﻿using PerformanceManagement.Application.PerformanceManagement.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceManagement.Application.PerformanceManagement.PeerEvaluation.ViewModels
{
    public class PeerEvaluationCreateItemVm : EvaluationCreateBaseVm
    {
        #region Properties

        public int PeerCVid { get; set; }
        public string PeerUserName { get; set; }

        #endregion

        public PeerEvaluationCreateItemVm( string category, int peerCvId, string peerUserName, int reviewCvId, IEnumerable<int> selectedYears )
            : base( category, reviewCvId, selectedYears )
        {
            PeerCVid = peerCvId;
            PeerUserName = peerUserName;
        }

        public PeerEvaluationCreateItemVm()
        {
        }
    }
}


