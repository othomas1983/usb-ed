﻿using PerformanceManagement.Application.PerformanceManagement.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PerformanceManagement.Application.Common;

namespace PerformanceManagement.Application.PerformanceManagement.PeerEvaluation.ViewModels
{
    public class PeerEvaluationEditItemVm : EvaluationEditBaseVm
    {
        #region Properties

        public int PeerCVid { get; set; }

        public string PeerUserName { get; set; }

        #endregion

        public PeerEvaluationEditItemVm( string category, int peerCvId, int reviewCvId, string peerUserName)
        {
            Category = category;
            PeerCVid = peerCvId;
            CVid = reviewCvId;
            PeerUserName = peerUserName;
        }

        public PeerEvaluationEditItemVm()
        {
        }
    }
}
