﻿using PerformanceManagement.Application.PerformanceManagement.Common;
using PerformanceManagement.Core.ViewModels.Interfaces;
using PerformanceManagement.Domain.Model.Enums;
using PerformanceManagement.Infrastructure.Utilities.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PerformanceManagement.Domain.Model.Common;

namespace PerformanceManagement.Application.PerformanceManagement.PeerEvaluation.ViewModels
{
    public class PeerEvaluationVm : IViewModel
    {
        #region Properties

        /// <summary>
        /// Gets the peer.
        /// </summary>
        /// <value>
        /// The peer.
        /// </value>
        private FacultyUser Peer { get; }

        /// <summary>
        /// Gets the full name.
        /// </summary>
        /// <value>
        /// The full name.
        /// </value>
        public string FullName => $"{Peer?.Surname}, {Peer?.Description}"; // Surname, FirstName


        public EvaluationGroupVm Integrity { get; set; } = new EvaluationGroupVm { Category = EvaluationCategory.Integrity.ConvertToString() };
        public EvaluationGroupVm Inclusivity { get; set; } = new EvaluationGroupVm { Category = EvaluationCategory.Inclusivity.ConvertToString() };
        public EvaluationGroupVm Innovation { get; set; } = new EvaluationGroupVm { Category = EvaluationCategory.Innovation.ConvertToString() };
        public EvaluationGroupVm Engagement { get; set; } = new EvaluationGroupVm { Category = EvaluationCategory.Engagement.ConvertToString() };
        public EvaluationGroupVm Excellence { get; set; } = new EvaluationGroupVm { Category = EvaluationCategory.Excellence.ConvertToString() };
        public EvaluationGroupVm Sustainability { get; set; } = new EvaluationGroupVm { Category = EvaluationCategory.Sustainability.ConvertToString() };

        /// <summary>
        /// All evaluations
        /// </summary>
        public List<Domain.Model.PerformanceManagement.PeerEvaluation> AllEvaluations { get; set; }

        #endregion

        public PeerEvaluationVm( FacultyUser peer, List<Domain.Model.PerformanceManagement.PeerEvaluation> evaluations )
        {
            Peer = peer;

            AllEvaluations = evaluations;

            var groupedEvaluations = evaluations.GroupBy( e => e.Category );

            Integrity.Evaluations = groupedEvaluations
             .FirstOrDefault( i => i.Key.Equals( EvaluationCategory.Integrity.ConvertToString() ) )
             ?.Select( CreateEvaluation ).ToList()
             ?? new List<PeerEvaluationItemVm>();

            Inclusivity.Evaluations = groupedEvaluations
           .FirstOrDefault( i => i.Key.Equals( EvaluationCategory.Inclusivity.ConvertToString() ) )
           ?.Select( CreateEvaluation ).ToList()
           ?? new List<PeerEvaluationItemVm>();

            Innovation.Evaluations = groupedEvaluations
           .FirstOrDefault( i => i.Key.Equals( EvaluationCategory.Innovation.ConvertToString() ) )
           ?.Select( CreateEvaluation ).ToList()
           ?? new List<PeerEvaluationItemVm>();

            Engagement.Evaluations = groupedEvaluations
           .FirstOrDefault( i => i.Key.Equals( EvaluationCategory.Engagement.ConvertToString() ) )
           ?.Select( CreateEvaluation ).ToList()
           ?? new List<PeerEvaluationItemVm>();

            Excellence.Evaluations = groupedEvaluations
           .FirstOrDefault( i => i.Key.Equals( EvaluationCategory.Excellence.ConvertToString() ) )
           ?.Select( CreateEvaluation ).ToList()
           ?? new List<PeerEvaluationItemVm>();

            Sustainability.Evaluations = groupedEvaluations
           .FirstOrDefault( i => i.Key.Equals( EvaluationCategory.Sustainability.ConvertToString() ) )
           ?.Select( CreateEvaluation ).ToList()
           ?? new List<PeerEvaluationItemVm>();          
        }

        private PeerEvaluationItemVm CreateEvaluation( Domain.Model.PerformanceManagement.PeerEvaluation x )
        {
            var vm = new PeerEvaluationItemVm
            {
                Id = x.Id,
                Year = x.Year,
                ValueCommitment = x.ValueCommitment,
                Category = x.Category,
                Description = x.Description,
                CVid = x.CVid,
                PeerCVid = x.PeerCVid
            };

            return vm;
        }
    }
}
