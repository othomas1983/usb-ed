﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PerformanceManagement.Application.Common;
//using PerformanceManagement.Application.ManagementLoads.ViewModels.Common;
using PerformanceManagement.Application.PerformanceManagement.Common;
using PerformanceManagement.Application.PerformanceManagement.SelfEvaluation.ViewModels;
using PerformanceManagement.Core.Cache;
using PerformanceManagement.Core.Security.Identity;
using PerformanceManagement.Core.ViewModels.Interfaces;
using PerformanceManagement.Data.Interfaces;
using PerformanceManagement.Data.Models;
using PerformanceManagement.Domain.Factories;

namespace PerformanceManagement.Application.PerformanceManagement.SelfEvaluation
{
    public class SelfEvaluationService : ApplicationService<SelfEvaluationVm>, ISelfEvaluationService
    {
        private readonly IPerformanceManagementDbService _dbService;

        public SelfEvaluationService( IPerformanceManagementDbService dbService )
        {
            _dbService = dbService;
        }


        public override IViewModel GetViewModel( IUser<CurrentPerson> person )
        {
            return ViewModelCache.Read( person.CVid, () => CreateViewModel( person.CVid ) );
        }

        public ICreateEditVm GetCreateVm( string category, IUser<CurrentPerson> person )
        {
            var selectedYears = ViewModelCache.Read( person.CVid, () => CreateViewModel( person.CVid ) )
                .AllEvaluations.Where( e => e.Category.Equals( category ) )
                .Select( x => x.Year )
                .Distinct(); // <-- current data has duplicate years

            var model = new EvaluationCreateBaseVm( category, person.CVid, selectedYears );

            return model;
        }

        #region Override Methods

        public override T GetItem<T>( int itemId, IUser<CurrentPerson> person )
        {
            var model = default( T );
            object item = null;

            var viewModel = ViewModelCache.Read( person.CVid, () => CreateViewModel( person.CVid ) );

            if ( typeof( T ) == typeof( EvaluationEditBaseVm ) )
            {
                item = viewModel.AllEvaluations.Single( e => e.Id == itemId );
            }

            model = Mapper.Map<T>( item );
            // Db design issue: Update requires CVID
            (model as EvaluationEditBaseVm).CVid = person.CVid;

            return model;
        }

        public override bool SaveChanges( ICreateEditVm model, IUser<CurrentPerson> person, IUser<CurrentUser> user )
        {
            ISaveModel dbModel = Mapper.Map<SaveEvaluation>( model );

            bool success = _dbService.SaveChanges( dbModel, user.Username );

            if ( success )
            {
                FlushViewModel( person );
            }

            return success;
        }

        public override bool Delete<T>( int id, IUser<CurrentPerson> person, IUser<CurrentUser> user )
        {
            bool success = _dbService.Delete<T>( id, user.Username );

            if ( success )
            {
                FlushViewModel( person );
            }

            return success;
        }

        #endregion

        #region Private Methods
        protected override SelfEvaluationVm CreateViewModel( int cvId )
        {
            var dataSet = _dbService.GetSelfEvaluations( cvId );
            var evaluations = ModelFactory.CreateList<Domain.Model.PerformanceManagement.SelfEvaluation>( dataSet )
                                        .OrderBy( t => t.Year ).ToList();

            var model = new SelfEvaluationVm( evaluations );

            return model;
        }



        #endregion

    }
}
