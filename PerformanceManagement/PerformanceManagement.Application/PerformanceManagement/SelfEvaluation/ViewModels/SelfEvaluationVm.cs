﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PerformanceManagement.Application.PerformanceManagement.Common;
using PerformanceManagement.Core.ViewModels.Interfaces;
using PerformanceManagement.Domain.Model.Enums;
using PerformanceManagement.Infrastructure.Utilities.ExtensionMethods;

namespace PerformanceManagement.Application.PerformanceManagement.SelfEvaluation.ViewModels
{
    public class SelfEvaluationVm : IViewModel
    {
        #region Properties

        public EvaluationGroupVm Integrity { get; set; } = new EvaluationGroupVm { Category = EvaluationCategory.Integrity.ConvertToString() };
        public EvaluationGroupVm Inclusivity { get; set; } = new EvaluationGroupVm { Category = EvaluationCategory.Inclusivity.ConvertToString() };
        public EvaluationGroupVm Innovation { get; set; } = new EvaluationGroupVm { Category = EvaluationCategory.Innovation.ConvertToString() };
        public EvaluationGroupVm Engagement { get; set; } = new EvaluationGroupVm { Category = EvaluationCategory.Engagement.ConvertToString() };
        public EvaluationGroupVm Excellence { get; set; } = new EvaluationGroupVm { Category = EvaluationCategory.Excellence.ConvertToString() };
        public EvaluationGroupVm Sustainability { get; set; } = new EvaluationGroupVm { Category = EvaluationCategory.Sustainability.ConvertToString() };

        /// <summary>
        /// All evaluations
        /// </summary>
        public List<Domain.Model.PerformanceManagement.SelfEvaluation> AllEvaluations { get; set; }

        #endregion

        public SelfEvaluationVm( List<Domain.Model.PerformanceManagement.SelfEvaluation> evaluations )
        {
            AllEvaluations = evaluations;

            var groupedEvaluations = evaluations.GroupBy( e => e.Category );

            Integrity.Evaluations = groupedEvaluations
                .FirstOrDefault( i => i.Key.Equals( EvaluationCategory.Integrity.ConvertToString() ) )
                ?.Select( CreateEvaluation ).ToList()
                ?? new List<EvaluationBaseVm>();

            Inclusivity.Evaluations = groupedEvaluations
              .FirstOrDefault( i => i.Key.Equals( EvaluationCategory.Inclusivity.ConvertToString() ) )
              ?.Select( CreateEvaluation ).ToList()
              ?? new List<EvaluationBaseVm>(); ;

            Innovation.Evaluations = groupedEvaluations
              .FirstOrDefault( i => i.Key.Equals( EvaluationCategory.Innovation.ConvertToString() ) )
              ?.Select( CreateEvaluation ).ToList()
              ?? new List<EvaluationBaseVm>(); ;

            Engagement.Evaluations = groupedEvaluations
              .FirstOrDefault( i => i.Key.Equals( EvaluationCategory.Engagement.ConvertToString() ) )
              ?.Select( CreateEvaluation ).ToList()
              ?? new List<EvaluationBaseVm>(); ;

            Excellence.Evaluations = groupedEvaluations
              .FirstOrDefault( i => i.Key.Equals( EvaluationCategory.Excellence.ConvertToString() ) )
              ?.Select( CreateEvaluation ).ToList()
              ?? new List<EvaluationBaseVm>(); ;

            Sustainability.Evaluations = groupedEvaluations
              .FirstOrDefault( i => i.Key.Equals( EvaluationCategory.Sustainability.ConvertToString() ) )
              ?.Select( CreateEvaluation ).ToList()
              ?? new List<EvaluationBaseVm>(); ;
        }


        #region Create Methods
        
        private EvaluationBaseVm CreateEvaluation( Domain.Model.PerformanceManagement.SelfEvaluation x )
        {
            var vm = new EvaluationBaseVm
            {
                Id = x.Id,
                Year = x.Year,
                ValueCommitment = x.ValueCommitment,
                Category = x.Category,
                Description = x.Description,
                CVid = x.CVid,
            };

            return vm;
        }


        #endregion
    }
}
