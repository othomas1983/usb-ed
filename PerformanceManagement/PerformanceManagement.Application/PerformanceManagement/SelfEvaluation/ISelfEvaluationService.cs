﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PerformanceManagement.Application.Common;
using PerformanceManagement.Core.Security.Identity;

namespace PerformanceManagement.Application.PerformanceManagement.SelfEvaluation
{
    public interface ISelfEvaluationService : IApplicationService
    {
        /// <summary>
        /// Gets the create view model.
        /// </summary>
        /// <param name="category">The category.</param>
        /// <param name="person">The person.</param>
        /// <returns></returns>
        ICreateEditVm GetCreateVm( string category, IUser<CurrentPerson> person );
    }
}
