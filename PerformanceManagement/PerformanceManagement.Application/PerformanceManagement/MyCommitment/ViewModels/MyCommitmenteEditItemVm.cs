﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using PerformanceManagement.Application.Common;

namespace PerformanceManagement.Application.PerformanceManagement.MyCommitment.ViewModels
{
    public class MyCommitmenteEditItemVm : ICreateEditVm
    {
        #region Properties

        public int? Id { get; set; }
        [DataType( "YearDatePickerCommitment" ), Required]
        public int Year { get;  set; }
        [DisplayName( "Commitment" ), Required]
        public string CommitmentValue { get; set; }
        [DisplayName("Description")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Description { get; set; }        
        [DisplayName( "Category" ), Required]
        public int PerformanceCategoryId { get; set; }
        public int? CVid { get; set; }

        // used for dynamic ajax drop down list
        public IEnumerable<SelectListItem> PerformanceCategories { get; set; } = new List<SelectListItem>();

        #endregion
       
    }
}
