﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using PerformanceManagement.Core.ViewModels.Interfaces;
using PerformanceManagement.Domain.Model.PerformanceManagement;

namespace PerformanceManagement.Application.PerformanceManagement.MyCommitment.ViewModels
{
    public class MyCommitmentVm : IViewModel
    {
        public IEnumerable<CommitmentVm> Commitments { get; }

        public MyCommitmentVm( IEnumerable<CommitmentVm> commitments )
        {
            Commitments = commitments;
        }       
    }
}
