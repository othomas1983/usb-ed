﻿using System.Collections.Generic;
using PerformanceManagement.Application.PerformanceManagement.Common;

namespace PerformanceManagement.Application.PerformanceManagement.SupervisorEvaluation.ViewModels
{
    public class SupervisorEvaluationCreateItemVm : EvaluationCreateBaseVm
    {
        #region Properties

        public int SupervisorCVid { get; set; }
        public string SupervisorUserName { get; set; }

        #endregion

        public SupervisorEvaluationCreateItemVm( string category, int supervisorCvId, string supervisorUserName, int reviewCvId, IEnumerable<int> selectedYears )
            : base( category, reviewCvId, selectedYears )
        {
            SupervisorCVid = supervisorCvId;
            SupervisorUserName = supervisorUserName;
        }

        public SupervisorEvaluationCreateItemVm()
        {
        }
    }
}
