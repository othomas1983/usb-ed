﻿using System.Collections.Generic;
using System.Linq;
using PerformanceManagement.Application.PerformanceManagement.Common;
using PerformanceManagement.Core.ViewModels.Interfaces;
using PerformanceManagement.Domain.Model.Common;
using PerformanceManagement.Domain.Model.Enums;
using PerformanceManagement.Infrastructure.Utilities.ExtensionMethods;
using PerformanceManagement.Application.PerformanceManagement.SupervisorReviewed.ViewModels;
using PerformanceManagement.Domain.Model;

namespace PerformanceManagement.Application.PerformanceManagement.SupervisorEvaluation.ViewModels
{
    public class SupervisorEvaluationVm : IViewModel
    {
        #region Properties

        /// <summary>
        /// Gets the peer.
        /// </summary>
        /// <value>
        /// The peer.
        /// </value>
        private FacultyUser Supervisor { get; }
        private UserLogin User { get; }

        /// <summary>
        /// Gets the full name.
        /// </summary>
        /// <value>
        /// The full name.
        /// </value>
        public string FullName => $" {Supervisor?.Description} {Supervisor?.Surname}"; // Surname, FirstName
        


        public EvaluationGroupVm Integrity { get; set; } = new EvaluationGroupVm { Category = EvaluationCategory.Integrity.ConvertToString() };
        public EvaluationGroupVm Inclusivity { get; set; } = new EvaluationGroupVm { Category = EvaluationCategory.Inclusivity.ConvertToString() };
        public EvaluationGroupVm Innovation { get; set; } = new EvaluationGroupVm { Category = EvaluationCategory.Innovation.ConvertToString() };
        public EvaluationGroupVm Engagement { get; set; } = new EvaluationGroupVm { Category = EvaluationCategory.Engagement.ConvertToString() };
        public EvaluationGroupVm Excellence { get; set; } = new EvaluationGroupVm { Category = EvaluationCategory.Excellence.ConvertToString() };
        public EvaluationGroupVm Sustainability { get; set; } = new EvaluationGroupVm { Category = EvaluationCategory.Sustainability.ConvertToString() };
      



        /// <summary>
        /// All evaluations
        /// </summary>
        public List<Domain.Model.PerformanceManagement.SupervisorEvaluation> AllEvaluations { get; set; }

        #endregion

        public SupervisorEvaluationVm( FacultyUser supervisor, List<Domain.Model.PerformanceManagement.SupervisorEvaluation> evaluations )
        {
            Supervisor = supervisor;

            AllEvaluations = evaluations;

            var groupedEvaluations = evaluations.GroupBy( e => e.Category );

            Integrity.Evaluations = groupedEvaluations
             .FirstOrDefault(i => i.Key.Equals(EvaluationCategory.Integrity.ConvertToString()))
             ?.Select(CreateEvaluation).ToList()
             ?? new List<SupervisorEvaluationItemVm>();

            Inclusivity.Evaluations = groupedEvaluations
           .FirstOrDefault(i => i.Key.Equals(EvaluationCategory.Inclusivity.ConvertToString()))
           ?.Select(CreateEvaluation).ToList()
           ?? new List<SupervisorEvaluationItemVm>();

            Innovation.Evaluations = groupedEvaluations
           .FirstOrDefault(i => i.Key.Equals(EvaluationCategory.Innovation.ConvertToString()))
           ?.Select(CreateEvaluation).ToList()
           ?? new List<SupervisorEvaluationItemVm>();

            Engagement.Evaluations = groupedEvaluations
           .FirstOrDefault(i => i.Key.Equals(EvaluationCategory.Engagement.ConvertToString()))
           ?.Select(CreateEvaluation).ToList()
           ?? new List<SupervisorEvaluationItemVm>();

            Excellence.Evaluations = groupedEvaluations
           .FirstOrDefault(i => i.Key.Equals(EvaluationCategory.Excellence.ConvertToString()))
           ?.Select(CreateEvaluation).ToList()
           ?? new List<SupervisorEvaluationItemVm>();

            Sustainability.Evaluations = groupedEvaluations
           .FirstOrDefault(i => i.Key.Equals(EvaluationCategory.Sustainability.ConvertToString()))
           ?.Select(CreateEvaluation).ToList()
           ?? new List<SupervisorEvaluationItemVm>();

        
        }

        private SupervisorEvaluationItemVm CreateEvaluation( Domain.Model.PerformanceManagement.SupervisorEvaluation x )
        {
            var vm = new SupervisorEvaluationItemVm
            {
                Id = x.Id,
                Year = x.Year,
                ValueCommitment = x.ValueCommitment,
                Category = x.Category,
                Description = x.Description,
                CVid = x.CVid,
                SupervisorCVid = x.SupervisorCVid
            };

            return vm;
        }
    }
}
