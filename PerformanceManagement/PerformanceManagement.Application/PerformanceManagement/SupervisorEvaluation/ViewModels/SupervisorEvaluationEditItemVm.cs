﻿using PerformanceManagement.Application.PerformanceManagement.Common;

namespace PerformanceManagement.Application.PerformanceManagement.SupervisorEvaluation.ViewModels
{
    public class SupervisorEvaluationEditItemVm : EvaluationEditBaseVm
    {
        #region Properties

        public int SupervisorCVid { get; set; }
        public string SupervisorUserName { get; set; }

        #endregion

        public SupervisorEvaluationEditItemVm( string category, int supervisorCvId, int reviewCvId, string supervisorUserName )
        {
            Category = category;
            SupervisorCVid = supervisorCvId;
            CVid = reviewCvId;
            SupervisorUserName = supervisorUserName;
        }

        public SupervisorEvaluationEditItemVm()
        {
        }
    }
}
