﻿using PerformanceManagement.Application.PerformanceManagement.Common;

namespace PerformanceManagement.Application.PerformanceManagement.SupervisorEvaluation.ViewModels
{
    public class SupervisorEvaluationItemVm : EvaluationBaseVm
    {
        #region Properties

        public int SupervisorCVid { get; set; }

        #endregion
    }
}
