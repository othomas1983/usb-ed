﻿using PerformanceManagement.Application.PerformanceManagement.Common;

namespace PerformanceManagement.Application.PerformanceManagement.SupervisorEvaluation.ViewModels
{
    public class SupervisorEvaluationDeleteItemVm : EvaluationBaseVm
    {
        #region Properties


        public int SupervisorCVid { get; set; }
        public string SupervisorUserName { get; set; }
        public int? Id { get; set; }
        #endregion

        public SupervisorEvaluationDeleteItemVm(int id,string category, int supervisorCvId, int reviewCvId, string supervisorUserName)
        {
            Id = id;
            Category = category;
            SupervisorCVid = supervisorCvId;
            CVid = reviewCvId;
            SupervisorUserName = supervisorUserName;
        }

        public SupervisorEvaluationDeleteItemVm()
        {
        }

       
    }
}
