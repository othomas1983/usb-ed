﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PerformanceManagement.Application.Common;
using PerformanceManagement.Domain.Model.PerformanceManagement;

namespace PerformanceManagement.Application.PerformanceManagement.PeerReviewed
{
    public interface IPeerReviewedService : IApplicationService
    {
        /// <summary>
        /// Gets the performance categories.
        /// </summary>
        /// <returns></returns>
       // IEnumerable<PerformanceCategory> GetPerformanceCategories();
    }
}
