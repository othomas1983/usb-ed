﻿using System.Collections.Generic;
using System.Linq;
using PerformanceManagement.Application.PerformanceManagement.Common;
using PerformanceManagement.Core.ViewModels.Interfaces;
using PerformanceManagement.Domain.Model.Common;
using PerformanceManagement.Domain.Model.Enums;
using PerformanceManagement.Infrastructure.Utilities.ExtensionMethods;

namespace PerformanceManagement.Application.PerformanceManagement.PeerReviewed.ViewModels
{
   
        public class PeerReviewedVm : IViewModel
        {
            public IEnumerable<PeerReviewsVm> Reviewed { get; }

            public PeerReviewedVm(IEnumerable<PeerReviewsVm> reviewed)
            {
            Reviewed = reviewed;
            }
        }
    }

