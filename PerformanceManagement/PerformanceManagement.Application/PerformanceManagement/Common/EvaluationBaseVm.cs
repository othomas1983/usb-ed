﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceManagement.Application.PerformanceManagement.Common
{
    public class EvaluationBaseVm : IEvaluationVm
    {
        #region Properties

        public int? Id { get; set; }
        public int Year { get;  set; }
        public int CVid { get;  set; }
        public string ValueCommitment { get;  set; }
        public string Category { get;  set; }
        public string Description { get;  set; }


        #endregion
    }
}
