﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PerformanceManagement.Application.Common;
using PerformanceManagement.Application.TeachingLoads.ViewModels;
using PerformanceManagement.Core.Security.Identity;
using PerformanceManagement.Application.WebUsers.ViewModels;
using PerformanceManagement.Application.Admin;

namespace PerformanceManagement.Application.TeachingLoads
{
    public interface ITeachingLoadService : IApplicationService
    {
        /// <summary>
        /// Filters the write hand research with optional year filter.
        /// </summary>
        /// <param name="year">The year.</param>
        /// <param name="currentPerson">The year.</param>
        /// <returns></returns>
        TeachingLoadVm FilterViewModel( IUser<CurrentPerson> currentPerson, int? year );    
    }
}
