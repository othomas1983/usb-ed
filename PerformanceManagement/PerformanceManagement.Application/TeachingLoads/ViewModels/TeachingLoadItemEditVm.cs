﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PerformanceManagement.Application.Common;
using PerformanceManagement.Core.Cache;
using PerformanceManagement.Core.ViewModels.Interfaces;
using PerformanceManagement.Domain.Model.Common;
using PerformanceManagement.Domain.Model.Enums;
using PerformanceManagement.Domain.Model.Loads.Common;

namespace PerformanceManagement.Application.TeachingLoads.ViewModels
{
    public class TeachingLoadItemEditVm : ICreateEditVm
    {
        #region Properties

        public int Id { get; set; }
        [DataType( "YearDatePickerLoad" ), Required]
        public int Year { get; set; }
        [Required]
        public string ProgrammeOffering { get; set; }

        /// <summary>
        /// Gets the porgrammeId using the programme text
        /// </summary>
        [DataType( "ProgrammeId" ), DisplayName( "Programme" ), Required]
        public int? ProgrammeId
        {
            get
            {
                if ( string.IsNullOrEmpty( _programme ) ) return _programmeId;

                var data = DropDownsCache.Read<Programme>( DropDownType.Programmes );
                return data.FirstOrDefault( x => x.Description == Programme )?.Id;
            }
            set => _programmeId = value;
        }
        private int? _programmeId;

        /// <summary>
        /// Gets the porgramme using the lookup table and ProgrammeId.
        /// </summary>
        public string Programme
        {
            get
            {
                if ( _programmeId.HasValue )
                {
                    var data = DropDownsCache.Read<Nationality>( DropDownType.Nationalities );
                    return data.FirstOrDefault( x => x.Id == ProgrammeId.Value )?.Description;
                }
                return _programme;
            }
            set => _programme = value;
        }
        private string _programme;


      
        [Required]
        public string Module { get; set; }
        [Required, DataType("PositiveNumber"),]
        [RegularExpression(@"^\d+\.\d{0,1}$")]
        public decimal Sessions { get; set; }
        [DataType( "PositiveNumber" ), Required]
        public int Students { get; set; }
        [DataType( "PositiveNumber" ), Required]
        public int BaseCredit { get; set; }

        [DataType("PositiveNumber"), Required]
        [RegularExpression(@"^\d+\.\d{0,1}$")]
        public decimal TotalCredits { get; set; }

        public int? CVid { get; set; }
        #endregion

        public TeachingLoadItemEditVm( int cVid )
        {
            CVid = cVid;
        }

        public TeachingLoadItemEditVm()
        {
        }
    }
}
