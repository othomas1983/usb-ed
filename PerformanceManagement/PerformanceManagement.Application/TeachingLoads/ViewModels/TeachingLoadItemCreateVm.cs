﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PerformanceManagement.Application.Common;
using PerformanceManagement.Core.Cache;
using PerformanceManagement.Core.ViewModels.Interfaces;
using PerformanceManagement.Domain.Model.Common;
using PerformanceManagement.Domain.Model.Enums;
using PerformanceManagement.Domain.Model.Loads.Common;

namespace PerformanceManagement.Application.TeachingLoads.ViewModels
{
    public class TeachingLoadItemCreateVm : ICreateEditVm
    {
        #region Properties

        public int? Id { get; set; } = null;
        [DataType( "YearDatePickerLoad" ), Required]
        public int Year { get; set; }
        [Required]
        public string ProgrammeOffering { get; set; }
        [DataType( "ProgrammeId" ), DisplayName("Programme"), Required]
        public int? ProgrammeId { get; set; }
        /// <summary>
        /// Gets the porgramme using the lookup table and ProgrammeId.
        /// </summary>
        public string Programme
        {
            get
            {
                if ( !ProgrammeId.HasValue ) return string.Empty;

                var data = DropDownsCache.Read<Programme>( DropDownType.Programmes );
                return data.FirstOrDefault( x => x.Id == ProgrammeId.Value )?.Description;
            }
        }
        [Required]
        public string Module { get; set; }
        [DataType("PositiveNumber"), Required]
        [RegularExpression(@"^\d+\.\d{0,1}$")]
        public decimal Sessions { get; set; }
        [DataType( "PositiveNumber" ), Required]
        public int Students { get; set; }
        [DataType( "PositiveNumber" ), Required]
        public int BaseCredit { get; set; }

        [DataType("PositiveNumber"), Required]
        [RegularExpression(@"^\d+\.\d{0,1}$")]
        public decimal TotalCredits { get; set; }

        public int? CVid { get; set; }
        #endregion

        public TeachingLoadItemCreateVm( int cVid )
        {
            CVid = cVid;
        }

        public TeachingLoadItemCreateVm()
        {
        }
    }
}
