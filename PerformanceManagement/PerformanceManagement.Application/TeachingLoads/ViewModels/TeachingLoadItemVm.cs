﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PerformanceManagement.Core.ViewModels.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace PerformanceManagement.Application.TeachingLoads.ViewModels
{
    public class TeachingLoadItemVm
    {
        #region Properties

        public int Id { get; set; }
        public int Year { get;  set; }
        public string ProgrammeOffering { get;  set; }
        public string Programme { get;  set; }
        public string Module { get;  set; }
        [RegularExpression(@"^\d+\.\d{0,2}$")]
        public decimal Sessions { get;  set; }
        public int Students { get;  set; }
        public int BaseCredit { get;  set; }
        [RegularExpression(@"^\d+\.\d{0,2}$")]
        public decimal TotalCredits { get; set; }
        public string Category { get; set; }
        #endregion
    }
}
