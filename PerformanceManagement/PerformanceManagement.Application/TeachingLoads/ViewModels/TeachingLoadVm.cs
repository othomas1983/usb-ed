﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using PerformanceManagement.Core.ViewModels.Interfaces;
using PerformanceManagement.Domain.Model;
using System.ComponentModel.DataAnnotations;

namespace PerformanceManagement.Application.TeachingLoads.ViewModels
{
    public class TeachingLoadVm : IViewModel
    {
        #region Properties
        public List<SelectListItem> Years { get; set; } = new List<SelectListItem>();

        public List<TeachingLoadItemVm> TeachingLoads { get; set; } 
        #endregion

        public TeachingLoadVm( List<TeachingLoadItemVm> teachingLoads )
        {
            TeachingLoads = teachingLoads;

            if ( teachingLoads  != null && teachingLoads.Any() )
            {                
                Years.Add( new SelectListItem
                {
                    Value = null,
                    Text = "-- All --"
                } );
                teachingLoads.ForEach( x => AddToYears( x.Year ) );
            }
        }

        public TeachingLoadVm()
        {
        }

        #region Private Methods
        private void AddToYears( int year )
        {
            var item = new SelectListItem
            {
                Value = year.ToString(),
                Text = year.ToString()
            };

            if ( !Years.Select( x => x.Value ).Contains( item.Value ) )
            {
                Years.Add( item );
            }
        }
        [RegularExpression(@"^\d+\.\d{0,2}$")]
        public decimal TotalCredits { get; set; }
        [RegularExpression(@"^\d+\.\d{0,2}$")]
        public decimal Sessions { get; set; }
        #endregion
    }
}
