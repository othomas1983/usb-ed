﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PerformanceManagement.Application.Common;
using PerformanceManagement.Application.TeachingLoads.ViewModels;
using PerformanceManagement.Core.Cache;
using PerformanceManagement.Core.Security.Identity;
using PerformanceManagement.Core.ViewModels.Interfaces;
using PerformanceManagement.Data.Interfaces;
using PerformanceManagement.Data.Models;
using PerformanceManagement.Domain.Factories;
using PerformanceManagement.Domain.Model;
using PerformanceManagement.Domain.Model.Loads;
using PerformanceManagement.Domain.Model.Common;
using PerformanceManagement.Application.WebUsers.ViewModels;
using PerformanceManagement.Application.Admin;

namespace PerformanceManagement.Application.TeachingLoads
{
    public class TeachingLoadService : ApplicationService<TeachingLoadVm>, ITeachingLoadService
    {
        private readonly ILoadsDbService _dbService;

        public TeachingLoadService( ILoadsDbService dbService )
        {
            _dbService = dbService;
        }


        public override IViewModel GetViewModel(IUser<CurrentPerson> currentPerson)
        {
            return ViewModelCache.Read(currentPerson.CVid, () => CreateViewModel(currentPerson.CVid));

        }

        public TeachingLoadVm FilterViewModel( IUser<CurrentPerson> currentPerson, int? year )
        {
            var model = ViewModelCache.Read( currentPerson.CVid, () => CreateViewModel( currentPerson.CVid ) );
            // Make a deep copy to avoid the filter updating the cached items
            var deepCopy = Mapper.Map<TeachingLoadVm>( model );

            if ( year.HasValue && model.TeachingLoads != null )
            {
                var filteredLoads = deepCopy.TeachingLoads.Where( w => w.Year == year ).ToList();

                deepCopy.TeachingLoads = filteredLoads;
            }

            return deepCopy;
        }


        #region Override Methods

        public override T GetItem<T>( int itemId, IUser<CurrentPerson> person )
        {
            var model = default( T );

            var viewModel = ViewModelCache.Read( person.CVid, () => CreateViewModel( person.CVid ) );

            object item = viewModel.TeachingLoads.Single( e => e.Id == itemId );

            model = Mapper.Map<T>( item );

            return model;
        }

        public override bool SaveChanges( ICreateEditVm model, IUser<CurrentPerson> person, IUser<CurrentUser> user )
        {
            bool success = true;

            return success;
        }

        #endregion

        #region Private Methods
        protected override TeachingLoadVm CreateViewModel( int cvId )
        {
            var teachingLoadDs = _dbService.GetTeachingLoads( cvId );
            var teachingLoads = ModelFactory.CreateList<TeachingLoad>( teachingLoadDs ).OrderByDescending( t => t.Year ).ToList();

            var mappedItems = Mapper.Map<List<TeachingLoadItemVm>>( teachingLoads );

            var model = new TeachingLoadVm( mappedItems );

            return model;
        }
        #endregion
    }
}
