﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceManagement.Application.Common
{
    /// <summary>
    /// Base class for all view models which provides custom validation
    /// </summary>
    public abstract class ViewModel
    {
        /// <summary>
        /// Gets the validation results for the view model
        /// </summary>        
        public virtual List<ValidationResult> ValidationResults { get; private set; }

       /* /// <summary>
        /// Gets a value indicating whether this instance is valid.
        /// </summary>
        public virtual bool IsValid
        {
            get
            {
                var valContext = new ValidationContext( this, serviceProvider: null, items: null );
                ValidationResults = new List<ValidationResult>();
                return Validator.TryValidateObject( this, valContext, ValidationResults, true );
            }
        }*/
    }
}
