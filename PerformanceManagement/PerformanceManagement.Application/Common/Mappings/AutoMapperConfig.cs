﻿using AutoMapper;
//using PerformanceManagement.Application.Admin.ViewModels;

using PerformanceManagement.Application.PerformanceManagement.Common;
using PerformanceManagement.Application.PerformanceManagement.MyCommitment.ViewModels;
using PerformanceManagement.Application.PerformanceManagement.PeerEvaluation.ViewModels;
using PerformanceManagement.Application.PerformanceManagement.SelfEvaluation.ViewModels;
using PerformanceManagement.Application.PerformanceManagement.SupervisorEvaluation.ViewModels;
using PerformanceManagement.Application.Research.ViewModels;
using PerformanceManagement.Application.SupervisionLoad.ViewModels;
using PerformanceManagement.Application.TeachingLoads.ViewModels;
using PerformanceManagement.Application.ManagementLoads.ViewModels.Common;
using PerformanceManagement.Application.ManagementLoads.ViewModels.ProgrammeHead;
using PerformanceManagement.Application.ManagementLoads.ViewModels.TaskTeamMember;
using PerformanceManagement.Application.Admin.ViewModels;
using PerformanceManagement.Data.Models;
using PerformanceManagement.Domain.Model.Loads;
using PerformanceManagement.Domain.Model.Common;
using PerformanceManagement.Domain.Model.PerformanceManagement;
using PerformanceManagement.Domain.Model.Research;
using PerformanceManagement.Infrastructure.Utilities.ExtensionMethods;
using PerformanceManagement.Application.PerformanceManagement.SupervisorReviewed.ViewModels;
using PerformanceManagement.Application.PerformanceManagement.PeerReviewed.ViewModels;

namespace PerformanceManagement.Application.Common.Mappings
{
    public static class AutoMapperConfig
    {
        public static IMapper GetCustomMapper()
        {
            var config = new MapperConfiguration( cfg =>
            {
                // source - destination
               
                ///* Teaching Load */
                cfg.CreateMap<TeachingLoad, TeachingLoadItemVm>();
                cfg.CreateMap<TeachingLoadItemVm, TeachingLoadItemEditVm>();
                cfg.CreateMap<TeachingLoadVm, TeachingLoadVm>();

                ///* Management Load */
                ///* Programme Heads */
                cfg.CreateMap<ProgrammeHeadVm, ProgrammeHeadEditVm>();
      
                /* Task Team Member */
                cfg.CreateMap<TaskTeamMemberVm, TaskTeamMemberEditVm>();
          
                /* Centre Head */
                cfg.CreateMap<ManagementLoadGeneralVm, ManagementLoadGeneralEditCreateVm>();

                /*Supervisor Reviewed*/
                cfg.CreateMap<Reviewed, ReviewedVm>();

                /*Peer Reviewed*/
                cfg.CreateMap<PeerReviews, PeerReviewsVm>();

                /* Performance Mangement */
                /* Self Evaluation */
                cfg.CreateMap<EvaluationCreateBaseVm, SaveEvaluation>();
                cfg.CreateMap<EvaluationEditBaseVm, SaveEvaluation>();
                cfg.CreateMap<SelfEvaluation, EvaluationEditBaseVm> ();
                /* Peer Evaluation */
                cfg.CreateMap<PeerEvaluationCreateItemVm, SaveEvaluation>();
                cfg.CreateMap<PeerEvaluationEditItemVm, SaveEvaluation>();
                cfg.CreateMap<PeerEvaluation, PeerEvaluationEditItemVm>();
                /* Supervisor Evaluation */
                cfg.CreateMap<SupervisorEvaluationCreateItemVm, SaveEvaluation>();
                cfg.CreateMap<SupervisorEvaluationEditItemVm, SaveEvaluation>();
                cfg.CreateMap<SupervisorEvaluation, SupervisorEvaluationEditItemVm>();
                /* My Commitments */
                cfg.CreateMap<Commitment, CommitmentVm>();
                cfg.CreateMap<CommitmentVm, MyCommitmenteEditItemVm>();
                cfg.CreateMap<MyCommitmentCreateItemVm, SaveCommitment>();
                cfg.CreateMap<MyCommitmenteEditItemVm, SaveCommitment>();

                ///* Research  */
                cfg.CreateMap<ResearchHistory, ResearchHistoryVm>();
                cfg.CreateMap<ResearchHistoryVm, ResearchHistoryEditVm>(); 
                cfg.CreateMap<Contributor, ContributorVm>();            

                ///* Classifications & Sufficiencies  */
                cfg.CreateMap<ClassificationsAndSufficienciesVm, SaveClassifications>();             


                ///* Supervision Loads */
                cfg.CreateMap<Domain.Model.Admin.SupervisionLoad, SupervisionLoadItemVm>();  
                cfg.CreateMap<Domain.Model.Admin.SupervisionLoad, SupervisionLoadEditVm>();
            } );

            return config.CreateMapper();
        }
    }
}
