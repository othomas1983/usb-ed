﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceManagement.Application.SupervisionLoad.ViewModels
{
    public class SupervisionLoadItemVm
    {
        #region Properties

        public int?  Id { get; set; }

        public int CVid { get;  set; }
        public string FirstSupervisor { get;  set; }
        public int SecondCVid { get;  set; }
        public string SecondSupervisor { get;  set; }
        public int? Year { get;  set; }
        public string StudentName { get;  set; }
        public string ThesisTopic { get;  set; }
        public string Role { get;  set; }
        public int ProgrammeId { get; set; }
        public string Programme { get; set; }
        public int ProgrammeOfferingId { get; set; }
        public string ProgrammeOffering { get; set; }
        public string ProgressStatus { get;  set; }
        public int Hours { get; set; }
        public decimal SupervisorCredit { get;  set; }
        public decimal SecondarySupervisorCredit { get;  set; }
        public string SupervisionResearch { get; set; }

        public string SourceSystem { get;  set; }

        #endregion
    }
}
