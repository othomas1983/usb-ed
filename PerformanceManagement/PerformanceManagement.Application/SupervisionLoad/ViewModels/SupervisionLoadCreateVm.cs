﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PerformanceManagement.Application.Admin;
using PerformanceManagement.Application.Common;
using PerformanceManagement.Data.Services;
using PerformanceManagement.Infrastructure;

namespace PerformanceManagement.Application.SupervisionLoad.ViewModels
{
    public class SupervisionLoadCreateVm : ICreateEditVm
    {
        #region Properties

        public int? CVid { get; set; }
        public int? SecondCVid => SelectedUserCvid;

        [DataType( FmDataTypes.YearDatePickerLoad ), Required]
        public int? Year { get; set; }
        [Required]
        public string StudentName { get; set; }
        [DisplayName( "Title" ), Required]
        public string ThesisTopic { get; set; }
        public string Role { get; set; } = "Supervisor";

        //[DataType( FmDataTypes.ProgrammeId ), DisplayName( "Programme" ), Required]
        //public int ProgrammeId { get; set; }

        //[DataType( FmDataTypes.ProgrammeOfferingId ), DisplayName( "Programme Offering" ), Required]
        //public int ProgrammeOfferingId { get; set; }

        [DataType( FmDataTypes.ProgressStatus ), Required]
        public string ProgressStatus { get; set; }

        [DataType("PositiveNumber"), Range(1, 9999, ErrorMessage = "{0} must be 1 or more."), Required]
        public int? SupervisorCredit { get; set; } = 1;

        [DataType( FmDataTypes.PositiveNumber )]
        public int? SecondarySupervisorCredit { get; set; }

        [DataType( FmDataTypes.FacultyUsersPickerId ), DisplayName( "Secondary supervisor" ), Required]
        public int? SelectedUserCvid { get; set; }

        public string SourceSystem { get; set; }

        #endregion


        public SupervisionLoadCreateVm( string sourceSystem, int cvId )
        {
            SourceSystem = sourceSystem;
            CVid = cvId;
        }

        public SupervisionLoadCreateVm()
        {
        }
    }
}
