﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using PerformanceManagement.Core.ViewModels.Interfaces;

namespace PerformanceManagement.Application.SupervisionLoad.ViewModels
{
    public class SupervisionLoadsVm : IViewModel
    {
        #region Properties

        public List<SelectListItem> Years { get; set; } = new List<SelectListItem>();

        public WriteHandResearch WriteHandResearch { get; set; }
        public List<SupervisionLoadItemVm> MbaLoads { get; set; }
        public List<SupervisionLoadItemVm> PhdLoads { get; set; }

        public List<SupervisionLoadItemVm> AllLoads { get; set; }

        #endregion

        public SupervisionLoadsVm(List<SupervisionLoadItemVm> loads)
        {
            MbaLoads = loads.ToList();

            AllLoads = MbaLoads.ToList();
        }

        public SupervisionLoadsVm()
        {
        }


        #region Private Methods
        private void AddToYears(int year)
        {
            var item = new SelectListItem
            {
                Value = year.ToString(),
                Text = year.ToString()
            };

            if (!Years.Select(x => x.Value).Contains(item.Value))
            {
                Years.Add(item);
            }
        }
        #endregion
    }

    public class WriteHandResearch
    {
        public List<Topic> Topics { get; set; }

        public WriteHandResearch(List<Topic> topics)
        {
            Topics = topics;
        }

        public WriteHandResearch()
        {
        }
    }

    public class Topic
    {
        public string Title { get; set; }
        public string Programme { get; set; }
        public string Status { get; set; }
        public Student Student { get; set; }
        public Supervisor Supervisor { get; set; }
    }

    public class Student
    {
        public string StudentNumber { get; set; }
        public string Name { get; set; }
        public int Year { get; set; }
    }

    public class Supervisor
    {
        public string EmployeeId { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public List<object> Credits { get; set; }
    }




}
