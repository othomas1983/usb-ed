﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PerformanceManagement.Application.Admin;
using PerformanceManagement.Application.Common;
using PerformanceManagement.Data.Services;
using PerformanceManagement.Infrastructure;

namespace PerformanceManagement.Application.SupervisionLoad.ViewModels
{
    public class SupervisionLoadEditVm : ICreateEditVm
    {
        #region Properties

        public int? CVid { get; set; }
        public int? SecondCVid { get; set; }

        [DataType( FmDataTypes.YearDatePickerLoad ), Required]
        public int? Year { get; set; }
        [Required]
        public string StudentName { get; set; }
        [DisplayName( "Title" ), Required]
        public string ThesisTopic { get; set; }

        public string Role { get; set; } = "Supervisor";
        //public string ProgrammeOffering { get; set; }

        ///// <summary>
        ///// Gets the porgrammeId using the programme text
        ///// </summary>
        //[DataType("ProgrammeId"), DisplayName("Programme"), Required]
        //public int? ProgrammeId
        //{
        //    get;set;
        //}
      
        ///// <summary>
        ///// Gets the porgramme using the lookup table and ProgrammeId.
        ///// </summary>
        //public string Programme
        //{
        //    get;set;
        //}
     


        [DataType( FmDataTypes.ProgressStatus ), Required]
        public string ProgressStatus { get; set; }

        [DataType( FmDataTypes.PositiveNumber ), Required]
        public int? SupervisorCredit { get; set; }
        [DataType( FmDataTypes.PositiveNumber ),]
        public int? SecondarySupervisorCredit { get; set; }

        [DataType( FmDataTypes.FacultyUsersPickerId ), DisplayName( "Secondary supervisor" )]
        public int? SelectedUserCvid { get; set; }

        public string SourceSystem { get; set; }

        [DataType(FmDataTypes.StringTextBoxReadOnly )]
        public string SelectedFacultyUser
        {
            get
            {
                if ( !SecondCVid.HasValue ) return null;

                var userService = new UserService( new FacultyUserDbService() );

                string fullName = userService.GetFacultyUser( SecondCVid.Value ).GetFullName();

                return fullName;
            }
        }

        #endregion


        public SupervisionLoadEditVm( string sourceSystem, int cvId )
        {
            SourceSystem = sourceSystem;
            CVid = cvId;
        }

        public SupervisionLoadEditVm()
        {
        }
    }
}
