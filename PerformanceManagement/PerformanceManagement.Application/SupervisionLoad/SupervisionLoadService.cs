﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PerformanceManagement.Application.Admin;
using PerformanceManagement.Application.Common;
using PerformanceManagement.Application.SupervisionLoad.ViewModels;
using PerformanceManagement.Core.Cache;
using PerformanceManagement.Core.Security.Identity;
using PerformanceManagement.Core.ViewModels.Interfaces;
using PerformanceManagement.Data.Interfaces;
using PerformanceManagement.Data.Models;
using PerformanceManagement.Domain.Factories;
using PerformanceManagement.Infrastructure.Network;
using Newtonsoft.Json;

namespace PerformanceManagement.Application.SupervisionLoad
{
    public class SupervisionLoadService : ApplicationService<SupervisionLoadsVm>, ISupervisionLoadService
    {
        private readonly INetworkService _networkService;
        private readonly IUserService _userService;
        private readonly ILoadsDbService _dbService;

        public SupervisionLoadService(INetworkService networkService, IUserService userService, ILoadsDbService dbService)
        {
            _networkService = networkService;
            _userService = userService;
            _dbService = dbService;
        }

        #region Public Override Methods

        public override IViewModel GetViewModel(IUser<CurrentPerson> currentPerson)
        {
            return CreateViewModel(currentPerson.CVid);
        }

        #endregion

        #region ISupervisionLoadService Methods

        public SupervisionLoadsVm FilterViewModel(IUser<CurrentPerson> currentPerson, int? year)
        {
            ModelCache.Flush<Domain.Model.Admin.SupervisionLoad>(currentPerson.CVid);
            var model = this.GetViewModel(currentPerson) as SupervisionLoadsVm;

            //if ( year.HasValue && model.WriteHandResearch.Topics != null )
            //{
            //    var filteredTopics = model.WriteHandResearch.Topics
            //        .Where( w => w.Student.Year == year ).ToList();

            //    model.WriteHandResearch = new WriteHandResearch( filteredTopics );
            //}

            return model;
        }

        #endregion

        #region IDbService Methods

        public override bool SaveChanges(ICreateEditVm model, IUser<CurrentPerson> person, IUser<CurrentUser> user)
        {
            return false;
           
        }

        public override T GetItem<T>(int itemId, IUser<CurrentPerson> person)
        {

            var model = default(T);
            object item = null;

            var loads = ModelCache.Read<Domain.Model.Admin.SupervisionLoad>(person.CVid, () => GetSupervisionLoads(person.CVid));

            if (loads.Any())
            {
                item = loads.Single(l => l.Id == itemId);
            }

            model = Mapper.Map<T>(item);
            return model;
        }

        public override bool Delete<T>(int id, IUser<CurrentPerson> person, IUser<CurrentUser> user)
        {
            bool success = _dbService.Delete<T>(id, user.Username);

            if (success)
            {
                ModelCache.Flush<Domain.Model.Admin.SupervisionLoad>(person.CVid);
            }

            return success;
        }

        #endregion

        #region Protected Methods

        protected override SupervisionLoadsVm CreateViewModel(int cvId)
        {
            // Get MBA, PhD data
            var loads = ModelCache.Read<Domain.Model.Admin.SupervisionLoad>(cvId, () => GetSupervisionLoads(cvId));
            var mappedLoads = Mapper.Map<List<SupervisionLoadItemVm>>(loads);

            return new SupervisionLoadsVm(mappedLoads);
        }

        #endregion

        /// <summary>
        /// Gets the MBA and PhD supervision loads.
        /// </summary>
        /// <param name="cVid">The cvid.</param>
        /// <returns></returns>
        private List<Domain.Model.Admin.SupervisionLoad> GetSupervisionLoads(int cvId)
        {
            var dataSet = _dbService.GetSupervisionLoads(cvId);
            var loads = ModelFactory.CreateList<Domain.Model.Admin.SupervisionLoad>(dataSet).OrderBy(t => t.Year).ToList();

            return loads;
        }
    }
}

