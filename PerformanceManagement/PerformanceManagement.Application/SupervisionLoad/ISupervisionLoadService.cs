﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PerformanceManagement.Application.Common;
using PerformanceManagement.Application.SupervisionLoad.ViewModels;
using PerformanceManagement.Core.Security.Identity;

namespace PerformanceManagement.Application.SupervisionLoad
{
    public interface ISupervisionLoadService : IApplicationService
    {
        /// <summary>
        /// Filters the write hand research with optional year filter.
        /// </summary>
        /// <param name="year">The year.</param>
        /// <param name="currentPerson">The year.</param>
        /// <returns></returns>
        SupervisionLoadsVm FilterViewModel( IUser<CurrentPerson> currentPerson, int? year  );
    }
}
