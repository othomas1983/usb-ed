﻿using System.Collections.Generic;
using PerformanceManagement.Application.Admin.ViewModels.Common;
using PerformanceManagement.Application.Common;
using PerformanceManagement.Application.Research.ViewModels;
using PerformanceManagement.Core.Security.Identity;
using PerformanceManagement.Core.ViewModels.Interfaces;
using PerformanceManagement.Infrastructure.Utilities;

namespace PerformanceManagement.Application.Research
{
    public interface IResearchService : IApplicationService
    {
       /// <summary>
        /// Gets the faculty user details.
        /// </summary>
        /// <param name="cvId">As integer.</param>
        /// <returns></returns>
        FacultyUserVm GetFacultyUserDetails( int cvId );

        /// <summary>
        /// Gets the research activity.
        /// </summary>
        /// <param name="itemId">The item identifier.</param>
        /// <returns></returns>
        ResearchHistoryEditVm GetResearchWithContributors( int itemId, IUser<CurrentPerson> person );
    }
}
