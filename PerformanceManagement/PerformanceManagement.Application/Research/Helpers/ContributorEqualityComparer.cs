﻿using System.Collections.Generic;
using PerformanceManagement.Application.Research.ViewModels;

namespace PerformanceManagement.Application.Research.Helpers
{
    /// <summary>
    /// Used to comparer to contributors. Id being the unique factor
    /// </summary>
    /// <seealso cref="ContributorVm" />
    public class ContributorEqualityComparer : IEqualityComparer<ContributorVm>
    {
        bool IEqualityComparer<ContributorVm>.Equals( ContributorVm x, ContributorVm y )
        {
            return this.Equals( x, y );
        }

        public bool Equals( ContributorVm x, ContributorVm y )
        {
            if ( !x.Id.HasValue || !y.Id.HasValue)
            {
                return false;
            }

            return x.Id == y.Id;
        }

        int IEqualityComparer<ContributorVm>.GetHashCode( ContributorVm obj )
        {
            return this.GetHashCode( obj );
        }

        public int GetHashCode( ContributorVm obj )
        {
            if ( !obj.Id.HasValue )
            {
                return 0;
            }

            return obj.Id.Value;
        }
    }
}
