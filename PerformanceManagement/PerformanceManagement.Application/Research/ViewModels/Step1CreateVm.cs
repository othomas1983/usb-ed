﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PerformanceManagement.Application.Common;

namespace PerformanceManagement.Application.Research.ViewModels
{
    public class Step1CreateVm : ICreateEditVm
    {
        #region Properties

        public int? Id { get; set; }        
        public string Description { get; set; }

        [DataType( nameof( ResearchContributionTypeId ) ), DisplayName( "Contribution Type" ), Required]
        public int ResearchContributionTypeId { get; set; }

        [DataType(nameof( ResearchStatusId ) ), DisplayName("Research Status"), Required]
        public int ResearchStatusId { get; set; }
        [Required]
        public DateTime ResearchStatusDate { get;  set; }

        [DataType( nameof( ResearchOutputCategoryId ) ), DisplayName( "Research Category" ), Required]
        public int ResearchOutputCategoryId { get;  set; }

        [DataType( nameof( LocusId ) ), DisplayName( "Local / International" ), Required]
        public int LocusId { get; set; }
        [DisplayName( "Research Title" ), Required]
        public string ArticleTitle { get; set; }
        public string PublicationName { get; set; }

        [DataType( nameof( ABSRatingStatusId ) ), DisplayName( "ABS Rating Status" ), Required]
        public int ABSRatingStatusId { get; set; }
        [DataType( nameof( DHETStatusId ) ), DisplayName( "DHET Status" ), Required]
        public int DHETStatusId { get; set; }

        public int Credit { get;  set; }

        public int? CVid { get; set; }

        #endregion

        public Step1CreateVm()
        {
        }

        public Step1CreateVm( int cvId)
        {
            CVid = cvId;
            ResearchStatusDate = DateTime.Today;            
        }
    }
}
