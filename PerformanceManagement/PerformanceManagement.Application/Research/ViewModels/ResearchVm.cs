﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PerformanceManagement.Core.ViewModels.Interfaces;
using PerformanceManagement.Domain.Model.Research;

namespace PerformanceManagement.Application.Research.ViewModels
{
    public class ResearchVm : IViewModel
    {
        public List<ResearchHistoryVm> Activities { get; }

        public ResearchVm( List<ResearchHistoryVm> activities )
        {
            Activities = activities;
        }

    }
}
