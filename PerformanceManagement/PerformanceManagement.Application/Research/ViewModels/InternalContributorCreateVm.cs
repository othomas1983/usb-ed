﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using PerformanceManagement.Application.Common;
using PerformanceManagement.Core.Cache;
using PerformanceManagement.Domain.Model.Common;
using PerformanceManagement.Domain.Model.Enums;
using PerformanceManagement.Domain.Model.Research;
using PerformanceManagement.Infrastructure.Utilities;
using PerformanceManagement.Infrastructure.Utilities.ExtensionMethods;

namespace PerformanceManagement.Application.Research.ViewModels
{
    public class InternalContributorCreateVm : ICreateEditVm
    {
        private string cvId;
        private string username;

        #region Properties

        public string Username
        {
            get
            {
                FacultyHelper.SplitUserDetails( SelectedUsernameAndCvid, out cvId, out username );
                return username;
            }
            set { }
        }

        public int? CVid
        {
            get
            {
              
                FacultyHelper.SplitUserDetails( SelectedUsernameAndCvid, out cvId, out username );
                return cvId.AsInteger();
            }
            set { }
        }

        public string SelectedUsernameAndCvid { get; set; }
        [DataType( "PositiveNumber" ), Required]
        public int Sequence { get; set; }
        [DataType( "PositiveNumber" )]
        public int DHETCredit { get; set; }        
        [DataType( "PersonRoleId" ), DisplayName( "Role" ), Required]
        public int? PersonRoleId { get; set; }
        [Required]
        public string Type { get; set; }
     

        #endregion

        public InternalContributorCreateVm()
        {
        }

        public InternalContributorCreateVm( string type )
        {
            Type = type;
        }

    }
}
