﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PerformanceManagement.Application.Common;

namespace PerformanceManagement.Application.Research.ViewModels
{
    public class Step3CreateVm : ICreateEditVm
    {
        public List<ContributorVm> Contributors{ get; set; } = new List<ContributorVm>();

        public int? CVid { get; set; }
    }
}
