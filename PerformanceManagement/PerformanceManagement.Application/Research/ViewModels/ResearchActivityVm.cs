﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceManagement.Application.Research.ViewModels
{
    public class ResearchActivityVm
    {
        #region Properties

        public int Id { get; set; }
        public string Description { get; set; }
        public DateTime? ResearchStatusDate { get;  set; }
        public string ResearchStatusDescription { get;  set; }
        public string ResearchOutputCategoryDescription { get;  set; }
        public string ABSRatingStatusDescription { get;  set; }
        public string DHETStatusDescription { get;  set; }
        public string ArticleTitle { get;  set; }
        public string PublicationName { get;  set; }
        public DateTime? PublicationDate { get; set; }

        public int Credit { get;  set; }

        public int CVid { get; set; }

        #endregion
    }
}
