﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using PerformanceManagement.Application.Common;
using PerformanceManagement.Core.Cache;
using PerformanceManagement.Domain.Model.Common;
using PerformanceManagement.Domain.Model.Enums;

namespace PerformanceManagement.Application.Research.ViewModels
{
    public class ResearchActivityCreateVm : ICreateEditVm
    {
        #region Properties

        public List<ContributorVm> Contributors { get; set; } = new List<ContributorVm>();

        public int? Id { get; set; }
        public string Description { get; set; }

        [DataType( nameof( ResearchContributionTypeId ) ), DisplayName( "Contribution Type" ), Required]
        public int ResearchContributionTypeId { get; set; }

        [DataType( nameof( ResearchStatusId ) ), DisplayName( "Research Status" ), Required]
        public int ResearchStatusId { get; set; }
        [Required]
        public DateTime ResearchStatusDate { get; set; }

        [DataType( nameof( ResearchOutputCategoryId ) ), DisplayName( "Research Category" ), Required]
        public int ResearchOutputCategoryId { get; set; }

        [DataType( nameof( LocusId ) ), DisplayName( "Local / International" ), Required]
        public int LocusId { get; set; }
        [DisplayName( "Research Title" ), Required]
        public string ArticleTitle { get; set; }
        public string PublicationName { get; set; }

        [DataType( nameof( ABSRatingStatusId ) ), DisplayName( "ABS Rating Status" ), Required]
        public int ABSRatingStatusId { get; set; }
        [DataType( nameof( DHETStatusId ) ), DisplayName( "DHET Status" ), Required]
        public int DHETStatusId { get; set; }
        
        [Required]
        public string PublisherName { get; set; }

        public DateTime? PublicationDate { get; set; }
        [DataType( "PositiveNumber" ), Required]
        public int Credit { get; set; }
        [DataType( "PositiveNumber" ), DisplayName( "Total Pages in Book" )]
        public int NoOfPages { get; set; }
        [DataType( "PositiveNumber" ), DisplayName( "Pages From" )]
        public int StartPage { get; set; }
        [DataType( "PositiveNumber" ), DisplayName( "Pages To" )]
        public int EndPage { get; set; }
        [DataType( "PositiveNumber" )]
        public int Volume { get; set; }
        [MaxLength( 50 )]
        public string Edition { get; set; }
        [MaxLength( 10 )]
        public string Issue { get; set; }
        public string City { get; set; }

        [DataType( "NationalityId" ), DisplayName( "Country" )]
        public int? CountryId { get; set; }

        /// <summary>
        /// Gets the country using the lookup table and countryId.
        /// </summary>
        /// <value>
        /// The country.
        /// </value>
        public string Country
        {
            get
            {
                if ( !CountryId.HasValue ) return string.Empty;

                var data = DropDownsCache.Read<Nationality>( DropDownType.Nationalities );
                return data.FirstOrDefault( x => x.Id == CountryId.Value )?.Description;
            }
        }
        
        [DataType( DataType.Upload )]
        public HttpPostedFileBase PublicationUpload { get; set; }

        [DataType( DataType.MultilineText )]
        public string Note { get; set; }
        [DisplayName("Add Me as Contributor?")]
        public bool AddSelfAsContributor { get; set; }
        public int? CVid { get; set; }

        #endregion


        public ResearchActivityCreateVm()
        {
        }

        public ResearchActivityCreateVm( int cvId )
        {
            CVid = cvId;
            ResearchStatusDate = DateTime.Today;
        }
    }
}
