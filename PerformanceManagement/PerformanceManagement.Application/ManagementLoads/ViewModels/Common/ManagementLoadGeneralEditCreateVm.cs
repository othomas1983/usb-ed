﻿using System;
using System.ComponentModel.DataAnnotations;
using PerformanceManagement.Application.Common;
using PerformanceManagement.Domain.Model.Enums;
using PerformanceManagement.Infrastructure.Utilities.ExtensionMethods;

namespace PerformanceManagement.Application.ManagementLoads.ViewModels.Common
{
    public class ManagementLoadGeneralEditCreateVm : ManagementLoadBaseVm, ICreateEditVm
    {
        #region Properties

        public string Heading { get; set; }

        [Required]
        public string Description { get; set; }

        public int ManagementLoadCategoryId { get; set; }

        #endregion

        public ManagementLoadGeneralEditCreateVm( int cVid, int managementLoadCategoryId )
        {
            ManagementLoadCategoryId = managementLoadCategoryId;
            StartDate = DateTime.UtcNow.AddDays( -1 );
            EndDate = DateTime.UtcNow;
            CVid = cVid;
        }

        public ManagementLoadGeneralEditCreateVm()
        {
        }


        public string GetSectionHeading()
        {
            return Heading ?? "Management Load";
        }
    }
}
