﻿using PerformanceManagement.Application.Common;
using PerformanceManagement.Application.ManagementLoads.ViewModels.Common;
using PerformanceManagement.Core.Cache;
using PerformanceManagement.Domain.Model.Enums;
using PerformanceManagement.Domain.Model.Loads.Common;
using PerformanceManagement.Infrastructure;
using PerformanceManagement.Infrastructure.Utilities.ExtensionMethods;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;

namespace PerformanceManagement.Application.ManagementLoads.ViewModels.ProgrammeHead
{
    public class ProgrammeHeadCreateVm : ManagementLoadBaseVm, ICreateEditVm
    {
        #region Properties

        [HiddenInput]
        public int ManagementLoadCategoryId { get; set; } = ManagementLoadType.ProgrammeHead.ConvertToInt();
              
        public int ElectiveModuleCount { get; set; } = 1;
               
        public int ElectiveSAQACredit { get; set; } = 1;

        //Change ddropdown to use Degree instead

        //[DataType(FmDataTypes.ProgrammeId), DisplayName("Programme"), Required]
        public int? ProgrammeId { get; set; }

        ///// <summary>
        ///// Gets the porgramme using the lookup table and ProgrammeId.
        ///// </summary>
        public string Programme
        {
            get
            {
                if (!ProgrammeId.HasValue) return string.Empty;

                var data = DropDownsCache.Read<Programme>(DropDownType.Programmes);
                return data.FirstOrDefault(x => x.Id == ProgrammeId.Value)?.Description;
            }
        }

        [DataType(FmDataTypes.DegreeId), DisplayName("Degree"), Required]
        public int? DegreeId { get; set; }
        /// <summary>
        /// Gets the porgramme using the lookup table and ProgrammeId.
        /// </summary>
        public string Degree
        {
            get
            {
                if (!DegreeId.HasValue) return string.Empty;

                var data = DropDownsCache.Read<Domain.Model.Common.Degree>(DropDownType.Degrees);
                return data.FirstOrDefault(x => x.Id == DegreeId.Value)?.Description;
            }
        }
        #endregion

        public ProgrammeHeadCreateVm(int cVid)
        {
            StartDate = DateTime.UtcNow.AddDays(-1);
            EndDate = DateTime.UtcNow;
            CVid = cVid;
        }

        public ProgrammeHeadCreateVm()
        {
        }
    }
}
