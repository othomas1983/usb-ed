﻿using System.Linq;
using PerformanceManagement.Application.ManagementLoads.ViewModels.Common;
using PerformanceManagement.Core.Cache;
using PerformanceManagement.Domain.Model.Enums;
using PerformanceManagement.Domain.Model.Loads.Common;

namespace PerformanceManagement.Application.ManagementLoads.ViewModels.ProgrammeHead
{
    public class ProgrammeHeadVm : ManagementLoadBaseVm
    {
        #region Properties
        //Changed drop down to use Degree Id

        public int? ProgrammeId { get; set; }
        /// <summary>
        /// Gets the porgramme using the lookup table and ProgrammeId.
        /// </summary>
        public string Programme
        {
            get
            {
                if (!ProgrammeId.HasValue) return string.Empty;

                var data = DropDownsCache.Read<Programme>(DropDownType.Programmes);
                return data.FirstOrDefault(x => x.Id == ProgrammeId.Value)?.Description;
            }
        }

        public int? DegreeId { get; set; }
        /// <summary>
        /// Gets the porgramme using the lookup table and ProgrammeId.
        /// </summary>
        public string Degree
        {
            get
            {
                if (!DegreeId.HasValue) return string.Empty;

                var data = DropDownsCache.Read<Domain.Model.Common.Degree>(DropDownType.Degrees);
                return data.FirstOrDefault(x => x.Id == DegreeId.Value)?.Description;
            }
        }
        public string ProgrammeSize { get; set; }
        public int ElectiveModuleCount { get;  set; }
        public int ElectiveSAQACredit { get;  set; }        

        #endregion
    }
}
