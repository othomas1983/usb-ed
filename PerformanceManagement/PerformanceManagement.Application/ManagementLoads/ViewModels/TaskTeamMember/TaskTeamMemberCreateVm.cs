﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PerformanceManagement.Application.Common;
using PerformanceManagement.Application.ManagementLoads.ViewModels.Common;
using PerformanceManagement.Domain.Model.Enums;
using PerformanceManagement.Infrastructure.Utilities.ExtensionMethods;

namespace PerformanceManagement.Application.ManagementLoads.ViewModels.TaskTeamMember
{
    public class TaskTeamMemberCreateVm : ManagementLoadBaseVm, ICreateEditVm
    {    
        #region Properties
        
        [DataType( "TaskTeamActivityId" ), Required, DisplayName( "Task Team Member" )]
        public int TaskTeamActivityId { get; set; }
        [Required]
        public string Description { get; set; }
        public int ManagementLoadCategoryId { get; set; } = ManagementLoadType.TaskTeamMember.ConvertToInt();

        #endregion

        public TaskTeamMemberCreateVm( int cVid )
        {
            StartDate = DateTime.UtcNow.AddDays( -1 );
            EndDate = DateTime.UtcNow;
            CVid = cVid;
        }

        public TaskTeamMemberCreateVm()
        {
        }
    }
}
