﻿
using System.Collections.Generic;
using PerformanceManagement.Application.Authentication.ViewModels;
using PerformanceManagement.Domain.Model.Admin;

namespace PerformanceManagement.Application.Authentication
{
    public interface IAuthService
    {
        /// <summary>
        /// Authenticates the specified user.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        bool Authenticate( LoginVm model, out string errorMessage );

        /// <summary>
        /// Gets the auths for a user
        /// </summary>
        /// <param name="cvid">The cvid.</param>
        /// <returns></returns>
        List<Auth> GetAuths( int cvid );
    }
}