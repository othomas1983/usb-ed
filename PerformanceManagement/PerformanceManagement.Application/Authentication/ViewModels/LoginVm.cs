﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace PerformanceManagement.Application.Authentication.ViewModels
{
    public class LoginVm
    {
        [Required]
        [UIHint("Username")]
        [DataType("UsersId"),DisplayName("Faculty Member")]
        public string UserDropDown { get; set; }
        public string  Username { get; set; }

        [Required]
        [DataType( DataType.Password )]
        public string Password { get; set; }


    }
}
