﻿using AutoMapper;
using PerformanceManagement.Application.WebUsers.ViewModels;
using PerformanceManagement.Core.Cache;
using PerformanceManagement.Core.Security.Identity;
using PerformanceManagement.Data.Interfaces;
using PerformanceManagement.Domain.Factories;
using PerformanceManagement.Domain.Model.Common;
using System.Collections.Generic;
using System.Linq;

namespace PerformanceManagement.Application.WebUsers
{
    public sealed class WebUserService : IWebUserService
    {
        private readonly IWebUserDbService _dbService;
        private readonly IMapper _mapper;

        public WebUserService(IWebUserDbService dbService, IMapper mapper)
        {
            _dbService = dbService;
            _mapper = mapper;
        }
       
        public WebUserVm GetViewModel(IUser<CurrentUser> currentUser)
        {
            var availableUsers = ViewModelCache.Read(currentUser.CVid, () => CreateViewModel(currentUser.CVid));
            var model = new WebUserVm(availableUsers);

            return model;
        } 
      
        private  WebUserVm CreateViewModel(int CVID)
        {
            var webUserDs = _dbService.GetWebUsers(CVID);
            var webUsers = ModelFactory.CreateList<WebUser>(webUserDs).ToList();

            var mappedItems = _mapper.Map<List<WebUserItemVm>>(webUsers);

           return new WebUserVm(mappedItems);
        }

        public IEnumerable<WebUser> GetWebUsers(int cvId, bool displayOnWeb)
        {
            var webUserDs = _dbService.GetWebUsers(cvId, displayOnWeb);

            var webUsers = ModelFactory.CreateList<WebUser>(webUserDs).ToList();

            return webUsers;
        }

        public void UpdateUserDisplayOnWebOn(IEnumerable<int> cvIds)
        {
            _dbService.UpdateUserWebDisplayState(cvIds, true);
        }
        
        public void UpdateUserDisplayOnWebOff(IEnumerable<int> cvIds)
        {
            _dbService.UpdateUserWebDisplayState(cvIds, false);
        }

      
    }
}