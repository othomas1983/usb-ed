﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using PerformanceManagement.Core.ViewModels.Interfaces;

namespace PerformanceManagement.Application.WebUsers.ViewModels
{
    //public class WebUserVm : IViewModel
    //{
    //    private WebUserVm availableUsers;

    //    public MultiSelectList AvailableUsers { get; set; }

    //    public int CvId { get; set; }
    //    public bool WebDisplay { get; set; }
    //     public List<string> selectedCVIds { get; set; }

    //    //public WebUserVm( IEnumerable<Domain.Model.Common.WebUser> users )
    //    //{
    //    //    AvailableUsers = new MultiSelectList( users, "CVID", "Surname");
    //    //}

    //    public WebUserVm(WebUserVm availableUsers)
    //    {
    //        AvailableUsers = new MultiSelectList("CVID", "Surname");
    //    }
    //    public List<WebUserItemVm> WebUserItems { get; set; }

    //    public WebUserVm()
    //    {

    //    }

    //    public WebUserVm(List<WebUserItemVm> webusers)
    //    {
    //        WebUserItems = webusers;
    //    }

    //    public List<WebUserVm> WebUsers { get; }

    public class WebUserVm : IViewModel
    {
        #region Properties

        //public int CVID { get; set; }
        //public string Name { get; set; }
        //public string Surname { get; set; }
        //public bool WebDisplay { get; set; }

        #endregion
        public List<string> selectedCVIds { get; set; }
        public List<WebUserItemVm> WebUsers { get; set; }

        public SelectList SelectedEmployeeIdAdd { get; set; }
        public SelectList SelectedEmployeeIdRemove { get; set; }
        public WebUserVm(List<WebUserItemVm> webUsers)
        {
            WebUsers = webUsers;

           
        }
        public MultiSelectList AvailableUsers { get; set; }
        public WebUserVm(WebUserVm availableUsers)
        {           
            AvailableUsers = new MultiSelectList("CVID", "Description");
        }
        public WebUserVm()
        {
        }
        #region Private Methods

        #endregion
    }
}
