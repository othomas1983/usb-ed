using PerformanceManagement.Application.Common;
using PerformanceManagement.Application.WebUsers.ViewModels;
using PerformanceManagement.Core.Security.Identity;
using PerformanceManagement.Domain.Model.Common;
using System.Collections.Generic;

namespace PerformanceManagement.Application.WebUsers
{
    public interface IWebUserService
    {
        /// <summary>
        /// Gets all Web Users
        /// </summary>      
        /// <param name="currentUser">currentUser</param>
        /// <returns></returns>
        WebUserVm GetViewModel(IUser<CurrentUser> currentUser);


        IEnumerable<WebUser> GetWebUsers(int cvId, bool displayOnWeb);

        void UpdateUserDisplayOnWebOn(IEnumerable<int> cvIds);


        void UpdateUserDisplayOnWebOff(IEnumerable<int> cvIds);


    }
}
