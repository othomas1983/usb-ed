﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PerformanceManagement.Application.Admin.ViewModels.Common;
using PerformanceManagement.Domain.Model.Common;

namespace PerformanceManagement.Application.Admin
{
    public class AddUsersResult
    {
        public bool Success { get; set; } = false;

        public FacultyUserVm FacultyUser { get; set; }
    }
}
