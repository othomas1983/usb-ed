﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PerformanceManagement.Application.Admin.ViewModels.Common;
using PerformanceManagement.Application.Common;
using PerformanceManagement.Domain.Model.Admin;
using PerformanceManagement.Domain.Model.Common;

namespace PerformanceManagement.Application.Admin.ViewModels
{
    public class ClassificationsAndSufficienciesVm : ICreateEditVm
    {
        #region Properties

        [DataType( "FacultyClassificationId" ),DisplayName( "Faculty Classification" ), Required]
        public int? ClassificationId { get; set; }     

        public string Username { get; set; }
        public int? CVid { get; set; }
        
        public FacultyUserVm FacultyUser { get; set; }
        #endregion
        bool success { get; set; }
        public ClassificationsAndSufficienciesVm( FacultyUser user, ClassificationAndSufficiency classification )
        {
            Username = user.SamAccountName;
            CVid = user.CVid;
            ClassificationId = classification.ClassificationId;
            //SufficiencyId = classification.SufficiencyId;

            FacultyUser = new FacultyUserVm( user );
        }

        public ClassificationsAndSufficienciesVm()
        {
        }
    }

}
