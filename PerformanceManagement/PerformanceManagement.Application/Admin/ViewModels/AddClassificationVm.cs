﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PerformanceManagement.Core.ViewModels.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using PerformanceManagement.Application.Admin;
using PerformanceManagement.Application.Common;
using PerformanceManagement.Data.Services;
using PerformanceManagement.Infrastructure;
using PerformanceManagement.Infrastructure.Utilities;

namespace PerformanceManagement.Application.Admin.ViewModels
{
    public class AddClassificationVm : IViewModel
    {

        public int? CVid { get; set; }
        public int? SecondCVid { get; set; }
        [DataType(FmDataTypes.FacultyUsersPickerId), DisplayName("Select Faculty Member")]
        public int? SelectedUserCvid { get; set; }
        [DataType(FmDataTypes.StringTextBoxReadOnly)]
        public string SelectedFacultyUser
        {
            get
            {
                if (!SecondCVid.HasValue) return null;

                var userService = new UserService(new FacultyUserDbService());

                string fullName = userService.GetFacultyUser(SecondCVid.Value).GetFullName();

                return fullName;
            }
        }
    }
}
