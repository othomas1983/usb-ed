﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PerformanceManagement.Application.Admin.ViewModels.Common;
using PerformanceManagement.Domain.Model.Common;

namespace PerformanceManagement.Application.Admin.ViewModels
{
    public class AuthDetailGrouping
    {
        public FacultyUserVm Manager { get; set; }
        public int ManagerCVid { get; set; }
        public string Action { get; set; }
        public string Feature { get; set; }

        public AuthDetailGrouping( FacultyUser manager, string feature, string action )
        {
            Feature = feature;
            Action = action;
            Manager = new FacultyUserVm( manager );
        }
    }
}
