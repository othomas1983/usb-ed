﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PerformanceManagement.Application.Admin.ViewModels.Common;
using PerformanceManagement.Domain.Model.Admin;
using PerformanceManagement.Domain.Model.Common;

namespace PerformanceManagement.Application.Admin.ViewModels
{
    public class AuthVm
    {
        #region Properties

        public FacultyUserVm User { get; set; }        
        public List<AuthDetailGrouping> AuthDetails { get; }
        public int CVid { get;  set; }


        #endregion

        public AuthVm( )
        {
        }

        public AuthVm( FacultyUser user, List<AuthDetailGrouping> authDetails )
        {
            CVid = user.CVid;
            User = new FacultyUserVm( user );
            AuthDetails = authDetails;            
        }
    }
}
