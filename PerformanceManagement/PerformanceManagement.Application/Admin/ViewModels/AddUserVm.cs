﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using PerformanceManagement.Core.ViewModels.Interfaces;
using PerformanceManagement.Domain.Model;

namespace PerformanceManagement.Application.Admin.ViewModels
{
    public class AddUserVm : IViewModel
    {
        public MultiSelectList AvailableUsers { get; set; }
        [Required]
        public List<string> SelectedEmployeeIds { get; set; }

        public AddUserVm( IEnumerable<ActiveDirectoryUser> users )
        {
            AvailableUsers = new MultiSelectList( users, "EmployeeId", "DisplayName" );
        }

        public AddUserVm()
        {
        }
    }
}
