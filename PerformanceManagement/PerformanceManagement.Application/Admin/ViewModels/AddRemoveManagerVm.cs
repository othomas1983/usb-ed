﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using PerformanceManagement.Core.Attributes;
using PerformanceManagement.Core.ViewModels.Interfaces;
using PerformanceManagement.Domain.Model;

namespace PerformanceManagement.Application.Admin.ViewModels
{
    public class AddRemoveManagerVm : IViewModel
    {
        public VmType Type { get; set; }
        public MultiSelectList AvailableUsers { get; set; }
        [CannotBeEmpty]
        public List<string> SelectedSamAccountNames { get; set; }

        public AddRemoveManagerVm( IEnumerable<ActiveDirectoryUser> users, VmType type )
        {
            Type = type;
            AvailableUsers = new MultiSelectList( users, "SamAccountName", "DisplayName" );
        }

        public AddRemoveManagerVm()
        {
        }
    }

    public enum VmType
    {
        Add,
        Remove
    }
}
