﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PerformanceManagement.Application.Admin.ViewModels;
using PerformanceManagement.Application.Admin.ViewModels.Common;
using PerformanceManagement.Application.Authentication;
using PerformanceManagement.Application.Common;

using PerformanceManagement.Core.Cache;
using PerformanceManagement.Core.Security.Identity;
using PerformanceManagement.Core.ViewModels.Interfaces;
using PerformanceManagement.Data.Interfaces;
using PerformanceManagement.Data.Models;
using PerformanceManagement.Domain.Factories;
using PerformanceManagement.Domain.Model;
using PerformanceManagement.Domain.Model.Admin;
using PerformanceManagement.Domain.Model.Common;
using PerformanceManagement.Domain.Model.Enums;
using PerformanceManagement.Infrastructure;
using PerformanceManagement.Infrastructure.Utilities;
using PerformanceManagement.Infrastructure.Utilities.ExtensionMethods;
using System.Net;

namespace PerformanceManagement.Application.Admin
{
    public class AdminService : ApplicationService<AddUserVm>, IAdminService
    {
        private readonly IAdminDbService _dbService;
        private readonly IUserService _userService;
        private readonly IAuthService _authService;

        public AdminService( IAdminDbService dbService, IUserService userService, IAuthService authService )
        {
            _dbService = dbService;
            _userService = userService;
            _authService = authService;
        }

        #region Add Users

        /// <summary>
        /// Gets the add user view model.
        /// </summary>        
        /// <param name="currentUser"></param>
        /// <returns></returns>
        public IViewModel GetAddUserViewModel( IUser<CurrentUser> currentUser )
        {
            var facultyUsers = _userService.GetFacultyUsers( currentUser );
            var activeDirectoryUsers = _userService.GetActiveDirectoryUsers();
            // Gets the active directory users not in faculty management.
            var availableUsers = activeDirectoryUsers.Where( a => !facultyUsers.Select( f => f.EmployeeId ).Contains( a.EmployeeId ) );

            var model = new AddUserVm( availableUsers );

            return model;
        }


        public AddUsersResult AddUsers( IEnumerable<string> selectedEmployeeIds, IUser<CurrentPerson> currentPerson )
        {
            var result = new AddUsersResult();
            // Get selected users from the Active Directory Users Cache
            var newUsers = from user in _userService.GetActiveDirectoryUsers()
                                   where selectedEmployeeIds.Contains( user.EmployeeId )
                                   select user;

            result.Success = _dbService.AddUsers( newUsers, currentPerson.Username );
            // Reset Users Cache as the DB has changed
            if ( result.Success )
            {
                UsersCache.Flush<FacultyUser>();
                UsersCache.Flush<ActiveDirectoryUser>();

                // Set the first user so we can load their profile later
                var facultyUser = _userService.GetFacultyUser( selectedEmployeeIds.First() );
                // This check is needed as not all FM users have Employee Id set :(
                if ( facultyUser != null )
                {
                    result.FacultyUser = new FacultyUserVm( facultyUser );
                }

            }

            return result;
        }

        #endregion

        #region Add Manager

        /// <summary>
        /// Gets the add manager view model.
        /// </summary>
        /// <param name="currentUser">The current user.</param>
        /// <returns></returns>
        public IViewModel GetAddManagerViewModel( IUser<CurrentUser> currentUser )
        {
            // Get all users that are not managers i.e. in AD group : FM Manager
            var availableUsers = _userService.GetActiveDirectoryUsers( includeGroups: true ).Where( a => !a.IsManager() );

            return new AddRemoveManagerVm( availableUsers, VmType.Add );            
        }

        /// <summary>
        /// Gets the remove manager view model.
        /// </summary>
        /// <param name="currentUser">The current user.</param>
        /// <returns></returns>
        public IViewModel GetRemoveManagerViewModel( IUser<CurrentUser> currentUser )
        {
            // Get all users that are managers i.e. in AD group : FM Manager
            var availableUsers = _userService.GetActiveDirectoryUsers( includeGroups: true ).Where( a => a.IsManager() );

            return new AddRemoveManagerVm( availableUsers, VmType.Remove );
        }

        /// <summary>
        /// Adds the users to the FM Manager Active Directory Group.
        /// </summary>
        /// <param name="usernames">The selected usernames.</param>
        public void AddManagers( List<string> usernames )
        {
             _userService.AddUsersToGroup( usernames, ADGroups.Manager );
        }

        /// <summary>
        /// Removes the users to the FM Manager Active Directory Group.
        /// </summary>
        /// <param name="usernames">The selected usernames.</param>
        public void RemoveManagers( List<string> usernames )
        {
            _userService.RemoveUsersFromGroup( usernames, ADGroups.Manager );
        }

        /// <summary>
        /// Gets the auths.
        /// </summary>
        /// <param name="cvid">The cvid.</param>
        /// <returns></returns>
        public AuthVm GetAuths( int cvid )
        {
            // Get Auths grouped by manager
            var authGroupByManager = _authService.GetAuths( cvid ).GroupBy( a => a.ManagerCVid );
            
            List<AuthDetailGrouping> authDetails = new List<AuthDetailGrouping>();
            foreach ( var authGroup in authGroupByManager )
            {
                foreach ( var auth in authGroup )
                {
                    var manager = _userService.GetFacultyUser( authGroup.Key );
                    authDetails.Add( new AuthDetailGrouping( manager, auth.Feature, auth.Action ) );
                }
            }

            var facultyUser = _userService.GetFacultyUser( cvid );

            var model = new AuthVm( facultyUser, authDetails);

            return model;
        }

        #endregion

        public IViewModel GetSelectUserViewModel()
        {
            return new UserSelectBaseVm();
        }

        #region Add Organization

        public OrganisationAndRolesVm GetOrganisations( int cvId )
        {
            var user = _userService.GetFacultyUser( cvId );

            var departmentsDs = _dbService.GetFacultyDepartments( cvId );
            // Store users selected categories in cache for use later
            var departments = ModelCache.Read( cvId, () => ModelFactory.CreateList<FacultyDepartment>( departmentsDs ) );

            return new OrganisationAndRolesVm( user, departments );
        }

        #endregion

        #region Add Classifications

      
        public ClassificationsAndSufficienciesVm GetClassificationsAndSufficiencies( int cvId,int? classificationId )
        {
            if (classificationId == 0)
            {
                //populate current saved 
                var user = _userService.GetFacultyUser(cvId);

                var classificationsDs = _dbService.GetClassificationsAndSufficiencies(cvId, 0);
                // Store users selected values in cache for use later
                var classification =  new ClassificationAndSufficiency(classificationsDs);

                return new ClassificationsAndSufficienciesVm(user, classification);
            }
            else
            {

                //Add new classification
                var user = _userService.GetFacultyUser(cvId);

                var classificationsDs = _dbService.GetClassificationsAndSufficiencies(cvId,classificationId);
                // Store users selected values in cache for use later
                var classification = ModelCache.Read(cvId, () => new ClassificationAndSufficiency(classificationsDs));

                return new ClassificationsAndSufficienciesVm(user, classification);
            }
           
        }

        #endregion

        #region Override Methods

       


        #endregion

        /// <summary>
        /// Names AutoComplete Search
        /// </summary>       
        /// <param name="term">The term.</param>        
        /// <param name="currentUser"></param>
        /// <param name="includeCurrentUser"></param>
        /// <returns></returns>
        public IEnumerable<AutoCompleteResult> NameAutoComplete( string term, IUser<CurrentUser> currentUser, bool includeCurrentUser = false )
        {
            var users = _userService.GetFacultyUsers( currentUser, includeCurrentUser );

            var filteredUsers = users.Where( u => u.Surname.ContainsCaseInsensitive( term ) || u.Description.ContainsCaseInsensitive( term ) );

            var results = filteredUsers.Select( x => new AutoCompleteResult
            {
                id = x.CVid.ToString(),
                value = x.SamAccountName + ":" + x.CVid.ToString(),
                label = x.Surname + ", " + x.Description,
            } );

            return results;
        }

        #region Executive Profile

        /// <summary>
        /// Gets the executive profile.
        /// </summary>
        /// <param name="person">The current person.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public ExecutiveProfileVm GetExecutiveProfile( IUser<CurrentPerson> person )
        {
            var user = _userService.GetFacultyUser( person.CVid );

            var model = new ExecutiveProfileVm( user );

            return model;
        }

        #endregion

        #region Process Methods

        /// <summary>
        /// Processes the classifications and sufficiencies.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="person">The person.</param>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        private bool ProcessClassificationsAndSufficiencies( ClassificationsAndSufficienciesVm model, IUser<CurrentPerson> person, IUser<CurrentUser> user )
        {
            var dbModel = Mapper.Map<SaveClassifications>( model );

            bool success = _dbService.SaveChanges( dbModel, user.Username );

            if ( success )
            {
                ModelCache.Flush<ClassificationAndSufficiency>( person.CVid );
            }

            return success;
        }

      
     
        #endregion
    }
}