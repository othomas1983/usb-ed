using System.Collections.Generic;
using PerformanceManagement.Application.Admin.ViewModels;
using PerformanceManagement.Application.Common;
using PerformanceManagement.Core.Security.Identity;
using PerformanceManagement.Core.ViewModels.Interfaces;
using PerformanceManagement.Domain.Model;
using PerformanceManagement.Infrastructure.Utilities;

namespace PerformanceManagement.Application.Admin
{
    public interface IAdminService : IApplicationService
    {
        /// <summary>
        /// Gets the add user view model.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        IViewModel GetAddUserViewModel( IUser<CurrentUser> user );

        /// <summary>
        ///  Names AutoComplete Search.
        /// </summary>
        /// <param name="term">The term.</param>
        /// <param name="currentPerson">The current person.</param>
        /// <param name="includeCurrentUser"></param>
        /// <returns></returns>
        IEnumerable<AutoCompleteResult> NameAutoComplete( string term, IUser<CurrentUser> user,  bool includeCurrentUser = false );

        /// <summary>
        /// Adds the users  to Faculty Management
        /// </summary>
        /// <param name="selectedEmployeeIds">The selected employee ids.</param>
        /// <param name="currentPerson"></param>
        /// <returns></returns>
        AddUsersResult AddUsers( IEnumerable<string> selectedEmployeeIds, IUser<CurrentPerson> currentPerson );

        /// <summary>
        /// Adds the users to the FM Manager Active Directory Group.
        /// </summary>
        /// <param name="usernames">The selected usernames.</param>        
        /// <returns></returns>
        void AddManagers( List<string> usernames );

        /// <summary>
        /// Removes the users to the FM Manager Active Directory Group.
        /// </summary>
        /// <param name="usernames">The selected usernames.</param>        
        /// <returns></returns>
        void RemoveManagers( List<string> usernames );

        /// <summary>
        /// Gets the select user view model.
        /// </summary>
        /// <returns></returns>
        IViewModel GetSelectUserViewModel();


        /// <summary>
        /// Gets the organisations ( faculty departments) for a user.
        /// </summary>
        /// <param name="cvId">The cv identifier.</param>
        /// <returns></returns>
        OrganisationAndRolesVm GetOrganisations( int cvId );

        /// <summary>
        /// Gets the classifications and sufficiencies.
        /// </summary>
        /// <param name="cvId">The cv identifier.</param>
        /// <returns></returns>
        ClassificationsAndSufficienciesVm GetClassificationsAndSufficiencies( int cvId, int? classificationId);

        /// <summary>
        /// Gets the executive profile.
        /// </summary>
        /// <param name="currentPerson">The current person.</param>
        /// <returns></returns>
        ExecutiveProfileVm GetExecutiveProfile( IUser<CurrentPerson> currentPerson );

        /// <summary>
        /// Gets the add manager view model.
        /// </summary>
        /// <param name="currentUser">The current user.</param>
        /// <returns></returns>
        IViewModel GetAddManagerViewModel( IUser<CurrentUser> currentUser );


        /// <summary>
        /// Gets the remove manager view model.
        /// </summary>
        /// <param name="currentUser">The current user.</param>
        /// <returns></returns>
        IViewModel GetRemoveManagerViewModel( IUser<CurrentUser> currentUser );

        AuthVm GetAuths( int cvid );
        //bool SaveChanges(ClassificationsAndSufficienciesVm model, int? cVid, IUser<CurrentUser> currentUser);
        //bool UpdateClassifications(ClassificationsAndSufficienciesVm model, int? cVid, IUser<CurrentUser> currentUser);
    }
}
