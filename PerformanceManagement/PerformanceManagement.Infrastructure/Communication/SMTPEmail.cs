﻿using System.Configuration;
using System.Net.Mail;
using PerformanceManagement.Infrastructure.Utilities.ExtensionMethods;

namespace PerformanceManagement.Infrastructure.Communication
{
    public class SMTPEmail
    {
        private static string Server { get; set; } = ConfigurationManager.AppSettings["SMTPServer"];
        private static int Port { get; set; } = ConfigurationManager.AppSettings["Port"].AsIntegerOrNull() ?? 25; 
        private static string Username { get; set; } = ConfigurationManager.AppSettings["Username"];
        private static string Password { get; set; } = ConfigurationManager.AppSettings["Password"];
        private static bool UseSsl { get; set; } = ConfigurationManager.AppSettings["UseSsl"].AsBooleanOrNull() ?? false;

        public static void Send( string recipient, string from, string subject, string body )
        {
            MailMessage message = new MailMessage();

            // set from 
            if ( !string.IsNullOrWhiteSpace( from ) )
            {
                message.From = new MailAddress( from );
            }

            message.IsBodyHtml = true;
            message.Priority = MailPriority.Normal;
            message.To.Add( recipient );
            message.Subject = subject;
            message.Body = body;

            using ( var smtpClient = GetSmtpClient() )
            {
                smtpClient.Send( message );
            }

        }

        /// <summary>
        /// Creates an SmtpClient using this Server, Port and SSL settings.
        /// </summary>
        /// <returns></returns>
        private static SmtpClient GetSmtpClient()
        {
            // Create SMTP Client
            SmtpClient smtpClient = new SmtpClient( Server, Port );
            smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtpClient.EnableSsl = UseSsl;

            if ( !string.IsNullOrEmpty( Username ) )
            {
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = new System.Net.NetworkCredential( Username, Password );
            }

            return smtpClient;
        }
    }

   
}
