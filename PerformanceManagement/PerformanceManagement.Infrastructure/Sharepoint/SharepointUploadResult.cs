﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceManagement.Infrastructure.Sharepoint
{
    public class SharepointUploadResult
    {
        public int DocumentId { get; set; }
        public string Url { get; set; }
        public string Result { get; set; }
        public bool Success { get; set; }
        public string ResultDetails { get; set; }
    }
}
