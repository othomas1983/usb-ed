﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceManagement.Infrastructure.Network
{
    /// <summary>
    /// Provides functions to read data from web services
    /// </summary>
    public interface INetworkService
    {
        /// <summary>
        /// Reads the write hand data as json format.
        /// </summary>
        /// <param name="employeeId">The employee identifier.</param>
        /// <returns></returns>
        string ReadWriteHandData( string employeeId );
    }
}
