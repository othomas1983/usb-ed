﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceManagement.Infrastructure.Utilities
{
    public enum WorkflowStep
    {
        One = 1,
        Two = 2,
        Three = 3
    }
}
