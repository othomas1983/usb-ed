﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceManagement.Infrastructure.Utilities
{
    public class FacultyHelper
    {
        /// <summary>
        /// Splits the user details into username and cvid.
        /// </summary>
        /// <param name="selectedUsernameAndCvid">The selected username and cvid.</param>
        /// <param name="cVid">The c vid.</param>
        /// <param name="username">The username.</param>
        public static void SplitUserDetails( string selectedUsernameAndCvid, out string cVid, out string username )
        {
            var usernameAndCvid = selectedUsernameAndCvid.Split( ':' );
            username = usernameAndCvid[0];
            cVid = usernameAndCvid[1];
        }
    }
}
