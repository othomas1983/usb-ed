﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceManagement.Infrastructure.Utilities
{
    /// <summary>
    /// Interface that classes can implement to have code run at startup
    /// </summary>
    public interface IRunAtStartup
    {
        /// <summary>
        /// All IRunAtStartup classes will be run in order by this value. If class does not depend on an order, return zero.
        /// </summary>
        /// <value>
        /// The order.
        /// </value>
        int StartupOrder { get; }

        /// <summary>
        /// Method that will be run at  startup
        /// </summary>
        void Execute();
    }
}
