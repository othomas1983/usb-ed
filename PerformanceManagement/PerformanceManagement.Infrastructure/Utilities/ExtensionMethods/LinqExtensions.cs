﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceManagement.Infrastructure.Utilities.ExtensionMethods
{
    public static class LinqExtensions
    {
        /// <summary>
        /// Determines whether [contains case insensitive] the specified string
        /// </summary>
        /// <param name="input">The input.</param>
        /// <param name="term">The find me.</param>
        /// <returns>
        ///   <c>true</c> if [contains case insensitive] [the specified find me]; otherwise, <c>false</c>.
        /// </returns>
        public static bool ContainsCaseInsensitive( this string input, string term )
        {
            return !string.IsNullOrWhiteSpace( input ) && input.ToLower().Contains( term.ToLower() );
        }

        /// <summary>
        /// Wheres the prinicpal.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        public static IEnumerable<UserPrincipal> WherePrinicpal( this IEnumerable<Principal> source )
        {
            foreach ( var result in source )
            {
                var match = true;
                var user = (UserPrincipal) result;

                if ( !string.IsNullOrEmpty( user.EmailAddress ) )
                {
                    var mail = user.EmailAddress.ToUpper();
                    match = mail.Contains( "@SPL" ) || mail.Contains( "USB-ED" );
                }

                if ( !match )
                {
                    yield return user;
                }
            }            
        }
    }
}
