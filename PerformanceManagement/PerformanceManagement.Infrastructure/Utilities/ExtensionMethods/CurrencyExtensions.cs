﻿
namespace PerformanceManagement.Infrastructure.Utilities.ExtensionMethods
{
    /// <summary>
    ///
    /// </summary>
    public static partial class ExtensionMethods
    {
        #region Currency (decimal,double) Extensions

        /// <summary>
        /// Formats as currency using the CurrencySymbol from Global Attributes
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static string FormatAsCurrency( this decimal value, string currencySymbol )
        {
            return string.Format( "{0}{1:N}", currencySymbol, value );
        }

        /// <summary>
        /// Formats as currency using the CurrencySymbol from Global Attributes
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static string FormatAsCurrency( this decimal? value, string currencySymbol )
        {
            if ( value.HasValue )
            {
                return FormatAsCurrency( value.Value, currencySymbol );
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Formats as currency using the CurrencySymbol from Global Attributes
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static string FormatAsCurrency( this double value, string currencySymbol )
        {
            return ( (decimal) value ).FormatAsCurrency( currencySymbol );
        }

        /// <summary>
        /// Formats as currency using the CurrencySymbol from Global Attributes
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static string FormatAsCurrency( this double? value, string currencySymbol )
        {
            return ( (decimal?) value ).FormatAsCurrency( currencySymbol );
        }

        /// <summary>
        /// Formats as currency using the CurrencySymbol from Global Attributes
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static string FormatAsCurrency( this int value, string currencySymbol )
        {
            return ( (decimal) value ).FormatAsCurrency( currencySymbol );
        }

        /// <summary>
        /// Formats as currency using the CurrencySymbol from Global Attributes
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static string FormatAsCurrency( this int? value, string currencySymbol )
        {
            return ( (decimal?) value ).FormatAsCurrency( currencySymbol );
        }

        #endregion
    }
}
