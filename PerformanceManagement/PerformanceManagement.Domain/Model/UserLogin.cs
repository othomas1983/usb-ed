﻿using System.Security.Claims;

namespace PerformanceManagement.Domain.Model
{
    public class UserLogin
    {
        public string Username { get; }

        public UserLogin( ClaimsPrincipal principal )
        {
            Username = principal?.FindFirst( ClaimTypes.Name ).Value;
        }
    }
}
