﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PerformanceManagement.Domain.Common;
using PerformanceManagement.Domain.ExtensionMethods;

namespace PerformanceManagement.Domain.Model.Admin
{
    /// <summary>
    /// Records for Supervision : [sp_SupervisionRead]
    /// </summary>  
    public class SupervisionLoad : DataRowModel
    {
        #region Properties

        public int CVid { get; private set; }
        public string FirstSupervisor { get; private set; }
        public int SecondCVid { get; private set; }
        public string SecondSupervisor { get; private set; }
        public int? Year { get; private set; }
        public string StudentName { get; private set; }
        public string ThesisTopic { get; private set; }
        public string Role { get; private set; }

        public int ProgrammeId { get; private set; }
        public string ProgrammeDescription { get; private set; }
        public int ProgrammeOfferingId { get; private set; }
        public string ProgrammeOfferingDescription { get; private set; }
        public string ProgressStatus { get; private set; }

        public int SupervisorCredit { get; private set; }
        public int SecondarySupervisorCredit { get; private set; }

        public string SourceSystem { get; private set; }

        #endregion

        public SupervisionLoad()
        {
        }

        public SupervisionLoad( DataRow row )
        {
            MapFromDataRow( row );
        }

        public override void MapFromDataRow( DataRow row )
        {
            if ( row == null ) return;

            Id = row["SupervisionId"].ValueOrDefault<int>();
            CVid = row["SupervisorCVID"].ValueOrDefault<int>();
            FirstSupervisor = row["FirstSupervisor"].ValueOrDefault<string>();
            SecondCVid = row["SecondarySupervisorCVID"].ValueOrDefault<int>();
            SecondSupervisor = row["SecondSupervisor"].ValueOrDefault<string>();
            Year = row["Year"].ValueOrDefault<int?>();
            StudentName = row["StudentName"].ValueOrDefault<string>();
            ThesisTopic = row["ThesisTopic"].ValueOrDefault<string>();
            Role = row["Role"].ValueOrDefault<string>();

            ProgrammeId = row["ProgrammeId"].ValueOrDefault<int>();
            ProgrammeDescription = row["ProgrammeDescription"].ValueOrDefault<string>();
            ProgrammeOfferingId = row["ProgrammeOfferingId"].ValueOrDefault<int>();
            ProgrammeOfferingDescription = row["ProgrammeOfferingDescription"].ValueOrDefault<string>();
            ProgressStatus = row["ProgressStatus"].ValueOrDefault<string>();

            SupervisorCredit = row["SupervisorCredit"].ValueOrDefault<int>();
            SecondarySupervisorCredit = row["SecondarySupervisorCredit"].ValueOrDefault<int>();

            SourceSystem = row["SourceSystem"].ValueOrDefault<string>();
        }
    }
}
