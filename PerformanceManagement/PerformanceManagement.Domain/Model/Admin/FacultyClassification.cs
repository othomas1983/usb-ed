﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PerformanceManagement.Domain.Common;
using PerformanceManagement.Domain.ExtensionMethods;

namespace PerformanceManagement.Domain.Model.Admin
{
    /// <summary>
    /// Records for Faculty Department : [sp_FacultyDepartmentRead]
    /// </summary>
    public class FacultyClassification : DataRowModel
    {
       public FacultyClassification()
        {
        }

        public FacultyClassification( DataRow row )
        {
            MapFromDataRow( row );
        }

        public override void MapFromDataRow( DataRow row )
        {
            Id = row["ClassificationID"].ValueOrDefault<int>();
            Description = row["Classification"].ValueOrDefault<string>();
        }
    }
}
