﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PerformanceManagement.Domain.Common;
using PerformanceManagement.Domain.ExtensionMethods;

namespace PerformanceManagement.Domain.Model.Admin
{
    public class ClassificationAndSufficiency : DataSetModel
    {
        #region MyRegion

        public int CVid { get; set; }
        public int? ClassificationId { get; set; }
        public int? SufficiencyId { get; set; }

        #endregion

        public ClassificationAndSufficiency( DataSet dataSet ) : base( dataSet )
        {
            MapFromDataSet();
        }

        public override void MapFromDataSet()
        {
            CVid = Row["CVID"].ValueOrDefault<int>();
            ClassificationId = Row["ClassificationID"].ValueOrDefault<int?>();
            SufficiencyId = Row["SufficiencyID"].ValueOrDefault<int?>();
        }
    }
}
