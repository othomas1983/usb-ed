﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceManagement.Domain.Model.Enums
{
    /// <summary>
    /// Enum value must match ExperienceCategoryID in database
    /// </summary>
    public enum EvaluationCategory
    {
        Integrity,
        Inclusivity,
        Innovation,
        Engagement,
        Excellence,
        Sustainability
        
    }
}
