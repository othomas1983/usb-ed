﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceManagement.Domain.Model.Enums
{
    public enum ManagementLoadType
    {
        //IMPORTANT - Enum value must match ManagementLoadCategoryID in database
        ProgrammeHead = 1,
        LeadershipOneOnOne = 2,
        CentreHead = 3,
        QuestionnaireAdvisory = 4,
        OrganisingConference = 5,
        AccreditationReports = 6,
        ResearchReportOneOnOne = 7,
        Other = 8,
        ChairOfAdministrative = 9,
        TaskTeamMember = 10

    }
}
