﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceManagement.Domain.Model.Enums
{
    public enum DropDownType
    {
        ABSRating,
        AcademicRanks,
        Affiliations,        
        ApplicationUserRoles,
        Degrees,
        Departments,
        DepartmentCategories,
        DHETStatus,
        FacultyClassifications,
        FacultySufficiencies,                
        HighestQualifications,
        Languages,
        Locus,
        MainActivities,
        Nationalities,
        PerformanceCategory,
        PersonRole,
        Programmes,
        ProgrammeOfferings,
        ResearchContributionType,
        ResearchOutputCategory,
        ResearchStatus,
        TaskTeamActivity,
        BusinessUnit,
        Users
    }
}
