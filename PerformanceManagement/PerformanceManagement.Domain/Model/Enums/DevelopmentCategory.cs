﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceManagement.Domain.Model.Enums
{
    /// <summary>
    /// Enum value must match DevelopmentCategoryID in database
    /// </summary>
    public enum DevelopmentCategory
    {
        Membership = 1,
        ContinuingEducation = 2,
        NonAcademic = 3,
        Extracurricular = 4
    }
}
