﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceManagement.Domain.Model.Enums
{
    /// <summary>
    /// Must match Ids in Department Table. SP: sp_DepartmentRead
    /// </summary>
    public enum DepartmentType
    {
        AcademicFaculty = 1,
        SupportStaff = 2
    }
}
