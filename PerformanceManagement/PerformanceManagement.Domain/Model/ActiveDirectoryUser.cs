﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PerformanceManagement.Infrastructure;

namespace PerformanceManagement.Domain.Model
{
    public class ActiveDirectoryUser
    {
        #region Properties

        public string EmployeeId { get; set; }
        public string DisplayName { get; set; }
        public string GivenName { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Description { get; set; }
        public string EmailAddress { get; set; }
        public string VoiceTelephoneNumber { get; set; }
        public string SamAccountName { get; set; }

        public List<string> Groups { get; set; }
        #endregion

        /// <summary>
        /// Determines whether this instance is admin.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance is admin; otherwise, <c>false</c>.
        /// </returns>
        public bool IsAdmin()
        {
            return Groups.Contains( ADGroups.Admin );
        }

        /// <summary>
        /// Determines whether this instance is manager.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance is manager; otherwise, <c>false</c>.
        /// </returns>
        public bool IsManager()
        {
            return Groups.Contains( ADGroups.Manager );
        }

        /// <summary>
        /// Adds the group.
        /// </summary>
        /// <param name="groupName">Name of the group.</param>
        public void AddGroup( string groupName )
        {
            if ( Groups == null )
            {
                Groups = new List<string> { groupName };
            }
            else
            {
                Groups.Add( groupName );
            }
        }

        /// <summary>
        /// Removes the group.
        /// </summary>
        /// <param name="groupName">Name of the group.</param>
        public void RemoveGroup( string groupName )
        {
            if ( Groups == null )
            {
                Groups = new List<string>{ groupName };
            }
            else
            {
                Groups.Remove( groupName );
            }
        }
    }
}
