﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PerformanceManagement.Domain.ExtensionMethods;
using PerformanceManagement.Domain.Model.PerformanceManagement.Common;

namespace PerformanceManagement.Domain.Model.PerformanceManagement
{
    public class PeerEvaluation : Evaluation
    {
        #region Properties

        public int PeerCVid { get; set; }

        #endregion

        public PeerEvaluation()
        {
        }

        public PeerEvaluation( DataRow row )
        {
            
        }

        public override void MapFromDataRow( DataRow row )
        {
            if ( row == null ) return;

            base.MapFromDataRow( row );

            Id = row["PeerPerformanceManagementID"].ValueOrDefault<int>();
            CVid = row["ReviewerCVID"].ValueOrDefault<int>();
            PeerCVid = row["PeerCVID"].ValueOrDefault<int>();
        }

    }
}
