﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PerformanceManagement.Domain.ExtensionMethods;
using PerformanceManagement.Domain.Model.PerformanceManagement.Common;

namespace PerformanceManagement.Domain.Model.PerformanceManagement
{
    public class SupervisorEvaluation : Evaluation
    {
        #region Properties

        public int SupervisorCVid { get; set; }
        public string SupervisorUserName { get; set; }

        #endregion

        public SupervisorEvaluation()
        {
        }

        public SupervisorEvaluation( DataRow row )
        {
            
        }

        public override void MapFromDataRow( DataRow row )
        {
            if ( row == null ) return;

            base.MapFromDataRow( row );

            Id = row["SupervisorPerformanceManagementId"].ValueOrDefault<int>();
            CVid = row["ReviewerCVID"].ValueOrDefault<int>();
            SupervisorCVid = row["SupervisorCVID"].ValueOrDefault<int>();
            SupervisorUserName = row["SupervisorUserName"].ValueOrDefault<string>();
        }

    }
}
