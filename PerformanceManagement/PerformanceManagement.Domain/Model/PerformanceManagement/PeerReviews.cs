﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PerformanceManagement.Domain.Common;
using PerformanceManagement.Domain.ExtensionMethods;

namespace PerformanceManagement.Domain.Model.PerformanceManagement
{
   public class PeerReviews : DataRowModel
    {
        #region Properties

        public int ReviewerCVID { get; private set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }

        #endregion

        public PeerReviews()
        {
        }

        public PeerReviews( DataRow row )
        {
            MapFromDataRow( row );
        }

        public override void MapFromDataRow( DataRow row )
        {
            if ( row == null ) return;  
            Username = row["Samaccountname"].ValueOrDefault<string>().Replace("/","//"); 
            Id = row["ReviewerCVID"].ValueOrDefault<int>();
            Name = row["Name"].ValueOrDefault<string>();
            LastName = row["Surname"].ValueOrDefault<string>();
            ReviewerCVID = row["PeerCVID"].ValueOrDefault<int>();
        }
    }
}
