﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PerformanceManagement.Domain.Common;
using PerformanceManagement.Domain.ExtensionMethods;

namespace PerformanceManagement.Domain.Model.PerformanceManagement
{
    public class PerformanceCategory : DataRowModel
    {
        #region Properties

        public string BaseLine { get; protected set; }

        #endregion

        public PerformanceCategory()
        {
        }

        public PerformanceCategory( DataRow row )
        {
            MapFromDataRow( row );
        }

        public override void MapFromDataRow( DataRow row )
        {
            if ( row == null ) return;

            Id = row["PerformanceCategoryID"].ValueOrDefault<int>();
            Description = row["PerformanceCategory"].ValueOrDefault<string>();
            BaseLine = row["BaseLine"].ValueOrDefault<string>();          
        }
    }
}
