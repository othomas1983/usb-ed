﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PerformanceManagement.Domain.Common;
using PerformanceManagement.Domain.ExtensionMethods;

namespace PerformanceManagement.Domain.Model.PerformanceManagement
{
   public class Reviewed : DataRowModel
    {
        #region Properties

        public int ReviewerCVID { get; private set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }

        #endregion

        public Reviewed()
        {
        }

        public Reviewed( DataRow row )
        {
            MapFromDataRow( row );
        }

        public override void MapFromDataRow( DataRow row )
        {
            if ( row == null ) return;  
            Username = row["Samaccountname"].ValueOrDefault<string>().Replace("/","//"); 
            Id = row["SupervisorCVID"].ValueOrDefault<int>();
            Name = row["Name"].ValueOrDefault<string>();
            LastName = row["Surname"].ValueOrDefault<string>();
            ReviewerCVID = row["SupervisorCVID"].ValueOrDefault<int>();
        }
    }
}
