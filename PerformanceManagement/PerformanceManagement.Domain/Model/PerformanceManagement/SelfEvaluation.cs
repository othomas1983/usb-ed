﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PerformanceManagement.Domain.ExtensionMethods;
using PerformanceManagement.Domain.Model.PerformanceManagement.Common;

namespace PerformanceManagement.Domain.Model.PerformanceManagement
{
    public class SelfEvaluation : Evaluation
    {
        #region Properties
        
        #endregion

        public SelfEvaluation()
        {
        }

        public SelfEvaluation( DataRow row )
        {
            
        }

        public override void MapFromDataRow( DataRow row )
        {
            if ( row == null ) return;

            base.MapFromDataRow( row );

            Id = row["PerformanceManagementID"].ValueOrDefault<int>();
            CVid = row["CVid"].ValueOrDefault<int>();
        }

    }
}
