﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PerformanceManagement.Domain.Common;
using PerformanceManagement.Domain.ExtensionMethods;

namespace PerformanceManagement.Domain.Model.Research
{
    public class Contributor : DataRowModel
    {
        #region Properties

        public int? CVid { get; private set; }
        public int ResearchId { get; private set; }
        

        public decimal Credit { get; private set; }
        public int ContributorSequence { get; private set; }
        public int PersonRoleId { get; private set; }
        public string PersonRoleDescription { get; private set; }
        public string Type { get; private set; }
        public string Institution { get; private set; }

        // External Contributor Properties
        public string Surname { get; private set; }
        public string FirstName { get; private set; }
        public string Initials { get; private set; }
        public int? NationalityId { get; private set; }
        public int? AffiliationId { get; private set; }
        public string Gender { get; private set; }
        public string Email { get; private set; }

        #endregion

        public Contributor()
        {
        }

        public Contributor( DataRow row )
        {
            MapFromDataRow( row );
        }

        public override void MapFromDataRow( DataRow row )
        {
            if ( row == null ) return;

            Id = row["PeopleinResearchID"].ValueOrDefault<int>();
            CVid = row["PeopleinResearchCVID"].ValueOrDefault<int?>();
            ResearchId = row["ResearchId"].ValueOrDefault<int>();
            Credit = row["Credit"].ValueOrDefault<decimal>();
            ContributorSequence = row["ContributorSequence"].ValueOrDefault<int>();
            PersonRoleId = row["PersonRoleId"].ValueOrDefault<int>();
            PersonRoleDescription = row["PersonRoleDescription"].ValueOrDefault<string>();            
            Institution = row["Institution"].ValueOrDefault<string>();
            Type = row["Type"].ValueOrDefault<string>();

            // External Contributor Fields
            Surname = row["Surname"].ValueOrDefault<string>();
            FirstName = row["Name"].ValueOrDefault<string>();
            Initials = row["Initials"].ValueOrDefault<string>();
            Gender = row["Gender"].ValueOrDefault<string>();
            Institution = row["Institution"].ValueOrDefault<string>();
            Email = row["Email"].ValueOrDefault<string>();
            Institution = row["Institution"].ValueOrDefault<string>();
            NationalityId = row["NationalityId"].ValueOrDefault<int?>();
            AffiliationId = row["AffiliationId"].ValueOrDefault<int?>();
        }

        /// <summary>
        /// Gets the full name of the contributor.
        /// </summary>
        /// <returns></returns>
        public string GetExternalContributorFullName()
        {
            return $"{Surname}, {FirstName}";
        }
    }
}
