﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PerformanceManagement.Domain.Common;
using PerformanceManagement.Domain.ExtensionMethods;

namespace PerformanceManagement.Domain.Model.Research
{
    public class DHETStatus : DataRowModel
    {
        public DHETStatus()
        {
        }

        public DHETStatus( DataRow row )
        {
            MapFromDataRow( row );
        }

        public override void MapFromDataRow( DataRow row )
        {
            Id = row["DHETStatusId"].ValueOrDefault<int>();            
            Description = row["DHETStatusDescription"].ValueOrDefault<string>();            
        }
    }
}
