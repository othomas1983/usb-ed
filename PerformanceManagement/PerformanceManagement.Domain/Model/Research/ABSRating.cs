﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PerformanceManagement.Domain.Common;
using PerformanceManagement.Domain.ExtensionMethods;

namespace PerformanceManagement.Domain.Model.Research
{
    public class ABSRating : DataRowModel
    {
        public int Credit { get; private set; }

        public ABSRating()
        {
        }

        public ABSRating( DataRow row )
        {
            MapFromDataRow( row );
        }

        public override void MapFromDataRow( DataRow row )
        {
            Id = row["ABSRatingStatusId"].ValueOrDefault<int>();            
            Description = row["ABSRatingStatusDescription"].ValueOrDefault<string>();

            Credit = row["Credit"].ValueOrDefault<int>();
        }
    }
}
