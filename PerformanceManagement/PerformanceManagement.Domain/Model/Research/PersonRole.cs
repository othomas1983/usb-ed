﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PerformanceManagement.Domain.Common;
using PerformanceManagement.Domain.ExtensionMethods;

namespace PerformanceManagement.Domain.Model.Research
{
    public class PersonRole : DataRowModel
    {
        public PersonRole()
        {
        }

        public PersonRole( DataRow row )
        {
            MapFromDataRow( row );
        }

        public override void MapFromDataRow( DataRow row )
        {
            Id = row["PersonRoleId"].ValueOrDefault<int>();            
            Description = row["PersonRoleDescription"].ValueOrDefault<string>();
        }
    }
}
