﻿using System.Data;
using PerformanceManagement.Domain.Common;
using PerformanceManagement.Domain.ExtensionMethods;

namespace PerformanceManagement.Domain.Model.Common
{
    public class ResearchContributionType : DataRowModel
    {
        public ResearchContributionType()
        {
        }

        public ResearchContributionType( DataRow row )
        {
            MapFromDataRow( row );
        }

        public override void MapFromDataRow( DataRow row )
        {
            Id = row["ResearchContributionTypeId"].ValueOrDefault<int>();
            Description = row["ResearchContributionType"].ValueOrDefault<string>();            
        }
    }
}
