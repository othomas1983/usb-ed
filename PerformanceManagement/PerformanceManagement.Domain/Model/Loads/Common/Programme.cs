﻿using System.Data;
using PerformanceManagement.Domain.Common;
using PerformanceManagement.Domain.ExtensionMethods;

namespace PerformanceManagement.Domain.Model.Loads.Common
{
    public class Programme : DataRowModel
    {
        #region Properties

        #endregion

        public Programme()
        {
        }

        public Programme( DataRow row )
        {
            MapFromDataRow( row );
        }

        public override void MapFromDataRow( DataRow row )
        {
            if ( row == null ) return;

            Id = row["ProgrammeId"].ValueOrDefault<int>();
            Description = row["Programme"].ValueOrDefault<string>();
        }
    }
}
