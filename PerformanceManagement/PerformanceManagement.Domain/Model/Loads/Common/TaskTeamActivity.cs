﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PerformanceManagement.Domain.Common;
using PerformanceManagement.Domain.ExtensionMethods;

namespace PerformanceManagement.Domain.Model.Loads.Common
{
    public class TaskTeamActivity : DataRowModel
    {

        public TaskTeamActivity()
        {
        }

        public TaskTeamActivity( DataRow row )
        {
            MapFromDataRow( row );
        }   

        public override void MapFromDataRow( DataRow row )
        {
            if ( row == null ) return;

            Id = row["TaskTeamActivityId"].ValueOrDefault<int>();
            Description = row["TaskTeamActivityDescription"].ValueOrDefault<string>();
        }
    }
}
