﻿using System.Data;
using PerformanceManagement.Domain.Common;
using PerformanceManagement.Domain.ExtensionMethods;

namespace PerformanceManagement.Domain.Model.Loads.Common
{
    public class ProgrammeOffering : DataRowModel
    {
        #region Properties

        public int ProgrammeId { get; set; }

        #endregion

        public ProgrammeOffering()
        {
        }

        public ProgrammeOffering( DataRow row )
        {
            MapFromDataRow( row );
        }

        public override void MapFromDataRow( DataRow row )
        {
            if ( row == null ) return;

            Id = row["ProgrammeOfferingId"].ValueOrDefault<int>();
            Description = row["ProgrammeOffering"].ValueOrDefault<string>();
            ProgrammeId = row["ProgrammeId"].ValueOrDefault<int>();            
        }
    }
}
