﻿using System.Data;
using PerformanceManagement.Domain.Common;
using PerformanceManagement.Domain.ExtensionMethods;

namespace PerformanceManagement.Domain.Model.Loads
{
    public class TeachingLoad : DataRowModel
    {
        #region Properties

        public int Year { get; private set; }
        public string ProgrammeOffering { get; private set; }
        public string Programme { get; private set; }
        public string Module { get; private set; }
        public decimal TotalCredits { get; private set; }
        public decimal Sessions { get; private set; }
        public int Students { get; private set; }
        public int BaseCredit { get; private set; }

        public string Category { get; set; }

        #endregion

        public TeachingLoad()
        {
        }

        public TeachingLoad( DataRow row )
        {
            MapFromDataRow( row );
        }

        public override void MapFromDataRow( DataRow row )
        {
            if ( row == null ) return;

            Id = row["TeachingLoadID"].ValueOrDefault<int>();
            Year = row["Year"].ValueOrDefault<int>();
            ProgrammeOffering = row["ProgrammeOffering"].ValueOrDefault<string>();
            Programme = row["Programme"].ValueOrDefault<string>();
            Module = row["Module"].ValueOrDefault<string>();
            TotalCredits = row["TotalCredit"].ValueOrDefault<decimal>();
            Sessions = row["SessionCount"].ValueOrDefault<decimal>();
            Students = row["StudentCount"].ValueOrDefault<int>();
            BaseCredit = row["BaseCredit"].ValueOrDefault<int>();
            Category = row["CategoryName"].ValueOrDefault<string>();

            

        }
    }
}
