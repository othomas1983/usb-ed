﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PerformanceManagement.Domain.Common;
using PerformanceManagement.Domain.ExtensionMethods;

namespace PerformanceManagement.Domain.Model.Loads
{
    public class ManagementLoad : DataRowModel
    {
        #region Properties


        public int Year { get; private set; }

        public DateTime StartDate { get; private set; }

        public DateTime EndDate { get; private set; }

        public int ManagementLoadCategoryId { get; private set; }

        public int ModuleCount { get; private set; }

        public int StudentCount { get; private set; }

        //public int ProgrammeId { get; private set; }
        public int DegreeId { get; private set; }

        public int ProgrammeOfferingId { get; private set; }

        public int ElectiveModuleCount { get; private set; }

        public int ElectiveSAQACredit { get; private set; }

        public int TaskTeamActivityId { get; private set; }

        public string TaskTeamActivityDescription { get; private set; }

        public int Credit { get; private set; }
        public string ProgrammeSize { get; set; }

        #endregion

        public ManagementLoad( DataRow row )
        {
            MapFromDataRow( row );
        }

        public ManagementLoad()
        {
        }

        public override void MapFromDataRow( DataRow row )
        {
            if ( row == null ) return;

            Id = row["ManagementLoadId"].ValueOrDefault<int>();
            Year = row["ManagementLoadYear"].ValueOrDefault<int>();
            IsActive = row["IsActive"].ValueOrDefault<int>();
            StartDate = row["ManagementLoadStartDate"].ValueOrDefault<DateTime>();
            EndDate = row["ManagementLoadEndDate"].ValueOrDefault<DateTime>();
            Description = row["Description"].ValueOrDefault<string>();
            ManagementLoadCategoryId = row["ManagementLoadCategoryId"].ValueOrDefault<int>();

            ModuleCount = row["ModuleCount"].ValueOrDefault<int>();
            StudentCount = row["StudentCount"].ValueOrDefault<int>();
            Credit = row["Credit"].ValueOrDefault<int>();

            DegreeId = row["DegreeId"].ValueOrDefault<int>();
            ProgrammeOfferingId = row["ProgrammeOfferingId"].ValueOrDefault<int>();

            ElectiveModuleCount = row["ElectiveModuleCount"].ValueOrDefault<int>();
            ElectiveSAQACredit = row["ElectiveSAQACredit"].ValueOrDefault<int>();
            

            TaskTeamActivityId = row["TaskTeamActivityID"].ValueOrDefault<int>();
            TaskTeamActivityDescription = row["TaskTeamActivityDescription"].ValueOrDefault<string>();
            ProgrammeSize = row["ProgrammeSize"].ValueOrDefault<string>();

        }
    }
}
