﻿using System.Data;
using PerformanceManagement.Domain.Common;
using PerformanceManagement.Domain.ExtensionMethods;

namespace PerformanceManagement.Domain.Model.Common
{
    public class AcademicRank : DataRowModel
    {
        public AcademicRank()
        {
        }

        public AcademicRank( DataRow row )
        {
            MapFromDataRow( row );
        }

        public override void MapFromDataRow( DataRow row )
        {
            Id = row["AcademicRankId"].ValueOrDefault<int>();
            Description = row["AcademicRankDescription"].ValueOrDefault<string>();
        }
    }
}
