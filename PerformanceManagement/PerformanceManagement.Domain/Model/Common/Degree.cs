﻿using System.Data;
using PerformanceManagement.Domain.Common;
using PerformanceManagement.Domain.ExtensionMethods;

namespace PerformanceManagement.Domain.Model.Common
{
    public class Degree : DataRowModel
    {
        public Degree()
        {
        }

        public Degree( DataRow row )
        {
            MapFromDataRow( row );
        }

        public override void MapFromDataRow( DataRow row )
        {
            Id = row["DegreeId"].ValueOrDefault<int>();
            Description = row["DegreeDescription"].ValueOrDefault<string>();
        }
    }
}
