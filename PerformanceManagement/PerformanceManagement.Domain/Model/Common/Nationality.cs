﻿using System.Data;
using PerformanceManagement.Domain.Common;
using PerformanceManagement.Domain.ExtensionMethods;

namespace PerformanceManagement.Domain.Model.Common
{
    public class Nationality : DataRowModel
    {
        public Nationality()
        {
        }

        public Nationality( DataRow row )
        {
            MapFromDataRow( row );
        }

        public override void MapFromDataRow( DataRow row )
        {
            Id = row["NationalityId"].ValueOrDefault<int>();
            Description = row["Nationality"].ValueOrDefault<string>();            
        }
    }
}
