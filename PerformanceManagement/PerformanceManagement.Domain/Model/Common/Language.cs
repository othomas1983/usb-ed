﻿using System.Data;
using PerformanceManagement.Domain.Common;
using PerformanceManagement.Domain.ExtensionMethods;

namespace PerformanceManagement.Domain.Model.Common
{
    public class Language : DataRowModel
    {
        #region Properties

        #endregion

        public Language()
        {
        }

        public Language( DataRow row )
        {
            MapFromDataRow( row );
        }

        public override void MapFromDataRow( DataRow row )
        {
            if ( row == null ) return;

            Id = row["LanguageId"].ValueOrDefault<int>();            
            Description = row["Language"].ValueOrDefault<string>();
            IsActive = row["IsActive"].ValueOrDefault<int>();
            //FacultyLanguageId = row["FacultyLanguageId"].ValueOrDefault<int>();
        }
    }
}
