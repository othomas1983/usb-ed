﻿using System.Data;
using PerformanceManagement.Domain.Common;
using PerformanceManagement.Domain.ExtensionMethods;

namespace PerformanceManagement.Domain.Model.Common
{
    public class FacultyUser : DataRowModel
    {
        #region Properties

        public int CVid { get; private set; }
        public string Surname { get; private set; }
        public string SamAccountName { get; private set; }
        public string EmployeeId { get; private set; }
        public string Institution { get; private set; }
        public string Biography { get; private set; }


        #endregion

        public FacultyUser()
        {
        }

        public FacultyUser( DataRow row )
        {
            MapFromDataRow( row );
        }

        public override void MapFromDataRow( DataRow row )
        {
            if ( row == null ) return;

            CVid = row["CVID"].ValueOrDefault<int>();
            //Description = row["Name"].ValueOrDefault<string>(); ;
            //Surname = row["Surname"].ValueOrDefault<string>();
            //SamAccountName = row["Samaccountname"].ValueOrDefault<string>();
            //EmployeeId = row["EmployeeId"].ValueOrDefault<string>();
            //Institution = row["HomeInstitution"].ValueOrDefault<string>();
            //Biography = row["Biography"].ValueOrDefault<string>();
            CVid = row["CVID"].ValueOrDefault<int>();
            Description = row["FirstName"].ValueOrDefault<string>(); ;
            Surname = row["LastName"].ValueOrDefault<string>();
            SamAccountName = row["UserName"].ValueOrDefault<string>();
        }

        /// <summary>
        /// Gets the full name of the faculty user.
        /// </summary>
        /// <returns></returns>
        public string GetFullName()
        {
            return $"{Surname}, {Description}";
        }
    }
}
