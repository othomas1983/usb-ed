﻿using System.Data;
using PerformanceManagement.Domain.Common;
using PerformanceManagement.Domain.ExtensionMethods;

namespace PerformanceManagement.Domain.Model.Common
{
    public class HighestQualification : DataRowModel
    {

        public HighestQualification()
        {
        }

        public HighestQualification( DataRow row )
        {
            MapFromDataRow( row );
        }

        public override void MapFromDataRow( DataRow row )
        {
            Id = row["HighestQualificationId"].ValueOrDefault<int>();
            Description = row["HighestQualification"].ValueOrDefault<string>();
        }
    }
}
