﻿using System.Data;
using PerformanceManagement.Domain.Common;
using PerformanceManagement.Domain.ExtensionMethods;

namespace PerformanceManagement.Domain.Model.Common
{
    public class Affiliation : DataRowModel
    {
        public Affiliation()
        {
        }

        public Affiliation( DataRow row )
        {
            MapFromDataRow( row );
        }

        public override void MapFromDataRow( DataRow row )
        {
            Id = row["AffiliationId"].ValueOrDefault<int>();
            Description = row["AffiliationDescription"].ValueOrDefault<string>();
        }
    }
}
