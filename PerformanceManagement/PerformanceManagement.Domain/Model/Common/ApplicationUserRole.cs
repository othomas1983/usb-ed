﻿using System.Data;
using PerformanceManagement.Domain.Common;
using PerformanceManagement.Domain.ExtensionMethods;

namespace PerformanceManagement.Domain.Model.Common
{
    public class ApplicationUserRole : DataRowModel
    {
        public ApplicationUserRole()
        {
        }

        public ApplicationUserRole( DataRow row )
        {
            MapFromDataRow( row );
        }

        public override void MapFromDataRow( DataRow row )
        {
            Id = row["ApplicationUserRoleId"].ValueOrDefault<int>();
            Description = row["ApplicationUserRole"].ValueOrDefault<string>();
        }
    }
}
