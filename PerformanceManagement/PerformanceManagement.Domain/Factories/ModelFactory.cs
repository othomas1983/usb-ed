﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PerformanceManagement.Domain.Common;
using PerformanceManagement.Domain.Model;

namespace PerformanceManagement.Domain.Factories
{
    public class ModelFactory
    {
        /// <summary>
        /// Creates the list of T from a given dataset with multiple rows.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dataSet">The data set.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">T</exception>
        /// <exception cref="ObjectNotFoundException">T</exception>
        public static List<T> CreateList<T>( DataSet dataSet ) where T : IDataRowModel, new()
        {
            if ( dataSet == null ) throw new ArgumentNullException( nameof( T ) );
           // DataTable table = dataSet.Tables[0];
           // if ( !(table.Rows.Count > 0) ) throw new ObjectNotFoundException( nameof( T ) );

            var list = new List<T>();

            foreach ( DataRow row in dataSet.Tables[0].Rows )
            {
                var item = new T();
                item.MapFromDataRow( row );
                list.Add( item );
            }

            return list;
        }
    }
}
