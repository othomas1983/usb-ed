﻿using System;
using System.Data;
using System.Reflection;

namespace PerformanceManagement.Domain.ExtensionMethods
{
    public static class SqlExtensions
    {
        /// <summary>
        /// Converts the object value to the specified type if not null.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static T ValueOrDefault<T>( this object value )
        {
            if ( DBNull.Value.Equals( value ) ) return default( T );

            Type type = typeof( T );
            Type u = Nullable.GetUnderlyingType( type );

            if ( u != null )
            {
                return ( value == null ) ? default( T ) : (T) Convert.ChangeType( value, u );
            }

            return (T) Convert.ChangeType( value, type );


            //return (T) Convert.ChangeType( value, typeof( T ) );
        }

        /// <summary>
        /// returns null if the value is 0, otherwise it will return the original value
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static int? AsIntergerOrNull( this int? value )
        {
            if ( !value.HasValue || value == 0 )
            {
                return null;
            }

            return value;
        }



    }
}
