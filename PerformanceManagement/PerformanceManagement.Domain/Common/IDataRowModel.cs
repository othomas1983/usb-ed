﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceManagement.Domain.Common
{
    /// <summary>
    /// Inteface for a model that consists of multiple rows e.g. lookup
    /// </summary>
    public interface IDataRowModel
    {
        /// <summary>
        /// Maps a specific datarow to domain model
        /// </summary>
        void MapFromDataRow( DataRow row );
    }
}
