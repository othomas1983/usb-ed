﻿using System;
using System.ServiceModel;
using BudgetEventReceiver.Helper;
using BudgetEventReceiver.HubServiceReference;
using Microsoft.SharePoint;
using System.Collections.Generic;
using System.Diagnostics;

namespace BudgetEventReceiver.EventReceiver1
{
    /// <summary>
    /// List Item Events
    /// </summary>
    public class BudgetEventReceiver : SPItemEventReceiver
    {
        #region Locals

        //TODO: move to external settings file
        private const string _STATUSCOLUMN = "Budget Status";
        private const string _PROGRAMMEOFFERINGDOCUMENTLIBRARY = "Programme Offering";

        private const string _EVENTSOURCE = "Budget Event Receiver";
        private const string _EVENTLOG = "Application";

        #endregion

        #region Events

        /// <summary>
        /// An item was added
        /// </summary>
        public override void ItemAdded(SPItemEventProperties properties)
        {
            try
            {
                if (properties.ListItem.IsNotNull())
                {
                    SendToOds(properties.ListItemId);

                    SetStatus(properties);
                }
            }
            catch (Exception ex)
            {
                if (!EventLog.SourceExists(_EVENTSOURCE))
                    EventLog.CreateEventSource(_EVENTSOURCE, _EVENTLOG);

                EventLog.WriteEntry(_EVENTSOURCE, "Exception: " + ex.Message, EventLogEntryType.Error);
            }
        }

        /// <summary>
        /// An item was updated.
        /// </summary>
        public override void ItemUpdated(SPItemEventProperties properties)
        {
            try
            {
                if (properties.ListItem.IsNotNull())
                {
                    SendToOds(properties.ListItemId);
                    
                    SetStatus(properties);
                }
            }
            catch (Exception ex)
            {
                if (!EventLog.SourceExists(_EVENTSOURCE))
                    EventLog.CreateEventSource(_EVENTSOURCE, _EVENTLOG);

                EventLog.WriteEntry(_EVENTSOURCE, "Exception: " + ex.Message, EventLogEntryType.Error);
            }

        }


        /// <summary>
        /// An item is being deleted
        /// </summary>
        public override void ItemDeleting(SPItemEventProperties properties)
        {
            if (!EventLog.SourceExists(_EVENTSOURCE))
                EventLog.CreateEventSource(_EVENTSOURCE, _EVENTLOG);

            EventLog.WriteEntry(_EVENTSOURCE, "ItemDeleting triggered: " + DateTime.Now.ToString());

            DeleteBudgetSet(properties.ListItemId);
        }

        #endregion


        #region Methods


        private void SetStatus(SPItemEventProperties properties)
        {
            if (properties.ListItem[_STATUSCOLUMN].ToString() == BudgetSetStatus.Final.GetName<BudgetSetStatus>())
            {
                var siteUri = "http://" + Environment.MachineName + properties.RelativeWebUrl.ToString();

                using (SPSite site = new SPSite(siteUri))
                {
                    var webUri = properties.RelativeWebUrl.ToString();
                    string[] arrListUriSegments;
                    using (SPWeb web = site.OpenWeb(webUri))
                    {
                        arrListUriSegments = properties.ListItem.Url.Split('/');
                        var lib = web.Lists[_PROGRAMMEOFFERINGDOCUMENTLIBRARY] as SPDocumentLibrary;
                        foreach (SPFile file in lib.RootFolder.SubFolders[arrListUriSegments[1]].Files)
                        {
                            var item = file.Item;
                            if (item.ID != properties.ListItemId)
                            {
                                string status = item[_STATUSCOLUMN].ToString();
                                if (status == BudgetSetStatus.Final.GetName<BudgetSetStatus>())
                                {
                                    try
                                    {
                                        item[_STATUSCOLUMN] = BudgetSetStatus.Approved.GetName<BudgetSetStatus>();
                                        item.Update();
                                    }
                                    catch (Exception)
                                    {

                                    }
                                }
                            }
                        }
                    }

                }

            }

        }

        private bool SendToOds(int id)
        {
            WSHttpBinding myBinding;
            EndpointAddress address;
            Helper.ClientService.InitializeServiceClient(out myBinding, out address);
            HubServiceReference.BudgetingServiceClient client = new BudgetingServiceClient(myBinding, address);
            var result = client.SendBudgetToOds(id);
            return result.HasPassed;
        }

        private void DeleteBudgetSet(int id)
        {
            WSHttpBinding myBinding;
            EndpointAddress address;
            Helper.ClientService.InitializeServiceClient(out myBinding, out address);
            HubServiceReference.BudgetingServiceClient client = new BudgetingServiceClient(myBinding, address);
            client.DeleteBudgetSet(id);
        }

        #endregion

 





    }
}
