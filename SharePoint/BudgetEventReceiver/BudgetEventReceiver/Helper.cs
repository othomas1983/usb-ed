﻿using System;
using System.ServiceModel;
using System.ServiceModel.Security;
using System.Text;

namespace BudgetEventReceiver.Helper
{

    /// <summary>
    /// BudgetSetStatus enum
    /// </summary>
    public enum BudgetSetStatus
    {
        Draft = 0,
        Approved = 1,
        Final = 2
    }

    /// <summary>
    /// EditState enum
    /// </summary>
    public enum EditState
    { 
        Adding = 0,
        Editing = 1
    }

    /// <summary>
    /// Extension methods to aid developement
    /// </summary>
    public static class Extensions
    {
        public static string GetName<T>(this Enum value)
        {
            if (typeof(T).BaseType != typeof(Enum))
            {
                throw new InvalidCastException();
            }
            if (Enum.IsDefined(typeof(T), value) == false)
            {
                throw new InvalidCastException();
            }

            return Enum.GetName(typeof(T), Convert.ToInt32(value)); ;
        }

        public static bool IsNotNull(this object value)
        {
            return value != null ? true : false;
        }
    }

    public static class ClientService
    {
        #region Locals

        private const string _BINDINGNAME = "WSHttpBinding_IBudgetingService";
        private const string _ENDPOINTURI = "http://bpccrm03:8092/HubService/BudgetService/soap";
        private const string _SERVICEPRINCIPALNAME = "host/bpccrm03.belpark.sun.ac.za";

        #endregion

        public static void InitializeServiceClient(out WSHttpBinding myBinding, out EndpointAddress address)
        {
            myBinding = new WSHttpBinding();
            myBinding.Name = _BINDINGNAME;
            myBinding.CloseTimeout = new TimeSpan(0, 1, 0); 
            myBinding.OpenTimeout = new TimeSpan(0, 1, 0); 
            myBinding.ReceiveTimeout = new TimeSpan(0, 10, 0); 
            myBinding.SendTimeout = new TimeSpan(0, 1, 0); 
            myBinding.BypassProxyOnLocal = false;
            myBinding.TransactionFlow = false;
            myBinding.HostNameComparisonMode = HostNameComparisonMode.StrongWildcard;
            myBinding.MaxBufferPoolSize = 524288;
            myBinding.MaxReceivedMessageSize = 65536;
            myBinding.MessageEncoding = WSMessageEncoding.Text;
            myBinding.TextEncoding = Encoding.UTF8;
            myBinding.UseDefaultWebProxy = true;
            myBinding.AllowCookies = false;
            myBinding.ReaderQuotas.MaxDepth = 32;
            myBinding.ReaderQuotas.MaxStringContentLength = 8192;
            myBinding.ReaderQuotas.MaxArrayLength = 16384;
            myBinding.ReaderQuotas.MaxBytesPerRead = 4096;
            myBinding.ReaderQuotas.MaxNameTableCharCount = 16384;
            myBinding.ReliableSession.Ordered = true;
            myBinding.ReliableSession.InactivityTimeout = new TimeSpan(0, 10, 0); 
            myBinding.ReliableSession.Enabled = false;
            myBinding.Security.Mode = SecurityMode.None;
            myBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            myBinding.Security.Transport.ProxyCredentialType = HttpProxyCredentialType.None;
            myBinding.Security.Transport.Realm = string.Empty;
            myBinding.Security.Message.ClientCredentialType = MessageCredentialType.Windows;
            myBinding.Security.Message.NegotiateServiceCredential = true;
            myBinding.Security.Message.AlgorithmSuite = SecurityAlgorithmSuite.Default;

            Uri uri = new Uri(_ENDPOINTURI);
            EndpointIdentity identity = EndpointIdentity.CreateSpnIdentity(_SERVICEPRINCIPALNAME);
            address = new EndpointAddress(uri, identity);
        }
    }

  
}
