﻿ALTER TABLE [shpt].[BudgetSetYear]
    ADD CONSTRAINT [FK_BudgetSetYear_GeneralLedgerCode] FOREIGN KEY ([GeneralLedgerCodeID]) REFERENCES [shpt].[GeneralLedgerCode] ([ID]) ON DELETE CASCADE ON UPDATE CASCADE;

