﻿ALTER TABLE [shpt].[GeneralLedgerCode]
    ADD CONSTRAINT [FK_GeneralLedgerCode_BudgetSet] FOREIGN KEY ([BudgetSetID]) REFERENCES [shpt].[BudgetSet] ([ID]) ON DELETE CASCADE ON UPDATE CASCADE;

