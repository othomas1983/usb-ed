﻿CREATE TABLE [shpt].[BudgetSet] (
    [ID]                  INT              IDENTITY (1, 1) NOT NULL,
    [SharepointID]        INT              NULL,
    [ProgrammeOfferingID] UNIQUEIDENTIFIER NULL,
    [CrmUserID]           UNIQUEIDENTIFIER NULL,
    [Set]                 INT              NULL,
    [StartPeriod]         INT              NULL,
    [Title]               VARCHAR (255)    NULL,
    [FileName]            VARCHAR (255)    NULL,
    [FilePath]            VARCHAR (255)    NULL,
    [DateCreated]         DATETIME         NULL,
    [UserCreatedID]       INT              NULL,
    [DateModified]        DATETIME         NULL,
    [UserModifiedID]      INT              NULL,
    [Status]              INT              NULL,
    [SentToAccpac]        BIT              NULL,
    [IsDeleted]           BIT              NULL,
    [IsValid]             BIT              NULL
);





