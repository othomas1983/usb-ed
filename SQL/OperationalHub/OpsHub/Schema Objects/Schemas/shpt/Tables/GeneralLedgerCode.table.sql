﻿CREATE TABLE [shpt].[GeneralLedgerCode] (
    [ID]            INT             IDENTITY (1, 1) NOT NULL,
    [BudgetSetID]   INT             NULL,
    [Code]          VARCHAR (45)    NULL,
    [Account]       VARCHAR (10)    NULL,
    [Market]        VARCHAR (10)    NULL,
    [SubMarket]     VARCHAR (10)    NULL,
    [ProgrammeCode] VARCHAR (10)    NULL,
    [TotalAmount]   DECIMAL (19, 3) NULL
);



