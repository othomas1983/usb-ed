﻿CREATE TABLE [shpt].[BudgetSetYear] (
    [ID]                  INT             IDENTITY (1, 1) NOT NULL,
    [GeneralLedgerCodeID] INT             NULL,
    [FiscalYear]          INT             NULL,
    [Amount]              DECIMAL (19, 3) NULL
);



