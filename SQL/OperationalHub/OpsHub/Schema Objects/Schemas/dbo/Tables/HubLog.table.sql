﻿CREATE TABLE [dbo].[HubLog] (
    [HubLogID]   INT           IDENTITY (1, 1) NOT NULL,
    [Level]      VARCHAR (30)  NULL,
    [Stacktrace] VARCHAR (MAX) NULL,
    [Message]    VARCHAR (MAX) NULL,
    [DateLogged] DATETIME      NULL
);

