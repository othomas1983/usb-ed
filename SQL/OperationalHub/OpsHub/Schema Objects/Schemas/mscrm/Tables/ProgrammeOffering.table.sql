﻿CREATE TABLE [mscrm].[ProgrammeOffering] (
    [ID]                  INT              IDENTITY (1, 1) NOT NULL,
    [ProgrammeOfferingID] UNIQUEIDENTIFIER NULL,
    [TotalAmount]         DECIMAL (19, 3)  NULL,
    [DateCreated]         DATETIME         NULL,
    [DateModified]        DATETIME         NULL,
    [UserCreated]         UNIQUEIDENTIFIER NULL,
    [UserModified]        UNIQUEIDENTIFIER NULL
);

