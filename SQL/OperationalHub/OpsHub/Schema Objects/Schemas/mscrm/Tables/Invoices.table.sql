﻿CREATE TABLE [mscrm].[Invoices] (
    [ARINVID]   INT              IDENTITY (1, 1) NOT NULL,
    [MSCUST]    UNIQUEIDENTIFIER NULL,
    [IDCUST]    VARCHAR (12)     NULL,
    [INVCDESC]  VARCHAR (60)     NULL,
    [IDINVC]    VARCHAR (22)     NULL,
    [DATEINVC]  DATETIME         NULL,
    [GLACCOUNT] VARCHAR (45)     NULL,
    [AMTNETTOT] DECIMAL (10, 3)  NULL,
    [TEXTTRX]   INT              NULL,
    [TRXSTATUS] INT              NULL,
    [TRXDATE]   DATETIME         NULL,
    [COMMENT]   VARCHAR (60)     NULL
);



