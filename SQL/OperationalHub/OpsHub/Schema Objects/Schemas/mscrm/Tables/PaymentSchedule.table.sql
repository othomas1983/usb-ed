﻿CREATE TABLE [mscrm].[PaymentSchedule] (
    [ID]                  INT             IDENTITY (1, 1) NOT NULL,
    [ProgrammeOfferingID] INT             NULL,
    [Description]         DECIMAL (5, 2)  NULL,
    [Date]                SMALLDATETIME   NULL,
    [Amount]              DECIMAL (19, 3) NULL
);

