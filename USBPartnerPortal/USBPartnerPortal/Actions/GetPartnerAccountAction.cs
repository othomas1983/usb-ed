﻿using System;
using Airborne.Automapper;
using Airborne.Notifications;
using Domain;
using Domain.Models;
using NLog;
using USBPartnerPortal.Models;

namespace USBPartnerPortal.Actions
{
    public class GetPartnerAccountAction<T> where T : class
    {
        private IPartnerPortalContext context;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public GetPartnerAccountAction(IPartnerPortalContext context)
        {
            this.context = context;
        }

        public Func<PartnerAccountViewModel, T> OnSuccess { get; set; }

        public Func<NotificationCollection, T> OnFailed { get; set; }

        public T Invoke(string username, string password, string emailAddress)
        {
            var errors = NotificationCollection.CreateEmpty();

            try
            {
                var partnerAccount = context.AccountService.GetPartnerAccountBy(username, password, emailAddress);

                if (partnerAccount.IsNull())
                {
                    errors.AddError("No matching partner account found.");

                    return OnFailed(errors);
                }

                var partnerAccountMapper = new GenericMapper<PartnerAccount, PartnerAccountViewModel>();
                var partnerAccountViewModel = partnerAccountMapper.MapObjects(partnerAccount);

                if (!partnerAccountViewModel.AccountType.HasValue)
                {
                    errors.AddError("No Account Number or Debtor Code found for this Account.");

                    return OnFailed(errors);
                }
                
                return OnSuccess(partnerAccountViewModel);
            }
            catch (Exception ex)
            {
                Logger.Error("Unable to get Partner Account: " + ex.Message);
                errors.AddError("There was an error processing your request.");
                return OnFailed(errors);
            }
        }
    }
}