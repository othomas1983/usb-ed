﻿using System;
using System.Collections.Generic;
using System.Linq;
using Airborne.Automapper;
using Airborne.Notifications;
using Domain;
using Domain.Models;
using NLog;
using USBPartnerPortal.ViewModels;
using Delegate = Domain.Models.Delegate;

namespace USBPartnerPortal.Actions
{
    public class GetDelegatesBySelectedOfferingAction<T> where T : class
    {
        private IPartnerPortalContext context;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public GetDelegatesBySelectedOfferingAction(IPartnerPortalContext context)
        {
            this.context = context;
        }

        public Func<List<DelegateViewModel>, T> OnSuccess { get; set; }

        public Func<NotificationCollection, T> OnFailed { get; set; }

        public T Invoke(string offeringId)
        {
            var errors = NotificationCollection.CreateEmpty();

            try
            {
                if (offeringId.IsNullOrEmpty())
                {
                    errors.AddError("No short course offering selected.");

                    return OnFailed(errors);
                }

                var delegates = context.DelegateService.GetDelegatesBySelectedOffering(Guid.Parse(offeringId));

                if (!delegates.Any())
                {
                    errors.AddError("No delegates found for the selected offering.");

                    return OnFailed(errors);
                }
                
                var delegatesList = (from item in delegates
                    let delegateMapper = new GenericMapper<Delegate, DelegateViewModel>()
                    select delegateMapper.MapObjects(item)).ToList();

                return OnSuccess(delegatesList);
            }
            catch (Exception ex)
            {
                Logger.Error("Unable to get Delegates for Selected Offering: " + ex.Message);
                errors.AddError("There was an error processing your request.");
                return OnFailed(errors);
            }
        }
    }
}