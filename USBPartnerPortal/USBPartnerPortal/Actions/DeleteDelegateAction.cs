﻿using System;
using Airborne.Notifications;
using Domain;
using NLog;

namespace USBPartnerPortal.Actions
{
    public class DeleteDelegateAction<T> where T : class
    {
        private IPartnerPortalContext context;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public DeleteDelegateAction(IPartnerPortalContext context)
        {
            this.context = context;
        }

        public Func<NotificationCollection, T> OnSuccess { get; set; }

        public Func<NotificationCollection, T> OnFailed { get; set; }

        public T Invoke(string delegateId)
        {
            var notifications = NotificationCollection.CreateEmpty();

            try
            {
                if (delegateId.IsNullOrEmpty())
                {
                    notifications.AddError("No delegate selected.");

                    return OnFailed(notifications);
                }

                notifications = context.DelegateService.DeleteDelegate(Guid.Parse(delegateId));

                return OnSuccess(notifications);
            }
            catch (Exception ex)
            {
                Logger.Error("Unable to delete Delegate: " + ex.Message);
                notifications.AddError("There was an error processing your request.");

                return OnFailed(notifications);
            }
        }
    }
}