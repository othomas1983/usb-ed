﻿using System;
using System.Collections.Generic;
using System.Linq;
using Airborne.Automapper;
using Airborne.Notifications;
using Domain;
using Domain.Models;
using NLog;
using USBPartnerPortal.ViewModels;

namespace USBPartnerPortal.Actions
{
    public class GetPartnerShortCourseOfferingsAction<T> where T : class
    {
        private IPartnerPortalContext context;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public GetPartnerShortCourseOfferingsAction(IPartnerPortalContext context)
        {
            this.context = context;
        }

        public Func<List<ShortCourseOfferingViewModel>, T> OnSuccess { get; set; }

        public Func<NotificationCollection, T> OnFailed { get; set; }

        public T Invoke(string accountId)
        {
            var errors = NotificationCollection.CreateEmpty();

            try
            {
                if (accountId.IsNullOrEmpty())
                {
                    errors.AddError("No account id provided.");

                    return OnFailed(errors);
                }

                var shortCourseOfferings =
                    context.ShortCourseOfferingService.GetShortCourseOfferingsByAccountId(Guid.Parse(accountId));

                if (!shortCourseOfferings.Any())
                {
                    errors.AddError("No offerings found for this account.");

                    return OnFailed(errors);
                }

                var shortCourseOfferingList = (from item in shortCourseOfferings
                    let shortCourseOfferingMapper =
                    new GenericMapper<ShortCourseOffering, ShortCourseOfferingViewModel>()
                    select shortCourseOfferingMapper.MapObjects(item)).ToList();

                return OnSuccess(shortCourseOfferingList);
            }
            catch (Exception ex)
            {
                Logger.Error("Unable to get Short Course Offerings: " + ex.Message);
                errors.AddError("There was an error processing your request.");
                return OnFailed(errors);
            }
        }
    }
}