﻿using System;
using System.Text;
using System.Web.Configuration;
using System.Web.Script.Serialization;
using Airborne.Automapper;
using Airborne.Notifications;
using CrmServiceAdapter.Helpers;
using Domain;
using Domain.Helpers;
using NLog;
using USBPartnerPortal.ViewModels;
using Delegate = Domain.Models.Delegate;

namespace USBPartnerPortal.Actions
{
    public class SendApplicationLinkToDelegate<T> where T : class
    {
        private IPartnerPortalContext context;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public SendApplicationLinkToDelegate(IPartnerPortalContext context)
        {
            this.context = context;
        }

        public Func<NotificationCollection, T> OnSuccess { get; set; }

        public Func<NotificationCollection, T> OnFailed { get; set; }

        public T Invoke(DelegateViewModel delegateDetails, ShortCourseOfferingViewModel selectedOffering)
        {
            var notifications = NotificationCollection.CreateEmpty();

            try
            {
                if (delegateDetails.IsNull() || selectedOffering.IsNull())
                {
                    notifications.AddError("No delegate or program offering selected.");

                    return OnFailed(notifications);
                }
                
                var serializer = new JavaScriptSerializer();

                var jsonObj = serializer.Serialize(new
                {
                    delegateDetails.AccountId,
                    selectedOffering.ShortCourseOfferingId,
                    delegateDetails.DelegateEmail,
                    delegateDetails.DelegateName,
                    delegateDetails.DelegateSurname
                });

                var encFormData = Encryptor.Encrypt(jsonObj);
                var encodedFormData = UrlSafeEncode.Encode(encFormData);

                delegateDetails.DelegateApplicationUrl = $"{WebConfigurationManager.AppSettings["UsbEdShortCourseApplicationFormUrl"]}?token={encodedFormData}";

                var mail = new MailHelper
                {
                    Recipient = delegateDetails.DelegateEmail,
                    RecipientCC = null,
                    Subject = $"Usb-Ed - Online application link for your Programme - {selectedOffering.OfferingName}",
                    Body = EmailBody(delegateDetails, selectedOffering)
                };

                mail.Send();
                notifications.AddMessage(Notification.Create($"Email sent successfully to {delegateDetails.DelegateEmail}", NotificationSeverity.Information));

                var delegateModelMapper = new GenericMapper<DelegateViewModel, Delegate>();
                var delegateModel = delegateModelMapper.MapObjects(delegateDetails);

                context.DelegateService.UpdateDelegateEmailSentStatus(delegateModel);

                return OnSuccess(notifications);
            }
            catch (Exception ex)
            {
                Logger.Error("Unable to send email to delegate: " + ex.Message);
                notifications.AddError("There was an error processing your request.");

                return OnFailed(notifications);
            }
        }

        private string EmailBody(DelegateViewModel delegateDetails, ShortCourseOfferingViewModel selectedOffering)
        {
            var body = new StringBuilder();

            body.Append($"Dear {delegateDetails.DelegateName}");
            body.AppendLine("<br/><br/>");
            body.Append(
                $"We are pleased to inform you that you have been selected to participate on the {selectedOffering.OfferingName} programme presented by USB-ED and Basil Read.");
            body.AppendLine("<br/><br/>");
            body.Append(
                $"Please click on this link <a href='{delegateDetails.DelegateApplicationUrl}'>Online Application Form</a> to apply online.");
            body.AppendLine("<br/><br/>");
            body.Append(
                "Once your application is finalised, you will receive another email with credentials to log in to your course.");
            body.AppendLine("<br/><br/>");
            body.Append($"If you require any support or have any questions about the programme please contact: <a href='mailto: {selectedOffering.OwnerEmailAddress}'>{selectedOffering.OwnerName}</a>");
            body.AppendLine("<br/><br/>");
            body.AppendLine("Regards,");
            body.AppendLine("<br/>");
            body.AppendLine("The learn.usb-ed team");

            return body.ToString();
        }
    }
}