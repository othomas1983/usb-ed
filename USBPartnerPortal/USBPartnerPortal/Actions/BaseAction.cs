﻿using System;
using Airborne.Notifications;
using Domain;

namespace USBPartnerPortal.Actions
{
    public class BaseAction<T> where T : class
    {
        internal NotificationCollection notifications;

        //protected IPartnerPortalContext context;

        public Func<NotificationCollection, T> OnSuccess { get; set; }

        public Func<NotificationCollection, T> OnFailed { get; set; }

        public BaseAction()
        {
            this.notifications = NotificationCollection.CreateEmpty();
            //this.context = context;
        }
    }
}