﻿using System;
using System.Web.Script.Serialization;
using Airborne.Notifications;
using Domain;
using Domain.Helpers;
using NLog;
using USBPartnerPortal.ViewModels;

namespace USBPartnerPortal.Actions
{
    public class CreateDelegateTokenAction<T> where T : class
    {
        private IPartnerPortalContext context;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public CreateDelegateTokenAction(IPartnerPortalContext context)
        {
            this.context = context;
        }

        public Func<TokenViewModel, T> OnSuccess { get; set; }

        public Func<NotificationCollection, T> OnFailed { get; set; }

        public T Invoke(DelegateViewModel delegateDetails)
        {
            var errors = NotificationCollection.CreateEmpty();

            try
            {
                if (delegateDetails.IsNull())
                {
                    errors.AddError("No delegate selected.");

                    return OnFailed(errors);
                }

                var model = new TokenViewModel();
                var serializer = new JavaScriptSerializer();

                var jsonObj = serializer.Serialize(new
                {
                    delegateDetails.AccountId,
                    delegateDetails.SelectedOffering,
                    delegateDetails.DelegateEmail,
                    delegateDetails.DelegateName,
                    delegateDetails.DelegateSurname
                });

                var encryptedData = Encryptor.Encrypt(jsonObj);
                var encodedData = UrlSafeEncode.Encode(encryptedData);

                model.Token = encodedData;

                return OnSuccess(model);
            }
            catch (Exception ex)
            {
                Logger.Error("Unable to generate delegate token: " + ex.Message);
                errors.AddError("There was an error processing your request.");
                return OnFailed(errors);
            }
        }
    }
}