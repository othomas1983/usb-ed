﻿using System;
using System.Collections.Generic;
using System.Linq;
using Airborne.Automapper;
using Airborne.Notifications;
using Domain;
using NLog;
using USBPartnerPortal.ViewModels;
using Delegate = Domain.Models.Delegate;

namespace USBPartnerPortal.Actions
{
    public class AddImportedDelegatesAction<T> where T : class
    {
        private IPartnerPortalContext context;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public AddImportedDelegatesAction(IPartnerPortalContext context)
        {
            this.context = context;
        }

        public Func<NotificationCollection, T> OnSuccess { get; set; }

        public Func<NotificationCollection, T> OnFailed { get; set; }

        public T Invoke(List<DelegateViewModel> importedDelegateDetails, Guid selectedOffering, Guid accountId)
        {
            var notifications = NotificationCollection.CreateEmpty();
            var delegateModels = new List<Delegate>();

            try
            {
                if (!importedDelegateDetails.Any() || selectedOffering == Guid.Empty)
                {
                    notifications.AddError("No short course offering selected.");

                    return OnFailed(notifications);
                }

                delegateModels.AddRange(from item in importedDelegateDetails
                    let newDelegateMapper = new GenericMapper<DelegateViewModel, Delegate>()
                    select newDelegateMapper.MapObjects(item));

                notifications = context.DelegateService.SaveImportedDelegates(delegateModels, selectedOffering, accountId);

                return notifications.HasErrors() ? OnFailed(notifications) : OnSuccess(notifications);
            }
            catch (Exception ex)
            {
                Logger.Error("Unable to save imported Delegates: " + ex.Message);
                notifications.AddError("There was an error processing your request.");

                return OnFailed(notifications);
            }
        }
    }
}