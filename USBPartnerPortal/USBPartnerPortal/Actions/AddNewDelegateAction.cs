﻿using System;
using Airborne.Automapper;
using Airborne.Notifications;
using Domain;
using NLog;
using USBPartnerPortal.ViewModels;
using Delegate = Domain.Models.Delegate;

namespace USBPartnerPortal.Actions
{
    public class AddNewDelegateAction<T> where T : class
    {
        private IPartnerPortalContext context;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public AddNewDelegateAction(IPartnerPortalContext context)
        {
            this.context = context;
        }

        public Func<NotificationCollection, T> OnSuccess { get; set; }

        public Func<NotificationCollection, T> OnFailed { get; set; }

        public T Invoke(DelegateViewModel newDelegate)
        {
            var notifications = NotificationCollection.CreateEmpty();

            try
            {
                if (newDelegate.IsNull() || !newDelegate.SelectedOffering.HasValue)
                {
                    notifications.AddError("No short course offering selected.");

                    return OnFailed(notifications);
                }

                var newDelegateMapper = new GenericMapper<DelegateViewModel, Delegate>();
                var delegateModel = newDelegateMapper.MapObjects(newDelegate);

                notifications = context.DelegateService.SaveDelegate(delegateModel);

                if (notifications.HasErrors())
                {
                    return OnFailed(notifications);
                }
                
                return OnSuccess(notifications);
            }
            catch (Exception ex)
            {
                Logger.Error("Unable to save new Delegate: " + ex.Message);
                notifications.AddError("There was an error processing your request.");

                return OnFailed(notifications);
            }
        }
    }
}