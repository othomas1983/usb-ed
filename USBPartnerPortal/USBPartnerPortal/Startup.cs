﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(USBPartnerPortal.Startup))]
namespace USBPartnerPortal
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
