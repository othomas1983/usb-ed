﻿using System.Security.Claims;
using System.Web.Mvc;
using USBPartnerPortal.Security;

namespace USBPartnerPortal.Views
{
    public abstract class ApplicationViewPage<TModel> : WebViewPage<TModel>
    {
        protected AppUser CurrentUser => new AppUser(User as ClaimsPrincipal);
    }

    public abstract class ApplicationViewPage : ApplicationViewPage<dynamic>
    {

    }
}