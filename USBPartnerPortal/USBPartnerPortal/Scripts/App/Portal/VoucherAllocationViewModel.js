﻿"use strict";

ko.validation.registerExtenders();
ko.validation.init({
    insertMessages: true,
    errorElementClass: 'has-error',
    decorateInputElement: true
}, true);

var VoucherAllocationViewModel = function() {

    var self = this;

    // Observables

    self.selectedOffering = ko.observable();

    self.delegateId = ko.observable();
    self.delegateName = ko.observable().extend({ required: true });
    self.delegateSurname = ko.observable().extend({ required: true });
    self.delegateEmail = ko.observable().extend({
        required: true,
        email: true
    });

    self.delegates = ko.observableArray();
    self.selectedDelegate = ko.observable();

    self.delegateToken = ko.observable();

    self.editDelegateSelectedOffering = ko.observable();

    self.canUpdate = ko.observable(false);

    self.canChangeOffering = ko.observable(false);

    self.errors = ko.validation.group(self);

    //
    // Subscribers

    self.selectedOffering.subscribe(function (data) {
        if (data != undefined) {
            self.editDelegateSelectedOffering(data);
            self.getDelegatesBySelectedOffering(data.ShortCourseOfferingId);
        }
    });

    amplify.subscribe("viewDelegateByShortCourseOffering",
        function (data) {
            if (data != undefined) {
                self.selectedOffering(data);
            }
        });

    //
    // Functions

    self.checkEnrollmentStatus = function (data) {
        if (data != undefined) {
            if (data !== "None") {
                return true;
            }
            return false;
        }
        return false;
    };

    self.getDelegateLinkButtonText = function (rowNum, description) {

        if (rowNum != undefined && description != undefined) {
            if (description !== "None") {
                $("#sendDelegateLinkButton" + rowNum).removeClass("btn-default").addClass("btn-warning");
                return description;
            }
        }
        $("#sendDelegateLinkButton" + rowNum).removeClass("btn-warning").addClass("btn-default");
        return "Send Link";
    };

    self.isValid = function (data) {
        //if (data != null &&
        //(data.DelegateName === "" ||
        //    data.DelegateSurname === "" ||
        //    data.DelegateEmail === "" ||
        //    self.editDelegateSelectedOffering() === null ||
        //    self.editDelegateSelectedOffering() === undefined)) {
        //    self.canUpdate(false);
        //} else {
        //    self.canUpdate(true);
        //}

        if (data != null &&
        (self.delegateName().length == 0 ||
            self.delegateSurname().length == 0 ||
            self.delegateEmail().length == 0)) {
            self.canUpdate(false);
        } else {
            self.canUpdate(true);
        }
    };

    self.resolveNumberOfVouchersPurchased = function(data) {
        if (data === 0) {
            return "N/A";
        }
        return data;
    };

    self.getDelegatesBySelectedOffering = function(offeringId) {
        document.getElementById("errorsCallout").style.display = "none";
        self.delegates(null);

        if (offeringId === undefined) {
            alert("No offering selected.");
        } else {
            amplify.publish("busyLoading", "#dashboardSection", "show");

            $.ajax({
                url: "VoucherAllocation/GetDelegatesBySelectedOffering?offeringId=" + offeringId,
                type: "GET",
                contentType: "application/json",
                success: function(data) {
                    var model = JSON.parse(data);

                    if (model.Errors != undefined && model.Errors.length > 0) {
                        var errors = { Heading: model.ErrorTitle, Errors: model.Errors };
                        amplify.publish("notifications.errors", errors);
                    } else {
                        self.delegates(model);

                        if ($.fn.DataTable.isDataTable("#approvedDelegatesTable")) {
                            $("#approvedDelegatesTable").dataTable().fnDestroy();
                        }

                        $("#approvedDelegatesTable").DataTable({
                            "order": [],
                            "pageLength": 5,
                            "lengthMenu": [[5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100, "All"]]
                        });
                    }
                    amplify.publish("busyLoading", "#dashboardSection", "hide");

                    amplify.publish("recreatePopoverDialog", "#csvImportPopover");
                },
                error: function(error) {
                    amplify.publish("busyLoading", "#dashboardSection", "hide");

                    var status = error.status;
                    var statusText = error.statusText;
                    var responseText = error.responseText;

                    alert(statusText);
                }
            });
        }
    }

    self.openCSVImport = function () {
        $("#csvImportFile").click();
    };

    self.csvFileUpload = function(data, event) {
        document.getElementById("errorsCallout").style.display = "none";
        amplify.publish("busyLoading", "#dashboardSection", "show");

        var importedDelegates = new Array();

        if (event != undefined && event.target.files.length > 0) {
            Papa.parse(event.target.files[0],
            {
                header: false,
                dynamicTyping: true,
                complete: function (results) {
                    if (results != undefined && results.data != undefined) {
                        if (results.errors.length > 0) {
                            alert(results.errors[0].message);
                        }
                        results.data.every(function(item, index) {
                            if (item.length >= 3) {
                                if (item[0].length > 0)
                                    self.delegateName(item[0]);
                                if (item[1].length > 0)
                                    self.delegateSurname(item[1]);
                                if (item[2].length > 0)
                                    self.delegateEmail(item[2]);

                                if (self.errors().length > 0) {
                                    alert("Error on line " + (index + 1) + ": " + self.errors()[0]);
                                    return false;
                                } else {
                                    var importedObj = {
                                        delegateName: self.delegateName(),
                                        delegateSurname: self.delegateSurname(),
                                        delegateEmail: self.delegateEmail()
                                    };

                                    importedDelegates.push(importedObj);
                                }
                            }
                            return true;
                        });

                        self.saveImportedDelegateDetails(importedDelegates);
                    }

                    self.clearCSVFileImport();
                    amplify.publish("busyLoading", "#dashboardSection", "hide");
                }
            });

        } else {
            amplify.publish("busyLoading", "#dashboardSection", "hide");
            alert("Please select a .csv file to import.");
        }
    };

    self.clearCSVFileImport = function() {
        var csvImportFileEl = $('#csvImportFile');
        csvImportFileEl.wrap("<form>").closest("form").get(0).reset();
        csvImportFileEl.unwrap();
    };

    self.openStudentDialog = function (data) {
        self.delegateId(null);
        self.delegateName(null);
        self.delegateSurname(null);
        self.delegateEmail(null);
        self.errors.showAllMessages(false);

        if (data != undefined) {

            if (data.DelegateId != undefined)
                self.delegateId(data.DelegateId);
            if (data.DelegateName != undefined)
                self.delegateName(data.DelegateName);
            if (data.DelegateSurname != undefined)
                self.delegateSurname(data.DelegateSurname);
            if (data.DelegateEmail != undefined)
                self.delegateEmail(data.DelegateEmail);

        }
        $("#studentDialog").modal("show");
    };

    self.saveDelegateDetails = function () {
        if (self.errors().length === 0) {
            document.getElementById("infoCallout").style.display = "none";
            document.getElementById("errorsCallout").style.display = "none";
            $("#saveDelegate").attr("disabled", true);

                $.ajax({
                    url: "VoucherAllocation/SaveDelegateDetails",
                    type: "POST",
                    data: JSON.stringify({
                        "delegateId": self.delegateId(),
                        "delegateName": self.delegateName(),
                        "delegateSurname": self.delegateSurname(),
                        "delegateEmail": self.delegateEmail(),
                        "selectedOffering": self.selectedOffering().ShortCourseOfferingId
                    }),
                    contentType: "application/json",
                    success: function(data) {
                        var model = JSON.parse(data);

                        if (model.Errors != undefined && model.Errors.length > 0) {
                            var errors = { Heading: model.ErrorTitle, Errors: model.Errors };
                            amplify.publish("notifications.errors", errors);
                        }

                        if (model.Information != undefined && model.Information.length > 0) {
                            self.getDelegatesBySelectedOffering(self.selectedOffering().ShortCourseOfferingId);

                            var info = { Heading: model.InformationTitle, Messages: model.Information };
                            amplify.publish("notifications.info", info);

                            self.delegateId(null);
                            self.delegateName(null);
                            self.delegateSurname(null);
                            self.delegateEmail(null);

                            $("#saveDelegate").attr("disabled", false);
                        }

                        $("#studentDialog").modal("hide");
                    },
                    error: function(error) {
                        $("#saveDelegate").attr("disabled", false);

                        var status = error.status;
                        var statusText = error.statusText;
                        var responseText = error.responseText;

                        alert(statusText);
                    }
                });
            }
    };

    self.saveImportedDelegateDetails = function (delegates) {
        if (self.errors().length === 0 && delegates.length > 0) {
            document.getElementById("infoCallout").style.display = "none";
            document.getElementById("errorsCallout").style.display = "none";
            amplify.publish("busyLoading", "#dashboardSection", "show");

            $.ajax({
                url: "VoucherAllocation/SaveImportedDelegateDetails",
                type: "POST",
                data: JSON.stringify({
                    "importedDelegateDetails": delegates,
                    "selectedOffering": self.selectedOffering().ShortCourseOfferingId
                }),
                contentType: "application/json",
                success: function (data) {
                    var model = JSON.parse(data);

                    if (model.Errors != undefined && model.Errors.length > 0) {
                        var errors = { Heading: model.ErrorTitle, Errors: model.Errors };
                        amplify.publish("notifications.errors", errors);
                    }

                    if (model.Information != undefined && model.Information.length > 0) {
                        self.getDelegatesBySelectedOffering(self.selectedOffering().ShortCourseOfferingId);

                        var info = { Heading: model.InformationTitle, Messages: model.Information };
                        amplify.publish("notifications.info", info);

                        self.delegateId(null);
                        self.delegateName(null);
                        self.delegateSurname(null);
                        self.delegateEmail(null);
                    }

                    amplify.publish("busyLoading", "#dashboardSection", "hide");
                },
                error: function (error) {
                    amplify.publish("busyLoading", "#dashboardSection", "hide");

                    var status = error.status;
                    var statusText = error.statusText;
                    var responseText = error.responseText;

                    alert(statusText);
                }
            });
        }
    };
    
    self.editDelegate = function(item) {

        if (item != undefined) {
            self.selectedDelegate(item);
            $("#editDelegateDialog").modal("show");
        }

    };

    self.deleteDelegateDialog = function (item) {
        if (item != undefined) {
            self.delegateId(item.DelegateId);
            $("#deleteDelegateDialog").modal("show");
        }
    };

    self.updateDelegateDetails = function(data) {
        if (self.errors().length === 0) {
            document.getElementById("infoCallout").style.display = "none";
            document.getElementById("errorsCallout").style.display = "none";

            //if (data === undefined) {
            //    alert("No delegate selected to update.");
            //} else {
            //    $.ajax({
            //        url: "VoucherAllocation/UpdateDelegateDetails",
            //        type: "POST",
            //        data: JSON.stringify({
            //            "delegateId": data.DelegateId,
            //            "delegateName": data.DelegateName,
            //            "delegateSurname": data.DelegateSurname,
            //            "delegateEmail": data.DelegateEmail,
            //            "selectedOffering": self.editDelegateSelectedOffering().ShortCourseOfferingId,
            //            "accountId": data.AccountId
            //        }),
            //        contentType: "application/json",
            //        success: function(data) {
            //            var model = JSON.parse(data);

            //            if (model.Errors != undefined && model.Errors.length > 0) {
            //                var errors = { Heading: model.ErrorTitle, Errors: model.Errors };
            //                amplify.publish("notifications.errors", errors);
            //            }

            //            if (model.Information != undefined && model.Information.length > 0) {
            //                self.getDelegatesBySelectedOffering(self.selectedOffering().ShortCourseOfferingId);

            //                var info = { Heading: model.InformationTitle, Messages: model.Information };
            //                amplify.publish("notifications.info", info);
            //            }

            //            self.delegateName("");
            //            self.delegateSurname("");
            //            self.delegateEmail("");

            //            $("#editDelegateDialog").modal("hide");
            //        },
            //        error: function(error) {

            //            var status = error.status;
            //            var statusText = error.statusText;
            //            var responseText = error.responseText;

            //            alert(statusText);
            //        }
            //    });
            //}
        }
    };

    self.deleteDelegate = function () {
        document.getElementById("infoCallout").style.display = "none";
        document.getElementById("errorsCallout").style.display = "none";
        $("#deleteDelegate").attr("disabled", true);

        if (self.delegateId().length == 0) {
            alert("No delegate selected to delete.");
        } else {
            $.ajax({
                url: "VoucherAllocation/DeleteDelegate?delegateId=" + self.delegateId(),
                type: "GET",
                contentType: "application/json",
                success: function(data) {
                    var model = JSON.parse(data);

                    if (model.Errors != undefined && model.Errors.length > 0) {
                        var errors = { Heading: model.ErrorTitle, Errors: model.Errors };
                        amplify.publish("notifications.errors", errors);
                    }

                    if (model.Information != undefined && model.Information.length > 0) {
                        self.getDelegatesBySelectedOffering(self.selectedOffering().ShortCourseOfferingId);

                        var info = { Heading: model.InformationTitle, Messages: model.Information };
                        amplify.publish("notifications.info", info);
                    }

                    self.delegateName("");
                    self.delegateSurname("");
                    self.delegateEmail("");

                    $("#deleteDelegate").attr("disabled", false);
                    $("#deleteDelegateDialog").modal("hide");
                },
                error: function(error) {
                    $("#deleteDelegate").attr("disabled", false);

                    var status = error.status;
                    var statusText = error.statusText;
                    var responseText = error.responseText;

                    alert(statusText);
                }
            });
        }
    }

    self.sendApplicationLink = function(data) {
        document.getElementById("infoCallout").style.display = "none";
        document.getElementById("errorsCallout").style.display = "none";

        if (data === undefined) {
            alert("No delegate or program offering selected.");
        } else {
            $.ajax({
                url: "VoucherAllocation/SendApplicationLink/",
                type: "POST",
                data: JSON.stringify({ delegateDetails: data, selectedOffering: self.selectedOffering() }),
                contentType: "application/json",
                success: function(data) {
                    var model = JSON.parse(data);

                    if (model.Errors != undefined && model.Errors.length > 0) {
                        var errors = { Heading: model.ErrorTitle, Errors: model.Errors };
                        amplify.publish("notifications.errors", errors);
                    }

                    if (model.Information != undefined && model.Information.length > 0) {

                        var info = { Heading: model.InformationTitle, Messages: model.Information };
                        amplify.publish("notifications.info", info);
                    }
                },
                error: function(error) {

                    var status = error.status;
                    var statusText = error.statusText;
                    var responseText = error.responseText;

                    alert(statusText);
                }
            });
        }
    };

    self.sendMailToDelegate = function (data) {
        if (data !== undefined) {
            window.location.assign("mailto:" + data.DelegateEmail + "?Subject=" + self.selectedOffering().OfferingName);
        }
    };

    self.applyOnline = function (item) {
        if (item != undefined) {
            amplify.publish("busyLoading", "#selectOfferingSection", "show");

            $.ajax({
                url: "VoucherAllocation/CreateDelegateToken",
                data: JSON.stringify(item),
                type: "POST",
                contentType: "application/json",
                success: function (data) {
                    var model = JSON.parse(data);

                    if (model.Errors != undefined && model.Errors.length > 0) {
                        var errors = { Heading: model.ErrorTitle, Errors: model.Errors };
                        amplify.publish("notifications.errors", errors);
                    } else {
                        //self.delegateToken(model.Token);
                        $("#tokenId").val(model.Token);
                        $("#delegateApplyOnline").submit();
                    }

                    amplify.publish("busyLoading", "#selectOfferingSection", "hide");
                },
                error: function(error) {
                    amplify.publish("busyLoading", "#selectOfferingSection", "hide");

                    var status = error.status;
                    var statusText = error.statusText;
                    var responseText = error.responseText;

                    alert(statusText);
                }
            });
        }
    };

    //
};