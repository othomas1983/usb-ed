﻿"use strict";

var AccountProfileViewModel = function() {

    var self = this;

    // Observables



    //
    // Subscribers



    //
    // Functions

    //self.resolveTotalPurchased = function(data) {
    //    if (data !== undefined) {
    //        debugger;
    //        self.totalPurchased((self.totalPurchased() + data));
    //    }
    //    return self.totalPurchased();
    //};

    //self.resolveTotalRedeemable = function(data) {
    //    if (data !== undefined) {
    //        debugger;
    //        self.totalRedeemable((self.totalRedeemable() + data));
    //    }
    //    return self.totalRedeemable();
    //};

    //self.resolveTotalRedeemed = function(data) {
    //    if (data !== undefined) {
    //        debugger;
    //        self.totalRedeemable((self.totalRedeemable() + data));
    //    }
    //    return self.totalRedeemable();
    //};

    self.viewDelegateByShortCourseOffering = function (data) {

        if (data != undefined) {
            amplify.publish("isAccountProfileVisible", false);
            amplify.publish("isVoucherAllocationVisible", true);

            amplify.publish("viewDelegateByShortCourseOffering", data);
        }

    };
    //
};