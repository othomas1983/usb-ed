﻿"use strict";

var PortalViewModel = function() {

    var self = this;

    // Observables

    self.isDashboardPanel = ko.observable(false);
    self.isLearnerProfile = ko.observable(false);
    self.isVoucherAllocation = ko.observable(false);
    self.isAccountProfile = ko.observable(true);

    self.offerings = ko.observableArray();

    self.totalPurchased = ko.observable(0);
    self.totalRedeemable = ko.observable(0);
    self.totalRedeemed = ko.observable(0);

    // Learner Profile VM observable
    self.LearnerProfile = ko.observable(new LearnerProfileViewModel());

    // Offering Selection VM observable
    self.VoucherAllocation = ko.observable(new VoucherAllocationViewModel());

    // Account Profile VM observable
    self.AccountProfile = ko.observable(new AccountProfileViewModel());

    //
    // Subscribers

    amplify.subscribe("isDashboardPanelVisible",
        function(data) {
            if (data != undefined) {
                self.showSectionByTopic("isDashboardPanelVisible");
            }
        });

    amplify.subscribe("isLearnerProfileVisible",
        function(data) {
            if (data != undefined) {
                self.showSectionByTopic("isLearnerProfileVisible");
            }
        });

    amplify.subscribe("isVoucherAllocationVisible",
        function(data) {
            if (data != undefined) {
                self.showSectionByTopic("isVoucherAllocationVisible");
            }
        });

    amplify.subscribe("isAccountProfileVisible",
        function(data) {
            if (data != undefined) {
                self.showSectionByTopic("isAccountProfileVisible");
            }
        });

    // TODO: Start using this as shared loading overlay
    amplify.subscribe("busyLoading",
        function (id, state) {
            if (id != undefined && state != undefined) {
                self.toggleLoadingOverlay(id, state);
            }
        });

    //
    // Functions

    self.loadOfferings = function() {

        document.getElementById("errorsCallout").style.display = "none";
        amplify.publish("busyLoading", "#dashboardSection", "show");

        $.ajax({
            url: "Portal/LoadAvailableShortCourseOfferings",
            type: "GET",
            contentType: "application/json",
            success: function(data) {
                var model = JSON.parse(data);

                if (model.Errors != undefined && model.Errors.length > 0) {
                    var errors = { Heading: model.ErrorTitle, Errors: model.Errors };
                    amplify.publish("notifications.errors", errors);
                } else {
                    self.offerings(model);

                    for (var i = 0; i < model.length; i++) {
                        self.totalPurchased((self.totalPurchased() + model[i].NumberOfVouchersPurchased));
                        self.totalRedeemable((self.totalRedeemable() + model[i].NumberOfVouchersRedeemable));
                        self.totalRedeemed((self.totalRedeemed() + model[i].NumberOfVouchersRedeemed));
                    }

                    if ($.fn.DataTable.isDataTable("#availableShortCourseOfferingsTable")) {
                        $("#availableShortCourseOfferingsTable").dataTable().fnDestroy();
                    }

                    $("#availableShortCourseOfferingsTable").DataTable({
                        "order": [],
                        "pageLength": 5,
                        "lengthMenu": [[5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100, "All"]]
                    });
                }

                amplify.publish("busyLoading", "#dashboardSection", "hide");
            },
            error: function(error) {

                amplify.publish("busyLoading", "#dashboardSection", "hide");
                var status = error.status;
                var statusText = error.statusText;
                var responseText = error.responseText;

                alert(statusText);
            },
            done: function() {
                
            }
        });

    };

    self.loadLearnerProfile = function() {
        self.showSectionByTopic("isLearnerProfileVisible");
    };

    self.loadVoucherAllocation = function() {
        self.showSectionByTopic("isVoucherAllocationVisible");
    };

    self.loadAccountProfile = function() {
        self.showSectionByTopic("isAccountProfileVisible");
    };

    self.showSectionByTopic = function(topic) {

        document.getElementById("errorsCallout").style.display = "none";
        //amplify.publish("isDashboardPanelVisible", false);
        //amplify.publish("isVoucherAllocationVisible", false);
        //amplify.publish("isLearnerProfileVisible", false);
        //amplify.publish("isAccountProfileVisible", false);

        self.isDashboardPanel(false);
        self.isVoucherAllocation(false);
        self.isLearnerProfile(false);
        self.isAccountProfile(false);


        switch (topic) {
        case "isDashboardPanelVisible":
            //amplify.publish("isDashboardPanelVisible", true);
            self.isDashboardPanel(true);
            break;
        case "isVoucherAllocationVisible":
            //amplify.publish("isVoucherAllocationVisible", true);
            self.isVoucherAllocation(true);
            break;
        case "isLearnerProfileVisible":
            //amplify.publish("isLearnerProfileVisible", true);
            self.isLearnerProfile(true);
            break;
        case "isAccountProfileVisible":
            //amplify.publish("isAccountProfileVisible", true);
            self.isAccountProfile(true);
            break;
        }

    };

    //self.back = function () {
    //    amplify.publish("isDashboardPanelVisible", true);
    //    amplify.publish("isLearnerProfileVisible", false);
    //    amplify.publish("isVoucherAllocationVisible", false);
    //    amplify.publish("isAccountProfileVisible", false);
    //};

    self.toggleLoadingOverlay = function(elementId, state) {

        switch (state) {
        case "show":
            $(elementId)
                .LoadingOverlay(state,
                {
                    image: "./Content/img/avatar-red.gif",
                    size: "30%"
                });
            break;
        case "hide":
            $(elementId).LoadingOverlay(state, true);
            break;
        }
    };

};