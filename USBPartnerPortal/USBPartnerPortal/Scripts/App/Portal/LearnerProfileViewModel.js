﻿"use strict";

var LearnerProfileViewModel = function() {

    var self = this;

    // Observables

    self.selectedOffering = ko.observable();

    self.token = ko.observable();

    //
    // Subscriptions

    self.selectedOffering.subscribe(function (data) {
        if (data != null) {
            self.createToken(data.ShortCourseOfferingId);
            self.clearToken();
        }
    });

    //
    // Functions

    self.resolveNumberOfVouchersPurchased = function (data) {
        if (data === 0) {
            return "N/A";
        }
        return data;
    };

    self.createToken = function (selectedOfferingId) {

        amplify.publish("showLearnerProfileSection", "#learnerProfileSection", "show");

        var data = { shortCourseOfferingId: selectedOfferingId };

        $.ajax({
            url: "Portal/CreateToken",
            data: JSON.stringify(data),
            type: "POST",
            contentType: "application/json",
            success: function (data) {
                var isTokenValid = self.isTokenValid(data);

                if (isTokenValid) {
                    var model = JSON.parse(data);

                    if (model.Errors != undefined && model.Errors.length > 0) {
                        var errors = { Heading: model.ErrorTitle, Errors: model.Errors };
                        amplify.publish("notifications.errors", errors);
                    } else {
                        self.token(model.Token);
                    }

                    amplify.publish("showLearnerProfileSection", "#learnerProfileSection", "hide");
                }
            },
            error: function (error) {
                amplify.publish("showLearnerProfileSection", "#learnerProfileSection", "hide");

                var status = error.status;
                var statusText = error.statusText;
                var responseText = error.responseText;

                alert(statusText);
            }
        });
    }

    self.clearToken = function () {
        setTimeout(function () {
            self.selectedOffering(null);
        },
            60000);
    };

    self.isTokenValid = function (data) {
        if (data === null || data === "") {
            alert("Your session expired. Please log in again.");
            window.location.href = "Account/Login";
        }

        return true;
    };

    //
};