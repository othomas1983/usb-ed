﻿"use strict";

var NavigationViewModel = function() {

    var self = this;

    // Observables


    // Subscribers


    // Functions

    self.showAccountProfile = function() {
        amplify.publish("isAccountProfileVisible", true);
    }

    self.showDashboard = function() {
        amplify.publish("isDashboardPanelVisible", true);
    }

    self.showLearnerProfile = function() {
        amplify.publish("isVoucherAllocationVisible", true);
    }

    // Shared //
    // On dropdown open
    $(document).on('shown.bs.dropdown',
        function(event) {
            var dropdown = $(event.target);

            // Set aria-expanded to true
            dropdown.find('.dropdown-menu').attr('aria-expanded', true);

            // Set focus on the first link in the dropdown
            setTimeout(function() {
                    dropdown.find('.dropdown-menu .toggleDropdownFocus').focus();
                },
                10);
        });

    amplify.subscribe("recreatePopoverDialog",
        function(elId) {
            if (elId != undefined) {
                $(elId).popover({
                    container: "body",
                    placement: "right",
                    trigger: "hover"
                });
            }
        });
};