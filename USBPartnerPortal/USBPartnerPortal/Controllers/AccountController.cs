﻿using System;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using Domain;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using USBPartnerPortal.Actions;
using USBPartnerPortal.Models;

namespace USBPartnerPortal.Controllers
{
    [AllowAnonymous]
    public class AccountController : BaseController
    {
        public AccountController(IPartnerPortalContext context)
            : base(context)
        {
            this.context = context;
        }

        //
        // GET: /Account/Login
        [HttpGet]
        public ActionResult Login(string returnUrl)
        {
            var model = new LoginViewModel
            {
                ReturnUrl = returnUrl
            };

            ViewBag.PageHeading = "PARTNER DASHBOARD";

            return View(model);
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            ViewBag.PageHeading = "PARTNER DASHBOARD";

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var action = new GetPartnerAccountAction<ActionResult>(context)
            {
                OnSuccess = (partnerAccount) =>
                {
                    var identity = new ClaimsIdentity(new[]
                        {
                            new Claim(ClaimTypes.Sid, partnerAccount.AccountId.ToString()),
                            new Claim(ClaimTypes.GivenName, partnerAccount.AccountName),
                            new Claim(ClaimTypes.Name, partnerAccount.Username),
                            new Claim(ClaimTypes.Email, partnerAccount.EmailAddress),
                            new Claim(ClaimTypes.Uri,
                                partnerAccount.AccountImageUrl.IsNotNullOrEmpty() ? partnerAccount.AccountImageUrl : ""),
                            new Claim("CanNominateDelegates", partnerAccount.CanNominateDelegates.ToString()),
                            new Claim("AccountType", partnerAccount.AccountType.ToString())
                        },
                        DefaultAuthenticationTypes.ApplicationCookie);

                    var ctx = Request.GetOwinContext();
                    var authManager = ctx.Authentication;

                    authManager.SignIn(identity);

                    return RedirectToLocal(returnUrl);
                },
                OnFailed = (errors) =>
                {
                    model.Notifications = errors;
                    ModelState.AddModelError("", errors);

                    return View(model);
                }
            };

            return action.Invoke(model.Username, model.Password, null);
        }

        //
        // POST: /Account/LogOff
        public ActionResult LogOff()
        {
            var ctx = Request.GetOwinContext();
            var authManager = ctx.Authentication;

            authManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Portal");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {

            }

            base.Dispose(disposing);
        }

        #region Helpers

        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get { return HttpContext.GetOwinContext().Authentication; }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Portal");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties {RedirectUri = RedirectUri};
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }

        #endregion
    }
}