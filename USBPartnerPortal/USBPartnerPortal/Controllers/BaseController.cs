﻿using System.Security.Claims;
using System.Web.Mvc;
using Domain;
using USBPartnerPortal.Security;

namespace USBPartnerPortal.Controllers
{
    [Authorize]
    public class BaseController : Controller
    {
        internal IPartnerPortalContext context;

        public BaseController(IPartnerPortalContext context)
        {
            this.context = context;
        }

        public AppUser CurrentUser => new AppUser(User as ClaimsPrincipal);
    }
}