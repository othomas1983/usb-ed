﻿using System.Web.Mvc;
using Domain;
using USBPartnerPortal.Actions;
using USBPartnerPortal.Models;
using USBPartnerPortal.ViewModels;

namespace USBPartnerPortal.Controllers
{
    public class PortalController : BaseController
    {
        public PortalController(IPartnerPortalContext context)
            : base(context)
        {
            this.context = context;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult LoadAvailableShortCourseOfferings()
        {
            var model = new ShortCourseOfferingViewModel();

            var action = new GetPartnerShortCourseOfferingsAction<ActionResult>(context)
            {
                OnSuccess =
                    (shortCourseOfferings) => Json(Newtonsoft.Json.JsonConvert.SerializeObject(shortCourseOfferings),
                        JsonRequestBehavior.AllowGet),
                OnFailed = (errors) =>
                {
                    model.Notifications = errors;

                    return Json(Newtonsoft.Json.JsonConvert.SerializeObject(model), JsonRequestBehavior.AllowGet);
                }
            };

            return
                action.Invoke(CurrentUser.AccountId);
        }
    }
}