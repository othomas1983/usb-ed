﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using CrmServiceAdapter.Helpers;
using Domain;
using USBPartnerPortal.Actions;
using USBPartnerPortal.ViewModels;

namespace USBPartnerPortal.Controllers
{
    public class VoucherAllocationController : BaseController
    {
        public VoucherAllocationController(IPartnerPortalContext context, MailHelper mailer)
            : base(context)
        {
            this.context = context;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SaveDelegateDetails(DelegateViewModel delegateDetails)
        {
            var model = new DelegateViewModel();
            delegateDetails.AccountId = Guid.Parse(CurrentUser.AccountId);

            var action = new AddNewDelegateAction<ActionResult>(context)
            {
                OnSuccess =
                    (info) =>
                    {
                        model.Notifications = info;
                        return Json(Newtonsoft.Json.JsonConvert.SerializeObject(model), JsonRequestBehavior.AllowGet);
                    },
                OnFailed = (errors) =>
                {
                    model.Notifications = errors;
                    return Json(Newtonsoft.Json.JsonConvert.SerializeObject(model), JsonRequestBehavior.AllowGet);
                }
            };

            return action.Invoke(delegateDetails);
        }

        [HttpPost]
        public ActionResult SaveImportedDelegateDetails(List<DelegateViewModel> importedDelegateDetails,
            Guid selectedOffering)
        {
            var model = new DelegateViewModel();

            var action = new AddImportedDelegatesAction<ActionResult>(context)
            {
                OnSuccess =
                    (info) =>
                    {
                        model.Notifications = info;
                        return Json(Newtonsoft.Json.JsonConvert.SerializeObject(model), JsonRequestBehavior.AllowGet);
                    },
                OnFailed = (errors) =>
                {
                    model.Notifications = errors;
                    return Json(Newtonsoft.Json.JsonConvert.SerializeObject(model), JsonRequestBehavior.AllowGet);
                }
            };

            return action.Invoke(importedDelegateDetails, selectedOffering, Guid.Parse(CurrentUser.AccountId));
        }

        [HttpGet]
        public ActionResult GetDelegatesBySelectedOffering(string offeringId)
        {
            var model = new DelegateViewModel();

            var action = new GetDelegatesBySelectedOfferingAction<ActionResult>(context)
            {
                OnSuccess =
                    (delegates) =>
                            Json(Newtonsoft.Json.JsonConvert.SerializeObject(delegates), JsonRequestBehavior.AllowGet),
                OnFailed = (errors) =>
                {
                    model.Notifications = errors;
                    return Json(Newtonsoft.Json.JsonConvert.SerializeObject(model), JsonRequestBehavior.AllowGet);
                }
            };

            return action.Invoke(offeringId);
        }

        [HttpGet]
        public ActionResult DeleteDelegate(string delegateId)
        {
            var model = new DelegateViewModel();

            var action = new DeleteDelegateAction<ActionResult>(context)
            {
                OnSuccess =
                    (info) =>
                    {
                        model.Notifications = info;
                        return Json(Newtonsoft.Json.JsonConvert.SerializeObject(model), JsonRequestBehavior.AllowGet);
                    },
                OnFailed = (errors) =>
                {
                    model.Notifications = errors;
                    return Json(Newtonsoft.Json.JsonConvert.SerializeObject(model), JsonRequestBehavior.AllowGet);
                }
            };

            return action.Invoke(delegateId);
        }

        [HttpPost]
        public ActionResult CreateDelegateToken(DelegateViewModel delegateDetails)
        {
            var model = new TokenViewModel();
            delegateDetails.AccountId = Guid.Parse(CurrentUser.AccountId);

            var action = new CreateDelegateTokenAction<ActionResult>(context)
            {
                OnSuccess =
                    (token) => Json(Newtonsoft.Json.JsonConvert.SerializeObject(token), JsonRequestBehavior.AllowGet),
                OnFailed = (errors) =>
                {
                    model.Notifications = errors;
                    return Json(Newtonsoft.Json.JsonConvert.SerializeObject(model), JsonRequestBehavior.AllowGet);
                }
            };

            return action.Invoke(delegateDetails);
        }

        [HttpPost]
        public ActionResult SendApplicationLink(DelegateViewModel delegateDetails, ShortCourseOfferingViewModel selectedOffering)
        {
            var model = new DelegateViewModel();

            var action = new SendApplicationLinkToDelegate<ActionResult>(context)
            {
                OnSuccess = (info) =>
                {
                    model.Notifications = info;
                    return Json(Newtonsoft.Json.JsonConvert.SerializeObject(model), JsonRequestBehavior.AllowGet);
                },
                OnFailed = (errors) =>
                {
                    model.Notifications = errors;
                    return Json(Newtonsoft.Json.JsonConvert.SerializeObject(model), JsonRequestBehavior.AllowGet);
                }
            };

            return action.Invoke(delegateDetails, selectedOffering);
        }
    }
}