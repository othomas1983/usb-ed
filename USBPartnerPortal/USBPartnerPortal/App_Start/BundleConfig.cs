﻿using System.Web;
using System.Web.Optimization;

namespace USBPartnerPortal
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/appLibs").IncludeDirectory(
                "~/Scripts/App/", "*.js", searchSubdirectories: true));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/Libs/jquery-{version}.js",
                "~/Scripts/Libs/jquery-ui-{version}.js",
                "~/Scripts/Libs/jquery-dataTables.js",
                "~/Scripts/Libs/moment.js",
                "~/Scripts/Libs/knockout-{version}.js",
                "~/Scripts/Libs/knockout.validation*",
                "~/Scripts/Libs/JSLINQ.js",
                "~/Scripts/Libs/loadingoverlay.js",
                "~/Scripts/Libs/amplify.js",
                "~/Scripts/Libs/papaparse.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                "~/Scripts/Libs/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                "~/Scripts/Libs/bootstrap.js",
                "~/Scripts/Libs/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/bootstrap.css",
                "~/Content/site.css",
                "~/Content/jquery-dataTables.css"));
        }
    }
}
