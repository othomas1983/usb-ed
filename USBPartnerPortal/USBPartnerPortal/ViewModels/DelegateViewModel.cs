﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Domain.Models.Enums;

namespace USBPartnerPortal.ViewModels
{
    public class DelegateViewModel : BaseModel
    {
        public Guid? DelegateId { get; set; }

        public string DelegateName { get; set; }

        public string DelegateSurname { get; set; }

        public string DelegateEmail { get; set; }

        public string DelegateApplicationUrl { get; set; }

        public Guid? SelectedOffering { get; set; }

        public Guid? AccountId { get; set; }

        public DelegateStatus DelegateStatus { get; set; }

        public string EnrollmentStatusDescription { get; set; }

        public EnrollmentStatus EnrollmentStatus
        {
            set
            {
                var statusDescription = value.GetDescription();
                EnrollmentStatusDescription = statusDescription ?? "No Status";
            }
        }
    }
}