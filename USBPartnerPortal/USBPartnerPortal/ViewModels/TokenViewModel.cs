﻿namespace USBPartnerPortal.ViewModels
{
    public class TokenViewModel : BaseModel
    {
        public string Token { get; set; }
    }
}