﻿using System.Linq;
using Airborne.Notifications;

namespace USBPartnerPortal.ViewModels
{
    public class BaseModel
    {
        #region Constructor

        public BaseModel()
        {
            Notifications = NotificationCollection.CreateEmpty();
            ErrorTitle = "Error";
            InformationTitle = "Info";
        }

        #endregion

        #region Properties

        public NotificationCollection Notifications { get; set; }

        public NotificationCollection Errors
        {
            get
            {
                var errorList = Notifications.Where(n => n.Severity == NotificationSeverity.Error).ToList();

                return NotificationCollection.Create(errorList);
            }
        }

        public NotificationCollection Information
        {
            get
            {
                var infoList = Notifications.Where(n => n.Severity == NotificationSeverity.Information).ToList();

                return NotificationCollection.Create(infoList);
            }
        }

        public string ErrorTitle { get; set; }

        public string InformationTitle { get; set; }

        public bool HasErrors => Errors.Any();

        public bool HasInformation => Information.Any();

        #endregion
    }
}