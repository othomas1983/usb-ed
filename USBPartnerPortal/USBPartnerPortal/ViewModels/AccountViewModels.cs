﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Domain.Models.Enums;
using USBPartnerPortal.ViewModels;

namespace USBPartnerPortal.Models
{
    public class LoginViewModel : BaseModel
    {
        [Required]
        public string Username { get; set; }
        
        [Required]
        public string Password { get; set; }
        
        [Display(Name = "Remember Me?")]
        public bool RememberMe { get; set; }

        [HiddenInput]
        public string ReturnUrl { get; set; }
    }

    public class PartnerAccountViewModel : BaseModel
    {
        public Guid AccountId { get; set; }

        public string AccountName { get; set; }

        public string EmailAddress { get; set; }

        public string Username { get; set; }

        public string AccountImageUrl { get; set; }

        public bool CanNominateDelegates { get; set; }

        public AccountType? AccountType { get; set; }
    }
}
