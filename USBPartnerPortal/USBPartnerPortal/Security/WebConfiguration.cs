﻿using System.Web.Configuration;

namespace USBPartnerPortal.Security
{
    public class WebConfiguration
    {
        public WebConfiguration()
        {
        }

        public string UsbShortCourseApplicationFormUrl
            => WebConfigurationManager.AppSettings["UsbEdShortCourseApplicationFormUrl"];
    }
}