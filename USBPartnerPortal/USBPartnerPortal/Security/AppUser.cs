﻿using System;
using System.Security.Claims;
using Domain.Models.Enums;

namespace USBPartnerPortal.Security
{
    public class AppUser : ClaimsPrincipal
    {
        public AppUser(ClaimsPrincipal principal)
            : base(principal)
        {
            WebConfiguration = new WebConfiguration();
        }
        
        public string AccountId => FindFirst(ClaimTypes.Sid).Value;

        public string Username => FindFirst(ClaimTypes.Name).Value;

        public string AccountName => FindFirst(ClaimTypes.GivenName).Value;

        public string EmailAddress => FindFirst(ClaimTypes.Email).Value;

        public string AccountImageUrl => FindFirst(ClaimTypes.Uri).Value;

        public bool CanNominateDelegates => bool.Parse(FindFirst("CanNominateDelegates").Value);

        public AccountType AccountType => (AccountType) Enum.Parse(typeof(AccountType), FindFirst("AccountType").Value);

        public WebConfiguration WebConfiguration { get; set; }
    }
}