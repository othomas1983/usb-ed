﻿using System;
using System.Linq;
using Airborne.Notifications;
using Domain;
using FakeItEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using USBPartnerPortal.Actions;
using USBPartnerPortal.ViewModels;

namespace USBPartnerPortalTests.Actions
{
    [TestClass]
    public class CreateDelegateTokenActionTest
    {
        [TestMethod]
        public void USBPartnerPortal_CreateDelegateToken_Success()
        {
            // Arrange
            var delegateVM = new DelegateViewModel
            {
                AccountId = Guid.NewGuid(),
                SelectedOffering = Guid.NewGuid(),
                DelegateEmail = "test@test.com",
                DelegateName = "Test",
                DelegateSurname = "Test"
            };

            var fakeContext = A.Fake<IPartnerPortalContext>();
            
            // Act
            var action = new CreateDelegateTokenAction<dynamic>(fakeContext)
            {
                OnSuccess = token => new {data = token},
                OnFailed = error => new {data = error}
            }.Invoke(delegateVM);

            var result = action.data;

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Token);
        }

        [TestMethod]
        public void USBPartnerPortal_CreateDelegateToken_No_Delegate_Selected()
        {
            // Arrange
            var fakeContext = A.Fake<IPartnerPortalContext>();

            // Act
            var action = new CreateDelegateTokenAction<dynamic>(fakeContext)
            {
                OnSuccess = token => new { data = token },
                OnFailed = error => new { data = error }
            }.Invoke(null);

            var result = action.data as NotificationCollection;

            // Assert
            Assert.IsTrue(result.HasErrors());
            Assert.IsNotNull(result.FirstOrDefault().Text == "No delegate selected.");
        }
    }
}
