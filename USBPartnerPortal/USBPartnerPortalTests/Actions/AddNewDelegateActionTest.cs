﻿using System;
using System.Linq;
using Airborne.Notifications;
using Domain;
using FakeItEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using USBPartnerPortal.Actions;
using USBPartnerPortal.ViewModels;
using Delegate = Domain.Models.Delegate;

namespace USBPartnerPortalTests.Actions
{
    [TestClass]
    public class AddNewDelegateActionTest
    {
        [TestMethod]
        public void USBPartnerPortal_SaveDelegate_Success()
        {
            // Arrange
            var delegateVM = new DelegateViewModel
            {
                AccountId = Guid.NewGuid(),
                DelegateName = "Test",
                DelegateEmail = "test@test.com",
                SelectedOffering = Guid.NewGuid()
            };
            var notifications = NotificationCollection.CreateEmpty();
            notifications.AddMessage(Notification.Create("Delegate created successfully.", NotificationSeverity.Information));

            var fakeContext = A.Fake<IPartnerPortalContext>();

            A.CallTo(() => fakeContext.DelegateService.SaveDelegate(new Delegate()))
                .WithAnyArguments()
                .Returns(notifications);

            // Act
            var action = new AddNewDelegateAction<dynamic>(fakeContext)
            {
                OnSuccess = message => new {data = message},
                OnFailed = error => new {data = error}
            }.Invoke(delegateVM);

            var result = action.data as NotificationCollection;

            // Assert
            Assert.IsFalse(result.HasErrors());
            Assert.IsTrue(result.FirstOrDefault().Text == "Delegate created successfully.");
        }

        [TestMethod]
        public void USBPartnerPortal_SaveDelegate_No_Offering_Selected()
        {
            // Arrange
            var notifications = NotificationCollection.CreateEmpty();

            var fakeContext = A.Fake<IPartnerPortalContext>();

            A.CallTo(() => fakeContext.DelegateService.SaveDelegate(new Delegate()))
                .WithAnyArguments()
                .Returns(notifications);

            // Act
            var action = new AddNewDelegateAction<dynamic>(fakeContext)
            {
                OnSuccess = message => new { data = message },
                OnFailed = error => new { data = error }
            }.Invoke(new DelegateViewModel());

            var result = action.data as NotificationCollection;

            // Assert
            Assert.IsTrue(result.HasErrors());
            Assert.IsTrue(result.FirstOrDefault().Text == "No short course offering selected.");
        }
    }
}
