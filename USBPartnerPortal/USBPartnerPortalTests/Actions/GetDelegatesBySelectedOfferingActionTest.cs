﻿using System;
using System.Collections.Generic;
using System.Linq;
using Airborne.Notifications;
using Domain;
using FakeItEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using USBPartnerPortal.Actions;
using USBPartnerPortal.ViewModels;
using Delegate = Domain.Models.Delegate;

namespace USBPartnerPortalTests.Actions
{
    [TestClass]
    public class GetDelegatesBySelectedOfferingActionTest
    {
        [TestMethod]
        public void USBPartnerPortal_GetDelegatesBySelectedOffering_Success()
        {
            // Arrange
            var delegates = new List<Delegate>
            {
                new Delegate {AccountId = Guid.NewGuid(), DelegateName = "test", DelegateEmail = "test@test.com"},
                new Delegate {AccountId = Guid.NewGuid(), DelegateName = "test1", DelegateEmail = "test1@test1.com"}
            };

            var fakeContext = A.Fake<IPartnerPortalContext>();

            A.CallTo(() => fakeContext.DelegateService.GetDelegatesBySelectedOffering(Guid.NewGuid()))
                .WithAnyArguments()
                .Returns(delegates);

            // Act
            var action = new GetDelegatesBySelectedOfferingAction<dynamic>(fakeContext)
            {
                OnSuccess = delegatesResult => new {data = delegatesResult},
                OnFailed = error => new {data = error}
            }.Invoke(Guid.NewGuid().ToString());

            var result = action.data as List<DelegateViewModel>;

            // Assert
            Assert.IsTrue(result.Count > 0);
        }

        [TestMethod]
        public void USBPartnerPortal_GetDelegatesBySelectedOffering_No_Offering_Selected()
        {
            // Arrange
            var fakeContext = A.Fake<IPartnerPortalContext>();

            A.CallTo(() => fakeContext.DelegateService.GetDelegatesBySelectedOffering(Guid.NewGuid()))
                .WithAnyArguments()
                .Returns(new List<Delegate>());

            // Act
            var action = new GetDelegatesBySelectedOfferingAction<dynamic>(fakeContext)
            {
                OnSuccess = delegatesResult => new { data = delegatesResult },
                OnFailed = error => new { data = error }
            }.Invoke(null);

            var result = action.data as NotificationCollection;

            // Assert
            Assert.IsTrue(result.HasErrors());
            Assert.IsTrue(result.FirstOrDefault().Text == "No short course offering selected.");
        }

        [TestMethod]
        public void USBPartnerPortal_GetDelegatesBySelectedOffering_No_Delegates_For_Selected_Offering()
        {
            // Arrange
            var fakeContext = A.Fake<IPartnerPortalContext>();

            A.CallTo(() => fakeContext.DelegateService.GetDelegatesBySelectedOffering(Guid.NewGuid()))
                .WithAnyArguments()
                .Returns(new List<Delegate>());

            // Act
            var action = new GetDelegatesBySelectedOfferingAction<dynamic>(fakeContext)
            {
                OnSuccess = delegatesResult => new { data = delegatesResult },
                OnFailed = error => new { data = error }
            }.Invoke(Guid.NewGuid().ToString());

            var result = action.data as NotificationCollection;

            // Assert
            Assert.IsTrue(result.HasErrors());
            Assert.IsTrue(result.FirstOrDefault().Text == "No delegates found for the selected offering.");
        }
    }
}
