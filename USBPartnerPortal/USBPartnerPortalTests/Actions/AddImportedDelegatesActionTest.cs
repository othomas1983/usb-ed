﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Airborne.Notifications;
using Domain;
using FakeItEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using USBPartnerPortal.Actions;
using USBPartnerPortal.ViewModels;
using Delegate = Domain.Models.Delegate;

namespace USBPartnerPortalTests.Actions
{
    [TestClass]
    public class AddImportedDelegatesActionTest
    {
        [TestMethod]
        public void USBPartnerPortal_SaveImportedDelegates_Success()
        {
            // Arrange
            var delegates = new List<DelegateViewModel>
            {
                new DelegateViewModel {AccountId = Guid.NewGuid(), DelegateName = "test", DelegateEmail = "test@test.com"}
            };
            var notifications = NotificationCollection.CreateEmpty();
            notifications.AddMessage(Notification.Create("Delegate created successfully.", NotificationSeverity.Information));

            var fakeContext = A.Fake<IPartnerPortalContext>();

            A.CallTo(
                    () => fakeContext.DelegateService.SaveImportedDelegates(new List<Delegate>(), new Guid(), new Guid()))
                .WithAnyArguments()
                .Returns(notifications);

            // Act
            var action = new AddImportedDelegatesAction<dynamic>(fakeContext)
            {
                OnSuccess = message => new {data = message},
                OnFailed = error => new {data = error}
            }.Invoke(delegates, Guid.NewGuid(), Guid.NewGuid());

            var result = action.data as NotificationCollection;

            // Assert
            Assert.IsFalse(result.HasErrors());
            Assert.IsTrue(result.FirstOrDefault().Text == "Delegate created successfully.");
        }

        [TestMethod]
        public void USBPartnerPortal_SaveImportedDelegates_No_Offering_Selected()
        {
            // Arrange
            var notifications = NotificationCollection.CreateEmpty();

            var fakeContext = A.Fake<IPartnerPortalContext>();

            A.CallTo(
                    () => fakeContext.DelegateService.SaveImportedDelegates(new List<Delegate>(), new Guid(), new Guid()))
                .WithAnyArguments()
                .Returns(notifications);

            // Act
            var action = new AddImportedDelegatesAction<dynamic>(fakeContext)
            {
                OnSuccess = message => new { data = message },
                OnFailed = error => new { data = error }
            }.Invoke(new List<DelegateViewModel>(), new Guid(), new Guid());

            var result = action.data as NotificationCollection;

            // Assert
            Assert.IsTrue(result.HasErrors());
            Assert.IsTrue(result.FirstOrDefault().Text == "No short course offering selected.");
        }
    }
}
