﻿using System;
using System.Linq;
using Airborne.Notifications;
using Domain;
using Domain.Models;
using Domain.Models.Enums;
using FakeItEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using USBPartnerPortal.Actions;

namespace USBPartnerPortalTests.Actions
{
    [TestClass]
    public class GetPartnerAccountActionTest
    {
        [TestMethod]
        public void USBPartnerPortal_GetPartnerAccount_Success()
        {
            // Arrange
            var fakeContext = A.Fake<IPartnerPortalContext>();

            A.CallTo(() => fakeContext.AccountService.GetPartnerAccountBy("test", "test", null))
                .WithAnyArguments()
                .Returns(new PartnerAccount
                {
                    AccountId = new Guid(),
                    AccountName = "Test",
                    EmailAddress = "test@test.com",
                    AccountType = AccountType.BasilRead
                });

            // Act
            var action = new GetPartnerAccountAction<dynamic>(fakeContext)
            {
                OnSuccess = partnerAccount => new {data = partnerAccount},
                OnFailed = error => new {data = error}
            }.Invoke("test", "test", null);

            var result = action.data;

            // Assert
            Assert.IsNotNull(result.AccountId);
            Assert.IsNotNull(result.AccountName);
            Assert.IsNotNull(result.EmailAddress);
            Assert.IsNotNull(result.AccountType);
        }

        [TestMethod]
        public void USBPartnerPortal_GetPartnerAccount_No_Account_Found()
        {
            // Arrange
            var fakeContext = A.Fake<IPartnerPortalContext>();

            A.CallTo(() => fakeContext.AccountService.GetPartnerAccountBy("test", "test", null))
                .WithAnyArguments()
                .Returns(null);

            // Act
            var action = new GetPartnerAccountAction<dynamic>(fakeContext)
            {
                OnSuccess = partnerAccount => new {data = partnerAccount},
                OnFailed = error => new {data = error}
            }.Invoke("test", "test", null);

            var result = action.data as NotificationCollection;

            // Assert
            Assert.IsTrue(result.HasErrors());
            Assert.IsTrue(result.FirstOrDefault().Text == "No matching partner account found.");
        }

        [TestMethod]
        public void USBPartnerPortal_GetPartnerAccount_No_Debtor_Code_Found()
        {
            // Arrange
            var fakeContext = A.Fake<IPartnerPortalContext>();

            A.CallTo(() => fakeContext.AccountService.GetPartnerAccountBy("test", "test", null))
                .WithAnyArguments()
                .Returns(new PartnerAccount());

            // Act
            var action = new GetPartnerAccountAction<dynamic>(fakeContext)
            {
                OnSuccess = partnerAccount => new {data = partnerAccount},
                OnFailed = error => new {data = error}
            }.Invoke("test", "test", null);

            var result = action.data as NotificationCollection;

            // Assert
            Assert.IsTrue(result.HasErrors());
            Assert.IsTrue(result.FirstOrDefault().Text == "No Account Number or Debtor Code found for this Account.");
        }
    }
}
