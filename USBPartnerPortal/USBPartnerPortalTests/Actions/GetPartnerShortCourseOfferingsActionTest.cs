﻿using System;
using System.Collections.Generic;
using System.Linq;
using Airborne.Notifications;
using Domain;
using Domain.Models;
using FakeItEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using USBPartnerPortal.Actions;
using USBPartnerPortal.ViewModels;

namespace USBPartnerPortalTests.Actions
{
    [TestClass]
    public class GetPartnerShortCourseOfferingsActionTest
    {
        [TestMethod]
        public void USBPartnerPortal_GetShortCourseOfferingsByAccountId_Success()
        {
            // Arrange
            var offerings = new List<ShortCourseOffering>
            {
                new ShortCourseOffering
                {
                    ShortCourseOfferingId = Guid.NewGuid(),
                    OfferingName = "Test",
                    OwnerName = "Test"
                },
                new ShortCourseOffering
                {
                    ShortCourseOfferingId = Guid.NewGuid(),
                    OfferingName = "Test1",
                    OwnerName = "Test1"
                }
            };

            var fakeContext = A.Fake<IPartnerPortalContext>();

            A.CallTo(() => fakeContext.ShortCourseOfferingService.GetShortCourseOfferingsByAccountId(Guid.NewGuid()))
                .WithAnyArguments()
                .Returns(offerings);

            // Act
            var action = new GetPartnerShortCourseOfferingsAction<dynamic>(fakeContext)
            {
                OnSuccess = offeringsList => new {data = offeringsList},
                OnFailed = error => new {data = error}
            }.Invoke(Guid.NewGuid().ToString());

            var result = action.data as List<ShortCourseOfferingViewModel>;

            // Assert
            Assert.IsTrue(result.Count > 0);
        }

        [TestMethod]
        public void USBPartnerPortal_GetShortCourseOfferingsByAccountId_No_Account_Id_Provided()
        {
            // Arrange
            var fakeContext = A.Fake<IPartnerPortalContext>();

            A.CallTo(() => fakeContext.ShortCourseOfferingService.GetShortCourseOfferingsByAccountId(Guid.NewGuid()))
                .WithAnyArguments()
                .Returns(new List<ShortCourseOffering>());

            // Act
            var action = new GetPartnerShortCourseOfferingsAction<dynamic>(fakeContext)
            {
                OnSuccess = offeringsList => new { data = offeringsList },
                OnFailed = error => new { data = error }
            }.Invoke(null);

            var result = action.data as NotificationCollection;

            // Assert
            Assert.IsTrue(result.HasErrors());
            Assert.IsTrue(result.FirstOrDefault().Text == "No account id provided.");
        }

        [TestMethod]
        public void USBPartnerPortal_GetShortCourseOfferingsByAccountId_No_Offerings_Found()
        {
            // Arrange
            var fakeContext = A.Fake<IPartnerPortalContext>();

            A.CallTo(() => fakeContext.ShortCourseOfferingService.GetShortCourseOfferingsByAccountId(Guid.NewGuid()))
                .WithAnyArguments()
                .Returns(new List<ShortCourseOffering>());

            // Act
            var action = new GetPartnerShortCourseOfferingsAction<dynamic>(fakeContext)
            {
                OnSuccess = offeringsList => new { data = offeringsList },
                OnFailed = error => new { data = error }
            }.Invoke(Guid.NewGuid().ToString());

            var result = action.data as NotificationCollection;

            // Assert
            Assert.IsTrue(result.HasErrors());
            Assert.IsTrue(result.FirstOrDefault().Text == "No offerings found for this account.");
        }
    }
}
