﻿using System;
using System.Linq;
using Airborne.Notifications;
using Domain;
using FakeItEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using USBPartnerPortal.Actions;

namespace USBPartnerPortalTests.Actions
{
    [TestClass]
    public class DeleteDelegateActionTest
    {
        [TestMethod]
        public void USBPartnerPortal_DeleteDelegate_Success()
        {
            // Arrange
            var notifications = NotificationCollection.CreateEmpty();
            notifications.AddMessage(Notification.Create("Delegate removed.", NotificationSeverity.Information));

            var fakeContext = A.Fake<IPartnerPortalContext>();

            A.CallTo(() => fakeContext.DelegateService.DeleteDelegate(Guid.NewGuid()))
                .WithAnyArguments()
                .Returns(notifications);

            // Act
            var action = new DeleteDelegateAction<dynamic>(fakeContext)
            {
                OnSuccess = message => new {data = message},
                OnFailed = error => new {data = error}
            }.Invoke(Guid.NewGuid().ToString());

            var result = action.data as NotificationCollection;

            // Assert
            Assert.IsFalse(result.HasErrors());
            Assert.IsTrue(result.FirstOrDefault().Text == "Delegate removed.");
        }

        [TestMethod]
        public void USBPartnerPortal_DeleteDelegate_No_Delegate_Selected()
        {
            // Arrange
            var notifications = NotificationCollection.CreateEmpty();

            var fakeContext = A.Fake<IPartnerPortalContext>();

            A.CallTo(() => fakeContext.DelegateService.DeleteDelegate(Guid.NewGuid()))
                .WithAnyArguments()
                .Returns(notifications);

            // Act
            var action = new DeleteDelegateAction<dynamic>(fakeContext)
            {
                OnSuccess = message => new { data = message },
                OnFailed = error => new { data = error }
            }.Invoke(null);

            var result = action.data as NotificationCollection;

            // Assert
            Assert.IsTrue(result.HasErrors());
            Assert.IsTrue(result.FirstOrDefault().Text == "No delegate selected.");
        }
    }
}
