﻿using System;
using System.Collections.Generic;
using System.Linq;
using Airborne.Notifications;
using CrmServiceAdapter.Services;
using CrmServiceAdapter.Services.Interfaces;
using Domain.Services;
using FakeItEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Delegate = Domain.Models.Delegate;

namespace CrmServiceAdapterTests.Services
{
    [TestClass]
    public class DelegateServiceProviderTest
    {
        private ICrmAdapter _fakeCrmAdapter;
        private IDelegateServiceProvider _delegateServiceProvider;

        [TestInitialize]
        public void CrmServiceAdapter_DelegateServiceProviderTest_Initialize()
        {
            _fakeCrmAdapter = A.Fake<ICrmAdapter>();
            _delegateServiceProvider = new DelegateServiceProvider(_fakeCrmAdapter);
        }

        [TestMethod]
        public void CrmServiceAdapter_SaveDelegate()
        {
            // Arrange
            var notifications = NotificationCollection.CreateEmpty();
            notifications.AddMessage(new Notification("Delegate created successfully.", NotificationSeverity.Information));

            A.CallTo(() => _fakeCrmAdapter.SaveDelegate(new Delegate())).WithAnyArguments().Returns(notifications);

            // Act
            var message = _delegateServiceProvider.SaveDelegate(new Delegate
            {
                AccountId = new Guid(),
                DelegateEmail = "test@test.com"
            });

            // Assert
            Assert.IsFalse(message.HasErrors());
            Assert.IsTrue(message.FirstOrDefault().Text == "Delegate created successfully.");
        }

        [TestMethod]
        public void CrmServiceAdapter_UpdateDelegate()
        {
            // Arrange
            var notifications = NotificationCollection.CreateEmpty();
            notifications.AddMessage(new Notification("Delegate details updated successfully.", NotificationSeverity.Information));

            var delegateDetails = new Delegate
            {
                DelegateId = Guid.NewGuid(),
                DelegateName = "Test",
                DelegateSurname = "Test",
                DelegateEmail = "test@test.com"
            };

            A.CallTo(() => _fakeCrmAdapter.UpdateDelegateDetails(delegateDetails)).WithAnyArguments().Returns(notifications);

            // Act
            var message = _delegateServiceProvider.UpdateDelegateDetails(delegateDetails);

            // Assert
            Assert.IsFalse(message.HasErrors());
            Assert.IsTrue(message.FirstOrDefault().Text == "Delegate details updated successfully.");
        }

        [TestMethod]
        public void CrmServiceAdapter_SaveDelegate_Exist_Already()
        {
            // Arrange
            var notifications = NotificationCollection.CreateEmpty();

            var delegateDetails = new Delegate
            {
                DelegateName = "Test",
                DelegateSurname = "Test",
                DelegateEmail = "test@test.com"
            };

            notifications.AddMessage(new Notification($"Details for {delegateDetails.DelegateName} {delegateDetails.DelegateSurname} already exist.", NotificationSeverity.Information));
            
            A.CallTo(() => _fakeCrmAdapter.SaveDelegate(delegateDetails)).WithAnyArguments().Returns(notifications);

            // Act
            var message = _delegateServiceProvider.SaveDelegate(delegateDetails);

            // Assert
            Assert.IsFalse(message.HasErrors());
            Assert.IsTrue(message.FirstOrDefault().Text == $"Details for {delegateDetails.DelegateName} {delegateDetails.DelegateSurname} already exist.");
        }

        [TestMethod]
        public void CrmServiceAdapter_SaveImportedDelegates()
        {
            // Arrange
            var notifications = NotificationCollection.CreateEmpty();
            notifications.AddMessage(new Notification("Delegate created successfully.", NotificationSeverity.Information));

            A.CallTo(() => _fakeCrmAdapter.SaveImportedDelegates(new List<Delegate>(), Guid.NewGuid(), Guid.NewGuid()))
                .WithAnyArguments()
                .Returns(notifications);


            // Act
            var message = _delegateServiceProvider.SaveImportedDelegates(new List<Delegate>(), new Guid(), new Guid());

            // Assert
            Assert.IsFalse(message.HasErrors());
            Assert.IsTrue(message.FirstOrDefault().Text == "Delegate created successfully.");
        }

        [TestMethod]
        public void CrmServiceAdapter_SaveImportedDelegates_Exist_Already()
        {
            // Arrange
            var delegateDetails = new List<Delegate>
            {
                new Delegate { DelegateName = "Test", DelegateSurname = "Test", DelegateEmail = "test@test.com" }
            };

            var notifications = NotificationCollection.CreateEmpty();
            notifications.AddMessage(new Notification($"Details for {delegateDetails[0].DelegateName} {delegateDetails[0].DelegateSurname} already exist.", NotificationSeverity.Information));

            A.CallTo(() => _fakeCrmAdapter.SaveImportedDelegates(delegateDetails, Guid.NewGuid(), Guid.NewGuid()))
                .WithAnyArguments()
                .Returns(notifications);
            
            // Act
            var message = _delegateServiceProvider.SaveImportedDelegates(delegateDetails, new Guid(), new Guid());

            // Assert
            Assert.IsFalse(message.HasErrors());
            Assert.IsTrue(message.FirstOrDefault().Text == $"Details for {delegateDetails[0].DelegateName} {delegateDetails[0].DelegateSurname} already exist.");
        }

        [TestMethod]
        public void CrmServiceAdapter_GetDelegatesBySelectedOffering()
        {
            // Arrange
            var delegateDetails = new List<Delegate>
            {
                new Delegate {DelegateName = "Test", DelegateSurname = "Test", DelegateEmail = "test@test.com"},
                new Delegate {DelegateName = "Test1", DelegateSurname = "Test1", DelegateEmail = "test1@test1.com"},
                new Delegate {DelegateName = "Test2", DelegateSurname = "Test2", DelegateEmail = "test2@test2.com"}
            };

            A.CallTo(() => _fakeCrmAdapter.GetDelegatesBySelectedOffering(Guid.NewGuid()))
                .WithAnyArguments()
                .Returns(delegateDetails);

            // Act
            var result = _delegateServiceProvider.GetDelegatesBySelectedOffering(new Guid());

            // Arrange
            Assert.IsTrue(result.Count > 0);
            Assert.IsTrue(result[0].DelegateName == "Test" && result[0].DelegateSurname == "Test" &&
                          result[0].DelegateEmail == "test@test.com");
            Assert.IsTrue(result[1].DelegateName == "Test1" && result[1].DelegateSurname == "Test1" &&
                          result[1].DelegateEmail == "test1@test1.com");
            Assert.IsTrue(result[2].DelegateName == "Test2" && result[2].DelegateSurname == "Test2" &&
                          result[2].DelegateEmail == "test2@test2.com");
        }

        [TestMethod]
        public void CrmServiceAdapter_DeleteDelegate()
        {
            // Arrange
            var notifications = NotificationCollection.CreateEmpty();
            notifications.AddMessage(Notification.Create("Delegate removed.", NotificationSeverity.Information));

            A.CallTo(() => _fakeCrmAdapter.DeleteDelegate(Guid.NewGuid())).WithAnyArguments().Returns(notifications);

            // Act
            var message = _delegateServiceProvider.DeleteDelegate(new Guid());

            // Assert
            Assert.IsFalse(message.HasErrors());
            Assert.IsTrue(message.FirstOrDefault().Text == "Delegate removed.");
        }

        [TestMethod]
        public void CrmServiceAdapter_UpdateDelegateEmailSentStatus()
        {
            // Arrange
            var notifications = NotificationCollection.CreateEmpty();
            notifications.AddMessage(Notification.Create("Updated delegate status.", NotificationSeverity.Information));

            A.CallTo(() => _fakeCrmAdapter.UpdateDelegateEmailSentStatus(new Delegate()))
                .WithAnyArguments()
                .Returns(notifications);

            // Act
            var message = _delegateServiceProvider.UpdateDelegateEmailSentStatus(new Delegate
            {
                AccountId = new Guid(),
                DelegateEmail = "test@test.com"
            });

            // Assert
            Assert.IsFalse(message.HasErrors());
            Assert.IsTrue(message.FirstOrDefault().Text == "Updated delegate status.");
        }
    }
}
