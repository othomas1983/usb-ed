﻿using System;
using System.Collections.Generic;
using CrmServiceAdapter.Services;
using CrmServiceAdapter.Services.Interfaces;
using Domain.Models;
using FakeItEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CrmServiceAdapterTests.Services
{
    [TestClass]
    public class ContactServiceProviderTest
    {
        [TestMethod]
        public void CrmServiceAdapter_GetEnrolledContacts()
        {
            // Arrange
            var accountContacts = new List<PartnerContact>
            {
                new PartnerContact {Name = "Test", Surname = "Test", Email = "test@test.com"},
                new PartnerContact {Name = "Test1", Surname = "Test1", Email = "test1@test1.com"},
                new PartnerContact {Name = "Test2", Surname = "Test2", Email = "test2@test2.com"}
            };

            var fakeCrmAdapter = A.Fake<ICrmAdapter>();
            var contactServiceProvider = new ContactServiceProvider(fakeCrmAdapter);

            A.CallTo(() => fakeCrmAdapter.GetEnrolledContactsBy(Guid.NewGuid()))
                .WithAnyArguments()
                .Returns(accountContacts);

            // Act
            var contacts = contactServiceProvider.GetEnrolledContactsBy(Guid.NewGuid());

            // Assert
            Assert.IsTrue(contacts.Count > 0);
            Assert.IsTrue(contacts[0].Name == "Test" && contacts[0].Surname == "Test" &&
                          contacts[0].Email == "test@test.com");
            Assert.IsTrue(contacts[1].Name == "Test1" && contacts[1].Surname == "Test1" &&
                          contacts[1].Email == "test1@test1.com");
            Assert.IsTrue(contacts[2].Name == "Test2" && contacts[2].Surname == "Test2" &&
                          contacts[2].Email == "test2@test2.com");
        }
    }
}