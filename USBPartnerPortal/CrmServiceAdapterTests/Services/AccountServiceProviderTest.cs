﻿using System;
using CrmServiceAdapter.Services;
using CrmServiceAdapter.Services.Interfaces;
using Domain.Models;
using Domain.Models.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FakeItEasy;

namespace CrmServiceAdapterTests.Services
{
    [TestClass]
    public class AccountServiceProviderTest
    {
        [TestMethod]
        public void CrmServiceAdapter_GetPartnerAccount()
        {
            // Arrange
            var partnerAccount = new PartnerAccount
            {
                AccountId = Guid.NewGuid(),
                AccountName = "Test",
                Username = "Test",
                EmailAddress = "test@test.com",
                AccountType = AccountType.BasilRead
            };

            var fakeCrmAdapter = A.Fake<ICrmAdapter>();
            var accountServiceProvider = new AccountServiceProvider(fakeCrmAdapter);

            A.CallTo(() => fakeCrmAdapter.GetPartnerAccountBy("Test", "test", null))
                .WithAnyArguments()
                .Returns(partnerAccount);

            // Act
            var account = accountServiceProvider.GetPartnerAccountBy("Test", "test", null);

            // Assert
            Assert.IsTrue(account.IsNotNull());
            Assert.IsTrue(account.AccountType == AccountType.BasilRead);
            Assert.IsTrue(account.AccountName.IsNotNull());
            Assert.IsTrue(account.Username.IsNotNull());
            Assert.IsTrue(account.EmailAddress.IsNotNull());
        }
    }
}
