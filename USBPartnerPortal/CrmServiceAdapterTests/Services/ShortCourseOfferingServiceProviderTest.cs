﻿using System;
using CrmServiceAdapter.Services;
using CrmServiceAdapter.Services.Interfaces;
using FakeItEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CrmServiceAdapterTests.Services
{
    [TestClass]
    public class ShortCourseOfferingServiceProviderTest
    {
        [TestMethod]
        public void CrmServiceAdapter_GetShortCourseOfferingsByAccountIdTest()
        {
            // Arrange
            var fakeCrmAdapter = A.Fake<ICrmAdapter>();
            var shortCourseOfferingServiceAdatper = new ShortCourseOfferingServiceProvider(fakeCrmAdapter);

            // Act
            var shortCourseOfferings = shortCourseOfferingServiceAdatper.GetShortCourseOfferingsByAccountId(new Guid());

            // Assert
            Assert.IsTrue(shortCourseOfferings.IsNotNull());
        }
    }
}
