﻿using System;
using System.IO;
using System.Net;
using System.Net.Mail;
using SettingsMan = CrmServiceAdapter.Properties.Settings1;

namespace CrmServiceAdapter.Helpers
{
    public class MailHelper
    {
        private const int Timeout = 180000;
        private readonly string _host;
        private readonly int _port;
        private readonly string _sender;
        
        public string Recipient { get; set; }
        public string RecipientCC { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string AttachmentFile { get; set; }

        public MailHelper()
        {
            //Sender - From address
            _sender = SettingsMan.Default.Sender;
            //MailServer - Represents the SMTP Server
            _host = SettingsMan.Default.MailServer;
            //Port- Represents the port number
            _port = int.Parse(SettingsMan.Default.Port);
        }

        public void Send()
        {
            try
            {
                // We do not catch the error here... let it pass direct to the caller
                Attachment att = null;
                var message = new MailMessage(_sender, Recipient, Subject, Body) { IsBodyHtml = true };
                if (RecipientCC != null)
                {
                    message.Bcc.Add(RecipientCC);
                }
                
                var smtp = new SmtpClient(_host)
                {
                    Port = _port,
                    EnableSsl = false,
                    UseDefaultCredentials = false
                };

                if (!string.IsNullOrEmpty(AttachmentFile))
                {
                    if (File.Exists(AttachmentFile))
                    {
                        att = new Attachment(AttachmentFile);
                        message.Attachments.Add(att);
                    }
                }

                smtp.Send(message);

                att?.Dispose();
                message.Dispose();
                smtp.Dispose();
            }

            catch (Exception ex)
            {

            }
        }
    }
}
