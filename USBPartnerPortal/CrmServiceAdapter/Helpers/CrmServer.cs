﻿using Microsoft.Xrm.Client;
using SettingsMan = CrmServiceAdapter.Properties.Settings1;

namespace CrmServiceAdapter.Helpers
{
    public static class CrmServer
    {
        private static CrmConnection crmConnection = CrmConnection.Parse(SettingsMan.Default.CrmConnectionString);

        private static string crmDomain = SettingsMan.Default.CrmDomain;

        private static string crmUsername = SettingsMan.Default.CrmUsername;

        private static string crmPassword = SettingsMan.Default.CrmPassword;

        public static CrmConnection CrmConnection => crmConnection;

        public static string CrmDomain => crmDomain;

        public static string CrmUsername => crmUsername;

        public static string CrmPassword => crmPassword;
    }
}
