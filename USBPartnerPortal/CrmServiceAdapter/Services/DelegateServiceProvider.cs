﻿using System;
using System.Collections.Generic;
using Airborne.Notifications;
using CrmServiceAdapter.Services.Interfaces;
using Domain.Services;
using Delegate = Domain.Models.Delegate;

namespace CrmServiceAdapter.Services
{
    public class DelegateServiceProvider : IDelegateServiceProvider
    {
        private readonly ICrmAdapter _crmAdapter;

        public DelegateServiceProvider(ICrmAdapter crmAdapter)
        {
            _crmAdapter = crmAdapter;
        }

        /// <summary>
        /// Insert/Update delegate details
        /// </summary>
        /// <param name="delegateDetails"></param>
        /// <returns></returns>
        public NotificationCollection SaveDelegate(Delegate delegateDetails)
        {
            return _crmAdapter.SaveDelegate(delegateDetails);
        }

        /// <summary>
        /// Save imported delegate details by offering Id
        /// </summary>
        /// <param name="delegateImportedDetails"></param>
        /// <param name="selectedOffering"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public NotificationCollection SaveImportedDelegates(List<Delegate> delegateImportedDetails, Guid selectedOffering, Guid accountId)
        {
            return _crmAdapter.SaveImportedDelegates(delegateImportedDetails, selectedOffering, accountId);
        }

        /// <summary>
        /// Update delegate details
        /// </summary>
        /// <param name="delegateDetails"></param>
        /// <returns></returns>
        public NotificationCollection UpdateDelegateDetails(Delegate delegateDetails)
        {
            return _crmAdapter.UpdateDelegateDetails(delegateDetails);
        }

        /// <summary>
        /// Get delegate by selected offering Id
        /// </summary>
        /// <param name="offeringId"></param>
        /// <returns></returns>
        public List<Domain.Models.Delegate> GetDelegatesBySelectedOffering(Guid offeringId)
        {
            return _crmAdapter.GetDelegatesBySelectedOffering(offeringId);
        }

        /// <summary>
        /// Delete delegate by id
        /// </summary>
        /// <param name="delegateId"></param>
        /// <returns></returns>
        public NotificationCollection DeleteDelegate(Guid delegateId)
        {
            return _crmAdapter.DeleteDelegate(delegateId);
        }

        /// <summary>
        /// Update delegate status
        /// </summary>
        /// <param name="delegateDetails"></param>
        /// <returns></returns>
        public NotificationCollection UpdateDelegateEmailSentStatus(Delegate delegateDetails)
        {
            return _crmAdapter.UpdateDelegateEmailSentStatus(delegateDetails);
        }
    }
}
