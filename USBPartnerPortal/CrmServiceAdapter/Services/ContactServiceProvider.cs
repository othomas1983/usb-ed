﻿using System;
using System.Collections.Generic;
using CrmServiceAdapter.Services.Interfaces;
using Domain.Models;
using Domain.Services;

namespace CrmServiceAdapter.Services
{
    public class ContactServiceProvider : IContactServiceProvider
    {
        private readonly ICrmAdapter _crmAdapter;

        public ContactServiceProvider(ICrmAdapter crmAdapter)
        {
            _crmAdapter = crmAdapter;
        }

        /// <summary>
        /// Get enrollments by offering id
        /// </summary>
        /// <param name="shortCourseOfferingId"></param>
        /// <returns></returns>
        public List<PartnerContact> GetEnrolledContactsBy(Guid shortCourseOfferingId)
        {
            return _crmAdapter.GetEnrolledContactsBy(shortCourseOfferingId);
        }
    }
}
