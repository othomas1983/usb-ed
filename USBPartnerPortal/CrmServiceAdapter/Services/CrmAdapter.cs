﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Airborne.Automapper;
using Airborne.Notifications;
using CrmServiceAdapter.Helpers;
using CrmServiceAdapter.Services.Interfaces;
using Domain.Models;
using Domain.Models.Enums;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk;
using NLog;
using StellenboschUniversity.UsbEd.Integration.Crm;
using Delegate = Domain.Models.Delegate;
using AppSettings = CrmServiceAdapter.Properties.Settings1;

namespace CrmServiceAdapter.Services
{
    public class CrmAdapter : ICrmAdapter
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private static readonly string _MasterStart = AppSettings.Default.MasterStart;
        private static readonly string _BasilRead = AppSettings.Default.BasilRead;
        private static readonly string _GetSmarter = AppSettings.Default.GetSmarter;

        /// <summary>
        /// Get short course offering details by Account Id
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public List<ShortCourseOffering> GetShortCourseOfferingsByAccountId(Guid accountId)
        {
            var shortCourseOfferings = new List<ShortCourseOffering>();

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                try
                {
                    var result =
                        crmServiceContext.stb_shortcourseofferingSet.Where(
                            x =>
                                x.stb_AccountLookup.Id == accountId &&
                                x.statuscode == (int) ShortCourseOfferingStatus.OpenforApplication).ToList();

                    foreach (var item in result)
                    {
                        var shortCourseMapper = new GenericMapper<stb_shortcourseoffering, ShortCourseOffering>();
                        var shortCourseModel = shortCourseMapper.MapObjects(item);
                        shortCourseModel.ShortCourseOfferingId = item.stb_shortcourseofferingId.GetValueOrDefault();
                        shortCourseModel.OfferingName = item.stb_OfferingNameEnglish ?? string.Empty;
                        shortCourseModel.StartDate = item.stb_StartDate ?? DateTime.MinValue;
                        shortCourseModel.EndDate = item.stb_EndDate ?? DateTime.MinValue;

                        var vouchers = item.stb_Voucher_ShortCourseOfferingLookup.ToList();
                        
                        if (vouchers.Any())
                            shortCourseModel.NumberOfVouchersRedeemable =
                                vouchers.Count(x => x.statuscode == (int) VoucherStatus.Redeemable);

                        shortCourseModel.NumberOfVouchersRedeemed = vouchers.Count(x => x.statuscode == (int) VoucherStatus.Redeemed);

                        //if (item.stb_VouchersRedeemed.IsNotNullOrEmpty())
                        //{
                        //    shortCourseModel.NumberOfVouchersRedeemed = int.Parse(item.stb_VouchersRedeemed);
                        //}
                        //else
                        //{
                        //    shortCourseModel.NumberOfVouchersRedeemed =
                        //        item.stb_enrollment_ShortCourseOfferingLookup.Count(
                        //            x => x.statuscode == (int) EnrollmentStatus.Approved);
                        //}
                        
                        shortCourseModel.NumberOfVouchersPurchased = item.stb_VoucherTotal.GetValueOrDefault();

                        var shortCourseOfferingOwner =
                            crmServiceContext.SystemUserSet.FirstOrDefault(x => x.Id == item.OwnerId.Id);

                        if (shortCourseOfferingOwner.IsNotNull())
                        {
                            shortCourseModel.OwnerName = shortCourseOfferingOwner.FullName;
                            shortCourseModel.OwnerEmailAddress = shortCourseOfferingOwner.InternalEMailAddress;
                        }

                        shortCourseOfferings.Add(shortCourseModel);
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("Unable to get Short Course Offering details from CRM: " + ex.Message);
                    throw;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }
            }

            return shortCourseOfferings;
        }

        /// <summary>
        /// Get enrollments by offering id
        /// </summary>
        /// <param name="shortCourseOfferingId"></param>
        /// <returns></returns>
        public List<PartnerContact> GetEnrolledContactsBy(Guid shortCourseOfferingId)
        {
            var enrollments = new List<PartnerContact>();

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                try
                {
                    var result = (from a in crmServiceContext.stb_enrollmentSet
                        join b in crmServiceContext.ContactSet
                        on a.stb_StudentContact_ContactLookup.Id equals b.ContactId
                        where
                        a.stb_ShortCourseOfferingLookup.Id == shortCourseOfferingId &&
                        a.statuscode == (int) EnrollmentStatus.Applied
                        select b).ToList();

                    if (result.Any())
                    {
                        foreach (var item in result)
                        {
                            var enrollmentMapper = new GenericMapper<Contact, PartnerContact>();
                            var enrollmentModel = enrollmentMapper.MapObjects(item);
                            enrollmentModel.Name = item.FirstName ?? string.Empty;
                            enrollmentModel.Email = item.EMailAddress1 ?? string.Empty;
                            enrollmentModel.Surname = item.LastName ?? string.Empty;

                            enrollments.Add(enrollmentModel);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("Unable to get Contact details from CRM: " + ex.Message);
                    throw;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }
            }

            return enrollments;
        }

        /// <summary>
        /// Get enrollment status by name, surname and offering id
        /// </summary>
        /// <param name="firstname"></param>
        /// <param name="surname"></param>
        /// <param name="shortCourseOfferingId"></param>
        /// <param name="emailAddress"></param>
        /// <returns></returns>
        public EnrollmentStatus GetEnrollmentStatus(string firstname, string surname, string emailAddress,
            Guid shortCourseOfferingId)
        {
            var status = EnrollmentStatus.None;

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                try
                {
                    var result = (from a in crmServiceContext.stb_enrollmentSet
                        join b in crmServiceContext.ContactSet
                        on a.stb_StudentContact_ContactLookup.Id equals b.ContactId
                        where
                        a.stb_ShortCourseOfferingLookup.Id == shortCourseOfferingId
                        && b.FirstName == firstname
                        && b.LastName == surname
                        && b.EMailAddress1 == emailAddress
                        select a.statuscode).FirstOrDefault();

                    if (result.IsNotNull())
                        status = (EnrollmentStatus) result.GetValueOrDefault();

                }
                catch (Exception ex)
                {
                    Logger.Error("Unable to get Enrollment status from CRM: " + ex.Message);
                    throw;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }
            }

            return status;
        }

        /// <summary>
        /// Get partner account
        /// </summary>
        /// <param name="accountName"></param>
        /// <param name="password"></param>
        /// <param name="emailAddress"></param>
        /// <returns></returns>
        public PartnerAccount GetPartnerAccountBy(string accountName = "", string password = "",
            string emailAddress = "")
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                Account result = null;

                try
                {
                    if (accountName.IsNotNullOrEmpty() && password.IsNotNullOrEmpty())
                    {
                        result =
                            crmServiceContext.AccountSet.FirstOrDefault(
                                x => x.stb_Username == accountName && x.stb_Password == password);
                    }
                    else if (emailAddress.IsNotNullOrEmpty())
                    {
                        result =
                            crmServiceContext.AccountSet.FirstOrDefault(x => x.EMailAddress1 == emailAddress);
                    }

                    if (result.IsNotNull())
                    {
                        var accountMapper = new GenericMapper<Account, PartnerAccount>();
                        var partnerAccountModel = accountMapper.MapObjects(result);
                        partnerAccountModel.Username = result.stb_Username ?? string.Empty;
                        partnerAccountModel.AccountId = result.AccountId.GetValueOrDefault();
                        partnerAccountModel.AccountName = result.Name ?? string.Empty;
                        partnerAccountModel.EmailAddress = result.EMailAddress1 ?? string.Empty;
                        partnerAccountModel.CanNominateDelegates = result.stb_NominatedDelegatesTwoOptions ?? false;

                        if (result.AccountNumber.IsNotNullOrEmpty())
                        {
                            if (result.AccountNumber == _MasterStart)
                                partnerAccountModel.AccountType = AccountType.MasterStart;

                            else if (result.AccountNumber == _BasilRead)
                                partnerAccountModel.AccountType = AccountType.BasilRead;

                            else if (result.AccountNumber == _GetSmarter)
                                partnerAccountModel.AccountType = AccountType.GetSmarter;
                        }

                        var client = new WebClient
                        {
                            Credentials =
                                new NetworkCredential(CrmServer.CrmUsername, CrmServer.CrmPassword, CrmServer.CrmDomain)
                        };

                        if (!result.EntityImage_URL.IsNotNullOrEmpty()) return partnerAccountModel;
                        byte[] imgBytes =
                            client.DownloadData(
                                $"{CrmServer.CrmConnection.ServiceUri.AbsoluteUri}{result.EntityImage_URL}");

                        partnerAccountModel.AccountImageUrl =
                            $"data:image/jpeg;base64,{Convert.ToBase64String(imgBytes)}";

                        return partnerAccountModel;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("Unable to get Account details from CRM: " + ex.Message);
                    throw;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }
            }

            return null;
        }

        /// <summary>
        /// Insert/Update delegate details
        /// </summary>
        /// <param name="delegateDetails"></param>
        /// <returns></returns>
        public NotificationCollection SaveDelegate(Delegate delegateDetails)
        {
            var notifications = NotificationCollection.CreateEmpty();

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                var delegateObject = new stb_delegate();

                try
                {
                    if (delegateDetails.IsNotNull())
                    {
                        var offeringOwner = GetCourseOfferingOwner(delegateDetails.SelectedOffering.GetValueOrDefault());

                        stb_delegate existingDelegate = null;

                        if (delegateDetails.DelegateId.HasValue && delegateDetails.DelegateId != Guid.Empty)
                        {
                            existingDelegate =
                                crmServiceContext.stb_delegateSet.FirstOrDefault(
                                    x => x.stb_delegateId == delegateDetails.DelegateId);
                        }
                        else
                        {
                            existingDelegate =
                                crmServiceContext.stb_delegateSet.FirstOrDefault(
                                    x =>
                                        x.stb_ShortCourseOfferingLookup.Id ==
                                        delegateDetails.SelectedOffering.GetValueOrDefault() &&
                                        x.stb_FirstName == delegateDetails.DelegateName &&
                                        x.stb_LastName == delegateDetails.DelegateSurname &&
                                        x.stb_email == delegateDetails.DelegateEmail);

                            if (existingDelegate.IsNotNull())
                            {
                                notifications.AddError(
                                    $"Details for {delegateDetails.DelegateName} {delegateDetails.DelegateSurname} already exist.");

                                return notifications;
                            }
                        }

                        if (existingDelegate.IsNotNull())
                            {
                                existingDelegate.stb_FirstName = delegateDetails.DelegateName;
                                existingDelegate.stb_LastName = delegateDetails.DelegateSurname;
                                existingDelegate.stb_email = delegateDetails.DelegateEmail;
                                //existingDelegate.stb_ShortCourseOfferingLookup =
                                //    new CrmEntityReference(stb_shortcourseoffering.EntityLogicalName,
                                //        delegateDetails.SelectedOffering.GetValueOrDefault());

                                crmServiceContext.UpdateObject(existingDelegate);
                                crmServiceContext.SaveChanges();

                                SetOwnershipOfDelegate(delegateDetails.DelegateId.GetValueOrDefault(), offeringOwner);

                                notifications.AddMessage(Notification.Create("Delegate details updated successfully.",
                                    NotificationSeverity.Information));
                            }
                        else
                        {
                            var programmeOffering =
                                crmServiceContext.stb_shortcourseofferingSet.FirstOrDefault(
                                    x => x.stb_shortcourseofferingId == delegateDetails.SelectedOffering);

                            if (programmeOffering.IsNotNull())
                            {
                                int totalDelegates =
                                    crmServiceContext.stb_delegateSet.Where(x => x.stb_ShortCourseOfferingLookup.Id ==
                                                                                 programmeOffering
                                                                                     .stb_shortcourseofferingId)
                                        .ToList()
                                        .Count;

                                if (programmeOffering.stb_VoucherTotal == totalDelegates)
                                {
                                    notifications.AddError(
                                        "The maximum allowed Delegates have been reached for the total amount of vouchers bought.");
                                    return notifications;
                                }
                            }

                            delegateObject.stb_FirstName = delegateDetails.DelegateName;
                            delegateObject.stb_LastName = delegateDetails.DelegateSurname;
                            delegateObject.stb_email = delegateDetails.DelegateEmail;
                            delegateObject.stb_ShortCourseOfferingLookup =
                                new CrmEntityReference(stb_shortcourseoffering.EntityLogicalName,
                                    delegateDetails.SelectedOffering.GetValueOrDefault());
                            delegateObject.stb_ApplicationFormURL = delegateDetails.DelegateApplicationUrl;
                            delegateObject.stb_AccountLookup = new CrmEntityReference(Account.EntityLogicalName,
                                delegateDetails.AccountId.GetValueOrDefault());

                            var newId = crmServiceContext.Create(delegateObject);

                            SetOwnershipOfDelegate(newId, offeringOwner);

                            notifications.AddMessage(Notification.Create("Delegate created successfully.",
                                NotificationSeverity.Information));
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("Unable to save new Delegate to CRM: " + ex.Message);
                    throw;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }
            }

            return notifications;
        }

        /// <summary>
        /// Save imported delegate details by offering Id
        /// </summary>
        /// <param name="delegateImportedDetails"></param>
        /// <param name="selectedOffering"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public NotificationCollection SaveImportedDelegates(List<Delegate> delegateImportedDetails,
            Guid selectedOffering, Guid accountId)
        {
            var notifications = NotificationCollection.CreateEmpty();

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                try
                {
                    foreach (var item in delegateImportedDetails)
                    {
                        item.SelectedOffering = selectedOffering;
                        item.AccountId = accountId;

                        notifications = SaveDelegate(item);

                        if (notifications.HasErrors()) break;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("Unable to save imported Delegate to CRM: " + ex.Message);
                    throw;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }
            }

            return notifications;
        }

        /// <summary>
        /// Update delegate details
        /// </summary>
        /// <param name="delegateDetails"></param>
        /// <returns></returns>
        public NotificationCollection UpdateDelegateDetails(Delegate delegateDetails)
        {
            var notifications = NotificationCollection.CreateEmpty();

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                try
                {
                    if (delegateDetails.IsNotNull())
                    {
                        var existingDelegateObject =
                            crmServiceContext.stb_delegateSet.FirstOrDefault(
                                x => x.stb_delegateId == delegateDetails.DelegateId);

                        if (existingDelegateObject.IsNotNull())
                        {
                            existingDelegateObject.stb_FirstName = delegateDetails.DelegateName;
                            existingDelegateObject.stb_LastName = delegateDetails.DelegateSurname;
                            existingDelegateObject.stb_email = delegateDetails.DelegateEmail;
                            existingDelegateObject.stb_ShortCourseOfferingLookup =
                                new CrmEntityReference(stb_shortcourseoffering.EntityLogicalName,
                                    delegateDetails.SelectedOffering.GetValueOrDefault());
                            
                            crmServiceContext.UpdateObject(existingDelegateObject);
                            crmServiceContext.SaveChanges();

                            var offeringOwner = GetCourseOfferingOwner(delegateDetails.SelectedOffering.GetValueOrDefault());
                            SetOwnershipOfDelegate(delegateDetails.DelegateId.GetValueOrDefault(), offeringOwner);

                            notifications.AddMessage(Notification.Create("Delegate details updated successfully.",
                                NotificationSeverity.Information));
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("Unable to update Delegate details to CRM: " + ex.Message);
                    throw;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }
            }

            return notifications;
        }

        /// <summary>
        /// Get delegates by selected offering Id
        /// </summary>
        /// <param name="offeringId"></param>
        /// <returns></returns>
        public List<Delegate> GetDelegatesBySelectedOffering(Guid offeringId)
        {
            var delegates = new List<Delegate>();

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                try
                {
                    var result =
                        crmServiceContext.stb_delegateSet.Where(x => x.stb_ShortCourseOfferingLookup.Id == offeringId).ToList();

                    if (!result.Any()) return delegates;
                    foreach (var item in result)
                    {
                        var delegateMapper = new GenericMapper<stb_delegate, Delegate>();
                        var delegateModel = delegateMapper.MapObjects(item);
                        delegateModel.DelegateId = item.stb_delegateId.GetValueOrDefault();
                        delegateModel.DelegateName = item.stb_FirstName ?? string.Empty;
                        delegateModel.DelegateSurname = item.stb_LastName ?? string.Empty;
                        delegateModel.DelegateEmail = item.stb_email ?? string.Empty;
                        delegateModel.DelegateApplicationUrl = item.stb_ApplicationFormURL ?? string.Empty;
                        delegateModel.SelectedOffering = item.stb_ShortCourseOfferingLookup.Id;
                        delegateModel.AccountId = item.stb_AccountLookup.Id;
                        delegateModel.DelegateName = item.stb_FirstName ?? string.Empty;
                        delegateModel.DelegateStatus = (DelegateStatus) item.statuscode.GetValueOrDefault();

                        delegateModel.EnrollmentStatus = GetEnrollmentStatus(delegateModel.DelegateName,
                            delegateModel.DelegateSurname, delegateModel.DelegateEmail, offeringId);

                        delegates.Add(delegateModel);
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("Unable to get Delegates from CRM: " + ex.Message);
                    throw;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }
            }

            return delegates;
        }

        /// <summary>
        /// Delete delegate by id
        /// </summary>
        /// <param name="delegateId"></param>
        /// <returns></returns>
        public NotificationCollection DeleteDelegate(Guid delegateId)
        {
            var notifications = NotificationCollection.CreateEmpty();

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                try
                {
                    var delegateRecord =
                        crmServiceContext.stb_delegateSet.FirstOrDefault(x => x.stb_delegateId == delegateId);

                    if (delegateRecord.IsNotNull())
                    {
                        crmServiceContext.Delete(stb_delegate.EntityLogicalName,
                            delegateRecord.stb_delegateId.GetValueOrDefault());
                        
                        notifications.AddMessage(Notification.Create("Delegate removed.",
                            NotificationSeverity.Information));
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("Unable to remove Delegate from CRM: " + ex.Message);
                    throw;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }
            }

            return notifications;
        }

        /// <summary>
        /// Update delegate status
        /// </summary>
        /// <param name="delegateDetails"></param>
        /// <returns></returns>
        public NotificationCollection UpdateDelegateEmailSentStatus(Delegate delegateDetails)
        {
            var notifications = NotificationCollection.CreateEmpty();

            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                try
                {
                    if (delegateDetails.IsNotNull())
                    {
                        var existingDelegateObject =
                            crmServiceContext.stb_delegateSet.FirstOrDefault(
                                x => x.stb_delegateId == delegateDetails.DelegateId);

                        if (existingDelegateObject.IsNotNull())
                        {
                            existingDelegateObject.statuscode = (int) DelegateStatus.URLSent;

                            crmServiceContext.UpdateObject(existingDelegateObject);
                            crmServiceContext.SaveChanges();

                            notifications.AddMessage(Notification.Create("Updated delegate status.",
                                NotificationSeverity.Information));
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("Unable to update Delegate details to CRM: " + ex.Message);
                    throw;
                }
                finally
                {
                    crmServiceContext.Dispose();
                }
            }

            return notifications;
        }

        /// <summary>
        /// Get short course offering owner Id
        /// </summary>
        /// <param name="contactDetailsOffering"></param>
        /// <returns></returns>
        public static Guid GetCourseOfferingOwner(Guid contactDetailsOffering)
        {
            using (var crmServiceContext = new CrmServiceContext(CrmServer.CrmConnection))
            {
                stb_shortcourseoffering crmOffering;

                try
                {
                    crmOffering = (from x in crmServiceContext.stb_shortcourseofferingSet
                                   where x.Id == contactDetailsOffering
                                   select x).FirstOrDefault();
                }
                finally
                {
                    crmServiceContext.Dispose();
                }

                if (crmOffering == null)
                {
                    throw new Exception("Unable to get short course offering from CRM");
                }

                return crmOffering.OwnerId.Id;
            }
        }

        /// <summary>
        /// Set ownership of Delegate
        /// </summary>
        /// <param name="delegateId"></param>
        /// /// <param name="offeringOwnerId"></param>
        private static void SetOwnershipOfDelegate(Guid delegateId, Guid offeringOwnerId)
        {
            using (var organizationService = new OrganizationService(CrmServer.CrmConnection))
            {
                var assignRequest = new AssignRequest
                {
                    Assignee = new EntityReference(SystemUser.EntityLogicalName, offeringOwnerId),
                    Target = new EntityReference(stb_delegate.EntityLogicalName, delegateId)
                };

                try
                {
                    organizationService.Execute(assignRequest);
                }
                finally
                {
                    organizationService.Dispose();
                }
            }
        }
    }
}
