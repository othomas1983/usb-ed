﻿using System;
using System.Collections.Generic;
using CrmServiceAdapter.Services.Interfaces;
using Domain.Models;
using Domain.Services;

namespace CrmServiceAdapter.Services
{
    public class ShortCourseOfferingServiceProvider : IShortCourseOfferingServiceProvider
    {
        private readonly ICrmAdapter _crmAdapter;

        public ShortCourseOfferingServiceProvider(ICrmAdapter crmAdapter)
        {
            _crmAdapter = crmAdapter;
        }

        /// <summary>
        /// Get short course offering details by Account Id
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public List<ShortCourseOffering> GetShortCourseOfferingsByAccountId(Guid accountId)
        {
            return _crmAdapter.GetShortCourseOfferingsByAccountId(accountId);
        }
    }
}
