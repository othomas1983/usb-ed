﻿using CrmServiceAdapter.Services.Interfaces;
using Domain.Models;
using Domain.Services;

namespace CrmServiceAdapter.Services
{
    public class AccountServiceProvider : IAccountServiceProvider
    {
        private readonly ICrmAdapter _crmAdapter;

        public AccountServiceProvider(ICrmAdapter crmAdapter)
        {
            _crmAdapter = crmAdapter;
        }

        /// <summary>
        /// Get partner account by account name and/or password
        /// </summary>
        /// <param name="accountName"></param>
        /// <param name="password"></param>
        /// <param name="emailAddress"></param>
        /// <returns></returns>
        public PartnerAccount GetPartnerAccountBy(string accountName, string password, string emailAddress)
        {
            return _crmAdapter.GetPartnerAccountBy(accountName, password, emailAddress);
        }
    }
}
