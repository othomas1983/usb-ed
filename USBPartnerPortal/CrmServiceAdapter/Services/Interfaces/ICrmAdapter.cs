﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Airborne.Notifications;
using Domain.Models;
using Domain.Models.Enums;

namespace CrmServiceAdapter.Services.Interfaces
{
    public interface ICrmAdapter
    {
        /// <summary>
        /// Get short course offering details by Account Id
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns></returns>
        List<ShortCourseOffering> GetShortCourseOfferingsByAccountId(Guid accountId);

        /// <summary>
        /// Get enrollments by offering id
        /// </summary>
        /// <param name="shortCourseOfferingId"></param>
        /// <returns></returns>
        List<PartnerContact> GetEnrolledContactsBy(Guid shortCourseOfferingId);

        /// <summary>
        /// Get enrollment status by name, surname and offering id
        /// </summary>
        /// <param name="firstname"></param>
        /// <param name="surname"></param>
        /// <param name="shortCourseOfferingId"></param>
        /// <param name="emailAddress"></param>
        /// <returns></returns>
        EnrollmentStatus GetEnrollmentStatus(string firstname, string surname, string emailAddress,
            Guid shortCourseOfferingId);

        /// <summary>
        /// Get partner account
        /// </summary>
        /// <param name="accountName"></param>
        /// <param name="password"></param>
        /// <param name="emailAddress"></param>
        /// <returns></returns>
        PartnerAccount GetPartnerAccountBy(string accountName = "", string password = "",
            string emailAddress = "");

        /// <summary>
        /// Insert/Update delegate details
        /// </summary>
        /// <param name="delegateDetails"></param>
        /// <returns></returns>
        NotificationCollection SaveDelegate(Domain.Models.Delegate delegateDetails);

        /// <summary>
        /// Save imported delegate details by offering Id
        /// </summary>
        /// <param name="delegateImportedDetails"></param>
        /// <param name="selectedOffering"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        NotificationCollection SaveImportedDelegates(List<Domain.Models.Delegate> delegateImportedDetails,
            Guid selectedOffering, Guid accountId);

        /// <summary>
        /// Update delegate details
        /// </summary>
        /// <param name="delegateDetails"></param>
        /// <returns></returns>
        NotificationCollection UpdateDelegateDetails(Domain.Models.Delegate delegateDetails);

        /// <summary>
        /// Get delegates by selected offering Id
        /// </summary>
        /// <param name="offeringId"></param>
        /// <returns></returns>
        List<Domain.Models.Delegate> GetDelegatesBySelectedOffering(Guid offeringId);

        /// <summary>
        /// Delete delegate by id
        /// </summary>
        /// <param name="delegateId"></param>
        /// <returns></returns>
        NotificationCollection DeleteDelegate(Guid delegateId);

        /// <summary>
        /// Update delegate status
        /// </summary>
        /// <param name="delegateDetails"></param>
        /// <returns></returns>
        NotificationCollection UpdateDelegateEmailSentStatus(Domain.Models.Delegate delegateDetails);
    }
}
