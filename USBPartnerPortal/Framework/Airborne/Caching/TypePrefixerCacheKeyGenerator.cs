using System;

namespace Airborne.Caching
{
    /// <summary>
    /// An ICacheKeyGenerator implementation that prefixes the key with the instance type e.g. {type}_{id}
    /// </summary>
    public class TypePrefixerCacheKeyGenerator : ICacheKeyGenerator
    {
        /// <summary>
        /// Creates a {type}_{id} cache key based on the state of the object instance
        /// </summary>
        public string CreateKey(object key, object instance)
        {
            if (instance is Type)
            {
                return "{0}_{1}".FormatInvariantCulture(instance, key);
            }

            return "{0}_{1}".FormatInvariantCulture(instance.GetType(), key);
        }

        /// <summary>
        /// Creates a {type}_{id} cache key based on the state of object instance T
        /// </summary>
        public string CreateKey<T>(object key, T instance)
        {
            if (instance is Type)
            {
                return "{0}_{1}".FormatInvariantCulture(instance, key);
            }
            return "{0}_{1}".FormatInvariantCulture(typeof (T), key);
        }
    }
}