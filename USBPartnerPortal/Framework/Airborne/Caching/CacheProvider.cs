using System;
using System.Collections;
using Airborne.Logging;

namespace Airborne.Caching
{
    /// <summary>
    /// Base implementation of an ICacheProvider. Utilizes a TypePrefixerCacheKeyGenerator as the default KeyGenerator.
    /// </summary>
    public abstract class CacheProvider : ICacheProvider
    {
        #region Protected

        /// <summary>
        /// The appropriate ICacheKeyGenerator instance.
        /// </summary>
        private readonly ICacheKeyGenerator cacheKeyGenerator = new TypePrefixerCacheKeyGenerator();

        /// <summary>
        /// Gets and sets the ILogger instance.
        /// </summary>
        protected ILogger Logger { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Evicts the object from the cache based on the key
        /// </summary>
        public void Evict<T>(object key)
        {
            var ck = cacheKeyGenerator.CreateKey(key, typeof(T));
            EvictInternal(ck);
        }

        /// <summary>
        /// Evicts the object from the cache based on the key
        /// </summary>
        public void Evict<T>(object key, ICacheKeyGenerator keyGenerator)
        {
            var ck = keyGenerator.CreateKey(key, typeof(T));
            EvictInternal(ck);
        }

        /// <summary>
        /// Gets the object from cache.
        /// </summary>
        public T Get<T>(object key) where T : class
        {
            return Get<T>(key, this.cacheKeyGenerator, null);
        }

        /// <summary>
        /// Gets the object from cache.
        /// </summary>
        public T Get<T>(object key, string region) where T : class
        {
            return Get<T>(key, this.cacheKeyGenerator, region);
        }

        /// <summary>
        /// Gets the object from cache.
        /// </summary>
        public T Get<T>(object key, ICacheKeyGenerator keyGenerator, string region) where T : class
        {
            var ck = keyGenerator.CreateKey(key, typeof(T));
            var result = GetInternal<T>(ck, region);
            return result as T;
        }

        /// <summary>
        /// Gets the object from cache.
        /// </summary>
        public T Get<T>(object key, Func<T> loader) where T : class
        {
            return Get(key, loader, this.cacheKeyGenerator);
        }

        /// <summary>
        /// Gets the object from cache.
        /// </summary>
        public T Get<T>(object key, Func<T> loader, ICacheKeyGenerator keyGenerator) where T : class
        {
            return Get(key, loader, keyGenerator, null);
        }

        /// <summary>
        /// Gets the object from cache.
        /// </summary>
        public T Get<T>(object key, Func<T> loader, ICacheKeyGenerator keyGenerator, string region) where T : class
        {
            var instance = Get<T>(key, keyGenerator, region);

            var isEmpty = false;

            var enumerable = instance as IEnumerable;

            if (enumerable.IsNotNull())
            {
                isEmpty = !enumerable.Any();
            }

            if (instance.IsNull() || isEmpty)
            {
                instance = loader.Invoke();

                try
                {
                    Save(key, instance, keyGenerator, region);
                }
                catch(Exception ex)
                {
                    if (Logger.IsNotNull())
                    {
                        var msg = "Error saving aggregate {0} to cache".FormatInvariantCulture(instance.ToString());
                        Logger.Error(ex.AsLogMessage(msg));
                    }
                }
            }

            return instance;
        }

        /// <summary>
        /// Save an object to the cache using the given key and keyGenerator
        /// </summary>
        public void Save<T>(object key, T value, ICacheKeyGenerator keyGenerator) where T : class
        {
            Save(key, value, keyGenerator, null);
        }

        /// <summary>
        /// Save an object to the cache using the given key and keyGenerator
        /// </summary>
        public void Save<T>(object key, T value) where T : class
        {
            Save(key, value, this.cacheKeyGenerator);
        }

        /// <summary>
        /// Save an object to the cache of the specified region using the given key and keyGenerator
        /// </summary>
        public void Save<T>(object key, T value, ICacheKeyGenerator keyGenerator, string region) where T : class
        {
            var ck = keyGenerator.CreateKey(key, value);

            Add(ck, value, region);
        }

        /// <summary>
        /// Adds the object to the cache. the key will be utilized as is.
        /// </summary>
        public void Add(string key, object value)
        {
            Add(key, value, null);
        }


        #endregion

        #region Abstracts

        /// <summary>
        /// Resolves objects from cache.To be implemented by the relevant cache provider. 
        /// </summary>
        protected abstract object GetInternal<T>(string key, string region, bool isBulk = false);

        /// <summary>
        /// Evicts objects from cache.To be implemented by the relevant cache provider. 
        /// </summary>
        protected abstract void EvictInternal(string key);

        /// <summary>
        /// Add an object to the cache to the specified region using the given key.
        /// </summary>
        public abstract void Add(string key, object value, string region);

        /// <summary>
        /// Clear the Cache
        /// </summary>
        public abstract void Clear();

        /// <summary>
        /// Gets whether the underlying cache provider is available.
        /// </summary>
        public abstract bool IsAvailable { get; }

        #endregion

        #region Virtual
        /// <summary>
        /// Not implemented (not required)
        /// </summary>
        public virtual void AddSerialisationStrategy(Type type, ISerialisationStrategy cachingStrategy)
        {

        }

        #endregion

    }
}
