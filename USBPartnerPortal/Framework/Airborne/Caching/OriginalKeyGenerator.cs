namespace Airborne.Caching
{
    /// <summary>
    /// An ICacheKeyGenerator implementation that returns the original key e.g {id}
    /// </summary>
    public class SimpleKeyGenerator : ICacheKeyGenerator
    {
        /// <summary>
        /// Creates a {id} cache key based on the state of the object instance
        /// </summary>
        public string CreateKey(object key, object instance)
        {
            return key as string;
        }

        /// <summary>
        /// Creates a {id} cache key based on the state of the typed object instance
        /// </summary>
        public string CreateKey<T>(object key, T instance)
        {
            return key as string;
        }

    }
}