namespace Airborne.Caching
{
    /// <summary>
    /// Defines the contractual obligation for the ICacheKeyGenerator. 
    /// <para>
    /// An ICacheKeyGenerator is responsible for creating keys to query against a cache store.
    /// </para>
    /// </summary>
    public interface ICacheKeyGenerator
    {
        /// <summary>
        /// Creates a cache key based on the state of the object instance
        /// </summary>
        string CreateKey(object key, object instance);


        /// <summary>
        /// Creates a cache key based on the state of the typed object instance
        /// </summary>
        string CreateKey<T>(object key, T instance);
    }
}