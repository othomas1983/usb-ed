namespace Airborne.Aggregation
{
    /// <summary>
    /// Decsribes whether the entity is Shared or Composite
    /// </summary>
    public enum AggregationType
    {
        /// <summary>
        /// Shared is also known as a 'has a' relationship because the containing object has a member
        /// object and the member object can survive or exist without the enclosing or containing class
        /// or can have a meaning after the lifetime of the enclosing object also.
        /// </summary>
        Shared,

        /// <summary>
        /// Composition is also known as a 'is a part of' or 'is a' relationship because the member 
        /// object is a part of the containing class and the member object cannot survive or exist
        /// outside the enclosing or containing class or doesnt have a meaning after the lifetime 
        /// of the enclosing object.
        /// </summary>
        Composite
    }
}