﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Airborne.Utilities
{
    /// <summary>
    /// Utility used to create a alpha numeric string based on a specified format
    /// </summary>
    public class AlphanumericDataGenerator
    {
        private const string Numbers0To9Characters = "0123456789";
        private const string AllCharacters = "abcdefghijklmnopqrstuvwxyz";

        private Random mRand;

        /// <summary>
        /// Generates a new string based on the TextFormat specified
        /// </summary>
        /// <param name="textFormat">
        /// L - An uppercase Letter.
        /// l - A lowercase letter.
        /// x - Any number, 0-9.
        /// </param>
        /// <returns></returns>
        public string GenerateValue(string textFormat)
        {
            Guard.ArgumentNotEmpty(textFormat, "TextFormat");

            mRand = new Random(DateTime.Now.Millisecond);

            var stringBuilder = new StringBuilder(textFormat.Length);

            var characters = textFormat.ToCharArray();

            foreach (char current in characters)
            {
                stringBuilder.Append(GetNextPart(current));
            }

            return stringBuilder.ToString();
        }

        private string GetNextPart(char symbol)
        {
            switch (symbol)
            {
                case 'L':
                    return GenerateRandomString(AllCharacters, 1).ToUpperInvariant();
                case 'l':
                    return GenerateRandomString(AllCharacters, 1).ToLowerInvariant();
                case 'x':
                    return GenerateRandomString(Numbers0To9Characters, 1);
                default:
                    // Just append the character as it is not a symbol.
                    return symbol.ToString(CultureInfo.InvariantCulture);
            }
        }

        private string GenerateRandomString(string allowedCharacters, int length)
        {
            int numberofChars = allowedCharacters.Length;
            var sb = new StringBuilder(length);

            for (int i = 0; i < length; i++)
            {
                sb.Append(allowedCharacters[mRand.Next(numberofChars)]);
            }
            return sb.ToString();
        }
    }
}