﻿using System;

namespace Airborne.Utilities
{   
    /// <summary>
    /// Utility class for converting time
    /// </summary>
    public static class TimeConverter
    {
        /// <summary>
        /// Converts time (hours, minutes, seconds) into seconds
        /// </summary>
        public static double? ResolveTimeAsSeconds(double? hoursValue, double? minutesValue, double? secondsValue)
        {
            var seconds = 0.0;

            minutesValue = minutesValue.IsNotNull() ? minutesValue : 0.0;
            hoursValue = hoursValue.IsNotNull() ? hoursValue : 0.0;
            secondsValue = secondsValue.IsNotNull() ? secondsValue : 0.0;

            secondsValue = AdjustForOverSixty(ref secondsValue, ref minutesValue);
            minutesValue = AdjustForOverSixty(ref minutesValue, ref hoursValue);

            if (hoursValue.IsNotNull() && hoursValue > 0)
            {
                seconds += (double)hoursValue * 60 * 60;
            }
            if (minutesValue.IsNotNull() && minutesValue > 0)
            {
                seconds += (double)minutesValue * 60;
            }
            if (secondsValue.IsNotNull() && secondsValue > 0)
            {
                seconds += (double)secondsValue;
            }
            return seconds;
        }

        private static double? AdjustForOverSixty(ref double? value, ref double? valueToIncrement)
        {
            if (value.IsNotNull() && value > 60)
            {
                var remainingValue = value % 60;
                valueToIncrement += 1;
                value = remainingValue;
                return value;
            }
            return value;
        }
    }
}
