﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Runtime.Serialization;
using Airborne.Properties;

namespace Airborne.Domain
{
    /// <summary>
    /// An Entity is anything that has continuity through a life cycle and distinctions independent 
    /// of attributes that are important to the application's user.
    /// <remarks>see http://domaindrivendesign.org/node/109</remarks>
    /// </summary>
    [DataContract]
    public abstract class Entity : INotifyPropertyChanged, INotifyPropertyChanging
    {
        #region Events

        #region PropertyChanged

        /// <summary>
        /// Occurs when a properties value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventArgs args = new PropertyChangedEventArgs(propertyName);
            OnPropertyChanged(args);
        }

        /// <summary>
        /// Raises the <see cref="E:PropertyChanged"/> event.
        /// </summary>
        /// <param name="args">The <see cref="System.ComponentModel.PropertyChangedEventArgs"/> instance containing the event data.</param>
        protected virtual void OnPropertyChanged(PropertyChangedEventArgs args)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, args);
            }
        }

        #endregion


        #region PropertyChanging

        /// <summary>
        /// Occurs when a property value is changing.
        /// </summary>
        public event PropertyChangingEventHandler PropertyChanging;

        private void RaisePropertyChanging(string propertyName)
        {
            PropertyChangingEventArgs args = new PropertyChangingEventArgs(propertyName);
            OnPropertyChanging(args);
        }

        /// <summary>
        /// Raises the <see cref="E:PropertyChanging"/> event.
        /// </summary>
        /// <param name="args">The <see cref="System.ComponentModel.PropertyChangingEventArgs"/> instance containing the event data.</param>
        protected virtual void OnPropertyChanging(PropertyChangingEventArgs args)
        {
            PropertyChangingEventHandler handler = this.PropertyChanging;
            if (handler != null)
            {
                handler(this, args);
            }
        }

        #endregion
        #endregion

        #region Fields
        private IDictionary<DomainPropertyDescriptor, object> propertyValues = new Dictionary<DomainPropertyDescriptor, object>();
        #endregion

        #region Methods
        /// <summary>
        /// Set a value in the registry of values.
        /// </summary>
        protected void SetValue(string property, object value)
        {
            DomainPropertyDescriptor propertyDescriptor = ResolveProperty(property);

            if ((value == null) || (propertyDescriptor.PropertyType.IsAssignableFrom(value.GetType())))
            {
                RaisePropertyChanging(propertyDescriptor.PropertyName);
                if (propertyValues.ContainsKey(propertyDescriptor))
                {
                    propertyValues[propertyDescriptor] = value;
                }
                else
                {
                    propertyValues.Add(propertyDescriptor, value);
                }
                RaisePropertyChanged(propertyDescriptor.PropertyName);
            }
            else
            {
                throw new ArgumentException(Resources.PropertyTypeMismatch.FormatCurrentCulture(propertyDescriptor.PropertyName, propertyDescriptor.PropertyType, value.GetType()));
            }
        }

        private DomainPropertyDescriptor ResolveProperty(string property)
        {
            Guard.ArgumentNotEmpty(property, "property");
            DomainPropertyDescriptor descriptor = propertyValues.Keys.FirstOrDefault(p => string.Compare(p.PropertyName, property, StringComparison.Ordinal) == 0);

            if (descriptor.IsNull())
            {
                descriptor = new DomainPropertyDescriptor(this.GetType(), property);
            }
            return descriptor;

        }

        /// <summary>
        /// Gets the value of a property.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter", Justification = "For explicit usage guidelines")]
        protected TPropertyType GetValue<TPropertyType>(string property)
        {
            DomainPropertyDescriptor descriptor = ResolveProperty(property);
            if (typeof(TPropertyType).IsAssignableFrom(descriptor.PropertyType))
            {
                if (propertyValues.ContainsKey(descriptor))
                {
                    return (TPropertyType)propertyValues[descriptor];
                }
                else
                {
                    return default(TPropertyType);
                }
            }
            else
            {
                throw new ArgumentException(Resources.PropertyTypeMismatch.FormatCurrentCulture(descriptor.PropertyName, descriptor.PropertyType, typeof(TPropertyType)));
            }
        }


        /// <summary>
        /// Adds the collection item and takes care of notifications for the changing collection.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "1")]
        protected void AddCollectionItem<T>(string property, IList<T> collection, T item)
        {
            Guard.ArgumentNotNull(collection, "collection");
            Guard.ArgumentNotEmpty(property, "property");
            Guard.ArgumentNotNull(item, "item");

           

            RaisePropertyChanging(property);
            collection.Add(item);
            RaisePropertyChanged(property);
        }

        /// <summary>
        /// Removes the collection item and takes care of notifications for the changing collection.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "1")]
        protected void RemoveCollectionItem<T>(string property, IList<T> collection, T item)
        {
            Guard.ArgumentNotNull(collection, "collection");
            Guard.ArgumentNotEmpty(property, "property");
            Guard.ArgumentNotNull(item, "item");


            RaisePropertyChanging(property);
            collection.Remove(item);
            RaisePropertyChanged(property);
        }
        #endregion
    }
}
