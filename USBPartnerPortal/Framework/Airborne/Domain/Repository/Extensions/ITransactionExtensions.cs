﻿
namespace Airborne.Domain.Repository
{
    /// <summary>
    /// Extends the ITransaction interface with convenience methods
    /// </summary>
    public static class ITransactionExtensions
    {
        /// <summary>
        /// Extends the ITransaction interface to provide a conditional commit check.
        /// If the condition is evaluated to true the transaction is commited. if the evaluation is false the commit is skipped.
        /// </summary>
        /// <param name="tx">
        /// <see cref="ITransaction"/>
        /// </param>
        /// <param name="condition">
        /// The boolean condition that invokes a commit.
        /// </param>
        public static void Commit(this ITransaction tx, bool condition)
        {
            tx.Commit(condition, false);
        }

        /// <summary>
        /// Extends the ITransaction interface to provide a conditional commit check.
        /// If the condition is evaluated to true the transaction is commited. if the evaluation is false this extension allows for a rollback to be invoked.
        /// </summary>
        /// <param name="tx">
        /// <see cref="ITransaction"/>
        /// </param>
        /// <param name="condition">
        /// The boolean condition that invokes a commit.
        /// </param>
        /// <param name="forceRollback">
        /// A boolean to indicate if on a false condition the transaction's rollback is invoked.
        /// </param>
        public static void Commit(this ITransaction tx, bool condition, bool forceRollback)
        {
            if (condition)
            {
                tx.Commit();
            }

            if (!condition && forceRollback)
            {
                tx.Rollback();
            }
        }
    }
}