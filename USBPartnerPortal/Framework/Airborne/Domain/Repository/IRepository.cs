﻿using System;
using System.Collections.Generic;
using Airborne.Domain.Repository.QueryObjects;
using System.Linq.Expressions;

namespace Airborne.Domain.Repository
{
    /// <summary>
    /// A Repository mediates between the domain and 
    /// data mapping layers, acting like an in-memory 
    /// domain object collection. Client objects construct query 
    /// specifications declaratively and submit them to Repository 
    /// for satisfaction. Objects can be added to and removed 
    /// from the Repository, as they can from a simple collection of 
    /// objects, and the mapping code encapsulated by the Repository 
    /// will carry out the appropriate operations behind the scenes.
    /// <remarks>see http://martinfowler.com/eaaCatalog/repository.html</remarks>
    /// </summary>
    public interface IRepository : IDisposable
    {
        /// <summary>
        /// 
        /// </summary>
        void Init();

        /// <summary>
        /// Starts a transaction
        /// </summary>
        /// <returns></returns>
        ITransaction BeginTransaction();

        /// <summary>
        /// Saves the specified entity back to the repository
        /// </summary>
        void Add(object entity);

        /// <summary>
        /// Deletes the specified entity in the repository
        /// </summary>
        void Remove(object entity);

        /// <summary>
        /// This method shows intent to perform update actions on the <param name="entity"/>
        /// </summary>
        void Update(object entity);


        /// <summary>
        /// Refreshes an entity
        /// </summary>
        void Refresh(object entity);
       
        /// <summary>
        /// Tries the find one or returns the default value for the type.
        /// </summary>
        /// <param name="expression">The Lambda expression.</param>
        /// <returns>The first entity which matches the expession or null</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures")]
        T First<T>(Expression<Func<T, bool>> expression);


        /// <summary>
        /// Tries the find one or returns the default value for the type.
        /// </summary>
        /// <param name="criteria">The <see cref="Criteria"/>.</param>
        /// <returns>The first entity which matches the filter or null</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter", Justification = "Explicit usage guidelines")]
        T First<T>(Criteria criteria);

        /// <summary>
        /// Finds all entities in the repository
        /// </summary>
        /// <returns>List of <typeparam name="T"/></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter", Justification="Explicit usage guidelines")]
        IEnumerable<T> FindAll<T>();

        /// <summary>
        /// Finds all entities matching an expression
        /// </summary>
        /// <param name="expression">The expression.</param>
        /// <returns>List of <typeparam name="T"/></returns>
        //IEnumerable<T> FindAll<T>(Func<T, bool> expression);
        IEnumerable<T> FindAll<T>(Expression<Func<T, bool>> expression);
      

        /// <summary>
        /// Finds all entities matching a filter
        /// </summary>
        /// <param name="criteria">The <see cref="Criteria"/>.</param>
        /// <returns>List of <typeparam name="T"/></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter", Justification = "Explicit usage guidelines")]
        IEnumerable<T> FindAll<T>(Criteria criteria);

        /// <summary>
        /// Finds an entity based on a know id or key.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter", Justification = "Explicit usage guidelines")]
        T FindById<T>(object id) where T : class;
    }
}

