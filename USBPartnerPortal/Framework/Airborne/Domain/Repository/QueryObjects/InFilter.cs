﻿using System.Collections;

namespace Airborne.Domain.Repository.QueryObjects
{
    /// <summary>
    /// Filter based on a list of values.
    /// </summary>
    public class InFilter : Filter
    {

        #region Properties
        /// <summary>
        /// Gets or sets the property that the <see cref="SimpleFilter"/> will be applied to.
        /// </summary>
        /// <value>The property.</value>
        public string Property
        {
            get;
            private set;
        }


        /// <summary>
        /// Values that will be evaluated for a match in this <see cref="Filter"/>
        /// </summary>
        public IEnumerable Values
        {
            get;
            private set;
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="InFilter"/> class.
        /// </summary>
        /// <param name="property">The property.</param>
        /// <param name="values">The values.</param>
        public InFilter(string property, object[] values)
        {
            Property = property;
            Values = values;
        }

        #endregion
    }
}
