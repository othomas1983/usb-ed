﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;

namespace Airborne.Domain.Repository.QueryObjects
{
    /// <summary>
    /// Default linq over objects expression builder. This takes <see cref="Criteria"/>
    /// and converts it to expressions that can be executed over a memnory collection.
    /// </summary>
    public static class ExpressionBuilder
    {


        /// <summary>
        /// Accepts <paramref name="criteria"/> and converts it to Linq expression, that is compiled to a 
        /// lambda delegate.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter", Justification = "I need the Generic type ofr the function return to make sense")]
        public static Func<T, bool> BuildFunction<T>(Criteria criteria)
        {
            ParameterExpression victimParameter = Expression.Parameter(typeof(T), "param");

            Expression expression = ExpressionBuilder.BuildExpression(victimParameter, criteria);
            var resolvedExpression = Expression.Lambda<Func<T, bool>>(expression, victimParameter);

            return resolvedExpression.Compile();
        }


        /// <summary>
        /// Accepts <paramref name="criteria"/> and converts it to Linq expression.
        /// </summary>
        public static Expression BuildExpression(Expression parameter, Criteria criteria)
        {
            Expression resolvedExpression = ResolveExpression(parameter, criteria.Filter);

            if (resolvedExpression.IsNotNull() && (criteria.Sorting.Count() > 0))
            {
                //TODO: Add Sort
                //resolvedExpression = ResolveSorting(parameter, resolvedExpression, criteria.Sorting);
            }
            //TODO : Distinct

            return resolvedExpression;
        }

        /// <summary>
        /// Resolves the <see cref="Expression"/> based on the <paramref name="filter"/>.
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <param name="filter">The filter.</param>
        /// <returns></returns>
        private static Expression ResolveExpression(Expression parameter, Filter filter)
        {
            var equalityFilter = filter as SimpleFilter;
            if (equalityFilter.IsNotNull())
            {
                return ResolveEqualityFilter(parameter, equalityFilter);
            }

            var betweenFilter = filter as BetweenFilter;
            if (betweenFilter.IsNotNull())
            {
                return ResolveBetweenFilter(parameter, betweenFilter);
            }

            var inFilter = filter as InFilter;
            if (inFilter.IsNotNull())
            {
                return ResolveInFilter(parameter, inFilter);
            }


            var operatorFilter = filter as OperatorFilter;
            if (operatorFilter.IsNotNull())
            {
                return ResolveOperatorFilter(parameter, operatorFilter);
            }

            return null;
        }

        /// <summary>
        /// Resolves a <see cref="Expression"/> based on an <paramref name="operatorFilter"/>
        /// </summary>
        private static Expression ResolveOperatorFilter(Expression parameter, OperatorFilter operatorFilter)
        {
            Expression leftExpression = ResolveExpression(parameter, operatorFilter.Left);
            Expression rightExpression = ResolveExpression(parameter, operatorFilter.Right);

            Expression resolvedExpression = null;

            switch (operatorFilter.Operator)
            {
                case OperatorType.And:
                    resolvedExpression = Expression.And(leftExpression, rightExpression);
                    break;
                case OperatorType.Or:
                    resolvedExpression = Expression.Or(leftExpression, rightExpression);
                    break;

            }
            return resolvedExpression;
        }

        /// <summary>
        /// Resolves an <see cref="Expression"/> based on the <paramref name="filter"/>
        /// </summary>
        private static Expression ResolveInFilter(Expression parameter, InFilter filter)
        {
            Expression resolvedExpression = null;
            if (filter.Values.IsNotNull())
            {
                foreach (var item in filter.Values)
                {

                    var equalExpression = Expression.Equal(Expression.PropertyOrField(parameter, filter.Property),
                                                           Expression.Constant(item));
                    if (resolvedExpression.IsNull())
                    {
                        resolvedExpression = equalExpression;
                    }
                    else
                    {
                        resolvedExpression = Expression.Or(resolvedExpression, equalExpression);
                    }
                }
            }
            return resolvedExpression;

        }


        //public static Expression ResolveSorting(Expression parameter, Expression query, IEnumerable<Sort> sorting)//this IQueryable source, string ordering, params object[] values)
        //{
        //    Expression resolvedExpression = query;
        //    string methodAsc = "OrderBy";
        //    string methodDesc = "OrderByDescending";
           
        //    foreach (var item in sorting)
        //    {
        //        Expression sort = Expression.Call(typeof(Queryable), (item.Ascending) ? methodAsc : methodDesc, new Type[] { parameter.Type }, query);
        //        resolvedExpression = Expression.Add(resolvedExpression, sort);
        //        methodAsc = "ThenBy";
        //        methodDesc = "ThenByDescending";
        //    }


        //    return resolvedExpression;

        //    //if (source == null) throw new ArgumentNullException("source");
        //    //if (ordering == null) throw new ArgumentNullException("ordering");
        //    //ParameterExpression[] parameters = new ParameterExpression[] {
        //    //    Expression.Parameter(source.ElementType, "") };
        //    //ExpressionParser parser = new ExpressionParser(parameters, ordering, values);
        //    //IEnumerable<DynamicOrdering> orderings = parser.ParseOrdering();
        //    //Expression queryExpr = source.Expression;
        //    //string methodAsc = "OrderBy";
        //    //string methodDesc = "OrderByDescending";
        //    //foreach (DynamicOrdering o in orderings)
        //    //{
        //    //    queryExpr = Expression.Call(
        //    //        typeof(Queryable), o.Ascending ? methodAsc : methodDesc,
        //    //        new Type[] { source.ElementType, o.Selector.Type },
        //    //        queryExpr, Expression.Quote(Expression.Lambda(o.Selector, parameters)));
        //    //    methodAsc = "ThenBy";
        //    //    methodDesc = "ThenByDescending";
        //    //}
        //    //return source.Provider.CreateQuery(queryExpr);
        //}


        /// <summary>
        /// Resolves an <see cref="Expression"/> based on the <paramref name="filter"/>
        /// </summary>
        private static Expression ResolveBetweenFilter(Expression parameter, BetweenFilter filter)
        {
            Expression resolvedExpression = null;
            if (filter.Inclusive)
            {
                resolvedExpression = Expression.And(Expression.GreaterThanOrEqual(Expression.PropertyOrField(parameter, filter.Property),
                                                                 Expression.Constant(filter.Lower)),
                                                                 Expression.LessThanOrEqual(Expression.PropertyOrField(parameter, filter.Property),
                                                                 Expression.Constant(filter.Upper)));
            }
            else
            {
                resolvedExpression = Expression.And(Expression.GreaterThan(Expression.PropertyOrField(parameter, filter.Property),
                                                                  Expression.Constant(filter.Lower)),
                                                                  Expression.LessThan(Expression.PropertyOrField(parameter, filter.Property),
                                                                  Expression.Constant(filter.Upper)));
            }
            return resolvedExpression;
        }

        /// <summary>
        /// Resolves an <see cref="Expression"/> based on the <paramref name="filter"/>
        /// </summary>
        private static Expression ResolveEqualityFilter(Expression parameter, SimpleFilter filter)
        {
            Expression resolvedExpression = null;

            switch (filter.Operator)
            {
                case FilterType.Like:
                    if (filter.Value is string)
                    {
                        //Expression.Lambda(
                    }
                    break;
                case FilterType.Equal:
                    resolvedExpression = Expression.Equal(Expression.PropertyOrField(parameter, filter.Property),
                                                            Expression.Constant(filter.Value));
                    break;
                case FilterType.NotEqual:
                    resolvedExpression = Expression.NotEqual(Expression.PropertyOrField(parameter, filter.Property),
                                                            Expression.Constant(filter.Value));
                    break;
                case FilterType.GreaterThan:
                    resolvedExpression = Expression.GreaterThan(Expression.PropertyOrField(parameter, filter.Property),
                                                            Expression.Constant(filter.Value));
                    break;
                case FilterType.GreaterThanOrEqualTo:
                    resolvedExpression = Expression.GreaterThanOrEqual(Expression.PropertyOrField(parameter, filter.Property),
                                                            Expression.Constant(filter.Value));
                    break;
                case FilterType.LessThan:
                    resolvedExpression = Expression.LessThan(Expression.PropertyOrField(parameter, filter.Property),
                                                            Expression.Constant(filter.Value));
                    break;
                case FilterType.LessThanOrEqualTo:
                    resolvedExpression = Expression.LessThanOrEqual(Expression.PropertyOrField(parameter, filter.Property),
                                                            Expression.Constant(filter.Value));
                    break;
                default:
                    break;
            }

            return resolvedExpression;
        }

    }
}
