﻿using Airborne.Notifications;

namespace Airborne.Domain.Rules
{
    /// <summary>
    /// Once implemented will provide a common interface to perform validation or execute business rules agains entities.
    /// </summary>
    public interface IValidationEngine
    {
        /// <summary>
        /// Validates the specified <param name="instance"/>.
        /// </summary>
        NotificationCollection Validate(object instance);


        ///// <summary>
        ///// Validates the specified <param name="instance"/>.
        ///// </summary>
        //NotificationCollection Validate<TTypeToValidate>(TTypeToValidate instance);

    }
}
