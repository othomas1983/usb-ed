﻿

namespace Airborne.Domain
{
    /// <summary>
    /// Contract to define custom commands 
    /// </summary>
    public interface ICommand<TInput>
    {
        /// <summary>
        /// Gets the name of the command
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Executes this command.
        /// </summary>
        /// <returns></returns>
        object Execute(TInput input);
    }
}