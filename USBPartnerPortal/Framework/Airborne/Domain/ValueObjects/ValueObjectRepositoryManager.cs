//using Microsoft.Practices.ServiceLocation;

namespace Airborne.Domain.ValueObjects
{
   /// <summary>
   /// A manager to hide the instantiation nitty gritty 
   /// </summary>
   public static class ValueObjectRepositoryManager
   {
      #region ValueObjectRepositoryManager Singleton
      private static IValueObjectRepository instance;
      private static object mutex = new object();

      /// <summary>
      /// Gets a thread safe single instance of <see cref="ValueObjectRepositoryManager"/>
      /// </summary>
      public static IValueObjectRepository Instance
      {
         get
         {
            lock (mutex)
            {
               if (instance == null)
               {
                  //instance = ServiceLocator.Current.GetInstance<IValueObjectRepository>();
               }
            }
            return instance;
         }
      }
      #endregion

   }
}
