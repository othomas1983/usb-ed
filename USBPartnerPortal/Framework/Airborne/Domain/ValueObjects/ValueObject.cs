using System;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;

namespace Airborne.Domain.ValueObjects
{
    /// <summary>
    /// Value Object is an object whose identity is 
    /// defined by its state rather than by its address.
    /// </summary>
    [DataContract]
    public abstract class ValueObject : IValueObject
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ValueObject"/> class.
        /// </summary>
        protected ValueObject()
        {
        }

        #endregion

        #region Properties
        internal string Data
        {
            get
            {
                return RetrieveDefinition();
            }
            set
            {
                PopulateFields(value);

            }
        }




        /// <summary>
        /// Code that uniquely defines a value object.
        /// </summary>
        [DataMember]
        public string Code
        {
            get;
            set;
        }

        /// <summary>
        /// Descriptive text of the value object.
        /// </summary>
        [DataMember]
        public string Text
        {
            get;
            set;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Retrieves and xml document contianing values for all fields other than code and text.
        /// </summary>
        /// <returns></returns>
        private string RetrieveDefinition()
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(this);
            StringBuilder builder = new StringBuilder();
            builder.Append("<Properties>");
            for (int i = 0; i < properties.Count; i++)
            {
                PropertyDescriptor property = properties[i];
                if ((string.Compare("Code", property.Name, StringComparison.OrdinalIgnoreCase) != 0) && (string.Compare("Text", property.Name, StringComparison.OrdinalIgnoreCase) != 0))
                {
                    builder.AppendFormat("<{0}>{1}</{0}>", property.Name, property.GetValue(this));
                }
            }
            builder.Append("</Properties>");

            return builder.ToString();
        }


        /// <summary>
        /// Populate fileds based on an xml definition.
        /// </summary>
        private void PopulateFields(string data)
        {
            if (string.IsNullOrEmpty(data))
            {
                return;
            }

            //get all the fiels from the xml and their values.
            MemoryStream stream = new MemoryStream(Encoding.ASCII.GetBytes(data));
            XmlReader reader = XmlReader.Create(stream);

            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(this);
            reader.Read();
            while (!reader.EOF)
            {

                string propertyName = reader.Name;

                if (string.Compare(propertyName, "Properties", StringComparison.OrdinalIgnoreCase) == 0)
                {
                    reader.Read();
                    continue;
                }

                PropertyDescriptor property = properties.Find(propertyName, false);

                if (property == null)
                {
                    throw new TypeLoadException(string.Format(CultureInfo.CurrentCulture, "Property {0} does not exist on value object type {1}", propertyName, this.GetType().FullName));
                }
                else
                {

                    object value = null;
                    if ((typeof(ValueObject).IsAssignableFrom(property.PropertyType)))
                    {
                        string valueObjectCode = reader.ReadElementContentAsString();
                        value = ValueObject.Repository.Find(property.PropertyType, valueObjectCode);
                    }
                    else
                    {
                        value = reader.ReadElementContentAs(property.PropertyType, null);
                    }

                    property.SetValue(this, value);

                    reader.MoveToElement();
                }
            }

        }
        #endregion

        #region Operators

        #region ValueObject vs. String

        /// <summary>
        /// Implements the operator ==.
        /// </summary>
        public static bool operator ==(ValueObject left, string right)
        {
            return ValueObjectEqualsString(left, right);
        }

        /// <summary>
        /// Implements the operator !=.
        /// </summary>
        public static bool operator !=(ValueObject left, string right)
        {
            return !ValueObjectEqualsString(left, right);
        }

        #endregion

        #region ValueObject vs. ValueObject

        /// <summary>
        /// Implements the operator ==.
        /// </summary>
        public static bool operator ==(ValueObject left, ValueObject right)
        {
            return ValueObjectsEqual(left, right);
        }

        /// <summary>
        /// Implements the operator !=.
        /// </summary>
        public static bool operator !=(ValueObject left, ValueObject right)
        {
            return !ValueObjectsEqual(left, right);
        }

        #endregion

        #endregion

        #region Override Methods

        /// <summary>
        /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
        /// </returns>
        public override string ToString()
        {
            return Code;
        }

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>.
        /// </summary>
        /// <param name="obj">The <see cref="T:System.Object"/> to compare with the current <see cref="T:System.Object"/>.</param>
        /// <returns>
        /// true if the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>; otherwise, false.
        /// </returns>
        /// <exception cref="T:System.NullReferenceException">
        /// The <paramref name="obj"/> parameter is null.
        /// </exception>
        public override bool Equals(object obj)
        {
            ValueObject valueObject = obj as ValueObject;
            if (null != valueObject)
            {
                return ValueObjectsEqual(this, valueObject);
            }
            else if (obj is string)
            {
                return ValueObjectEqualsString(this, Convert.ToString(obj, CultureInfo.InvariantCulture));
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>
        /// A hash code for the current <see cref="T:System.Object"/>.
        /// </returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        #endregion

        #region Private Methods

        private static bool ValueObjectEqualsString(ValueObject left, string right)
        {
            if (Object.ReferenceEquals(left, null) && string.IsNullOrEmpty(right))
            {
                return true;
            }
            else if (Object.ReferenceEquals(left, null) || string.IsNullOrEmpty(right))
            {
                return false;
            }
            else
            {
                return left.Code.ToUpperInvariant().Equals(right.ToUpperInvariant());
            }
        }

        private static bool ValueObjectsEqual(ValueObject left, ValueObject right)
        {
            if (Object.ReferenceEquals(left, null) && Object.ReferenceEquals(right, null))
            {
                return true;
            }
            else if (Object.ReferenceEquals(left, null) || Object.ReferenceEquals(right, null))
            {
                return false;
            }
            else
            {
                return left.Code.ToUpperInvariant().Equals(right.Code.ToUpperInvariant());
            }
        }

        #endregion

        #region Static Properties

        private static IValueObjectRepository repository;

        /// <summary>
        /// Gets the repository.
        /// </summary>
        public static IValueObjectRepository Repository
        {
            get
            {
                if (repository == null)
                {
                    repository = ValueObjectRepositoryManager.Instance;
                }
                return repository;
            }
            set
            {
                repository = value;
            }
        }

        #endregion
    }

    /// <summary>
    /// Marker that indicates that the object can be consisted a value object.
    /// </summary>
    public interface IValueObject { }
}
