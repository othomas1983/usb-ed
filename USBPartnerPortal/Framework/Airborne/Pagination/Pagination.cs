﻿using System;
using System.Runtime.Serialization;

namespace Airborne.Pagination
{
    /// <summary>
    /// Provides base functionality for types that are considered pagination instances.
    /// </summary>
    [DataContract]
    public abstract class Pagination
    {
        /// <summary>
        /// Gets and sets the page index.
        /// </summary>
        [DataMember]
        public int? PageIndex { get; set; }

        /// <summary>
        /// Gets and sets the page size.
        /// </summary>
        [DataMember]
        public int? PageSize{ get; set; }

        /// <summary>
        /// Gets whether the pagination instance has pages.
        /// </summary>
        public bool HasPaging
        {
            get { return PageIndex.IsNotNull() && PageSize.IsNotNull(); }
        }
    }
}
