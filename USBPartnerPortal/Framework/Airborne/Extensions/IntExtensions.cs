﻿using Airborne.Extensions;
using System.Linq;
using System.Globalization;

namespace System
{
    /// <summary>
    /// Additional extensions on <see cref="System.Int32"/>
    /// </summary>
    public static class IntExtensions
    {
        /// <summary>
        /// e.g. the value 101 will return "One Hundred And One"
        /// </summary>
        public static string ToWords(this int value)
        {
            return NumberConverter.ConvertToWords(value);
        }


        /// <summary>
        /// Returns the integer value as a hour minute time string e.g 00h 00m.
        ///<para> Assumes the instance is in seconds. 
        /// digitalFormat of true will return the string as 00:00.
        /// </para>
        /// </summary>
        public static string ToHourMinuteString(this int seconds, bool digitalFormat = false)
        {
            var time = TimeSpan.FromSeconds(seconds);

            var displayFormat = digitalFormat ? "{0}:{1}" : "{0}h{1}m";
         
            var hours = time.Hours + (time.Days * 24);
            return displayFormat.FormatInvariantCulture(hours.ToString("00", CultureInfo.InvariantCulture), time.Minutes.ToString("00", CultureInfo.InvariantCulture));
        }


        /// <summary>
        /// e.g. the value 101 will return "One Hundred And One"
        /// </summary>
        public static string ToWords(this int? value)
        {
            if (value.HasValue)
            {
                return value.Value.ToWords();
            }
            return string.Empty;
        }

        /// <summary>
        /// e.g. the value 3 will return "3rd"
        /// </summary>
        public static string ToPlace(this int? value)
        {
            if (value.HasValue)
            {
                return value.Value.ToPlace();
            }
            return string.Empty;
        }

        /// <summary>
        /// e.g. the value 3 will return "3rd"
        /// </summary>
        public static string ToPlace(this int value)
        {
            return NumberConverter.ConvertToPlace(value);
        }

        /// <summary>
        ///  Determines if the value falls between <paramref name="value1"/> and <paramref name="value2"/>
        /// </summary>
        public static bool Between(this int value, int value1, int value2)
        {
            var min = new[] { value1, value2 }.Min();
            var max = new[] { value1, value2 }.Max();

            return value >= min && value <= max;
        }

    }
}