﻿
namespace System
{

    /// <summary>
    /// Additional extensions on double
    /// </summary>
    public static class DoubleExtensions
    {
        /// <summary>
        /// Returns logical result of value falling within the specified start and end range
        /// </summary>
        public static bool Between(this double instance, double start, double end)
        {
            return instance >= start && instance <= end;
        }

        /// <summary>
        /// Returns logical result of value falling within the specified start and end range. Allows control of the start and end date inclusion.
        /// </summary>
        public static bool Between(this double instance, double start, double end, bool includeStart, bool includeEnd)
        {
            var startExp = new Func<bool>(() => { return instance >= start; });
            var endExp = new Func<bool>(() => { return instance <= end; });

            if (!includeStart)
            {
                startExp = () => { return instance > start; };
            }

            if (!includeEnd)
            {
                endExp = () => { return instance < end; };
            }

            return (startExp.Invoke() && endExp.Invoke());
        }
    }
}