﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Airborne.Logging
{
    /// <summary>
    /// ILogger implementation using the System.Diagnostics.Debug as the underlying logger
    /// </summary>
    public class DebuggerLogger : ILogger
    {
        /// <summary>
        /// Writes an info message to the underlying trace listener.
        /// </summary>
        public void Info(string message)
        {
             Debug.WriteLine(message);
        }

        /// <summary>
        /// Writes an info message to the underlying trace listener.
        /// </summary>
        public void Warn(string message)
        {
             Debug.WriteLine("WARNING: {0}".FormatInvariantCulture(message));
        }

        /// <summary>
        /// Writes an info message to the underlying trace listener.
        /// </summary>
        public void Error(string message)
        {
            Debug.WriteLine("ERROR: {0}".FormatInvariantCulture(message));
        }

        /// <summary>
        /// Writes an info message to the underlying trace listener.
        /// </summary>
        public void Error(Exception ex)
        {
            Error(ex.Message);
            Debug.WriteLine(ex.ToString());
        }
    }
}
