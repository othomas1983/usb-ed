using System;
using System.Linq.Expressions;

namespace Airborne.Predication
{
    /// <summary>
    ///  Defines a method to create a predication.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IPredicateBuilder<T>
    {
        /// <summary>
        /// Returns the predicate.
        /// </summary>
        Expression<Func<T, bool>> AsPredicate();
    }
}