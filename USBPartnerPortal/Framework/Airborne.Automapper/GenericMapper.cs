﻿using Airborne.Utilities;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using AutoMapper.Mappers;

namespace Airborne.Automapper
{
    /// <summary>
    /// A generic mapper implementation utilizing the Automapper framework.
    /// </summary>
    public class GenericMapper<TIn, TOut> : IObjectMapper<TIn, TOut>
    {
        #region Constructors

        /// <summary>
        /// Instantiates a new instance of a generic mapper. <para>See <see cref="Airborne.Automapper.GenericMapper&lt;TIn, TOut&gt;"/> class for more information.</para>
        /// </summary>
        public GenericMapper()
        {
            namesToIgnore = new List<string>();
        }

        #endregion

        #region Locals

        private IList<string> namesToIgnore;

        #endregion

        #region IObjectMapper

        /// <summary>
        /// Performs the assembly from one object to the other.
        /// </summary>
        public virtual TOut MapObjects(TIn objectIn)
        {
            CreateMapper<TIn, TOut>();

            return Mapper.Map<TIn, TOut>(objectIn);
        }

        /// <summary>
        /// Performs the assembly from one object to the other. using the reference object as the base return.
        /// </summary>
        public virtual TOut MapObjects(TIn objectIn, TOut refObject)
        {
            CreateMapper<TIn, TOut>();

            return Mapper.Map<TIn, TOut>(objectIn, refObject);
        }

        #endregion

        #region Methods

        private void CreateMapper<In, Out>()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<In, Out>();
            });

            //var config = new MapperConfiguration(cfg =>
            //{
                
            //});

            //IMapper mapper = config.CreateMapper();

            //var mapE = Mapper.CreateMap<In, Out>();
            //ApplyIgnores(mapE);
        }

        private void ApplyIgnores<In, Out>(IMappingExpression<In, Out> expression)
        {
            foreach (var name in namesToIgnore)
            {
                expression.ForMember(name, opt => opt.Ignore());
            }
        }

        public void Ignore(string destinationMember)
        {
            namesToIgnore.Add(destinationMember);
        }

        public void Ignore(Expression<Func<TOut, object>> destinationMember)
        {


            throw new NotImplementedException();
        }

        #endregion
    }
}