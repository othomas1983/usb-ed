﻿using System;
using Domain.Models.Enums;

namespace Domain.Models
{
    public class Delegate
    {
        public Guid? DelegateId { get; set; }

        public string DelegateName { get; set; }

        public string DelegateSurname { get; set; }

        public string DelegateEmail { get; set; }

        public string DelegateApplicationUrl { get; set; }

        public Guid? SelectedOffering { get; set; }

        public Guid? AccountId { get; set; }

        public DelegateStatus DelegateStatus { get; set; }

        public EnrollmentStatus EnrollmentStatus { get; set; }
    }
}
