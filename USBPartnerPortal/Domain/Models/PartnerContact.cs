﻿using System;

namespace Domain.Models
{
    public class PartnerContact
    {
        public string Name { get; set; }

        public string Surname { get; set; }

        public string Email { get; set; }
    }
}
