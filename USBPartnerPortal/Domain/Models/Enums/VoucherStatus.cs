﻿namespace Domain.Models.Enums
{
    public enum VoucherStatus
    {
        Redeemable = 1,
        Redeemed = 956390000,
        Expired = 956390001
    }
}
