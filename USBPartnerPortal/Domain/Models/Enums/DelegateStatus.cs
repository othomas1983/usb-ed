﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models.Enums
{
    public enum DelegateStatus
    {
        [Description("Nominated")]
        Nominated = 1,
        [Description("URL Sent")]
        URLSent = 956390000,
        [Description("Applied")]
        Applied = 956390001
    }
}
