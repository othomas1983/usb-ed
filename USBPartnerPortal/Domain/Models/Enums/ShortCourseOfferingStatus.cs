﻿namespace Domain.Models.Enums
{
    public enum ShortCourseOfferingStatus
    {
        Cancelled = 2,
        New = 956390000,
        OpenforApplication = 956390001,
        ApplicationClosed = 956390002,
        Complete = 956390003
    }
}
