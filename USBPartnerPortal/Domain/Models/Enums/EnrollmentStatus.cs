﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Models.Enums
{
    public enum EnrollmentStatus
    {
        [Description("None")]
        None = 0,
        [Description("Applied")]
        Applied = 1,
        [Description("Deactivated")]
        Deactivated = 2,
        [Description("Request Admitted")]
        RequestAdmitted = 956390000,
        [Description("Admitted")]
        Admitted = 956390001,
        [Description("Failed Admitted")]
        FailedAdmitted = 956390002,
        [Description("Waitlisted")]
        Waitlisted = 956390003,
        [Description("Not Admitted")]
        NotAdmitted = 956390004,
        [Description("Request Registered")]
        RequestRegistered = 956390005,
        [Description("Registered")]
        Registered = 956390006,
        [Description("Failed Registered")]
        FailedRegistered = 956390007,
        [Description("Request Passed")]
        RequestPassed = 956390008,
        [Description("Passed")]
        Passed = 956390009,
        [Description("Failed Passed")]
        FailedPassed = 956390010,
        [Description("Request Failed")]
        RequestFailed = 956390011,
        [Description("Failed")]
        Failed = 956390012,
        [Description("Failed Failed")]
        FailedFailed = 956390013,
        [Description("Cancelled")]
        Cancelled = 956390014,
        [Description("Qualified")]
        Qualified = 956390017,
        [Description("Approved")]
        Approved = 956390018,
        [Description("Not Qualified")]
        NotQualified = 956390019,
        [Description("Not Approved")]
        NotApproved = 956390020,
        [Description("Accepted")]
        Accepted = 956390021,
        [Description("Not Accepted")]
        NotAccepted = 956390022,
        [Description("Paid")]
        Paid = 956390023,
        [Description("Not Paid")]
        NotPaid = 956390024,
        [Description("Completed")]
        Completed = 956390025,
        [Description("Incompleted")]
        Incompleted = 956390026,
        [Description("Request Qualified")]
        RequestQualified = 956390027,
        [Description("Request Accepted")]
        RequestAccepted = 956390028
    }
}
