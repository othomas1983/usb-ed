﻿using System;
using System.Collections.Generic;

namespace Domain.Models
{
    public class ShortCourseOffering
    {
        public Guid ShortCourseOfferingId { get; set; }

        public string OfferingName { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int NumberOfVouchersPurchased { get; set; }

        public int NumberOfVouchersRedeemable { get; set; }

        public int NumberOfVouchersRedeemed { get; set; }

        public string OwnerName { get; set; }

        public string OwnerEmailAddress { get; set; }
    }
}