﻿using System;
using Domain.Models.Enums;

namespace Domain.Models
{
    public class PartnerAccount
    {
        public Guid AccountId { get; set; }

        public string AccountName { get; set; }

        public string EmailAddress { get; set; }

        public string Username { get; set; }

        public string AccountImageUrl { get; set; }

        public bool CanNominateDelegates { get; set; }

        public AccountType? AccountType { get; set; }
    }
}
