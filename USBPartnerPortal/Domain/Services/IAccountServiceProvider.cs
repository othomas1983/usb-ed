﻿using Domain.Models;
using Microsoft.AspNet.Identity.Owin;

namespace Domain.Services
{
    public interface IAccountServiceProvider
    {
        /// <summary>
        /// Get partner account
        /// </summary>
        /// <param name="accountName"></param>
        /// <param name="password"></param>
        /// <param name="emailAddress"></param>
        /// <returns></returns>
        PartnerAccount GetPartnerAccountBy(string accountName, string password, string emailAddress);
    }
}
