﻿using System;
using System.Collections.Generic;
using Airborne.Notifications;
using Delegate = Domain.Models.Delegate;

namespace Domain.Services
{
    public interface IDelegateServiceProvider
    {
        /// <summary>
        /// Insert/Update delegate details
        /// </summary>
        /// <param name="delegateDetails"></param>
        /// <returns></returns>
        NotificationCollection SaveDelegate(Delegate delegateDetails);

        /// <summary>
        /// Save imported delegate details by offering Id
        /// </summary>
        /// <param name="delegateImportedDetails"></param>
        /// <param name="selectedOffering"></param>
        /// <param name="accountId"></param>
        /// <returns></returns>
        NotificationCollection SaveImportedDelegates(List<Delegate> delegateImportedDetails, Guid selectedOffering, Guid accountId);

        /// <summary>
        /// Update delegate details
        /// </summary>
        /// <param name="delegateDetails"></param>
        /// <returns></returns>
        NotificationCollection UpdateDelegateDetails(Delegate delegateDetails);

        /// <summary>
        /// Get delegates by selected offering Id
        /// </summary>
        /// <param name="offeringId"></param>
        /// <returns></returns>
        List<Delegate> GetDelegatesBySelectedOffering(Guid offeringId);

        /// <summary>
        /// Delete delegate by id
        /// </summary>
        /// <param name="delegateId"></param>
        /// <returns></returns>
        NotificationCollection DeleteDelegate(Guid delegateId);

        /// <summary>
        /// Update delegate status
        /// </summary>
        /// <param name="delegateDetails"></param>
        /// <returns></returns>
        NotificationCollection UpdateDelegateEmailSentStatus(Delegate delegateDetails);
    }
}
