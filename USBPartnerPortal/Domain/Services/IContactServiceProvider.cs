﻿using System;
using System.Collections.Generic;
using Domain.Models;

namespace Domain.Services
{
    public interface IContactServiceProvider
    {
        /// <summary>
        /// Get enrollments by offering id
        /// </summary>
        /// <param name="shortCourseOfferingId"></param>
        /// <returns></returns>
        List<PartnerContact> GetEnrolledContactsBy(Guid shortCourseOfferingId);
    }
}
