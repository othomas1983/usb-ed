﻿using System;
using System.Collections.Generic;
using Domain.Models;

namespace Domain.Services
{
    public interface IShortCourseOfferingServiceProvider
    {
        /// <summary>
        /// Get short course offering details by Account Id
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns></returns>
        List<ShortCourseOffering> GetShortCourseOfferingsByAccountId(Guid accountId);
    }
}
