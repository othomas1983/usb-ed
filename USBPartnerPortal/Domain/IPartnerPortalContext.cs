﻿using Domain.Services;
using Microsoft.Practices.Unity;

namespace Domain
{
    public interface IPartnerPortalContext
    {
        IAccountServiceProvider AccountService { get; set; }

        IContactServiceProvider ContactService { get; set; }

        IShortCourseOfferingServiceProvider ShortCourseOfferingService { get; set; }

        IDelegateServiceProvider DelegateService { get; set; }
    }

    public class ComposableIPartnerPortalContext : IPartnerPortalContext
    {
        [Dependency]
        public IAccountServiceProvider AccountService { get; set; }

        [Dependency]
        public IContactServiceProvider ContactService { get; set; }

        [Dependency]
        public IShortCourseOfferingServiceProvider ShortCourseOfferingService { get; set; }

        [Dependency]
        public IDelegateServiceProvider DelegateService { get; set; }
    }
}
