/******************************************************************************************************
	Synopsis:	Table to store User / Manager Permissions
	
	=================================================================================================
	WHEN		WHO					WHAT
	=================================================================================================
	31/05/2017	DA Cagnetta(EOHMC)	Created
	=================================================================================================
******************************************************************************************************/


CREATE TABLE [dbo].[Auth](
	[AuthID] [int] IDENTITY(1,1) NOT NULL,
	[UserCVID] [int] NOT NULL,
	[ManagerCVID] [int] NOT NULL,
	[Action] [nvarchar](50) NOT NULL,
	[Feature] [nvarchar](50) NOT NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[ModifiedBy] [nvarchar](50) NULL,
	[isActive] [bit] NOT NULL CONSTRAINT [DF_Auth_isActive]  DEFAULT ((1)),
 CONSTRAINT [PK_dbo.Auth] PRIMARY KEY CLUSTERED 
(
	[AuthId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Auth] WITH CHECK ADD  CONSTRAINT [FK_Auth_CV] FOREIGN KEY([UserCVID])
REFERENCES [dbo].[CV] ([CVID])
GO

ALTER TABLE [dbo].[Auth] WITH CHECK ADD  CONSTRAINT [FK_Auth_CV1] FOREIGN KEY([ManagerCVID])
REFERENCES [dbo].[CV] ([CVID])
GO



