ALTER PROC [dbo].[sp_FacultyDepartmentRead]
/******************************************************************************************************
	Synopsis:	Proc will read records for Faculty Department
				PARAMETERS @CVID
	
	=================================================================================================
	WHEN		WHO					WHAT
	=================================================================================================
	14/10/2015	J Combrinck(EOHMC)	Created
	21/12/2016	CA Pieterse(EOHMC)	Added 'isActive' to query result.
	11/05/2017	DA Cagnetta(EOHMC)	Added 'isActive' to where filter.
	=================================================================================================
******************************************************************************************************/
@CVID INT
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRY
		-----------------------------------------------------
			--	10.	Faculty Department Read
		-----------------------------------------------------
		SELECT	 FD.CVID
				,D.DepartmentName
				,DC.DepartmentCategoryName
				,D.DepartmentID
				,DC.DepartmentCategoryID
				,FD.FacultyDepartmentID
                ,FD.isActive
		FROM dbo.FacultyDepartment	FD
		JOIN dbo.Department			D  ON FD.DepartmentID = D.DepartmentID
		JOIN dbo.DepartmentCategory	DC ON FD.DepartmentCategoryID = DC.DepartmentCategoryID
		WHERE FD.CVID = @CVID 
				AND FD.isActive = 1
		;
	END TRY
	
	BEGIN CATCH
		DECLARE	 @ERR_MSG	NVARCHAR(4000) 
				,@ERR_STA	SMALLINT 

		SELECT	 @ERR_MSG	= ERROR_MESSAGE()
				,@ERR_STA	= ERROR_STATE()
 
		SET @ERR_MSG= 'Read FAILED: ' + ISNULL(@ERR_MSG,'');
 
		THROW 50001, @ERR_MSG, @ERR_STA;

	END CATCH
	;
END
;