/******************************************************************************************************
	Synopsis:	dbo.AuthTable User Defined Table				
	
	=================================================================================================
	WHEN		WHO					WHAT
	=================================================================================================
	31/05/2017	DA Cagnetta(EOHMC)	Created	
	=================================================================================================
******************************************************************************************************/

CREATE TYPE dbo.AuthTable AS TABLE 
( 
	[AuthId] INT IDENTITY(1,1) NOT NULL,
	[UserCVID] [int] NOT NULL,
	[ManagerCVID] [int] NOT NULL,
	[Action] [nvarchar](50) NOT NULL,
	[Feature] [nvarchar](50) NOT NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[ModifiedBy] [nvarchar](50) NULL,
	[isActive] [bit] NOT NULL DEFAULT ((1))
)
GO