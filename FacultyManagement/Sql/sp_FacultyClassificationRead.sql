CREATE Procedure [dbo].[sp_FacultyClassificationRead]
/******************************************************************************************************
	Synopsis:	Proc will read records for Faculty Classifications
				PARAMETERS @CVID
	
	=================================================================================================
	WHEN		WHO					WHAT
	=================================================================================================
	12/05/2017	DA Cagnetta(EOHMC)	Created
	=================================================================================================
******************************************************************************************************/
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRY
		-----------------------------------------------------
			--	10.	Faculty Sufficiency Read
		-----------------------------------------------------
		SELECT [ClassificationID]
			   ,[Classification]			 
		  FROM [FacultyManagement].[dbo].[FacultyClassification]
			WHERE isActive = 1
		;
	END TRY
	
	BEGIN CATCH
		DECLARE	 @ERR_MSG	NVARCHAR(4000) 
				,@ERR_STA	SMALLINT 

		SELECT	 @ERR_MSG	= ERROR_MESSAGE()
				,@ERR_STA	= ERROR_STATE()
 
		SET @ERR_MSG= 'Read FAILED: ' + ISNULL(@ERR_MSG,'');
 
		THROW 50001, @ERR_MSG, @ERR_STA;

	END CATCH
	;
END
;
