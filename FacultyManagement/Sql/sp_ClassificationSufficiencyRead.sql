CREATE PROC [dbo].[sp_ClassificationSufficiencyRead]
/******************************************************************************************************
	Synopsis:	Proc will read records for Grant
				PARAMETERS @CVID
	
	=================================================================================================
	WHEN		WHO					WHAT
	=================================================================================================
	12/05/2016	DA Cagnetta(EOHMC)	Created
	=================================================================================================
******************************************************************************************************/
@CVID			INT 
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRY
		-----------------------------------------------------
			--	10.	Grant Read
		-----------------------------------------------------
		SELECT	 CVID
				,ClassificationID
				,SufficiencyID				
		FROM dbo.[CV]
		WHERE CVID = @CVID
		;
	END TRY
	
	BEGIN CATCH
		DECLARE	 @ERR_MSG	NVARCHAR(4000) 
				,@ERR_STA	SMALLINT 

		SELECT	 @ERR_MSG	= ERROR_MESSAGE()
				,@ERR_STA	= ERROR_STATE()
 
		SET @ERR_MSG= 'Read FAILED: ' + ISNULL(@ERR_MSG,'');
 
		THROW 50001, @ERR_MSG, @ERR_STA;

	END CATCH
	;
END
;