
CREATE PROC [dbo].[sp_AuthCUD]
/******************************************************************************************************
	Synopsis:	Proc will create new records Education and update and soft delete the records
				PARAMETERS @tvpEducation, @UserName
	
	=================================================================================================
	WHEN		WHO					WHAT
	=================================================================================================
	17/08/2015	J Combrinck(EOHMC)	Created
	01/09/2015	R Goosen(EOHMC)     Updated Insert section to only insert when AwardID is null
	01/09/2015	J Combrinck(EOHMC)	Removed Award and updated for only Education 
	=================================================================================================
******************************************************************************************************/
 @tvpAuth		dbo.AuthTable READONLY
,@UserName		 VARCHAR(50) = 'User Not Provided'

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRY
		BEGIN TRANSACTION
			-------------------------------------------
			--	10.	Create new records for Education
			-------------------------------------------
			INSERT INTO dbo.Auth ([AuthId] , [UserCVID], [ManagerCVID], [Action], [Feature], [CreatedBy], [ModifiedBy], [isActive])
			SELECT	 [AuthId]
					,[UserCVID]
					,[ManagerCVID]
					,[Action]
					,[Feature]
					,@UserName
					,@UserName
					,[isActive]
			FROM @tvpAuth  
			WHERE [AuthId] IS NULL
			;
			
			-------------------------------------------
			--	20.	Update values in Education table
			-------------------------------------------
			UPDATE A
			SET	 [UserCVID]			= T.[UserCVID]
				,[ManagerCVID]		= T.[ManagerCVID]
				,[Action]			= T.[Action]		
				,[Feature]			= T.[Feature]		
				,ModifiedBy 		= @UserName
				,CreatedBy 		= @UserName
			FROM dbo.Auth	A
			JOIN @tvpAuth	T ON A.[AuthId] = T.[AuthId]
			WHERE T.isActive = 1
			AND (
					   ISNULL(A.[UserCVID],-1)			!= ISNULL(T.[UserCVID],-1)
					OR ISNULL(A.[ManagerCVID],-1)		!= ISNULL(T.[ManagerCVID],-1)
					OR ISNULL(A.[Action],'')			!= ISNULL(T.[Action],'')
					OR ISNULL(A.[Feature],'')			!= ISNULL(T.[Feature],'')
				)
			;
			
			-------------------------------------------
			--	30.	Soft Delete Education
			-------------------------------------------
			UPDATE A
			SET	 isActive		= 0
				,ModifiedBy		= @UserName
			FROM dbo.Auth	A
			JOIN @tvpAuth	T ON A.[AuthId] = T.[AuthId]
			WHERE T.isActive = 0
			;
		
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
		DECLARE	 @ERR_MSG	NVARCHAR(4000) 
				,@ERR_STA	SMALLINT 

		SELECT	 @ERR_MSG	= ERROR_MESSAGE()
				,@ERR_STA	= ERROR_STATE()
 
		SET @ERR_MSG= 'Create FAILED: ' + ISNULL(@ERR_MSG,'');
 
		THROW 50001, @ERR_MSG, @ERR_STA;

		ROLLBACK
	END CATCH
	;
END
;
	
