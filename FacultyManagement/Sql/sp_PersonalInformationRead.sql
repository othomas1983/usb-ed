ALTER PROC [dbo].[sp_PersonalInformationRead]
/******************************************************************************************************
	Synopsis:	Proc will read records for personal information
				PARAMETERS @EmployeeID, @CVID
	
	=================================================================================================
	WHEN		WHO					WHAT
	=================================================================================================
	18/08/2015	J Combrinck(EOHMC)	Created
	19/08/2015	J Combrinck(EOHMC)	Modified to only return Personal Info
	31/08/2015	J Combrinck(EOHMC)	Modified to inclued table ID
	18/09/2015  R Goosen(EOHMC)     Modified to include AffiliationID
	05/10/2015	J Combrinck(EOHMC)	Added MainActivityID and ResearchInterest
	14/10/2015	J Combrinck(EOHMC)	Added HomeInstitution
	03/12/2015	J Combrinck(EOHMC)	Added BusinessUnit Join
	15/02/2016	J Combrinck(EOHMC)	Added Gender and Nationality
	26/08/2016	J Combrinck(EOHMC)	Updated to use Samaccountname as the parameter instead of EmployeID
	12/05/2017	DA Cagnetta(EOHMC)	Added Classification and Sufficiency Join
	30/05/2017	DA Cagnetta(EOHMC)	Added GoogleScholarHIndex
	=================================================================================================
******************************************************************************************************/
 @Samaccountname	VARCHAR(50) 
,@CVID				INT = NULL OUTPUT

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRY
		-----------------------------------------------------
			--	10.	Personal Info Read
		-----------------------------------------------------
		SELECT	 C.Biography
				,C.Title
				,C.Initials
				,C.Name
				,C.GivenName
				,C.Surname
				,C.Gender
				,NT.Nationality
				,NT.NationalityID
				,C.BusinessContactNo
				,C.EmailAddress
				,C.Position
				,AcademicRank						= AR.AcademicRankDescription
				,HQ.HighestQualification
				,HQ.HighestQualificationID
				,C.MainActivityID
				,MA.MainActivity
				,BusinessUnit						= BU.BusinessUnitName
				,C.Expertise
				,C.ResearchInterest
				,C.HomeInstitution
				,C.OrganisationalResponsibility
				,C.CVID
				,C.AffiliationID
				,C.Samaccountname
				,C.ApplicationUserRoleID
				,AU.ApplicationUserRole
				,C.isFaculty
				,C.EmployeeId
				,FC.Classification
				,FS.Sufficiency
				,C.GoogleScholarHIndex
		FROM dbo.CV						C
		LEFT
		JOIN dbo.AcademicRank			AR ON C.AcademicRankID = AR.AcademicRankID
		LEFT
		JOIN dbo.MainActivity			MA ON C.MainActivityID = MA.MainActivityID
		LEFT
		JOIN dbo.ApplicationUserRole	AU ON C.ApplicationUserRoleID = AU.ApplicationUserRoleID
		LEFT 
		JOIN dbo.BusinessUnit			BU ON C.BusinessUnitID = BU.BusinessUnitID
		LEFT
		JOIN dbo.HighestQualification	HQ ON C.HighestQualificationID = HQ.HighestQualificationID
		LEFT 
		JOIN dbo.Nationality			NT ON C.NationalityID = NT.NationalityID
		LEFT 
		JOIN dbo.FacultySufficiency		FS ON C.SufficiencyID = FS.SufficiencyID
		LEFT 
		JOIN dbo.FacultyClassification	FC ON C.ClassificationID = FC.ClassificationID
		-- We have moved away from using EmployeeID as a lookup to Samaccountname. The App now uses samaccount name for lookup and windows authentication
		WHERE C.Samaccountname = @Samaccountname
		;
		
		-----------------------------------------------------
			--	20.	Output CVID
		-----------------------------------------------------
		SELECT @CVID = CVID
		FROM dbo.CV
		WHERE Samaccountname = @Samaccountname
		;
	END TRY
	
	BEGIN CATCH
		DECLARE	 @ERR_MSG	NVARCHAR(4000) 
				,@ERR_STA	SMALLINT 

		SELECT	 @ERR_MSG	= ERROR_MESSAGE()
				,@ERR_STA	= ERROR_STATE()
 
		SET @ERR_MSG= 'Read FAILED: ' + ISNULL(@ERR_MSG,'');
 
		THROW 50001, @ERR_MSG, @ERR_STA;

	END CATCH
	;
END
;
