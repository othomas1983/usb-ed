-- Script update proc 
CREATE PROC [dbo].[sp_ClassificationSufficiencyUpdate] 
  /******************************************************************************************************
    Synopsis:  Proc will update records for CV 
          PARAMETERS @ClassificationID, @SufficiencyID, @CVID, @UserName                 
     
    =================================================================================================
    WHEN    WHO          WHAT 
    =================================================================================================    
	12/05/2017  DA Cagnetta(EOHMC)  Created 
    =================================================================================================
  ******************************************************************************************************/
   @ClassificationID INT
  ,@SufficiencyID	 INT
  ,@CVID			 INT
  ,@UserName	     VARCHAR(50) = 'User Not Provided' 
AS 
  BEGIN 
      SET nocount ON; 

      BEGIN try 
          BEGIN TRANSACTION 

          ------------------------------------------- 
          --  10.  Update records for CV
          ------------------------------------------- 
          UPDATE dbo.CV 
          SET    ClassificationID = @ClassificationID 
				 ,SufficiencyID		= @SufficiencyID				 
                 ,ModifiedBy	= @Username 
                 ,ModifiedDT	= Getdate() 
          WHERE  CVID	= @CVID; 

          COMMIT TRANSACTION 
      END try 

      BEGIN catch 
          DECLARE @ERR_MSG NVARCHAR(4000), 
                  @ERR_STA SMALLINT 

          SELECT @ERR_MSG = ERROR_MESSAGE() 
                 ,@ERR_STA = ERROR_STATE() 

          SET @ERR_MSG= 'Update FAILED: ' + ISNULL(@ERR_MSG, ''); 

          THROW 50001, @ERR_MSG, @ERR_STA; 

          ROLLBACK 
      END catch; 
  END; 

