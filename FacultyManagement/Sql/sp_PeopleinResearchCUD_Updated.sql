
ALTER PROC [dbo].[sp_PeopleinResearchCUD_Updated]
/******************************************************************************************************
	Synopsis:	Proc will create new records PeopleinResearch and update and soft delete the records
				PARAMETERS @tvpPeopleInResearch, @UserName
	
	=================================================================================================
	WHEN		WHO					WHATF
	=================================================================================================
	19/05/2017	DA Cagnetta(EOHMC)		Created : sp_PeopleinResearchCUD but removed unnecessary CV fields
	=================================================================================================
******************************************************************************************************/
 @tvpPeopleInResearch dbo.PeopleinResearchTableUpdate READONLY
,@UserName		 VARCHAR(50) = 'User Not Provided'

AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRY
		BEGIN TRANSACTION
			-------------------------------------------
			--	10.	Create new records for PeopleinResearch
			-------------------------------------------
			INSERT INTO dbo.PeopleinResearch (CVID, PersonRoleID, ResearchID, Credit, ContributorSequence, CreatedBy, ModifiedBy)
			SELECT	 CVID
					,PersonRoleID
					,ResearchID
					,Credit
					,ContributorSequence							
					,@UserName
					,@UserName					
			FROM @tvpPeopleInResearch 
			WHERE PeopleinResearchID IS NULL
			;
			
			-------------------------------------------
			--	20.	Update values in PeopleInResearch table
			-------------------------------------------
			UPDATE P
			SET	 ResearchID			= T.ResearchID
				,CVID				= T.CVID
				,PersonRoleID		= T.PersonRoleID
				,Credit				= T.Credit
				,ContributorSequence= T.ContributorSequence						
				,ModifiedBy 		= @UserName
			FROM dbo.PeopleinResearch	P
			JOIN @tvpPeopleInResearch	T ON P.PeopleinResearchID = T.PeopleinResearchID
			WHERE T.isActive = 1
			AND (
					   ISNULL(P.ResearchID,-1)				!= ISNULL(T.ResearchID,-1)					
				)
			;
			
			-------------------------------------------
			--	30.	Soft Delete PeopleinResearch
			-------------------------------------------
			UPDATE P
			SET	 isActive		= 0
				,ModifiedBy		= @UserName
			FROM dbo.PeopleinResearch	P
			JOIN @tvpPeopleInResearch	T ON P.PeopleinResearchID = T.PeopleinResearchID
			WHERE T.isActive = 0
			;
		
		COMMIT TRANSACTION
	END TRY
	
	BEGIN CATCH
		DECLARE	 @ERR_MSG	NVARCHAR(4000) 
				,@ERR_STA	SMALLINT 

		SELECT	 @ERR_MSG	= ERROR_MESSAGE()
				,@ERR_STA	= ERROR_STATE()
 
		SET @ERR_MSG= 'Create FAILED: ' + ISNULL(@ERR_MSG,'');
 
		THROW 50001, @ERR_MSG, @ERR_STA;

		ROLLBACK
	END CATCH
	;
END
;
