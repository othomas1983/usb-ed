CREATE PROC [dbo].[sp_AuthRead]
/******************************************************************************************************
	Synopsis:	Proc will read records for Auth				
	
	=================================================================================================
	WHEN		WHO					WHAT
	=================================================================================================
	31/05/2017	DA Cagnetta(EOHMC)	Created	
	=================================================================================================
******************************************************************************************************/
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRY
		-----------------------------------------------------
			--	10.	Auth Read
		-----------------------------------------------------
		SELECT	 A.AuthId
				,A.[UserCVID]				
				,A.[ManagerCVID]
				,A.[Action]
				,A.[Feature]
				,A.isActive				
		FROM dbo.Auth A
		WHERE A.isActive = 1
		;
	END TRY
	
	BEGIN CATCH
		DECLARE	 @ERR_MSG	NVARCHAR(4000) 
				,@ERR_STA	SMALLINT 

		SELECT	 @ERR_MSG	= ERROR_MESSAGE()
				,@ERR_STA	= ERROR_STATE()
 
		SET @ERR_MSG= 'Read FAILED: ' + ISNULL(@ERR_MSG,'');
 
		THROW 50001, @ERR_MSG, @ERR_STA;

	END CATCH
	;
END
;