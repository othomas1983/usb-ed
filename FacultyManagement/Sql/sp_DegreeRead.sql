CREATE PROC [dbo].[sp_DegreeRead]
/******************************************************************************************************
	Synopsis:	Proc will read records for Degrees
	
	=================================================================================================
	WHEN		WHO					WHAT
	=================================================================================================
	26/05/2017	DA Cagnetta(EOHMC)	Created
	=================================================================================================
******************************************************************************************************/
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRY
		-----------------------------------------------------
			--	10.	Degree Read
		-----------------------------------------------------
		SELECT	 D.DegreeID
				,D.DegreeDescription
		FROM dbo.Degree D
		WHERE  D.isActive = 1
		;
	END TRY
	
	BEGIN CATCH
		DECLARE	 @ERR_MSG	NVARCHAR(4000) 
				,@ERR_STA	SMALLINT 

		SELECT	 @ERR_MSG	= ERROR_MESSAGE()
				,@ERR_STA	= ERROR_STATE()
 
		SET @ERR_MSG= 'Read FAILED: ' + ISNULL(@ERR_MSG,'');
 
		THROW 50001, @ERR_MSG, @ERR_STA;

	END CATCH
	;
END
;
