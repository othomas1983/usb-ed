﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace FacultyManagement.Core.Attributes
{
    /// <summary>
    /// Validates that the property is an image
    /// </summary>
    /// <seealso cref="System.ComponentModel.DataAnnotations.ValidationAttribute" />
    public class ImageAttribute : ValidationAttribute
    {
        public ImageAttribute() : base( "Please select valid jpg, png, gif or jpeg image." ) { }

        protected override ValidationResult IsValid( object value, ValidationContext validationContext )
        {
            // Because image is not required
            if ( value == null ) return ValidationResult.Success;

            if ( value is HttpPostedFileBase )
            {
                /* var fileName = ((HttpPostedFileBase) value).FileName;
                 return _formats.Any( item => fileName.EndsWith( item, StringComparison.OrdinalIgnoreCase ) );*/
                var contentType = ((HttpPostedFileBase) value).ContentType;
                if ( contentType.Contains( "Image" ) )
                {
                    return ValidationResult.Success;
                }
                ;
            }

            return new ValidationResult( ErrorMessage );
        }
    }
}
