﻿using System;
using System.Collections.Generic;
using System.Runtime.Caching;
using FacultyManagement.Core.ViewModels.Interfaces;
using FacultyManagement.Domain.Common;

namespace FacultyManagement.Core.Cache
{
    /// <summary>
    /// Cache implementations for Models
    /// </summary>
    public class ModelCache : CacheBase
    {
        private static string CacheKey( Type type, int id )
        {
            return $"FM:Model:{type.Name}:{id}";
        }

        /// <summary>
        /// Gets the existing or a new model from cache for the specific user
        /// </summary>
        /// <typeparam name="T">The type of the t.</typeparam>
        /// <param name="cVid"></param>
        /// <param name="valueFactory">The value factory.</param>
        /// <returns></returns>
        public static List<T> Read<T>( int cVid, Func<List<T>> valueFactory ) where T : DataRowModel
        {
            var key = CacheKey( typeof( T ), cVid );

            var cacheItemPolicy = new CacheItemPolicy
            {
                AbsoluteExpiration = DateTime.Now.AddMinutes( 30 )
            };

            return GetOrAddExisting( key, valueFactory, cacheItemPolicy );
        }

        public static T Read<T>( int cVid, Func<T> valueFactory ) where T : DataSetModel
        {
            var key = CacheKey( typeof( T ), cVid );

            var cacheItemPolicy = new CacheItemPolicy
            {
                AbsoluteExpiration = DateTime.Now.AddMinutes( 30 )
            };

            return GetOrAddExisting( key, valueFactory, cacheItemPolicy );
        }

        /// <summary>
        /// Removes the specified view model from cache.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cVid">The c vid.</param>
        public static void Flush<T>( int cVid )
        {
            var key = CacheKey( typeof( T ), cVid );

            if ( CacheContainsKey( key ) )
            {
                FlushCache( key );
            }
        }


    }
}
