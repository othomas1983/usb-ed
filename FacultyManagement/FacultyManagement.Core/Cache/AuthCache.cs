﻿using System;
using System.Collections.Generic;
using System.Runtime.Caching;
using FacultyManagement.Core.ViewModels.Interfaces;
using FacultyManagement.Data.Interfaces;
using FacultyManagement.Data.Services;
using FacultyManagement.Domain.Common;
using FacultyManagement.Domain.Factories;
using FacultyManagement.Domain.Model.Admin;
using NLog;

namespace FacultyManagement.Core.Cache
{
    /// <summary>
    /// Cache implementations for Auths
    /// </summary>
    public class AuthCache : CacheBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private static readonly IAuthDbService DbService = new AuthDbService();

        private static string CacheKey()
        {
            return $"FM:Auth";
        }

        /// <summary>
        /// Gets the existing or a new model from cache for the specific user
        /// </summary>
        /// <typeparam name="T">The type of the t.</typeparam>
        /// <param name="cVid"></param>
        /// <param name="valueFactory">The value factory.</param>
        /// <returns></returns>
        public static List<Auth> Read( ) 
        {
            FmMemoryCache cache = FmMemoryCache.Default;
            object _lock = new object();

            var key = CacheKey();

            // Try to get the object from the cache
            var value = cache[key] as List<Auth>;

            // Check whether the value exists
            if ( value == null )
            {
                // Try to get the object from the cache again
                value = cache[key] as List<Auth>;
                // Double-check that another thread did not call the DB already and load the cache
                if ( value == null )
                {
                    // Get from the DB
                    lock ( _lock )
                    {
                        value = ModelFactory.CreateList<Auth>( DbService.GetAuths() );
                        // Add to the cache
                        cache.Set( key, value, new CacheItemPolicy() );

                        Logger.Info( $"Auths Loaded from Database, Items:{value.Count}" );
                    }
                }
            }

            return value;
        }
        
        /// <summary>
        /// Removes the specified auths from cache.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cVid">The c vid.</param>
        public static void Flush( )
        {
            var key = CacheKey( );

            if ( CacheContainsKey( key ) )
            {
                FlushCache( key );
            }
        }

        public static void Reload()
        {
            var key = CacheKey( );

            if ( CacheContainsKey( key ) )
            {
                FlushCache( key );
                Read();
            }
        }
        
    }
}
