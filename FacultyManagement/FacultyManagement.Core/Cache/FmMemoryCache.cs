﻿using System.Collections.Specialized;
using System.Runtime.Caching;

namespace FacultyManagement.Core.Cache
{
    public class FmMemoryCache : MemoryCache
    {
        // singleton instance of FmMemoryCache
        private static FmMemoryCache _cache;
        private static readonly object Lock = new object();

        /// <summary>
        /// Gets the default.
        /// </summary>
        /// <value>
        /// The default.
        /// </value>
        public new static FmMemoryCache Default
        {
            get
            {
                if ( _cache == null )
                {
                    lock ( Lock )
                    {
                        if ( _cache == null )
                        {
                            _cache = new FmMemoryCache();
                        }
                    }
                }

                return _cache;
            }
        }


        private FmMemoryCache() : this ("FmMemoryCache") { }

        private FmMemoryCache( string name, NameValueCollection config = null ) : base( name, config )
        {
        }

    }
}
