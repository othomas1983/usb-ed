﻿using System.Collections.Generic;
using System.Runtime.Caching;
using FacultyManagement.Data.Interfaces;
using FacultyManagement.Data.Services;
using FacultyManagement.Domain.Common;
using FacultyManagement.Domain.Factories;
using FacultyManagement.Domain.Model.Enums;
using FacultyManagement.Infrastructure.Utilities.ExtensionMethods;
using NLog;

namespace FacultyManagement.Core.Cache
{
    /// <summary>
    /// Cache implementations for dropdowns
    /// </summary>
    public class DropDownsCache
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private static readonly IDropDownsDbService DbService = new DropDownsDbService();

        private static string CacheKey( DropDownType type )
        {
            return $"FM:DropDown:{type.ConvertToString()}";
        }
        /// <summary>
        /// Adds or get the existing drop down list data from the cache.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        public static List<T> Read<T>( DropDownType type ) where T : IDataRowModel, new()
        {
            FmMemoryCache cache = FmMemoryCache.Default;
            object _lock = new object();

            var key = CacheKey( type );
            List<T> value = new List<T>();
            lock (_lock)
            {
                value = ModelFactory.CreateList<T>(DbService.GetData(type));
                // Add to the cache
               // cache.Set(key, value, new CacheItemPolicy());

               // Logger.Info($"{type.ToStringSafe()} Loaded from Database, Items:{value.Count}");
            }
            // Try to get the object from the cache
            //var value = cache[key] as List<T>;

            //// Check whether the value exists
            //if ( value == null )
            //{
            //    // Try to get the object from the cache again
            //    value = cache[key] as List<T>;
            //    // Double-check that another thread did not call the DB already and load the cache
            //    if ( value == null )
            //    {
            //        // Get from the DB
            //        lock ( _lock )
            //        {
            //            value = ModelFactory.CreateList<T>( DbService.GetData( type ) );
            //            // Add to the cache
            //            cache.Set( key, value, new CacheItemPolicy() );

            //            Logger.Info( $"{type.ToStringSafe()} Loaded from Database, Items:{value.Count}" );
            //        }
            //    }
            //}

            return value;
        }
    }
}
