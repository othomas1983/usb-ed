﻿namespace FacultyManagement.Core.Security.Identity
{
    /// <summary>
    /// Used to abstract away the  user
    /// </summary>
    public interface IUser<T> where T : class
    {
        // UserLogin UserLogin { get; }

        /// <summary>
        /// Gets the username.
        /// </summary>
        /// <value>
        /// The username.
        /// </value>
        string Username { get; }

        /// <summary>
        /// Gets the cvid.
        /// </summary>
        /// <value>
        /// The c vid.
        /// </value>
        int CVid { get; }
    }
}
