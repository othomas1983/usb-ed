﻿using System;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;

namespace FacultyManagement.Core.Security.Authentication
{
    public static class CustomAuthentication
    {
        public const String ApplicationCookie = "FacultyManagementAuthenticationType";
    }

    public class AuthSetup
    {
        public static void ConfigureAuth( IAppBuilder app )
        {
            // need to add UserManager into owin, because this is used in cookie invalidation
            app.UseCookieAuthentication( new CookieAuthenticationOptions
            {
                AuthenticationType = CustomAuthentication.ApplicationCookie,
                LoginPath = new PathString( "/Account/Login" ),
                Provider = new CookieAuthenticationProvider(),
                CookieName = ".ADAuthCookie",
                CookieHttpOnly = true,
                SlidingExpiration = true,
                ExpireTimeSpan = TimeSpan.FromHours( 12 )
            } );
        }
    }
}
