using System.Collections.Generic;
using System.Security.Principal;
using FacultyManagement.Domain.Model;
using FacultyManagement.Domain.Model.Admin;

namespace FacultyManagement.Core.Security.Authentication
{
    public interface IAuthentication
    {
        /// <summary>
        /// Authenticates the specified user.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        bool Authenticate( string username, string password, out string errorMessage );

        /// <summary>
        /// Changes the password.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="oldPassword">The old password.</param>
        /// <param name="newPassword">The new password.</param>
        /// <param name="warningMessage">The warning message.</param>
        /// <returns></returns>
        bool ChangePassword( UserLogin user, string oldPassword, string newPassword, out string warningMessage );

        /// <summary>
        /// Creates a principal.
        /// </summary>
        /// <param name="cVid">The c vid.</param>
        /// <param name="username">The username.</param>
        /// <returns></returns>
        IPrincipal CreatePrincipal( string cVid, string username );      
    }
}