﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacultyManagement.Infrastructure.Utilities
{
    public class AutoCompleteResult
    {
        // must match jQuery 
        public string id { get; set; }
        public string value { get; set; }
        public string label { get; set; }
    }
}
