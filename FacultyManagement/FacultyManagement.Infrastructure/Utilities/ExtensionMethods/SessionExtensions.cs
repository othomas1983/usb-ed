﻿using System.Web;

namespace FacultyManagement.Infrastructure.Utilities.ExtensionMethods
{
    /// <summary>
    /// Session Object Management Extensions
    /// </summary>
    public static class SessionExtensions
    {
        /// <summary>
        /// Gets the data from session.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="session">The session.</param>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public static T GetDataFromSession<T>( this HttpSessionStateBase session, string key )
        {
            return (T) session[key];
        }

        /// <summary>
        /// Sets the data in session.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="session">The session.</param>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        public static void SetDataInSession<T>( this HttpSessionStateBase session, string key, object value )
        {
            session[key] = value;
        }
    }
}
