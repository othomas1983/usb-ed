﻿using System;

namespace FacultyManagement.Infrastructure.Utilities.ExtensionMethods
{
    /// <summary>
    /// Boolean Extensions
    /// </summary>
    public static partial class ExtensionMethods
    {
        #region Boolean Extensions

        /// <summary>
        /// Returns a numeric 1 (if true) or 0 (if false).
        /// </summary>
        /// <param name="field"></param>
        /// <returns></returns>
        public static int Bit( this Boolean field )
        {
            return field ? 1 : 0;
        }

        /// <summary>
        /// Returns either "Yes" or "No".
        /// </summary>
        /// <param name="value">if set to <c>true</c> [value].</param>
        /// <returns></returns>
        public static string ToYesNo( this bool value )
        {
            return value ? "Yes" : "No";
        }

        public static string ToYesNo( this int value )
        {
            return value == 1 ? "Yes" : "No";
        }

        /// <summary>
        /// Returns the string "True" or "False".
        /// </summary>
        /// <param name="value">if set to <c>true</c> [value].</param>
        /// <returns></returns>
        public static string ToTrueFalse( this bool value )
        {
            return value ? "True" : "False";
        }

        /// <summary>
        /// Returns either "Activated" or "Deactivated".
        /// </summary>
        /// <param name="value">if set to <c>true</c> [value].</param>
        /// <returns></returns>
        public static string ToActivatedDeactivated( this bool value )
        {
            return value ? "Activated" : "Deactivated";
        }


        /// <summary>
        ///  Returns a bool for numeric 1 (if true) or 0 (if false).
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static bool AsBoolean( this int value )
        {
            return value == 1;
        }
        #endregion Boolean Extensions
    }
}
