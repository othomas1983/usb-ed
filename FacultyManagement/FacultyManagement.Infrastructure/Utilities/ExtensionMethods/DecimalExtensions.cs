﻿using System;

namespace FacultyManagement.Infrastructure.Utilities.ExtensionMethods
{
    /// <summary>
    ///
    /// </summary>
    public static partial class ExtensionMethods
    {
        /// <summary>
        /// Returns the number of digits following the decimal place. 5.68 would return 2. 17.9998 would return 4.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static int GetDecimalPrecision( this decimal value )
        {
            return BitConverter.GetBytes( decimal.GetBits( value )[3] )[2];
        }

        /// <summary>
        /// Returns the floor (round down) value with the given decimal precision
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="precision">The precision.</param>
        /// <returns></returns>
        public static decimal? Floor( this decimal? value, int precision = 0 )
        {
            if ( !value.HasValue )
            {
                return null;
            }

            var shiftFactor = Convert.ToDecimal( Math.Pow( 10, precision ) );
            var shiftedValue = value.Value * shiftFactor;
            var shiftedFloor = Math.Floor( shiftedValue );
            var unshiftedFloor = shiftedFloor / shiftFactor;
            return unshiftedFloor;
        }

        /// <summary>
        /// Rounds the specified value to desired decimal places.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="precision"></param>
        /// <returns></returns>
        public  static  decimal RoundDecimal( this decimal value, int precision = 0 )
        {
            return Math.Round( value, precision );
        }
    }
}