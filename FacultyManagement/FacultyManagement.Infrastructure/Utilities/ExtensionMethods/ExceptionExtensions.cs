﻿using System;
using System.Collections.Generic;

namespace FacultyManagement.Infrastructure.Utilities.ExtensionMethods
{
    /// <summary>
    /// Exception Extensions
    /// </summary>
    public static partial class ExtensionMethods
    {
        #region Exception Extensions

        /// <summary>
        /// Gets the exception's message and the messages for all internal exceptions.
        /// </summary>
        /// <param name="ex">The ex.</param>
        /// <returns></returns>
        public static List<string> Messages( this Exception ex )
        {
            var messages = new List<string>();

            var exception = ex;
            while ( exception != null )
            {
                messages.Add( exception.Message );
                exception = exception.InnerException;
            }

            return messages;
        }

        #endregion
    }
}
