﻿namespace FacultyManagement.Infrastructure
{
    public class Messages
    {
        public const string LoginError = "There was a problem logging you in.";
        public const string UserNotExists = "Your account was correctly authenticated but does not exist on Faculty Management. Please contact your administrator.";
        public const string SavingSuccess = "Your changes were successfully saved.";
        public const string SavingError = "There was a problem saving your changes.";
        public const string DeleteSuccess = "Your data was successfully deleted.";
        public const string DeleteError = "There was a problem deleting your data.";
        public const string DbConnectionError = "There was a problem connecting to the database.";
        public const string NoDataError = "There was no data returned.";
    }

    public class FmDataTypes
    {
        public const string DegreeId = "DegreeId";
        public const string ProgrammeOfferingId = "ProgrammeOfferingId";
        public const string ProgressStatus = "ProgressStatus";
        
        public const string PositiveNumber = "PositiveNumber";
        public const string ProgrammeId = "ProgrammeId";


        public const string FacultyUsersPickerId = "FacultyUsersPickerId";
        public const string YearDatePickerLoad = "YearDatePickerLoad";


        public const string StringTextBoxReadOnly = "StringTextBoxReadOnly";

        public const string AffiliationId = "AffiliationId";
        public const string HighestQualificationId = "HighestQualificationId";
        public const string MainActivityId = "MainActivityId";
        public const string AcademicRank = "AcademicRank";
        public const string NationalityId = "NationalityId";
        public const string ApplicationUserRoleId = "ApplicationUserRoleId";
        public const string BusinessUnit = "BusinessUnit";

        public const string ProgrammeSize = "ProgrammeSize";
        public const string SupervisionResearch = "SupervisionResearch";
        public const string Category = "Category";


    }

    public class ADGroups
    {
        public const string Admin = "FM Admin";
        public const string Manager = "FM Manager";
    }

    public class Authentication
    {
        public const string VIEW = "View";
        public const string EDIT = "Edit";
    }

    public class Feature
    {
        public const string PERSONAL_INFORMATION = "Personal Information";        
    }
}
