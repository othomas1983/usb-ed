﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacultyManagement.Infrastructure.Sharepoint
{
    public interface ISharepointWebService
    {
        /// <summary>
        /// Uploads the research.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="cvId">The cv identifier.</param>
        /// <param name="publicationDate"></param>
        /// <param name="fileData">The file data.</param>
        /// <returns></returns>        
        SharepointUploadResult UploadResearch( string fileName, int cvId, DateTime publicationDate, byte[] fileData );
    }
}
