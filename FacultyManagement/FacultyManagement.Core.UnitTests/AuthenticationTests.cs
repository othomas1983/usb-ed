﻿using System.Collections.Generic;
using System.Security.Claims;
using FacultyManagement.Core.Security.Authentication;
using FacultyManagement.Data.Interfaces;
using FacultyManagement.Data.Services;
using FacultyManagement.Infrastructure;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using NUnit.Framework;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using FacultyManagement.Domain.Model;
using FacultyManagement.Domain.Model.Common;
using FacultyManagement.Infrastructure.Utilities.ExtensionMethods;

namespace FacultyManagement.Core.IntegrationTests
{
    [TestFixture]
    public class AuthenticationTests
    {
        public IAuthenticationManager AuthenticationManager { get; set; } = new OwinContext().Authentication;
        public string Username { get; set; } = @"ultra\rog";
        public string Password { get; set; } = "81118599";
        public IAuthDbService AuthDbService { get; set; } = new AuthDbService();

        [Test]
        public void Can_Authenticate_With_Local_Machine_Credentials()
        {
            // Arrange
            var authenticator = new ActiveDirectory( AuthenticationManager, AuthDbService );
            // Act
            string errorMessage;
            bool authenticated = authenticator.Authenticate( Username, Password, out errorMessage );
            // Assert
            Assert.IsTrue( authenticated );
        }

        [Test]
        public void Can_Authenticate_With_Active_Directory_Credentials()
        {
            // Arrange
            Username = @"Belpark\Airb04";
            Password = "BPCb0rn3";

            var authenticator = new ActiveDirectory( AuthenticationManager, AuthDbService );
            authenticator.Server = "bpcad01.belpark.sun.ac.za";
            // Act
            string errorMessage;
            bool authenticated = authenticator.Authenticate( Username, Password, out errorMessage );
            // Assert
            Assert.IsTrue( authenticated );
            Assert.IsFalse( errorMessage.Equals( Messages.LoginError ) );
        }

        /// <summary>
        /// Must have a correct AD account which doesnt exist in Faculty Management for this test to pass 
        /// </summary>
        [Test]
        public void Should_Authenticate_With_Active_Directory_Credential_But_Not_With_FM_As_User_Doesnt_Exist()
        {
            // Arrange
            Username = @"Belpark\Airb04";
            Password = "BPCb0rn3";

            var authenticator = new ActiveDirectory( AuthenticationManager, AuthDbService );
            authenticator.Server = "bpcad01.belpark.sun.ac.za";
            // Act
            string errorMessage;
            bool authenticated = authenticator.Authenticate( Username, Password, out errorMessage );
            // Assert
            Assert.IsFalse( authenticated );
            Assert.IsTrue( errorMessage.Equals( Messages.UserNotExists ) );
        }


        [Test]
        public void Should_Create_Principal_With_Claims_Identity()
        {
            // Arrange
            var authenticator = new ActiveDirectory( AuthenticationManager, AuthDbService );
            // Act
            string errorMessage;
            bool authenticated = authenticator.Authenticate( Username, Password, out errorMessage );
            // Assert
            Assert.IsTrue( authenticated );
            Assert.IsNotNull( AuthenticationManager.AuthenticationResponseGrant.Principal );
            Assert.IsNotNull( AuthenticationManager.AuthenticationResponseGrant.Principal.FindFirst( ClaimTypes.Name ) );
        }

        [Test]
        public void Should_Load_Active_Directory_Users()
        {
            // Arrange
            string username = "Airb04";
            string password = "BPCb0rn3";
            string server = "bpcad01.belpark.sun.ac.za";
            const string container = @"OU=BELPARK Staff, DC=belpark,DC=sun,DC=ac,DC=za";
            var principalContext = new PrincipalContext( ContextType.Domain, server, container, username, password );

            List<ActiveDirectoryUser> ADUsers = new List<ActiveDirectoryUser>();

            // Act
            using ( var searcher = new PrincipalSearcher( new UserPrincipal( principalContext ) ) )
            {
                foreach ( var user in searcher.FindAll().WherePrinicpal().OrderBy( u => u.DisplayName ) )
                {
                    if ( !string.IsNullOrEmpty( user.EmployeeId ) && !string.IsNullOrEmpty( user.GivenName ) &&
                         !string.IsNullOrEmpty( user.DisplayName ) )
                    {
                        ADUsers.Add( new ActiveDirectoryUser
                        {
                            Description = user.Description,
                            DisplayName = user.Surname + " " + user.GivenName,
                            EmailAddress = user.EmailAddress,
                            EmployeeId = user.EmployeeId,
                            //GivenName = user.GivenName,
                            Surname = user.Surname,
                            VoiceTelephoneNumber = user.VoiceTelephoneNumber,
                            SamAccountName = user.SamAccountName

                        } );
                    }
                }
                // Assert
                Assert.IsTrue( ADUsers.Count > 0 );
            }

        }
    }
}
