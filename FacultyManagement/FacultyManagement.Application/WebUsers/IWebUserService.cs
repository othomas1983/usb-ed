using FacultyManagement.Application.Common;
using FacultyManagement.Application.WebUsers.ViewModels;
using FacultyManagement.Core.Security.Identity;
using FacultyManagement.Domain.Model.Common;
using System.Collections.Generic;

namespace FacultyManagement.Application.WebUsers
{
    public interface IWebUserService
    {
        /// <summary>
        /// Gets all Web Users
        /// </summary>      
        /// <param name="currentUser">currentUser</param>
        /// <returns></returns>
        WebUserVm GetViewModel(IUser<CurrentUser> currentUser);


        IEnumerable<WebUser> GetWebUsers(int cvId, bool displayOnWeb);

        void UpdateUserDisplayOnWebOn(IEnumerable<int> cvIds);


        void UpdateUserDisplayOnWebOff(IEnumerable<int> cvIds);


    }
}
