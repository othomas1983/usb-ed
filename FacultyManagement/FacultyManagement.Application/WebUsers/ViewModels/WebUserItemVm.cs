﻿using FacultyManagement.Core.ViewModels.Interfaces;


namespace FacultyManagement.Application.WebUsers.ViewModels
{
    public class WebUserItemVm : IViewModel
    {
        #region Properties

        public bool? WebDisplay { get; set; }
        public int CVID { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }


        #endregion  
    }
}
