﻿using System;

namespace FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels
{
    public class ExperienceVm
    {
        #region Properties

        public int Id { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int ExperienceCategoryId { get; set; }
        public string Subject { get; set; }
        public string Position { get; set; }
        public string Institution { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public short NoOfDays { get;  set; }
        public string Programme { get;  set; }
        public string KnowledgeArea { get;  set; }
        public string Client { get;  set; }
        public int IsCurrent { get; set; }

        public string Publication { get; set; }
        public string PublicationStatus { get; set; }
        public int? PublicationEdition { get; set; }
        #endregion
    }
}
