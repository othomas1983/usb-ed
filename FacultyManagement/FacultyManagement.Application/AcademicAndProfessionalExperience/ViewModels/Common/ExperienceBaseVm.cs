﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.Common
{
    public class ExperienceBaseVm : IValidatableObject
    {
        #region Properties
        [Required]
        public DateTime StartDate { get; set; }
        
        public DateTime? EndDate { get; set; }
        [Required]
        public string Subject { get; set; }
        [DisplayName( "Current" )]
        public bool IsCurrent { get; set; } = false;

        [HiddenInput]
        public int? CVid { get; set; } 
        #endregion
        /// <summary>
        /// Determines whether the model is valid.
        /// </summary>
        /// <param name="validationContext">The validation context.</param>
        /// <returns>
        /// A collection that holds failed-validation information.
        /// </returns>
        public IEnumerable<ValidationResult> Validate( ValidationContext validationContext )
        {
            var validationResults = new List<ValidationResult>();

            if ( (!IsCurrent && !this.EndDate.HasValue) || ( this.EndDate < this.StartDate) )
            {
                validationResults.Add( new ValidationResult( "End date must be greater than start date", new[] { "StartDate" } ) );
            }

            return validationResults;
        }
    }
}
