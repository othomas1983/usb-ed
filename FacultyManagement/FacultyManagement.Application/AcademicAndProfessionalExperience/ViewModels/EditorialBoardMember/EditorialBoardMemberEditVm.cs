﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using DataAnnotationsExtensions;
using FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.Common;
using FacultyManagement.Application.Common;

namespace FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.EditorialBoardMember
{
    public class EditorialBoardMemberEditVm : ExperienceBaseVm, ICreateEditVm
    {
        #region Properties

        public int Id { get; set; }

        [HiddenInput]
        public int ExperienceCategoryId { get; set; }

        [Required]
        public string Publication { get; set; }

        [DataType( "PublicationStatus" ),  Required]
        public string PublicationStatus { get; set; }

        [DataType("PositiveNumber"), Range(1, 1000, ErrorMessage = "{0} must be 1 or more."), Required]
        public int PublicationEdition { get; set; } = 1;

        public int IsActive { get; set; }


        #endregion

        public EditorialBoardMemberEditVm( int cVid )
        {
            CVid = cVid;
        }

        public EditorialBoardMemberEditVm()
        {
        }
    }
}
