﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using DataAnnotationsExtensions;
using FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.Common;
using FacultyManagement.Application.Common;
using FacultyManagement.Core.Cache;
using FacultyManagement.Domain.Model.Common;
using FacultyManagement.Domain.Model.Enums;
using FacultyManagement.Infrastructure.Utilities.ExtensionMethods;

namespace FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.EditorialBoardMember
{
    public class EditorialBoardMemberCreateVm : ExperienceBaseVm, ICreateEditVm
    {
        #region Properties

        public int? Id { get; set; } = null;

        public int IsActive { get; set; } = 1;

        [HiddenInput]
        public int ExperienceCategoryId { get; set; } = ExperienceCategory.EditorialBoardmembers.ConvertToInt();

        [Required]
        public string Publication { get; set; }

        [DataType( "PublicationStatus" ), Required]
        public string PublicationStatus { get; set; }

       [DataType("PositiveNumber"), Range(1, 1000, ErrorMessage = "{0} must be 1 or more."), Required]
        public int PublicationEdition { get; set; }// = 1;
       

        #endregion

        public EditorialBoardMemberCreateVm( int cVid )
        {
            StartDate = DateTime.UtcNow.AddDays( -1 );
            EndDate = DateTime.UtcNow;
            CVid = cVid;
        }

        public EditorialBoardMemberCreateVm()
        {
        }
    }
}
