﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.EditorialBoardMember
{
    public class EditorialBoardMemberVm
    {
        #region Properties

        public int Id { get; set; }
        public int IsActive { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int ExperienceCategoryId { get; set; }
        public string Subject { get; set; }
        public string IsCurrent { get; set; }


        public string Publication { get; set; }
        public string PublicationStatus { get; set; }
        public int? PublicationEdition { get; set; }

        #endregion
    }
}
