﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using DataAnnotationsExtensions;
using FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.Common;
using FacultyManagement.Application.Common;
using System;

namespace FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.PartTimeConsulting
{
    public class PartTimeConsultingEditVm : ExperienceBaseVm, ICreateEditVm
    {
        #region Properties

        public int Id { get; set; } 
        [HiddenInput]
        public int ExperienceCategoryId { get; set; }

        [DataType("PositiveNumber"), Range(1, 9999, ErrorMessage = "{0} must be 1 or more."), Required]
        public int NoOfDays { get; set; } = 1;

        [Required]
        public string Client { get; set; }
    
        public int IsActive { get; set; }

        #endregion

        public PartTimeConsultingEditVm( int cVid )
        {
            CVid = cVid;
        }

        public PartTimeConsultingEditVm()
        {
        }   
    }
}
