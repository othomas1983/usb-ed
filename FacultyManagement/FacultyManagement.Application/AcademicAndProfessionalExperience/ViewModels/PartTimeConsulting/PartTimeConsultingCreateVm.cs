﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using DataAnnotationsExtensions;
using FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.Common;
using FacultyManagement.Application.Common;
using FacultyManagement.Domain.Model.Enums;
using FacultyManagement.Infrastructure.Utilities.ExtensionMethods;

namespace FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.PartTimeConsulting
{
    public class PartTimeConsultingCreateVm : ExperienceBaseVm,  ICreateEditVm
    {
        #region Properties

        public int? Id { get; set; } = null;
       
        [HiddenInput]
        public int ExperienceCategoryId { get; set; } = ExperienceCategory.PartTime.ConvertToInt();

        [DataType("PositiveNumber"), Range(1, 9999, ErrorMessage = "{0} must be 1 or more."), Required]
        public int NoOfDays { get; set; } = 1;

        [Required]
        public string Client { get; set; }

        [DisplayName( "Current" )]
        
        public int IsActive { get; set; } = 1;

        #endregion

        public PartTimeConsultingCreateVm( int cVid )
        {
            StartDate = DateTime.UtcNow.AddDays( -1 );
            EndDate = DateTime.UtcNow;
            CVid = cVid;
        }

        public PartTimeConsultingCreateVm()
        {
        }
        
    }
}
