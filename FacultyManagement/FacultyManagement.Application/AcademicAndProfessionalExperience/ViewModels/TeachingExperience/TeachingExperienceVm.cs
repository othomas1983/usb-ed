﻿using System;

namespace FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.TeachingExperience
{
    public class TeachingExperienceVm
    {
        #region Properties
        public int Id { get; set; }
        public DateTime StartDate { get;  set; }
        public DateTime? EndDate { get;  set; }
        public int ExperienceCategoryId { get;  set; }
        public string Subject { get;  set; }
        public string Position { get;  set; }
        public string Institution { get;  set; }
        public string City { get;  set; }
        public string Country { get;  set; }
        public string IsCurrent { get;  set; }
        public int IsActive { get; set; }
        #endregion
    }
}
