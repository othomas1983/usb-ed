﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.Common;
using FacultyManagement.Application.Common;
using FacultyManagement.Core.Cache;
using FacultyManagement.Domain.Model.Common;
using FacultyManagement.Domain.Model.Enums;
using FacultyManagement.Infrastructure.Utilities.ExtensionMethods;

namespace FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.TeachingExperience
{
    public class TeachingExperienceCreateVm : ExperienceBaseVm,  ICreateEditVm
    {
        #region Properties

        public int? Id { get; set; } = null;
       
        [HiddenInput]
        public int ExperienceCategoryId { get; set; } = ExperienceCategory.Teaching.ConvertToInt();

        [Required]
        public string Position { get; set; }

        [Required]
        public string Institution { get; set; }

        [Required]
        public string City { get; set; }

        [DataType( "NationalityId" ), DisplayName( "Country" ), Required]
        public int? CountryId { get; set; }

        /// <summary>
        /// Gets the country using the lookup table and countryId.
        /// </summary>
        /// <value>
        /// The country.
        /// </value>
        public string Country
        {
            get
            {
                if ( !CountryId.HasValue ) return string.Empty;

                var data = DropDownsCache.Read<Nationality>( DropDownType.Nationalities );
                return data.FirstOrDefault( x => x.Id == CountryId.Value )?.Description;
            }
        }
      
        public int IsActive { get; set; } = 1;

        #endregion

        public TeachingExperienceCreateVm( int cVid )
        {
            StartDate = DateTime.UtcNow.AddDays( -1 );
            EndDate = DateTime.UtcNow;
            CVid = cVid;
        }

        public TeachingExperienceCreateVm()
        {
        }
        
    }
}
