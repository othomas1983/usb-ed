﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.Common;
using FacultyManagement.Application.Common;
using FacultyManagement.Core.Cache;
using FacultyManagement.Domain.Model.Common;
using FacultyManagement.Domain.Model.Enums;


namespace FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.TeachingExperience
{
    public class TeachingExperienceEditVm : ExperienceBaseVm, ICreateEditVm
    {
        #region Properties

        public int Id { get; set; } 
        [HiddenInput]
        public int ExperienceCategoryId { get; set; }
        [Required]
        public string Position { get; set; }
        [Required]
        public string Institution { get; set; }
        [Required]
        public string City { get; set; }

        /// <summary>
        /// Gets the countryId using the lookup table and country text.
        /// </summary>
        /// <value>
        /// The country.
        /// </value>
        [DataType( "NationalityId" ), DisplayName( "Country" ), Required]
        public int? CountryId
        {
            get
            {
                if ( string.IsNullOrEmpty( _country ) ) return _countryId;

                var data = DropDownsCache.Read<Nationality>( DropDownType.Nationalities );
                return data.FirstOrDefault( x => x.Description == Country )?.Id;
            }
            set => _countryId = value;
        }
        private int? _countryId;

        /// <summary>
        /// Gets the country using the lookup table and countryId.
        /// </summary>
        /// <value>
        /// The country.
        /// </value>
        public string Country
        {
            get
            {
                if ( _countryId.HasValue )
                {
                    var data = DropDownsCache.Read<Nationality>( DropDownType.Nationalities );
                    return data.FirstOrDefault( x => x.Id == CountryId.Value )?.Description;
                }
                return _country;
            }
            set => _country = value;
        }
        private string _country;
        
        public int IsActive { get; set; }


        #endregion

        public TeachingExperienceEditVm( int cVid )
        {
            CVid = cVid;
        }

        public TeachingExperienceEditVm()
        {
        }   
    }
}
