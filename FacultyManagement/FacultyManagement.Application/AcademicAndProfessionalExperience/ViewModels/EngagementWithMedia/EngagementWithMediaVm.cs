﻿using System;

namespace FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.EngagementWithMedia
{
    public class EngagementWithMediaVm
    {
        #region Properties

        public int Id { get; set; }
        public int IsActive { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int ExperienceCategoryId { get; set; }
        public string Subject { get; set; }
        public int NoOfDays { get; set; }
        public string Client { get; set; }
        public string IsCurrent { get; set; }

        #endregion
    }
}
