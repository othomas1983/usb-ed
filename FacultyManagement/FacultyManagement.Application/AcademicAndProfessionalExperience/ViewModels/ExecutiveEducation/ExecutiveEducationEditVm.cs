﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using DataAnnotationsExtensions;
using FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.Common;
using FacultyManagement.Application.Common;
using FacultyManagement.Core.Cache;
using FacultyManagement.Domain.Model.Common;
using FacultyManagement.Domain.Model.Enums;

namespace FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.ExecutiveEducation
{
    public class ExecutiveEducationEditVm : ExperienceBaseVm, ICreateEditVm
    {
        #region Properties

        public int Id { get; set; }

        [HiddenInput]
        public int ExperienceCategoryId { get; set; }

        [DataType("PositiveNumber"), Range(1, 9999, ErrorMessage = "{0} must be 1 or more."), Required]
        public int NoOfDays { get; set; } = 1;

        [Required]
        public string Programme { get; set; }

        /// <summary>
        /// Gets the countryId using the lookup table and country text.
        /// </summary>
        /// <value>
        /// The country.
        /// </value>
        [DataType( "NationalityId" ), DisplayName( "Country" ), Required]
        public int? CountryId
        {
            get
            {
                if ( string.IsNullOrEmpty( _country ) ) return _countryId;

                var data = DropDownsCache.Read<Nationality>( DropDownType.Nationalities );
                return data.FirstOrDefault( x => x.Description == Country )?.Id;
            }
            set => _countryId = value;
        }
        private int? _countryId;

        /// <summary>
        /// Gets the country using the lookup table and countryId.
        /// </summary>
        /// <value>
        /// The country.
        /// </value>
        public string Country
        {
            get
            {
                if ( _countryId.HasValue )
                {
                    var data = DropDownsCache.Read<Nationality>( DropDownType.Nationalities );
                    return data.FirstOrDefault( x => x.Id == CountryId.Value )?.Description;
                }
                return _country;
            }
            set => _country = value;
        }
        private string _country;

        public int IsActive { get; set; }


        #endregion

        public ExecutiveEducationEditVm( int cVid )
        {
            CVid = cVid;
        }

        public ExecutiveEducationEditVm()
        {
        }
    }
}
