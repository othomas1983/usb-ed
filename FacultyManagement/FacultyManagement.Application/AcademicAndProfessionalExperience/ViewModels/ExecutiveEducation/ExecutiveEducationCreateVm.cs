﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using DataAnnotationsExtensions;
using FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.Common;
using FacultyManagement.Application.Common;
using FacultyManagement.Core.Cache;
using FacultyManagement.Domain.Model.Common;
using FacultyManagement.Domain.Model.Enums;
using FacultyManagement.Infrastructure.Utilities.ExtensionMethods;

namespace FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.ExecutiveEducation
{
    public class ExecutiveEducationCreateVm : ExperienceBaseVm, ICreateEditVm
    {
        #region Properties

        public int? Id { get; set; } = null;

        public int IsActive { get; set; } = 1;

        [HiddenInput]
        public int ExperienceCategoryId { get; set; } = ExperienceCategory.ExecutiveEducation.ConvertToInt();

        [DataType( "PositiveNumber" ), Range(1, 9999, ErrorMessage = "{0} must be 1 or more."), Required]       
        public int NoOfDays { get; set; } = 1;

        [Required]
        public string Programme { get; set; }

        [DataType( "NationalityId" ), DisplayName( "Country" ), Required]
        public int? CountryId { get; set; }

        /// <summary>
        /// Gets the country using the lookup table and countryId.
        /// </summary>
        /// <value>
        /// The country.
        /// </value>
        public string Country
        {
            get
            {
                if ( !CountryId.HasValue ) return string.Empty;

                var data = DropDownsCache.Read<Nationality>( DropDownType.Nationalities );
                return data.FirstOrDefault( x => x.Id == CountryId.Value )?.Description;
            }
        }        

        #endregion

        public ExecutiveEducationCreateVm( int cVid )
        {
            StartDate = DateTime.UtcNow.AddDays( -1 );
            EndDate = DateTime.UtcNow;
            CVid = cVid;
        }

        public ExecutiveEducationCreateVm()
        {
        }
    }
}
