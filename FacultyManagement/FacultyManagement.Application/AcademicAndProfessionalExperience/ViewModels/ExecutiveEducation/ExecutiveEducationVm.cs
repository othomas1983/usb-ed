﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.ExecutiveEducation
{
    public class ExecutiveEducationVm
    {
        #region Properties
        public int Id { get; set; }
        public int IsActive { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int ExperienceCategoryId { get; set; }
        public string Subject { get; set; }

        [DataType("PositiveNumber"), Range(1, 9999, ErrorMessage = "{0} must be 1 or more."), Required]
        public int NoOfDays { get; set; }
        public string Programme { get; set; }
        public string Country { get; set; }
        public string IsCurrent { get; set; }

        #endregion
    }
}
