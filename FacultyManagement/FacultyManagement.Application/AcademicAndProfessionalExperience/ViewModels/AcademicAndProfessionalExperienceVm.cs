﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.EditorialBoardMember;
using FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.EngagementWithBusinessAndSociety;
using FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.EngagementWithMedia;
using FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.ExecutiveEducation;
using FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.PartTimeConsulting;
using FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.TeachingExperience;
using FacultyManagement.Core.ViewModels.Interfaces;
using FacultyManagement.Domain.Model.AcademicAndProfessionalExperience;
using FacultyManagement.Domain.Model.Enums;
using FacultyManagement.Infrastructure.Utilities.ExtensionMethods;

namespace FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels
{
    public class AcademicAndProfessionalExperienceVm : IViewModel
    {
        public List<TeachingExperienceVm> TeachingExperiences;
        public List<ExecutiveEducationVm> ExecutiveEducation;
        public List<PartTimeConsultingVm> PartTimeConsulting;
        public List<EngagementWithBusinessVm> EngagementWithBusiness;
        public List<EditorialBoardMemberVm> EditorialBoardMember;
        public List<EngagementWithMediaVm> EngagementWithMedia;

        public AcademicAndProfessionalExperienceVm( List<Experience> experiences )
        {
            TeachingExperiences = experiences
                .Where( e => e.ExperienceCategoryId == ExperienceCategory.Teaching.ConvertToInt() )
                .Select( CreateTeachingExperience )
                .ToList();

            ExecutiveEducation = experiences
                .Where( e => e.ExperienceCategoryId == ExperienceCategory.ExecutiveEducation.ConvertToInt() )
                .Select( CreateExecutiveEducation )
                .ToList();

            PartTimeConsulting = experiences
              .Where( e => e.ExperienceCategoryId == ExperienceCategory.PartTime.ConvertToInt() )
              .Select( CreatePartTimeConsulting )
              .ToList();

            EngagementWithBusiness = experiences
            .Where( e => e.ExperienceCategoryId == ExperienceCategory.EngagementWithBusinessAndSociety.ConvertToInt() )
            .Select( CreateEngagementWithBusiness )
            .ToList();

            EditorialBoardMember = experiences
               .Where( e => e.ExperienceCategoryId == ExperienceCategory.EditorialBoardmembers.ConvertToInt() )
               .Select( CreateEditorialBoardMember )
               .ToList();

            EngagementWithMedia = experiences
                .Where( e => e.ExperienceCategoryId == ExperienceCategory.EngagementWithMedia.ConvertToInt() )
                .Select( CreateEngagementWithMedia )
                .ToList();
        }



        #region Create View Models

        private TeachingExperienceVm CreateTeachingExperience( Experience x )
        {
            var vm = new TeachingExperienceVm
            {
                Id = x.Id,
                IsActive = x.IsActive,
                StartDate = x.StartDate,
                EndDate = x.EndDate,
                Subject = x.Subject,
                Position = x.Position,
                Institution = x.Institution,
                City = x.City,
                Country = x.Country,
                IsCurrent = x.IsCurrent.ToYesNo(),
                ExperienceCategoryId = x.ExperienceCategoryId
            };

            return vm;
        }

        private ExecutiveEducationVm CreateExecutiveEducation( Experience x )
        {
            var vm = new ExecutiveEducationVm
            {
                Id = x.Id,
                IsActive = x.IsActive,
                StartDate = x.StartDate,
                EndDate = x.EndDate,
                Subject = x.Subject,
                NoOfDays = x.NoOfDays,
                Programme = x.Programme,
                Country = x.Country,
                IsCurrent = x.IsCurrent.ToYesNo(),
                ExperienceCategoryId = x.ExperienceCategoryId
            };

            return vm;
        }

        private PartTimeConsultingVm CreatePartTimeConsulting( Experience x )
        {
            var vm = new PartTimeConsultingVm
            {
                Id = x.Id,
                IsActive = x.IsActive,
                StartDate = x.StartDate,
                EndDate = x.EndDate,
                Subject = x.Subject,
                NoOfDays = x.NoOfDays,
                Client = x.Client,
                IsCurrent = x.IsCurrent.ToYesNo(),
                ExperienceCategoryId = x.ExperienceCategoryId
            };

            return vm;
        }

        private EngagementWithBusinessVm CreateEngagementWithBusiness( Experience x )
        {
            var vm = new EngagementWithBusinessVm
            {
                Id = x.Id,
                IsActive = x.IsActive,
                StartDate = x.StartDate,
                EndDate = x.EndDate,
                Subject = x.Subject,
                Institution = x.Institution,
                KnowledgeArea = x.KnowledgeArea,
                City = x.City,
                Country = x.Country,
                IsCurrent = x.IsCurrent.ToYesNo(),
                ExperienceCategoryId = x.ExperienceCategoryId
            };

            return vm;
        }

        private EditorialBoardMemberVm CreateEditorialBoardMember( Experience x )
        {
            var vm = new EditorialBoardMemberVm
            {
                Id = x.Id,
                IsActive = x.IsActive,
                StartDate = x.StartDate,
                EndDate = x.EndDate,
                Subject = x.Subject,
                Publication = x.Publication,
                PublicationStatus = x.PublicationStatus,
                PublicationEdition = x.PublicationEdition,
                IsCurrent = x.IsCurrent.ToYesNo(),
                ExperienceCategoryId = x.ExperienceCategoryId
            };

            return vm;
        }

        private EngagementWithMediaVm CreateEngagementWithMedia( Experience x )
        {
            var vm = new EngagementWithMediaVm
            {
                Id = x.Id,
                IsActive = x.IsActive,
                StartDate = x.StartDate,
                EndDate = x.EndDate,
                Subject = x.Subject,
                NoOfDays = x.NoOfDays,
                Client = x.Client,
                IsCurrent = x.IsCurrent.ToYesNo(),
                ExperienceCategoryId = x.ExperienceCategoryId
            };

            return vm;
        }
        #endregion

    }
}
