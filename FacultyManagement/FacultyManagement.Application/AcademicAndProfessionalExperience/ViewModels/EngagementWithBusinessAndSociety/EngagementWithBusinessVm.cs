﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.EngagementWithBusinessAndSociety
{
    public class EngagementWithBusinessVm
    {
        #region Properties
        public int Id { get; set; }
        public int IsActive { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int ExperienceCategoryId { get; set; }
        public string Subject { get; set; }
        public string Institution { get; set; }
        public string KnowledgeArea { get; set; }
        public string City { get; set; }
        public string Country { get; set; }

        public string IsCurrent { get; set; }

        #endregion
    }
}
