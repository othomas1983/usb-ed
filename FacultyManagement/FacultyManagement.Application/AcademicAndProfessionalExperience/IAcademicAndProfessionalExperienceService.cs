﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels;
using FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.ExecutiveEducation;
using FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.TeachingExperience;
using FacultyManagement.Application.Common;
using FacultyManagement.Core.ViewModels.Interfaces;

namespace FacultyManagement.Application.AcademicAndProfessionalExperience
{
    public interface IAcademicAndProfessionalExperienceService : IApplicationService
    {
    }
}
