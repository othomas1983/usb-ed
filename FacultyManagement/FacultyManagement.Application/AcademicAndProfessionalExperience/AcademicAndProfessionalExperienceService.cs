﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels;
using FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.EngagementWithBusinessAndSociety;
using FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.ExecutiveEducation;
using FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.PartTimeConsulting;
using FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.TeachingExperience;
using FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.EditorialBoardMember;
using FacultyManagement.Application.Common;
using FacultyManagement.Application.EducationAndAwards.ViewModels.Education;
using FacultyManagement.Core.Cache;
using FacultyManagement.Core.Security.Identity;
using FacultyManagement.Core.ViewModels.Interfaces;
using FacultyManagement.Data.Interfaces;
using FacultyManagement.Data.Models;
using FacultyManagement.Domain.Factories;
using FacultyManagement.Domain.Model.AcademicAndProfessionalExperience;
using FacultyManagement.Domain.Model.EducationAndAwards;

namespace FacultyManagement.Application.AcademicAndProfessionalExperience
{
    public class AcademicAndProfessionalExperienceService : ApplicationService<AcademicAndProfessionalExperienceVm>, IAcademicAndProfessionalExperienceService
    {
        private readonly IAcademicAndProfessionalExperienceDbService _dbService;

        public AcademicAndProfessionalExperienceService( IAcademicAndProfessionalExperienceDbService dbService )
        {
            _dbService = dbService;
        }

        public override IViewModel GetViewModel( IUser<CurrentPerson> peerPerson )
        { 
            // Create a new view model so that new updated data is loaded
            //return this.CreateViewModel( cvId );
            return ViewModelCache.Read( peerPerson.CVid, () => CreateViewModel( peerPerson.CVid ) );
        }
        
        #region Override Methods

        /// <summary>
        /// Gets the item.
        /// </summary>
        /// <param name="itemId"></param>        
        /// <param name="currentPerson"></param>
        /// <returns></returns>
        public override T GetItem<T>( int itemId, IUser<CurrentPerson> currentPerson )
        {
            var model = default( T );
            object item = null;

            var viewModel = ViewModelCache.Read( currentPerson.CVid, () => CreateViewModel( currentPerson.CVid ) );

            if ( typeof( T ) == typeof( TeachingExperienceEditVm ) )
            {
                item = viewModel.TeachingExperiences.Single( e => e.Id == itemId );
            }

            if ( typeof( T ) == typeof( ExecutiveEducationEditVm ) )
            {
                item = viewModel.ExecutiveEducation.Single( e => e.Id == itemId );
            }

            if ( typeof( T ) == typeof( PartTimeConsultingEditVm ))
            {
                item = viewModel.PartTimeConsulting.Single( e => e.Id == itemId );
            }

            if ( typeof( T ) == typeof( EngagementWithBusinessEditVm ) )
            {
                item = viewModel.EngagementWithBusiness.Single( e => e.Id == itemId );
            }

            if (typeof(T) == typeof(EditorialBoardMemberEditVm))
            {
                item = viewModel.EditorialBoardMember.Single(e => e.Id == itemId);
            }

            model = Mapper.Map<T>( item );

            return model;
        }

        /// <summary>
        /// Saves the changes for the appropriate model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="currentPerson"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public override bool SaveChanges( ICreateEditVm model, IUser<CurrentPerson> currentPerson, IUser<CurrentUser> user )
        {
            ISaveModel dbModel = Mapper.Map<SaveExperience>( model );

            bool success = _dbService.SaveChanges( dbModel, user.Username );
            // Reset Cache because the model has changed
            if ( success )
            {
                FlushViewModel( currentPerson );
            }

            return success;
        }

        public override bool Delete<T>( int id, IUser<CurrentPerson> currentPerson, IUser<CurrentUser> user )
        {         

            //return _dbService.Delete<T>( id, user.Username );
            bool success = _dbService.Delete<T>(id, user.Username);

            if (success)
            {
                FlushViewModel(currentPerson);
            }

            return success;
        }

        #endregion
        
        #region Protected Methods

        /// <summary>
        /// Creates the view model.
        /// </summary>
        /// <param name="cvId">The c vid.</param>
        /// <returns></returns>
        protected override AcademicAndProfessionalExperienceVm CreateViewModel( int cvId )
        {
            var experiencesDs = _dbService.GetExperiences( cvId );
            var experiences = ModelFactory.CreateList<Experience>( experiencesDs );

            // Order by end date to get correct order
            var model = new AcademicAndProfessionalExperienceVm( experiences.OrderBy( e => e.EndDate ).ToList() );

            return model;
        }

        #endregion
    }
}
