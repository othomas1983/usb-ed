﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Web;
using System.Web.Mvc;
using FacultyManagement.Application.Common;
using FacultyManagement.Core.Attributes;
using FacultyManagement.Domain.Model;
using FacultyManagement.Domain.Model.PersonalInformation;
using FacultyManagement.Infrastructure;
using System.Net;

namespace FacultyManagement.Application.PersonalInformation.ViewModels
{
    public class PersonalInformationEditVm :  ICreateEditVm
    {
        #region Properties 
        [DataType( DataType.MultilineText ), DisplayName( "Executive profile’" )]
        public string Biography { get; set; }

        [DataType("Title"), Required]
        public string Title { get; set; }

        [MaxLength( 50 )]
        public string Initials { get; set; }

        [MaxLength( 100 ),DisplayName( "First name" ), Required]
        public string Name { get; set; }

        //[MaxLength( 100 ), DisplayName( "Given name" ), Required]
        //public string GivenName { get; set; }

        [MaxLength(100), Required]
        public string Surname { get; set; }

        [DataType( nameof( Gender ) )]
        public string Gender { get; set; }

        [DataType( FmDataTypes.NationalityId  ), DisplayName( "Nationality" )]
        public int? NationalityId { get; set; }

        [Required]
        public string BusinessContactNo { get; set; }

        [DataType( "string" ), EmailAddress, Required]
        public string EmailAddress { get; set; }

        [MaxLength( 250 ),  DisplayName( "Organisational role" )]
        public string Position { get; set; }

        [DataType( FmDataTypes.AcademicRank ),  DisplayName( "Organisational position" )]
        public string AcademicRank { get; set; }

        [DataType( FmDataTypes.HighestQualificationId ), DisplayName( "Highest qualification" )]
        public int? HighestQualificationId { get; set; }

        [DataType( FmDataTypes.MainActivityId ), DisplayName( "Main activity" )]
        public int? MainActivityId { get; set; }

        [DataType(FmDataTypes.BusinessUnit), DisplayName("Business Unit")]
        public string BusinessUnit { get; set; }

        [MaxLength( 500 ), DisplayName( "Area(s) of expertise" ), Required]
        public string Expertise { get; set; }

        [MaxLength( 500 )]
        public string ResearchInterest { get; set; }

        [MaxLength( 100 )]
        public string HomeInstitution { get; set; }

        [MaxLength( 150 )]
        public string OrganisationalResponsibility { get; set; }

        [HiddenInput]
        public int? CVid { get; set; }

        [DataType( FmDataTypes.AffiliationId ), DisplayName( "Affiliation" )]
        public int? AffiliationId { get; set; }

        public string SamAccountName { get; set; }

        [DataType( FmDataTypes.ApplicationUserRoleId  ), DisplayName( "User role" )]
        public int? ApplicationUserRoleId { get; set; }

        [HiddenInput]
        public string EmployeeId { get; set; }

        [HiddenInput]
        public int? IsFaculty { get; set; }

        [DataType(FmDataTypes.PositiveNumber)]
        public int? GoogleScholarIndex { get; set; }

        public string ImageSource { get; set; }

        [DataType( DataType.Upload ), ImageSize( 100000 )]
        public HttpPostedFileBase ImageUpload { get; set; }

        public bool? WebDisplay { get; set; } = null;

        public UserPhoto UserPhoto
        {         
            get
            {
                if ( ImageUpload != null )
                {
                    byte[] imageData;
                    using ( var reader = new BinaryReader( ImageUpload.InputStream ) )
                    {
                        imageData = reader.ReadBytes( ImageUpload.ContentLength );
                    }

                    string extension = Path.GetExtension( ImageUpload.FileName );

                    return new UserPhoto( EmployeeId, extension, imageData );
                }

                return null;
            }
        }

        #endregion
    }
}
