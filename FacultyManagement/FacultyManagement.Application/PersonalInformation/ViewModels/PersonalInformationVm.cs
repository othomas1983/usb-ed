﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using FacultyManagement.Application.Common;
using FacultyManagement.Core.ViewModels.Interfaces;
using FacultyManagement.Infrastructure;

namespace FacultyManagement.Application.PersonalInformation.ViewModels
{
    public class PersonalInformationVm : IViewModel
    {
        #region Properties 
        [DisplayName("Executive profile")]

        public string Biography { get; set; }

        public string Title { get; set; }

        public string Initials { get; set; }

        [DisplayName( "First name" )]
        public string Name { get; set; }

        //[DisplayName("Given name")]
        //public string GivenName { get;  set; }
        public string Surname { get; set; }

        public string Gender { get; set; }

        public string Nationality { get; set; }

        public int? NationalityId { get; set; }

        public string BusinessContactNo { get; set; }

        [EmailAddress]
        public string EmailAddress { get; set; }

        [DisplayName( "Organisational role" )]
        public string Position { get; set; }

        [DisplayName( "Organisational position" )]
        public string AcademicRank { get; set; }

        public string HighestQualification { get; set; }

        public int? HighestQualificationId { get; set; }

        public int? MainActivityId { get; set; }

        public string MainActivity { get; set; }

        public string BusinessUnit { get; set; }

        [DisplayName( "Area(s) of expertise" )]
        public string Expertise { get; set; }

        public string ResearchInterest { get; set; }

        public string HomeInstitution { get; set; }

        public string OrganisationalResponsibility { get; set; }
        public int Cvid { get; set; }

        [DataType( FmDataTypes.AffiliationId ), DisplayName("Affiliation")]
        public int? AffiliationId { get; set; }

        public string SamAccountName { get; set; }

        public int? ApplicationUserRoleId { get; set; }

        public string ApplicationUserRole { get; set; }

        public int? IsFaculty { get; set; }

        public string EmployeeId { get; set; }

        /* Organisation, Classification and Sufficiency */
        public string OrganisationCategory { get; set; } = string.Empty;

        public string Classification { get; set; }

        public string Sufficiency { get; set; }
        
        public int? GoogleScholarIndex { get; set; }
        
        public string ImageSource { get; set; }

        public bool? WebDisplay { get; set; } = null;

        #endregion
    }
}
