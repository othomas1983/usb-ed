﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using FacultyManagement.Application.Common;
using FacultyManagement.Application.PersonalInformation.ViewModels;
using FacultyManagement.Core.Cache;
using FacultyManagement.Core.Security.Identity;
using FacultyManagement.Core.ViewModels.Interfaces;
using FacultyManagement.Data.Interfaces;
using FacultyManagement.Data.Models;
using FacultyManagement.Domain.Factories;
using FacultyManagement.Domain.Model;
using FacultyManagement.Domain.Model.Admin;
using FacultyManagement.Domain.Model.Common;
using FacultyManagement.Domain.Model.Enums;
using FacultyManagement.Domain.Model.PersonalInformation;
using FacultyManagement.Infrastructure.Utilities.ExtensionMethods;

namespace FacultyManagement.Application.PersonalInformation
{
    public class PersonalInformationService : ApplicationService<PersonalInformationVm>, IPersonalInformationService
    {
        private readonly IPersonalInformationDbService _dbService;
        private readonly IAdminDbService _adminDbService;

        public PersonalInformationService( IPersonalInformationDbService dbService, IAdminDbService adminDbService )
        {
            _dbService = dbService;
            _adminDbService = adminDbService;
        }

        public override IViewModel GetViewModel( IUser<CurrentPerson> currentPerson )
        {
            return ViewModelCache.Read( currentPerson.CVid, () => CreateViewModel( currentPerson ) );
        }


        /// <summary>
        /// Saves the changes.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="currentPerson">The current person.</param>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        public override bool SaveChanges( ICreateEditVm model, IUser<CurrentPerson> person, IUser<CurrentUser> user )
        {
            var dbModel = Mapper.Map<SavePersonalInformation>( model );

            bool success = _dbService.SaveChanges( dbModel, user.Username );

            if ( success )
            {
                FlushViewModel( person );
            }

            return success;
        }

        /// <summary>
        /// Creates the view model.
        /// </summary>
        /// <param name="currentPerson">The current person.</param>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        protected override PersonalInformationVm CreateViewModel( IUser<CurrentPerson> currentPerson, IUser<CurrentUser> user = null )
        {
            var personDs = _dbService.GetPersonalInformation( currentPerson.Username );
            var person = new Domain.Model.PersonalInformation.PersonalInformation( personDs );

            var imageDs = _dbService.GetUserImage( person.EmployeeId );
            UserPhoto image = null;
            try
            {
                image = new UserPhoto( imageDs );
            }
            catch ( ObjectNotFoundException )
            {
                // They dont have a picture
            }

            var model = Mapper.Map<PersonalInformationVm>( person );
            model.ImageSource = image?.ImageSource;

            // Get the users faculty departments and store in cache
            int? departmentId = ModelCache.Read( currentPerson.CVid, () => ModelFactory.CreateList<FacultyDepartment>( 
                                              _adminDbService.GetFacultyDepartments( currentPerson.CVid ) ) )
                                              .FirstOrDefault()?.DepartmentId;

            if ( departmentId.HasValue )
            {
                var department = DropDownsCache.Read<Department>( DropDownType.Departments )
                  .SingleOrDefault( d => d.Id == departmentId.Value );

                model.OrganisationCategory = department?.Description;
            }

            return model;
        }

        /// <summary>
        /// Gets the edit model.
        /// </summary>
        /// <param name="currentPerson">The username.</param>
        /// <returns></returns>
        public override ICreateEditVm GetEditModel( IUser<CurrentPerson> currentPerson )
        {
            var personDs = _dbService.GetPersonalInformation( currentPerson.Username );
            var person = new Domain.Model.PersonalInformation.PersonalInformation( personDs );

            var imageDs = _dbService.GetUserImage( person.EmployeeId );
            UserPhoto image = null;
            try
            {
                image = new UserPhoto( imageDs );
            }
            catch ( ObjectNotFoundException )
            {
                // They dont have a picture
            }

            var model = new PersonalInformationEditVm();
            model = Mapper.Map<PersonalInformationEditVm>( person );
            model.ImageSource = image?.ImageSource;

            return model;
        }
    }
}
