﻿using FacultyManagement.Application.Common;
using FacultyManagement.Application.PersonalInformation.ViewModels;
using FacultyManagement.Core.ViewModels.Interfaces;
using FacultyManagement.Data.Models;

namespace FacultyManagement.Application.PersonalInformation
{
    public interface IPersonalInformationService : IApplicationService
    {
    }
}