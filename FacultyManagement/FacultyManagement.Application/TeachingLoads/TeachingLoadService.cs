﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Application.Common;
using FacultyManagement.Application.TeachingLoads.ViewModels;
using FacultyManagement.Core.Cache;
using FacultyManagement.Core.Security.Identity;
using FacultyManagement.Core.ViewModels.Interfaces;
using FacultyManagement.Data.Interfaces;
using FacultyManagement.Data.Models;
using FacultyManagement.Domain.Factories;
using FacultyManagement.Domain.Model;
using FacultyManagement.Domain.Model.Loads;
using FacultyManagement.Domain.Model.Common;
using FacultyManagement.Application.WebUsers.ViewModels;
using FacultyManagement.Application.Admin;

namespace FacultyManagement.Application.TeachingLoads
{
    public class TeachingLoadService : ApplicationService<TeachingLoadVm>, ITeachingLoadService
    {
        private readonly ILoadsDbService _dbService;

        public TeachingLoadService( ILoadsDbService dbService )
        {
            _dbService = dbService;
        }


        public override IViewModel GetViewModel(IUser<CurrentPerson> currentPerson)
        {
            return ViewModelCache.Read(currentPerson.CVid, () => CreateViewModel(currentPerson.CVid));

        }

        public TeachingLoadVm FilterViewModel( IUser<CurrentPerson> currentPerson, int? year )
        {
            var model = ViewModelCache.Read( currentPerson.CVid, () => CreateViewModel( currentPerson.CVid ) );
            // Make a deep copy to avoid the filter updating the cached items
            var deepCopy = Mapper.Map<TeachingLoadVm>( model );

            if ( year.HasValue && model.TeachingLoads != null )
            {
                var filteredLoads = deepCopy.TeachingLoads.Where( w => w.Year == year ).ToList();

                deepCopy.TeachingLoads = filteredLoads;
            }

            return deepCopy;
        }


        #region Override Methods

        public override T GetItem<T>( int itemId, IUser<CurrentPerson> person )
        {
            var model = default( T );

            object item = null;

            var viewModel = ViewModelCache.Read( person.CVid, () => CreateViewModel( person.CVid ) );

            item = viewModel.TeachingLoads.Single( e => e.Id == itemId );

            model = Mapper.Map<T>( item );

            return model;
        }

        public override bool SaveChanges( ICreateEditVm model, IUser<CurrentPerson> person, IUser<CurrentUser> user )
        {
            var dbModel = Mapper.Map<SaveTeachingLoad>( model );

        
          
                // Reset Cache because the model has changed
                FlushViewModel( person );
          

            
            if (dbModel.Programme == "")
            {
                dbModel.Programme = dbModel.ProgrammeDescription;
            }
            if (dbModel.ProgrammeOffering == "")
            {
                dbModel.ProgrammeOffering = dbModel.ProgrammeOfferingDescription;
            }
            if (dbModel.Module == "")
            {
                dbModel.Module = dbModel.ModuleDescription;
            }
            if (dbModel.Year == 0)
            {
                dbModel.Year = dbModel.YearDescription;
            }
            if (dbModel.Category == "")
            {
                dbModel.Category = dbModel.CategoryDescription;
            }
            return _dbService.SaveChanges(dbModel, user.Username);
           
        }
        public override bool Delete<T>(int id, IUser<CurrentPerson> person, IUser<CurrentUser> user)
        {
            bool success = _dbService.Delete<T>(id, user.Username);

            if (success)
            {
                FlushViewModel(person);
            }

            return success;
        }

        #endregion

        #region Private Methods
        protected override TeachingLoadVm CreateViewModel( int cvId )
        {
            var teachingLoadDs = _dbService.GetTeachingLoads( cvId );
            var teachingLoads = ModelFactory.CreateList<TeachingLoad>( teachingLoadDs ).OrderByDescending( t => t.Year ).ToList();

            var mappedItems = Mapper.Map<List<TeachingLoadItemVm>>( teachingLoads );

            var model = new TeachingLoadVm( mappedItems );

            return model;
        }
        #endregion
    }
}
