﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Application.Common;
using FacultyManagement.Core.Cache;
using FacultyManagement.Core.ViewModels.Interfaces;
using FacultyManagement.Domain.Model.Common;
using FacultyManagement.Domain.Model.Enums;
using FacultyManagement.Domain.Model.Loads.Common;

namespace FacultyManagement.Application.TeachingLoads.ViewModels
{
    public class TeachingLoadItemCreateVm : ICreateEditVm
    {
        #region Properties

        public int? Id { get; set; } = null;
        [DataType( "YearDatePickerLoad" ), Required]
        public int Year { get; set; }

        public string ProgrammeOfferingDescription { get; set; }
        public string ProgrammeDescription { get; set; }
        public string ModuleDescription { get; set; }
        public int YearDescription { get; set; }
        public string CategoryDescription { get; set; }

        [DataType("PMProgrammeOfferingId"),Required, DisplayName("Offering")]

        public int? ProgrammeOfferingId { get; set; }
        public string ProgrammeOffering
        {
            get
            {
                if (!ProgrammeOfferingId.HasValue) return string.Empty;

                var data = DropDownsCache.Read<PMProgrammeOffering>(DropDownType.PMProgrammeOfferings);
                return data.FirstOrDefault(x => x.Id == ProgrammeOfferingId.Value)?.Description;
            }
           
        }


        [DataType( "PMProgrammeId" ), DisplayName("Programme"), Required]
        public int? ProgrammeId { get; set; }
        /// <summary>
        /// Gets the porgramme using the lookup table and ProgrammeId.
        /// </summary>
        public string Programme
        {
            get
            {
                if ( !ProgrammeId.HasValue ) return string.Empty;

                var data = DropDownsCache.Read<PMProgramme>( DropDownType.PMProgrammes );
                return data.FirstOrDefault( x => x.Id == ProgrammeId.Value )?.Description;
            }
        }


        [DataType("PMProgrammeOfferingModuleId"), DisplayName("Module"), Required]
        public int? ModuleId { get; set; }
        /// <summary>
        /// Gets the porgramme using the lookup table and ModuleId.
        /// </summary>
        public string Module
        {
            get
            {
                if (!ModuleId.HasValue) return string.Empty;

                var data = DropDownsCache.Read<PMProgrammeOfferingModule>(DropDownType.PMProgrammeOfferingModules);
                return data.FirstOrDefault(x => x.Id == ModuleId.Value)?.Description;
            }
        }



        [DataType("PMCategoryId"), DisplayName("Category"), Required]
        public int? CategoryId { get; set; }
        /// <summary>
        /// Gets the porgramme using the lookup table and ProgrammeId.
        /// </summary>CategoryId
        public string Category
        {
            get
            {
                if (!CategoryId.HasValue) return string.Empty;

                var data = DropDownsCache.Read<PMTeachingCategory>(DropDownType.PMCategory);
                return data.FirstOrDefault(x => x.Id == CategoryId.Value)?.Description;
            }
        }


        //[DataType("PositiveNumber"), Range(1, 9999, ErrorMessage = "{0} must be 1 or more."), Required]
        //[RegularExpression(@"^\d+\.\d{0,2}$")]
        public decimal Sessions { get; set; }


        //[DataType("PositiveNumber"), Range(1, 9999, ErrorMessage = "{0} must be 1 or more."), Required]
        //[RegularExpression(@"^\d+\.\d{0,2}$")]
        [Required, DisplayName("Hours")]
        public decimal TotalCredits { get; set; }

        public int? CVid { get; set; }
        #endregion

        public TeachingLoadItemCreateVm( int cVid )
        {
            CVid = cVid;
        }

        public TeachingLoadItemCreateVm()
        {
        }
    }
}
