﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Core.ViewModels.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace FacultyManagement.Application.TeachingLoads.ViewModels
{
    public class TeachingLoadItemVm
    {
        #region Properties

        public int Id { get; set; }
        public int Year { get;  set; }
        public string ProgrammeOffering { get;  set; }
        public string Programme { get;  set; }

        public string ProgrammeOfferingDescription { get; set; }
        public string ProgrammeDescription { get; set; }
        public string ModuleDescription { get; set; }

        public int YearDescription { get; set; }
        public string Module { get;  set; }
        //[RegularExpression(@"^\d+\.\d{0,2}$")]
        public decimal Sessions { get;  set; } 

        //[RegularExpression(@"^\d+\.\d{0,2}$")]
        public decimal TotalCredits { get; set; }
        public string Category { get; set; }
        public string CategoryDescription { get; set; }
        #endregion
    }
}
