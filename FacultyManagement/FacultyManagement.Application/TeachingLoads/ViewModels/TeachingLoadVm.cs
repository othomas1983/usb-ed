﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using FacultyManagement.Core.ViewModels.Interfaces;
using FacultyManagement.Domain.Model;

namespace FacultyManagement.Application.TeachingLoads.ViewModels
{
    public class TeachingLoadVm : IViewModel
    {
        #region Properties
        public List<SelectListItem> Years { get; set; } = new List<SelectListItem>();

        public List<TeachingLoadItemVm> TeachingLoads { get; set; }

        public string ProgrammeOfferingDescription { get; set; }
        public string ProgrammeDescription { get; set; }
        public string ModuleDescription { get; set; }
        public int YearDescription { get; set; }
        public string CategoryDescription { get; set; }

        #endregion

        public TeachingLoadVm( List<TeachingLoadItemVm> teachingLoads )
        {
            TeachingLoads = teachingLoads;

            if ( teachingLoads  != null && teachingLoads.Any() )
            {                
                Years.Add( new SelectListItem
                {
                    Value = null,
                    Text = "-- All --"
                } );
                teachingLoads.ForEach( x => AddToYears( x.Year ) );
            }
        }

        public TeachingLoadVm()
        {
        }

        #region Private Methods
        private void AddToYears( int year )
        {
            var item = new SelectListItem
            {
                Value = year.ToString(),
                Text = year.ToString()
            };

            if ( !Years.Select( x => x.Value ).Contains( item.Value ) )
            {
                Years.Add( item );
            }
        }
        #endregion
    }
}
