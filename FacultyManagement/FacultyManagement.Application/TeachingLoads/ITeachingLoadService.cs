﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Application.Common;
using FacultyManagement.Application.TeachingLoads.ViewModels;
using FacultyManagement.Core.Security.Identity;
using FacultyManagement.Application.WebUsers.ViewModels;
using FacultyManagement.Application.Admin;

namespace FacultyManagement.Application.TeachingLoads
{
    public interface ITeachingLoadService : IApplicationService
    {
        /// <summary>
        /// Filters the write hand research with optional year filter.
        /// </summary>
        /// <param name="year">The year.</param>
        /// <param name="currentPerson">The year.</param>
        /// <returns></returns>
        TeachingLoadVm FilterViewModel( IUser<CurrentPerson> currentPerson, int? year );    
    }
}
