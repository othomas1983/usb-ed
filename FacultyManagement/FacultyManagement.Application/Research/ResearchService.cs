﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using FacultyManagement.Application.Admin;
using FacultyManagement.Application.Admin.ViewModels.Common;
using FacultyManagement.Application.Common;
using FacultyManagement.Application.Research.Helpers;
using FacultyManagement.Application.Research.ViewModels;
using FacultyManagement.Core.Cache;
using FacultyManagement.Core.Security.Identity;
using FacultyManagement.Core.ViewModels.Interfaces;
using FacultyManagement.Data.Interfaces;
using FacultyManagement.Data.Models;
using FacultyManagement.Domain.Factories;
using FacultyManagement.Domain.Model.Common;
using FacultyManagement.Domain.Model.Enums;
using FacultyManagement.Domain.Model.Research;
using FacultyManagement.Infrastructure.Sharepoint;


namespace FacultyManagement.Application.Research
{
    public class ResearchService : ApplicationService<ResearchVm>, IResearchService
    {
        private readonly IResearchDbService _dbService;
        private readonly IUserService _userService;
        private readonly ISharepointWebService _sharepointWebService;

        public ResearchService( IResearchDbService dbService, IUserService userService, ISharepointWebService sharepointWebService )
        {
            _dbService = dbService;
            _userService = userService;
            _sharepointWebService = sharepointWebService;
        }

        #region Override Methods

        public override IViewModel GetViewModel( IUser<CurrentPerson> currentPerson )
        {
            return ViewModelCache.Read( currentPerson.CVid, () => CreateViewModel( currentPerson.CVid ) );
        }

        public override T GetItem<T>( int itemId, IUser<CurrentPerson> person )
        {
            var model = default( T );
            object item = null;

            var viewModel = ViewModelCache.Read( person.CVid, () => CreateViewModel( person.CVid ) );

            if ( typeof( T ) == typeof( ResearchHistoryEditVm ) )
            {
                item = viewModel.Activities.Single( e => e.Id == itemId );
            }

            model = Mapper.Map<T>( item );

            return model;
        }

        public override bool SaveChanges( ICreateEditVm model, IUser<CurrentPerson> person, IUser<CurrentUser> user )
        {
            var dbModel = Mapper.Map<SaveResearchActivity>( model );

            var viewModel = (ResearchHistoryCreateEditVm) model;
            // This is an edited research being saved
            if ( viewModel.ResearchId.HasValue )
            {
                var dataSet = _dbService.GetContributors( viewModel.ResearchId.Value );
                var existingContributors = ModelFactory.CreateList<Contributor>( dataSet ).ToList();
                var existingContributorsVm = CreateContributorsDetails( existingContributors );

                // Contributors from the database would have Id set : these are the ones we want to use to determine who should be removed from the database
                var contributorsToRemove = existingContributorsVm.Except( viewModel.Contributors.Where( c => c.Id.HasValue ), new ContributorEqualityComparer() );

                dbModel.ContributorsToRemove = Mapper.Map<List<SaveContributor>>( contributorsToRemove );
            }

            bool success = _dbService.SaveChanges( dbModel, user.Username );
            

            if ( success )
            {
                FlushViewModel( person );
                // Handle Publication Upload
                success = HandlePublicationFileUpload( viewModel.PublicationUpload, person.CVid );
            }

            return success;
        }

        public override bool Delete<T>( int id, IUser<CurrentPerson> person, IUser<CurrentUser> user )
        {
            bool success = _dbService.Delete<T>( id, user.Username );

            if ( success )
            {
                FlushViewModel( person );
            }

            return success;
        }

        #endregion

        #region IResearchService Methods

        public FacultyUserVm GetFacultyUserDetails( int cvId )
        {
            return new FacultyUserVm( _userService.GetFacultyUser( cvId ) );
        }

        public ResearchHistoryEditVm GetResearchWithContributors( int itemId, IUser<CurrentPerson> person )
        {
            var researchHistory = this.GetItem<ResearchHistoryEditVm>( itemId, person );

            // Need to get the contributors as well
            var dataSet = _dbService.GetContributors( researchHistory.ResearchId.Value );
            var contributors = ModelFactory.CreateList<Contributor>( dataSet ).ToList();

            researchHistory.Contributors = CreateContributorsDetails( contributors );

            return researchHistory;
        }

        #endregion

        #region Protected Methods
        protected override ResearchVm CreateViewModel( int cvId )
        {
            var dataSet = _dbService.GetResearch( cvId );
            var activities = ModelFactory.CreateList<ResearchHistory>( dataSet ).ToList();

            var mappedItems = Mapper.Map<List<ResearchHistoryVm>>( activities );

            var model = new ResearchVm( mappedItems );

            return model;
        }



        #endregion

        #region Private Methods

        /// <summary>
        /// Creates the contributors details.
        /// </summary>
        /// <param name="contributors">The contributors.</param>
        /// <returns></returns>
        private List<ContributorVm> CreateContributorsDetails( List<Contributor> contributors )
        {
            var contributorsVm = new List<ContributorVm>();

            foreach ( var contributor in contributors )
            {
                // Internal Contributor
                if ( contributor.Type.Equals( "Internal" ) )
                {
                    var facultyUser = _userService.GetFacultyUser( contributor.CVid.Value );
                    contributorsVm.Add( new ContributorVm( contributor, facultyUser ) );
                }
                // External Contributor
                else
                {
                    contributorsVm.Add( new ContributorVm( contributor ) );
                }
            }

            return contributorsVm;
        }


        private bool HandlePublicationFileUpload( File file, int cvId )
        {
            // No file attached
            if ( string.IsNullOrEmpty( file?.Data ) ) return true;

            // Remove unnecessary base64 initial info in string
            string convert = Regex.Replace( file.Data, @"^[^,]*,", string.Empty );

            byte[] fileData = Convert.FromBase64String( convert );
            string fileName = file.Name;

            var result = _sharepointWebService.UploadResearch( fileName, cvId, DateTime.Now, fileData );

            return result.Success;
        }
        #endregion
    }
}
