﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Infrastructure.Sharepoint;
using System.Web.Services;
using Microsoft.SharePoint.Client;

namespace FacultyManagement.Application.Research
{
    [WebService( Namespace = "http://www.usb.ac.za/",
        Description = "A service to submit or retrieve BPC staff and faculty",
        Name = "CampusFaculty" )]
    [WebServiceBinding( ConformsTo = WsiProfiles.BasicProfile1_1 )]
    [System.ComponentModel.ToolboxItem( false )]
    public class SharepointWebService : WebService, ISharepointWebService
    {
        private readonly string _sharePointUrl = ConfigurationManager.AppSettings["SharePointUrl"];
        private readonly string _domain = ConfigurationManager.AppSettings["ActiveDirectoryDomain"];
        private readonly string _username = ConfigurationManager.AppSettings["SharePointUsername"];
        private readonly string _password = ConfigurationManager.AppSettings["SharePointPassword"];
        private readonly string _researchListTitle = ConfigurationManager.AppSettings["ResearchListTitle"];
        private readonly string _researchListName = ConfigurationManager.AppSettings["ResearchListName"];

        [WebMethod]
        public SharepointUploadResult UploadResearch( string fileName, int cvId, DateTime publicationDate, byte[] fileData )
        {
            var result = new SharepointUploadResult();
            try
            {
                using ( ClientContext spClient = new ClientContext( _sharePointUrl ) )
                {
                    spClient.AuthenticationMode = ClientAuthenticationMode.Default;
                    spClient.Credentials = new System.Net.NetworkCredential( _username, _password, _domain );
                    var spDocLibResearch = spClient.Web.Lists.GetByTitle( _researchListTitle );

                    var itemInfo = new FileCreationInformation();
                    itemInfo.Url = $"{_sharePointUrl}{_researchListName}/{fileName}";
                    itemInfo.Content = fileData;
                    var uploadFile = spDocLibResearch.RootFolder.Files.Add( itemInfo );
                    spClient.Load( uploadFile );
                    spClient.ExecuteQuery();

                    CamlQuery cvFilter = new CamlQuery();    //filter by room number
                    cvFilter.ViewXml = "<View><Query><Where><In><FieldRef Name='CVID'/><Values>";


                    cvFilter.ViewXml += "<Value Type='Number'>" + cvId + "</Value>";


                    cvFilter.ViewXml += "</Values></In></Where></Query></View>";

                    var cvList = spClient.Web.Lists.GetByTitle( "Faculty" );
                    var facultyMembers = cvList.GetItems( cvFilter );
                    spClient.Load( facultyMembers );
                    spClient.ExecuteQuery();

                    if ( facultyMembers.Count > 0 )
                    {
                        var lecturers = new List<FieldLookupValue>();
                        foreach ( var facultyMember in facultyMembers )
                        {
                            FieldLookupValue lecturer = new FieldLookupValue { LookupId = facultyMember.Id };
                            lecturers.Add( lecturer );
                        }
                        //uploadFile.ListItemAllFields["cvID"] = .Id;
                        uploadFile.ListItemAllFields["Lecturers"] = lecturers;
                        uploadFile.ListItemAllFields["Title"] = fileName.Substring( 0, fileName.LastIndexOf( '.' ) );
                        uploadFile.ListItemAllFields["ResearchPublicationDate"] = publicationDate;
                    }
                    else
                    {
                        //throw new Exception("No faculty members were found matching the CV IDs submitted.");
                        result.Success = false;
                        result.Result = "No faculty members were found matching the CV IDs submitted.";
                    }

                    uploadFile.ListItemAllFields.Update();
                    spClient.ExecuteQuery();
                    result.Result = "Success";
                    result.Success = true;
                }
            }
            catch ( Exception cEx )
            {
                result.Result = cEx.Message;
                result.Success = false;
                result.ResultDetails = cEx.Message;
            }

            //Upload file
            return result;
        }
    }
}
