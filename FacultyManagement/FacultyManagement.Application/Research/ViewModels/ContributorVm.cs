﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Domain.Model.Common;
using FacultyManagement.Domain.Model.Research;
using System.ComponentModel.DataAnnotations;

namespace FacultyManagement.Application.Research.ViewModels
{
    public class ContributorVm
    {
        #region Properties

        public int? Id { get; set; }

        public int? CVid { get; set; }

        public string Author { get; set; }

        [DataType("PositiveNumber"), Range(1, 9999, ErrorMessage = "{0} must be 1 or more."), Required]
        public int Sequence { get; set; } = 1;

        [DataType("PositiveNumber"), Range(1, 9999, ErrorMessage = "{0} must be 1 or more."), Required]
        [RegularExpression(@"^\d+\.\d{0,2}$")]
        public decimal DHETCredit { get; set; }

        public string Institution { get; set; }

        [Required]
        public string Role { get; set; }

        public int RoleId { get; set; }

        /// <summary>
        /// The contributor Type: Internal or External.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        public string Type { get; set; }
   


        // External Contributor Properties
        public string Surname { get; set; }
        public string FirstName { get; set; }
        public string Initials { get; set; }
        public int? NationalityId { get; set; }
        public int? AffiliationId { get; set; }
        public string Gender { get; set; }
        public string Email { get; set; }


        public int IsActive { get; set; } = 1;
        #endregion

        public ContributorVm( Contributor contributor, FacultyUser facultyUser ) : this( contributor )
        {
            Author = facultyUser.GetFullName();
        }

        public ContributorVm( Contributor contributor )
        {
            //if(contributor.Type == "External")
            //{
            //    Type = "External";
            //}
            //else
            //{
            //    Type ="Internal";
            //}
            Id = contributor.Id;
            CVid = contributor.CVid;
            Author = contributor.GetExternalContributorFullName();
            Sequence = contributor.ContributorSequence;
            DHETCredit = contributor.Credit;
            Institution = contributor.Institution;
            Role = contributor.PersonRoleDescription;
            RoleId = contributor.PersonRoleId;
            Type = contributor.Type;

            Surname = contributor.Surname;
            FirstName = contributor.FirstName;
            Initials = contributor.Initials;
            Gender = contributor.Gender;
            Email = contributor.Email;
            NationalityId = contributor.NationalityId;
            AffiliationId = contributor.AffiliationId;
        }

        public ContributorVm()
        {
        }
    }
}
