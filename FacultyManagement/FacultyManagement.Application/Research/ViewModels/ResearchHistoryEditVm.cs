﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Application.Common;
using FacultyManagement.Core.Cache;
using FacultyManagement.Domain.Model.Common;
using FacultyManagement.Domain.Model.Enums;
using System.Web;

namespace FacultyManagement.Application.Research.ViewModels
{
    public class ResearchHistoryEditVm : ICreateEditVm
    {
        #region Properties

        public List<ContributorVm> Contributors { get; set; } = new List<ContributorVm>();

        public int? Id { get; set; }

        public int? ResearchId { get; set; }

        public string Description { get; set; }

        public int ResearchContributionTypeId { get; set; }

        public int ResearchStatusId { get; set; }

        public DateTime ResearchStatusDate { get; set; }

        public int ResearchOutputCategoryId { get; set; }

        public int LocusId { get; set; }

        public string ArticleTitle { get; set; }

        public string PublicationName { get; set; }

        public int ABSRatingStatusId { get; set; }

        public int DHETStatusId { get; set; }

        public string PublisherName { get; set; }  
        
        public int NoOfPages { get; set; }

        public int StartPage { get; set; }

        public int EndPage { get; set; }

        public int Volume { get; set; }

        public string Edition { get; set; }

        public string Issue { get; set; }

        public string PublicationCity { get; set; }

        [DataType("PositiveNumber"), Range(1, 9999, ErrorMessage = "{0} must be 1 or more."), Required]
        public int Sequence { get; set; } = 1;

        public string ResearchStatusDescription { get; set; }

        public string ResearchOutputCategoryDescription { get; set; }

        public string LocusDescription { get; set; }

        public string ABSRatingStatusDescription { get; set; }

        public string DHETStatusDescription { get; set; }

        /// <summary>
        /// Gets the countryId using the lookup table and country text.
        /// </summary>
        /// <value>
        /// The country.
        /// </value>
        [DataType("NationalityId"), DisplayName("Country"), Required]
        public int? CountryId
        {
            get
            {
                if (string.IsNullOrEmpty(_country)) return _countryId;

                var data = DropDownsCache.Read<Nationality>(DropDownType.Nationalities);
                return data.FirstOrDefault(x => x.Description == Country)?.Id;
            }
            set => _countryId = value;
        }
        private int? _countryId;

        /// <summary>
        /// Gets the country using the lookup table and countryId.
        /// </summary>
        /// <value>
        /// The country.
        /// </value>
        public string Country
        {
            get
            {
                if (_countryId.HasValue)
                {
                    var data = DropDownsCache.Read<Nationality>(DropDownType.Nationalities);
                    return data.FirstOrDefault(x => x.Id == CountryId.Value)?.Description;
                }
                return _country;
            }
            set => _country = value;
        }
        private string _country;
        public DateTime? PublicationDate { get; set; }
        [DisplayName("Hours")]
        public int Credit { get; set; }       
        public string Note { get; set; }
        public bool AddSelfAsContributor { get; set; }

        public int? CVid { get; set; }

        // private string publicationFile;
        //public string PublicationFile
        //{
        //    //publicationFile = PublicationUpload.Data;

        //    get { return publicationFile = PublicationUpload.Name; }
        //    set { publicationFile = PublicationUpload.Name; }

        //}


        public File PublicationUpload { get; set; }
        //private string fileName;
        //public string FileName
        //{

        //    get { return fileName = PublicationUpload.Name; }
        //    set { fileName = PublicationUpload.Name; }

        //}

        ////Use Filename until i can get Ivor to Adjust the SP
        //private string publicationFile;
        public string PublicationFile { get;set; }
        //{
        //    //publicationFile = PublicationUpload.Data;

        //    get { return publicationFile = PublicationUpload.Name; }
        //    set { publicationFile = PublicationUpload.Name; }

        //}

        public class File
        {
            public string Name { get; set; }
            public int Size { get; set; }
            public string Type { get; set; }
            public string Data { get; set; }
        }
        #endregion
    }
}
