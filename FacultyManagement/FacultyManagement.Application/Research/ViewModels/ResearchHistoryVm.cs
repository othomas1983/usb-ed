﻿using FacultyManagement.Core.Cache;
using FacultyManagement.Domain.Model.Common;
using FacultyManagement.Domain.Model.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacultyManagement.Application.Research.ViewModels
{
    public class ResearchHistoryVm
    {
        #region Properties

        public int Id { get; set; }

        public int? ResearchId { get; set; }

        public string Description { get; set; }

        #region Edit Fields
        public int? ResearchContributionTypeId { get; set; }
        public int? ResearchStatusId { get; set; }
        public int? ResearchOutputCategoryId { get; set; }
        public int? LocusId { get; set; }
        public string LocusDescription { get; set; }
        public int? ABSRatingStatusId { get; set; }
        public int? DHETStatusId { get; set; }
       
        public string Note { get; set; }
        #endregion
        public string PublisherName { get; set; }       
        
        public int NoOfPages { get; set; }  

        public int StartPage { get; set; }

        public int EndPage { get; set; } 

        public int Volume { get; set; }  

        public string Edition { get; set; }  
        
        public string Issue { get; set; }

        public string PublicationCity { get; set; }        

        public DateTime? ResearchStatusDate { get;  set; }

        public string ResearchStatusDescription { get;  set; }

        public string ResearchOutputCategoryDescription { get;  set; }

        public string ABSRatingStatusDescription { get;  set; }

        public string DHETStatusDescription { get;  set; }

        public string ArticleTitle { get;  set; }

        public string PublicationName { get;  set; }

        public DateTime? PublicationDate { get; set; }

        public string Country { get; set; }


      // public File PublicationUpload { get; set; }
        //private string fileName;
        //public string FileName
        //{

        //    get { return fileName = PublicationUpload.Name; }
        //    set { fileName = PublicationUpload.Name; }

        //}

        ////Use Filename until i can get Ivor to Adjust the SP
        //private string publicationFile;
        //public string PublicationFile
        //{
        //    //publicationFile = PublicationUpload.Data;

        //    get { return publicationFile = PublicationUpload.Name; }
        //    set { publicationFile = PublicationUpload.Name; }

        //}

        //public class File
        //{
        //    public string Name { get; set; }

        //    public int Size { get; set; }
        //    public string Type { get; set; }
        //    public string Data { get; set; }
        //}

        public int Credit { get;  set; }

        public int CVid { get; set; }
        public string PublicationFile = "some file name hard coded";

        #endregion
    }
   
}
