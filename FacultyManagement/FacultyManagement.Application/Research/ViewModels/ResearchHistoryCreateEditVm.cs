﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using FacultyManagement.Application.Common;
using FacultyManagement.Core.Cache;
using FacultyManagement.Domain.Model.Common;
using FacultyManagement.Domain.Model.Enums;

namespace FacultyManagement.Application.Research.ViewModels
{
    public class ResearchHistoryCreateEditVm : ICreateEditVm
    {
        #region Properties

        public List<ContributorVm> Contributors { get; set; } = new List<ContributorVm>();

        public int? Id { get; set; }

        public int? ResearchId { get; set; }

        public string Description { get; set; }

        [DataType( nameof( ResearchContributionTypeId ) ), DisplayName( "Contribution Type" ), Required]
        public int ResearchContributionTypeId { get; set; }

        [DataType(nameof(ResearchStatusId)), DisplayName("Research Status"), Required]
        public int ResearchStatusId { get; set; }

        [Required]
        public DateTime ResearchStatusDate { get; set; }

        [DataType( nameof( ResearchOutputCategoryId ) ), DisplayName( "Research Category" ), Required]
        public int ResearchOutputCategoryId { get; set; }

        [DataType( nameof( LocusId ) ), DisplayName( "Local / International" ), Required]
        public int LocusId { get; set; }

        [DisplayName( "Research Title" ), Required]
        public string ArticleTitle { get; set; }

        public string PublicationName { get; set; }

        [DataType( nameof( ABSRatingStatusId ) ), DisplayName( "ABS Rating Status" ), Required]
        public int ABSRatingStatusId { get; set; }

        [DataType( nameof( DHETStatusId ) ), DisplayName( "DHET Status" ), Required]
        public int DHETStatusId { get; set; }
        
        [Required]
        public string PublisherName { get; set; }

        public DateTime? PublicationDate { get; set; }

        [DataType("PositiveNumber"), Range(1, 9999, ErrorMessage = "{0} must be 1 or more."), Required]
        [RegularExpression(@"^\d+\.\d{0,2}$"), DisplayName("Hours")]
        public decimal Credit { get; set; } = 1;

        [DataType( "PositiveNumber" ), DisplayName( "Total Pages in Book" )]
        public int NoOfPages { get; set; }

        [DataType("PositiveNumber"), DisplayName("Pages From")]
        public int StartPage { get; set; } = 1;

        [DataType( "PositiveNumber" ), DisplayName( "Pages To" )]
        public int EndPage { get; set; }

        [DataType( "PositiveNumber" )]
        public int Volume { get; set; }

        [MaxLength( 50 )]
        public string Edition { get; set; }

        [MaxLength( 10 )]
        public string Issue { get; set; }
        public string PublicationCity { get; set; }

        /// <summary>
        /// Gets the countryId using the lookup table and country text.
        /// </summary>
        /// <value>
        /// The country.
        /// </value>
        [DataType("NationalityId"), DisplayName("Country"), Required]
        public int? CountryId
        {
            get
            {
                if (string.IsNullOrEmpty(_country)) return _countryId;

                var data = DropDownsCache.Read<Nationality>(DropDownType.Nationalities);
                return data.FirstOrDefault(x => x.Description == Country)?.Id;
            }
            set => _countryId = value;
        }
        private int? _countryId;

        /// <summary>
        /// Gets the country using the lookup table and countryId.
        /// </summary>
        /// <value>
        /// The country.
        /// </value>
        public string Country
        {
            get
            {
                if (_countryId.HasValue)
                {
                    var data = DropDownsCache.Read<Nationality>(DropDownType.Nationalities);
                    return data.FirstOrDefault(x => x.Id == CountryId.Value)?.Description;
                }
                return _country;
            }
            set => _country = value;
        }
        private string _country;

        public File PublicationUpload { get; set; }

        [DataType( DataType.MultilineText )]
        public string Note { get; set; }
        [DisplayName("Add Me as Contributor?")]
        public bool AddSelfAsContributor { get; set; }
        public int? CVid { get; set; }

        #endregion


        public ResearchHistoryCreateEditVm()
        {
        }

        public ResearchHistoryCreateEditVm( int cvId )
        {
            CVid = cvId;
            ResearchStatusDate = DateTime.Today;
        }


        //private string fileName;
        //public string FileName
        //{

        //    get { return fileName = PublicationUpload.Name; }
        //    set { fileName = PublicationUpload.Name; }

        //}

        //Use Filename until i can get Ivor to Adjust the SP
        private string publicationFile;
        public string PublicationFile
        {
            //publicationFile = PublicationUpload.Data;

            get { return publicationFile = PublicationUpload.Name; }
            set { publicationFile = PublicationUpload.Name; }

        }

       

    }
    public class File
    {
        public string Name { get; set; }
        public int Size { get; set; }
        public string Type { get; set; }
        public string Data { get; set; }
    }

}
