﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using FacultyManagement.Application.Common;
using FacultyManagement.Core.Cache;
using FacultyManagement.Domain.Model.Common;
using FacultyManagement.Domain.Model.Enums;
using FacultyManagement.Domain.Model.Research;

namespace FacultyManagement.Application.Research.ViewModels
{
    public class Step2CreateVm : ICreateEditVm
    {
        #region Properties
        [Required]
        public string PublisherName { get; set; }        
        
        public DateTime? PublicationDate { get; set; }        
        [DataType( "PositiveNumber" ), Required]
        public int? Credit { get; set; }
        [DataType( "PositiveNumber" ), DisplayName( "Total Pages in Book" )]
        public int NoOfPages { get; set; }
        [DataType( "PositiveNumber" ), DisplayName( "Pages From" )]
        public int StartPage { get; set; }
        [DataType( "PositiveNumber" ), DisplayName( "Pages To" )]
        public int EndPage { get; set; }
        [DataType( "PositiveNumber" )]
        public int Volume { get; set; }
        [MaxLength( 50 )]
        public string Edition { get; set; }
        [MaxLength( 10 )]
        public string Issue { get; set; }        
        public string City { get; set; }        

        [DataType( "NationalityId" ), DisplayName( "Country" )]
        public int? CountryId { get; set; }

        /// <summary>
        /// Gets the country using the lookup table and countryId.
        /// </summary>
        /// <value>
        /// The country.
        /// </value>
        public string Country
        {
            get
            {
                if ( !CountryId.HasValue ) return string.Empty;

                var data = DropDownsCache.Read<Nationality>( DropDownType.Nationalities );
                return data.FirstOrDefault( x => x.Id == CountryId.Value )?.Description;
            }
        }

        [DataType(DataType.MultilineText)]
        public string Note { get; set; }

        [DataType( DataType.Upload )]
        public HttpPostedFileBase PublicationUpload { get; set; }


        public int? CVid { get; set; }

        #endregion

        public Step2CreateVm()
        {
        }

        public Step2CreateVm( int cvId )
        {
            CVid = cvId;
            PublicationDate = DateTime.Today;            
        }

        public Step2CreateVm(  int? credit )
        {            
            PublicationDate = DateTime.Today;
            Credit = credit;
        }
    }
}
