﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Core.ViewModels.Interfaces;
using FacultyManagement.Domain.Model.Research;

namespace FacultyManagement.Application.Research.ViewModels
{
    public class ResearchVm : IViewModel
    {
        public List<ResearchHistoryVm> Activities { get; }

        public ResearchVm( List<ResearchHistoryVm> activities )
        {
            Activities = activities;
        }

    }
}
