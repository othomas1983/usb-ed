﻿using System.Collections.Generic;
using FacultyManagement.Application.Admin.ViewModels.Common;
using FacultyManagement.Application.Common;
using FacultyManagement.Application.Research.ViewModels;
using FacultyManagement.Core.Security.Identity;
using FacultyManagement.Core.ViewModels.Interfaces;
using FacultyManagement.Infrastructure.Utilities;

namespace FacultyManagement.Application.Research
{
    public interface IResearchService : IApplicationService
    {
       /// <summary>
        /// Gets the faculty user details.
        /// </summary>
        /// <param name="cvId">As integer.</param>
        /// <returns></returns>
        FacultyUserVm GetFacultyUserDetails( int cvId );

        /// <summary>
        /// Gets the research activity.
        /// </summary>
        /// <param name="itemId">The item identifier.</param>
        /// <returns></returns>
        ResearchHistoryEditVm GetResearchWithContributors( int itemId, IUser<CurrentPerson> person );
    }
}
