﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Core.Security.Identity;
using FacultyManagement.Domain.Model;
using FacultyManagement.Domain.Model.Common;

namespace FacultyManagement.Application.Admin
{
    /// <summary>
    /// Provides functions to retrieve user information for both Faculty Management and Active Directory
    /// </summary>
    public interface IUserService
    {
        /// <summary>
        /// Gets the faculty users from cache or database with filtering out the current user option
        /// </summary>
        /// <returns></returns>
        List<FacultyUser> GetFacultyUsers( IUser<CurrentUser> currentUser = null, bool includeCurrentUser = false );

        /// <summary>
        /// Gets the faculty user by their cVid.
        /// </summary>
        /// <param name="cVid">The c vid.</param>
        /// <returns></returns>
        FacultyUser GetFacultyUser( int cVid );

        /// <summary>
        /// Gets the faculty user by their employeeId.
        /// </summary>
        /// <param name="employeeId">The employee identifier.</param>
        /// <returns></returns>
        FacultyUser GetFacultyUser( string employeeId );

        /// <summary>
        /// Gets the active directory users from cache or loads from active directory.
        /// </summary>
        /// <returns></returns>
        List<ActiveDirectoryUser> GetActiveDirectoryUsers( bool includeGroups = false );

        /// <summary>
        /// Adds the user to group.
        /// </summary>
        /// <param name="samAccountNames">The samAccountNames.</param>
        /// <param name="groupName">Name of the group.</param>
        void AddUsersToGroup( IEnumerable<string> samAccountNames, string groupName );

        /// <summary>
        /// Removes the user from group.
        /// </summary>
        /// <param name="samAccountNames">The samAccountNames.</param>
        /// <param name="groupName">Name of the group.</param>
        void RemoveUsersFromGroup( IEnumerable<string> samAccountNames, string groupName );
    }
}
