﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Application.Admin.ViewModels.Common;
using FacultyManagement.Domain.Model.Common;

namespace FacultyManagement.Application.Admin
{
    public class AddUsersResult
    {
        public bool Success { get; set; } = false;

        public FacultyUserVm FacultyUser { get; set; }
    }
}
