﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using FacultyManagement.Core.ViewModels.Interfaces;
using FacultyManagement.Domain.Model;
using FacultyManagement.Domain.Model.Common;

namespace FacultyManagement.Application.Admin.ViewModels
{
    public class FacultyUsersVm
    {
        #region Properties

        [Display( Name = "User" ), Required]
        public string SelectedUsernameAndCvid { get; set; }

        public IEnumerable<SelectListItem> AvailableUsers
        {
            get => _availableUsers.OrderBy( u => u.Text );
            set => _availableUsers = value;
        }
        private IEnumerable<SelectListItem> _availableUsers;

        #endregion

        public FacultyUsersVm( IEnumerable<FacultyUser> users )
        {
            AvailableUsers = users.Select( u => new SelectListItem
            {
                Text = u.Surname + ", " + u.Description,
                Value = u.SamAccountName + ":" + u.CVid.ToString()
            } );
        }
    }
}
