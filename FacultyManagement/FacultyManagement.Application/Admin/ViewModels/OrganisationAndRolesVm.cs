﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using FacultyManagement.Application.Admin.ViewModels.Common;
using FacultyManagement.Application.Common;
using FacultyManagement.Core.Cache;
using FacultyManagement.Domain.Model.Admin;
using FacultyManagement.Domain.Model.Common;
using FacultyManagement.Domain.Model.Enums;
using FacultyManagement.Infrastructure.Utilities.ExtensionMethods;

namespace FacultyManagement.Application.Admin.ViewModels
{
    public class OrganisationAndRolesVm : ICreateEditVm
    {
        #region Properties

        [DisplayName( "Support Staff" )]
        public IList<SelectListItem> AvailableSupportStaffCategories { get; set; }
        [DisplayName( "Academic Faculty" )]
        public IList<SelectListItem> AvailableAcademicFacultyCategories { get; set; }

        // Needed for sp_FacultyDepartmentCUD to create new record
        public int? FacultyDepartmentId { get; set; } = null;

        public List<int> SelectedSupportStaffIds { get; set; } = new List<int>();
        public List<int> SelectedAcademicFacultyds { get; set; } = new List<int>();

        public int? CVid { get; set; }
        public string Username { get; set; }

        public FacultyUserVm FacultyUser { get; set; }


        #endregion

        public OrganisationAndRolesVm()
        {
        }

        public OrganisationAndRolesVm( FacultyUser user, List<FacultyDepartment> userDepartments )
        {
            CVid = user.CVid;
            Username = user.SamAccountName;
            FacultyUser = new FacultyUserVm( user );

            PopulateLists( userDepartments );
        }

        #region Private Methods

        private void PopulateLists( List<FacultyDepartment> userDepartments )
        {
            var supportStaffItems = DropDownsCache.Read<DepartmentCategory>( DropDownType.DepartmentCategories )
                .Where( c => c.DepartmentId == DepartmentType.SupportStaff.ConvertToInt() );
            var academicFacultyItems = DropDownsCache.Read<DepartmentCategory>( DropDownType.DepartmentCategories )
                .Where( c => c.DepartmentId == DepartmentType.AcademicFaculty.ConvertToInt() );

            // Set the selected items
            var selectedStaffItems = userDepartments
                    .Where( d => d.DepartmentId == DepartmentType.SupportStaff.ConvertToInt() )
                    .Select( x => x.DepartmentCategoryId );

            AvailableSupportStaffCategories = supportStaffItems.Select( x => new SelectListItem
            {
                Value = x.Id.ToString(),
                Text = x.Description,
                Selected = selectedStaffItems.Contains( x.Id )
            } )
              .ToList();

            var selectedAcademicItems = userDepartments
                .Where( d => d.DepartmentId == DepartmentType.AcademicFaculty.ConvertToInt() )
                .Select( x => x.DepartmentCategoryId );

            AvailableAcademicFacultyCategories = academicFacultyItems.Select( x => new SelectListItem
            {
                Value = x.Id.ToString(),
                Text = x.Description,
                Selected = selectedAcademicItems.Contains( x.Id )
            } )
           .ToList();
        }

    }
    #endregion
}
