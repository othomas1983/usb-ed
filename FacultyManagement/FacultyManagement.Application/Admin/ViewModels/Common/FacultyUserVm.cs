﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Domain.Model.Common;

namespace FacultyManagement.Application.Admin.ViewModels.Common
{
    public class FacultyUserVm
    {
        #region Properties

        public int CVid { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string SamAccountName { get; set; }
        public string EmployeeId { get; set; }
        public string Institution { get; set; }

        public string FullName { get; set; }
        #endregion

        public FacultyUserVm( FacultyUser facultyUser )
        {
            CVid = facultyUser.CVid;
            Name = facultyUser.Description;
            Surname = facultyUser.Surname;
            SamAccountName = facultyUser.SamAccountName;
            EmployeeId = facultyUser.EmployeeId;
            Institution = facultyUser.Institution;

            FullName = facultyUser.GetFullName();
        }
    }
}
