﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Core.ViewModels.Interfaces;
using FacultyManagement.Infrastructure.Utilities;
using FacultyManagement.Infrastructure.Utilities.ExtensionMethods;

namespace FacultyManagement.Application.Admin.ViewModels.Common
{
    public class UserSelectBaseVm : IViewModel
    { 

        private string cvId;
        private string username;

        #region Properties

        public string Username
        {
            get
            {
                FacultyHelper.SplitUserDetails( SelectedUsernameAndCvid, out cvId, out username );
                return username;
            }
        }
        public int? CVid
        {
            get
            {

                FacultyHelper.SplitUserDetails( SelectedUsernameAndCvid, out cvId, out username );
                return cvId.AsInteger();
            }
        }
        public string SelectedUsernameAndCvid { get; set; }

        #endregion
    }
}
