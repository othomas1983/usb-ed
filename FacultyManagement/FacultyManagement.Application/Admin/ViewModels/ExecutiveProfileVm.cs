﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Application.Common;
using FacultyManagement.Core.Security.Identity;
using FacultyManagement.Domain.Model.Common;
using System.Web.Mvc;

namespace FacultyManagement.Application.Admin.ViewModels
{
    public class ExecutiveProfileVm : ICreateEditVm
    {
        #region Properties

        [Required]
        [AllowHtml]
        [UIHint("tinymce_full_compressed")]
        public string Biography { get; set; }
        public string FullName { get; set; }

        public int? CVid { get; set; } 

        #endregion

        public ExecutiveProfileVm( FacultyUser user )
        {
            FullName = user.GetFullName();
            Biography = user.Biography;
            CVid = user.CVid;
        }

        public ExecutiveProfileVm()
        {
        }
    }
}
