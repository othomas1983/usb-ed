﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Application.Admin.ViewModels.Common;
using FacultyManagement.Application.Common;
using FacultyManagement.Domain.Model.Admin;
using FacultyManagement.Domain.Model.Common;

namespace FacultyManagement.Application.Admin.ViewModels
{
    public class ClassificationsAndSufficienciesVm : ICreateEditVm
    {
        #region Properties

        [DataType( "FacultyClassificationId" ),DisplayName( "Faculty Classification" ), Required]
        public int? ClassificationId { get; set; }
        [DataType( "FacultySufficiencyId" ), DisplayName( "Faculty Sufficiency" ), Required]
        public int? SufficiencyId { get; set; }

        public string Username { get; set; }
        public int? CVid { get; set; }
        
        public FacultyUserVm FacultyUser { get; set; }
        #endregion

        public ClassificationsAndSufficienciesVm( FacultyUser user, ClassificationAndSufficiency classification )
        {
            Username = user.SamAccountName;
            CVid = user.CVid;
            ClassificationId = classification.ClassificationId;
            SufficiencyId = classification.SufficiencyId;

            FacultyUser = new FacultyUserVm( user );
        }

        public ClassificationsAndSufficienciesVm()
        {
        }
    }
}
