﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using FacultyManagement.Core.ViewModels.Interfaces;
using FacultyManagement.Domain.Model;

namespace FacultyManagement.Application.Admin.ViewModels
{
    public class AddUserVm : IViewModel
    {
        public MultiSelectList AvailableUsers { get; set; }
        [Required]
        public List<string> SelectedEmployeeIds { get; set; }

        public AddUserVm( IEnumerable<ActiveDirectoryUser> users )
        {
            AvailableUsers = new MultiSelectList( users, "EmployeeId", "DisplayName" );
        }

        public AddUserVm()
        {
        }
    }
}
