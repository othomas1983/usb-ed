﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Application.Admin.ViewModels.Common;
using FacultyManagement.Domain.Model.Common;

namespace FacultyManagement.Application.Admin.ViewModels
{
    public class AuthDetailGrouping
    {
        public FacultyUserVm Manager { get; set; }
        public int ManagerCVid { get; set; }
        public string Action { get; set; }
        public string Feature { get; set; }

        public AuthDetailGrouping( FacultyUser manager, string feature, string action )
        {
            Feature = feature;
            Action = action;
            Manager = new FacultyUserVm( manager );
        }
    }
}
