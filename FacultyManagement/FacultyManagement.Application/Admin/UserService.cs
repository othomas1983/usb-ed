﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Application.Properties;
using FacultyManagement.Core.Cache;
using FacultyManagement.Core.Security.Identity;
using FacultyManagement.Data.Interfaces;
using FacultyManagement.Domain.Factories;
using FacultyManagement.Domain.Model;
using FacultyManagement.Domain.Model.Common;
using FacultyManagement.Infrastructure.Utilities.ExtensionMethods;
using NLog;
using System.DirectoryServices.ActiveDirectory;

namespace FacultyManagement.Application.Admin
{
    /// <summary>
    /// Provides functions to read users from Faculty Management or Active Directory
    /// </summary>
    /// <seealso cref="FacultyManagement.Application.Admin.IUserService" />
    public class UserService : IUserService
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly IFacultyUserDbService _dbService;

        private readonly string _adUsername = Settings.Default.ActiveDirectoryUsername;
        private readonly string _adPassword = Settings.Default.ActiveDirectoryPassword;
        private readonly string _adServer = Settings.Default.ActiveDirectoryServer;
        private readonly string _adContainer = Settings.Default.ActiveDirectoryContainer;       


        public UserService( IFacultyUserDbService dbService )
        {
            _dbService = dbService;
        }

        #region Faculty Users

        /// <summary>
        /// Gets the faculty users with filtering out the current user option
        /// </summary>
        /// <returns></returns>
        public List<FacultyUser> GetFacultyUsers( IUser<CurrentUser> currentUser = null, bool includeCurrentUser = false )
        {
            var users = UsersCache.Read<FacultyUser>( this.LoadFacultyUsers );
            // Exclude current user?
            if ( currentUser != null && !includeCurrentUser )
            {
                users = users.Where( u => u.CVid != currentUser.CVid ).ToList();
            }

            return users;
        }

        /// <summary>
        /// Gets the faculty user by their cVid.
        /// </summary>
        /// <param name="cVid">The c vid.</param>
        /// <returns></returns>
        public FacultyUser GetFacultyUser( int cVid )
        {
            return UsersCache.Read<FacultyUser>( this.LoadFacultyUsers )
                .FirstOrDefault( u => u.CVid == cVid );
        }

        /// <summary>
        /// Gets the faculty user by their employeeId.
        /// </summary>
        /// <param name="employeeId">The employee identifier.</param>
        /// <returns></returns>
        public FacultyUser GetFacultyUser( string employeeId )
        {
            return UsersCache.Read<FacultyUser>( this.LoadFacultyUsers )
                .FirstOrDefault( u => !string.IsNullOrEmpty( u.EmployeeId ) && u.EmployeeId.Equals(employeeId) );
        }

        /// <summary>
        /// Loads all faculty users from the database
        /// </summary>
        /// <returns></returns>
        private List<FacultyUser> LoadFacultyUsers()
        {
            var usersDs = _dbService.GetAllFacultyUsers();

            var users = ModelFactory.CreateList<FacultyUser>( usersDs );

            return users;
        }
        #endregion

        #region Active Directory Users

        /// <summary>
        /// Gets the active directory users from cache or reloads from active directory.
        /// </summary>
        /// <returns></returns>
        public List<ActiveDirectoryUser> GetActiveDirectoryUsers( bool includeGroups = false )
        {
            var cacheItemPolicy = new CacheItemPolicy
            {
                AbsoluteExpiration = DateTime.Now.AddMinutes( Settings.Default.ActiveDirectoryCacheTime.AsInteger() )
            };

            var adUsers = UsersCache.Read<ActiveDirectoryUser>( this.LoadActiveDirectoryUsers, cacheItemPolicy );
            if ( includeGroups )
            {
                return adUsers;
            }

            var adUsersWithoutGroups= new List<ActiveDirectoryUser>();
            foreach ( var user in adUsers )
            {
                adUsersWithoutGroups.Add( new ActiveDirectoryUser
                {
                    Description = user.Description,
                    DisplayName = user.DisplayName,
                    EmailAddress = user.EmailAddress,
                    EmployeeId = user.EmployeeId,
                    GivenName = user.GivenName,
                    Surname = user.Surname,
                    VoiceTelephoneNumber = user.VoiceTelephoneNumber,
                    SamAccountName = user.SamAccountName,                    
                } );
            }
            return adUsersWithoutGroups;
        }

        /// <summary>
        /// Loads the active directory users from active directory.
        /// </summary>
        /// <returns></returns>
        private List<ActiveDirectoryUser> LoadActiveDirectoryUsers()
        {
            var principalContext = new PrincipalContext( ContextType.Domain, _adServer, _adContainer, _adUsername, _adPassword );

            List<ActiveDirectoryUser> ADUsers = new List<ActiveDirectoryUser>();

            using ( var searcher = new PrincipalSearcher( new UserPrincipal( principalContext ) ) )
            {
                foreach ( var user in searcher.FindAll().WherePrinicpal().OrderBy( u => u.DisplayName ) )
                {
                    if ( !string.IsNullOrEmpty( user.EmployeeId ) &&
                        // !string.IsNullOrEmpty( user.GivenName ) &&
                         !string.IsNullOrEmpty( user.DisplayName ) )
                    {
                        ADUsers.Add( new ActiveDirectoryUser
                        {
                            Description = user.Description,
                            DisplayName = user.Surname + " " + user.GivenName,
                            EmailAddress = user.EmailAddress,
                            EmployeeId = user.EmployeeId,
                            GivenName = user.GivenName,
                            Surname = user.Surname,
                            VoiceTelephoneNumber = user.VoiceTelephoneNumber,
                            SamAccountName = user.SamAccountName,
                            Groups = user.GetGroups().Select( g => g.Name ).ToList()
                        } );
                    }
                }
            }

            return ADUsers;
        }

        /// <summary>
        /// Adds the user to group.
        /// </summary>
        /// <param name="samAccountNames">The username.</param>
        /// <param name="groupName">Name of the group.</param>
        /// <returns></returns>
        public void AddUsersToGroup( IEnumerable<string> samAccountNames, string groupName )
        {            
            try
            {
                var context = new PrincipalContext( ContextType.Domain, _adServer,  _adUsername, _adPassword );

                using ( context )
                {
                    var group = GroupPrincipal.FindByIdentity( context, groupName );

                    foreach ( var samAccountName in samAccountNames )
                    {
                        group.Members.Add( context, IdentityType.SamAccountName, samAccountName );
                        Logger.Info( $"Username:{samAccountName} added to Group:{groupName}" );

                        // This keeps the cache up to date without having to reload all the active directory users again
                        var adUser = GetActiveDirectoryUsers( includeGroups: true ).FirstOrDefault( a => a.SamAccountName.Equals( samAccountName ) );
                        adUser?.AddGroup( groupName );
                    }

                    group?.Save();
                }
            }
            catch ( Exception ex )
            {
                Logger.Error( $"Add Users to Group Exception:{ex.StackTrace} for Usernames:{samAccountNames}" );  
                throw new ActiveDirectoryOperationException( $"Add Users to Group Exception:{ex.StackTrace} for Usernames:{samAccountNames}" );
            }            
        }

        /// <summary>
        /// Removes the user from group.
        /// </summary>
        /// <param name="samAccountNames">The samAccountNames.</param>
        /// <param name="groupName">Name of the group.</param>
        /// <returns></returns>
        public void RemoveUsersFromGroup( IEnumerable<string> samAccountNames, string groupName )
        {            
            try
            {
                var context = new PrincipalContext( ContextType.Domain, _adServer, _adUsername, _adPassword );

                using ( context )
                {
                    var group = GroupPrincipal.FindByIdentity( context, groupName );

                    foreach ( var samAccountName in samAccountNames )
                    {
                        group.Members.Remove( context, IdentityType.SamAccountName, samAccountName );
                        Logger.Info( $"Username:{samAccountName} removed to Group:{groupName}" );

                        // This keeps the cache up to date without having to reload all the active directory users again
                        var adUser = GetActiveDirectoryUsers( includeGroups: true ).FirstOrDefault( a => a.SamAccountName.Equals( samAccountName ) );
                        adUser?.RemoveGroup( groupName );
                    }

                    group?.Save();
                }
            }
            catch ( Exception ex )
            {
                Logger.Error( $"Remove Users to Group Exception:{ex.StackTrace} for Usernames:{samAccountNames}" );
                throw new ActiveDirectoryOperationException( $"Add Users to Group Exception:{ex.StackTrace} for Usernames:{samAccountNames}" );
            }            
        }
        #endregion        
    }
}
