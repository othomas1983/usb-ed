﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using FacultyManagement.Application.Common.Mappings;
using FacultyManagement.Core.Security.Identity;
using FacultyManagement.Core.ViewModels.Interfaces;
using FacultyManagement.Core.Cache;

namespace FacultyManagement.Application.Common
{
    /// <summary>
    /// Application service base class which provides AutoMapper support and optional (virtual) service methods
    /// </summary>
    public abstract class ApplicationService<TViewModel>  : IApplicationService  where TViewModel : class, IViewModel
    {
        protected readonly IMapper Mapper;

        protected ApplicationService() : this( AutoMapperConfig.GetCustomMapper() ) { }

        protected ApplicationService( IMapper mapper )
        {
            Mapper = mapper;
        }


        #region IApplicationService Methods

        /// <summary>
        /// Gets the view model.
        /// </summary>
        /// <param name="currentPerson"></param>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public virtual IViewModel GetViewModel( IUser<CurrentPerson> currentPerson )
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the view model. May pass in both CurrentPerson and CurrentUser
        /// </summary>
        /// <param name="currentPerson">The current person.</param>
        /// <param name="currentUser">The current user.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public virtual IViewModel GetViewModel( IUser<CurrentPerson> currentPerson, IUser<CurrentUser> currentUser )
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the edit model for the current person
        /// </summary>
        /// <param name="currentPerson">The current person.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public virtual ICreateEditVm GetEditModel( IUser<CurrentPerson> currentPerson )
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the item for the current person
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="itemId">The item identifier.</param>        
        /// <param name="person"></param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public virtual T GetItem<T>( int itemId, IUser<CurrentPerson> person ) where T : ICreateEditVm, new()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the item. May pass in both CurrentPerson and CurrentUser
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="itemId">The item identifier.</param>
        /// <param name="person">The person.</param>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public virtual T GetItem<T>( int itemId, IUser<CurrentPerson> person, IUser<CurrentUser> user ) where T : ICreateEditVm, new()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Saves the changes for the appropriate model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="person"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public virtual bool SaveChanges( ICreateEditVm model, IUser<CurrentPerson> person, IUser<CurrentUser> user )
        {
            throw new NotImplementedException();
        }
        public virtual bool SaveChanges( ICreateEditVm model, IUser<CurrentPerson> person, IUser<CurrentUser> user, out string errorMessage )
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id">The identifier.</param>
        /// <param name="person"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public virtual bool Delete<T>( int id, IUser<CurrentPerson> person, IUser<CurrentUser> user )
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Reset Cache because the model has changed
        /// </summary>
        /// <param name="person"></param>
        protected void FlushViewModel( IUser<CurrentPerson> person ) 
        {
            ViewModelCache.Flush<TViewModel>( person.CVid );
        }

        /// <summary>
        /// Creates the view model using the cvid as a unique identifier
        /// </summary>
        /// <param name="cvId">The c vid.</param>
        /// <returns></returns>
        protected virtual TViewModel CreateViewModel( int cvId )
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Creates the view model. May pass in both CurrentPerson and CurrentUser
        /// </summary>
        /// <param name="person">The person.</param>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        protected virtual TViewModel CreateViewModel( IUser<CurrentPerson> person, IUser<CurrentUser> user )
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
