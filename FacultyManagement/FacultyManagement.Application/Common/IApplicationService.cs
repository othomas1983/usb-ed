﻿using FacultyManagement.Core.Security.Identity;
using FacultyManagement.Core.ViewModels.Interfaces;

namespace FacultyManagement.Application.Common
{
    /// <summary>
    /// Main Interface which all Application Services must implement
    /// </summary>
    public interface IApplicationService
    {
        /// <summary>
        /// Gets the view model for the CurrentPerson.
        /// </summary>
        /// <param name="currentPerson"></param>
        /// <returns></returns>
        IViewModel GetViewModel( IUser<CurrentPerson> currentPerson );

        /// <summary>
        /// Gets the view model. May pass in both CurrentPerson and CurrentUser
        /// </summary>
        /// <param name="currentPerson">The current person.</param>
        /// <param name="currentUser">The current user.</param>
        /// <returns></returns>
        IViewModel GetViewModel( IUser<CurrentPerson> currentPerson, IUser<CurrentUser> currentUser );

        /// <summary>
        /// Gets the edit model.
        /// </summary>
        /// <param name="currentPerson">The username.</param>
        /// <returns></returns>
        ICreateEditVm GetEditModel( IUser<CurrentPerson> currentPerson );

        /// <summary>
        /// Gets the item.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="itemId">The item identifier.</param>        
        /// <param name="person"></param>
        /// <returns></returns>
        T GetItem<T>( int itemId, IUser<CurrentPerson> person ) where T : ICreateEditVm, new ();
        T GetItem<T>( int itemId, IUser<CurrentPerson> person, IUser<CurrentUser> currentUser ) where T : ICreateEditVm, new();
        /// <summary>
        /// Saves the changes for the appropriate model.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="model">The model.</param>
        /// <param name="person"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        bool SaveChanges( ICreateEditVm model, IUser<CurrentPerson> person, IUser<CurrentUser> user );
        bool SaveChanges( ICreateEditVm model, IUser<CurrentPerson> person, IUser<CurrentUser> user, out string errorMessage );

        bool Delete<T>( int id, IUser<CurrentPerson> person, IUser<CurrentUser> user );
    }
}