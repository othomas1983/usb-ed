﻿using AutoMapper;
using FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels;
using FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.EditorialBoardMember;
using FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.EngagementWithBusinessAndSociety;
using FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.ExecutiveEducation;
using FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.PartTimeConsulting;
using FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.TeachingExperience;
using FacultyManagement.Application.Admin.ViewModels;
using FacultyManagement.Application.EducationAndAwards.ViewModels.Awards;
using FacultyManagement.Application.EducationAndAwards.ViewModels.Education;
using FacultyManagement.Application.EducationAndAwards.ViewModels.FacultyLanguages;
using FacultyManagement.Application.EducationAndAwards.ViewModels.Grants;
using FacultyManagement.Application.ManagementLoads.ViewModels.Common;
using FacultyManagement.Application.ManagementLoads.ViewModels.ProgrammeHead;
using FacultyManagement.Application.ManagementLoads.ViewModels.TaskTeamMember;
using FacultyManagement.Application.PerformanceManagement.Common;
using FacultyManagement.Application.PerformanceManagement.MyCommitment.ViewModels;
using FacultyManagement.Application.PerformanceManagement.PeerEvaluation.ViewModels;
using FacultyManagement.Application.PerformanceManagement.SelfEvaluation.ViewModels;
using FacultyManagement.Application.PerformanceManagement.SupervisorEvaluation.ViewModels;
using FacultyManagement.Application.PersonalInformation.ViewModels;
using FacultyManagement.Application.ProfessionalAndPersonalDevelopment.ViewModels.ContinuingEducation;
using FacultyManagement.Application.ProfessionalAndPersonalDevelopment.ViewModels.Membership;
using FacultyManagement.Application.ProfessionalAndPersonalDevelopment.ViewModels.NonAcademic;
using FacultyManagement.Application.Research.ViewModels;
using FacultyManagement.Application.SupervisionLoad.ViewModels;
using FacultyManagement.Application.TeachingLoads.ViewModels;
using FacultyManagement.Application.WebUsers.ViewModels;
using FacultyManagement.Data.Models;
using FacultyManagement.Domain.Model.AcademicAndProfessionalExperience;
using FacultyManagement.Domain.Model.EducationAndAwards;
using FacultyManagement.Domain.Model.Loads;
using FacultyManagement.Domain.Model.Common;
using FacultyManagement.Domain.Model.PerformanceManagement;
using FacultyManagement.Domain.Model.Research;
using FacultyManagement.Infrastructure.Utilities.ExtensionMethods;

namespace FacultyManagement.Application.Common.Mappings
{
    public static class AutoMapperConfig
    {
        public static IMapper GetCustomMapper()
        {
            var config = new MapperConfiguration( cfg =>
            {
                // source - destination

                /* Personal Information */
                cfg.CreateMap<Domain.Model.PersonalInformation.PersonalInformation, PersonalInformationVm>();
                cfg.CreateMap<Domain.Model.PersonalInformation.PersonalInformation, PersonalInformationEditVm>();
                cfg.CreateMap<PersonalInformationEditVm, SavePersonalInformation>();
                // .ForMember( dest => dest.AcademicRankId, map => map.MapFrom( src => src.AcademicRank.AsInteger() ) );

                /* Education and Awards */
                cfg.CreateMap<Education, EducationVm>();
                cfg.CreateMap<EducationVm, EducationEditVm>();
                cfg.CreateMap<EducationCreateVm, SaveEducation>();
                cfg.CreateMap<EducationEditVm, SaveEducation>();

                cfg.CreateMap<Award, AwardVm>();
                cfg.CreateMap<AwardVm, AwardEditVm>();
                cfg.CreateMap<AwardCreateVm, SaveAward>();
                cfg.CreateMap<AwardEditVm, SaveAward>();

                cfg.CreateMap<Grant, GrantVm>();
                cfg.CreateMap<GrantVm, GrantEditVm>();
                cfg.CreateMap<GrantCreateVm, SaveGrant>();
                cfg.CreateMap<GrantEditVm, SaveGrant>();

                cfg.CreateMap<FacultyLanguage, FacultyLanguageVm>();
                cfg.CreateMap<FacultyLanguageCreateVm, SaveFacultyLanguage>();

                /* Academic And Professional Experience */
                cfg.CreateMap<Experience, ExperienceVm>();
                /* Teaching Experience */
                cfg.CreateMap<TeachingExperienceCreateVm, SaveExperience>()
                    .ForMember( dest => dest.IsCurrent, opt => opt.MapFrom( src => src.IsCurrent.Bit() ) );
                cfg.CreateMap<TeachingExperienceEditVm, SaveExperience>()
                  .ForMember( dest => dest.IsCurrent, opt => opt.MapFrom( src => src.IsCurrent.Bit() ) );
                cfg.CreateMap<TeachingExperienceVm, TeachingExperienceEditVm>()
                    .ForMember( dest => dest.IsCurrent, opt => opt.MapFrom( src => src.IsCurrent.AsBooleanOrNull() ) );
                /* Executive Education */
                cfg.CreateMap<ExecutiveEducationCreateVm, SaveExperience>()
                    .ForMember( dest => dest.IsCurrent, opt => opt.MapFrom( src => src.IsCurrent.Bit() ) );
                cfg.CreateMap<ExecutiveEducationEditVm, SaveExperience>()
                  .ForMember( dest => dest.IsCurrent, opt => opt.MapFrom( src => src.IsCurrent.Bit() ) );
                cfg.CreateMap<ExecutiveEducationVm, ExecutiveEducationEditVm>()
                    .ForMember( dest => dest.IsCurrent, opt => opt.MapFrom( src => src.IsCurrent.AsBooleanOrNull() ) );
                /* Part Time Consulting */
                cfg.CreateMap<PartTimeConsultingCreateVm, SaveExperience>()
                   .ForMember( dest => dest.IsCurrent, opt => opt.MapFrom( src => src.IsCurrent.Bit() ) );
                cfg.CreateMap<PartTimeConsultingEditVm, SaveExperience>()
                  .ForMember( dest => dest.IsCurrent, opt => opt.MapFrom( src => src.IsCurrent.Bit() ) );
                cfg.CreateMap<PartTimeConsultingVm, PartTimeConsultingEditVm>()
                    .ForMember( dest => dest.IsCurrent, opt => opt.MapFrom( src => src.IsCurrent.AsBooleanOrNull() ) );
                /* Engagement with Business */
                cfg.CreateMap<EngagementWithBusinessCreateVm, SaveExperience>()
                   .ForMember( dest => dest.IsCurrent, opt => opt.MapFrom( src => src.IsCurrent.Bit() ) );
                cfg.CreateMap<EngagementWithBusinessEditVm, SaveExperience>()
                  .ForMember( dest => dest.IsCurrent, opt => opt.MapFrom( src => src.IsCurrent.Bit() ) );
                cfg.CreateMap<EngagementWithBusinessVm, EngagementWithBusinessEditVm>()
                    .ForMember( dest => dest.IsCurrent, opt => opt.MapFrom( src => src.IsCurrent.AsBooleanOrNull() ) );
                /* Editorial Board Member */
                cfg.CreateMap<EditorialBoardMemberCreateVm, SaveExperience>()
                   .ForMember( dest => dest.IsCurrent, opt => opt.MapFrom( src => src.IsCurrent.Bit() ) );
                cfg.CreateMap<EditorialBoardMemberEditVm, SaveExperience>()
                  .ForMember( dest => dest.IsCurrent, opt => opt.MapFrom( src => src.IsCurrent.Bit() ) );
                cfg.CreateMap<EditorialBoardMemberVm, EditorialBoardMemberEditVm>()
                    .ForMember( dest => dest.IsCurrent, opt => opt.MapFrom( src => src.IsCurrent.AsBooleanOrNull() ) );



                /* Profession and Personal Development */
                /* Membership */
                cfg.CreateMap<MembershipCreateVm, SaveDevelopment>()
                    .ForMember( dest => dest.IsCurrent, opt => opt.MapFrom( src => src.IsCurrent.Bit() ) );
                cfg.CreateMap<MembershipEditVm, SaveDevelopment>()
                  .ForMember( dest => dest.IsCurrent, opt => opt.MapFrom( src => src.IsCurrent.Bit() ) );
                cfg.CreateMap<MembershipVm, MembershipEditVm>()
                    .ForMember( dest => dest.IsCurrent, opt => opt.MapFrom( src => src.IsCurrent.AsBooleanOrNull() ) );
                /* Continuing Education */
                cfg.CreateMap<ContinuingEducationCreateVm, SaveDevelopment>()
                    .ForMember( dest => dest.IsCurrent, opt => opt.MapFrom( src => src.IsCurrent.Bit() ) );
                cfg.CreateMap<ContinuingEducationEditVm, SaveDevelopment>()
                  .ForMember( dest => dest.IsCurrent, opt => opt.MapFrom( src => src.IsCurrent.Bit() ) );
                cfg.CreateMap<ContinuingEducationVm, ContinuingEducationEditVm>()
                    .ForMember( dest => dest.IsCurrent, opt => opt.MapFrom( src => src.IsCurrent.AsBooleanOrNull() ) );
                /* Non Academic */
                cfg.CreateMap<NonAcademicCreateVm, SaveDevelopment>()
                    .ForMember( dest => dest.IsCurrent, opt => opt.MapFrom( src => src.IsCurrent.Bit() ) );
                cfg.CreateMap<NonAcademicEditVm, SaveDevelopment>()
                  .ForMember( dest => dest.IsCurrent, opt => opt.MapFrom( src => src.IsCurrent.Bit() ) );
                cfg.CreateMap<NonAcademicVm, NonAcademicEditVm>()
                    .ForMember( dest => dest.IsCurrent, opt => opt.MapFrom( src => src.IsCurrent.AsBooleanOrNull() ) );

                /* Teaching Load */
                cfg.CreateMap<TeachingLoad, TeachingLoadItemVm>();
                cfg.CreateMap<TeachingLoadItemCreateVm, SaveTeachingLoad>();
                cfg.CreateMap<TeachingLoadItemEditVm, SaveTeachingLoad>();
                cfg.CreateMap<TeachingLoadItemVm, TeachingLoadItemEditVm>();
                cfg.CreateMap<TeachingLoadVm, TeachingLoadVm>();

                /*Web User*/
                cfg.CreateMap<WebUser, WebUserItemVm>();
                cfg.CreateMap<WebUserVm, WebUserVm>();

                /* Management Load */
                /* Programme Heads */
                cfg.CreateMap<ProgrammeHeadVm, ProgrammeHeadEditVm>();
                cfg.CreateMap<ProgrammeHeadCreateVm, SaveManagementLoad>();
                cfg.CreateMap<ProgrammeHeadEditVm, SaveManagementLoad>();
                /* Task Team Member */
                cfg.CreateMap<TaskTeamMemberVm, TaskTeamMemberEditVm>();
                cfg.CreateMap<TaskTeamMemberCreateVm, SaveManagementLoad>();
                cfg.CreateMap<TaskTeamMemberEditVm, SaveManagementLoad>();
                /* Centre Head */
                cfg.CreateMap<ManagementLoadGeneralVm, ManagementLoadGeneralEditCreateVm>();
                cfg.CreateMap<ManagementLoadGeneralEditCreateVm, SaveManagementLoad>();


                /* Performance Mangement */
                /* Self Evaluation */
                cfg.CreateMap<EvaluationCreateBaseVm, SaveEvaluation>();
                cfg.CreateMap<EvaluationEditBaseVm, SaveEvaluation>();
                cfg.CreateMap<SelfEvaluation, EvaluationEditBaseVm> ();
                /* Peer Evaluation */
                cfg.CreateMap<PeerEvaluationCreateItemVm, SaveEvaluation>();
                cfg.CreateMap<PeerEvaluationEditItemVm, SaveEvaluation>();
                cfg.CreateMap<PeerEvaluation, PeerEvaluationEditItemVm>();
                /* Supervisor Evaluation */
                cfg.CreateMap<SupervisorEvaluationCreateItemVm, SaveEvaluation>();
                cfg.CreateMap<SupervisorEvaluationEditItemVm, SaveEvaluation>();
                cfg.CreateMap<SupervisorEvaluation, SupervisorEvaluationEditItemVm>();
                /* My Commitments */
                cfg.CreateMap<Commitment, CommitmentVm>();
                cfg.CreateMap<CommitmentVm, MyCommitmenteEditItemVm>();
                cfg.CreateMap<MyCommitmentCreateItemVm, SaveCommitment>();
                cfg.CreateMap<MyCommitmenteEditItemVm, SaveCommitment>();

                /* Research  */
                cfg.CreateMap<ResearchHistory, ResearchHistoryVm>();
                cfg.CreateMap<ResearchHistoryVm, ResearchHistoryEditVm>();
                cfg.CreateMap<ResearchHistoryCreateEditVm, SaveResearchActivity>();
                cfg.CreateMap<ContributorVm, SaveContributor>();
                cfg.CreateMap<Contributor, ContributorVm>();

                /* Department Categories  */
                cfg.CreateMap<OrganisationAndRolesVm, SaveFacultyDepartment>();

                /* Classifications & Sufficiencies  */
                cfg.CreateMap<ClassificationsAndSufficienciesVm, SaveClassifications>();

                /* Executive Profile / Biography */
                cfg.CreateMap<ExecutiveProfileVm, SaveExecutiveProfile>();               
           

                /* Supervision Loads */
                cfg.CreateMap<Domain.Model.Admin.SupervisionLoad, SupervisionLoadItemVm>();
                cfg.CreateMap<SupervisionLoadCreateVm, SaveSupervisionLoad>();
                cfg.CreateMap<SupervisionLoadEditVm, SaveSupervisionLoad>();
                cfg.CreateMap<Domain.Model.Admin.SupervisionLoad, SupervisionLoadEditVm>();
            } );

            return config.CreateMapper();
        }
    }
}
