﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacultyManagement.Application.Common
{
    public class Messages
    {
        public const string LoginError = "There was a problem logging you in.";
        public const string SavingSuccess = "Your changes were successfully saved.";
        public const string SavingError = "There was a problem saving your changes.";
        public const string DeleteSuccess = "Your data was successfully deleted.";
        public const string DeleteError = "There was a problem deleting your data.";
        public const string DbConnectionError = "There was a problem connecting to the database.";
        public const string NoDataError = "There was no data returned.";
    }
}
