﻿using System.Linq;
using FacultyManagement.Application.Common;
using FacultyManagement.Application.ManagementLoads.ViewModels;
using FacultyManagement.Application.ManagementLoads.ViewModels.Common;
using FacultyManagement.Application.ManagementLoads.ViewModels.ProgrammeHead;
using FacultyManagement.Application.ManagementLoads.ViewModels.TaskTeamMember;
using FacultyManagement.Core.Cache;
using FacultyManagement.Core.Security.Identity;
using FacultyManagement.Core.ViewModels.Interfaces;
using FacultyManagement.Data.Interfaces;
using FacultyManagement.Data.Models;
using FacultyManagement.Domain.Factories;
using FacultyManagement.Domain.Model.Loads;

namespace FacultyManagement.Application.ManagementLoads
{
    public class ManagementLoadService : ApplicationService<ManagementLoadVm>, IManagementLoadService
    {
        private readonly ILoadsDbService _dbService;

        public ManagementLoadService(ILoadsDbService dbService)
        {
            _dbService = dbService;
        }

        public override IViewModel GetViewModel(IUser<CurrentPerson> currentPerson)
        {
            return ViewModelCache.Read(currentPerson.CVid, () => CreateViewModel(currentPerson.CVid));
        }

        #region Override Methods

        public override T GetItem<T>(int itemId, IUser<CurrentPerson> person)
        {
            var model = default(T);
            object item = null;

            var viewModel = ViewModelCache.Read(person.CVid, () => CreateViewModel(person.CVid));

            if (typeof(T) == typeof(ProgrammeHeadEditVm))
            {
                item = viewModel.ProgrammeHeads.Single(e => e.Id == itemId);
            }

            if (typeof(T) == typeof(TaskTeamMemberEditVm))
            {
                item = viewModel.TaskTeamMembers.Single(e => e.Id == itemId);
            }

            if (typeof(T) == typeof(ManagementLoadGeneralEditCreateVm))
            {
                item = viewModel.AllGenerals.Single(e => e.Id == itemId);
            }


            model = Mapper.Map<T>(item);
            // Db design issue: Update requires CVID
            (model as ManagementLoadBaseVm).CVid = person.CVid;

            return model;
        }


        public override bool SaveChanges(ICreateEditVm model, IUser<CurrentPerson> person, IUser<CurrentUser> user)
        {
            ISaveModel dbModel = Mapper.Map<SaveManagementLoad>(model);

            bool success = _dbService.SaveChanges(dbModel, user.Username);

            if (success)
            {
                FlushViewModel(person);
            }

            return success;
        }

        public override bool Delete<T>(int id, IUser<CurrentPerson> person, IUser<CurrentUser> user)
        {
            bool success = _dbService.Delete<T>(id, user.Username);

            if (success)
            {
                FlushViewModel(person);
            }

            return success;
        }

        #endregion

        #region Private Methods
        protected override ManagementLoadVm CreateViewModel(int cvId)
        {
            var dataSet = _dbService.GetManagementLoads(cvId);
            var loads = ModelFactory.CreateList<ManagementLoad>(dataSet).OrderBy(t => t.Year).ToList();

            var model = new ManagementLoadVm(loads);

            return model;
        }

        #endregion

    }
}
