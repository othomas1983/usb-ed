﻿using System.Linq;
using FacultyManagement.Application.ManagementLoads.ViewModels.Common;
using FacultyManagement.Core.Cache;
using FacultyManagement.Domain.Model.Enums;
using FacultyManagement.Domain.Model.Loads.Common;

namespace FacultyManagement.Application.ManagementLoads.ViewModels.ProgrammeHead
{
    public class ProgrammeHeadVm : ManagementLoadBaseVm
    {
        #region Properties
        //Changed drop down to use Degree Id

        public int? ProgrammeId { get; set; }
        /// <summary>
        /// Gets the porgramme using the lookup table and ProgrammeId.
        /// </summary>
        public string Programme
        {
            get
            {
                if (!ProgrammeId.HasValue) return string.Empty;

                var data = DropDownsCache.Read<Programme>(DropDownType.Programmes);
                return data.FirstOrDefault(x => x.Id == ProgrammeId.Value)?.Description;
            }
        }

        public int? DegreeId { get; set; }
        /// <summary>
        /// Gets the porgramme using the lookup table and ProgrammeId.
        /// </summary>
        public string Degree
        {
            get
            {
                if (!DegreeId.HasValue) return string.Empty;

                var data = DropDownsCache.Read<Domain.Model.Common.Degree>(DropDownType.Degrees);
                return data.FirstOrDefault(x => x.Id == DegreeId.Value)?.Description;
            }
        }

        public int ElectiveModuleCount { get;  set; }
        public int ElectiveSAQACredit { get;  set; }


        //public int? ProgrammeSizeId { get; set; }
        ///// <summary>
        ///// Gets the porgramme using the lookup table and ProgrammeId.
        ///// </summary>
        //public string ProgrammeSize
        //{
        //    get
        //    {
        //        if (!ProgrammeSizeId.HasValue) return string.Empty;

        //        var data = DropDownsCache.Read<Domain.Model.Common.ProgrammeSize>(DropDownType.ProgrammeSize);
        //        return data.FirstOrDefault(x => x.Id == ProgrammeSizeId.Value)?.Description;
        //    }
        //}

        public string ProgrammeSize { get; set; }
        public string ProgrammeSizeDescription { get; set; }

        #endregion
    }
}
