﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using FacultyManagement.Application.Common;
using FacultyManagement.Application.ManagementLoads.ViewModels.Common;
using FacultyManagement.Core.Cache;
using FacultyManagement.Domain.Model.Common;
using FacultyManagement.Domain.Model.Enums;
using FacultyManagement.Domain.Model.Loads.Common;
using FacultyManagement.Infrastructure.Utilities.ExtensionMethods;

namespace FacultyManagement.Application.ManagementLoads.ViewModels.ProgrammeHead
{
    public class ProgrammeHeadEditVm : ManagementLoadBaseVm, ICreateEditVm
    {
        #region Properties        

        [HiddenInput]
        public int ManagementLoadCategoryId { get; set; } = ManagementLoadType.ProgrammeHead.ConvertToInt();
        [DataType( "PositiveNumber" ), Required]
        public int ElectiveModuleCount { get; set; }
       
        public int ElectiveSAQACredit { get; set; }

        //Changed drop dropdown to use Degree instead
        //[DataType( "ProgrammeId" ), DisplayName( "Programme" ), Required]
        public int? ProgrammeId { get; set; }
        ///// <summary>
        ///// Gets the porgramme using the lookup table and ProgrammeId.
        ///// </summary>
        public string Programme
        {
            get
            {
                if (!ProgrammeId.HasValue) return string.Empty;

                var data = DropDownsCache.Read<Programme>(DropDownType.Programmes);
                return data.FirstOrDefault(x => x.Id == ProgrammeId.Value)?.Description;
            }
        }
        [DataType("DegreeId"), DisplayName("Degree"), Required]
        public int? DegreeId { get; set; }
        /// <summary>
        /// Gets the porgramme using the lookup table and ProgrammeId.
        /// </summary>
        public string Degree
        {
            get
            {
                if (!DegreeId.HasValue) return string.Empty;

                var data = DropDownsCache.Read<Domain.Model.Common.Degree>(DropDownType.Degrees);
                return data.FirstOrDefault(x => x.Id == DegreeId.Value)?.Description;
            }
        }

        [DataType("ProgrammeSizeId"), Required, DisplayName("Programme Size")]
        public int? ProgrammeSizeId { get; set; }
        /// <summary>
        /// Gets the porgramme using the lookup table and ProgrammeId.
        /// </summary>
        public string ProgrammeSize
        {
            get
            {
                if (!ProgrammeSizeId.HasValue) return string.Empty;

                var data = DropDownsCache.Read<Domain.Model.Common.ProgrammeSize>(DropDownType.ProgrammeSize);
                return data.FirstOrDefault(x => x.Id == ProgrammeSizeId.Value)?.Description;
            }
        }

        public string ProgrammeSizeDescription { get; set; }
        #endregion

        public ProgrammeHeadEditVm( int cVid )
        {
            CVid = cVid;
        }

        public ProgrammeHeadEditVm()
        {
        }
    }
}
