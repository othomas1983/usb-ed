﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacultyManagement.Application.ManagementLoads.ViewModels.Common
{
   public class ManagementLoadGeneralVm : ManagementLoadBaseVm
    {
        #region Properties

        public string Description { get; set; }
        public int ManagementLoadCategoryId { get; set; }

        #endregion
    }
}
