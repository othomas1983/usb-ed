﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace FacultyManagement.Application.ManagementLoads.ViewModels.Common
{

    public class ManagementLoadBaseVm : IValidatableObject
    {
        #region Properties

        public int? Id { get; set; }

        public int IsActive { get; set; } = 1;

        [Required]
        public DateTime StartDate { get; set; }

        [Required]
        public DateTime EndDate { get; set; }
               
        public int Credit { get; set; }

        [HiddenInput]
        public int? CVid { get; set; }
        #endregion

        /// <summary>
        /// Determines whether the model is valid.
        /// </summary>
        /// <param name="validationContext">The validation context.</param>
        /// <returns>
        /// A collection that holds failed-validation information.
        /// </returns>
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var validationResults = new List<ValidationResult>();

            if (this.EndDate < this.StartDate)
            {
                validationResults.Add(new ValidationResult("End date must be greater than start date", new[] { "StartDate" }));
            }

            return validationResults;
        }
    }
}
