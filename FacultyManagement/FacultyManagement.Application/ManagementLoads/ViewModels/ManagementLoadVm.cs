﻿using System.Collections.Generic;
using System.Linq;
using FacultyManagement.Application.ManagementLoads.ViewModels.Common;
using FacultyManagement.Application.ManagementLoads.ViewModels.ProgrammeHead;
using FacultyManagement.Application.ManagementLoads.ViewModels.TaskTeamMember;
using FacultyManagement.Core.ViewModels.Interfaces;
using FacultyManagement.Domain.Model.Enums;
using FacultyManagement.Domain.Model.Loads;
using FacultyManagement.Infrastructure.Utilities.ExtensionMethods;

namespace FacultyManagement.Application.ManagementLoads.ViewModels
{
    public class ManagementLoadVm : IViewModel
    {
        public List<ProgrammeHeadVm> ProgrammeHeads { get; set; }
        public List<TaskTeamMemberVm> TaskTeamMembers { get; set; }
        public List<ManagementLoadGeneralVm> CentreHeads { get; set; }
        public List<ManagementLoadGeneralVm> ChairOfAdministrative { get; set; }
        public List<ManagementLoadGeneralVm> OrganisingConferences { get; set; }
        public List<ManagementLoadGeneralVm> Other { get; set; }

        /// <summary>
        /// Gets the list of all ManagementLoadGeneralVm Lists concatenated together.
        /// <seealso cref="ManagementLoadGeneralVm"/>
        /// </summary>
        /// <value>
        /// All generalsget.
        /// </value>
        public List<ManagementLoadGeneralVm> AllGenerals { get; set; }

        public ManagementLoadVm( List<ManagementLoad> loads )
        {
            ProgrammeHeads = loads
                .Where( e => e.ManagementLoadCategoryId == ManagementLoadType.ProgrammeHead.ConvertToInt() )
                .Select( CreateProgrammeHead )
                .ToList();

            TaskTeamMembers = loads
                .Where( e => e.ManagementLoadCategoryId == ManagementLoadType.TaskTeamMember.ConvertToInt() )
                .Select( CreateTaskTeamMember )
                .ToList();

            CentreHeads = loads
                .Where( e => e.ManagementLoadCategoryId == ManagementLoadType.CentreHead.ConvertToInt() )
                .Select( CreateGeneral )
                .ToList();

            ChairOfAdministrative = loads
              .Where( e => e.ManagementLoadCategoryId == ManagementLoadType.ChairOfAdministrative.ConvertToInt() )
              .Select( CreateGeneral )
              .ToList();


            OrganisingConferences = loads
              .Where( e => e.ManagementLoadCategoryId == ManagementLoadType.OrganisingConference.ConvertToInt() )
              .Select( CreateGeneral )
              .ToList();

            Other = loads
             .Where( e => e.ManagementLoadCategoryId == ManagementLoadType.Other.ConvertToInt() )
             .Select( CreateGeneral )
             .ToList();

            AllGenerals = CentreHeads.Concat( ChairOfAdministrative ).Concat( OrganisingConferences ).Concat( Other ).ToList();
        }

        #region Create Methods

        private ProgrammeHeadVm CreateProgrammeHead( ManagementLoad x )
        {
            var vm = new ProgrammeHeadVm
            {
                Id = x.Id,
                StartDate = x.StartDate,
                EndDate = x.EndDate,
                DegreeId = x.DegreeId,  
                ProgrammeSize = x.ProgrammeSize,
                ProgrammeSizeDescription=x.ProgrammeSizeDescription
                
                
           
                
                
                //ElectiveModuleCount = x.ElectiveModuleCount,
                //ElectiveSAQACredit = x.ElectiveSAQACredit,
                //Credit = x.Credit
            };

            return vm;
        }

        private TaskTeamMemberVm CreateTaskTeamMember( ManagementLoad x )
        {
            var vm = new TaskTeamMemberVm
            {
                Id = x.Id,
                StartDate = x.StartDate,
                EndDate = x.EndDate,
                TaskTeamActivityId = x.TaskTeamActivityId,
                TaskTeamActivity = x.TaskTeamActivityDescription,
                Description = x.Description,
                Credit = x.Credit
            };

            return vm;
        }

        private ManagementLoadGeneralVm CreateGeneral( ManagementLoad x )
        {
            var vm = new ManagementLoadGeneralVm
            {
                Id = x.Id,
                StartDate = x.StartDate,
                EndDate = x.EndDate,
                Description = x.Description,
                Credit = x.Credit,
                ManagementLoadCategoryId = x.ManagementLoadCategoryId
            };

            return vm;
        }
        
        #endregion
    }
}
