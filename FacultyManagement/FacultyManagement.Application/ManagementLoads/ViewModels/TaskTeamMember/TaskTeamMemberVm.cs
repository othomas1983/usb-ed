﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Application.ManagementLoads.ViewModels.Common;

namespace FacultyManagement.Application.ManagementLoads.ViewModels.TaskTeamMember
{
    public class TaskTeamMemberVm : ManagementLoadBaseVm
    {
        #region Properties
        
        public int TaskTeamActivityId { get; set; }
        public string TaskTeamActivity { get; set; }
        public string Description { get; set; }

        #endregion
    }
}
