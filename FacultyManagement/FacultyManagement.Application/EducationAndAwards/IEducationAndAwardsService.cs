﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Application.Common;
using FacultyManagement.Application.EducationAndAwards.ViewModels;
using FacultyManagement.Application.EducationAndAwards.ViewModels.Awards;
using FacultyManagement.Application.EducationAndAwards.ViewModels.Education;
using FacultyManagement.Application.EducationAndAwards.ViewModels.Grants;
using FacultyManagement.Core.ViewModels.Interfaces;

namespace FacultyManagement.Application.EducationAndAwards
{
    public interface IEducationAndAwardsService : IApplicationService
    {
        ICreateEditVm GetLanguageCreateViewModel( int cVid );
    }
}
