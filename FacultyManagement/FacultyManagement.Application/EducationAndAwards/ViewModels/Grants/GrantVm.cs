﻿namespace FacultyManagement.Application.EducationAndAwards.ViewModels.Grants
{
    public class GrantVm
    {
        #region Properties

        public int Id { get; set; }
        public int Year { get; set; }
        public string Description { get; set; }
        public string GrantFunding { get; set; }
        public string Country { get; set; }
        public int IsActive { get; set; }

        #endregion
    }
}
