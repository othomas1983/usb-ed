﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using FacultyManagement.Application.Common;
using FacultyManagement.Core.Cache;
using FacultyManagement.Domain.Model.Common;
using FacultyManagement.Domain.Model.Enums;

namespace FacultyManagement.Application.EducationAndAwards.ViewModels.Grants
{
    public class GrantCreateVm : ICreateEditVm
    {
        #region Properties

        /// <summary>
        /// Id is set to null for insert stored procedure.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int? Id { get;} = null;
        [DataType( "YearDatePicker" ), Required]
        public int? Year { get; set; }
        [Required]
        public string GrantFunding { get; set; }
        [Required]
        public string Description { set; get; }
        public int IsActive { get; set; } = 1;
        [DataType( "NationalityId" ), DisplayName( "Country" ), Required]
        public int? CountryId { get; set; }

        /// <summary>
        /// Gets the country using the lookup table and countryId.
        /// </summary>
        /// <value>
        /// The country.
        /// </value>
        public string Country
        {
            get
            {
                if ( !CountryId.HasValue ) return string.Empty;

                var data = DropDownsCache.Read<Nationality>( DropDownType.Nationalities );
                return data.FirstOrDefault( x => x.Id == CountryId.Value )?.Description;
            }
        }
        public int? CVid { get; set; }

        public GrantCreateVm( int cVid )
        {
            CVid = cVid;
        }

        // Required by MVC Model Binder
        public GrantCreateVm()
        {
        }

        #endregion
    }
}
