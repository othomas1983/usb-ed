﻿namespace FacultyManagement.Application.EducationAndAwards.ViewModels.Education
{
    public class EducationVm 
    {
        #region Properties

        public int Id { get; set; }
        public int Year { get;  set; }
        public string Degree { get;  set; }
        public string FieldOfStudy { get;  set; }
        public string University { get;  set; }
        public string Country { get;  set; }
        public int IsActive { get;  set; }

        #endregion
    }
}
