﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using FacultyManagement.Application.Common;
using FacultyManagement.Core.Cache;
using FacultyManagement.Domain.Model.Common;
using FacultyManagement.Domain.Model.Enums;

namespace FacultyManagement.Application.EducationAndAwards.ViewModels.Education
{
    public class EducationEditVm : ICreateEditVm
    {
        #region Properties

        
        public int Id { get; set; }
        [DataType( "YearDatePicker" ), Required]
        public int Year { get; set; }
        [Required]
        public string Degree { get; set; }
        [Required, DisplayName( "" )]
        public string FieldOfStudy { get; set; }
        [Required]
        public string University { get; set; }
        public int IsActive { get; set; } = 1;

        /// <summary>
        /// Gets the countryId using the lookup table and country text.
        /// </summary>
        /// <value>
        /// The country.
        /// </value>
        [DataType( "NationalityId" ), DisplayName( "Country" ), Required]
        public int? CountryId
        {
            get
            {
                if ( string.IsNullOrEmpty( _country ) ) return _countryId;

                var data = DropDownsCache.Read<Nationality>( DropDownType.Nationalities );
                return data.FirstOrDefault( x => x.Description == Country )?.Id;
            }
            set => _countryId = value;
        }
        private int? _countryId;

        /// <summary>
        /// Gets the country using the lookup table and countryId.
        /// </summary>
        /// <value>
        /// The country.
        /// </value>
        public string Country
        {
            get
            {
                if ( _countryId.HasValue )
                {
                    var data = DropDownsCache.Read<Nationality>( DropDownType.Nationalities );
                    return data.FirstOrDefault( x => x.Id == CountryId.Value )?.Description;
                }
                return _country;
            }
            set => _country = value;
        }
        private string _country;

        public int? CVid { get; set; }

        #endregion
    }
}
