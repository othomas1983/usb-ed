﻿namespace FacultyManagement.Application.EducationAndAwards.ViewModels.Awards
{
   public  class AwardVm
    {
        #region Properties

        public int Id { get; set; }
        public int Year { get; set; }
        public string Description { get; set; }
        public string AwardType { get; set; }
        public string Country { get; set; }
        public int IsActive { get; set; }

        #endregion
    }
}
