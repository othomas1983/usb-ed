﻿using System.Collections.Generic;
using FacultyManagement.Application.EducationAndAwards.ViewModels.Awards;
using FacultyManagement.Application.EducationAndAwards.ViewModels.Education;
using FacultyManagement.Application.EducationAndAwards.ViewModels.FacultyLanguages;
using FacultyManagement.Application.EducationAndAwards.ViewModels.Grants;
using FacultyManagement.Core.ViewModels.Interfaces;

namespace FacultyManagement.Application.EducationAndAwards.ViewModels
{
    public class EducationAndAwardsVm : IViewModel
    {
        public List<EducationVm> Educations { get; set; }
        public List<AwardVm> Awards { get; set; }
        public List<GrantVm> Grants { get; set; }
        public List<FacultyLanguageVm> Languages { get; set; }

        public EducationAndAwardsVm( List<EducationVm> educations, List<AwardVm> awards,
                                                              List<GrantVm> grants, List<FacultyLanguageVm> languages )
        {
            Educations = educations;
            Awards = awards;
            Grants = grants;
            Languages = languages;
        }
    }
}
