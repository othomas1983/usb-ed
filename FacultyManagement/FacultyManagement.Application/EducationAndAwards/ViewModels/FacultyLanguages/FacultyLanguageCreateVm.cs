﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using FacultyManagement.Application.Common;
using FacultyManagement.Core.Cache;
using FacultyManagement.Domain.Model.Common;
using FacultyManagement.Domain.Model.Enums;

namespace FacultyManagement.Application.EducationAndAwards.ViewModels.FacultyLanguages
{
    public class FacultyLanguageCreateVm : ICreateEditVm
    {
        #region Properties

        public int? Id { get; set; }
        public int IsActive { get; set; } = 1;
        public int? CVid { get; set; }

        /// <summary>
        /// Gets or sets the selected language ids.
        /// Used remove from the dropdown list selection
        /// </summary>
        /// <value>
        /// The selected language ids.
        /// </value>
        [DataType( nameof( LanguageIds ) ), DisplayName( "Language" ), Required]
        public List<int> LanguageIds { get; set; }

        #endregion

        public FacultyLanguageCreateVm( int cVid )
        {
            CVid = cVid;
        }

        // Required by MVC Model Binder
        public FacultyLanguageCreateVm()
        {
        }
    }
}
