﻿namespace FacultyManagement.Application.EducationAndAwards.ViewModels.FacultyLanguages
{
    public class FacultyLanguageVm
    {
        #region Properties

        public int Id { get; set; }
        public string Description { get; set; }
        public int LanguageId { get; set; }
        public int IsActive { get; set; }

        #endregion
    }
}
