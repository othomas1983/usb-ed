﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Application.Common;
using FacultyManagement.Application.EducationAndAwards.ViewModels;
using FacultyManagement.Application.EducationAndAwards.ViewModels.Awards;
using FacultyManagement.Application.EducationAndAwards.ViewModels.Education;
using FacultyManagement.Application.EducationAndAwards.ViewModels.FacultyLanguages;
using FacultyManagement.Application.EducationAndAwards.ViewModels.Grants;
using FacultyManagement.Core.Cache;
using FacultyManagement.Core.Security.Identity;
using FacultyManagement.Core.ViewModels.Interfaces;
using FacultyManagement.Data.Interfaces;
using FacultyManagement.Data.Models;
using FacultyManagement.Domain.Factories;
using FacultyManagement.Domain.Model;
using FacultyManagement.Domain.Model.EducationAndAwards;

namespace FacultyManagement.Application.EducationAndAwards
{
    public class EducationAndAwardsService : ApplicationService<EducationAndAwardsVm>, IEducationAndAwardsService
    {
        private readonly IEducationAndAwardsDbService _dbService;

        public EducationAndAwardsService( IEducationAndAwardsDbService dbService )
        {
            _dbService = dbService;
        }

        /// <summary>
        /// Gets the view model.
        /// </summary>        
        /// <returns></returns>
        public override IViewModel GetViewModel( IUser<CurrentPerson> currentPerson )
        {
            // Create a new view model so that new updated data is loaded
            return ViewModelCache.Read( currentPerson.CVid, () => CreateViewModel( currentPerson.CVid ) );
        }

        #region Override Methods

        /// <summary>
        /// Gets the item.
        /// </summary>
        /// <param name="itemId"></param>
        /// <param name="currentPerson"></param>
        /// <returns></returns>
        public override T GetItem<T>( int itemId, IUser<CurrentPerson> currentPerson )
        {
            var model = default( T );
            object item = null;

            var viewModel = ViewModelCache.Read( currentPerson.CVid, () => CreateViewModel( currentPerson.CVid ) );

            if ( typeof( T ) == typeof( EducationEditVm ) )
            {
                item = viewModel.Educations.Single( e => e.Id == itemId );
            }

            if ( typeof( T ) == typeof( AwardEditVm ) )
            {
                item = viewModel.Awards.Single( e => e.Id == itemId );
            }

            if ( typeof( T ) == typeof( GrantEditVm ) )
            {
                item = viewModel.Grants.Single( e => e.Id == itemId );
            }

            model = Mapper.Map<T>( item );

            return model;
        }


        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id">The identifier.</param>
        /// <param name="currentPerson"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public override bool Delete<T>( int id, IUser<CurrentPerson> currentPerson, IUser<CurrentUser> user )
        {
            //return _dbService.Delete<T>( id, user.Username );
            bool success = _dbService.Delete<T>(id, user.Username);

            if (success)
            {
                FlushViewModel(currentPerson);
            }

            return success;
        }

        /// <summary>
        /// Saves the changes for the appropriate model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="currentPerson"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public override bool SaveChanges( ICreateEditVm model, IUser<CurrentPerson> currentPerson, IUser<CurrentUser> user )
        {
            ISaveModel dbModel = null;
            // Education
            if ( model is EducationCreateVm || model is EducationEditVm )
            {
                dbModel = Mapper.Map<SaveEducation>( model );
            }
            // Awards
            if ( model is AwardCreateVm || model is AwardEditVm )
            {
                dbModel = Mapper.Map<SaveAward>( model );
            }
            // Grants
            if ( model is GrantCreateVm || model is GrantEditVm )
            {
                dbModel = Mapper.Map<SaveGrant>( model );
            }
            // Faculty Languages
            if ( model is FacultyLanguageCreateVm )
            {
                dbModel = Mapper.Map<SaveFacultyLanguage>( model );
            }

            bool success = _dbService.SaveChanges( dbModel, user.Username );
            if ( success )
            {
                // Reset Cache because the model has changed
                FlushViewModel( currentPerson);
            }
            
            return success;
        }

#endregion

        #region Languages

        /// <summary>
        /// Gets the language create view model with prepoulated selected language ids.
        /// </summary>
        /// <param name="cVid">The c vid.</param>
        /// <returns></returns>
        public ICreateEditVm GetLanguageCreateViewModel( int cVid )
        {
            var viewModel = ViewModelCache.Read( cVid, () => CreateViewModel( cVid ) );

            var model = new FacultyLanguageCreateVm( cVid );
            model.LanguageIds = viewModel.Languages.Select( x => x.LanguageId ).ToList();

            return model;
        }

        #endregion


        #region Private Methods

        /// <summary>
        /// Creates the view model for the specific user.
        /// </summary>
        /// <param name="cvId">The c vid.</param>
        /// <returns></returns>
        protected override EducationAndAwardsVm CreateViewModel( int cvId )
        {
            // Get Educations
            var educationDs = _dbService.GetEducations( cvId );
            var educations = ModelFactory.CreateList<Education>( educationDs );
            var educationsVm = Mapper.Map<List<EducationVm>>( educations );

            // Get Awards
            var awardsDs = _dbService.GetAwards( cvId );
            var awards = ModelFactory.CreateList<Award>( awardsDs );
            var awardsVm = Mapper.Map<List<AwardVm>>( awards );

            // Get Grants
            var grantsDs = _dbService.GetGrants( cvId );
            var grants = ModelFactory.CreateList<Grant>( grantsDs );
            var grantsVm = Mapper.Map<List<GrantVm>>( grants );

            // Get Teaching Languages
            var languagesDs = _dbService.GetLanguages( cvId );
            var languages = ModelFactory.CreateList<FacultyLanguage>( languagesDs );
            var languagesVm = Mapper.Map<List<FacultyLanguageVm>>( languages );

            // Create the view model
            var model = new EducationAndAwardsVm( educationsVm, awardsVm, grantsVm, languagesVm );

            return model;
        }

        #endregion
    }
}
