﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Infrastructure.Network;
using NLog;

namespace FacultyManagement.Application.SupervisionLoad
{
    public class NetworkService : INetworkService
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly string _writeHandUrl = ConfigurationSettings.AppSettings["WritehandAssignmentsURL"];

        public string ReadWriteHandData( string employeeId )
        {

            string data = string.Empty;
            try
            {
                using ( WebClient webClient = new WebClient() )
                {
                    webClient.Encoding = Encoding.UTF8;
                    webClient.Headers.Add( "Content-Type", "application/json" );
                    webClient.QueryString.Add( "employeeId", employeeId );

                    data = webClient.DownloadString( _writeHandUrl );
                }
            }
            catch ( WebException wex )
            {
                Logger.Error( $"{wex.StackTrace}, for employeeId: {employeeId}" );
            }

            return data;
        }
    }
}
