﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Application.Admin;
using FacultyManagement.Application.Common;
using FacultyManagement.Data.Services;
using FacultyManagement.Infrastructure;
using FacultyManagement.Core.Cache;
using FacultyManagement.Domain.Model.Loads.Common;
using FacultyManagement.Domain.Model.Enums;

namespace FacultyManagement.Application.SupervisionLoad.ViewModels
{
    public class SupervisionLoadCreateVm : ICreateEditVm
    {
        #region Properties

        public int? Id { get; set; } 
        public int? CVid { get; set; }  

        [DataType( FmDataTypes.YearDatePickerLoad ), Required]
        public int? Year { get; set; }

        [Required, DisplayName("No of Students")]
        [RegularExpression("([0-9]+)", ErrorMessage = "Please enter a numeric value.")]
        public string StudentName { get; set; }

    
        public int YearDescription { get; set; }


    

        [DataType("PMSupervisionResearchId"), DisplayName("Supervision Research"), Required]
        public int? SupervisionResearchId { get; set; }
        public string SupervisionResearch
        {
            get
            {
                if (!SupervisionResearchId.HasValue) return string.Empty;

                var data = DropDownsCache.Read<PMSupervisionResearch>(DropDownType.PMSupervisionResearch);
                return data.FirstOrDefault(x => x.Id == SupervisionResearchId.Value)?.Description;
            }

        }
        public string SupervisionResearchDescription { get; set; }

        [Required, DisplayName("Hours")]

        public decimal Hours { get; set; }     
        #endregion

        public SupervisionLoadCreateVm( int cvId )
        {
           
            CVid = cvId;
        }

        public SupervisionLoadCreateVm()
        {
        }
    }
}
