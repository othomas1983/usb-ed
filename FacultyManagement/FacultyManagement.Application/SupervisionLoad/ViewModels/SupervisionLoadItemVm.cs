﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacultyManagement.Application.SupervisionLoad.ViewModels
{
    public class SupervisionLoadItemVm
    {
        #region Properties

        public int Id { get; set; }
        public int CVid { get;  set; }
        public string FirstSupervisor { get;  set; }
        public int SecondCVid { get;  set; }
        public string SecondSupervisor { get;  set; }
        public int? Year { get;  set; }
        [DisplayName("Student Number")]
        public string StudentName { get;  set; }

        public string SupervisionResearch { get; set; }

        public decimal Hours { get;  set; }

   

        #endregion
    }
}
