﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Application.Common;
using FacultyManagement.Application.SupervisionLoad.ViewModels;
using FacultyManagement.Core.Security.Identity;

namespace FacultyManagement.Application.SupervisionLoad
{
    public interface ISupervisionLoadService : IApplicationService
    {
        /// <summary>
        /// Filters the write hand research with optional year filter.
        /// </summary>
        /// <param name="year">The year.</param>
        /// <param name="currentPerson">The year.</param>
        /// <returns></returns>
        SupervisionLoadsVm FilterViewModel( IUser<CurrentPerson> currentPerson, int? year  );
    }
}
