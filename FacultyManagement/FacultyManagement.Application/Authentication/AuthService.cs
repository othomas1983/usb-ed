﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Security.Claims;
using FacultyManagement.Application.Authentication.ViewModels;
using FacultyManagement.Core.Cache;
using FacultyManagement.Core.Security.Authentication;
using FacultyManagement.Domain.Model.Admin;
using Microsoft.Owin.Security;

namespace FacultyManagement.Application.Authentication
{
    public class AuthService : IAuthService
    {
        private readonly IAuthentication _authenticator;

        public AuthService( IAuthentication authenticator )
        {
            _authenticator = authenticator;
        }

        /// <summary>
        /// Check if username and password matches existing account in AD. 
        /// </summary>
        /// <returns></returns>
        public bool Authenticate( LoginVm model, out string errorMessage )
        {            
            return _authenticator.Authenticate( model.Username, model.Password, out errorMessage );
        }

        /// <summary>
        /// Gets the auths for a user
        /// </summary>
        /// <param name="cvid">The cvid.</param>
        /// <returns></returns>
        public List<Auth> GetAuths( int cvid )
        {
            return AuthCache.Read().Where( a => a.CVid == cvid ).ToList();
        }
    }
}





