﻿
using System.Collections.Generic;
using FacultyManagement.Application.Authentication.ViewModels;
using FacultyManagement.Domain.Model.Admin;

namespace FacultyManagement.Application.Authentication
{
    public interface IAuthService
    {
        /// <summary>
        /// Authenticates the specified user.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        bool Authenticate( LoginVm model, out string errorMessage );

        /// <summary>
        /// Gets the auths for a user
        /// </summary>
        /// <param name="cvid">The cvid.</param>
        /// <returns></returns>
        List<Auth> GetAuths( int cvid );
    }
}