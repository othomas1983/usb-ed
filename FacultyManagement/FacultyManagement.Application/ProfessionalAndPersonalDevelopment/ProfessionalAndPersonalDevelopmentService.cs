﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.EngagementWithBusinessAndSociety;
using FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.ExecutiveEducation;
using FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.PartTimeConsulting;
using FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.TeachingExperience;
using FacultyManagement.Application.Common;
using FacultyManagement.Application.ProfessionalAndPersonalDevelopment.ViewModels;
using FacultyManagement.Application.ProfessionalAndPersonalDevelopment.ViewModels.Common;
using FacultyManagement.Application.ProfessionalAndPersonalDevelopment.ViewModels.ContinuingEducation;
using FacultyManagement.Application.ProfessionalAndPersonalDevelopment.ViewModels.Membership;
using FacultyManagement.Application.ProfessionalAndPersonalDevelopment.ViewModels.NonAcademic;
using FacultyManagement.Core.Cache;
using FacultyManagement.Core.Security.Identity;
using FacultyManagement.Core.ViewModels.Interfaces;
using FacultyManagement.Data.Interfaces;
using FacultyManagement.Data.Models;
using FacultyManagement.Domain.Common;
using FacultyManagement.Domain.Model.AcademicAndProfessionalExperience;
using FacultyManagement.Domain.Factories;
using FacultyManagement.Domain.Model.ProfessionalAndPersonalDevelopment;

namespace FacultyManagement.Application.ProfessionalAndPersonalDevelopment
{
    public class ProfessionalAndPersonalDevelopmentService : ApplicationService<ProfessionalAndPersonalDevelopmentVm>, IProfessionalAndPersonalDevelopmentService
    {
        private readonly IProfessionalAndPersonalDevelopmentDbService _dbService;

        public ProfessionalAndPersonalDevelopmentService( IProfessionalAndPersonalDevelopmentDbService dbService )
        {
            _dbService = dbService;
        }


        public override IViewModel GetViewModel( IUser<CurrentPerson> currentPerson )
        {
            // Create a new view model so that new updated data is loaded
            return ViewModelCache.Read( currentPerson.CVid, () => CreateViewModel( currentPerson.CVid ) );
        }

        #region Override Methods

        public override T GetItem<T>( int itemId, IUser<CurrentPerson> person )
        {
            var model = default( T );
            object item = null;

            var viewModel = ViewModelCache.Read( person.CVid, () => CreateViewModel( person.CVid ) );

            if ( typeof( T ) == typeof( MembershipEditVm ) )
            {
                item = viewModel.Memberships.Single( e => e.Id == itemId );
            }
            if ( typeof( T ) == typeof( ContinuingEducationEditVm ) )
            {
                item = viewModel.ContinuingEducation.Single( e => e.Id == itemId );
            }
            if ( typeof( T ) == typeof( NonAcademicEditVm ) )
            {
                item = viewModel.NonAcademic.Single( e => e.Id == itemId );
            }

            model = Mapper.Map<T>( item );
            // Db design issue: Update requires CVID – no other update does
            (model as DevelopmentBaseVm ).CVid = person.CVid;

            return model;
        }

        /// <summary>
        /// Saves the changes for the appropriate model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="person"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public override bool SaveChanges( ICreateEditVm model, IUser<CurrentPerson> person, IUser<CurrentUser> user )
        {
            ISaveModel dbModel = Mapper.Map<SaveDevelopment>( model );

            bool success = _dbService.SaveChanges( dbModel, user.Username );

            if ( success )
            {
                FlushViewModel( person );
            }

            return success;
        }


        public override bool Delete<T>( int id, IUser<CurrentPerson> person, IUser<CurrentUser> user )
        {
            bool success = false;

            if ( typeof( T ) == typeof( Development ) )
            {
                success =  _dbService.Delete<Development>( id, user.Username );
            }

            if ( success )
            {
                FlushViewModel( person );
            }

            return success;
        }

        #endregion

        #region Protected Methods

        protected override ProfessionalAndPersonalDevelopmentVm CreateViewModel( int cvId )
        {
            var developmentsDs = _dbService.GetDevelopments( cvId );
            var developments = ModelFactory.CreateList<Development>( developmentsDs );

            // Order by end date to get correct order
            var model = new ProfessionalAndPersonalDevelopmentVm( developments.OrderBy( e => e.EndDate ).ToList() );

            return model;
        }
        #endregion
    }
}
