﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using FacultyManagement.Application.Common;
using FacultyManagement.Application.ProfessionalAndPersonalDevelopment.ViewModels.Common;
using FacultyManagement.Core.Cache;
using FacultyManagement.Domain.Model.Common;
using FacultyManagement.Domain.Model.Enums;
using FacultyManagement.Infrastructure.Utilities.ExtensionMethods;

namespace FacultyManagement.Application.ProfessionalAndPersonalDevelopment.ViewModels.NonAcademic
{
    public class NonAcademicCreateVm : DevelopmentBaseVm, ICreateEditVm
    {
        #region Properties

        public int? Id { get; set; } = null;

        public int IsActive { get; set; } = 1;

        [HiddenInput]
        public int DevelopmentCategoryId { get; set; } = DevelopmentCategory.NonAcademic.ConvertToInt();

        [DataType("PositiveNumber"), Range(1, 9999, ErrorMessage = "{0} must be 1 or more."), Required]
        public int NoOfDays { get; set; } = 1;

        [Required]
        public string Accomplishment { get; set; }

        [Required]
        public string Institution { get; set; }

        [DataType( "NationalityId" ), DisplayName( "Country" ), Required]
        public int? CountryId { get; set; }
        /// <summary>
        /// Gets the country using the lookup table and countryId.
        /// </summary>
        /// <value>
        /// The country.
        /// </value>
        public string Country
        {
            get
            {
                if ( !CountryId.HasValue ) return string.Empty;

                var data = DropDownsCache.Read<Nationality>( DropDownType.Nationalities );
                return data.FirstOrDefault( x => x.Id == CountryId.Value )?.Description;
            }
        }
        public bool IsCurrent { get; set; } = false;

        #endregion

        public NonAcademicCreateVm( int cVid )
        {
            StartDate = DateTime.UtcNow.AddDays( -1 );
            EndDate = DateTime.UtcNow;
            CVid = cVid;
        }

        public NonAcademicCreateVm()
        {
        }
    }
}
