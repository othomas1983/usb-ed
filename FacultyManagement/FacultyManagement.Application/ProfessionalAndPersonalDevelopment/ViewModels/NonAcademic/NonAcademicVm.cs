﻿using System;

namespace FacultyManagement.Application.ProfessionalAndPersonalDevelopment.ViewModels.NonAcademic
{
    public class NonAcademicVm
    {
        #region Properties

        public int Id { get; set; }
        public int IsActive { get; set; }
        public DateTime StartDate { get;  set; }
        public DateTime? EndDate { get;  set; }
        public int DevelopmentCategoryId { get;  set; }
        public string Accomplishment { get;  set; }
        public string Topic { get;  set; }
        public string Institution { get;  set; }
        public string Country { get;  set; }        
        public int NoOfDays { get;  set; }
        public string IsCurrent { get;  set; }

        public int CVid { get; set; }
        #endregion
    }
}
