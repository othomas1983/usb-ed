﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using DataAnnotationsExtensions;
using FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.Common;
using FacultyManagement.Application.Common;
using FacultyManagement.Application.ProfessionalAndPersonalDevelopment.ViewModels.Common;
using FacultyManagement.Core.Cache;
using FacultyManagement.Domain.Model.Common;
using FacultyManagement.Domain.Model.Enums;
using FacultyManagement.Infrastructure.Utilities.ExtensionMethods;

namespace FacultyManagement.Application.ProfessionalAndPersonalDevelopment.ViewModels.Membership
{
    public class MembershipCreateVm : DevelopmentBaseVm, ICreateEditVm
    {
        #region Properties

        public int? Id { get; set; } = null;
        public int IsActive { get; set; } = 1;
        [HiddenInput]
        public int DevelopmentCategoryId { get; set; } = DevelopmentCategory.Membership.ConvertToInt();
        [Required]
        public string Accomplishment { get; set; }
        public bool IsCurrent { get; set; } = false;

        #endregion

        public MembershipCreateVm( int cVid )
        {
            StartDate = DateTime.UtcNow.AddDays( -1 );
            EndDate = DateTime.UtcNow;
            CVid = cVid;
        }

        public MembershipCreateVm()
        {
        }
    }
}
