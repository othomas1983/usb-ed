﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacultyManagement.Application.ProfessionalAndPersonalDevelopment.ViewModels.Membership
{
    public class MembershipVm
    {
        #region Properties

        public int Id { get; set; }
        public int IsActive { get; set; }
        public DateTime StartDate { get;  set; }
        public DateTime? EndDate { get; set; }
        public int DevelopmentCategoryId { get;  set; }
        public string Accomplishment { get;  set; }
        public string IsCurrent { get;  set; }

        public int CVid { get; set; }
        #endregion
    }
}
