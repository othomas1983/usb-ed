﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using DataAnnotationsExtensions;
using FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.Common;
using FacultyManagement.Application.Common;
using FacultyManagement.Application.ProfessionalAndPersonalDevelopment.ViewModels.Common;
using FacultyManagement.Core.Cache;
using FacultyManagement.Domain.Model.Common;
using FacultyManagement.Domain.Model.Enums;

namespace FacultyManagement.Application.ProfessionalAndPersonalDevelopment.ViewModels.Membership
{
    public class MembershipEditVm : DevelopmentBaseVm, ICreateEditVm
    {
        #region Properties

        public int Id { get; set; }
        [HiddenInput]
        public int DevelopmentCategoryId { get; set; }
        [Required]
        public string Accomplishment { get; set; }

        [DisplayName( "Current" )]
        public bool IsCurrent { get; set; }
        public int IsActive { get; set; }


        #endregion

        public MembershipEditVm( int cVid )
        {
            CVid = cVid;
        }

        public MembershipEditVm()
        {
        }
    }
}
