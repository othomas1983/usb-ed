﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FacultyManagement.Application.ProfessionalAndPersonalDevelopment.ViewModels.Common
{
    public class DevelopmentBaseVm 
    {
        #region Properties
        [Required]
        public DateTime StartDate { get; set; }
       
        public DateTime? EndDate { get; set; }

        [HiddenInput]
        public int? CVid { get; set; }
        #endregion       
    }
}
