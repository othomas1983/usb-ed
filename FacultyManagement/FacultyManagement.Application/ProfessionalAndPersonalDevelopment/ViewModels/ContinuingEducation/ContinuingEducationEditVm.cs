﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using FacultyManagement.Application.Common;
using FacultyManagement.Application.ProfessionalAndPersonalDevelopment.ViewModels.Common;
using FacultyManagement.Core.Cache;
using FacultyManagement.Domain.Model.Common;
using FacultyManagement.Domain.Model.Enums;
using System;

namespace FacultyManagement.Application.ProfessionalAndPersonalDevelopment.ViewModels.ContinuingEducation
{
    public class ContinuingEducationEditVm : DevelopmentBaseVm, ICreateEditVm
    {
        #region Properties

        public int Id { get; set; }
        [HiddenInput]
        public int DevelopmentCategoryId { get; set; }

        [DataType("PositiveNumber"), Range(1, 9999, ErrorMessage = "{0} must be 1 or more."), Required]
        public int NoOfDays { get; set; } = 1;

        [Required]
        public string Topic { get; set; }

        [Required]
        public string Institution { get; set; }

        /// <summary>
        /// Gets the countryId using the lookup table and country text.
        /// </summary>
        /// <value>
        /// The country.
        /// </value>
        [DataType( "NationalityId" ), DisplayName( "Country" ), Required]
        public int? CountryId
        {
            get
            {
                if ( string.IsNullOrEmpty( _country ) ) return _countryId;

                var data = DropDownsCache.Read<Nationality>( DropDownType.Nationalities );
                return data.FirstOrDefault( x => x.Description == Country )?.Id;
            }
            set => _countryId = value;
        }
        private int? _countryId;

        /// <summary>
        /// Gets the country using the lookup table and countryId.
        /// </summary>
        /// <value>
        /// The country.
        /// </value>
        public string Country
        {
            get
            {
                if ( _countryId.HasValue )
                {
                    var data = DropDownsCache.Read<Nationality>( DropDownType.Nationalities );
                    return data.FirstOrDefault( x => x.Id == CountryId.Value )?.Description;
                }
                return _country;
            }
            set => _country = value;
        }
        private string _country;

        [DisplayName( "Current" )]
        public bool IsCurrent { get; set; }
        public int IsActive { get; set; }


        #endregion

        public ContinuingEducationEditVm( int cVid )
        {
            CVid = cVid;
        }

        public ContinuingEducationEditVm()
        {
        }
    }
}
