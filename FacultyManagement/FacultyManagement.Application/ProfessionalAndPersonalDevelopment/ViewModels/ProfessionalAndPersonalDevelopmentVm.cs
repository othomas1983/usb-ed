﻿using System.Collections.Generic;
using System.Linq;
using FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.EditorialBoardMember;
using FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.EngagementWithBusinessAndSociety;
using FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.ExecutiveEducation;
using FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.PartTimeConsulting;
using FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.TeachingExperience;
using FacultyManagement.Application.ProfessionalAndPersonalDevelopment.ViewModels.ContinuingEducation;
using FacultyManagement.Application.ProfessionalAndPersonalDevelopment.ViewModels.Membership;
using FacultyManagement.Application.ProfessionalAndPersonalDevelopment.ViewModels.NonAcademic;
using FacultyManagement.Core.ViewModels.Interfaces;
using FacultyManagement.Domain.Model.AcademicAndProfessionalExperience;
using FacultyManagement.Domain.Model.Enums;
using FacultyManagement.Domain.Model.ProfessionalAndPersonalDevelopment;
using FacultyManagement.Infrastructure.Utilities.ExtensionMethods;

namespace FacultyManagement.Application.ProfessionalAndPersonalDevelopment.ViewModels
{
    public class ProfessionalAndPersonalDevelopmentVm : IViewModel
    {
        public List<MembershipVm> Memberships;
        public List<ContinuingEducationVm> ContinuingEducation;
        public List<NonAcademicVm> NonAcademic;


        public ProfessionalAndPersonalDevelopmentVm( List<Development> developments )
        {
            Memberships = developments
                .Where( e => e.DevelopmentCategoryId == DevelopmentCategory.Membership.ConvertToInt() )
                .Select( CreateMembership )
                .ToList();

            ContinuingEducation = developments
                .Where( e => e.DevelopmentCategoryId == DevelopmentCategory.ContinuingEducation.ConvertToInt() )
                .Select( CreateContinuingEducation )
                .ToList();

            NonAcademic = developments
              .Where( e => e.DevelopmentCategoryId == DevelopmentCategory.NonAcademic.ConvertToInt() )
              .Select( CreateNonAcademic )
              .ToList();

        }


        #region Create View Models

        private MembershipVm CreateMembership( Development x )
        {
            var vm = new MembershipVm
            {
                Id = x.Id,
                IsActive = x.IsActive,
                StartDate = x.StartDate,
                EndDate = x.EndDate,
                Accomplishment = x.Accomplishment,
                IsCurrent = x.IsCurrent.ToYesNo(),
                DevelopmentCategoryId = x.DevelopmentCategoryId
            };

            return vm;
        }

        private ContinuingEducationVm CreateContinuingEducation( Development x )
        {
            var vm = new ContinuingEducationVm
            {
                Id = x.Id,
                IsActive = x.IsActive,
                StartDate = x.StartDate,
                EndDate = x.EndDate,
                Topic = x.Topic,
                NoOfDays = x.NoOfDays,
                Institution = x.Institution,
                Country = x.Country,
                IsCurrent = x.IsCurrent.ToYesNo(),
                DevelopmentCategoryId = x.DevelopmentCategoryId
            };

            return vm;
        }

        private NonAcademicVm CreateNonAcademic( Development x )
        {
            var vm = new NonAcademicVm
            {
                Id = x.Id,
                IsActive = x.IsActive,
                StartDate = x.StartDate,
                EndDate = x.EndDate,
                Accomplishment = x.Accomplishment,
                NoOfDays = x.NoOfDays,
                Institution = x.Institution,
                Country = x.Country,
                IsCurrent = x.IsCurrent.ToYesNo(),
                DevelopmentCategoryId = x.DevelopmentCategoryId
            };

            return vm;
        }

       
        #endregion

    }
}
