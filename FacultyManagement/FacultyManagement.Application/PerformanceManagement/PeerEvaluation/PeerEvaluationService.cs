﻿using System.Linq;
using FacultyManagement.Application.Admin;
using FacultyManagement.Application.Common;
using FacultyManagement.Application.PerformanceManagement.PeerEvaluation.ViewModels;
using FacultyManagement.Application.PerformanceManagement.SelfEvaluation;
using FacultyManagement.Core.Cache;
using FacultyManagement.Core.Security.Identity;
using FacultyManagement.Core.ViewModels.Interfaces;
using FacultyManagement.Data.Interfaces;
using FacultyManagement.Data.Models;
using FacultyManagement.Domain.Factories;
using FacultyManagement.Domain.Model.Common;

namespace FacultyManagement.Application.PerformanceManagement.PeerEvaluation
{
    public class PeerEvaluationService : ApplicationService<PeerEvaluationVm>, IPeerEvaluationService
    {
        private readonly IPerformanceManagementDbService _dbService;
        private readonly IUserService _userService;

        public PeerEvaluationService(IPerformanceManagementDbService dbService, IUserService userService)
        {
            _dbService = dbService;
            _userService = userService;
        }


        public override IViewModel GetViewModel(IUser<CurrentPerson> peerPerson, IUser<CurrentUser> reviewUser)
        {
            return ViewModelCache.Read(peerPerson.CVid, () => CreateViewModel(peerPerson, reviewUser));
        }

        public ICreateEditVm GetCreateVm(string category, IUser<CurrentPerson> peerPerson, IUser<CurrentUser> reviewUser)
        {
            var selectedYears = ViewModelCache.Read(peerPerson.CVid, () => CreatePeerEvaluationVm(peerPerson, reviewUser))
                                            .AllEvaluations.Where(e => e.Category.Equals(category))
                                            .Select(x => x.Year)
                                            .Distinct();  // <-- current data has duplicate years

            var model = new PeerEvaluationCreateItemVm(category, peerPerson.CVid, reviewUser.CVid, selectedYears);

            return model;
        }

        #region Override Methods

        public override T GetItem<T>(int itemId, IUser<CurrentPerson> peerPerson, IUser<CurrentUser> reviewUser)
        {
            var model = default(T);
            object item = null;

            var viewModel = ViewModelCache.Read(peerPerson.CVid, () => CreateViewModel(peerPerson, reviewUser));

            if (typeof(T) == typeof(PeerEvaluationEditItemVm))
            {
                item = viewModel.AllEvaluations.Single(e => e.Id == itemId);
            }

            model = Mapper.Map<T>(item);
            // This is the person doing the review
            (model as PeerEvaluationEditItemVm).CVid = peerPerson.CVid;

            return model;
        }

        public override bool SaveChanges(ICreateEditVm model, IUser<CurrentPerson> peerPerson, IUser<CurrentUser> user)
        {
            ISaveModel dbModel = Mapper.Map<SaveEvaluation>(model);

            bool success = _dbService.SaveChanges(dbModel, user.Username);

            if (success)
            {
                FlushViewModel(peerPerson);
            }

            return success;
        }

        public override bool Delete<T>(int id, IUser<CurrentPerson> peerPerson, IUser<CurrentUser> user)
        {
            bool success = _dbService.Delete<T>(id, user.Username);

            if (success)
            {
                FlushViewModel(peerPerson);
            }

            return success;
        }

        #endregion

        #region Private Methods
        protected override PeerEvaluationVm CreateViewModel(IUser<CurrentPerson> peerPerson, IUser<CurrentUser> reviewUser)
        {
            return CreatePeerEvaluationVm(peerPerson, reviewUser);
        }

        private PeerEvaluationVm CreatePeerEvaluationVm(IUser<CurrentPerson> peerPerson, IUser<CurrentUser> reviewUser)
        {
            var dataSet = _dbService.GetPeerEvaluations(peerPerson.CVid);
            var evaluations = ModelFactory.CreateList<Domain.Model.PerformanceManagement.PeerEvaluation>(dataSet)
                                         .Where(e => e.CVid == reviewUser.CVid) // <-- Filter only reviews done by the current user
                                         .OrderBy(t => t.Year)
                                        .ToList();

            var peer = _userService.GetFacultyUser(peerPerson.CVid);

            var model = new PeerEvaluationVm(peer, evaluations);

            return model;
        }


        #endregion

    }
}
