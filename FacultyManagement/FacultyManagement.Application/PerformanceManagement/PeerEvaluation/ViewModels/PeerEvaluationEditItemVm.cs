﻿using FacultyManagement.Application.PerformanceManagement.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Application.Common;

namespace FacultyManagement.Application.PerformanceManagement.PeerEvaluation.ViewModels
{
    public class PeerEvaluationEditItemVm : EvaluationEditBaseVm
    {
        #region Properties

        public int PeerCVid { get; set; }

        #endregion

        public PeerEvaluationEditItemVm( string category, int peerCvId, int reviewCvId )
        {
            Category = category;
            PeerCVid = peerCvId;
            CVid = reviewCvId;
        }

        public PeerEvaluationEditItemVm()
        {
        }
    }
}
