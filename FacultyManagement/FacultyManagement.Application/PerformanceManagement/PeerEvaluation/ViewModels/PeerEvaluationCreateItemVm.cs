﻿using FacultyManagement.Application.PerformanceManagement.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacultyManagement.Application.PerformanceManagement.PeerEvaluation.ViewModels
{
    public class PeerEvaluationCreateItemVm : EvaluationCreateBaseVm
    {
        #region Properties

        public int PeerCVid { get; set; }

        #endregion

        public PeerEvaluationCreateItemVm( string category, int peerCvId, int reviewCvId, IEnumerable<int> selectedYears )
            : base( category, reviewCvId, selectedYears )
        {
            PeerCVid = peerCvId;
        }

        public PeerEvaluationCreateItemVm()
        {
        }
    }
}
