﻿using FacultyManagement.Application.PerformanceManagement.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacultyManagement.Application.PerformanceManagement.PeerEvaluation.ViewModels
{
    public class PeerEvaluationItemVm : EvaluationBaseVm
    {
        #region Properties

        public int PeerCVid { get; set; }

        #endregion
    }
}
