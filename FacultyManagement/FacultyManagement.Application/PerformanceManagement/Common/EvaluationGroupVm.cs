﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacultyManagement.Application.PerformanceManagement.Common
{
    /// <summary>
    /// Used to provide Meta Data to Evaluation View Models
    /// This is needed because we are using a level of abtraction and need to know what kind of Evaluation we are dealing with.
    /// This becomes extremely important when editting and creating evaluations
    /// </summary>
    public class EvaluationGroupVm
    {
        /// <summary>
        /// Gets or sets the category.
        /// </summary>
        /// <value>
        /// The category.
        /// </value>
        public string Category { get; set; }

       /// <summary>
        /// Gets or sets the evaluations.
        /// </summary>
        /// <value>
        /// The evaluations.
        /// </value>
        public IEnumerable<IEvaluationVm> Evaluations { get; set; }       
    }
}
