﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacultyManagement.Application.PerformanceManagement.Common
{
    public interface IEvaluationVm
    {
        int? Id { get; set; }
        int Year { get; set; }
        int CVid { get; set; }
        string ValueCommitment { get; set; }
        string Category { get; set; }
        string Description { get; set; }
    }
}
