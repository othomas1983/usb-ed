﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Application.Common;

namespace FacultyManagement.Application.PerformanceManagement.Common
{
    public class EvaluationEditBaseVm : ICreateEditVm
    {
        #region Properties

        public int? Id { get; set; }
        [DataType( "IntegerTextBoxReadOnly" )]
        public int Year { get; set; }
        public int? CVid { get; set; }
        [DataType( "StringTextBoxReadOnly" )]
        public string ValueCommitment { get; set; }
        public string Category { get; set; }
        [Required]
        public string Description { get; set; }

        #endregion

        public EvaluationEditBaseVm( string category, int cVid )
        {
            Category = category;
            CVid = cVid;
        }

        public EvaluationEditBaseVm()
        {
        }
    }
}
