﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Application.Common;

namespace FacultyManagement.Application.PerformanceManagement.Common
{
    public class EvaluationCreateBaseVm : ICreateEditVm
    {
        #region Properties

        public int? Id { get; set; }
        [DataType( "YearDatePickerLoad" ), Required]
        public int? Year => SelectedYears.FirstOrDefault();
        public int? CVid { get; set; }
        [DataType( "Evaluation" ), Required]
        public string ValueCommitment { get; set; }
        public string Category { get; set; }
        [Required]
        public string Description { get; set; }


        /// <summary>
        /// Gets selected years. 
        /// Used to remove these from the drop down to not allow double creating for the same years
        /// </summary>
        /// <value>
        /// The selected years.
        /// </value>
        [DataType( "SelectedYearDatePicker" ), Required]
        public IEnumerable<int> SelectedYears { get; set; }

        #endregion

        public EvaluationCreateBaseVm( string category, int cVid, IEnumerable<int> selectedYears  )
        {
            Category = category;
            CVid = cVid;
            SelectedYears = selectedYears;
        }

        public EvaluationCreateBaseVm()
        {
        }
    }
}
