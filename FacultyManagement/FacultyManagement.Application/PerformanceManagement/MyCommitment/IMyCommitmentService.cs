﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Application.Common;
using FacultyManagement.Domain.Model.PerformanceManagement;

namespace FacultyManagement.Application.PerformanceManagement.MyCommitment
{
    public interface IMyCommitmentService : IApplicationService
    {
        /// <summary>
        /// Gets the performance categories.
        /// </summary>
        /// <returns></returns>
        IEnumerable<PerformanceCategory> GetPerformanceCategories();
    }
}
