﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacultyManagement.Application.PerformanceManagement.MyCommitment.ViewModels
{
    public class CommitmentVm
    {
        #region Properties

        public int? Id { get; set; }
        public int Year { get;  set; }
        public string Description { get; set; }
        public int? PerformanceCategoryId { get;  set; }
        public string PerformanceCategory { get;  set; }
        public string CommitmentValue { get;  set; }
        public string BaseLine { get;  set; }
        public int CVid { get;  set; }

        #endregion
    }
}
