﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using FacultyManagement.Application.Common;

namespace FacultyManagement.Application.PerformanceManagement.MyCommitment.ViewModels
{
    public class MyCommitmentCreateItemVm : ICreateEditVm
    {
        #region Properties

        public int? Id { get; set; }
        [DataType( "YearDatePickerCommitment" ), Required]
        public int Year { get;  set; }
        [DisplayName( "Commitment" ), Required]
        public string CommitmentValue { get; set; }
        [DisplayName( "Notes" )]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Description { get; set; }        
        [DisplayName( "Category" ), Required]
        public int PerformanceCategoryId { get; set; }
        public int? CVid { get; set; }

        // used for dynamic ajax drop down list
        public IEnumerable<SelectListItem> PerformanceCategories { get; set; } = new List<SelectListItem>();

        #endregion

        public MyCommitmentCreateItemVm( int cVid )
        {
            CVid = cVid;
        }

        // Required by MVC Model Binder
        public MyCommitmentCreateItemVm()
        {
        }
    }
}
