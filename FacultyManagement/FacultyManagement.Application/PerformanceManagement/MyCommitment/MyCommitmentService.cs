﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Application.Common;
using FacultyManagement.Application.PerformanceManagement.MyCommitment.ViewModels;
using FacultyManagement.Core.Cache;
using FacultyManagement.Core.Security.Identity;
using FacultyManagement.Core.ViewModels.Interfaces;
using FacultyManagement.Data.Interfaces;
using FacultyManagement.Domain.Factories;
using FacultyManagement.Domain.Model.Enums;
using FacultyManagement.Domain.Model.Loads.Common;
using FacultyManagement.Domain.Model.PerformanceManagement;
using FacultyManagement.Data.Models;

namespace FacultyManagement.Application.PerformanceManagement.MyCommitment
{
    public class MyCommitmentService : ApplicationService<MyCommitmentVm>, IMyCommitmentService
    {
        private readonly IPerformanceManagementDbService _dbService;

        public MyCommitmentService( IPerformanceManagementDbService dbService )
        {
            _dbService = dbService;
        }



        #region Override Methods

        public override IViewModel GetViewModel( IUser<CurrentPerson> currentPerson )
        {
            //return CreateViewModel( currentPerson.CVid );
            return ViewModelCache.Read(currentPerson.CVid, () => CreateViewModel(currentPerson.CVid));
        }

        public override T GetItem<T>( int itemId, IUser<CurrentPerson> person )
        {
            var model = default( T );
            object item = null;

            var viewModel = ViewModelCache.Read( person.CVid, () => CreateViewModel( person.CVid ) );

            if ( typeof( T ) == typeof( MyCommitmenteEditItemVm ) )
            {
                item = viewModel.Commitments.Single( e => e.Id == itemId );
            }

            model = Mapper.Map<T>( item );                       

            return model;
        }

        public override bool SaveChanges(ICreateEditVm model, IUser<CurrentPerson> person, IUser<CurrentUser> user)
        {
            ISaveModel dbModel = Mapper.Map<SaveCommitment>( model );

            bool success = _dbService.SaveChanges( dbModel, user.Username );

            if ( success )
            {
                FlushViewModel( person );
            }

            return success;
        }

        public override bool Delete<T>( int id, IUser<CurrentPerson> person, IUser<CurrentUser> user )
        {
            bool success = _dbService.Delete<T>( id, user.Username );

            if ( success )
            {
                FlushViewModel( person );
            }

            return success;
        }

        #endregion

        #region protected Methods

        protected override MyCommitmentVm CreateViewModel( int cvId )
        {
            var dataSet = _dbService.GetMyCommitments( cvId );
            var commitments = ModelFactory.CreateList<Commitment>( dataSet )
                                            .OrderBy( t => t.Year )
                                            .ToList();

            var commitmentsVm = Mapper.Map<IList<CommitmentVm>>( commitments );

            return new MyCommitmentVm( commitmentsVm );
        }

        #endregion

        #region IMyCommitmentService Methods

        /// <summary>
        /// Gets the performance categories.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<PerformanceCategory> GetPerformanceCategories()
        {
            return DropDownsCache.Read<PerformanceCategory>( DropDownType.PerformanceCategory );
        }

        #endregion
    }
}
