﻿using System.Collections.Generic;
using FacultyManagement.Application.PerformanceManagement.Common;

namespace FacultyManagement.Application.PerformanceManagement.SupervisorEvaluation.ViewModels
{
    public class SupervisorEvaluationCreateItemVm : EvaluationCreateBaseVm
    {
        #region Properties

        public int SupervisorCVid { get; set; }

        #endregion

        public SupervisorEvaluationCreateItemVm( string category, int supervisorCvId, int reviewCvId, IEnumerable<int> selectedYears )
            : base( category, reviewCvId, selectedYears )
        {
            SupervisorCVid = supervisorCvId;
        }

        public SupervisorEvaluationCreateItemVm()
        {
        }
    }
}
