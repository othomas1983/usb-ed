﻿using FacultyManagement.Application.PerformanceManagement.Common;

namespace FacultyManagement.Application.PerformanceManagement.SupervisorEvaluation.ViewModels
{
    public class SupervisorEvaluationItemVm : EvaluationBaseVm
    {
        #region Properties

        public int SupervisorCVid { get; set; }

        #endregion
    }
}
