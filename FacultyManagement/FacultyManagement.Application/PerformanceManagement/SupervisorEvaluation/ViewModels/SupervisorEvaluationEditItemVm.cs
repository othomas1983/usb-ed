﻿using FacultyManagement.Application.PerformanceManagement.Common;

namespace FacultyManagement.Application.PerformanceManagement.SupervisorEvaluation.ViewModels
{
    public class SupervisorEvaluationEditItemVm : EvaluationEditBaseVm
    {
        #region Properties

        public int SupervisorCVid { get; set; }

        #endregion

        public SupervisorEvaluationEditItemVm( string category, int supervisorCvId, int reviewCvId )
        {
            Category = category;
            SupervisorCVid = supervisorCvId;
            CVid = reviewCvId;
        }

        public SupervisorEvaluationEditItemVm()
        {
        }
    }
}
