﻿using System.Linq;
using FacultyManagement.Application.Admin;
using FacultyManagement.Application.Common;
using FacultyManagement.Application.PerformanceManagement.Common;
using FacultyManagement.Application.PerformanceManagement.SelfEvaluation;
using FacultyManagement.Application.PerformanceManagement.SelfEvaluation.ViewModels;
using FacultyManagement.Application.PerformanceManagement.SupervisorEvaluation.ViewModels;
using FacultyManagement.Core.Cache;
using FacultyManagement.Core.Security.Identity;
using FacultyManagement.Core.ViewModels.Interfaces;
using FacultyManagement.Data.Interfaces;
using FacultyManagement.Data.Models;
using FacultyManagement.Domain.Factories;

namespace FacultyManagement.Application.PerformanceManagement.SupervisorEvaluation
{
    public class SupervisorEvaluationService : ApplicationService<SupervisorEvaluationVm>, ISupervisorEvaluationService
    {
        private readonly IPerformanceManagementDbService _dbService;
        private readonly IUserService _userService;

        public SupervisorEvaluationService(IPerformanceManagementDbService dbService, IUserService userService)
        {
            _dbService = dbService;
            _userService = userService;
        }


        public override IViewModel GetViewModel(IUser<CurrentPerson> supervisorPerson, IUser<CurrentUser> reviewUser)
        {
            return ViewModelCache.Read(supervisorPerson.CVid, () => CreateViewModel(supervisorPerson, reviewUser));
        }

        public ICreateEditVm GetCreateVm(string category, IUser<CurrentPerson> peerPerson, IUser<CurrentUser> reviewUser)
        {
            var selectedYears = ViewModelCache.Read(peerPerson.CVid, () => CreateSupervisorEvaluationVm(peerPerson, reviewUser))
                                            .AllEvaluations.Where(e => e.Category.Equals(category))
                                            .Select(x => x.Year)
                                            .Distinct();  // <-- current data has duplicate years

            var model = new SupervisorEvaluationCreateItemVm(category, peerPerson.CVid, reviewUser.CVid, selectedYears);

            return model;
        }

        #region Override Methods

        public override T GetItem<T>(int itemId, IUser<CurrentPerson> person, IUser<CurrentUser> reviewUser)
        {
            var model = default(T);
            object item = null;

            var viewModel = ViewModelCache.Read(person.CVid, () => CreateViewModel(person, reviewUser));

            if (typeof(T) == typeof(SupervisorEvaluationEditItemVm))
            {
                item = viewModel.AllEvaluations.Single(e => e.Id == itemId);
                model = Mapper.Map<T>(item);
                // Db design issue: Update requires CVID
                model.CVid = person.CVid;
            }

            return model;
        }

        public override bool SaveChanges(ICreateEditVm model, IUser<CurrentPerson> supervisorPerson, IUser<CurrentUser> user)
        {
            ISaveModel dbModel = Mapper.Map<SaveEvaluation>(model);

            bool success = _dbService.SaveChanges(dbModel, user.Username);

            if (success)
            {
                FlushViewModel(supervisorPerson);
            }

            return success;
        }

        public override bool Delete<T>(int id, IUser<CurrentPerson> supervisorPerson, IUser<CurrentUser> user)
        {
            bool success = _dbService.Delete<T>(id, user.Username);

            if (success)
            {
                FlushViewModel(supervisorPerson);
            }

            return success;
        }
        #endregion

        #region Private Methods

        protected override SupervisorEvaluationVm CreateViewModel(IUser<CurrentPerson> supervisorPerson, IUser<CurrentUser> reviewUser)
        {
            return CreateSupervisorEvaluationVm(supervisorPerson, reviewUser);
        }


        private SupervisorEvaluationVm CreateSupervisorEvaluationVm(IUser<CurrentPerson> supervisorPerson, IUser<CurrentUser> reviewUser)
        {
            var dataSet = _dbService.GetSupervisorEvaluations(supervisorPerson.CVid);
            var evaluations = ModelFactory.CreateList<Domain.Model.PerformanceManagement.SupervisorEvaluation>(dataSet)
                                         .Where(e => e.CVid == reviewUser.CVid) // <-- Filter only reviews done by the current user
                                         .OrderBy(t => t.Year)
                                        .ToList();

            var supervisor = _userService.GetFacultyUser(supervisorPerson.CVid);

            var model = new SupervisorEvaluationVm(supervisor, evaluations);

            return model;
        }

        #endregion

    }
}
