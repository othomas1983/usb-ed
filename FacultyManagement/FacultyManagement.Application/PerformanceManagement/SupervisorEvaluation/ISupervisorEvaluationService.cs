﻿using FacultyManagement.Application.Common;
using FacultyManagement.Core.Security.Identity;

namespace FacultyManagement.Application.PerformanceManagement.SupervisorEvaluation
{
    public interface ISupervisorEvaluationService : IApplicationService
    {
        /// <summary>
        /// Gets the create view model.
        /// </summary>
        /// <param name="category">The category.</param>
        /// <param name="person">The person.</param>
        /// <param name="user"></param>
        /// <returns></returns>
        ICreateEditVm GetCreateVm( string category, IUser<CurrentPerson> person, IUser<CurrentUser> user );
    }
}
