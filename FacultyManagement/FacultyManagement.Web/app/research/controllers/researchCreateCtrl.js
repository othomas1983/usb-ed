﻿(function () {
    "use strict";
        angular
        .module("facultyManagement")
        .controller("ResearchCreateCtrl", researchCreateCtrl);


    function researchCreateCtrl($http, $uibModal, $log, $window) {
        var vm = this;
        var dataService = $http;
        var modalService = $uibModal;        

        vm.contributors = [];        

        vm.success = false;
        vm.error = false;

        vm.uiState = {
            isFirstTabVisible: true,
            isSecondTabVisible: false,
            isThirdTabVisible: false
        };

        vm.cvid = "";
        // Text box inputs
        vm.ArticleTitle = "";
        vm.PublicationName = "";
        vm.PublisherName = "";
        vm.ResearchStatusDate = new Date().toDateString();
        vm.Credit = "";
        vm.NoOfPages = "";
        vm.StartPage = "";
        vm.EndPage = "";
        vm.Volume = "";
        vm.PublicationCity = "";
        vm.Edition = "";
        vm.Issue = "";
        vm.Note = "";        
        vm.PublicationUpload = {};
        vm.PublicationFile = "";

       
        // Drop downs lists
        vm.ResearchOutputCategories = [];
        vm.ResearchContributionType = [];
        vm.Locus = [];
        vm.ABSRatingStatus = [];
        vm.DHETStatus = [];
        vm.ResearchStatus = [];
        vm.Countries = [];


        vm.dropdowns = {
            selectedResearchOutputCategory: null,
            selectedResearchContributionType: null,
            selectedLocus: null,
            selectedABSRatingStatus: null,
            selectedDHETStatus: null,
            selectedResearchStatus: null,
            selectedCountry: null
        };

        // Initializes the controller via ng-init
        vm.init = function (research) {
            console.log(research);
            vm.research = research;
            
            // Set the selected values on drop downs
            vm.dropdowns.selectedResearchOutputCategory.id = research.researchOutputCategoryId;
            vm.dropdowns.selectedResearchContributionType.id = research.researchContributionTypeId;
            vm.dropdowns.selectedLocus.id = research.locusId;
            vm.dropdowns.selectedABSRatingStatus.id = research.absRatingStatusId;
            vm.dropdowns.selectedDHETStatus.id = research.dhetStatusId;
            vm.dropdowns.selectedResearchStatus.id = research.researchStatusId;
            vm.dropdowns.selectedCountry.id = research.countryId;

            // Set the selected descriptions to be displayed
            vm.dropdowns.selectedResearchOutputCategory.description = research.researchOutputCategoryDescription;
            vm.dropdowns.selectedLocus.description = research.locusDescription;
            vm.dropdowns.selectedABSRatingStatus.description = research.absRatingStatusDescription;
            vm.dropdowns.selectedDHETStatus.description = research.dhetStatusDescription;
            vm.dropdowns.selectedResearchStatus.description = research.researchStatusDescription;
            vm.dropdowns.selectedCountry.description = research.country;

            // Set other values
            vm.cvid = research.cVid;
            vm.id = research.id;   // ResearchHistoryId
            vm.researchId = research.researchId; // ResearchId

            vm.ArticleTitle = research.articleTitle;
            vm.PublicationName = research.publicationName;
            vm.PublicationCity = research.publicationCity;
            vm.PublisherName = research.publisherName;
            vm.ResearchStatusDate = new Date().toDateString();
            vm.Credit = research.credit;
            vm.NoOfPages = research.noOfPages;
            vm.StartPage = research.startPage;
            vm.EndPage = research.endPage;
            vm.Volume = research.volume;
            vm.PublicationCity = research.publicationCity;
            vm.Edition = research.edition;
            vm.Issue = research.issue;
            vm.Note = research.note;
            //alert(research.publicationUpload.file);
            vm.PublicationUpload = research.publicationUpload.file;
            vm.PublicationFile = research.publicationFile;
              
            vm.contributors = research.contributors;

            // Update contributors to add an index column for tracking
            for (var i = 0; i < vm.contributors.length; i++) {
                vm.contributors[i].index = i;
            }
            vm.index = research.contributors.length;
        }


        // Opens the modal
        vm.openInternal = function (size, parentSelector) {
            var modalInstance = modalService.open({
                ariaLabelledBy: "modal-title",
                ariaDescribedBy: "modal-body",
                templateUrl: "/FacultyManagement/app/research/views/internalContributorModal.html",
                controller: "InternalContributorCreateModalCtrl",
                controllerAs: "vmodal",
                size: size,
                // Passed into the modal controller function
                resolve: {
                    items: function () {
                        return vm.contributors;
                    }
                }
            });

            // This is what the modal returns when clicking "ok"
            modalInstance.result.then(function (data) {
                console.log(data);

                var contributor = data.contributor;
                vm.index = vm.contributors.length;
                // Add to the contributors list
                vm.contributors.push(
                    {
                        "index": vm.index++,
                        "id": "",
                        "cvid": contributor.cvid,
                        "author": contributor.author,
                        "sequence": contributor.sequence,
                        "dhetCredit": contributor.dhetCredit,
                        "institution": contributor.institution,
                        "role": contributor.role,
                        "roleId": contributor.roleId,
                        "type": "Internal"
                    }
                );
            },
                function () {
                    $log.info("Modal dismissed at: " + new Date());
                });
        };

        vm.openExternal = function (size, parentSelector) {
            var modalInstance = modalService.open({
                ariaLabelledBy: "modal-title",
                ariaDescribedBy: "modal-body",
                templateUrl: "/FacultyManagement/app/research/views/externalContributorModal.html",
                controller: "ExternalContributorCreateModalCtrl",
                controllerAs: "vmodal",
                size: size,
                // Passed into the modal controller function
                resolve: {
                    items: function () {
                        return vm.contributors;
                    }
                }
            });


            // This is what the modal returns when clicking "ok"
            modalInstance.result.then(function (data) {
                console.log(data);

                var contributor = data.contributor;
                vm.index = vm.contributors.length;
                // Add to the contributors list
                vm.contributors.push(
                    {
                        "index": vm.index++,
                        "id": "",
                        "cvid": "",
                        "author": contributor.author,
                        "sequence": contributor.sequence,
                        "dhetCredit": contributor.dhetCredit,
                        "institution": contributor.institution,
                        "role": contributor.role,
                        "roleId": contributor.roleId,
                        "type": "External",
                        // Exterrnal contributor fields
                        "surname": contributor.surname,
                        "firstname": contributor.firstname,
                        "initials": contributor.initials,
                        "email": contributor.email,
                        "gender": contributor.gender,
                        "affiliationId": contributor.affiliationId,
                        "nationalityId": contributor.nationalityId,
                        
                    }
                );
            },
                function () {
                    $log.info("Modal dismissed at: " + new Date());
                });
        };

        // Remove Contributor from List
        vm.deleteContributor = function(index) {
            $log.info("delete index passed " + index);

            // removes the matching index
            for (var i = 0; i < vm.contributors.length; i++) {
                if (vm.contributors[i].index === index) {
                    vm.contributors.splice(i, 1);
                    break;
                }
            }
        }
        
        // Submits the form
        vm.submit = function (isValid) {
          
            var selectedCountryId;
            if (vm.dropdowns.selectedCountry === null) {
                selectedCountryId = 0;
            }
            else {               
                selectedCountryId = vm.dropdowns.selectedCountry.id;               
            }

            vm.submitted = true;
            if (isValid) {
                var model = {
                    contributors: vm.contributors,
                    cvid: vm.cvid,
                    researchContributionTypeId: vm.dropdowns.selectedResearchContributionType.id,
                    researchStatusId: vm.dropdowns.selectedResearchStatus.id,
                    researchStatusDate: vm.ResearchStatusDate,
                    researchOutputCategoryId: vm.dropdowns.selectedResearchOutputCategory.id,
                    locusId: vm.dropdowns.selectedLocus.id,
                    articleTitle: vm.ArticleTitle,
                    publicationName: vm.PublicationName,
                    absRatingStatusId: vm.dropdowns.selectedABSRatingStatus.id,
                    dhetStatusId: vm.dropdowns.selectedDHETStatus.id,
                    publicationDate: vm.publicationDate,

                    publisherName: vm.PublisherName,
                    credit: vm.Credit,
                    noOfPages: vm.NoOfPages,
                    startPage: vm.StartPage,
                    endPage: vm.EndPage,
                    volume: vm.Volume,
                    edition: vm.Edition,
                    issue: vm.Issue,
                    publicationCity: vm.PublicationCity,
                    //countryId: vm.CountryId,
                    note: vm.Note,                    
                    publicationUpload: vm.PublicationUpload.file,
                    countryId: selectedCountryId
                };
               // alert("dasd"+vm.PublicationUpload.file);
               // alert(vm.PublicationUpload.File);
               // alert(vm.publicationUpload.File);
              //  alert(vm.publicationUpload.file);
                console.log(model);

                dataService.post("/FacultyManagement/Research/SaveResearch", model)
                    .then(function (result) {
                        // Check if successful
                        console.log(result.data);
                        vm.success = result.data.success;

                        if (!vm.success) {
                            vm.error = true;
                        } else {
                            var landingUrl = "http://" + $window.location.host + "/FacultyManagement/Research";
                            // Delay this by 1 second
                            setTimeout(function () {
                                $window.location.href = landingUrl;                            
                            }, 1000);
                        }
                    },
                    function (error) {
                        // handle Exception(error);
                        vm.error = true;
                        vm.submitted = false;
                    });
            } else {
                alert("Please correct the validation errors first.");
            }
        };

        vm.uiStateClick = function (tabNumber) {
            if (tabNumber === 1) {
                vm.uiState.isFirstTabVisible = true;
                vm.uiState.isSecondTabVisible = false;
                vm.uiState.isThirdTabVisible = false;
            }
            if (tabNumber === 2) {
                vm.uiState.isFirstTabVisible = false;
                vm.uiState.isSecondTabVisible = true;
                vm.uiState.isThirdTabVisible = false;
            }
            if (tabNumber === 3) {
                vm.uiState.isFirstTabVisible = false;
                vm.uiState.isSecondTabVisible = false;
                vm.uiState.isThirdTabVisible = true;
            }
        }


        // Functions
        populateDropDowns();

        function populateDropDowns() {
            dataService.get("/FacultyManagement/Research/GetResearchDropDowns")
                .then(function (response) {

                    vm.ResearchOutputCategories = response.data.researchCategories;
                    vm.ResearchContributionType = response.data.researchContributionType;
                    vm.Locus = response.data.locus;
                    vm.ABSRatingStatus = response.data.absRatingStatus;
                    vm.DHETStatus = response.data.dhetStatus;
                    vm.ResearchStatus = response.data.researchStatus;
                    vm.Countries = response.data.countries;

                    console.log(response.data);
                });
        }

    }


}());