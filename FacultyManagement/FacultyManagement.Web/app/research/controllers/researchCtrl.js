﻿(function () {
    "use strict";

    angular
        .module("facultyManagement")
        .controller("ResearchCtrl", researchCtrl);


    function researchCtrl($http, NgTableParams) {
        var vm = this;
        var dataService = $http;
     
        // Variables
        vm.Type = "";
        vm.Title = "";
        vm.PublicationName = "";
        vm.PublicationStanding = "";
        vm.Status = "";
        vm.StatusDate = "";
        vm.Credit = "";

        vm.activities = [];

        vm.editResearch = function( id ) {
            dataService.get("/FacultyManagement/GetResearchActivities/Research/Edit/?id=" + id);
        };

        getResearchActivities();

        
        // Functions
        function getResearchActivities() {
            dataService.get("/FacultyManagement/Research/GetResearchActivities")
                .then(function (response) {
                    
                    vm.activities = response.data.activities;

                    vm.tableParams = new NgTableParams({page:1, count:10 }, { dataset: vm.activities });

                    console.log(vm.activities);
                });
        }

    }


}());