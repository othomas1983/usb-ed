﻿(function () {
    "use strict";

    angular
        .module("facultyManagement")
        .controller("ExternalContributorCreateModalCtrl", externalContributorCreateModalCtrl);

    function externalContributorCreateModalCtrl($http, $uibModalInstance) {
        var vmodal = this;
        var dataService = $http;

        vmodal.contributor = {};

        vmodal.Surname = "";
        vmodal.FirstName = "";
        vmodal.Initials = "";
        vmodal.Gender = "";
        vmodal.Nationality = "";
        vmodal.Email = "";
        vmodal.Affiliation = "";
        vmodal.Credit = "";
        vmodal.Institution = "";

        vmodal.selectedRole = {
            id: 1,
            description: ""
        };

        // Drop downs lists
        vmodal.Roles = [];
        vmodal.Genders = [];
        vmodal.Nationalities = [];
        vmodal.Affiliations = [];

        vmodal.selectedRole = null;
        vmodal.selectedGender = null;
        vmodal.selectedNationality = null;
        vmodal.selectedAffiliation = null;

        vmodal.ok = function () {
            // Construct contributor detail object which will be passed back
            vmodal.contributor = {
                author: vmodal.Surname + ", " + vmodal.FirstName,
                sequence: vmodal.Sequence,
                dhetCredit: vmodal.Credit,
                institution: vmodal.Institution,
                role: vmodal.selectedRole.description,
                roleId: vmodal.selectedRole.id,
                // External Contributor Fields
                surname: vmodal.Surname,
                firstname: vmodal.FirstName,
                initials: vmodal.Initials,
                email: vmodal.Email,
                gender: vmodal.selectedGender.description,
                affiliationId: vmodal.selectedAffiliation.id,
                nationalityId: vmodal.selectedNationality.id,                
            };

            console.log(vmodal.contributor);
            // Send details back to be displayed in parent view
            $uibModalInstance.close({ contributor: vmodal.contributor });
        };


        vmodal.cancel = function () {
            $uibModalInstance.dismiss("cancel");
        };


        populateDropDowns();

        function populateDropDowns() {
            dataService.get("/FacultyManagement/Research/GetExternalContributorDropDowns")
                .then(function (response) {

                    console.log(response.data);

                    vmodal.Roles = response.data.roles;
                    vmodal.Genders = response.data.genders;
                    vmodal.Nationalities = response.data.nationalities;
                    vmodal.Affiliations = response.data.affiliations;
                    vmodal.Credit = 1;

                });
        }

    }

}());