﻿using System.Collections.Generic;
using System.Globalization;

namespace FacultyManagement.Web
{
    internal class RequestLocalizationOptions
    {
        public object DefaultRequestCulture { get; set; }
        public List<CultureInfo> SupportedCultures { get; set; }
        public List<CultureInfo> SupportedUICultures { get; set; }
    }
}