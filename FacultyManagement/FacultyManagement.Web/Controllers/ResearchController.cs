﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FacultyManagement.Application.Common;
using FacultyManagement.Application.Research;
using FacultyManagement.Application.Research.ViewModels;
using FacultyManagement.Core.Cache;
using FacultyManagement.Domain.Model.Common;
using FacultyManagement.Domain.Model.Enums;
using FacultyManagement.Domain.Model.Research;
using FacultyManagement.Infrastructure.Utilities;
using FacultyManagement.Infrastructure.Utilities.ExtensionMethods;
using FacultyManagement.Web.ActionResults;
using FacultyManagement.Web.Infrastructure.Filters;

namespace FacultyManagement.Web.Controllers
{

    public class ResearchController : ControllerBase
    {
        public ResearchController( IResearchService service ) : base( service )
        {
        }


        public ActionResult Index()
        {
            return View();
        }

        #region Research Activity

        // GET: Research Activities
        public ActionResult GetResearchActivities()
        {
            var model = Service.GetViewModel( CurrentPerson ) as ResearchVm;

            return new JsonNetResult( model );
        }

        public ActionResult CreateResearch()
        {
            var model = new ResearchHistoryCreateEditVm( CurrentPerson.CVid );

            return View( model );
        }

        [HttpPost]
        public ActionResult SaveResearch( ResearchHistoryCreateEditVm model )
        {
            if(model.PublicationFile ==null || model.PublicationUpload == null)
            {
                model.PublicationUpload = null;
                model.PublicationFile = null;
            }
            bool success = Service.SaveChanges( model, CurrentPerson, CurrentUser );

            return new JsonNetResult( new { success } );
        }

        // Post : AddInternalContributor
        public ActionResult AddInternalContributor( string selectedUsernameAndCvid )
        {
            string username;
            string cVid;
            FacultyHelper.SplitUserDetails( selectedUsernameAndCvid, out cVid, out username );

            var model = ( (IResearchService) Service ).GetFacultyUserDetails( cVid.AsInteger() );

            return new JsonNetResult( model );
        }

        // Post : AddExternalContributor
        public ActionResult AddExternalContributor(string selectedUsernameAndCvid)
        {
            string username;
            string cVid;
            FacultyHelper.SplitUserDetails(selectedUsernameAndCvid, out cVid, out username);

            var model = ((IResearchService)Service).GetFacultyUserDetails(cVid.AsInteger());

            return new JsonNetResult(model);
        }

        public ActionResult Edit( int id )
        {
            var model = ( (IResearchService) Service ).GetResearchWithContributors( id, CurrentPerson );

            return View( "EditResearch", model );
        }

        public ActionResult GetResearch( int id )
        {
            var model = ( (IResearchService) Service ).GetResearchWithContributors( id, CurrentPerson );

            return new JsonNetResult( model );
        }

        #endregion

        #region Drop downs

        // GET: DropDowns : ResearchOutputCategories
        public ActionResult GetResearchDropDowns()
        {
            var model = new
            {
                ResearchCategories = DropDownsCache.Read<ResearchOutputCategory>( DropDownType.ResearchOutputCategory ),
                ResearchContributionType = DropDownsCache.Read<ResearchContributionType>( DropDownType.ResearchContributionType ),
                Locus = DropDownsCache.Read<Locus>( DropDownType.Locus ),
                ABSRatingStatus = DropDownsCache.Read<ABSRating>( DropDownType.ABSRating ),
                DHETStatus = DropDownsCache.Read<DHETStatus>( DropDownType.DHETStatus ),
                ResearchStatus = DropDownsCache.Read<ResearchStatus>( DropDownType.ResearchStatus ),
                Countries = DropDownsCache.Read<Nationality>( DropDownType.Nationalities ),
            };

            return new JsonNetResult( model );
        }

        public ActionResult GetPersonRolesDropDown()
        {
            var model = new
            {
                Roles = DropDownsCache.Read<PersonRole>( DropDownType.PersonRole ),
            };

            return new JsonNetResult( model );
        }

        public ActionResult GetExternalContributorDropDowns()
        {
            var model = new
            {
                Affiliations = DropDownsCache.Read<Affiliation>( DropDownType.Affiliations ),
                Roles = DropDownsCache.Read<PersonRole>( DropDownType.PersonRole ),
                Nationalities = DropDownsCache.Read<Nationality>( DropDownType.Nationalities ),
                Genders = new[]
                {
                    new {Id = "Male", Description = "Male"},
                    new {Id = "Female", Description = "Female"}
                }
            };

            return new JsonNetResult( model );
        }

        #endregion

        #region Delete

        // GET: ManagementLoads/Delete/5
        public ActionResult Delete( int id )
        {
            return PartialView( "_Delete", id );
        }

        // POST: ManagementLoads/Delete/5
        [HttpPost]
        [ActionName( "Delete" )]
        public ActionResult DeleteConfirmed( int id )
        {
            bool success = Service.Delete<ResearchHistory>( id, CurrentPerson, CurrentUser );

            return Json( new { success } );
        }
        #endregion

        #region Private Methods

        #endregion
    }
}