﻿using System.Web.Mvc;
using FacultyManagement.Application.TeachingLoads;
using FacultyManagement.Application.TeachingLoads.ViewModels;
using FacultyManagement.Domain.Model.Loads;

namespace FacultyManagement.Web.Controllers
{
    public class TeachingLoadController : ControllerBase
    {
        public TeachingLoadController( ITeachingLoadService service ) : base( service )
        {
        }

        // GET: TeachingLoad
        public ActionResult Index( int? year = null )
        {
            // Filter by Year
            var model = ( (ITeachingLoadService) Service ).FilterViewModel( CurrentPerson, year );
          
            return View( model );
        }

        
        // GET: TeachingLoad/Create
        public ActionResult Create()
        {
            var model = new TeachingLoadItemCreateVm( this.CVid );

            return PartialView( "_Create", model );
        }

        // POST: TeachingLoad/Create
        [HttpPost]
        public ActionResult Create( TeachingLoadItemCreateVm model )
        {
            
            bool success = Service.SaveChanges( model, CurrentPerson, CurrentUser );
             
            return Json( new { success } );
        }

        // GET: TeachingLoad/Edit/5
        public ActionResult Edit( int id )
        {
            var model = Service.GetItem<TeachingLoadItemEditVm>( id, CurrentPerson );

            return PartialView( "_Edit", model );
        }

        // POST: TeachingLoad/Edit/5
        [HttpPost]
        public ActionResult Edit( int id, TeachingLoadItemEditVm model )

        { 

        bool success = Service.SaveChanges(model, CurrentPerson, CurrentUser);


            return Json(new { success });


        }

        // GET: TeachingLoad/Delete/5
        public ActionResult Delete(int id)
        {
            return PartialView("_Delete", id);
        }

        // POST: ManagementLoads/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            bool success = Service.Delete<TeachingLoad>(id, CurrentPerson, CurrentUser);

            return Json(new { success });
        }
    }
}
