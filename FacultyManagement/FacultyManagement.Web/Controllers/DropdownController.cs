﻿using FacultyManagement.Domain.Model.Dropdown;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FacultyManagement.Web.Controllers
{
    public class DropdownController : Controller
    {
        // GET: Dropdown
      
        [HttpPost]
        public ActionResult OfferingList(int filterIndex, int year)
        {
            var dropDownViewModel = new DropdownFilterViewModel() { filterIndex = filterIndex, year = year };
            return PartialView(dropDownViewModel);
        }

        [HttpPost]
        public ActionResult ModuleList(int filterIndex)
        {
            var dropDownViewModel = new DropdownFilterViewModel() { filterIndex = filterIndex };
            return PartialView(dropDownViewModel);
        }

       
    }
}