﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FacultyManagement.Application.AcademicAndProfessionalExperience;
using FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels;
using FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.EditorialBoardMember;
using FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.EngagementWithBusinessAndSociety;
using FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.ExecutiveEducation;
using FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.PartTimeConsulting;
using FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.TeachingExperience;
using FacultyManagement.Application.Common;
using FacultyManagement.Domain.Model.AcademicAndProfessionalExperience;
using FacultyManagement.Infrastructure;
using FacultyManagement.Web.Infrastructure.Filters;
using ControllerBase = FacultyManagement.Web.Controllers.ControllerBase;

namespace FacultyManagement.Web.Controllers
{
    public class AcademicAndProfessionalExperienceController : ControllerBase
    {

        public AcademicAndProfessionalExperienceController( IAcademicAndProfessionalExperienceService service ) : base( service )
        {
        }
        // GET: AcademicAndProfessionalExperience
        public ActionResult Index()
        {
            AcademicAndProfessionalExperienceVm model = null;
            try
            {
                model = Service.GetViewModel( CurrentPerson ) as AcademicAndProfessionalExperienceVm;
            }
            catch ( ObjectNotFoundException )
            {
                ModelState.AddModelError( "", Messages.NoDataError );
            }

            return View( model );
        }

        #region Teaching Experience

        public ActionResult CreateTeachingExperience()
        {
            var model = new TeachingExperienceCreateVm( this.CVid );

            return PartialView( "_CreateTeachingExperience", model );
        }

        // POST: AcademicAndProfessionalExperience/Create
        [HttpPost]
        public ActionResult CreateTeachingExperience( TeachingExperienceCreateVm model )
        {
            if ( !ModelState.IsValid ) return Json( new { success = false, errorMessage = this.ErrorMessages }, JsonRequestBehavior.AllowGet );

            bool success = Service.SaveChanges( model, CurrentPerson, CurrentUser );

            return Json( new { success } );
        }

        // GET: AcademicAndProfessionalExperience/Edit/5
        public ActionResult EditTeachingExperience( int id )
        {
            var model = Service.GetItem<TeachingExperienceEditVm>( id, CurrentPerson );

            return PartialView( "_EditTeachingExperience", model );
        }

        // POST: AcademicAndProfessionalExperience/Edit/5
        [HttpPost]
        public ActionResult EditTeachingExperience( TeachingExperienceEditVm model )
        {
            if ( !ModelState.IsValid ) return Json( new { success = false, errorMessage = this.ErrorMessages }, JsonRequestBehavior.AllowGet );

            bool success = Service.SaveChanges( model, CurrentPerson, CurrentUser );

            return Json( new { success } );
        }
        #endregion

        #region Executive Education

        public ActionResult CreateExecutiveEducation()
        {
            var model = new ExecutiveEducationCreateVm( this.CVid );

            return PartialView( "_CreateExecutiveEducation", model );
        }

        // POST: AcademicAndProfessionalExperience/Create
        [HttpPost]
        public ActionResult CreateExecutiveEducation( ExecutiveEducationCreateVm model )
        {
            if ( !ModelState.IsValid ) return Json( new { success = false, errorMessage = this.ErrorMessages }, JsonRequestBehavior.AllowGet );

            bool success = Service.SaveChanges( model, CurrentPerson, CurrentUser );
            return Json( new { success } );
        }

        // GET: AcademicAndProfessionalExperience/Edit/5
        public ActionResult EditExecutiveEducation( int id )
        {
            var model = Service.GetItem<ExecutiveEducationEditVm>( id, CurrentPerson );

            return PartialView( "_EditExecutiveEducation", model );
        }

        // POST: AcademicAndProfessionalExperience/Edit/5
        [HttpPost]
        public ActionResult EditExecutiveEducation( ExecutiveEducationEditVm model )
        {
            if ( !ModelState.IsValid ) return Json( new { success = false, errorMessage = this.ErrorMessages }, JsonRequestBehavior.AllowGet );

            bool success = Service.SaveChanges( model, CurrentPerson, CurrentUser );

            return Json( new { success } );
        }

        #endregion

        #region Part Time Consulting

        public ActionResult CreatePartTimeConsulting()
        {
            var model = new PartTimeConsultingCreateVm( this.CVid );

            return PartialView( "_CreatePartTimeConsulting", model );
        }

        // POST: AcademicAndProfessionalExperience/Create
        [HttpPost]
        public ActionResult CreatePartTimeConsulting( PartTimeConsultingCreateVm model )
        {
            if ( !ModelState.IsValid ) return Json( new { success = false, errorMessage = this.ErrorMessages }, JsonRequestBehavior.AllowGet );

            bool success = Service.SaveChanges( model, CurrentPerson, CurrentUser );

            return Json( new { success } );
        }

        // GET: AcademicAndProfessionalExperience/Edit/5
        public ActionResult EditPartTimeConsulting( int id )
        {
            var model = Service.GetItem<PartTimeConsultingEditVm>( id, CurrentPerson );

            return PartialView( "_EditPartTimeConsulting", model );
        }

        // POST: AcademicAndProfessionalExperience/Edit/5
        [HttpPost]
        public ActionResult EditPartTimeConsulting( PartTimeConsultingEditVm model )
        {
            if ( !ModelState.IsValid ) return Json( new { success = false, errorMessage = this.ErrorMessages }, JsonRequestBehavior.AllowGet );

            bool success = Service.SaveChanges( model, CurrentPerson, CurrentUser );

            return Json( new { success } );
        }

        #endregion

        #region Engagement With Business

        public ActionResult CreateEngagementWithBusiness()
        {
            var model = new EngagementWithBusinessCreateVm( this.CVid );

            return PartialView( "_CreateEngagementWithBusiness", model );
        }

        // POST: AcademicAndProfessionalExperience/Create
        [HttpPost]
        public ActionResult CreateEngagementWithBusiness( EngagementWithBusinessCreateVm model )
        {
            if ( !ModelState.IsValid ) return Json( new { success = false, errorMessage = this.ErrorMessages }, JsonRequestBehavior.AllowGet );

            bool success = Service.SaveChanges( model, CurrentPerson, CurrentUser );

            return Json( new { success } );
        }

        // GET: AcademicAndProfessionalExperience/Edit/5
        public ActionResult EditEngagementWithBusiness( int id )
        {
            var model = Service.GetItem<EngagementWithBusinessEditVm>( id, CurrentPerson );

            return PartialView( "_EditEngagementWithBusiness", model );
        }

        // POST: AcademicAndProfessionalExperience/Edit/5
        [HttpPost]
        public ActionResult EditEngagementWithBusiness( EngagementWithBusinessEditVm model )
        {
            if ( !ModelState.IsValid ) return Json( new { success = false, errorMessage = this.ErrorMessages }, JsonRequestBehavior.AllowGet );

            bool success = Service.SaveChanges( model, CurrentPerson, CurrentUser );

            return Json( new { success } );
        }

        #endregion

        # region Editorial Board Member

        public ActionResult CreateEditorialBoardMember()
        {
            var model = new EditorialBoardMemberCreateVm( this.CVid );

            return PartialView( "_CreateEditorialBoardMember", model );
        }

        // POST: AcademicAndProfessionalExperience/Create
        [HttpPost]
        public ActionResult CreateEditorialBoardMember( EditorialBoardMemberCreateVm model )
        {
            if ( !ModelState.IsValid ) return Json( new { success = false, errorMessage = this.ErrorMessages }, JsonRequestBehavior.AllowGet );

            bool success = Service.SaveChanges( model, CurrentPerson, CurrentUser );

            return Json( new { success } );
        }

        // GET: AcademicAndProfessionalExperience/Edit/5
        public ActionResult EditEditorialBoardMember( int id )
        {
            var model = Service.GetItem<EditorialBoardMemberEditVm>( id, CurrentPerson );

            return PartialView( "_EditEditorialBoardMember", model );
        }

        // POST: AcademicAndProfessionalExperience/Edit/5
        [HttpPost]
        public ActionResult EditEditorialBoardMember( EditorialBoardMemberEditVm model )
        {
            if ( !ModelState.IsValid ) return Json( new { success = false, errorMessage = this.ErrorMessages }, JsonRequestBehavior.AllowGet );

            bool success = Service.SaveChanges( model, CurrentPerson, CurrentUser );

            return Json( new { success } );
        }

        #endregion

        #region Delete Methods

        public ActionResult DeleteExperience( int id )
        {
            return PartialView( "_Delete", id );
        }

        [HttpPost]
        [ActionName( "DeleteExperience" )]
        public ActionResult DeleteExperienceConfirmed( int id )
        {
            bool success = Service.Delete<Experience>( id, CurrentPerson, CurrentUser );

            return Json( new { success } );
        }
        #endregion
    }
}
