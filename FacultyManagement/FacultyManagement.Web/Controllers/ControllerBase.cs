﻿using System.Web.Mvc;
using FacultyManagement.Core.Security.Identity;
using FacultyManagement.Infrastructure.Utilities.ExtensionMethods;
using FacultyManagement.Web.Infrastructure;
using NLog;
using StructureMap.Attributes;
using System.Linq;
using FacultyManagement.Application.Common;

namespace FacultyManagement.Web.Controllers
{
    public class ControllerBase : Controller
    {
        protected static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        protected IApplicationService Service { get; set; }

        protected ControllerBase( IApplicationService service )
        {
            Service = service;
        }

        protected ControllerBase()
        {
        }

        /// <summary>
        /// Gets or sets the current logged in user.
        /// </summary>
        /// <value>
        /// The current user.
        /// </value>
        [SetterProperty]
        public IUser<CurrentUser> CurrentUser { get; set; }

        /// <summary>
        /// Gets the loaded user which is stored in session.
        /// </summary>
        /// <value>
        /// The loaded user.
        /// </value>
        [SetterProperty]
        public IUser<LoadedUser> LoadedUser { get; set; }

        /// <summary>
        /// Gets the username of either the current user or the loaded user.
        /// </summary>
        /// <value>
        /// The username.
        /// </value>
        public string Username => LoadedUser?.Username ?? CurrentUser.Username;

        /// <summary>
        /// Gets the cvid of either the current user or the loaded user.
        /// </summary>
        /// <value>
        /// The cvid.
        /// </value>
        public int CVid => LoadedUser?.CVid ?? CurrentUser.CVid;

        /// <summary>
        /// Gets the current person which is either the loaded user or the current user.
        /// </summary>
        /// <value>
        /// The current person.
        /// </value>
        public IUser<CurrentPerson> CurrentPerson
        {
            get
            {
                // Returns the active user
                if ( _currentPerson != null && _currentPerson.CVid == this.CVid )
                {
                    return _currentPerson;
                }

                // Current Person is not yet set
                if ( _currentPerson == null )
                {
                    _currentPerson = Session.GetDataFromSession<CurrentPerson>( "_CurrentPerson" );
                    // Updated Current Person  to the active user
                    if ( _currentPerson?.CVid != this.CVid )
                    {
                        _currentPerson = new CurrentPerson( this.CVid, this.Username );
                    }
                    return _currentPerson;
                }

                return null;
            }
            set
            {
                Session.SetDataInSession<CurrentPerson>( "_CurrentPerson", null );

                _currentPerson = value;
                if ( _currentPerson != null )
                {
                    Session.SetDataInSession<CurrentPerson>( "_CurrentPerson", value );
                }                
            }
        }
        private IUser<CurrentPerson> _currentPerson;

        /// <summary>
        /// Gets the model error messages.
        /// </summary>
        /// <value>
        /// The error messages.
        /// </value>
        public string ErrorMessages => string.Join( "\n", ModelState.Values.SelectMany( v => v.Errors.Select( b => b.ErrorMessage ) ) );


    }
}
