﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FacultyManagement.Application.PerformanceManagement.Common;
using FacultyManagement.Application.PerformanceManagement.SelfEvaluation;
using FacultyManagement.Application.PerformanceManagement.SelfEvaluation.ViewModels;
using FacultyManagement.Domain.Model.PerformanceManagement;
using ControllerBase = FacultyManagement.Web.Controllers.ControllerBase;

namespace FacultyManagement.Web.Controllers
{
    public class SelfEvaluationController : ControllerBase
    {
        public SelfEvaluationController( ISelfEvaluationService service ) : base( service )
        {
        }

        // GET: SelfEvaluation
        public ActionResult Index()
        {
            var model = Service.GetViewModel( CurrentPerson ) as SelfEvaluationVm;

            return PartialView( model );
        }


        // GET: SelfEvaluation/Create
        public ActionResult Create( string category )
        {
            var model = ( (ISelfEvaluationService) Service ).GetCreateVm( category, CurrentPerson );

            return PartialView( "_Create", model );
        }

        // POST: SelfEvaluation/Create
        [HttpPost]
        public ActionResult Create( EvaluationCreateBaseVm model )
        {
            bool success = Service.SaveChanges( model, CurrentPerson, CurrentUser );

            // This will send back partial page to be re-rendered to keep user where they were
            if ( success )
            {
                return RedirectToAction( "Index" );
            }

            return new HttpStatusCodeResult( HttpStatusCode.BadRequest );  // 400
        }

        // GET: SelfEvaluation/Edit/5
        public ActionResult Edit( int id )
        {
            var model = Service.GetItem<EvaluationEditBaseVm>( id, CurrentPerson );

            return PartialView( "_Edit", model );
        }

        // POST: SelfEvaluation/Edit/5
        [HttpPost]
        public ActionResult Edit( EvaluationEditBaseVm model )
        {
            bool success = Service.SaveChanges(model, CurrentPerson, CurrentUser);

            // This will send back partial page to be re-rendered to keep user where they were
            if ( success )
            {
                return RedirectToAction( "Index" );
            }

            return new HttpStatusCodeResult( HttpStatusCode.BadRequest );  // 400
        }

        #region Delete

        // GET: SelfEvaluation/Delete/5
        public ActionResult Delete( int id )
        {
            return PartialView( "_Delete", id );
        }

        // POST: ManagementLoads/Delete/5
        [HttpPost]
        [ActionName( "Delete" )]
        public ActionResult DeleteConfirmed( int id )
        {
            bool success = Service.Delete<SelfEvaluation>( id, CurrentPerson, CurrentUser );

            // This will send back partial page to be re-rendered to keep user where they were
            if ( success )
            {
                return RedirectToAction( "Index" );
            }

            return new HttpStatusCodeResult( HttpStatusCode.BadRequest );  // 400
        }
        #endregion
    }
}
