﻿using System.Web.Mvc;
using FacultyManagement.Application.SupervisionLoad;
using FacultyManagement.Application.SupervisionLoad.ViewModels;
using FacultyManagement.Domain.Model.Admin;

namespace FacultyManagement.Web.Controllers
{
    public class SupervisionLoadsController : ControllerBase
    {
        public SupervisionLoadsController( ISupervisionLoadService service ) : base( service )
        {
        }

        // GET: SupervisionLoads
        public ActionResult Index( int? year = null )
        {
            // Filter by Year
            var model = ( (ISupervisionLoadService) Service ).FilterViewModel( CurrentPerson, year );

            return View( model );
        }

        public ActionResult Create()
        {
            var model = new SupervisionLoadCreateVm( CurrentPerson.CVid );

            return PartialView( "_Create", model );
        }

        [HttpPost]
        public ActionResult Create( SupervisionLoadCreateVm model )
        {
           

            bool success = Service.SaveChanges( model, CurrentPerson, CurrentUser );

            return Json( new { success } );
        }

        public ActionResult Edit( int id )
        {
            var model = Service.GetItem<SupervisionLoadEditVm>( id, CurrentPerson );

            return PartialView( "_Edit", model );
        }

        // POST: ManagementLoads/Edit/5
        [HttpPost]
        public ActionResult Edit( SupervisionLoadEditVm model )
        {
            if ( !ModelState.IsValid ) return Json( new { success = false, errorMessage = this.ErrorMessages }, JsonRequestBehavior.AllowGet );

            bool success = Service.SaveChanges( model, CurrentPerson, CurrentUser );

            return Json( new { success } );
        }

        #region Delete

        public ActionResult Delete( int id )
        {
            return PartialView( "_Delete", id );
        }

        // POST: ManagementLoads/Delete/5
        [HttpPost]
        [ActionName( "Delete" )]
        public ActionResult DeleteConfirmed( int id )
        {
            bool success = Service.Delete<SupervisionLoad>( id, CurrentPerson, CurrentUser );

            return Json( new { success } );
        }

        #endregion

    }
}