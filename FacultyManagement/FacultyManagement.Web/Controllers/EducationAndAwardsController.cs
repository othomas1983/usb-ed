﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FacultyManagement.Application.Common;
using FacultyManagement.Application.EducationAndAwards;
using FacultyManagement.Application.EducationAndAwards.ViewModels;
using FacultyManagement.Application.EducationAndAwards.ViewModels.Awards;
using FacultyManagement.Application.EducationAndAwards.ViewModels.Education;
using FacultyManagement.Application.EducationAndAwards.ViewModels.FacultyLanguages;
using FacultyManagement.Application.EducationAndAwards.ViewModels.Grants;
using FacultyManagement.Core.Security.Identity;
using FacultyManagement.Domain.Model;
using FacultyManagement.Domain.Model.Common;
using FacultyManagement.Domain.Model.EducationAndAwards;
using FacultyManagement.Infrastructure;
using FacultyManagement.Web.Infrastructure.Alerts;
using ControllerBase = FacultyManagement.Web.Controllers.ControllerBase;

namespace FacultyManagement.Web.Controllers
{
    public class EducationAndAwardsController : ControllerBase
    {

        public EducationAndAwardsController( IEducationAndAwardsService service ) : base( service )
        {
        }

        // GET: EducationAndAwards
        public ActionResult Index()
        {
            EducationAndAwardsVm model = null;
            try
            {
                model = Service.GetViewModel( CurrentPerson ) as EducationAndAwardsVm;
            }
            catch ( ObjectNotFoundException )
            {
                ModelState.AddModelError( "", Messages.NoDataError );
            }

            return View( model );
        }

        #region Education

        // GET: EducationAndAwards/Create
        public ActionResult CreateEducation()
        {
            var model = new EducationCreateVm( CurrentPerson.CVid );

            return PartialView( "_CreateEducation", model );
        }

        // POST: EducationAndAwards/Create
        [HttpPost]
        public ActionResult CreateEducation( EducationCreateVm model )
        {
            bool success = Service.SaveChanges( model, CurrentPerson, CurrentUser );

            return Json( new { success } );
        }

        // GET: EducationAndAwards/Edit/5
        public ActionResult EditEducation( int id )
        {
            var model = Service.GetItem<EducationEditVm>( id, CurrentPerson );

            return PartialView( "_EditEducation", model );
        }

        // POST: EducationAndAwards/Edit/5
        [HttpPost]
        public ActionResult EditEducation( EducationEditVm model )
        {
            bool success = Service.SaveChanges( model, CurrentPerson, CurrentUser );

            return Json( new { success } );
        }

        // GET: EducationAndAwards/Delete/5
        public ActionResult DeleteEducation( int id )
        {
            return PartialView( "_DeleteEducation", id );
        }

        // POST: EducationAndAwards/Delete/5
        [HttpPost]
        [ActionName( "DeleteEducation" )]
        public ActionResult DeleteEducationConfirmed( int id )
        {
            try
            {
                bool success = Service.Delete<Education>( id, CurrentPerson, CurrentUser );

                return Json( new { success } );
            }
            catch
            {
                return Json( new { success = false } );
            }
        }
        #endregion

        #region Awards
        public ActionResult CreateAward()
        {
            var model = new AwardCreateVm( CurrentPerson.CVid );

            return PartialView( "_CreateAward", model );
        }

        // POST: EducationAndAwards/Create
        [HttpPost]
        public ActionResult CreateAward( AwardCreateVm model )
        {
            bool success = Service.SaveChanges( model, CurrentPerson, CurrentUser );

            return Json( new { success } );
        }

        public ActionResult EditAward( int id )
        {
            var model = Service.GetItem<AwardEditVm>( id, CurrentPerson );

            return PartialView( "_EditAward", model );
        }

        // POST: EducationAndAwards/Edit/5
        [HttpPost]
        public ActionResult EditAward( AwardEditVm model )
        {
            bool success = Service.SaveChanges( model, CurrentPerson, CurrentUser );

            return Json( new { success } );
        }

        public ActionResult DeleteAward( int id )
        {
            return PartialView( "_DeleteAward", id );
        }

        // POST: EducationAndAwards/Delete/5
        [HttpPost]
        [ActionName( "DeleteAward" )]
        public ActionResult DeleteAwardConfirmed( int id )
        {
            try
            {
                bool success = Service.Delete<Award>( id, CurrentPerson, CurrentUser );

                return Json( new { success } );
            }
            catch
            {
                return Json( new { success = false } );
            }
        }
        #endregion

        #region Grants
        public ActionResult CreateGrant()
        {
            var model = new GrantCreateVm( CurrentPerson.CVid );

            return PartialView( "_CreateGrant", model );
        }

        // POST: EducationAndAwards/Create
        [HttpPost]
        public ActionResult CreateGrant( GrantCreateVm model )
        {
            bool success = Service.SaveChanges( model, CurrentPerson, CurrentUser );

            return Json( new { success } );
        }

        public ActionResult EditGrant( int id )
        {
            var model = Service.GetItem<GrantEditVm>( id, CurrentPerson );

            return PartialView( "_EditGrant", model );
        }

        // POST: EducationAndAwards/Edit/5
        [HttpPost]
        public ActionResult EditGrant( GrantEditVm model )
        {
            bool success = Service.SaveChanges( model, CurrentPerson, CurrentUser );

            return Json( new { success } );
        }

        public ActionResult DeleteGrant( int id )
        {
            return PartialView( "_DeleteGrant", id );
        }

        // POST: EducationAndAwards/Delete/5
        [HttpPost]
        [ActionName( "DeleteGrant" )]
        public ActionResult DeleteGrantConfirmed( int id )
        {
            try
            {
                bool success = Service.Delete<Grant>( id, CurrentPerson, CurrentUser );

                return Json( new { success } );
            }
            catch
            {
                return Json( new { success = false } );
            }
        }
        #endregion

        #region Languages

        public ActionResult CreateLanguage()
        {
            var model = ((IEducationAndAwardsService) Service).GetLanguageCreateViewModel( CurrentPerson.CVid );

            return PartialView( "_CreateLanguage", model as FacultyLanguageCreateVm );
        }

        // POST: EducationAndAwards/Create
        [HttpPost]
        public ActionResult CreateLanguage( FacultyLanguageCreateVm model )
        {
            bool success = Service.SaveChanges( model, CurrentPerson, CurrentUser );

            return Json( new { success } );
        }

        public ActionResult DeleteLanguage( int id )
        {
            return PartialView( "_DeleteLanguage", id );
        }

        // POST: EducationAndAwards/Delete/5
        [HttpPost]
        [ActionName( "DeleteLanguage" )]
        public ActionResult DeleteLanguageConfirm( int id )
        {
            try
            {              
                bool success = Service.Delete<FacultyLanguage>( id, CurrentPerson, CurrentUser );

                return Json( new { success } );
            }
            catch
            {
                return Json( new { success = false } );
            }
        }
        #endregion
    }
}
