﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FacultyManagement.Web.Controllers
{
    public class HomeController : Controller
    {
        [Authorize]
        public ActionResult PerformanceManagementMenu()
        {
            return View();
        }    
    }
}