﻿using System.Web.Mvc;
using FacultyManagement.Application.ManagementLoads;
using FacultyManagement.Application.ManagementLoads.ViewModels;
using FacultyManagement.Application.ManagementLoads.ViewModels.Common;
using FacultyManagement.Application.ManagementLoads.ViewModels.ProgrammeHead;
using FacultyManagement.Application.ManagementLoads.ViewModels.TaskTeamMember;
using FacultyManagement.Domain.Model.Enums;
using FacultyManagement.Domain.Model.Loads;
using FacultyManagement.Infrastructure.Utilities.ExtensionMethods;

namespace FacultyManagement.Web.Controllers
{
    public class ManagementLoadsController : ControllerBase
    {
        public ManagementLoadsController(IManagementLoadService service) : base(service)
        {
        }

        // GET: ManagementLoads
        public ActionResult Index()
        {
            var model = Service.GetViewModel(CurrentPerson) as ManagementLoadVm;

            return View(model);
        }

        #region Programme Heads

        // GET: ManagementLoads/Create
        public ActionResult CreateProgrammeHead()
        {
            var model = new ProgrammeHeadCreateVm(this.CVid);

            return PartialView("_CreateProgrammeHead", model);
        }

        // POST: ManagementLoads/Create
        [HttpPost]
        public ActionResult CreateProgrammeHead(ProgrammeHeadCreateVm model)
        {
            if (!ModelState.IsValid) return Json(new { success = false, errorMessage = this.ErrorMessages }, JsonRequestBehavior.AllowGet);

            bool success = Service.SaveChanges(model, CurrentPerson, CurrentUser);

            return Json(new { success });
        }

        // GET: ManagementLoads/Edit/5
        public ActionResult EditProgrammeHead(int id)
        {
            var model = Service.GetItem<ProgrammeHeadEditVm>(id, CurrentPerson);

            return PartialView("_EditProgrammeHead", model);
        }

        // POST: ManagementLoads/Edit/5
        [HttpPost]
        public ActionResult EditProgrammeHead(ProgrammeHeadEditVm model)
        {
            if (!ModelState.IsValid) return Json(new { success = false, errorMessage = this.ErrorMessages }, JsonRequestBehavior.AllowGet);

            bool success = Service.SaveChanges(model, CurrentPerson, CurrentUser);

            return Json(new { success });
        }
        #endregion

        #region Task Team Activity

        public ActionResult CreateTaskTeamActivity()
        {
            var model = new TaskTeamMemberCreateVm(this.CVid);

            return PartialView("_CreateTaskTeamMember", model);
        }

        // POST: ManagementLoads/Create
        [HttpPost]
        public ActionResult CreateTaskTeamActivity(TaskTeamMemberCreateVm model)
        {
            if (!ModelState.IsValid) return Json(new { success = false, errorMessage = this.ErrorMessages }, JsonRequestBehavior.AllowGet);

            bool success = Service.SaveChanges(model, CurrentPerson, CurrentUser);

            return Json(new { success });
        }

        // GET: ManagementLoads/Edit/5
        public ActionResult EditTaskTeamActivity(int id)
        {
            var model = Service.GetItem<TaskTeamMemberEditVm>(id, CurrentPerson);

            return PartialView("_EditTaskTeamMember", model);
        }

        // POST: ManagementLoads/Edit/5
        [HttpPost]
        public ActionResult EditTaskTeamActivity(TaskTeamMemberEditVm model)
        {
            if (!ModelState.IsValid) return Json(new { success = false, errorMessage = this.ErrorMessages }, JsonRequestBehavior.AllowGet);

            bool success = Service.SaveChanges(model, CurrentPerson, CurrentUser);

            return Json(new { success });
        }

        #endregion

        #region Centre Heads

        public ActionResult CreateCentreHead()
        {
            var model = new ManagementLoadGeneralEditCreateVm(this.CVid, ManagementLoadType.CentreHead.ConvertToInt());

            model.Heading = "Centre Head";

            return PartialView("_CreateManagementLoadGeneral", model);
        }

        #endregion

        #region Organising Conference

        public ActionResult CreateOrganisingConference()
        {
            var model = new ManagementLoadGeneralEditCreateVm(this.CVid, ManagementLoadType.OrganisingConference.ConvertToInt());

            model.Heading = "Organising a Conference / Seminar";

            return PartialView("_CreateManagementLoadGeneral", model);
        }

        #endregion

        #region Chair Of Administrative

        public ActionResult CreateChairOfAdministrative()
        {
            var model = new ManagementLoadGeneralEditCreateVm(this.CVid, ManagementLoadType.ChairOfAdministrative.ConvertToInt());

            model.Heading = "Chair of Administrative / Management Committee";

            return PartialView("_CreateManagementLoadGeneral", model);
        }

        #endregion

        #region Other

        public ActionResult CreateOther()
        {
            var model = new ManagementLoadGeneralEditCreateVm(this.CVid, ManagementLoadType.Other.ConvertToInt());

            model.Heading = "Other";

            return PartialView("_CreateManagementLoadGeneral", model);
        }

        #endregion

        #region Management Load General

        // POST: ManagementLoads/Create
        [HttpPost]
        public ActionResult CreateManagementLoadGeneral(ManagementLoadGeneralEditCreateVm model)
        {
            if (!ModelState.IsValid) return Json(new { success = false, errorMessage = this.ErrorMessages }, JsonRequestBehavior.AllowGet);

            bool success = Service.SaveChanges(model, CurrentPerson, CurrentUser);

            return Json(new { success });
        }

        // GET: ManagementLoads/Edit/5
        public ActionResult EditManagementLoadGeneral(int id)
        {
            var model = Service.GetItem<ManagementLoadGeneralEditCreateVm>(id, CurrentPerson);

            return PartialView("_EditManagementLoadGeneral", model);
        }

        // POST: ManagementLoads/Edit/5
        [HttpPost]
        public ActionResult EditManagementLoadGeneral(ManagementLoadGeneralEditCreateVm model)
        {
            if (!ModelState.IsValid) return Json(new { success = false, errorMessage = this.ErrorMessages }, JsonRequestBehavior.AllowGet);

            bool success = Service.SaveChanges(model, CurrentPerson, CurrentUser);

            return Json(new { success });
        }

        #endregion

        #region Delete

        // GET: ManagementLoads/Delete/5
        public ActionResult Delete(int id)
        {
            return PartialView("_Delete", id);
        }

        // POST: ManagementLoads/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            bool success = Service.Delete<ManagementLoad>(id, CurrentPerson, CurrentUser);

            return Json(new { success });
        }
        #endregion
    }
}
