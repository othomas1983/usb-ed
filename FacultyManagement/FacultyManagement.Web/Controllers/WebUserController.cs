﻿using FacultyManagement.Application.WebUsers;
using FacultyManagement.Application.WebUsers.ViewModels;
using FacultyManagement.Infrastructure;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;

namespace FacultyManagement.Web.Controllers
{
    public class WebUserController : ControllerBase
    {
        private readonly IWebUserService _webUserService;

        public WebUserController(IWebUserService webUserService)
        {
            _webUserService = webUserService;
            
        }

        public SelectList SelectedEmployeeIdAdd { get; set; }

        public SelectList SelectedEmployeeIdRemove { get; set; }


        public class SelectedUser
        {
            public int CVID { get; set; }

            public string Surname { get; set; }

            public string Name { get; set; }


            public string Fullname
            {
                get
                {
                    return $"{Surname}, {Name}";
                }
            }
        }



        public ActionResult Index()
        {
            WebUserVm model = null;

            try
            {
                model = _webUserService.GetViewModel(CurrentUser);

                var objSAdd = GetSelectedAdd();
                SelectList objSAddToBind = new SelectList(objSAdd, "CVID", "Fullname", 0);
          

                model.SelectedEmployeeIdAdd = objSAddToBind;
               // ViewBag.SelectedEmployeeIdAdd = objSAddToBind;
                var objSRemove = GetSelectedRemove();
                SelectList objSRemoveToBind = new SelectList(objSRemove, "CVID", "Fullname", 0);
               // ViewBag.SelectedEmployeeIdAdd = objSRemoveToBind;
                model.SelectedEmployeeIdRemove = objSRemoveToBind;
            }
            catch (ObjectNotFoundException)
            {
                ModelState.AddModelError("", Messages.NoDataError);
            }

            return View(model);           
        }

        
        //public ActionResult SaveUsers(List<int> cvIdsAdded,
        //    List<int> cvIdsRemoved)
        //{
        //    var savedDisplayOn = UpdateAddedUsers(cvIdsAdded);
        //    var savedDisplayOff = UpdateRemovedUsers(cvIdsRemoved);
        //    return Json("success", JsonRequestBehavior.AllowGet);
        //}





        public JsonResult UpdateAddedUsers(List<int> cvIds)
        {
            _webUserService.UpdateUserDisplayOnWebOn(cvIds);           
            return Json("success", JsonRequestBehavior.AllowGet);
        }


        public JsonResult UpdateRemovedUsers(List<int> cvIds)
        {
            _webUserService.UpdateUserDisplayOnWebOff(cvIds);
            return Json("success", JsonRequestBehavior.AllowGet);
        }

        #region Populate List boxes

        private IEnumerable<SelectedUser> GetSelectedAdd()
        {
            return GetSelectedUserForDisplay(false);
        }

        private IEnumerable<SelectedUser> GetSelectedRemove()
        {
            return GetSelectedUserForDisplay(true);
        }

        private IEnumerable<SelectedUser> GetSelectedUserForDisplay(bool displayOnWeb)
        {
            var users = _webUserService.GetWebUsers(CVid, displayOnWeb);

            return users.Select(i => new SelectedUser
            {
                CVID = i.CVID,
                Surname = i.Surname,
                Name = i.Name                
            });
        }

        #endregion

    }
}