﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FacultyManagement.Infrastructure.Utilities;
using StructureMap;

namespace FacultyManagement.Web.DependencyResolution
{
    public class StartupRegistry : Registry
    {
        public StartupRegistry()
        {
            Scan( scan =>
            {
                scan.AssembliesFromApplicationBaseDirectory(
                    a => a.FullName.StartsWith( "FacultyManagement" ) );                
                scan.AddAllTypesOf<IRunAtStartup>();
            } );
        }
    }
}