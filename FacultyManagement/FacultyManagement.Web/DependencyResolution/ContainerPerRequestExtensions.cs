﻿using System.Web;
using FacultyManagement.Web.App_Start;
using StructureMap;

namespace FacultyManagement.Web.DependencyResolution
{
    public static class ContainerPerRequestExtensions
    {
        public static IContainer GetNestedContainer( this HttpContextBase context )
        {
            return StructuremapMvc.StructureMapDependencyScope.CurrentNestedContainer;
        }
    }
}