﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using FacultyManagement.Core.Security.Identity;
using FacultyManagement.Web.Infrastructure;
using Microsoft.Owin.Security;
using StructureMap;

namespace FacultyManagement.Web.DependencyResolution
{
    public class WebRegistry : Registry
    {
        public WebRegistry()
        {
            Scan( scan =>
            {
                scan.TheCallingAssembly();
                scan.WithDefaultConventions();
            } );

            // Identity
            For<IPrincipal>().Use( () => HttpContext.Current.User );
            For<IUser<CurrentUser>>().Use<CurrentUser>();   
            For<IUser<LoadedUser>>().Use(() => HttpContext.Current.Session["_LoadedUser"] as LoadedUser );
            // Http
            For<HttpSessionStateBase>()
                .Use( () => new HttpSessionStateWrapper( HttpContext.Current.Session ) );
            For<HttpContextBase>()
                .Use( () => new HttpContextWrapper( HttpContext.Current ) );
            For<HttpServerUtilityBase>()
                .Use( () => new HttpServerUtilityWrapper( HttpContext.Current.Server ) );
            // Owin
            For<IAuthenticationManager>()
                .Use( () => System.Web.HttpContext.Current.GetOwinContext().Authentication );
        }

    }
}