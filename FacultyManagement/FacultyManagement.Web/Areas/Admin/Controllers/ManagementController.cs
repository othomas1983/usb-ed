﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using FacultyManagement.Application.Admin;
using FacultyManagement.Application.Admin.ViewModels;
using FacultyManagement.Application.Admin.ViewModels.Common;
using FacultyManagement.Core.Security.Identity;
using FacultyManagement.Domain.ExtensionMethods;
using FacultyManagement.Infrastructure.Utilities;
using FacultyManagement.Infrastructure.Utilities.ExtensionMethods;
using FacultyManagement.Web.Infrastructure;
using ControllerBase = FacultyManagement.Web.Controllers.ControllerBase;

namespace FacultyManagement.Web.Areas.Admin.Controllers
{
    public class ManagementController : ControllerBase
    {

        public ManagementController(IAdminService service) : base(service)
        {
        }


        #region Departments

        // GET: Admin/Departments
        public ActionResult AddOrganisation()
        {
            var model = ((IAdminService)Service).GetSelectUserViewModel() as UserSelectBaseVm;

            return View(model);
        }

        [HttpPost]
        public ActionResult LoadUserOrganisations(string selectedUsernameAndCvid)
        {
            string username;
            string cVid;
            FacultyHelper.SplitUserDetails(selectedUsernameAndCvid, out cVid, out username);

            //Stop partial view page repeating
            if (Request.IsAjaxRequest())
            {
                var model = ((IAdminService)Service).GetOrganisations(cVid.AsInteger());

                return PartialView("_Organisations", model);
            }


            return Json(new { success = true });
        }

        [HttpPost]
        public ActionResult CreateUserOrganisations(OrganisationAndRolesVm model)
        {
            if (!ModelState.IsValid) return PartialView("_Organisations", model);

            bool success = Service.SaveChanges(model,
                                                                    new CurrentPerson(model.CVid.Value, model.Username),
                                                                    CurrentUser);

            return Json(new { success });
        }
        #endregion

        #region Classification & Sufficiency

        // GET: Admin/AddClassification
        public ActionResult AddClassification()
        {
            var model = ((IAdminService)Service).GetSelectUserViewModel() as UserSelectBaseVm;

            return View(model);
        }


        [HttpPost]
        public ActionResult LoadUserClassifications(string selectedUsernameAndCvid)
        {
            string username;
            string cVid;
            FacultyHelper.SplitUserDetails(selectedUsernameAndCvid, out cVid, out username);

            //Stop partial view page repeating
            if (Request.IsAjaxRequest())
            {
                var model = ((IAdminService)Service).GetClassificationsAndSufficiencies(cVid.AsInteger());

                return PartialView("_Classifications", model);
            }


            return Json(new { success = true });
        }

        [HttpPost]
        public ActionResult CreateUserClassifications(ClassificationsAndSufficienciesVm model)
        {
            if (!ModelState.IsValid) return PartialView("_Classifications", model);

            bool success = Service.SaveChanges(model,
                                                                    new CurrentPerson(model.CVid.Value, model.Username),
                                                                    CurrentUser);

            return Json(new { success });
        }

        #endregion

        #region Executive profile 

        public ActionResult EditExecutiveProfile()
        {
            var model = ((IAdminService)Service).GetExecutiveProfile(CurrentPerson);

            return PartialView("_EditExecutiveProfile", model);
        }

        [HttpPost]
        public ActionResult EditExecutiveProfile(ExecutiveProfileVm model)
        {
            bool success = Service.SaveChanges(model, CurrentPerson, CurrentUser);

            return Json(new { success });
        }

        public ActionResult LogOut()
        {
            Logger.Info($"Username:{this.CurrentUser.Username}");

            Request.GetOwinContext().Authentication.SignOut();
            return Redirect("/FacultyManagement");
        }


        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return Redirect("/");
        }
        #endregion
    }
}

