﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using FacultyManagement.Core.Security.Identity;

namespace FacultyManagement.Web.Infrastructure
{
    /// <summary>
    /// Represents a User that was loaded from admin
    /// </summary>
    /// <seealso cref="User" />
    /// <seealso cref="Core.Security.Identity.IUser{LoadedUser}" />
    public class LoadedUser : User, IUser<LoadedUser>
    {

        public LoadedUser( IPrincipal principal )
        {
            Principal = principal;
        }
    }
}