﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace FacultyManagement.Web.Infrastructure.Alerts
{
    /// <summary>
    /// Wraps Action result with alert functionality
    /// </summary>
    /// <seealso cref="System.Web.Mvc.ActionResult" />
    public class AlertDecoratorResult : ActionResult
    {
        public ActionResult InnerResult { get; set; }
        public string AlertClass { get; set; }
        public string Message { get; set; }

        public AlertDecoratorResult( ActionResult innerResult, string alertClass, string message )
        {
            InnerResult = innerResult;
            AlertClass = alertClass;
            Message = message;
        }

        public override void ExecuteResult( ControllerContext context )
        {
            List<Alert> alerts = context.Controller.TempData.GetAlerts();
            alerts.Add( new Alert( AlertClass, Message ) );
            InnerResult.ExecuteResult( context );
        }
    }
}