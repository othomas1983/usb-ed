﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using NLog;

namespace FacultyManagement.Web.Infrastructure.Filters
{
    /// <summary>
    /// Ajax Exception Filter
    /// </summary>
    /// <seealso cref="System.Web.Mvc.FilterAttribute" />
    /// <seealso cref="System.Web.Mvc.IExceptionFilter" />
    public class AjaxExceptionFilter : FilterAttribute, IExceptionFilter
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public void OnException( ExceptionContext filterContext )
        {
            //Send ajax response
            if ( filterContext.HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest" )
            {
                /* filterContext.Result = new JsonResult
                 {
                     JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                     Data = new
                     {
                         errorMessage = "An error has occured. Please try again later.",
                     }
                 };*/

                // Log Error
                Logger.Error( ProcessException( filterContext.Exception, "" ) );

                filterContext.ExceptionHandled = true;
                filterContext.HttpContext.Response.Clear();
                filterContext.HttpContext.Response.StatusCode = 400;
                return;
            }

            // All other Exceptions to go Global Exception Handler
            filterContext.HttpContext.Response.StatusCode = 500;
            filterContext.ExceptionHandled = false;
        }


        /// <summary>
        /// Processes the exception.
        /// </summary>
        /// <param name="ex">The ex.</param>
        /// <param name="exLevel">The ex level.</param>
        private string ProcessException( Exception ex, string exLevel )
        {
            StringBuilder sb = new StringBuilder();

            sb.Append( "Exception: " + exLevel + ex.GetType().Name + " in " + HttpUtility.HtmlEncode( ex.Source ) );
            sb.Append( "\r\nMessage: " + ex.Message );
            sb.Append( "\r\nStack Trace:" + ex.StackTrace );

            // check for inner exception
            if ( ex.InnerException != null )
            {
                ProcessException( ex.InnerException, "-" + exLevel );
            }

            return sb.ToString();
        }
    }
}