﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using FacultyManagement.Infrastructure.Utilities;
using FacultyManagement.Infrastructure.Utilities.ExtensionMethods;

namespace FacultyManagement.Web.Infrastructure.Filters
{
    /// <summary>
    /// Used to control the create research work flow. 
    /// Checks if research exists in session and uses this to determine which step (page) the user should be redirected to.
    /// Also stores the posted model automatically in session
    /// </summary>
    [AttributeUsage( AttributeTargets.Method )]
    public class ResearchWorkflowFilter : ActionFilterAttribute
    {
        private readonly WorkflowStep _workflowStep;

        public ResearchWorkflowFilter( WorkflowStep workflowStep )
        {
            _workflowStep = workflowStep;
        }

        public override void OnActionExecuting( ActionExecutingContext filterContext )
        {
            if ( filterContext.HttpContext.Request.RequestType == "GET" )
            {
                // Redirects to the correct page - skipping steps in not allowed
                int? currentStep = (int?) filterContext.HttpContext.Session["_ResearchWorkflowStep"];
                // Work flow has not been started - go to step 1
                if ( !currentStep.HasValue )
                {
                    filterContext.HttpContext.Session["_ResearchWorkflowStep"] = WorkflowStep.One.ConvertToInt();
                    filterContext.Result = GenerateRedirectUrl( "CreateStep1", "Research" );
                }
                // Prevent user from skipping a step
                else if ( currentStep == WorkflowStep.One.ConvertToInt() && _workflowStep == WorkflowStep.Two )
                {
                    filterContext.Result = GenerateRedirectUrl( "CreateStep1", "Research" );
                }
                else if ( currentStep == WorkflowStep.Two.ConvertToInt() && _workflowStep == WorkflowStep.Three )
                {
                    filterContext.Result = GenerateRedirectUrl( "CreateStep2", "Research" );
                } 
            }

            // Stores the posted model in session
            if ( filterContext.HttpContext.Request.RequestType == "POST" )
            {
                var model = filterContext.ActionParameters.FirstOrDefault( p => p.Key.StartsWith( "model" ) ).Value;

                if ( model != null )
                {
                    filterContext.HttpContext.Session[$"_{model.GetType().Name}"] = model; 
                }
            }

        }

        public override void OnActionExecuted( ActionExecutedContext filterContext )
        {
            // Advances to the next step on submitting the form
            if ( filterContext.HttpContext.Request.RequestType == "POST" )
            {
                int step = _workflowStep.ConvertToInt();
                filterContext.HttpContext.Session["_ResearchWorkflowStep"] = ++step;
            }
        }
        
        private RedirectToRouteResult GenerateRedirectUrl( string action, string controller )
        {
            return new RedirectToRouteResult( new RouteValueDictionary( new { action, controller } ) );
        }
    }
}