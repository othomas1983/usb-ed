﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using FacultyManagement.Application.Admin;
using FacultyManagement.Application.Authentication;
using FacultyManagement.Core.Cache;
using FacultyManagement.Data.Interfaces;
using FacultyManagement.Domain.Model;
using FacultyManagement.Domain.Model.Admin;
using FacultyManagement.Domain.Model.Common;
using FacultyManagement.Domain.Model.Enums;
using FacultyManagement.Domain.Model.Loads.Common;
using FacultyManagement.Domain.Model.PerformanceManagement;
using FacultyManagement.Domain.Model.Research;
using FacultyManagement.Infrastructure.Communication;
using FacultyManagement.Infrastructure.Utilities;
using FacultyManagement.Web.App_Start;
using FacultyManagement.Web.DependencyResolution;
using NLog;
using StructureMap;
using System.Globalization;

namespace FacultyManagement.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        // Container is disposed automatically by structureMap    
        public IContainer Container { get; } = StructuremapMvc.StructureMapDependencyScope.Container;

        #region Asp.Net Events

        protected void Application_Start()
        {

            var cultureInfo = new CultureInfo("en-ZA");
            cultureInfo.NumberFormat.NumberDecimalSeparator = ".";

            CultureInfo.DefaultThreadCurrentCulture = cultureInfo;

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters( GlobalFilters.Filters );
            RouteConfig.RegisterRoutes( RouteTable.Routes );
            BundleConfig.RegisterBundles( BundleTable.Bundles );

            AntiForgeryConfig.UniqueClaimTypeIdentifier = ClaimTypes.Name;
        
            //RunStartups();
            LoadCacheObjects();
        }

        /// <summary>
        /// Handles the Pre Send Request event of the Application control.
        /// </summary>
        protected void Application_PreSendRequestHeaders()
        {
            Response.Headers.Remove( "X-AspNet-Version" );
            Response.Headers.Remove( "Server" );
            ;
        }

        /// <summary>
        /// Handles the Error event of the Application control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        protected void Application_Error( object sender, EventArgs e )
        {
            try
            {
                // Save information before IIS redirects to Error.aspx on an unhandled 500 error (configured in Web.Config).
                HttpContext context = HttpContext.Current;
                if ( context != null )
                {
                    var ex = context.Server.GetLastError();

                    try
                    {
                        HttpException httpEx = ex as HttpException;
                        int? statusCode = httpEx?.GetHttpCode();
                        if ( statusCode == 404 )
                        {
                            context.ClearError();
                            context.Response.StatusCode = 404;
                            return;
                        }
                    }
                    catch
                    {
                        //
                    }

                    while ( ex is HttpUnhandledException && ex.InnerException != null )
                    {
                        ex = ex.InnerException;
                    }

                    // Log Error
                    Logger.Error( ProcessException( ex, "" ) );

                    if ( !( ex is HttpRequestValidationException ) )
                    {
                        SendNotification( ex );
                    }

                    context.Session["FMLastException"] = ex;
                }
            }
            catch { }
        }

        /// <summary>
        /// Handles the Start event of the Session control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        protected void Session_Start( object sender, EventArgs e )
        {
            Session["_LoadedUser"] = null;
        }

        #endregion

        /// <summary>
        /// Sends the notification.
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        private void SendNotification( Exception exception )
        {
            //SMTPEmail.Send( ConfigurationManager.AppSettings["AdminEmailAddress"] , string.Empty, "Application Error", exception.StackTrace );
        }

        /// <summary>
        /// Loads the cache objects.
        /// </summary>
        private void LoadCacheObjects()
        {
            DropDownsCache.Read<ABSRating>( DropDownType.ABSRating );
            DropDownsCache.Read<AcademicRank>( DropDownType.AcademicRanks );
            DropDownsCache.Read<Affiliation>( DropDownType.Affiliations );           
            DropDownsCache.Read<ApplicationUserRole>( DropDownType.ApplicationUserRoles );
            DropDownsCache.Read<Degree>( DropDownType.Degrees );
            DropDownsCache.Read<Department>( DropDownType.Departments );
            DropDownsCache.Read<DepartmentCategory>( DropDownType.DepartmentCategories );
            DropDownsCache.Read<DHETStatus>( DropDownType.DHETStatus );
            DropDownsCache.Read<FacultyClassification>( DropDownType.FacultyClassifications );
            DropDownsCache.Read<FacultySufficiency>( DropDownType.FacultySufficiencies );
            DropDownsCache.Read<HighestQualification>( DropDownType.HighestQualifications );
            DropDownsCache.Read<Language>( DropDownType.Languages );
            DropDownsCache.Read<Locus>( DropDownType.Locus );
            DropDownsCache.Read<MainActivity>( DropDownType.MainActivities );
            DropDownsCache.Read<Nationality>( DropDownType.Nationalities );
            DropDownsCache.Read<PerformanceCategory>( DropDownType.PerformanceCategory );
            DropDownsCache.Read<PersonRole>( DropDownType.PersonRole );
            DropDownsCache.Read<Programme>( DropDownType.Programmes );
            DropDownsCache.Read<ProgrammeOffering>( DropDownType.ProgrammeOfferings );
            DropDownsCache.Read<ResearchContributionType>( DropDownType.ResearchContributionType );
            DropDownsCache.Read<ResearchOutputCategory>( DropDownType.ResearchOutputCategory );
            DropDownsCache.Read<ResearchStatus>( DropDownType.ResearchStatus );
            DropDownsCache.Read<TaskTeamActivity>( DropDownType.TaskTeamActivity );
            DropDownsCache.Read<BusinessUnit>(DropDownType.BusinessUnit);

            //AuthCache.Read();

            /* var userService = Container.GetInstance<IUserService>();
             userService.GetActiveDirectoryUsers( true );*/

        }

        /// <summary>
        /// Runs the startups.
        /// </summary>
        private void RunStartups()
        {
            foreach ( var startup in Container.GetAllInstances<IRunAtStartup>().OrderBy( s => s.StartupOrder ) )
            {
                startup.Execute();
                Logger.Info( $"Startup Executed: {startup.GetType().FullName}" );
            }
        }

        /// <summary>
        /// Processes the exception.
        /// </summary>
        /// <param name="ex">The ex.</param>
        /// <param name="exLevel">The ex level.</param>
        private string ProcessException( Exception ex, string exLevel )
        {
            StringBuilder sb = new StringBuilder();

            sb.Append( "Exception: " + exLevel + ex.GetType().Name + " in " + HttpUtility.HtmlEncode( ex.Source ) );
            sb.Append( "\r\nMessage: " + ex.Message );
            sb.Append( "\r\nStack Trace:" + ex.StackTrace );

            // check for inner exception
            if ( ex.InnerException != null )
            {
                ProcessException( ex.InnerException, "-" + exLevel );
            }

            return sb.ToString();
        }
    }
}
