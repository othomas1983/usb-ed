﻿// Disables the EndDate if IsCurrent is checked
$(document).ready(function () {

    var isChecked;
        $('#IsCurrent').change(function () {
        isChecked = $(this).is(":checked");

        if (isChecked) {
            /* $( "#EndDate" ).attr( 'disabled', 'disabled' );*/
            $("#EndDate").prop('disabled', true);
            $("#EndDate").val("");
        } else {
            $("#EndDate").prop('disabled', false);
            $("#EndDate").val("");
        }
        $('#IsCurrent').val($(this).is(':checked'));
        });

      
       var endDate = $("#EndDate").val();
     
       if (endDate == null || endDate == "") {        
           $("#EndDate").prop('disabled', true);
           $("#EndDate").val("");
       }
       else
       {         
           $('#IsCurrent').prop('checked', false);
       }   
});
