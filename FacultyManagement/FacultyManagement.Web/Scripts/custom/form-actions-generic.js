﻿// Handles form submission
$("form").submit(function () {

    ResetValidationSummary();

    //showLoadingSpinner();

    if ($("form").valid()) {
        $.ajax({
            url: this.action,
            type: this.method,
            data: $(this).serialize(),
            beforeSend: function () {
                $("#div_loading").show();
            },
            complete: function () {
                $('#div_loading').hide();
            },
            success: function(result) {
                if (result.success) {
                    $('#div_loading').hide();
                    $("#MessageToClient")
                        .html("<div class='alert alert-success'> <i class='fa fa-check-circle-o fa-2x'> </i> Successfully Added.</div>");
                    // Delay this by 1 second
                    setTimeout(function() {
                            $("#modal-container").modal("hide");
                            location.reload();
                           // window.location = "~/PersonalInformation/Edit";
                    }, 1000);

                } else {
                    if (result.errorMessage) {
                        $("#MessageToClient").text(result.errorMessage);
                    } else {
                        $("#MessageToClient")
                            .html("<div class='alert alert-danger'> <i class='fa fa-exclamation-triangle fa-2x'> </i> " +
                                " An error has occurred while processing your request. <p> Your organization's administrators have been notified of this problem.</p></div>");
                    }
                }
            },
            error: function() {
                $("#MessageToClient")
                    .html("<div class='alert alert-danger'> <i class='fa fa-exclamation-triangle fa-2x'> </i> " +
                        " An error has occurred while processing your request. <p> Your organization's administrators have been notified of this problem. </p></div>");
            }
    });
        return false;
    }

});



function ResetValidationSummary() {
    // Clears validation summary
    $('.validation-summary-errors').each(function () {
        $(this).html("<ul><li style='display:none'></li></ul>");
    })
}

function showLoadingSpinner() {
    $("#div_loading").show();
    hideLoadingSpinner();
};

function hideLoadingSpinner() {
    setTimeout(function () {
        // $('.ajax-loader').remove();
        $('#div_loading').hide();
    }, 2000);
}     