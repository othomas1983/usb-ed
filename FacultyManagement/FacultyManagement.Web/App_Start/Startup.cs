﻿using FacultyManagement.Core.Security.Authentication;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(FacultyManagement.Web.Startup))]

namespace FacultyManagement.Web
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=316888
            AuthSetup.ConfigureAuth(app);
        }
    }
}
