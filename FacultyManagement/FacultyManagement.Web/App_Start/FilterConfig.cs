﻿using System.Web;
using System.Web.Mvc;
using FacultyManagement.Web.Infrastructure.Filters;

namespace FacultyManagement.Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters( GlobalFilterCollection filters )
        {
            filters.Add( new AuthorizeAttribute() );
            filters.Add( new AjaxExceptionFilter() );
        }
    }
}
