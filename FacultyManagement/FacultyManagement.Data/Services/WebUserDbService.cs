﻿using FacultyManagement.Data.Interfaces;
using System.Collections.Generic;
using System.Data;

namespace FacultyManagement.Data.Services
{
    public sealed class WebUserDbService : IWebUserDbService
    {

        /// <summary>
        /// Gets all web users.
        /// </summary>
        /// <param name="cVid">The c vid.</param>
        /// <returns></returns>
        public DataSet GetWebUsers(int cVid)
        {
            return DbService.GetDataSet("SELECT [cvid], [Surname], [Name], [webdisplay] FROM [CV]", CommandType.Text, null, 300);
        }
        
        /// <summary>
        /// Gets all web users for the specified display on web state.
        /// </summary>
        public DataSet GetWebUsers(int cVid, bool displayOnWeb)
        {

            var parameters = new Dictionary<string, object>
            {
                { "@display", displayOnWeb }
            };

            var x = DbService.GetDataSet("SELECT [cvid], [Surname], [Name], [webdisplay] FROM [CV] WHERE ISNULL([webdisplay],0) = @display ", CommandType.Text, parameters, 300);

            return x;
        }


        public void UpdateUserWebDisplayState(IEnumerable<int> cvIds, bool displayOnWeb)
        {
            //TOOD: underlying DvService will not treat this as a transaction ...
            //TODO: figure out batch IN ... at least this is better than a SQL injection ...
            foreach (var id in cvIds)
            {
                var parameters = new Dictionary<string, object>
                {
                    {"@display", displayOnWeb },
                    { "@Id", id }
                };

                DbService.GetDataSet("UPDATE [CV] SET WebDisplay = @display WHERE CVID =@Id", CommandType.Text, parameters, 300);
            }
        }
    }
}
