﻿using FacultyManagement.Data.Interfaces;
using FacultyManagement.Data.Models;
using FacultyManagement.Domain.ExtensionMethods;
using FacultyManagement.Domain.Model.EducationAndAwards;
using System.Collections.Generic;
using System.Data;

namespace FacultyManagement.Data.Services
{
    public class EducationAndAwardsDbService : IEducationAndAwardsDbService
    {
        #region Stored Procedure Queries

        /// <summary>
        /// Gets the education data.
        /// </summary>
        /// <param name="cVid">The users cvid.</param>
        /// <returns></returns>
        public DataSet GetEducations(int cVid)
        {
            var parameters = new Dictionary<string, object>
            {
                {"@CVID", cVid}
            };

            return DbService.GetDataSet("sp_EducationRead", CommandType.StoredProcedure, parameters, 300);
        }

        /// <summary>
        /// Gets the awards data.
        /// </summary>
        /// <param name="cVid">The c vid.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public DataSet GetAwards(int cVid)
        {
            var parameters = new Dictionary<string, object>
            {
                {"@CVID", cVid}
            };

            return DbService.GetDataSet("sp_AwardRead", CommandType.StoredProcedure, parameters, 300);
        }

        /// <summary>
        /// Gets the grants data.
        /// </summary>
        /// <param name="cVid">The c vid.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>   
        public DataSet GetGrants(int cVid)
        {
            var parameters = new Dictionary<string, object>
            {
                {"@CVID", cVid}
            };

            return DbService.GetDataSet("sp_GrantRead", CommandType.StoredProcedure, parameters, 300);
        }

        /// <summary>
        /// Gets the languages.
        /// </summary>
        /// <param name="cVid">The c vid.</param>
        /// <returns></returns>
        public DataSet GetLanguages(int cVid)
        {
            var parameters = new Dictionary<string, object>
            {
                {"@CVID", cVid}
            };

            return DbService.GetDataSet("sp_FacultyLanguageRead", CommandType.StoredProcedure, parameters, 300);
        }

        #endregion

        #region IDbService Methods

        /// <summary>
        /// Saves the changes made to the model to the database.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="username">The username.</param>
        /// <returns></returns>
        public bool SaveChanges(ISaveModel model, string username)
        {
            // Education
            if (model is SaveEducation)
            {
                var saveModel = model as SaveEducation;
                bool dataSaved = SaveData(saveModel, username);
                return dataSaved;
            }

            // Awards
            if (model is SaveAward)
            {
                return SaveData((SaveAward)model, username);
            }

            // Grants
            if (model is SaveGrant)
            {
                return SaveData((SaveGrant)model, username);
            }

            // Grants
            if (model is SaveFacultyLanguage)
            {
                return SaveData((SaveFacultyLanguage)model, username);
            }

            return false;
        }

        //TODO: add constraint to generic T
        public bool Delete<T>(int id, string username)
        {
            var query = string.Empty;

            if (typeof(T) == typeof(Education))
            {
                query = $"UPDATE  Education SET isActive = 0, ModifiedBy = @username  WHERE EducationID={id}";
            }

            if (typeof(T) == typeof(Award))
            {
                query = $"UPDATE  Award SET isActive = 0, ModifiedBy = @username  WHERE AwardID={id}";
            }

            if (typeof(T) == typeof(Grant))
            {
                query = $"UPDATE  [Grant] SET isActive = 0, ModifiedBy = @username  WHERE GrantID={id}";
            }

            if (typeof(T) == typeof(FacultyLanguage))
            {
                query = $"UPDATE  [FacultyLanguage] SET isActive = 0, ModifiedBy = @username  WHERE FacultyLanguageID={id}";
            }

            // Execute Query
            if (!string.IsNullOrEmpty(query))
            {
                var parameters = new Dictionary<string, object>()
                {
                    {"@username", username}
                };

                var rowsDeleted = DbService.ExecuteCommand(query, CommandType.Text, parameters);

                return rowsDeleted != 0;
            }

            return false;
        }
                
        #endregion

        //TODO: Design issue ... after saving why is the model not hydrated with the new state? e.g the ID ...

        #region Save Methods

        /// <summary>
        /// Saves the data for Educations.
        /// </summary>
        /// <param name="model">The model.</param>s
        /// <param name="username">The username.</param>
        /// <returns></returns>
        private bool SaveData(SaveEducation model, string username)
        {
            DataTable dataTable = MapToDataTable(model);

            var parameters = new Dictionary<string, object>
            {
                {"@tvpEducation", dataTable},
                {"@UserName", username}
            };

            int rowsUpdated = DbService.ExecuteCommand("sp_EducationCUD", CommandType.StoredProcedure, parameters, 300);

            return rowsUpdated != 0;
        }

        /// <summary>
        /// Saves the data for Awards.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="username">The username.</param>
        /// <returns></returns>
        private bool SaveData(SaveAward model, string username)
        {
            DataTable dataTable = MapToDataTable(model);

            var parameters = new Dictionary<string, object>
            {
                {"@tvpAward", dataTable},
                {"@UserName", username}
            };

            int rowsUpdated = DbService.ExecuteCommand("sp_AwardCUD", CommandType.StoredProcedure, parameters, 300);

            return rowsUpdated != 0;
        }

        /// <summary>
        /// Saves the data for Grants.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="username">The username.</param>
        /// <returns></returns>
        private bool SaveData(SaveGrant model, string username)
        {
            DataTable dataTable = MapToDataTable(model);

            var parameters = new Dictionary<string, object>
            {
                {"@tvpGrant", dataTable},
                {"@UserName", username}
            };

            int rowsUpdated = DbService.ExecuteCommand("sp_GrantCUD", CommandType.StoredProcedure, parameters, 300);

            return rowsUpdated != 0;
        }

        private bool SaveData(SaveFacultyLanguage model, string username)
        {
            DataTable dataTable = MapToDataTable(model);

            var parameters = new Dictionary<string, object>
            {
                {"@tvpFacultyLanguage", dataTable},
                {"@UserName", username}
            };

            int rowsUpdated = DbService.ExecuteCommand("sp_FacultyLanguageCUD", CommandType.StoredProcedure, parameters, 300);

            return rowsUpdated != 0;
        }

        private DataTable MapToDataTable(SaveEducation model)
        {
            var dataTable = new DataTable();

            dataTable.Columns.Add("CVID");
            dataTable.Columns.Add("EducationYear");
            dataTable.Columns.Add("Degree");
            dataTable.Columns.Add("FieldOfStudy");
            dataTable.Columns.Add("University");
            dataTable.Columns.Add("EducationCountry");
            dataTable.Columns.Add("isActive");
            dataTable.Columns.Add("EducationID");

            var newRow = dataTable.NewRow();
            dataTable.Rows.Add(newRow);

            newRow["CVID"] = model.CVid;
            newRow["EducationYear"] = model.Year;
            newRow["Degree"] = model.Degree;
            newRow["FieldOfStudy"] = model.FieldOfStudy;
            newRow["University"] = model.University;
            newRow["EducationCountry"] = model.Country;
            newRow["IsActive"] = model.IsActive;
            newRow["EducationID"] = model.Id.AsIntergerOrNull();

            return dataTable;
        }

        private DataTable MapToDataTable(SaveAward model)
        {
            var dataTable = new DataTable();

            dataTable.Columns.Add("CVID");
            dataTable.Columns.Add("Year");
            dataTable.Columns.Add("AwardType");
            dataTable.Columns.Add("AwardDescription");
            dataTable.Columns.Add("Country");
            dataTable.Columns.Add("IsActive");
            dataTable.Columns.Add("AwardID");

            var newRow = dataTable.NewRow();
            dataTable.Rows.Add(newRow);

            newRow["CVID"] = model.CVid;
            newRow["Year"] = model.Year;
            newRow["AwardType"] = model.AwardType;
            newRow["AwardDescription"] = model.Description;
            newRow["Country"] = model.Country;
            newRow["IsActive"] = model.IsActive;
            newRow["AwardID"] = model.Id.AsIntergerOrNull();

            return dataTable;
        }

        private DataTable MapToDataTable(SaveGrant model)
        {
            var dataTable = new DataTable();

            dataTable.Columns.Add("GrantID");
            dataTable.Columns.Add("CVID");
            dataTable.Columns.Add("Year");
            dataTable.Columns.Add("GrantFunding");
            dataTable.Columns.Add("Description");
            dataTable.Columns.Add("Country");
            dataTable.Columns.Add("IsActive");

            var newRow = dataTable.NewRow();
            dataTable.Rows.Add(newRow);

            newRow["GrantID"] = model.Id.AsIntergerOrNull();
            newRow["CVID"] = model.CVid;
            newRow["Year"] = model.Year;
            newRow["GrantFunding"] = model.GrantFunding;
            newRow["Description"] = model.Description;
            newRow["Country"] = model.Country;
            newRow["IsActive"] = model.IsActive;

            return dataTable;
        }

        private DataTable MapToDataTable(SaveFacultyLanguage model)
        {
            var dataTable = new DataTable();

            dataTable.Columns.Add("CVID");
            dataTable.Columns.Add("LanguageID");
            dataTable.Columns.Add("isActive");
            dataTable.Columns.Add("FacultyLanguageID");

            var newRow = dataTable.NewRow();
            dataTable.Rows.Add(newRow);

            newRow["CVID"] = model.CVid;
            newRow["LanguageID"] = model.LanguageId;
            newRow["isActive"] = model.IsActive;
            newRow["FacultyLanguageID"] = model.Id.AsIntergerOrNull();

            return dataTable;
        }
        
        #endregion


    }
}
