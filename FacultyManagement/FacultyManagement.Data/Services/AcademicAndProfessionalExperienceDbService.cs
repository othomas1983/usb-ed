﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Data.Interfaces;
using FacultyManagement.Data.Models;
using FacultyManagement.Domain.ExtensionMethods;
using FacultyManagement.Domain.Model.AcademicAndProfessionalExperience;

namespace FacultyManagement.Data.Services
{
    public class AcademicAndProfessionalExperienceDbService : IAcademicAndProfessionalExperienceDbService
    {
        /// <summary>
        /// Gets the experiences.
        /// </summary>
        /// <param name="cVid">The cvid.</param>
        /// <returns></returns>
        public DataSet GetExperiences( int cVid )
        {
            var parameters = new Dictionary<string, object>
            {
                {"@CVID", cVid}
            };

            return DbService.GetDataSet( "sp_ExperienceRead", CommandType.StoredProcedure, parameters, 300 );
        }

        /* /// <summary>
         /// Deletes the experience.
         /// </summary>
         /// <param name="id">The identifier.</param>
         /// <param name="username">The username.</param>
         /// <returns></returns>
         public bool DeleteExperience( int id, string username )
         {
             string query = $"UPDATE  Experience SET isActive = 0, ModifiedBy='{username}'  WHERE ExperienceID={id}";
             int rowsDeleted = DbService.ExecuteCommand( query );

             return rowsDeleted != 0;
         }*/

        #region IDbService Methods
        /// <summary>
        /// Saves the changes made to the model to the database.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="username">The username.</param>
        /// <returns></returns>
        public bool SaveChanges( ISaveModel model, string username )
        {
            if ( model is SaveExperience )
            {
                return SaveData( (SaveExperience) model, username );
            }

            return false;
        }

        public bool Delete<T>( int id, string username )
        {
            if ( typeof( T ) == typeof( Experience ) )
            {
                string query = $"UPDATE  Experience SET isActive = 0, ModifiedBy='{username}'  WHERE ExperienceID={id}";
                int rowsDeleted = DbService.ExecuteCommand( query );

                return rowsDeleted != 0;
            }

            return false;
        }

        #endregion
        #region Private Methods

        /// <summary>
        /// Saves the data.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="username">The username.</param>
        /// <returns></returns>
        private bool SaveData( SaveExperience model, string username )
        {
            DataTable dataTable = MapToDataTable( model );

            var parameters = new Dictionary<string, object>
            {
                {"@tvpExperience", dataTable},
                {"@UserName", username}
            };

            int rowsUpdated = DbService.ExecuteCommand( "sp_ExperienceCUD", CommandType.StoredProcedure, parameters, 300 );

            return rowsUpdated != 0;
        }

        /// <summary>
        /// Maps to data table.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        private DataTable MapToDataTable( SaveExperience model )
        {
            var dataTable = new DataTable();

            dataTable.Columns.Add( "CVID" );
            dataTable.Columns.Add( "ExperienceCategoryID" );
            dataTable.Columns.Add( "StartDate", typeof( DateTime ) ).DateTimeMode = DataSetDateTime.Local;
            dataTable.Columns.Add( "EndDate", typeof( DateTime ) ).DateTimeMode = DataSetDateTime.Local;
            dataTable.Columns.Add( "SubjectArea" );
            dataTable.Columns.Add( "Position" );
            dataTable.Columns.Add( "Institution" );
            dataTable.Columns.Add( "City" );
            dataTable.Columns.Add( "Country" );
            dataTable.Columns.Add( "IsCurrent" );
            dataTable.Columns.Add( "isActive" );
            dataTable.Columns.Add( "ExperienceID" );
            dataTable.Columns.Add( "NoOfDays" );
            dataTable.Columns.Add( "Programme" );
            dataTable.Columns.Add( "KnowledgeArea" );
            dataTable.Columns.Add( "Client" );
            dataTable.Columns.Add( "Publication" );
            dataTable.Columns.Add( "PublicationStatus" );
            dataTable.Columns.Add( "PublicationEdition" );

            var newRow = dataTable.NewRow();
            dataTable.Rows.Add( newRow );

            newRow["CVID"] = model.CVid;
            newRow["ExperienceCategoryID"] = model.ExperienceCategoryId;
            newRow["StartDate"] = model.StartDate.ToLocalTime();
            if ( model.EndDate.HasValue )
            {
                newRow["EndDate"] = model.EndDate.Value.ToLocalTime();
            }
            else
            {
                newRow["EndDate"] = DBNull.Value;
            }            
            newRow["SubjectArea"] = model.Subject;
            newRow["Position"] = model.Position;
            newRow["Institution"] = model.Institution;
            newRow["City"] = model.City;
            newRow["Country"] = model.Country;
            newRow["IsCurrent"] = model.IsCurrent;
            newRow["isActive"] = model.IsActive;
            newRow["ExperienceID"] = model.Id.AsIntergerOrNull();
            newRow["NoOfDays"] = model.NoOfDays;
            newRow["Programme"] = model.Programme;
            newRow["KnowledgeArea"] = model.KnowledgeArea;
            newRow["Client"] = model.Client;
            newRow["Publication"] = model.Publication;
            newRow["PublicationStatus"] = model.PublicationStatus;
            newRow["PublicationEdition"] = model.PublicationEdition;

            return dataTable;
        }

        #endregion
    }
}
