﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Data.Interfaces;
using FacultyManagement.Data.Models;
using FacultyManagement.Domain.ExtensionMethods;
using FacultyManagement.Domain.Model.AcademicAndProfessionalExperience;
using FacultyManagement.Domain.Model.ProfessionalAndPersonalDevelopment;

namespace FacultyManagement.Data.Services
{
    public class ProfessionalAndPersonalDevelopmentDbService : IProfessionalAndPersonalDevelopmentDbService
    {
        /// <summary>
        /// Gets the developments.
        /// </summary>
        /// <param name="cVid">The cvid.</param>
        /// <returns></returns>
        public DataSet GetDevelopments(int cVid)
        {
            var parameters = new Dictionary<string, object>
            {
                {"@CVID", cVid}
            };

            return DbService.GetDataSet("sp_DevelopmentRead", CommandType.StoredProcedure, parameters, 300);
        }

        public bool Delete<T>(int id, string username) //TODO: add constraint to generic T or remove
        {
            var query = string.Empty;

            if (typeof(T) == typeof(Development))
            {
                query = $"UPDATE  Development SET isActive = 0, ModifiedBy = @username WHERE DevelopmentID={id}";
            }


            if (!string.IsNullOrEmpty(query))
            {
                var parameters = new Dictionary<string, object>()
                {
                    {"@username", username}
                };

                var rowsDeleted = DbService.ExecuteCommand(query, CommandType.Text, parameters);

                return rowsDeleted != 0;
            }


            return false;
        }

        #region IDbService Methods

        /// <summary>
        /// Saves the changes made to the model to the database.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="username">The username.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public bool SaveChanges(ISaveModel model, string username)
        {
            if (model is SaveDevelopment)
            {
                return SaveData((SaveDevelopment)model, username);
            }

            return false;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Saves the data.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="username">The username.</param>
        /// <returns></returns>
        private bool SaveData(SaveDevelopment model, string username)
        {
            DataTable dataTable = MapToDataTable(model);

            var parameters = new Dictionary<string, object>
            {
                {"@tvpDevelopment", dataTable},
                {"@UserName", username}
            };

            int rowsUpdated = DbService.ExecuteCommand("sp_DevelopmentCUD", CommandType.StoredProcedure, parameters, 300);

            return rowsUpdated != 0;
        }

        /// <summary>
        /// Maps to data table.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        private DataTable MapToDataTable(SaveDevelopment model)
        {
            var dataTable = new DataTable();

            dataTable.Columns.Add("CVID");
            dataTable.Columns.Add("DevelopmentCategoryID");
            dataTable.Columns.Add("Year");
            dataTable.Columns.Add("Accomplishment");
            dataTable.Columns.Add("StartDate", typeof(DateTime)).DateTimeMode = DataSetDateTime.Local;
            dataTable.Columns.Add("EndDate", typeof(DateTime)).DateTimeMode = DataSetDateTime.Local;
            dataTable.Columns.Add("Topic");
            dataTable.Columns.Add("NoOfDays");
            dataTable.Columns.Add("Institution");
            dataTable.Columns.Add("Country");
            dataTable.Columns.Add("isCurrent");
            dataTable.Columns.Add("Position");
            dataTable.Columns.Add("Organisation");
            dataTable.Columns.Add("City");
            dataTable.Columns.Add("isActive");
            dataTable.Columns.Add("DevelopmentID");

            var newRow = dataTable.NewRow();
            dataTable.Rows.Add(newRow);

            newRow["CVID"] = model.CVid;
            newRow["DevelopmentCategoryID"] = model.DevelopmentCategoryId;
            //newRow["StartDate"] = model.StartDate;
            //newRow["EndDate"] = model.EndDate;
            newRow["Accomplishment"] = model.Accomplishment;
            newRow["StartDate"] = model.StartDate.ToLocalTime();
            if (model.EndDate.HasValue)
            {
                newRow["EndDate"] = model.EndDate.Value.ToLocalTime();
            }
            else
            {
                newRow["EndDate"] = DBNull.Value;
            }
            newRow["Topic"] = model.Topic;
            newRow["NoOfDays"] = model.NoOfDays;
            newRow["Institution"] = model.Institution;
            newRow["Country"] = model.Country;
            newRow["isCurrent"] = model.IsCurrent;
            newRow["Position"] = model.Position;
            newRow["Organisation"] = model.Organisation;
            newRow["City"] = model.City;
            newRow["isActive"] = model.IsActive;
            newRow["DevelopmentID"] = model.Id.AsIntergerOrNull();

            return dataTable;
        }

        #endregion
    }
}
