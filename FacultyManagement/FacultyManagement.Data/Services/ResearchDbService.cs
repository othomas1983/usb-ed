﻿using FacultyManagement.Data.Interfaces;
using FacultyManagement.Data.Models;
using FacultyManagement.Domain.ExtensionMethods;
using FacultyManagement.Domain.Model.Research;
using FacultyManagement.Infrastructure.Utilities.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace FacultyManagement.Data.Services
{
    public class ResearchDbService : IResearchDbService
    {
        /// <summary>
        /// Gets the research.
        /// </summary>
        /// <param name="cVid">The cvid.</param>
        /// <returns></returns>
        public DataSet GetResearch(int cVid)
        {
            var parameters = new Dictionary<string, object>
            {
                {"@CVID", cVid}
            };

            return DbService.GetDataSet("sp_ResearchHistoryRead", CommandType.StoredProcedure, parameters, 300);
        }

        /// <summary>
        /// Gets the contributors.
        /// </summary>
        /// <param name="researchId">The research identifier.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public DataSet GetContributors(int researchId)
        {
            var parameters = new Dictionary<string, object>
            {
                {"@ResearchID", researchId}
            };

            return DbService.GetDataSet("sp_PeopleinResearchRead", CommandType.StoredProcedure, parameters, 300);
        }

        #region IDbService Methods

        /// <summary>
        /// Saves the changes made to the model to the database.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="username">The username.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public bool SaveChanges(ISaveModel model, string username)
        {
            if (model is SaveResearchActivity)
            {
                return SaveData((SaveResearchActivity)model, username);
            }

            return false;
        }

        /// <summary>
        /// Deletes the specified database entity by its identifier.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id">The identifier.</param>
        /// <param name="username">The username.</param>
        /// <returns></returns>
        public bool Delete<T>(int id, string username) //TODO: add constraint to generic T
        {
            var query = $"UPDATE Research SET isActive = 0, ModifiedBy = @username WHERE ResearchID={id}";

            var parameters = new Dictionary<string, object>
            {
                 {"@username", username}
            };

            var rowsDeleted = DbService.ExecuteCommand(query, CommandType.Text, parameters);

            return rowsDeleted != 0;
        }

        #endregion

        #region Private Methods

        private bool SaveData(SaveResearchActivity model, string username)
        {
            #region Research

            DataTable dataTable = MapToDataTable(model);

            var researchIdOut = new SqlParameter("@ResearchID", SqlDbType.Int)
            {
                Direction = ParameterDirection.Output
            };

            var parameters = new Dictionary<string, SqlParameterInfo>
            {
                {"@tvpResearch", new SqlParameterInfo( "ResearchTable",SqlDbType.Structured,ParameterDirection.Input, dataTable)},
                {"@UserName", new SqlParameterInfo( username)},
            };

            int researchUpdated = DbService.ExecuteOutCommand("EXEC sp_ResearchCUD @TVPResearch, @UserName, @ResearchID OUT", parameters, ref researchIdOut, CommandType.Text, 300);
            #endregion

            // There was a problem
            if (researchUpdated == 0) return false;

            // Set the ResearchId on the model since we have it for sure now
            model.ResearchId = model.ResearchId ?? (int)researchIdOut.Value;

            #region Research History

            AddAndUpdateResearchHistory(model, username);

            #endregion

            #region Contributors

            AddOrUpdateContributors(model, username);
            RemoveContributors(model, username);

            #endregion

            return researchUpdated != 0;
        }



        /// <summary>
        /// Adds the and update research history.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="username">The username.</param>
        private void AddAndUpdateResearchHistory(SaveResearchActivity model, string username)
        {
            // Check if research status or research status date has changed
            string query = $@"SELECT Count(*) FROM ResearchHistory WHERE ResearchID = {model.ResearchId}
                                            AND ResearchStatusID = {model.ResearchStatusId}  
                                            AND ResearchStatusDate = '{model.ResearchStatusDate:yyyy-MM-dd}'
                                            AND IsActive = 1";

            int? researchCount = DbService.ExecuteScaler(query).ToString().AsIntegerOrNull();
            // Research History has changed
            if (researchCount == 0)
            {
                DataTable historyTable = CreateResearchHistoryDataTable(model);
                var historyParamaters = new Dictionary<string, object>
                {
                    {"@TVPResearchHistory", historyTable},
                    {"@UserName", username}
                };

                DbService.ExecuteCommand("sp_ResearchHistoryCreate", CommandType.StoredProcedure, historyParamaters, 300);

                // Update all history records for this research except the one added above to inactive
                query = $@"UPDATE ResearchHistory SET isActive = 0 WHERE ResearchID = {model.ResearchId} 
                                    AND NOT ( ResearchStatusID = {model.ResearchStatusId} 
                                    AND ResearchStatusDate = '{model.ResearchStatusDate:yyyy-MM-dd}')";


                DbService.ExecuteCommand(query);
            }
        }

        #region Contributors

        /// <summary>
        /// Adds the or update contributors.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="username">The username.</param>
        private void AddOrUpdateContributors(SaveResearchActivity model, string username)
        {
            if (model.Contributors != null && model.Contributors.Any())
            {
                DataTable contributorsTable = CreateContributorsDataTable(model.Contributors, model.ResearchId.Value);
                var contributorsParameters = new Dictionary<string, object>
                {
                    {"@tvpPeopleInResearch", contributorsTable},
                    {"@UserName", username}
                };

                DbService.ExecuteCommand("sp_PeopleinResearchCUD", CommandType.StoredProcedure, contributorsParameters, 300);
            }
        }

        /// <summary>
        /// Removes the contributors.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="username">The username.</param>
        private void RemoveContributors(SaveResearchActivity model, string username)
        {
            if (model.ContributorsToRemove != null && model.ContributorsToRemove.Any())
            {
                foreach (var contributor in model.ContributorsToRemove)
                {
                    var query = $"UPDATE PeopleinResearch SET isActive = 0, ModifiedBy=@username WHERE PeopleinResearchID={contributor.Id}"; ;

                    var parameters = new Dictionary<string, object>()
                    {
                        { "@username", username}
                    };

                    DbService.ExecuteCommand(query, CommandType.Text, parameters);
                }
            }
        }
        #endregion

        private DataTable MapToDataTable(SaveResearchActivity model)
        {
            var dataTable = new DataTable();


            dataTable.Columns.Add("CVID");
            dataTable.Columns.Add("ResearchCategoryID");
            dataTable.Columns.Add("Year");
            dataTable.Columns.Add("ResearchOutputCategoryID");
            dataTable.Columns.Add("DHETStatusID");
            dataTable.Columns.Add("LocusID");
            dataTable.Columns.Add("Description");
            dataTable.Columns.Add("Volume");
            dataTable.Columns.Add("PublisherName");
            dataTable.Columns.Add("PublicationCity");
            dataTable.Columns.Add("Country");
            dataTable.Columns.Add("NoOfPages");
            dataTable.Columns.Add("StartPage");
            dataTable.Columns.Add("EndPage");
            dataTable.Columns.Add("Edition");
            dataTable.Columns.Add("Issue");
            dataTable.Columns.Add("ArticleID");
            dataTable.Columns.Add("ArticleTitle");
            dataTable.Columns.Add("PublicationName");
            dataTable.Columns.Add("PublicationDate");
            dataTable.Columns.Add("SupervisionOrAssessment");
            dataTable.Columns.Add("Thesis");
            dataTable.Columns.Add("Candidate");
            dataTable.Columns.Add("isActive");
            dataTable.Columns.Add("ResearchID");
            dataTable.Columns.Add("CoordinatorID");
            dataTable.Columns.Add("JournalID");
            dataTable.Columns.Add("PublicationFile");
            //dataTable.Columns.Add("FileName");
            dataTable.Columns.Add("Note");
            dataTable.Columns.Add("ABSRatingStatusID");
            dataTable.Columns.Add("Credit");
            dataTable.Columns.Add("ResearchContributionTypeID");

            var newRow = dataTable.NewRow();
            dataTable.Rows.Add(newRow);


            newRow["CVID"] = model.CVid;
            newRow["ResearchCategoryID"] = ResearchHistory.ResearchCategoryId;
            newRow["Year"] = model.ResearchStatusDate.Year;
            newRow["ResearchOutputCategoryID"] = model.ResearchOutputCategoryId;
            newRow["DHETStatusID"] = model.DHETStatusId;
            newRow["LocusID"] = model.LocusId;
            newRow["Description"] = model.Description;
            newRow["Volume"] = model.Volume;
            newRow["PublisherName"] = model.PublisherName;
            newRow["PublicationCity"] = model.PublicationCity;
            newRow["Note"] = model.Note;
            newRow["Country"] = model.Country;
            newRow["NoOfPages"] = model.NoOfPages;
            newRow["StartPage"] = model.StartPage;
            newRow["EndPage"] = model.EndPage;
            newRow["Edition"] = model.Edition;
            newRow["Issue"] = model.Issue;
            newRow["ArticleID"] = null;
            newRow["ArticleTitle"] = model.ArticleTitle;
            newRow["PublicationName"] = model.PublicationName;
            newRow["SupervisionOrAssessment"] = null;
            newRow["Thesis"] = null;
            newRow["Candidate"] = null;
            newRow["isActive"] = 1;
            newRow["ResearchID"] = model.ResearchId.AsIntergerOrNull();
            newRow["CoordinatorID"] = 0;
            newRow["JournalID"] = null;
            newRow["PublicationFile"] = model.PublicationFile;
           // newRow["FileName"] = model.FileName;
            newRow["Note"] = model.Note;
            newRow["ABSRatingStatusID"] = model.ABSRatingStatusId;
            newRow["Credit"] = model.Credit;
            newRow["ResearchContributionTypeID"] = model.ResearchContributionTypeId;
            

            return dataTable;
        }

        private DataTable CreateResearchHistoryDataTable(SaveResearchActivity model)
        {
            var dataTable = new DataTable();

            dataTable.Columns.Add("ResearchID");
            dataTable.Columns.Add("ResearchStatusID");
            dataTable.Columns.Add("ResearchStatusDate", typeof(DateTime));
            dataTable.Columns["ResearchStatusDate"].DateTimeMode = DataSetDateTime.Local;
            dataTable.Columns.Add("isPresent");
            dataTable.Columns.Add("isActive");
            dataTable.Columns.Add("ResearchHistoryID");

            var newRow = dataTable.NewRow();
            dataTable.Rows.Add(newRow);

            newRow["ResearchID"] = model.ResearchId;
            newRow["ResearchStatusID"] = model.ResearchStatusId;
            newRow["ResearchStatusDate"] = model.ResearchStatusDate;
            newRow["isPresent"] = 1;
            newRow["isActive"] = 1;
            newRow["ResearchHistoryID"] = null;

            return dataTable;
        }

        private DataTable CreateContributorsDataTable(List<SaveContributor> contributors, int researchId)
        {
            var dataTable = new DataTable();

            dataTable.Columns.Add("PeopleinResearchID");
            dataTable.Columns.Add("ResearchID");
            dataTable.Columns.Add("CVID");
            dataTable.Columns.Add("PersonRoleID");
            dataTable.Columns.Add("isActive");
            dataTable.Columns.Add("Credit", typeof(decimal));
            dataTable.Columns.Add("ContributorSequence");
            dataTable.Columns.Add("Type");
            dataTable.Columns.Add("Name");
            dataTable.Columns.Add("Surname");
            dataTable.Columns.Add("Initials");
            dataTable.Columns.Add("Institution");
            dataTable.Columns.Add("Email");
            dataTable.Columns.Add("AffiliationID");
            dataTable.Columns.Add("Gender");
            dataTable.Columns.Add("NationalityID");

            foreach (var contributor in contributors)
            {
                var newRow = dataTable.NewRow();

                newRow["PeopleinResearchID"] = contributor.Id.AsIntergerOrNull();
                newRow["ResearchID"] = researchId;
                newRow["CVID"] = contributor.CVid;
                newRow["PersonRoleID"] = contributor.RoleId;
                newRow["isActive"] = contributor.IsActive;
                newRow["Credit"] = contributor.DHETCredit;
                newRow["ContributorSequence"] = contributor.Sequence;
                newRow["Name"] = contributor.FirstName;
                newRow["Surname"] = contributor.Surname;
                newRow["Initials"] = contributor.Initials;
                newRow["Institution"] = contributor.Institution;
                newRow["Type"] = contributor.Type;
                newRow["Email"] = contributor.Email;
                newRow["AffiliationID"] = contributor.AffiliationId;
                newRow["Gender"] = contributor.Gender;
                newRow["NationalityID"] = contributor.NationalityId;

                dataTable.Rows.Add(newRow);
            }

            return dataTable;
        }

        #endregion
    }
}
