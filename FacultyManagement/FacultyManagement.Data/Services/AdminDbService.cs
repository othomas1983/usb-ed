﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Data.Interfaces;
using FacultyManagement.Data.Models;
using FacultyManagement.Domain.ExtensionMethods;
using FacultyManagement.Domain.Model;
using FacultyManagement.Infrastructure.Utilities.ExtensionMethods;
using System.Data.SqlClient;
using System.Configuration;
using FacultyManagement.Domain.Model.Common;

namespace FacultyManagement.Data.Services
{
    public class AdminDbService : IAdminDbService
    {
        #region Departments

        public DataSet GetWebUsers(int cVid)
        {          

            return DbService.GetDataSet("Select cvid, surname, [name], webdisplay from cv", CommandType.Text,null,300);
        }
        /// <summary>
        /// Gets the department categories.
        /// </summary>
        /// <param name="cVid">The c vid.</param>
        /// <returns></returns>
        public DataSet GetFacultyDepartments( int cVid )
        {
            var parameters = new Dictionary<string, object>
            {
                {"@CVID", cVid}
            };

            return DbService.GetDataSet( "sp_FacultyDepartmentRead", CommandType.StoredProcedure, parameters, 300 );
        }

        #endregion

        #region Classifications

        /// <summary>
        /// Gets the department categories.
        /// </summary>
        /// <param name="cVid">The c vid.</param>
        /// <returns></returns>
        public DataSet GetClassificationsAndSufficiencies( int cVid )
        {
            var parameters = new Dictionary<string, object>
            {
                {"@CVID", cVid}
            };

            return DbService.GetDataSet( "sp_ClassificationSufficiencyRead", CommandType.StoredProcedure, parameters, 300 );
        }

        #endregion

        #region WebUsers
        /// <summary>
        /// Adds the Active Directory users to Faculty Management
        /// </summary>
        /// <param name="users">The users.</param>
        /// <param name="username"></param>
        /// <returns></returns>s
        /// <exception cref="System.NotImplementedException"></exception>

        public bool AddWebUsers(IEnumerable<WebUser> users, string username)
        {
            //{
            //    bool success = false;

            //    foreach (var user in users)
            //    {
            //        DataTable dataTable = MapToDataTable(user);

            //        var parameters = new Dictionary<string, object>
            //        {
            //            {"tvpPersonalInfo", dataTable},
            //            {"@UserName", username}
            //        };

            //        int rowsUpdated = DbService.ExecuteCommand("sp_PersonalInformationCreate", CommandType.StoredProcedure, parameters, 300);

            //        success = rowsUpdated != 0;

            //        if (!success) return false;
            //    }
            //    return success;
            return false;

        }

        #endregion

        #region IDbService Methods

        public bool SaveChanges(ISaveModel model, string username)
        {
            if (model is SaveFacultyDepartment)
            {
                return SaveData((SaveFacultyDepartment)model, username);
            }
            if (model is SaveClassifications)
            {
                return SaveData((SaveClassifications)model, username);
            }
            if (model is SaveExecutiveProfile)
            {
                return SaveData((SaveExecutiveProfile)model, username);
            }

            return false;
        }


        public bool Delete<T>(int id, string username)
        {
            throw new NotImplementedException();
        }

        #endregion


        #region Users
        /// <summary>
        /// Adds the Active Directory users to Faculty Management
        /// </summary>
        /// <param name="users">The users.</param>
        /// <param name="username"></param>
        /// <returns></returns>s
        /// <exception cref="System.NotImplementedException"></exception>
        public bool AddUsers( IEnumerable<ActiveDirectoryUser> users, string username )
        {
            bool success = false;

            foreach ( var user in users )
            {
                DataTable dataTable = MapToDataTable( user );

                var parameters = new Dictionary<string, object>
                {
                    {"tvpPersonalInfo", dataTable},
                    {"@UserName", username}
                };

                int rowsUpdated = DbService.ExecuteCommand( "sp_PersonalInformationCreate", CommandType.StoredProcedure, parameters, 300 );

                success = rowsUpdated != 0;

                if ( !success ) return false;
            }
            return success;
        }

        #endregion

        #region IDbService Methods

        public bool SaveWebChanges( ISaveModel model, string username )
        {
            if ( model is SaveFacultyDepartment )
            {
                return SaveData( (SaveFacultyDepartment) model, username );
            }
            if ( model is SaveClassifications )
            {
                return SaveData( (SaveClassifications) model, username );
            }
            if ( model is SaveExecutiveProfile )
            {
                return SaveData( (SaveExecutiveProfile) model, username );
            }

            return false;
        }

       
        public bool DeleteWeb<T>( int id, string username )
        {
            throw new NotImplementedException();
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Saves the data for management loads.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="username">The username.</param>
        /// <returns></returns>
        private bool SaveData( SaveFacultyDepartment model, string username )
        {
            DataTable dataTable = MapToDataTable( model );

            var parameters = new Dictionary<string, object>
            {
                {"@tvpFacultyDepartment", dataTable},
                {"@UserName", username}
            };

            int rowsUpdated = DbService.ExecuteCommand( "sp_FacultyDepartmentCUD", CommandType.StoredProcedure, parameters, 300 );

            return rowsUpdated != 0;
        }

        /// <summary>
        /// Saves the data for  Classifications & Sufficiencies .
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="username">The username.</param>
        /// <returns></returns>       
        private bool SaveData( SaveClassifications model, string username )
        {
            var parameters = new Dictionary<string, object>
            {
                {"@ClassificationID", model.ClassificationId},
                {"@SufficiencyID", model.SufficiencyId},
                {"@CVID", model.CVid},
                {"@UserName", username}
            };

            int rowsUpdated = DbService.ExecuteCommand( "sp_ClassificationSufficiencyUpdate", CommandType.StoredProcedure, parameters, 300 );

            return rowsUpdated != 0;
        }

        private bool SaveData( SaveExecutiveProfile model, string username )
        {
            string safeString = model.Biography.EscapeSqlQuotes();

            string query = $@"UPDATE  CV SET Biography = '{safeString}', ModifiedBy='{username}'  WHERE CVID={model.CVid}";

            int rowsUpdated = DbService.ExecuteCommand( query );           

            return rowsUpdated != 0;
        }

        private DataTable MapToDataTable( ActiveDirectoryUser user )
        {
            var dataTable = new DataTable();

            dataTable.Columns.Add( "CVID" );
            dataTable.Columns.Add( "Biography" );
            dataTable.Columns.Add( "Title" );
            dataTable.Columns.Add( "Initials" );
            dataTable.Columns.Add( "Name" );
           // dataTable.Columns.Add("GivenName");
            dataTable.Columns.Add( "Surname" );
            dataTable.Columns.Add( "Gender" );
            dataTable.Columns.Add( "NationalityID" );
            dataTable.Columns.Add( "BusinessContactNo" );
            dataTable.Columns.Add( "EmailAddress" );
            dataTable.Columns.Add( "Position" );
            dataTable.Columns.Add( "AcademicRank" );
            dataTable.Columns.Add( "HighestQualificationID" );
            dataTable.Columns.Add( "BusinessUnit" );
            dataTable.Columns.Add( "Expertise" );
            dataTable.Columns.Add( "EmployeeID" );
            dataTable.Columns.Add( "HomeInstitution" );
            dataTable.Columns.Add( "OrganisationalResponsibility" );
            dataTable.Columns.Add( "AffiliationID" );
            dataTable.Columns.Add( "ResearchInterest" );
            dataTable.Columns.Add( "MainActivityID" );
            dataTable.Columns.Add( "Samaccountname" );
            dataTable.Columns.Add( "isFaculty" );
            dataTable.Columns.Add("WebDisplay");


            var newRow = dataTable.NewRow();
            dataTable.Rows.Add( newRow );

            newRow["CVID"] = null;
            newRow["Biography"] = null;
            newRow["Title"] = null;
            newRow["Initials"] = null;
           newRow["Name"] = user.GivenName;
            newRow["Surname"] = user.Surname;
          // newRow["GivenName"] = user.GivenName;
            newRow["Gender"] = null;
            newRow["NationalityID"] = null;
            newRow["BusinessContactNo"] = null;
            newRow["EmailAddress"] = user.EmailAddress;
            newRow["Position"] = null;
            newRow["AcademicRank"] = null;
            newRow["HighestQualificationID"] = null;
            newRow["BusinessUnit"] = null;
            newRow["Expertise"] = null;
            newRow["EmployeeID"] = user.EmployeeId;
            newRow["HomeInstitution"] = null;
            newRow["OrganisationalResponsibility"] = null;
            newRow["AffiliationID"] = 5;
            newRow["ResearchInterest"] = null;
            newRow["MainActivityID"] = 1;
            newRow["Samaccountname"] = user.SamAccountName;
            newRow["isFaculty"] = 1;
            newRow["WebDisplay"] = null;

            return dataTable;
        }

        private DataTable MapToDataTable(WebUser model)
        {
            //var dataTable = new DataTable();

            //dataTable.Columns.Add("CVID");

            //dataTable.Columns.Add("WebDisplay");

            //foreach (var department in model.CVid)
            //{
            //    var newRow = dataTable.NewRow();
            //    newRow["CVID"] = department.CVid;
            //    newRow["FacultyDepartmentID"] = department.FacultyDepartmentId.AsIntergerOrNull();
            //    newRow["DepartmentID"] = department.DepartmentId;
            //    newRow["DepartmentCategoryID"] = department.DepartmentCategoryId;
            //    newRow["isActive"] = department.IsActive;

            //    dataTable.Rows.Add(newRow);
            //}

            //return dataTable;
            return null;
        }

        private DataTable MapToDataTable( SaveFacultyDepartment model )
        {
            var dataTable = new DataTable();

            dataTable.Columns.Add( "CVID" );
            dataTable.Columns.Add( "FacultyDepartmentID" );
            dataTable.Columns.Add( "DepartmentID" );
            dataTable.Columns.Add( "DepartmentCategoryID" );
            dataTable.Columns.Add( "isActive" );

            foreach ( var department in model.Departments )
            {
                var newRow = dataTable.NewRow();
                newRow["CVID"] = department.CVid;
                newRow["FacultyDepartmentID"] = department.FacultyDepartmentId.AsIntergerOrNull();
                newRow["DepartmentID"] = department.DepartmentId;
                newRow["DepartmentCategoryID"] = department.DepartmentCategoryId;
                newRow["isActive"] = department.IsActive;

                dataTable.Rows.Add( newRow );
            }

            return dataTable;
        }

        private static string GetConnectionString()
        {
            // Library setting file : used for unit and intergration tests
            string testingConnectionString = Properties.Settings.Default.FacultyContext;
            // Load application setting
            var connectionString = ConfigurationManager.ConnectionStrings["FacultyContext"];
            // Always prefer application setting
            return connectionString != null ? connectionString.ConnectionString : testingConnectionString;
        }

        #endregion
            #region Get Dataset
    /// <summary>
    /// Gets a data set.
    /// </summary>
    /// <param name="query">The query.</param>
    /// <param name="commandType">Type of the command.</param>
    /// <param name="parameters">The parameters.</param>
    /// <param name="timeOut">The time out in seconds.</param>
    /// <returns></returns>
    public static DataSet GetDataSet(string query, CommandType commandType, Dictionary<string, object> parameters, int? timeOut = null)
        {
            string connectionString = GetConnectionString();
            if (!string.IsNullOrWhiteSpace(connectionString))
            {
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                    }

                    using (SqlCommand sqlCommand = new SqlCommand(query, con))
                    {
                        if (timeOut.HasValue)
                        {
                            sqlCommand.CommandTimeout = timeOut.Value;
                        }
                        sqlCommand.CommandType = commandType;

                        if (parameters != null)
                        {
                            foreach (var parameter in parameters)
                            {
                                SqlParameter sqlParam = new SqlParameter();
                                sqlParam.ParameterName = parameter.Key.StartsWith("@") ? parameter.Key : "@" + parameter.Key;
                                sqlParam.Value = parameter.Value;
                                sqlCommand.Parameters.Add(sqlParam);
                            }
                        }

                        SqlDataAdapter adapter = new SqlDataAdapter(sqlCommand);
                        DataSet dataSet = new DataSet("webUserDs");
                        adapter.Fill(dataSet);
                        return dataSet;
                    }
                }
            }

            return null;
        }
        #endregion
    }
}
