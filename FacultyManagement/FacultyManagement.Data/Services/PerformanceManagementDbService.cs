﻿using FacultyManagement.Data.Interfaces;
using FacultyManagement.Data.Models;
using FacultyManagement.Domain.ExtensionMethods;
using FacultyManagement.Domain.Model.PerformanceManagement;
using System.Collections.Generic;
using System.Data;

namespace FacultyManagement.Data.Services
{
    public class PerformanceManagementDbService : IPerformanceManagementDbService
    {
        /// <summary>
        /// Gets the Self Evaluations. Performance Managements
        /// </summary>
        /// <param name="cVid">The c vid.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public DataSet GetSelfEvaluations(int cVid)
        {
            var parameters = new Dictionary<string, object>
            {
                {"@CVID", cVid}
            };

            return DbService.GetDataSet("sp_PerformanceManagementRead", CommandType.StoredProcedure, parameters, 300);
        }

        /// <summary>
        /// Gets the Peer Evaluations - Performance Managements
        /// </summary>
        /// <param name="cVid">The c vid.</param>
        /// <returns></returns>
        public DataSet GetPeerEvaluations(int cVid)
        {
            var parameters = new Dictionary<string, object>
            {
                {"@CVID", cVid}
            };

            return DbService.GetDataSet("sp_PeerPerformanceManagementRead", CommandType.StoredProcedure, parameters, 300);
        }

        public DataSet GetSupervisorEvaluations(int cVid)
        {
            var parameters = new Dictionary<string, object>
            {
                {"@CVID", cVid}
            };

            return DbService.GetDataSet("sp_SupervisorPerformanceManagementRead", CommandType.StoredProcedure, parameters, 300);
        }

        public DataSet GetMyCommitments(int cVid)
        {
            var parameters = new Dictionary<string, object>
            {
                {"@CVID", cVid}
            };

            return DbService.GetDataSet("sp_CommitmentRead", CommandType.StoredProcedure, parameters, 300);
        }

        #region IDbService Methods

        public bool SaveChanges(ISaveModel model, string username)
        {
            if (model is SaveEvaluation)
            {
                var dbmodel = (SaveEvaluation)model;
                // Peer Evaluation
                if (dbmodel.PeerCVid.HasValue)
                {
                    return SavePeerEvaluationData(dbmodel, username);
                }
                // Supervisor Evaluation
                if (dbmodel.SupervisorCVid.HasValue)
                {
                    return SaveSupervisorEvaluationData(dbmodel, username);
                }

                // Self Evaluation
                return SaveSelfEvaluationData(dbmodel, username);
            }

            // My Commitments
            if (model is SaveCommitment)
            {
                var dbmodel = (SaveCommitment)model;

                return SaveCommitmentData(dbmodel, username);
            }

            return false;
        }
        
        /// <summary>
        /// Deletes the specified database entity by its identifier.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id">The identifier.</param>
        /// <param name="username">The username.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public bool Delete<T>(int id, string username) //TODO: add constraint to T
        {
            var query = string.Empty;

            if (typeof(T) == typeof(PeerEvaluation))
            {
                query = $"UPDATE PeerPerformanceManagement SET isActive = 0, ModifiedBy=@username WHERE PeerPerformanceManagementID={id}";
            }

            if (typeof(T) == typeof(SelfEvaluation))
            {
                query = $"UPDATE PerformanceManagement SET isActive = 0, ModifiedBy=@username WHERE PerformanceManagementID={id}";
            }

            if (typeof(T) == typeof(SupervisorEvaluation))
            {
                query = $"UPDATE SupervisorPerformanceManagement SET isActive = 0, ModifiedBy=@username WHERE SupervisorPerformanceManagementID={id}";
            }

            if (typeof(T) == typeof(Commitment))
            {
                query = $"UPDATE Commitment SET isActive = 0, ModifiedBy=@username WHERE CommitmentID={id}";
            }


            if (!string.IsNullOrEmpty(query))
            {
                var parameters = new Dictionary<string, object>()
                {
                    {"@username", username}
                };

                var rowsDeleted = DbService.ExecuteCommand(query, CommandType.Text, parameters);

                return rowsDeleted != 0;
            }

            return false;
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Saves the peer evaluation data.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="username">The username.</param>
        /// <returns></returns>
        private bool SavePeerEvaluationData(SaveEvaluation model, string username)
        {
            // Common parameters
            var parameters = new Dictionary<string, object>
            {
               {"@Description", model.Description},
               {"@UserName", username}
            };

            int rowsUpdated = 0;
            // Update or Create New
            if (model.Id.HasValue)
            {
                parameters.Add("@PeerPerformanceManagementId", model.Id.AsIntergerOrNull());

                rowsUpdated = DbService.ExecuteCommand("sp_PeerPerformanceManagementUpdate", CommandType.StoredProcedure, parameters, 300);
            }
            else
            {
                parameters.Add("@Year", model.Year);
                parameters.Add("@ReviewerCVID", model.CVid);
                parameters.Add("@PeerCVID", model.PeerCVid);
                parameters.Add("@ValueCommitment", model.ValueCommitment);
                parameters.Add("@Category", model.Category);

                rowsUpdated = DbService.ExecuteCommand("[sp_PeerPerformanceManagementCreate]", CommandType.StoredProcedure, parameters, 300);
            }

            return rowsUpdated != 0;
        }

        /// <summary>
        /// Saves the supervisor evaluation data.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="username">The username.</param>
        /// <returns></returns>
        private bool SaveSupervisorEvaluationData(SaveEvaluation model, string username)
        {
            // Common parameters
            var parameters = new Dictionary<string, object>
            {
               {"@Description", model.Description},
               {"@UserName", username}
            };

            int rowsUpdated = 0;
            // Update or Create New
            if (model.Id.HasValue)
            {
                parameters.Add("@SupervisorPerformanceManagementId", model.Id.AsIntergerOrNull());

                rowsUpdated = DbService.ExecuteCommand("sp_SupervisorPerformanceManagementUpdate", CommandType.StoredProcedure, parameters, 300);
            }
            else
            {
                parameters.Add("@Year", model.Year);
                parameters.Add("@ReviewerCVID", model.CVid);
                parameters.Add("@SupervisorCVID", model.SupervisorCVid);
                parameters.Add("@ValueCommitment", model.ValueCommitment);
                parameters.Add("@Category", model.Category);

                rowsUpdated = DbService.ExecuteCommand("[sp_SupervisorPerformanceManagementCreate]", CommandType.StoredProcedure, parameters, 300);
            }

            return rowsUpdated != 0;
        }

        /// <summary>
        /// Saves the peer evaluation data.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="username">The username.</param>
        /// <returns></returns>
        private bool SaveSelfEvaluationData(SaveEvaluation model, string username)
        {
            // Common parameters
            var parameters = new Dictionary<string, object>
            {
               {"@Description", model.Description},
               {"@UserName", username}
            };

            int rowsUpdated = 0;
            // Update or Create New
            if (model.Id.HasValue)
            {
                parameters.Add("@PerformanceManagementId", model.Id.AsIntergerOrNull());

                rowsUpdated = DbService.ExecuteCommand("sp_PerformanceManagementUpdate", CommandType.StoredProcedure, parameters, 300);
            }
            else
            {
                parameters.Add("@Year", model.Year);
                parameters.Add("@CVID", model.CVid);
                parameters.Add("@ValueCommitment", model.ValueCommitment);
                parameters.Add("@Category", model.Category);

                rowsUpdated = DbService.ExecuteCommand("sp_PerformanceManagementCreate", CommandType.StoredProcedure, parameters, 300);
            }

            return rowsUpdated != 0;
        }

        /// <summary>
        /// Saves the peer evaluation data.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="username">The username.</param>
        /// <returns></returns>
        private bool SaveCommitmentData(SaveCommitment model, string username)
        {
            // Common parameters
            var parameters = new Dictionary<string, object>
            {
               {"@Year", model.Year},
               {"@Commitment", model.CommitmentValue},
               {"@CommitmentDesc", model.Description},
               {"@UserName", username}
            };

            int rowsUpdated = 0;
            // Update or Create New
            if (model.Id.HasValue)
            {
                parameters.Add("@CommitmentId", model.Id.AsIntergerOrNull());

                rowsUpdated = DbService.ExecuteCommand("sp_CommitmentUpdate", CommandType.StoredProcedure, parameters, 300);
            }
            else
            {
                parameters.Add("@PerformanceCategoryID", model.PerformanceCategoryId);
                parameters.Add("@CVID", model.CVid);

                rowsUpdated = DbService.ExecuteCommand("sp_CommitmentCreate", CommandType.StoredProcedure, parameters, 300);
            }

            return rowsUpdated != 0;
        }

        #endregion
    }
}
