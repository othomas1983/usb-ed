﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using FacultyManagement.Data.Interfaces;
using FacultyManagement.Domain.Model.Enums;
using FacultyManagement.Infrastructure.Utilities.ExtensionMethods;

namespace FacultyManagement.Data.Services
{
    public class DropDownsDbService : IDropDownsDbService
    {
        #region public methods

        public DataSet GetData( DropDownType type)
        {
            switch ( type )
            {
                case DropDownType.ABSRating:
                    return this.GetDataSet( "sp_ABSRatingRead" );
                case DropDownType.AcademicRanks:
                    return this.GetDataSet( "sp_AcademicRankRead" );
                case DropDownType.Affiliations:
                    return this.GetDataSet( "sp_AffiliationRead" );
                case DropDownType.ApplicationUserRoles:
                    return this.GetDataSet( "sp_ApplicationUserRoleRead" );
                case DropDownType.Degrees:
                    return this.GetDataSet( "sp_DegreeRead" );
                case DropDownType.Departments:
                    return this.GetDataSet( "sp_DepartmentRead" );
                case DropDownType.DepartmentCategories:
                    return this.GetDataSet( "sp_DepartmentCategoryRead" );
                case DropDownType.DHETStatus:
                    return this.GetDataSet( "sp_DHETStatusRead" );
                case DropDownType.FacultyClassifications:
                    return this.GetDataSet( "sp_FacultyClassificationRead" );
                case DropDownType.FacultySufficiencies:
                    return this.GetDataSet( "sp_FacultySufficiencyRead" );
                case DropDownType.HighestQualifications:
                    return this.GetDataSet( "sp_HighestQualificationRead" );
                case DropDownType.Languages:
                    /* This is a work around due to bad db design. This SP requires a parameter */
                    return this.GetDataSet( "sp_LanguageRead", 0 );
                case DropDownType.Locus:                    
                    return this.GetDataSet( "sp_LocusRead");
                case DropDownType.MainActivities:
                    return this.GetDataSet( "sp_MainActivityRead" );
                case DropDownType.Nationalities:
                    return this.GetDataSet( "sp_NationalityRead" );
                case DropDownType.PerformanceCategory:
                    return this.GetDataSet( "sp_PerformanceCategoryRead" );
                case DropDownType.PersonRole:
                    return this.GetDataSet( "sp_PersonRoleRead" );
                case DropDownType.Programmes:
                    return this.GetDataSet( "sp_Programme" );
                case DropDownType.ProgrammeOfferings:
                    return this.GetDataSet( "sp_ProgrammeOffering" );
                case DropDownType.ResearchContributionType:
                    return this.GetDataSet( "sp_ResearchContributionTypeRead" );
                case DropDownType.ResearchOutputCategory:
                    return this.GetDataSet( "sp_ResearchOutputCategoryRead" );
                case DropDownType.ResearchStatus:
                   return  DbService.GetDataSet("SELECT ResearchStatusID, ResearchStatusDescription FROM ResearchStatus WHERE isActive = 1", CommandType.Text, null, 300 );
                case DropDownType.TaskTeamActivity:
                    return this.GetDataSet( "sp_TaskTeamActivityRead" );
                case DropDownType.BusinessUnit:
                    return DbService.GetDataSet("SELECT BusinessUnitID, BusinessUnitName FROM BusinessUnit", CommandType.Text, null, 300);
                case DropDownType.PMProgrammes:
                    return DbService.GetDataSet("SELECT Name, ProgrammeID FROM pma.Programme", CommandType.Text,null, 300);
                case DropDownType.PMProgrammeOfferings:
                    return this.GetDataSet("sp_PMProgrammeOffering");
                case DropDownType.PMProgrammeOfferingModules:
                    return this.GetDataSet("sp_PMProgrammeOfferingModule");
                case DropDownType.PMCategory:
                    return DbService.GetDataSet("SELECT * FROM [FacultyManagement].[pma].[Category]", CommandType.Text, null, 300);
                case DropDownType.PMSupervisionResearch:
                    return DbService.GetDataSet("SELECT * FROM [FacultyManagement].[pma].[ResearchSupervision]", CommandType.Text, null, 300);
                case DropDownType.ProgrammeSize:
                    return DbService.GetDataSet("SELECT * FROM [pma].[ProgrammeSize]", CommandType.Text, null, 300);

                default:
                    throw new InvalidEnumArgumentException( type.ConvertToString() );
            }
        }
      
        #endregion

         #region private methods
            private DataSet GetDataSet( string storedProcedure, int? paramater = null )
        {
            if ( paramater == null )
            {
                return DbService.GetDataSet( storedProcedure, System.Data.CommandType.StoredProcedure, null, 300 );
            }

            /* This is a work around due to bad db design. This SP requires a parameter */
            var parameters = new Dictionary<string, object>
            {
                {"@CVID", paramater}
            };
            return DbService.GetDataSet( storedProcedure, System.Data.CommandType.StoredProcedure, parameters, 300 );
        }
        #endregion
    }
}
