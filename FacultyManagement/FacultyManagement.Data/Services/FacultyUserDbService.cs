﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Data.Interfaces;

namespace FacultyManagement.Data.Services
{
    public class FacultyUserDbService : IFacultyUserDbService
    {
        /// <summary>
        /// Gets all faculty users.
        /// </summary>
        /// <returns></returns>
        public DataSet GetAllFacultyUsers()
        {
            string query = SqlQueries.GetAllFacultyUsers;
            return DbService.GetDataSet( query, CommandType.Text, null, 300 );
        }
    }
}
