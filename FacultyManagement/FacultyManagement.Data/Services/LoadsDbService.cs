﻿using FacultyManagement.Data.Interfaces;
using FacultyManagement.Data.Models;
using FacultyManagement.Domain.ExtensionMethods;
using FacultyManagement.Domain.Model.Admin;
using FacultyManagement.Domain.Model.Loads;
using System;
using System.Collections.Generic;
using System.Data;

namespace FacultyManagement.Data.Services
{
    public class LoadsDbService : ILoadsDbService
    {
        #region Store Procedure
        public DataSet GetWebUsers(int cVid)
        {
            return DbService.GetDataSet("Select cvid, surname, [name], webdisplay from cv", CommandType.Text, null, 300);
        }
        /// <summary>
        /// Gets the teaching loads.
        /// </summary>
        /// <param name="cVid">The c vid.</param>
        /// <returns></returns>
        public DataSet GetTeachingLoads(int cVid)
        {
            var parameters = new Dictionary<string, object>
            {
                {"@CVID", cVid}
            };

            return DbService.GetDataSet("sp_TeachingLoadRead", CommandType.StoredProcedure, parameters, 300);
        }

        /// <summary>
        /// Gets the management loads.
        /// </summary>
        /// <param name="cVid">The c vid.</param>
        /// <returns></returns>
        public DataSet GetManagementLoads(int cVid)
        {
            var parameters = new Dictionary<string, object>
            {
                {"@CVID", cVid}
            };

            return DbService.GetDataSet("sp_ManagementLoadRead", CommandType.StoredProcedure, parameters, 300);
        }

        /// <summary>
        /// Gets the supervision loads.
        /// </summary>
        /// <param name="cVid">The c vid.</param>
        /// <returns></returns>
        public DataSet GetSupervisionLoads(int cVid)
        {
            var parameters = new Dictionary<string, object>
            {
                {"@CVID", cVid}
            };

            return DbService.GetDataSet("sp_FMSupervisionRead", CommandType.StoredProcedure, parameters, 300);
        }
        #endregion

        #region IDbService Methods

        /// <summary>
        /// Saves the changes made to the model to the database.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="username">The username.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public bool SaveChanges(ISaveModel model, string username)
        {
            if (model is SaveTeachingLoad)
            {
                return SaveData((SaveTeachingLoad)model, username);
            }

            if (model is SaveManagementLoad)
            {
                return SaveData((SaveManagementLoad)model, username);
            }

            if (model is SaveSupervisionLoad)
            {
                return SaveData((SaveSupervisionLoad)model, username);
            }

            return false;
        }

        /// <summary>
        /// Deletes the specified database entity by its identifier.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id">The identifier.</param>
        /// <param name="username">The username.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public bool Delete<T>(int id, string username) //TODO: add constraint to generic T
        {
            string query = string.Empty;

            if (typeof(T) == typeof(ManagementLoad))
            {
                query = $"UPDATE  ManagementLoad SET isActive = 0, ModifiedBy = @username WHERE ManagementLoadID={id}";
            }

            if (typeof(T) == typeof(SupervisionLoad))
            {
                query = $"UPDATE  [dbo].[pma.Supervision] SET isActive = 0, ModifiedBy = @username WHERE SupervisionID={id}";
            }

            if (typeof(T) == typeof(TeachingLoad))
            {
                query = $"UPDATE  TeachingLoad SET isActive = 0, ModifiedBy = @username WHERE TeachingLoadID={id}";
            }

            if (!string.IsNullOrEmpty(query))
            {
                var parameters = new Dictionary<string, object>()
                {
                    {"@username", username}
                };

                var rowsDeleted = DbService.ExecuteCommand(query, CommandType.Text, parameters);

                return rowsDeleted != 0;
            }
            
            return false;
        }

        #endregion


        #region Save Methods
        /// <summary>
        /// Saves the data for Educations.
        /// </summary>
        /// <param name="model">The model.</param>s
        /// <param name="username">The username.</param>
        /// <returns></returns>
        private bool SaveData(SaveTeachingLoad model, string username)
        {try
            {
                int rowsUpdated = 0;

                if(model.Category == "PhD Teaching" || model.Category == "USB-ED 2 hour sessions (no payment)" || model.Category == "International office / 2 hr contact contact session" 
                    || model.Category == "International study module leader" || model.Category == "Course development" || model.Category == "Leadership one-on-one")
                {
                    model.Programme = "0";
                    model.ProgrammeOffering = "0";
                    model.Module = "0";
                }
              

                var parameters = new Dictionary<string, object>
            {
                {"@Year", model.Year},
                {"@Category", model.Category},
                {"@ProgrammeOffering", model.ProgrammeOffering},
                {"@Programme", model.Programme},
                {"@Module", model.Module},             
                {"@SessionCount", model.Sessions},             
                {"@TotalCredit", model.TotalCredits},
              
            };
                if (model.Id > default(int))
                {
                    parameters.Add("@TeachingLoadID", model.Id);

                    rowsUpdated = DbService.ExecuteCommand("sp_TeachingLoadUpdate", CommandType.StoredProcedure, parameters, 300);
                }
                else
                {
                    parameters.Add("@CVID", model.CVid);
                    parameters.Add("@UserName", username);
                    
                    rowsUpdated = DbService.ExecuteCommand("sp_TeachingLoadCreate", CommandType.StoredProcedure, parameters, 300);
                }

                return rowsUpdated != 0;
            }
            catch(Exception e)
            {
                return false;
            }
        }

        /// <summary>
        /// Saves the data for Supervision Loads
        /// </summary>
        /// <param name="model">The model.</param>s
        /// <param name="username">The username.</param>
        /// <returns></returns>
        private bool SaveData(SaveSupervisionLoad model, string username)
        {
            int rowsUpdated = 0;
            var parameters = new Dictionary<string, object>
            {
                {"@SupervisorCVID", model.CVid },              
                {"@Year", model.Year},
                {"@Programme", "0"},
                {"@ProgrammeOffering", "0"},
                {"@StudentName", model.StudentName},                          
                {"@Hours", model.Hours},   
                {"@UserName", username},
                {"@SupervisionResearch", model.SupervisionResearch}

            };

            if (model.Id > default(int))
            {
                parameters.Add("@SupervisionLoadID", model.Id);
                rowsUpdated = DbService.ExecuteCommand("sp_PMSupervisionUpdate", CommandType.StoredProcedure, parameters, 300);
            }
            else
            {
                 rowsUpdated = DbService.ExecuteCommand("sp_PMSupervisionCreate", CommandType.StoredProcedure, parameters, 300);
            }
           

            return rowsUpdated != 0;
        }

        /// <summary>
        /// Saves the data for management loads.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="username">The username.</param>
        /// <returns></returns>
        private bool SaveData(SaveManagementLoad model, string username)
        {
            DataTable dataTable = MapToDataTable(model);

            var parameters = new Dictionary<string, object>
            {
                {"@tvpManagementLoad", dataTable},
                {"@UserName", username}
            };

            int rowsUpdated = DbService.ExecuteCommand("sp_ManagementLoadCUD", CommandType.StoredProcedure, parameters, 300);

            return rowsUpdated != 0;
        }

        private DataTable MapToDataTable(SaveManagementLoad model)
        {
            var dataTable = new DataTable();

            dataTable.Columns.Add("ManagementLoadCategoryID");
            dataTable.Columns.Add("CVID");
            dataTable.Columns.Add("isActive");
            dataTable.Columns.Add("ManagementLoadID");
            dataTable.Columns.Add("Credit");
            dataTable.Columns.Add("ModuleCount");
            dataTable.Columns.Add("StudentCount");
            dataTable.Columns.Add("Description");
            dataTable.Columns.Add("ProgrammeID");
            dataTable.Columns.Add("DegreeID");
            dataTable.Columns.Add("ProgrammeOfferingID");
            dataTable.Columns.Add("AccreditationReportID");
            dataTable.Columns.Add("ElectiveModuleCount");
            dataTable.Columns.Add("ElectiveSAQACredit");
            dataTable.Columns.Add("ManagementLoadYear");
            dataTable.Columns.Add("ManagementLoadStartDate", typeof(DateTime)).DateTimeMode = DataSetDateTime.Local;
            dataTable.Columns.Add("ManagementLoadEndDate", typeof(DateTime)).DateTimeMode = DataSetDateTime.Local;
            dataTable.Columns.Add("TaskTeamActivityID");
            dataTable.Columns.Add("ProgrammeSize");

            var newRow = dataTable.NewRow();
            dataTable.Rows.Add(newRow);

            newRow["ManagementLoadCategoryID"] = model.ManagementLoadCategoryId;
            newRow["CVID"] = model.CVid;
            newRow["isActive"] = model.IsActive;
            newRow["ManagementLoadID"] = model.Id.AsIntergerOrNull();
            newRow["Credit"] = model.Credit;
            newRow["ModuleCount"] = model.ModuleCount;
            newRow["StudentCount"] = model.StudentCount;
            newRow["Description"] = model.Description;
            newRow["ProgrammeID"] = DBNull.Value;
            newRow["DegreeID"] = model.DegreeId;
            newRow["ProgrammeOfferingID"] = model.ProgrammeOfferingId;
            newRow["AccreditationReportID"] = DBNull.Value;
            newRow["ElectiveModuleCount"] = model.ElectiveModuleCount;
            newRow["ElectiveSAQACredit"] = model.ElectiveSAQACredit;
            newRow["ManagementLoadYear"] = model.Year;
            newRow["ManagementLoadStartDate"] = model.StartDate;
            newRow["ManagementLoadEndDate"] = model.EndDate;
            newRow["TaskTeamActivityID"] = model.TaskTeamActivityId;
            newRow["ProgrammeSize"] = model.ProgrammeSize;


            return dataTable;
        }

        #endregion
    }
}
