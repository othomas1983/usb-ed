﻿using System.Collections.Generic;
using System.Data;
using FacultyManagement.Data.Interfaces;
using FacultyManagement.Data.Models;
using FacultyManagement.Domain.Model;
using FacultyManagement.Domain.Model.PersonalInformation;
using NLog;

namespace FacultyManagement.Data.Services
{
    public class PersonalInformationDbService : IPersonalInformationDbService
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #region Stored Procedure Queries

        /// <summary>
        /// Gets the personal information.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <returns></returns>
        public DataSet GetPersonalInformation( string username )
        {
            if ( !string.IsNullOrWhiteSpace( username ) )
            {
                var parameters = new Dictionary<string, object>
                {
                    {"Samaccountname", username}
                };

                return DbService.GetDataSet( "sp_PersonalInformationRead", System.Data.CommandType.StoredProcedure, parameters, 300 );
            }

            return null;
        }

        /// <summary>
        /// Gets the user image.
        /// </summary>
        /// <param name="employeeId">The employee identifier.</param>
        /// <returns></returns>
        public DataSet GetUserImage( string employeeId )
        {
            if ( !string.IsNullOrWhiteSpace( employeeId ) )
            {
                var parameters = new Dictionary<string, object>
                {
                    {"@USNumber", employeeId}
                };

                return DbService.GetDataSet( "sp_FacultyImageRead", System.Data.CommandType.StoredProcedure, parameters, 300 );
            }

            return null;
        }

        #endregion


        #region IDbService Methods

        /// <summary>
        /// Saves the changes made to the model to the database.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="username">The username.</param>
        /// <returns></returns>
        public bool SaveChanges( ISaveModel model, string username )
        {
            var saveModel = model as SavePersonalInformation;

            bool dataSaved = SavePersonData( saveModel, username );

            bool imageSaved = true;
            if ( saveModel.UserPhoto != null )
            {
                // Save the image
                imageSaved = SavePersonPhoto( saveModel.UserPhoto );
            }

            return dataSaved && imageSaved;
        }

        public bool Delete<T>( int id, string username )
        {
            throw new System.NotImplementedException();
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// Saves the person photo.
        /// </summary>
        /// <param name="userPhoto">The user photo.</param>
        /// <returns></returns>
        private bool SavePersonPhoto( UserPhoto userPhoto )
        {
            string imageName = userPhoto.UsNumber;
            string employeeId = imageName.Split( '.' )[0];

            // Delete Existing
            var parameters = new Dictionary<string, object>
            {
                {"@ImageName", employeeId}
            };
            DbService.ExecuteCommand( "sp_FacultyImageDelete", CommandType.StoredProcedure, parameters, 300 );

            // Create New
            parameters = new Dictionary<string, object>
            {
                {"@ImageName", imageName},
                {"@ImageFile", userPhoto.FacultyImage}
            };
            int rowsInserted = DbService.ExecuteCommand( "sp_FacultyImageCreate", CommandType.StoredProcedure, parameters, 300 );

            return rowsInserted != 0;
        }

        /// <summary>
        /// Saves the person data.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="username">The username.</param>
        /// <returns></returns>
        private bool SavePersonData(SavePersonalInformation model, string username)
        {
            string CvId = "";

            CvId = model.Cvid.ToString();

            Logger.Info("SavePersonalInformation Model:");

            

            DataTable dtBio= DbService.GetDataTable($@"Select Biography FROM CV WHERE CVID = " + CvId, CommandType.Text,null);

            DataTable dataTable = MapToDataTable(model,dtBio);

            var parameters = new Dictionary<string, object>
            {
                {"@tvpPersonalInfo", dataTable},
                {"@UserName", username}
            };

            int rowsUpdated = DbService.ExecuteCommand("sp_PersonalInformationCreate", CommandType.StoredProcedure, parameters, 300);
            string googleSIndex = "";
          

            // Update Google Scholar Index
            if (model.GoogleScholarIndex != null)
            {
                googleSIndex = model.GoogleScholarIndex.ToString();

            }
            else
            {
                googleSIndex = "0";
            }
            string query = $@"UPDATE CV SET GoogleScholarHIndex = " + googleSIndex + " WHERE CVID = " + CvId;
            DbService.ExecuteCommand( query );


            return rowsUpdated != 0;
        }


        /// <summary>
        /// Maps to data table.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        private DataTable MapToDataTable( SavePersonalInformation model, DataTable bio )
        {
            var dataTable = new DataTable();

            #region Create Columns
            dataTable.Columns.Add( "CVID" );
            dataTable.Columns.Add( "Biography" );
            dataTable.Columns.Add( "Title" );
            dataTable.Columns.Add( "Initials" );
            dataTable.Columns.Add( "Name" );
            //dataTable.Columns.Add("GivenName");
            dataTable.Columns.Add( "Surname" );
            dataTable.Columns.Add( "Gender" );
            dataTable.Columns.Add( "NationalityID" );
            dataTable.Columns.Add( "BusinessContactNo" );
            dataTable.Columns.Add( "EmailAddress" );
            dataTable.Columns.Add( "Position" );
            dataTable.Columns.Add( "AcademicRank" );
            dataTable.Columns.Add( "HighestQualificationID" );
            dataTable.Columns.Add( "BusinessUnit" );
            dataTable.Columns.Add( "Expertise" );
            dataTable.Columns.Add( "EmployeeId" );
            dataTable.Columns.Add( "HomeInstitution" );
            dataTable.Columns.Add( "OrganisationalResponsibility" );
            dataTable.Columns.Add( "AffiliationID" );
            dataTable.Columns.Add( "ResearchInterest" );
            dataTable.Columns.Add( "MainActivityID" );
            dataTable.Columns.Add( "Samaccountname" );
            dataTable.Columns.Add( "isFaculty" );
            dataTable.Columns.Add("WebDisplay");
            #endregion

            var newRow = dataTable.NewRow();
            dataTable.Rows.Add( newRow );

            SaveExecutiveProfile modelS = new SaveExecutiveProfile();

            #region Add New DataRow
            newRow["CVID"] = model.Cvid;
            newRow["Biography"] = bio.Rows[0].ItemArray[0].ToString();
            newRow["Title"] = model.Title;
            newRow["Initials"] = model.Initials;
            newRow["Name"] = model.Name;
            //newRow["GivenName"] = model.GivenName;
            newRow["Surname"] = model.Surname;
            newRow["Gender"] = model.Gender;
            newRow["NationalityID"] = model.NationalityId;
            newRow["BusinessContactNo"] = model.BusinessContactNo;
            newRow["EmailAddress"] = model.EmailAddress;
            newRow["Position"] = model.Position;
            newRow["AcademicRank"] = model.AcademicRank;
            newRow["HighestQualificationID"] = model.HighestQualificationId;
            newRow["BusinessUnit"] = model.BusinessUnit;
            newRow["Expertise"] = model.Expertise;
            newRow["EmployeeID"] = model.EmployeeId;
            newRow["HomeInstitution"] = model.HomeInstitution;
            newRow["OrganisationalResponsibility"] = model.OrganisationalResponsibility;
            newRow["AffiliationID"] = model.AffiliationId == 0 ? 1 : model.AffiliationId;
            newRow["ResearchInterest"] = model.ResearchInterest;
            newRow["MainActivityID"] = model.MainActivityId == 0 ? 1 : model.MainActivityId;
            newRow["Samaccountname"] = model.SamAccountName;
            newRow["isFaculty"] = model.IsFaculty;
            newRow["WebDisplay"] = null;
            #endregion

            return dataTable;
        }

        #endregion
    }
}
