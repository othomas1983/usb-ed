﻿using System.Collections.Generic;
using System.Data;

namespace FacultyManagement.Data.Interfaces
{
    public interface IWebUserDbService
    {
        /// <summary>
        /// Gets the WebUsers.
        /// </summary>
        /// <param name="cVid">The cvid.</param>
        /// <returns></returns>
        DataSet GetWebUsers(int cVid);


        DataSet GetWebUsers(int cVid, bool displayOnWeb);

        void UpdateUserWebDisplayState(IEnumerable<int> cvIds, bool displayOnWeb);

    }
}
