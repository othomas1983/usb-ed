﻿using System.Data;

namespace FacultyManagement.Data.Interfaces
{
    public interface IProfessionalAndPersonalDevelopmentDbService : IDbService
    {
        /// <summary>
        /// Gets the developments.
        /// </summary>
        /// <param name="cVid">The cvid.</param>
        /// <returns></returns>
        DataSet GetDevelopments( int cVid );
    }
}