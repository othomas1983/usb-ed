﻿using FacultyManagement.Data.Models;
using FacultyManagement.Domain.Common;

namespace FacultyManagement.Data.Interfaces
{
    public interface IDbService
    {
        /// <summary>
        /// Saves the changes made to the model to the database.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="username">The username.</param>
        /// <returns></returns>
        bool SaveChanges( ISaveModel model, string username );

        /// <summary>
        /// Deletes the specified database entity by its identifier.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id">The identifier.</param>
        /// <param name="username">The username.</param>
        /// <returns></returns>
        bool Delete<T>( int id, string username );
    }
}