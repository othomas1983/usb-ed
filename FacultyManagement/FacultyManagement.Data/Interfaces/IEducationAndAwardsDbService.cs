﻿using System.Data;

namespace FacultyManagement.Data.Interfaces
{
    public interface IEducationAndAwardsDbService : IDbService
    {
        /// <summary>
        /// Gets the education data.
        /// </summary>
        /// <param name="cVid">The users cvid.</param>
        /// <returns></returns>
        DataSet GetEducations( int cVid );
        DataSet GetAwards( int cVid );             
        DataSet GetGrants( int cVid );
        DataSet GetLanguages( int cVid );

    }
}
