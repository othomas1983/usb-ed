using System.Data;
using FacultyManagement.Domain.Model.Enums;

namespace FacultyManagement.Data.Interfaces
{
    public interface IDropDownsDbService
    {
        /// <summary>
        /// Factory method to get the appropriate data.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        DataSet GetData( DropDownType type );
     
    }
}