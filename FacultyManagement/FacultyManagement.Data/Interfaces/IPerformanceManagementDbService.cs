﻿using System.Data;

namespace FacultyManagement.Data.Interfaces
{
    public interface IPerformanceManagementDbService : IDbService
    {
        /// <summary>
        /// Gets the Self Evaluations - Performance Managements
        /// </summary>
        /// <param name="cVid">The c vid.</param>
        /// <returns></returns>
        DataSet GetSelfEvaluations( int cVid );

        /// <summary>
        /// Gets the Peer Evaluations - Performance Managements
        /// </summary>
        /// <param name="cVid">The c vid.</param>
        /// <returns></returns>
        DataSet GetPeerEvaluations( int cVid );

        /// <summary>
        /// Gets the Supervisor Evaluations - Performance Managements
        /// </summary>
        /// <param name="cVid">The c vid.</param>
        /// <returns></returns>
        DataSet GetSupervisorEvaluations( int cVid );

        /// <summary>
        /// Gets my commitments.  - Performance Managements
        /// </summary>
        /// <param name="cVid">The c vid.</param>
        /// <returns></returns>
        DataSet GetMyCommitments( int cVid );
    }
}
