﻿using System.Data;

namespace FacultyManagement.Data.Interfaces
{
    public interface IAuthDbService
    {
        /// <summary>
        /// Checks if the users exists in the database. Returns the CVid of the user
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="cVid">The c vid.</param>
        /// <returns></returns>
        bool CheckUserExists( string username, out int cVid );

        /// <summary>
        /// Gets the auths.
        /// </summary>
        /// <returns></returns>
        DataSet GetAuths();
    }
}
