﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacultyManagement.Data.Models
{
    public class SaveTeachingLoad : ISaveModel
    {
        #region Properties

        public int Id { get; set; }
        public int Year { get; set; }
        public string ProgrammeOffering { get; set; }
        public string Programme { get; set; }
        public string ProgrammeOfferingDescription { get; set; }
        public string ProgrammeDescription { get; set; }
        public string ModuleDescription { get; set; }
        public int YearDescription { get; set; }

        public string Module { get; set; }
        public decimal Sessions { get; set; }
       // public int Students { get; set; }
       // public int BaseCredit { get; set; }
        public decimal TotalCredits { get; set; }

        public int  CVid { get; set; }
        public string Category { get; set; }
        public string CategoryDescription { get; set; }
        #endregion
    }
}
