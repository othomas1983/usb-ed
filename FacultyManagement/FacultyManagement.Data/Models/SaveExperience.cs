﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacultyManagement.Data.Models
{
    public class SaveExperience : ISaveModel
    {
        #region Properties

        public int? Id { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int ExperienceCategoryId { get; set; }
        public string Subject { get; set; }
        public string Position { get; set; }
        public string Institution { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public int IsCurrent { get; set; }
        public int CVid { get; set; }
        public int? NoOfDays { get; set; }
        public string Programme { get; set; }
        public string KnowledgeArea { get; set; }
        public string Client { get; set; }
        public string Publication { get; set; }
        public string PublicationStatus { get; set; }
        public int? PublicationEdition { get; set; }

        public int IsActive { get; set; }
        #endregion
    }
}
