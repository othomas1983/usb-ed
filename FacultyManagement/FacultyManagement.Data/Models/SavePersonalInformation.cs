﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Domain.Model;
using FacultyManagement.Domain.Model.PersonalInformation;
using FacultyManagement.Infrastructure.Utilities.ExtensionMethods;

namespace FacultyManagement.Data.Models
{
    public class SavePersonalInformation : ISaveModel
    {
        #region Properties 

        public string Biography { get; set; }
        public string Title { get; set; }
        public string Initials { get; set; }
        public string Name { get; set; }
       // public string GivenName { get; set; }
        public string Surname { get; set; }
        public string Gender { get; set; }
        public int? NationalityId { get; set; }
        public string BusinessContactNo { get; set; }
        public string EmailAddress { get; set; }
        public string Position { get; set; }
        public string AcademicRank { get; set; }
        public int? HighestQualificationId { get; set; }
        public int? MainActivityId { get; set; }
        public string BusinessUnit { get; set; }
        public string Expertise { get; set; }
        public string ResearchInterest { get; set; }
        public string HomeInstitution { get; set; }
        public string OrganisationalResponsibility { get; set; }
        public int? Cvid { get; set; }
        public int? AffiliationId { get; set; }
        public string SamAccountName { get; set; }
        public int? ApplicationUserRoleId { get; set; }
        public string EmployeeId { get; set; }
        public int? IsFaculty { get; set; }
        public int? GoogleScholarIndex { get; set; }
        public string ImageSource { get; set; }
        public UserPhoto UserPhoto { get; set; }

        public bool? WebDisplay { get; set; } = null;

        #endregion

        public override string ToString()
        {
            return $@"Biography:{Biography},Title:{Title}, Name:{Name}, Surname:{Surname},Gender:{Gender},Nationality:{NationalityId}
                        Business Contact{BusinessContactNo}, Email:{EmailAddress}, Academic Rand:{AcademicRank}, CVid:{Cvid}, AffiliationId: {AffiliationId}
                        EmployeeId:{EmployeeId}, SamAccountName:{SamAccountName}";
        }
    }
}
