﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Domain.Model.Common;
using FacultyManagement.Domain.Model.Enums;

namespace FacultyManagement.Data.Models
{
    public class SaveAward : ISaveModel
    {
        #region Properties

        public int? Id { get; set; }
        public int Year { get; set; }
        public string AwardType { get; set; }
        public string Description { get; set; }
        public string Country { get; set; }     
        public int IsActive { get; set; }
        public int CVid { get; set; }
        #endregion
    }
}
