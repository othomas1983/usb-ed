﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacultyManagement.Data.Models
{
    public class SaveDevelopment : ISaveModel
    {
        #region Properties

        public int? Id { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int DevelopmentCategoryId { get; set; }
        public string Accomplishment { get; set; }
        public string Topic { get; set; }
        public string Institution { get; set; }
        public string Country { get; set; }
        public int? NoOfDays { get; set; }
        public int IsCurrent { get; set; }

        public int CVid { get; set; }
        public int IsActive { get; set; }


        #region Redundant
        public string Position { get; set; }
        public string Organisation { get; set; }
        public string City { get; set; }

        #endregion
        #endregion
    }
}
