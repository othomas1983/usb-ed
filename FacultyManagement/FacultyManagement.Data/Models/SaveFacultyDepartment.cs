﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Application.Admin;
using FacultyManagement.Domain.Model.Admin;
using FacultyManagement.Domain.Model.Common;

namespace FacultyManagement.Data.Models
{
    public class SaveFacultyDepartment : ISaveModel
    {
        #region Properties

        public List<SaveFacultyDepartmentItem> Departments { get; set; } = new List<SaveFacultyDepartmentItem>();

        public SaveFacultyDepartment(  )
        {
            
        }

        #endregion
    }
}
