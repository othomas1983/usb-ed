﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacultyManagement.Data.Models
{
    public class SaveClassifications : ISaveModel
    {
        #region Properties

        public int? ClassificationId { get; set; }
        public int? SufficiencyId { get; set; }
        public int CVid { get; set; }

        #endregion
    }
}
