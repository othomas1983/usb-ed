﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacultyManagement.Data.Models
{
    public class SaveManagementLoad : ISaveModel
    {
        #region Properties

        public int? Id { get; set; }
        public int IsActive { get; set; }
        public int? Year { get;  set; }
        public DateTime StartDate { get;  set; }
        public DateTime EndDate { get;  set; }
        public int ManagementLoadCategoryId { get;  set; }
        public int? ModuleCount { get;  set; }
        public int? StudentCount { get;  set; }
        public int? ProgrammeId { get; set; }
        public int? DegreeId { get; set; }
        public int? ProgrammeOfferingId { get;  set; }
        public int? ElectiveModuleCount { get;  set; }
        public int? ElectiveSAQACredit { get;  set; }
        public int? TaskTeamActivityId { get;  set; }
        public string TaskTeamActivityDescription { get;  set; }
        public string Description { get;  set; }
        public int? Credit { get;  set; }
        public string ProgrammeSize { get; set; }
        public int CVid { get; set; }
        #endregion
    }
}
