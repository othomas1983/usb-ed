﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Infrastructure.Utilities.ExtensionMethods;

namespace FacultyManagement.Application.Admin
{
    public class SaveFacultyDepartmentItem
    {
        public int? FacultyDepartmentId { get; set; }
        public int CVid { get; set; }
        public int DepartmentId { get; set; }
        public int DepartmentCategoryId { get; set; }
        public int IsActive { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="SaveFacultyDepartmentItem"/> class.
        /// </summary>
        /// <param name="cvId">The cv identifier.</param>
        /// <param name="facultyDepartmentId">The faculty department identifier.</param>
        /// <param name="departmentId">The department identifier.</param>
        /// <param name="departmentCategoryId">The department category identifier.</param>
        /// <param name="isActive"></param>
        public SaveFacultyDepartmentItem( int cvId, int? facultyDepartmentId, int departmentId,  int departmentCategoryId, bool isActive = true )
        {
            CVid = cvId;
            FacultyDepartmentId = facultyDepartmentId;
            DepartmentId = departmentId;
            DepartmentCategoryId = departmentCategoryId;
            IsActive = isActive.Bit();
        }
    }
}
