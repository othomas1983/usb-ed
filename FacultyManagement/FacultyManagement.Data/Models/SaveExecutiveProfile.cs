﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace FacultyManagement.Data.Models
{
    public class SaveExecutiveProfile : ISaveModel
    {
        #region Properties

        public string Biography { get; set; }       

        public int CVid { get; set; }

        #endregion
    }
}
