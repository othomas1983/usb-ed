﻿using System;
using System.Collections.Generic;

namespace FacultyManagement.Data.Models
{
    public class SaveResearchActivity : ISaveModel
    {
        #region Properties

        public List<SaveContributor> Contributors { get; set; }
        public List<SaveContributor> ContributorsToRemove { get; set; }

        public int? Id { get; set; }
        public string Description { get; set; }
        public int ResearchContributionTypeId { get; set; }
        public int? ResearchId { get; set; }
        public int ResearchStatusId { get; set; }
        public DateTime ResearchStatusDate { get; set; }
        public int ResearchOutputCategoryId { get; set; }
        public int LocusId { get; set; }
        public string ArticleTitle { get; set; }
        public string PublisherName { get; set; }
        public string PublicationName { get; set; }
        public DateTime? PublicationDate { get; set; }
        public int ABSRatingStatusId { get; set; }
        public int DHETStatusId { get; set; }
        public int? Credit { get; set; }


        public int? NoOfPages { get; set; }
        public int? StartPage { get; set; }
        public int? EndPage { get; set; }
        public string Edition { get; set; }
        public string Issue { get; set; }

        public string Country { get; set; }
        public int? Volume { get; set; }
        public string Note { get; set; }

        public int? CVid { get; set; }
        public string PublicationCity { get; internal set; }

        public string PublicationFile { get; set; }
      
        #endregion
    }
}
