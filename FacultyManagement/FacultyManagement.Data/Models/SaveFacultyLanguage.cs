﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacultyManagement.Data.Models
{
    public class SaveFacultyLanguage : ISaveModel
    {
        #region Properties

        public int? Id { get; set; }
        public List<int> LanguageIds { get; set; }

        public int? LanguageId
        {
            get => LanguageIds?[0];
            set => _languageId = value;
        }
        private int? _languageId;
        public int IsActive { get; set; }
        public int CVid { get; set; }

        #endregion
    }
}
