﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacultyManagement.Data.Models
{
    public class SaveSupervisionLoad : ISaveModel
    {
        #region MyRegion

        public int? Id { get; set; }

        public int CVid { get; set; }        
        public int SecondCVid { get; set; }        
        public int? Year { get; set; }
        public string StudentName { get; set; }
        public string ThesisTopic { get; set; }
        public string Role { get; set; }
        public string ProgrammeOffering { get; set; }
        public string Programme { get; set; }
        public string ProgrammeOfferingDescription { get; set; }
        public string ProgrammeDescription { get; set; }
        public int YearDescription { get; set; }
        public decimal Hours { get; set; }
        public string SupervisionResearch { get; set; }

        #endregion
    }
}
