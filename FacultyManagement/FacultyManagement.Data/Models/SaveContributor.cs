﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacultyManagement.Data.Models
{
    public class SaveContributor
    {
        #region Properties

        public int? Id { get; set; }
        public int? CVid { get; set; }
        public int Sequence { get; set; }
        public decimal DHETCredit { get; set; } = 1;
        public int RoleId { get; set; }

        public string Type { get; set; }

        public int IsActive { get; set; } = 1;


        // External Contributor Properties
        public string Surname { get; set; }
        public string FirstName { get; set; }
        public string Initials { get; set; }
        public int? NationalityId { get; set; }
        public int? AffiliationId { get; set; }
        public string Gender { get; set; }
        public string Email { get; set; }
        public string Institution { get; set; }

        #endregion
    }
}
