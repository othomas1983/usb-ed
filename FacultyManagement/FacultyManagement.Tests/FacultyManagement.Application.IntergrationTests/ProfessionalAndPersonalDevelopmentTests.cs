﻿using FacultyManagement.Application.IntegrationTests.Common;
using FacultyManagement.Application.ProfessionalAndPersonalDevelopment;
using FacultyManagement.Application.ProfessionalAndPersonalDevelopment.ViewModels;
using FacultyManagement.Data.Interfaces;
using FacultyManagement.Data.Services;
using NUnit.Framework;

namespace FacultyManagement.Application.IntegrationTests
{
    [TestFixture]
    public class ProfessionalAndPersonalDevelopmentTests
    {
        public CurrentPersonMock CurrentPerson { get; set; } = new CurrentPersonMock( 1, "rog" );
        public IProfessionalAndPersonalDevelopmentDbService DbService { get; set; } = new ProfessionalAndPersonalDevelopmentDbService();

        [Test]
        public void Should_Get_Developments_Data_And_Return_View_Model()
        {
            // Arrange
            var service = new ProfessionalAndPersonalDevelopmentService( DbService );

            // Act
            var model = service.GetViewModel( CurrentPerson );

            // Assert
            Assert.IsTrue( model is ProfessionalAndPersonalDevelopmentVm );
            Assert.IsTrue( (model as ProfessionalAndPersonalDevelopmentVm).Memberships.Count > 0 );
            Assert.IsTrue( (model as ProfessionalAndPersonalDevelopmentVm).NonAcademic.Count > 0 );
        }
    }
}
