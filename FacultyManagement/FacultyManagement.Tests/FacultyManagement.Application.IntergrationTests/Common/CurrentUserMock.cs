﻿using FacultyManagement.Core.Security.Identity;

namespace FacultyManagement.Application.IntegrationTests.Common
{
    /// <summary>
    /// Mocks the Current User
    /// </summary>
    /// <seealso cref="FacultyManagement.Core.Security.Identity.IUser{FacultyManagement.Core.Security.Identity.CurrentUser}" />
    public class CurrentUserMock : IUser<CurrentUser>
    {
        public string Username { get; }
        public int CVid { get; }

        public CurrentUserMock( int cVid, string username )
        {
            CVid = cVid;
            Username = username;
        }
    }
}
