﻿using FacultyManagement.Core.Security.Identity;

namespace FacultyManagement.Application.IntegrationTests.Common
{
    /// <summary>
    /// Mocks the Current Person
    /// </summary>
    /// <seealso cref="FacultyManagement.Core.Security.Identity.IUser{FacultyManagement.Core.Security.Identity.CurrentPerson}" />
    public class CurrentPersonMock : IUser<CurrentPerson>
    {
        public string Username { get; }
        public int CVid { get; }

        public CurrentPersonMock( int cVid, string username )
        {
            CVid = cVid;
            Username = username;
        }
    }
}
