﻿
using System.Linq;
using FacultyManagement.Application.Admin;
using FacultyManagement.Application.Admin.ViewModels;
using FacultyManagement.Application.Authentication;
using FacultyManagement.Application.IntegrationTests.Common;
using FacultyManagement.Core.Security.Authentication;
using FacultyManagement.Data.Interfaces;
using FacultyManagement.Data.Services;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using NUnit.Framework;

namespace FacultyManagement.Application.IntegrationTests
{
    [TestFixture]
    public class AdminTests
    {
        public CurrentUserMock CurrentUser { get; set; } = new CurrentUserMock( 1, "Charles Ajiboye" );
        public IAdminDbService DbService { get; set; } = new AdminDbService();
        public IAuthenticationManager AuthenticationManager { get; set; } = new OwinContext().Authentication;
        public IAuthDbService AuthDbService { get; set; } = new AuthDbService();

        [Test]
        public void Should_Get_Faculty_User_That_Match_Search_Term()
        {
            // Arrange
            var userService = new UserService( new FacultyUserDbService() );
            var service = new AdminService( DbService, userService, new AuthService( new ActiveDirectory( AuthenticationManager, AuthDbService ) ) );
            int cVid = 1;
            // Act
            var result = service.NameAutoComplete( "Morrison", CurrentUser );

            // Assert
            Assert.IsNotNull( result );
            Assert.IsTrue( result.Count() < 5 );
            Assert.IsTrue( result.Any( r => r.label.Contains( "Morrison" ) ) );
            Assert.IsTrue( !result.Any( r => r.id.Contains( cVid.ToString() ) ) );
        }

        [Test]
        public void Should_Get_Active_Directory_Users_Not_In_Faculty_Management()
        {
            // Arrange
            var userService = new UserService( new FacultyUserDbService() );
            var service = new AdminService( DbService, userService, new AuthService( new ActiveDirectory( AuthenticationManager, AuthDbService ) ) );
            // Act
            var model = service.GetAddUserViewModel( CurrentUser ) as AddUserVm;

            // Assert
            Assert.IsNotNull( model );
            Assert.IsTrue( model.AvailableUsers.Any() );
        }

        [Test]
        public void Should_Get_Faculty_Departments_For_CVid()
        {
            // Arrange
            var userService = new UserService( new FacultyUserDbService() );
            var service = new AdminService( DbService, userService, new AuthService( new ActiveDirectory( AuthenticationManager, AuthDbService ) ) );
            // Act
            var model = service.GetOrganisations( CurrentUser.CVid );

            // Assert
            Assert.IsNotNull( model );
        }
    }
}
