﻿using AutoMapper;
using FacultyManagement.Application.IntegrationTests.Common;
using FacultyManagement.Application.PersonalInformation;
using FacultyManagement.Application.PersonalInformation.ViewModels;
using FacultyManagement.Data.Services;
using NUnit.Framework;

namespace FacultyManagement.Application.IntegrationTests
{
    [TestFixture]
    public class PersonalInformationTests
    {
        public CurrentPersonMock CurrentPerson { get; set; } = new CurrentPersonMock( 1, @"Belpark\Dillan" );        
        public PersonalInformationDbService DbService { get; set; } = new PersonalInformationDbService();

        [Test]
        public void Should_Get_Personal_Information_And_Return_View_Model()
        {
            // Arrange
            var service = new PersonalInformationService( DbService, new AdminDbService() );

            // Act
            var model = service.GetViewModel( CurrentPerson ) as PersonalInformationVm;

            // Assert
            Assert.IsTrue( !string.IsNullOrWhiteSpace( model.ImageSource ) );
            Assert.IsTrue( !string.IsNullOrWhiteSpace( model.Name ) );
            Assert.IsTrue( !string.IsNullOrWhiteSpace( model.SamAccountName ) );
        }

        [Test]
        public void Should_Get_Personal_Information_And_Return_Edit_Model()
        {
            // Arrange
            var service = new PersonalInformationService( DbService, new AdminDbService() );

            // Act
            var model = service.GetEditModel( CurrentPerson ) as PersonalInformationEditVm;

            // Assert
            Assert.IsTrue( !string.IsNullOrWhiteSpace( model.ImageSource ) );
            Assert.IsTrue( !string.IsNullOrWhiteSpace( model.Name ) );
            Assert.IsTrue( !string.IsNullOrWhiteSpace( model.SamAccountName ) );
        }
    }
}
