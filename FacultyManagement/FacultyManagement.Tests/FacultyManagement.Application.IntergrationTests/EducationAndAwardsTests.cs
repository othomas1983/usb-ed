﻿using FacultyManagement.Application.EducationAndAwards;
using FacultyManagement.Application.EducationAndAwards.ViewModels;
using FacultyManagement.Application.EducationAndAwards.ViewModels.Education;
using FacultyManagement.Application.IntegrationTests.Common;
using FacultyManagement.Application.PersonalInformation;
using FacultyManagement.Data.Interfaces;
using FacultyManagement.Data.Services;
using NUnit.Framework;

namespace FacultyManagement.Application.IntegrationTests
{
    [TestFixture]
    public class EducationAndAwardsTests
    {
        public CurrentPersonMock CurrentPerson { get; set; } = new CurrentPersonMock( 1, "" );
        public IEducationAndAwardsDbService DbService { get; set; } = new EducationAndAwardsDbService();

        [Test]
        public void Should_Get_Education_And_Awards_And_Return_View_Model()
        {
            // Arrange
            var service = new EducationAndAwardsService( DbService );

            // Act
            var model = service.GetViewModel( CurrentPerson ) as EducationAndAwardsVm;

            // Assert
            Assert.IsTrue( model.Educations.Count > 0 );
            Assert.IsTrue( model.Awards.Count > 0 );
            Assert.IsTrue( model.Grants.Count > 0 );
        }
    }
}
