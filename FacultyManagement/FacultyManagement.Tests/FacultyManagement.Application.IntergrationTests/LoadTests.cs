﻿

using FacultyManagement.Application.IntegrationTests.Common;
using FacultyManagement.Application.ManagementLoads;
using FacultyManagement.Application.ManagementLoads.ViewModels;
using FacultyManagement.Application.TeachingLoads;
using FacultyManagement.Application.TeachingLoads.ViewModels;
using FacultyManagement.Data.Interfaces;
using FacultyManagement.Data.Services;
using NUnit.Framework;

namespace FacultyManagement.Application.IntegrationTests
{
    [TestFixture]
    public class LoadTests
    {
        public ILoadsDbService DbService { get; set; } = new LoadsDbService();
        public CurrentPersonMock CurrentPerson { get; set; } = new CurrentPersonMock( 1, "rog" );

        [Test]
        public void Should_Get_Teaching_Load_Data_And_Return_View_Model()
        {
            // Arrange
            var service = new TeachingLoadService( DbService );

            // Act
            var model = service.GetViewModel( CurrentPerson );

            // Assert
            Assert.IsTrue( model is TeachingLoadVm );
            Assert.IsTrue( (model as TeachingLoadVm).TeachingLoads.Count > 0 );
        }

        [Test]
        public void Should_Get_Management_Load_Data_And_Return_View_Model()
        {
            // Arrange
            var service = new ManagementLoadService( DbService );

            // Act
            var model = service.GetViewModel( CurrentPerson );

            // Assert
            Assert.IsTrue( model is ManagementLoadVm );
            Assert.IsTrue( (model as ManagementLoadVm).ProgrammeHeads.Count > 0 );
        }
    }
}
