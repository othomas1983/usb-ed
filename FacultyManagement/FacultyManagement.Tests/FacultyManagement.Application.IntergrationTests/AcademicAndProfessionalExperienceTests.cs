﻿using System;
using System.Security.Principal;
using FacultyManagement.Application.AcademicAndProfessionalExperience;
using FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels;
using FacultyManagement.Application.AcademicAndProfessionalExperience.ViewModels.TeachingExperience;
using FacultyManagement.Application.IntegrationTests.Common;
using FacultyManagement.Core.Security.Authentication;
using FacultyManagement.Core.Security.Identity;
using FacultyManagement.Data.Interfaces;
using FacultyManagement.Data.Services;
using FacultyManagement.Domain.Model.Enums;
using FacultyManagement.Infrastructure.Utilities.ExtensionMethods;
using Microsoft.Build.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FacultyManagement.Application.IntegrationTests
{
    [TestClass]
    public class AcademicAndProfessionalExperienceTests
    {
        public CurrentPersonMock CurrentPerson { get; set; } = new CurrentPersonMock( 1, "rog" );
        public CurrentUserMock CurrentUser { get; set; } = new CurrentUserMock( 1, "rog" );
        public IAcademicAndProfessionalExperienceDbService DbService { get; set; } = new AcademicAndProfessionalExperienceDbService();


        [TestMethod]
        public void Should_Get_Experience_Data_And_Return_View_Model()
        {
            // Arrange
            var service = new AcademicAndProfessionalExperienceService(DbService);

            // Act
            var model = service.GetViewModel( CurrentPerson );
            
            // Assert
            Assert.IsTrue( model is AcademicAndProfessionalExperienceVm );
            Assert.IsTrue( (model as AcademicAndProfessionalExperienceVm).TeachingExperiences.Count > 0);
            Assert.IsTrue( (model as AcademicAndProfessionalExperienceVm).ExecutiveEducation.Count > 0 );

        }

        [TestMethod]
        public void Should_Map_To_ISaveModel_And_Create_New_Experience()
        {
            // Arrange
            IPrincipal principal = new ActiveDirectory( null, null ).CreatePrincipal( CurrentPerson.CVid.ToString(), CurrentPerson.Username );
            CurrentUser user = new CurrentUser( principal );
            var service = new AcademicAndProfessionalExperienceService( DbService );
            var model = new TeachingExperienceCreateVm
            {
                Id = null,
                StartDate = DateTime.Today,
                EndDate = DateTime.Today.AddYears( 2 ),
                Subject = "New Test Teaching Experience",
                Position = "New Position",
                Institution = "New Institution",
                ExperienceCategoryId = ExperienceCategory.Teaching.ConvertToInt(),
                City = "Cape Town",
                CountryId = 1,
                IsCurrent = false,
                IsActive = 1,
                CVid = CurrentPerson.CVid
            };

            // Act
            var success = service.SaveChanges( model, CurrentPerson, CurrentUser );

            // Assert
            Assert.IsTrue( success );
        }

        [TestMethod]
        public void Should_Map_To_ISaveModel_And_Update_Experience()
        {
            // Arrange
            IPrincipal principal = new ActiveDirectory( null, null ).CreatePrincipal( CurrentPerson.CVid.ToString(), CurrentPerson.Username );
            CurrentUser user = new CurrentUser( principal );
            var service = new AcademicAndProfessionalExperienceService( DbService );
            var model = new TeachingExperienceEditVm
            {
                Id = 161,
                StartDate = DateTime.Today,
                EndDate = DateTime.Today.AddYears( 2 ),
                Subject = "Edit Test Teaching Experience",
                Position = "Edit Position",
                Institution = "Edit Institution",
                ExperienceCategoryId = ExperienceCategory.Teaching.ConvertToInt(),
                City = "Cape Town",
                CountryId = 1,
                IsCurrent = false,
                IsActive = 1,
                CVid = CurrentPerson.CVid
            };

            // Act
            var success = service.SaveChanges( model, CurrentPerson, CurrentUser );

            // Assert
            Assert.IsTrue( success );
        }
    }
}
