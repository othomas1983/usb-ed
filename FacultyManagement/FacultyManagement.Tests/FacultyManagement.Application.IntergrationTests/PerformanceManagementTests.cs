﻿using FacultyManagement.Application.EducationAndAwards;
using FacultyManagement.Application.EducationAndAwards.ViewModels;
using FacultyManagement.Application.EducationAndAwards.ViewModels.Education;
using FacultyManagement.Application.IntegrationTests.Common;
using FacultyManagement.Application.PerformanceManagement.SelfEvaluation;
using FacultyManagement.Application.PerformanceManagement.SelfEvaluation.ViewModels;
using FacultyManagement.Application.PersonalInformation;
using FacultyManagement.Data.Interfaces;
using FacultyManagement.Data.Services;
using NUnit.Framework;
using System.Linq;
using FacultyManagement.Application.Admin;
using FacultyManagement.Application.PerformanceManagement.PeerEvaluation;
using FacultyManagement.Application.PerformanceManagement.PeerEvaluation.ViewModels;
using FacultyManagement.Application.PerformanceManagement.SupervisorEvaluation;
using FacultyManagement.Application.PerformanceManagement.SupervisorEvaluation.ViewModels;

namespace FacultyManagement.Application.IntegrationTests
{
    [TestFixture]
    public class PerformanceManagementTests
    {
        public CurrentPersonMock CurrentPerson { get; set; } = new CurrentPersonMock( 214, "Andre Mouton" );
        public CurrentUserMock CurrentUser { get; set; } = new CurrentUserMock( 1, "Charles Ajiboye" );

        [Test]
        public void Should_Get_Self_Evaluation_Data_And_Return_View_Model()
        {
            // Arrange
            var service = new SelfEvaluationService( new PerformanceManagementDbService() );

            // Act
            var model = service.GetViewModel( CurrentPerson ) as SelfEvaluationVm;

            // Assert
            Assert.IsTrue( model.Integrity.Evaluations.ToList().Count > 0 );
            Assert.IsTrue( model.Inclusivity.Evaluations.ToList().Count > 0 );
            Assert.IsTrue( model.Sustainability.Evaluations.ToList().Count > 0 );
        }

        [Test]
        public void Should_Get_Peer_Evaluation_Data_And_Return_View_Model()
        {
            // Arrange
            var userService = new UserService( new FacultyUserDbService() );
            var service = new PeerEvaluationService( new PerformanceManagementDbService(), userService );

            // Act
            var model = service.GetViewModel( CurrentPerson, CurrentUser ) as PeerEvaluationVm;

            // Assert
            Assert.IsTrue( model.Integrity.Evaluations.ToList().Count > 0 );

        }

        [Test]
        public void Should_Get_Supervisor_Evaluation_Data_And_Return_View_Model()
        {
            // Arrange
            var userService = new UserService( new FacultyUserDbService() );
            var service = new SupervisorEvaluationService( new PerformanceManagementDbService(), userService );
            var supervisor = new CurrentPersonMock( 33, "Marsunet Horn" );

            // Act
            var model = service.GetViewModel( supervisor, CurrentUser ) as SupervisorEvaluationVm;

            // Assert
            Assert.IsTrue( model.Integrity.Evaluations.ToList().Count > 0 );

        }
    }
}
