﻿
using System.IO;
using System.Net;
using System.Text;
using NUnit.Framework;

namespace FacultyManagement.Application.IntegrationTests
{
    [TestFixture]
    public class SupervisionLoadTests
    {
        [Test]
        public void Should_Load_Writehand_Research_Assignments()
        {
            // Arrange
            string employeeId = "14728427";
            string url = "http://writehand.usb.ac.za/API/Reporting/ResearchAssignments";

            bool hasDownloadedInfo = false;

            // Act
            try
            {
                using ( WebClient webClient = new WebClient() )
                {
                    webClient.Encoding = Encoding.UTF8;
                    webClient.Headers.Add( "Content-Type", "application/json" );
                    webClient.QueryString.Add( "employeeId", employeeId );

                    string value = webClient.DownloadString( url );

                    hasDownloadedInfo = !string.IsNullOrWhiteSpace( value );
                }
            }
            catch ( WebException )
            {
                //
            }

            // Assert
            Assert.IsTrue( hasDownloadedInfo );
        }
    }
}
