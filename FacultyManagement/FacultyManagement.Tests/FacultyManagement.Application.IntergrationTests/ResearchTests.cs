﻿
using System;
using System.Linq;
using System.Text.RegularExpressions;
using FacultyManagement.Application.Admin;
using FacultyManagement.Application.IntegrationTests.Common;
using FacultyManagement.Application.Research;
using FacultyManagement.Application.Research.ViewModels;
using FacultyManagement.Data.Services;
using FacultyManagement.Domain.Factories;
using FacultyManagement.Domain.Model.Research;
using NUnit.Framework;
using System.Web;

namespace FacultyManagement.Application.IntegrationTests
{
    [TestFixture]
    public class ResearchTests
    {
        public CurrentPersonMock CurrentPerson { get; set; } = new CurrentPersonMock( 1, "Charles" );
        
        [Test]
        public void Should_Get_Research_And_Return_View_Model()
        {
            // Arrange
            var service = new ResearchService( new ResearchDbService(), new UserService( new FacultyUserDbService() ), new SharepointWebService() );
            // Act
            var model = service.GetViewModel( CurrentPerson ) as ResearchVm;

            // Assert
            Assert.IsTrue( model.Activities.ToList().Count > 0 );
        }

        [Test]
        public void Should_Get_Contributors_Given_A_Research_Id()
        {
            // Arrange
            int researchHistoryId = 185;

            // Act
            var dataSet = new ResearchDbService().GetContributors( researchHistoryId );
            var contributors = ModelFactory.CreateList<Contributor>( dataSet ).ToList();

            // Assert
            Assert.IsTrue( contributors.ToList().Count > 0 );
        }

        [Test]
        public void Should_Upload_File_To_Sharepoint()
        {
            // Arrange
            var service = new SharepointWebService();
            var file = new File
            {
                Name = "test_file.txt",
                Data = @"aGVsbG8gd29ybGQ="
            };
            int cvId = 214;

            // Act
            string plainText = HttpUtility.UrlDecode( file.Data );
            byte[] fileData = Convert.FromBase64String( plainText  );
            var result = service.UploadResearch(file.Name, cvId, DateTime.Now, fileData );

            // Assert
            Assert.IsTrue( result.Success );
        }

        [Test]
        public void Should_Remove_Base64_Info_From_File_Data()
        {
            // Arrange            
            var file = new File
            {
                Name = "test_file.txt",
                Data = @"data:text/plain;base64,aGVsbG8gd29ybGQ="
            };            

            // Act
            string convert = Regex.Replace( file.Data, @"^[^,]*,", string.Empty );

            // Assert
            Assert.IsFalse( convert.Contains( "data:text/plain;base64" ) );
        }
    }
}
