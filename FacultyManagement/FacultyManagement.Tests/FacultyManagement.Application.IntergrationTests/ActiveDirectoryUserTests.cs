﻿
using System.Collections.Generic;
using FacultyManagement.Application.Admin;
using FacultyManagement.Data.Services;
using FacultyManagement.Infrastructure;
using NUnit.Framework;

namespace FacultyManagement.Application.IntegrationTests
{
    [TestFixture]
    public class ActiveDirectoryUserTests
    {
        readonly List<string> _usernames = new List<string> { @"Belpark\Dillianc_admin" };

        [Test]
        public void Should_Add_User_To_Active_Directory_Group()
        {
            // Arrange
            var service = new UserService( new FacultyUserDbService() );
            bool success = true;
            // Act
            try
            {
                service.AddUsersToGroup( _usernames, ADGroups.Manager );
            }
            catch ( System.Exception )
            {
                success = false;
            }

            // Assert
            Assert.IsTrue( success );
        }

        [Test]
        public void Should_Remove_User_To_Active_Directory_Group()
        {
            // Arrange
            var service = new UserService( new FacultyUserDbService() );
            bool success = true;
            
            // Act
            try
            {
                service.RemoveUsersFromGroup( _usernames, ADGroups.Manager );
            }
            catch ( System.Exception )
            {
                success = false;
            }

            // Assert
            Assert.IsTrue( success );
        }
    }
}
