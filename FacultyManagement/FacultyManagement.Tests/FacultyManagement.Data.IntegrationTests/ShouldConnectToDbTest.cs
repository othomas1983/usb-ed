﻿using System;
using System.Data;
using System.Data.SqlClient;
using FacultyManagement.Data.Properties;
using NUnit.Framework;
using NUnit.Framework.Internal;

namespace FacultyManagement.Core.IntegrationTests
{
    [TestFixture]
    public class ShouldConnectToDbTest
    {
        [Test]
        public void Can_Connect_To_Database()
        {
            // Arrange
            string testingConnectionString = Settings.Default.FacultyContext;
            bool isOpen = false;

            try
            {
                // Act
                SqlConnection con;
                using ( con = new SqlConnection( testingConnectionString ) )
                {
                    if ( con.State == ConnectionState.Closed )
                    {
                        con.Open();
                    }

                    isOpen = con.State == ConnectionState.Open;
                }
            }
            catch ( Exception )
            {
               //
            }

            // Assert
            Assert.IsTrue( isOpen );
        }
    }
}
