﻿using System.Data;
using FacultyManagement.Data.Models;
using FacultyManagement.Data.Services;
using FacultyManagement.Domain.Model;
using FacultyManagement.Domain.Model.PersonalInformation;
using NUnit.Framework;

namespace FacultyManagement.Data.IntegrationTests
{
    [TestFixture]
    public class PersonalInformationTests
    {
        public string Username { get; set; } = @"belpark\dorrian";
        public PersonalInformationDbService Service { get; set; } = new PersonalInformationDbService();

        [Test]
        public void Should_Return_Info_For_Given_Username()
        {
            // Act
            DataSet dataSet = Service.GetPersonalInformation( Username );
            DataTable table = dataSet.Tables[0];
            DataRow row = table.Rows[0];
            string name = row["Name"].ToString();
            string surname = row["Surname"].ToString();

            // Assert
            Assert.IsNotNull( dataSet );
            Assert.IsNotNull( table );
            Assert.IsNotNull( row );
            Assert.IsNotEmpty( name );
            Assert.IsNotEmpty( surname );
        }

        [Test]
        public void Should_Save_And_Update_Database_Record()
        {
            // Arrange
            var model = new SavePersonalInformation
            {
                Cvid = 2,
                Title = "Professor",
                Initials = "DA",
                Name = "Dillan",
                Surname = "Cagnetta",
                NationalityId = 2,
                AffiliationId = 1,
                IsFaculty = 1,
                MainActivityId = 1,
                AcademicRank = "Senior Lecturer",
                ApplicationUserRoleId = 3,
                Biography = "He is such a great guy",
                BusinessContactNo = "555 888 9999",
                BusinessUnit = "EOH",
                EmailAddress = "dillan@yahoo.com",
                Position = "Professor",
                Gender = "Male",
                HighestQualificationId = 1,
                Expertise = "technology",
                HomeInstitution = "Rhodes University",
                OrganisationalResponsibility = "Developer",
                ResearchInterest = "Programming",
                SamAccountName = @"belpark\dorrian",
                EmployeeId = "16933001"
            };

            // Act
            bool success = Service.SaveChanges( model, Username );

            // Assert
            Assert.IsTrue( success );
        }

        [Test]
        public void Should_Map_To_Domain_Model_From_DataSet()
        {
            // Act
            var dataSet = Service.GetPersonalInformation( Username );
            var model = new PersonalInformation( dataSet );

            // Assert
            Assert.IsNotNull( model );
            Assert.AreEqual( "Professor", model.Title );
            Assert.AreEqual( "Dillan", model.Name );
            Assert.AreEqual( "Cagnetta", model.Surname );
        }

        [Test]
        public void Should_Save_And_Create_New_Database_Record()
        {
            // Arrange
            var model = new SavePersonalInformation
            {
                Cvid = null,
                Title = "Pastor",
                Initials = "DA",
                Name = "Dorrian",
                Surname = "Cagnetta",
                NationalityId = 2,
                AffiliationId = 1,
                IsFaculty = 1,
                MainActivityId = 1,
                AcademicRank = "Senior Lecturer",
                ApplicationUserRoleId = 3,
                Biography = "He is such a great guy",
                BusinessContactNo = "555 888 9999",
                BusinessUnit = "EOH",
                EmailAddress = "dillan@yahoo.com",
                Position = "Professor",
                Gender = "Male",
                HighestQualificationId = 1,
                Expertise = "technology",
                HomeInstitution = "Rhodes University",
                OrganisationalResponsibility = "Developer",
                ResearchInterest = "Programming",
                SamAccountName = "dorrian",
                EmployeeId = "16933001"
            };
            string query = $"SELECT *  FROM [FacultyManagement].[dbo].CV WHERE SamAccountName = '{Username}'";

            // Act
            bool success =Service.SaveChanges( model, Username );
            var data = DbService.GetDataTable( query, CommandType.Text, null );

            // Assert
            Assert.IsTrue( success );
            Assert.IsTrue( data.Rows.Count > 1 );
        }

        [Test]
        public void Should_Delete_And_Insert_New_UserPhoto()
        {
            // Arrange
            string employeeId = "14728427";  //  <---   'belpark\charlesa'
            byte[] fileData = { new byte(), new byte() };
            string query = $"SELECT file_stream FROM FacultyImageStore WHERE name LIKE '%{employeeId}%'";
            var model = new SavePersonalInformation
            {
                Cvid = 2,
                Title = "Pastor",
                Initials = "DA",
                Name = "Dillan",
                Surname = "Cagnetta",
                NationalityId = 2,
                AffiliationId = 1,
                IsFaculty = 1,
                MainActivityId = 1,
                AcademicRank = "Senior Lecturer",
                ApplicationUserRoleId = 3,
                Biography = "He is such a great guy",
                BusinessContactNo = "555 888 9999",
                BusinessUnit = "EOH",
                EmailAddress = "dillan@yahoo.com",
                Position = "Professor",
                Gender = "Male",
                HighestQualificationId = 1,
                Expertise = "technology",
                HomeInstitution = "Rhodes University",
                OrganisationalResponsibility = "Developer",
                ResearchInterest = "Programming",
                SamAccountName = @"belpark\dorrian",
                EmployeeId = employeeId,
                UserPhoto = new UserPhoto( employeeId, ".jpg",  fileData )
            };

            // Act
            Service.SaveChanges( model, Username );
            var table = DbService.GetDataTable( query, CommandType.Text, null );
            var row = table.Rows[0];
            int items = ((byte[]) row.ItemArray[0]).Length;
                
            // Assert
            Assert.IsTrue( items == 2 );
        }
    }
}
