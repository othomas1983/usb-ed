﻿using System.Collections.Generic;
using System.Data;
using FacultyManagement.Data.Services;
using FacultyManagement.Domain.Factories;
using FacultyManagement.Domain.Model;
using FacultyManagement.Domain.Model.Common;
using FacultyManagement.Domain.Model.Enums;
using NUnit.Framework;

namespace FacultyManagement.Data.IntegrationTests
{
    [TestFixture]
    public class DropDownsTests
    {
        [Test]
        public void Should_Return_Data_For_Department_Categories()
        {
            // Arrange
            var service = new DropDownsDbService();

            // Act
            DataSet dataSet = service.GetData( DropDownType.Departments );
            DataTable table = dataSet.Tables[0];
            int count = table.Rows.Count;

            // Assert
            Assert.IsTrue( count > 0 );
        }

        [Test]
        public void Should_Return_Data_For_TaskTeamActivities()
        {
            // Arrange
            var service = new DropDownsDbService();

            // Act
            DataSet dataSet = service.GetData( DropDownType.TaskTeamActivity );
            DataTable table = dataSet.Tables[0];
            int count = table.Rows.Count;

            // Assert
            Assert.IsTrue( count > 0 );
        }

        [Test]
        public void Should_Return_Data_For_Departments()
        {
            // Arrange
            var service = new DropDownsDbService();

            // Act
            DataSet dataSet = service.GetData( DropDownType.Departments );
            DataTable table = dataSet.Tables[0];
            int count = table.Rows.Count;

            // Assert
            Assert.IsTrue( count > 0 );
        }

        [Test]
        public void Should_Return_Data_For_Nationalities()
        {
            // Arrange
            var service = new DropDownsDbService();

            // Act
            DataSet dataSet = service.GetData( DropDownType.Nationalities );
            DataTable table = dataSet.Tables[0];
            int count = table.Rows.Count;

            // Assert
            Assert.IsTrue( count > 0 );
        }

        [Test]
        public void Factory_Should_Return_List_From_Datasets()
        {
            // Arrange
            var service = new DropDownsDbService();
            DataSet dataSet = service.GetData( DropDownType.Nationalities );

            // Act
            IList<Nationality> list = ModelFactory.CreateList<Nationality>( dataSet );

            // Assert
            Assert.IsTrue( list.Count > 0 );
        }
    }
}
