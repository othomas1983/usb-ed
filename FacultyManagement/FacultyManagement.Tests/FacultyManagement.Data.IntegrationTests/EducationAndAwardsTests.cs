﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Data.Models;
using FacultyManagement.Data.Services;
using NUnit.Framework.Internal;
using NUnit.Framework;

namespace FacultyManagement.Data.IntegrationTests
{
    [TestFixture]
    public  class EducationAndAwardsTests
    {
        public string Username { get; set; } = @"belpark\charlesa";
        public int CVid { get; set; } = 1;
        public EducationAndAwardsDbService DbService { get; set; } = new EducationAndAwardsDbService();

        [Test]
        public void Should_Save_And_Create_New_Education_Database_Record()
        {
            // Arrange
            var model = new SaveEducation
            {
                Id = null,
                IsActive = 1,
                Year = 2017,
                Degree = "BSc",
                FieldOfStudy = "IT",
                Country = "South Africa",
                University = "Rhodes",
                CVid = 1
            };
            // Act
            bool success = DbService.SaveChanges( model, Username );

            // Assert
            Assert.IsTrue( success );
        }

        [Test]
        public void Should_Save_Changes_To_Existing_Education_Database_Record()
        {
            // Arrange
            var model = new SaveEducation
            {
                Id = 5,
                IsActive = 1,
                Year = 2017,
                Degree = "BSc",
                FieldOfStudy = "IT",
                Country = "South Africa",
                University = "Rhodes",
                CVid = 1
            };
            // Act
            bool success = DbService.SaveChanges( model, Username );

            // Assert
            Assert.IsTrue( success );
        }
    }
}
