﻿
using FacultyManagement.Data.Interfaces;
using FacultyManagement.Data.Models;
using FacultyManagement.Data.Services;
using NUnit.Framework;

namespace FacultyManagement.Data.IntegrationTests
{
    [TestFixture]
    public class PerformanceManagementTests
    {
        public string Username { get; set; } = @"belpark\charlesa";
        public int CVid { get; set; } = 1;
        public IPerformanceManagementDbService DbService { get; set; } = new PerformanceManagementDbService();

        [Test]
        public void Should_Save_And_Create_New_Self_Evaluation_Database_Record()
        {
            // Arrange
            var model = new SaveEvaluation
            {
                Id = null,
                Year = 2017,
                Category = "Integrity",
                Description = "Say something nice",
                ValueCommitment = "Strong Performance",
                CVid = CVid
            };
            // Act
            bool success = DbService.SaveChanges( model, Username );

            // Assert
            Assert.IsTrue( success );
        }

        [Test]
        public void Should_Save_And_Update_Self_Evaluation_Database_Record()
        {
            // Arrange
            var model = new SaveEvaluation  
            {
                Id = 1,
                Description = "Say something nice",
            };
            // Act
            bool success = DbService.SaveChanges( model, Username );

            // Assert
            Assert.IsTrue( success );
        }

        [Test]
        public void Should_Save_And_Create_New_Peer_Evaluation_Database_Record()
        {
            // Arrange
            var model = new SaveEvaluation
            {
                Id = null,
                Year = 2017,
                Category = "Integrity",
                Description = "Say something nice",
                ValueCommitment = "Strong Performance",
                CVid = CVid,
                PeerCVid = 214
            };
            // Act
            bool success = DbService.SaveChanges( model, Username );

            // Assert
            Assert.IsTrue( success );
        }

        [Test]
        public void Should_Save_And_Update_Peer_Evaluation_Database_Record()
        {
            // Arrange
            var model = new SaveEvaluation
            {
                Id = 1,
                Description = "Say something nice",
                PeerCVid = 1
            };
            // Act
            bool success = DbService.SaveChanges( model, Username );

            // Assert
            Assert.IsTrue( success );
        }
    }
}
