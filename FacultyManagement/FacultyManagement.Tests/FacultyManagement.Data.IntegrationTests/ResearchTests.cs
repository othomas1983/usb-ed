﻿using System;
using System.Collections.Generic;
using FacultyManagement.Data.Interfaces;
using FacultyManagement.Data.Models;
using FacultyManagement.Data.Services;
using NUnit.Framework;

namespace FacultyManagement.Data.IntegrationTests
{
    [TestFixture]
    public class ResearchTests
    {
        public int ResearchId { get; set; } = 2077;
        public int ResearchStatusId { get; set; } = 4;
        public DateTime ResearchStatusDate { get; set; } = new DateTime( 2017, 05, 20 );


        public string Username { get; set; } = @"Belpark\Dillian";
        public int CVid { get; set; } = 1;
        public IResearchDbService DbService { get; set; } = new ResearchDbService();

        [Test]
        public void Should_Save_New_Research_And_Create_New_History_And_Contributors()
        {
            // Arrange
            var model = new SaveResearchActivity
            {
                Id = null,
                CVid = CVid,
                ResearchOutputCategoryId = 1,
                LocusId = 1,
                ArticleTitle = "Title",
                ABSRatingStatusId = 1,
                DHETStatusId = 1,
                Credit = 500,
                PublicationName = "Name",
                ResearchStatusDate = DateTime.Today,
                ResearchStatusId = 1,
                ResearchContributionTypeId = 1,
                Contributors = new List<SaveContributor> {
                    new SaveContributor
                    {
                        Id = null,
                        CVid = 3,
                        DHETCredit = 500,
                        Sequence = 2,
                        RoleId = 1,
                        Type = "Internal"
                    },
                    new SaveContributor
                    {
                        Id = null,
                        CVid = null,
                        DHETCredit = 500,
                        Sequence = 2,
                        RoleId = 1,
                        FirstName = "Dillan",
                        Surname = "Cagnetta",
                        Gender = "Male",
                        Type = "External"
                    }
                }
            };

            // Act 
            bool success = DbService.SaveChanges( model, Username );
            // Assert
            Assert.IsTrue( success );
        }

        [Test]
        public void Should_Edit_Research_And_Create_New_History_Row_For_Research_Status_Change()
        {
            // Arrange
            var model = new SaveResearchActivity
            {
                Id = ResearchId, //Research History Id
                CVid = CVid,
                ResearchOutputCategoryId = 1,
                LocusId = 1,
                ArticleTitle = "Updated Title",
                ABSRatingStatusId = 1,
                DHETStatusId = 1,
                Credit = 500,
                PublicationName = "Update Name",
                ResearchStatusDate = ResearchStatusDate, // Date has changed
                ResearchStatusId = ResearchStatusId,   // Status has changed
                ResearchContributionTypeId = 1
            };

            // Act 
            bool success = DbService.SaveChanges( model, Username );
            // Assert
            Assert.IsTrue( success );
        }

        [Test]
        public void Should_Return_Count_1_When_Finding_Matching_Research()
        {
            // Arrange
            string query = $@"SELECT Count(*) FROM ResearchHistory WHERE ResearchID = {ResearchId}
                                            AND ResearchStatusID = {ResearchStatusId}  AND ResearchStatusDate = '{ResearchStatusDate.ToShortDateString()}'
                                            AND IsActive = 1";


            // Act
            int? count = int.Parse( Services.DbService.ExecuteScaler( query ).ToString() );

            // Assert
            Assert.IsTrue( count > 0 );
        }

        [Test]
        public void Should_Edit_Research()
        {
            // Arrange
            var model = new SaveResearchActivity
            {
                Id = ResearchId, //Research History Id
                ResearchId = ResearchId, //Research Id
                CVid = CVid,
                ResearchOutputCategoryId = 1,
                LocusId = 1,
                ArticleTitle = "Updated Title",
                ABSRatingStatusId = 1,
                DHETStatusId = 1,
                Credit = 500,
                PublicationName = "Update Name",
                ResearchStatusDate = ResearchStatusDate, // Date has changed
                ResearchStatusId = ResearchStatusId,   // Status has changed
                ResearchContributionTypeId = 1,
                Country = "South Africa"
            };

            // Act 
            bool success = DbService.SaveChanges( model, Username );
            // Assert
            Assert.IsTrue( success );
        }
    }
}
