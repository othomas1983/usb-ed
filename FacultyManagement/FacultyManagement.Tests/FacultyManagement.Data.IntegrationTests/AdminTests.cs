﻿using System.Data;
using FacultyManagement.Data.Services;
using NUnit.Framework;

namespace FacultyManagement.Data.IntegrationTests
{
    [TestFixture]
    public class AdminTests
    {
        [Test]
        public void Should_Retrieve_User_Details()
        {
            // Arrange
            var service = new FacultyUserDbService();

            // Act
            var dataSet = service.GetAllFacultyUsers();
            DataTable table = dataSet.Tables[0];
            DataRow row = table.Rows[0];
            string name = row["Name"].ToString();
            string surname = row["Surname"].ToString();

            // Assert
            Assert.IsNotNull( dataSet );
            Assert.IsNotNull( table );
            Assert.IsNotNull( row );
            Assert.IsTrue( table.Rows.Count > 0 );
            Assert.IsNotEmpty( name );
            Assert.IsNotEmpty( surname );
        }
    }
}
