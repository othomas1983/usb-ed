﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Data.Services;
using NUnit.Framework;
using NUnit.Framework.Internal;

namespace FacultyManagement.Data.IntegrationTests
{
    [TestFixture]
    public class AuthDbServiceTests
    {
        public AuthDbService DbService { get; set; } = new AuthDbService();

        [Test]
        public void Should_Return_True_And_CVid_Value_If_Username_Exists()
        {
            // Arrange
            string username = @"belpark\Dillan";
            int cVid;

            // Act
            bool result = DbService.CheckUserExists( username, out cVid );

            // Assert
            Assert.IsTrue( result );
            Assert.IsTrue( cVid > 0 );

        }

        [Test]
        public void Should_Return_False_And_CVid_0_If_Username_Does_Not_Exist()
        {
            // Arrange
            string username = @"doesntexist";
            int cVid;

            // Act
            bool result = DbService.CheckUserExists( username, out cVid );

            // Assert
            Assert.IsFalse( result );
            Assert.IsTrue( cVid == 0 );
        }
    }
}
