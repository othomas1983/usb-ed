﻿using System;
using System.Data;
using FacultyManagement.Domain.Model;
using FacultyManagement.Domain.Model.PersonalInformation;
using NUnit.Framework;
using NUnit.Framework.Internal;

namespace FacultyManagement.Domain.UnitTests
{
    [TestFixture]
    public class ModelsShould
    {
        PersonalInformation _model;

        [Test]
        public void Models_Should_Throw_Null_Exception_When_DataSet_Is_Null()
        {
           // Assert
            Assert.Throws( typeof( ArgumentNullException ),
                new TestDelegate( ContructModelThrowsNull ) );
        }

        [Test]
        public void Models_Should_Throw_NotFound_Exception_When_No_Data_Returned()
        {
            // Assert
            Assert.Throws( typeof( ObjectNotFoundException ),
                new TestDelegate( ContructModelThrowsNotFound ) );
        }

        void ContructModelThrowsNull( )
        {
            DataSet dataSet = null;
            _model = new PersonalInformation( dataSet );
        }

        void ContructModelThrowsNotFound()
        {
            //Table contains no rows of data
            DataSet dataSet = new DataSet();
            DataTable table = new DataTable();
            dataSet.Tables.Add( table );

            _model = new PersonalInformation( dataSet );
        }
    }
}
