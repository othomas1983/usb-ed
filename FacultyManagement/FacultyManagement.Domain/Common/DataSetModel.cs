﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacultyManagement.Domain.Common
{
    /// <summary>
    /// DataSetModel base abstract class
    /// </summary>
    /// <seealso cref="FacultyManagement.Domain.Common.IDataSetModel" />
    public abstract class DataSetModel : IDataSetModel
    {
        public DataSet Set { get; set; }
        public DataRow Row { get; set; }

        protected DataSetModel()
        {
        }

        protected DataSetModel( DataSet dataSet )
        {
            if ( dataSet == null ) throw new ArgumentNullException();
            // Check if there is data in the table
            if ( !(dataSet.Tables[0].Rows.Count > 0) ) throw new ObjectNotFoundException();

            Set = dataSet;
            if ( dataSet.Tables[0].Rows[0] != null )
            {
                Row = dataSet.Tables[0].Rows[0];
            }
        }

        public abstract void MapFromDataSet();
    }
}
