﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacultyManagement.Domain.Common
{
    /// <summary>
    /// DataRowModel base abstract class
    /// </summary>
    /// <seealso cref="FacultyManagement.Domain.Common.IDataRowModel" />
    public abstract class DataRowModel : IDataRowModel
    {
        /// <summary>
        /// Gets or sets the primary key identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Id { get; protected set; }
        
        /// <summary>
        /// Gets or sets the name or description field.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Description { get; protected set; }

        /// <summary>
        /// Gets or sets the is active field.
        /// </summary>
        /// <value>
        /// The is active.
        /// </value>
        public int IsActive { get; protected set; }

        /// <summary>
        /// Maps a specific datarow to domain model
        /// </summary>
        /// <param name="row"></param>
        public abstract void MapFromDataRow( DataRow row );
    }
}
