﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacultyManagement.Domain.Common
{
    public interface IDataSetModel
    {
        /// <summary>
        /// Maps dataset to domain model
        /// </summary>
        void MapFromDataSet();
    }
}
