﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Domain.Common;
using FacultyManagement.Domain.ExtensionMethods;

namespace FacultyManagement.Domain.Model.AcademicAndProfessionalExperience
{
    public class Experience : DataRowModel
    {
        #region Properties

        public DateTime StartDate { get; private set; }
        public DateTime? EndDate { get; private set; }
        public int ExperienceCategoryId { get; private set; }
        public string Subject { get; private set; }
        public string Position { get; private set; }
        public string Institution { get; private set; }
        public string City { get; private set; }
        public string Country { get; private set; }
        public int NoOfDays { get; private set; }
        public string Programme { get; private set; }
        public string KnowledgeArea { get; private set; }
        public string Client { get; private set; }
        public int IsCurrent { get; private set; }

        public string Publication { get; private set; }
        public string PublicationStatus { get; private set; }
        public int? PublicationEdition { get; private set; }

        #endregion


        public Experience()
        {
        }

        public Experience( DataRow row )
        {
            MapFromDataRow( row );
        }
        public override void MapFromDataRow( DataRow row )
        {
            if ( row == null ) return;

            Id = row["ExperienceId"].ValueOrDefault<int>();
            IsActive = row["IsActive"].ValueOrDefault<int>();
            StartDate = row["StartDate"].ValueOrDefault<DateTime>();
            EndDate = row["EndDate"].ValueOrDefault<DateTime?>();
            Subject = row["SubjectArea"].ValueOrDefault<string>();
            Position = row["Position"].ValueOrDefault<string>();
            Institution = row["Institution"].ValueOrDefault<string>();
            City = row["City"].ValueOrDefault<string>();
            Country = row["Country"].ValueOrDefault<string>();
            ExperienceCategoryId = row["ExperienceCategoryId"].ValueOrDefault<int>();
            NoOfDays = row["NoOfDays"].ValueOrDefault<int>();
            Programme = row["Programme"].ValueOrDefault<string>();
            KnowledgeArea = row["KnowledgeArea"].ValueOrDefault<string>();
            Client = row["Client"].ValueOrDefault<string>();
            IsCurrent = row["IsCurrent"].ValueOrDefault<int>();

            Publication = row["Publication"].ValueOrDefault<string>();
            PublicationStatus = row["PublicationStatus"].ValueOrDefault<string>();
            PublicationEdition = row["PublicationEdition"].ValueOrDefault<int>();


        }
    }
}
