﻿using System.Security.Claims;

namespace FacultyManagement.Domain.Model
{
    public class UserLogin
    {
        public string Username { get; }

        public UserLogin( ClaimsPrincipal principal )
        {
            Username = principal?.FindFirst( ClaimTypes.Name ).Value;
        }
    }
}
