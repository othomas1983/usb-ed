﻿using System.Data;
using FacultyManagement.Domain.Common;
using FacultyManagement.Domain.ExtensionMethods;

namespace FacultyManagement.Domain.Model.PerformanceManagement.Common
{
    public class Evaluation : DataRowModel
    {
        #region Properties
        public int Year { get; protected set; }
        public int CVid { get; protected set; }
        public string ValueCommitment { get; protected set; }
        public string Category { get; protected set; }
        #endregion

        protected Evaluation()
        {
        }

        protected Evaluation( DataRow row )
        {
            MapFromDataRow( row );
        }

        public override void MapFromDataRow( DataRow row )
        {
            if ( row == null ) return;

            Year = row["Year"].ValueOrDefault<int>();
            ValueCommitment = row["ValueCommitment"].ValueOrDefault<string>();
            Category = row["Category"].ValueOrDefault<string>();
            Description = row["Description"].ValueOrDefault<string>();
        }
    }
}
