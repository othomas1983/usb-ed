﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Domain.ExtensionMethods;
using FacultyManagement.Domain.Model.PerformanceManagement.Common;

namespace FacultyManagement.Domain.Model.PerformanceManagement
{
    public class SelfEvaluation : Evaluation
    {
        #region Properties
        
        #endregion

        public SelfEvaluation()
        {
        }

        public SelfEvaluation( DataRow row )
        {
            
        }

        public override void MapFromDataRow( DataRow row )
        {
            if ( row == null ) return;

            base.MapFromDataRow( row );

            Id = row["PerformanceManagementID"].ValueOrDefault<int>();
            CVid = row["CVid"].ValueOrDefault<int>();
        }

    }
}
