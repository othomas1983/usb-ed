﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Domain.ExtensionMethods;
using FacultyManagement.Domain.Model.PerformanceManagement.Common;

namespace FacultyManagement.Domain.Model.PerformanceManagement
{
    public class SupervisorEvaluation : Evaluation
    {
        #region Properties

        public int SupervisorCVid { get; set; }

        #endregion

        public SupervisorEvaluation()
        {
        }

        public SupervisorEvaluation( DataRow row )
        {
            
        }

        public override void MapFromDataRow( DataRow row )
        {
            if ( row == null ) return;

            base.MapFromDataRow( row );

            Id = row["SupervisorPerformanceManagementId"].ValueOrDefault<int>();
            CVid = row["ReviewerCVID"].ValueOrDefault<int>();
            SupervisorCVid = row["SupervisorCVID"].ValueOrDefault<int>();
        }

    }
}
