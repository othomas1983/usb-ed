﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Domain.Common;
using FacultyManagement.Domain.ExtensionMethods;

namespace FacultyManagement.Domain.Model.PerformanceManagement
{
   public class Commitment : DataRowModel
    {
        #region Properties

        public int Year { get; private set; }
        public int PerformanceCategoryId { get; private set; }
        public string PerformanceCategory { get; private set; }
        public string CommitmentValue { get; private set; }
        public string BaseLine { get; private set; }
        public int CVid { get; private set; }

        #endregion

        public Commitment()
        {
        }

        public Commitment( DataRow row )
        {
            MapFromDataRow( row );
        }

        public override void MapFromDataRow( DataRow row )
        {
            if ( row == null ) return;

            Id = row["CommitmentID"].ValueOrDefault<int>();
            Year = row["Year"].ValueOrDefault<int>();
            Description = row["CommitmentDesc"].ValueOrDefault<string>();
            CommitmentValue = row["Commitment"].ValueOrDefault<string>();
            BaseLine = row["BaseLine"].ValueOrDefault<string>();
            PerformanceCategoryId = row["PerformanceCategoryId"].ValueOrDefault<int>();
            PerformanceCategory = row["PerformanceCategory"].ValueOrDefault<string>();
            CVid = row["CVID"].ValueOrDefault<int>();
        }
    }
}
