﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacultyManagement.Domain.Model.Dropdown
{
    public class DropdownFilterViewModel
    {
        public int? selectedIndex { get; set; }
        public int? filterIndex { get; set; }
        public int? year { get; set; }
    }
}
