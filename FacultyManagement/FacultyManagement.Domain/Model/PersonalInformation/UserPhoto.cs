﻿using System;
using System.Data;
using FacultyManagement.Domain.Common;
using FacultyManagement.Domain.ExtensionMethods;

namespace FacultyManagement.Domain.Model.PersonalInformation
{
    public class UserPhoto : DataSetModel
    {
        #region Properties

        public string UsNumber { get; private set; }
        public byte[] FacultyImage { get; private set; }
        public string ImageSource
        {
            get
            {
                var base64 = Convert.ToBase64String( FacultyImage );
                var imgSrc = $"data:image/gif;base64,{base64}";
                return imgSrc;
            }
        }

        #endregion

        public UserPhoto( string employeeId, string fileExtention, byte[] imageData )
        {
            FacultyImage = imageData;
            UsNumber = employeeId + fileExtention;
        }

        public UserPhoto( DataSet dataSet ) : base( dataSet )
        {
            MapFromDataSet();
        }

        public override void MapFromDataSet()
        {
            UsNumber = Row["UsNumber"].ValueOrDefault<string>();
            FacultyImage = Row["FacultyImage"].ValueOrDefault<byte[]>();
        }
    }
}
