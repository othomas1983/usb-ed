﻿using System.Data;
using FacultyManagement.Domain.Common;
using FacultyManagement.Domain.ExtensionMethods;

namespace FacultyManagement.Domain.Model.PersonalInformation
{
    public class PersonalInformation : DataSetModel
    {
        #region Properties
        public string Biography { get; private set; }
        public string Title { get; private set; }
        public string Initials { get; private set; }
        public string Name { get; private set; }
        //public string GivenName { get; private set; }
        public string Surname { get; private set; }
        public string Gender { get; private set; }
        public string Nationality { get; private set; }
        public int? NationalityId { get; private set; }
        public string BusinessContactNo { get; private set; }
        public string EmailAddress { get; private set; }
        public string Position { get; private set; }
        public string AcademicRank { get; private set; }
        public string HighestQualification { get; private set; }
        public int? HighestQualificationID { get; private set; }
        public int? MainActivityId { get; private set; }
        public string MainActivity { get; private set; }        
        public string BusinessUnit { get; private set; }
        public string Expertise { get; private set; }
        public string ResearchInterest { get; private set; }
        public string HomeInstitution { get; private set; }
        public string OrganisationalResponsibility { get; private set; }
        public int CVid { get; private set; }
        public int? AffiliationId { get; private set; }
        public string SamAccountName { get; private set; }
        public int? ApplicationUserRoleId { get; private set; }
        public string ApplicationUserRole { get; private set; }
        public int? IsFaculty { get; private set; }
        public string EmployeeId { get; private set; }

        public string Classification { get; private set; }
        public string Sufficiency { get; private set; }
        public int? GoogleScholarIndex { get; set; }
        public bool? WebDisplay { get; set; }
        #endregion

        public PersonalInformation( DataSet dataSet ) : base( dataSet )
        {
            MapFromDataSet();
        }

        /// <summary>
        /// Maps data entity to domain model
        /// </summary>
        public override void MapFromDataSet()
        {
            if ( Row == null ) return;

            Biography = Row["Biography"].ValueOrDefault<string>();
            Title = Row["Title"].ValueOrDefault<string>();
            Initials = Row["Initials"].ValueOrDefault<string>();
            Name = Row["Name"].ValueOrDefault<string>(); ;
            //GivenName = Row["GivenName"].ValueOrDefault<string>(); ;
            Surname = Row["Surname"].ValueOrDefault<string>();
            Gender = Row["Gender"].ValueOrDefault<string>();
            Nationality = Row["Nationality"].ValueOrDefault<string>();
            NationalityId = Row["NationalityID"].ValueOrDefault<int>();
            BusinessContactNo = Row["BusinessContactNo"].ValueOrDefault<string>();
            EmailAddress = Row["EmailAddress"].ValueOrDefault<string>();
            Position = Row["Position"].ValueOrDefault<string>();
            AcademicRank = Row["AcademicRank"].ValueOrDefault<string>();
            HighestQualification = Row["HighestQualification"].ValueOrDefault<string>();
            HighestQualificationID = Row["HighestQualificationID"].ValueOrDefault<int>();
            MainActivityId = Row["MainActivityID"].ValueOrDefault<int>();           
            BusinessUnit = Row["BusinessUnit"].ValueOrDefault<string>();
            MainActivity = Row["MainActivity"].ValueOrDefault<string>();
            Expertise = Row["Expertise"].ValueOrDefault<string>();
            ResearchInterest = Row["ResearchInterest"].ValueOrDefault<string>();
            HomeInstitution = Row["HomeInstitution"].ValueOrDefault<string>();
            OrganisationalResponsibility = Row["OrganisationalResponsibility"].ValueOrDefault<string>();
            CVid = Row["CVID"].ValueOrDefault<int>();
            AffiliationId = Row["AffiliationID"].ValueOrDefault<int>();
            SamAccountName = Row["Samaccountname"].ValueOrDefault<string>();
            ApplicationUserRoleId = Row["ApplicationUserRoleID"].ValueOrDefault<int>();
            IsFaculty = Row["isFaculty"].ValueOrDefault<int>();
            EmployeeId = Row["EmployeeId"].ValueOrDefault<string>();

            Classification = Row["Classification"].ValueOrDefault<string>();
            Sufficiency = Row["Sufficiency"].ValueOrDefault<string>();

            GoogleScholarIndex = Row["GoogleScholarHIndex"].ValueOrDefault<int?>();
            WebDisplay = Row["WebDisplay"].ValueOrDefault<bool?>();
        }
    }
}
