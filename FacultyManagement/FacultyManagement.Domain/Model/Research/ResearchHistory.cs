﻿using FacultyManagement.Domain.Common;
using FacultyManagement.Domain.ExtensionMethods;
using System;
using System.Data;
using System.Web;

namespace FacultyManagement.Domain.Model.Research
{
    public class ResearchHistory : DataRowModel
    {
        /// <summary>
        /// Gets the id for the research category
        /// </summary>
        public const int ResearchCategoryId = 5; //TODO: replace with a key lookup as this is mapped to an auto id in the DB ...

        #region Properties

        public int ResearchId { get; private set; }

        public int ResearchStatusId { get; private set; }

        public DateTime? ResearchStatusDate { get; private set; }

        public string ResearchStatusDescription { get; private set; }

        public int ResearchOutputCategoryId { get; private set; }

        public string ResearchOutputCategoryDescription { get; private set; }

        public int ABSRatingStatusId { get; private set; }

        public string ABSRatingStatusDescription { get; private set; }

        public int DHETStatusId { get; private set; }

        public string DHETStatusDescription { get; private set; }

        public int LocusId { get; private set; }

        public string LocusDescription { get; private set; }

        public string ArticleTitle { get; private set; }

        public string PublicationName { get; private set; }

        public DateTime? PublicationDate { get; private set; }

        public int Credit { get; private set; }

        public int ResearchContributionTypeId { get; private set; }

        public string PublisherName { get; private set; }

        public int NoOfPages { get; private set; }

        public int StartPage { get; private set; }

        public int EndPage { get; private set; }

        public int Volume { get; private set; }

        public string Edition { get; private set; }

        public string Issue { get; private set; }

        public string PublicationCity { get; private set; }
        public int CVid { set; private get; }

        public string Note { get; private set; }

        public string Country { get; private set; }

        public string PublicationFile
        {
            get; set;

        }

        #endregion

        public ResearchHistory()
        {
        }

        public ResearchHistory(DataRow row)
        {
            MapFromDataRow(row);
        }

        public override void MapFromDataRow(DataRow row)
        {
            Id = row["ResearchHistoryId"].ValueOrDefault<int>();
            Description = row["Description"].ValueOrDefault<string>();
            ResearchId = row["ResearchId"].ValueOrDefault<int>();
            CVid = row["ResearchCVID"].ValueOrDefault<int>();

            ResearchStatusId = row["ResearchStatusId"].ValueOrDefault<int>();
            ResearchStatusDate = row["ResearchStatusDate"].ValueOrDefault<DateTime?>();
            ResearchStatusDescription = row["ResearchStatusDescription"].ValueOrDefault<string>();

            ResearchOutputCategoryId = row["ResearchOutputCategoryId"].ValueOrDefault<int>();
            ResearchOutputCategoryDescription = row["ResearchOutputCategoryDescription"].ValueOrDefault<string>();

            ABSRatingStatusId = row["ABSRatingStatusId"].ValueOrDefault<int>();
            ABSRatingStatusDescription = row["ABSRatingStatusDescription"].ValueOrDefault<string>();

            DHETStatusId = row["DHETStatusId"].ValueOrDefault<int>();
            DHETStatusDescription = row["DHETStatusDescription"].ValueOrDefault<string>();

            LocusId = row["LocusId"].ValueOrDefault<int>();
            LocusDescription = row["LocusDescription"].ValueOrDefault<string>();

            ArticleTitle = row["ArticleTitle"].ValueOrDefault<string>();
            PublicationName = row["PublicationName"].ValueOrDefault<string>();
            PublicationDate = row["PublicationDate"].ValueOrDefault<DateTime?>();

            Credit = row["Credit"].ValueOrDefault<int>();
            ResearchContributionTypeId = row["ResearchContributionTypeId"].ValueOrDefault<int>();

            Note = row["Note"].ValueOrDefault<string>();
            PublisherName = row["PublisherName"].ValueOrDefault<string>();
            NoOfPages = row["NoOfPages"].ValueOrDefault<int>();
            StartPage = row["StartPage"].ValueOrDefault<int>();
            EndPage = row["EndPage"].ValueOrDefault<int>();
            Issue = row["Issue"].ValueOrDefault<string>();
            Country = row["Country"].ValueOrDefault<string>();
            Edition = row["Edition"].ValueOrDefault<string>();
            PublicationCity = row["PublicationCity"].ValueOrDefault<string>();
            Volume = row["Volume"].ValueOrDefault<int>();
            PublicationFile = row["PublicationFile"].ValueOrDefault<string>();
           
            


        }
    }
}
