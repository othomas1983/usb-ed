﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Domain.Common;
using FacultyManagement.Domain.ExtensionMethods;

namespace FacultyManagement.Domain.Model.Research
{
    public class ResearchActivity : DataRowModel
    {
        #region Properties

        public int ResearchId { get; private set; }
        public int ResearchStatusId { get; private set; }
        public DateTime? ResearchStatusDate { get; private set; }
        public string ResearchStatusDescription { get; private set; }

        public int ResearchOutputCategoryId { get; private set; }        
        public string ResearchOutputCategoryDescription { get; private set; }

        public int ABSRatingStatusId { get; private set; }
        public string ABSRatingStatusDescription { get; private set; }

        public int DHETStatusId { get; private set; }
        public string DHETStatusDescription { get; private set; }

        public int LocusId { get; private set; }
        public string LocusDescription { get; private set; }

        public string ArticleTitle { get; private set; }
        public string PublicationName { get; private set; }
        public DateTime? PublicationDate { get; private set; }

        public int Credit { get; private set; }
        public int ResearchContributionTypeId { get; private set; }


        public int CVid { set; private get; }

        #endregion

        public ResearchActivity()
        {
        }

        public ResearchActivity( DataRow row )
        {
            MapFromDataRow( row );
        }

        public override void MapFromDataRow( DataRow row )
        {
            Id = row["ResearchHistoryId"].ValueOrDefault<int>();
            Description = row["Description"].ValueOrDefault<string>();
            ResearchId = row["ResearchId"].ValueOrDefault<int>();
            CVid = row["ResearchCVID"].ValueOrDefault<int>();

            ResearchStatusId = row["ResearchStatusId"].ValueOrDefault<int>();
            ResearchStatusDate = row["ResearchStatusDate"].ValueOrDefault<DateTime?>();
            ResearchStatusDescription = row["ResearchStatusDescription"].ValueOrDefault<string>();

            ResearchOutputCategoryId = row["ResearchOutputCategoryId"].ValueOrDefault<int>();
            ResearchOutputCategoryDescription = row["ResearchOutputCategoryDescription"].ValueOrDefault<string>();

            ABSRatingStatusId = row["DHETStatusId"].ValueOrDefault<int>();
            ABSRatingStatusDescription = row["ABSRatingStatusDescription"].ValueOrDefault<string>();

            DHETStatusId = row["ABSRatingStatusId"].ValueOrDefault<int>();
            DHETStatusDescription = row["DHETStatusDescription"].ValueOrDefault<string>();

            LocusId = row["LocusId"].ValueOrDefault<int>();
            LocusDescription = row["LocusDescription"].ValueOrDefault<string>();

            ArticleTitle = row["ArticleTitle"].ValueOrDefault<string>();
            PublicationName = row["PublicationName"].ValueOrDefault<string>();
            PublicationDate = row["PublicationDate"].ValueOrDefault<DateTime?>();

            Credit = row["Credit"].ValueOrDefault<int>();
            ResearchContributionTypeId = row["ResearchContributionTypeId"].ValueOrDefault<int>();
        }
    }
}
