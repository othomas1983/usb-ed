﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Domain.Common;
using FacultyManagement.Domain.ExtensionMethods;

namespace FacultyManagement.Domain.Model.Research
{
    public class Locus : DataRowModel
    {
        public int Credit { get; private set; }

        public Locus()
        {
        }

        public Locus( DataRow row )
        {
            MapFromDataRow( row );
        }

        public override void MapFromDataRow( DataRow row )
        {
            Id = row["LocusId"].ValueOrDefault<int>();            
            Description = row["LocusDescription"].ValueOrDefault<string>();            
        }
    }
}
