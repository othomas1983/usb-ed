﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Domain.Common;
using FacultyManagement.Domain.ExtensionMethods;

namespace FacultyManagement.Domain.Model.Research
{
    public class ResearchOutputCategory : DataRowModel
    {
        public ResearchOutputCategory()
        {
        }

        public ResearchOutputCategory( DataRow row )
        {
            MapFromDataRow( row );
        }

        public override void MapFromDataRow( DataRow row )
        {
            Id = row["ResearchOutputCategoryId"].ValueOrDefault<int>();
            Description = row["ResearchOutputCategoryDescription"].ValueOrDefault<string>();
        }
    }
}
