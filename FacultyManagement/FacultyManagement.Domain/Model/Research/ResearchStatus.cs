﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Domain.Common;
using FacultyManagement.Domain.ExtensionMethods;

namespace FacultyManagement.Domain.Model.Research
{
    public class ResearchStatus : DataRowModel
    {
        public ResearchStatus()
        {
        }

        public ResearchStatus( DataRow row )
        {
            MapFromDataRow( row );
        }

        public override void MapFromDataRow( DataRow row )
        {
            Id = row["ResearchStatusId"].ValueOrDefault<int>();
            Description = row["ResearchStatusDescription"].ValueOrDefault<string>();
        }
    }
}
