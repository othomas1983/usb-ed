﻿using System.Data;
using FacultyManagement.Domain.Common;
using FacultyManagement.Domain.ExtensionMethods;

namespace FacultyManagement.Domain.Model.Loads.Common
{
    public class PMProgrammeOffering : DataRowModel
    {
        #region Properties

        public int ProgrammeId { get; set; }
        public int StartYear { get; set; }
        public int EndYear { get; set; }
        public string ProgrammeOfferingDescription { get; set; }

        #endregion

        public PMProgrammeOffering()
        {
        }

        public PMProgrammeOffering(DataRow row)
        {
            MapFromDataRow(row);
        }

        public override void MapFromDataRow(DataRow row)
        {
            if (row == null) return;

            Id = row["ProgrammeOfferingId"].ValueOrDefault<int>();
            Description = row["Name"].ValueOrDefault<string>();
            ProgrammeId = row["ProgrammeID"].ValueOrDefault<int>();
            StartYear = row["startyear"].ValueOrDefault<int>();
            EndYear = row["endyear"].ValueOrDefault<int>();
            ProgrammeOfferingDescription = row["Name"].ValueOrDefault<string>();
        }
    }
}
