﻿using System.Data;
using FacultyManagement.Domain.Common;
using FacultyManagement.Domain.ExtensionMethods;

namespace FacultyManagement.Domain.Model.Loads.Common
{
    public class PMSupervisionResearch : DataRowModel
    {
        #region Properties

        #endregion

        public PMSupervisionResearch()
        {
        }

        public PMSupervisionResearch( DataRow row )
        {
            MapFromDataRow( row );
        }

        public override void MapFromDataRow( DataRow row )
        {
            if ( row == null ) return;

            Id = row["ResearchSupervisionId"].ValueOrDefault<int>();
            Description = row["Name"].ValueOrDefault<string>();
        }
    }
}
