﻿using System.Data;
using FacultyManagement.Domain.Common;
using FacultyManagement.Domain.ExtensionMethods;

namespace FacultyManagement.Domain.Model.Loads.Common
{
    public class PMProgrammeOfferingModule : DataRowModel
    {
        #region Properties

        #endregion

        public PMProgrammeOfferingModule()
        {
        }
        public int ProgrammeOfferingId { get; set; }
        public string ModuleId { get; set; }
        public PMProgrammeOfferingModule(DataRow row)
        {
            MapFromDataRow(row);
        }

        public override void MapFromDataRow(DataRow row)
        {
            if (row == null) return;

            Id = row["ProgrammeOfferingModuleID"].ValueOrDefault<int>();
            Description = row["ModuleName"].ValueOrDefault<string>();
            ProgrammeOfferingId = row["ProgrammeOfferingID"].ValueOrDefault<int>();
            ModuleId = row["ModuleName"].ValueOrDefault<string>();



        }
    }
}
