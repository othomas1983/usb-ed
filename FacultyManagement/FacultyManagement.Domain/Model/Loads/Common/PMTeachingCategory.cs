﻿using System.Data;
using FacultyManagement.Domain.Common;
using FacultyManagement.Domain.ExtensionMethods;

namespace FacultyManagement.Domain.Model.Loads.Common
{
    public class PMTeachingCategory : DataRowModel
    {
        #region Properties

        #endregion

        public PMTeachingCategory()
        {
        }

        public PMTeachingCategory( DataRow row )
        {
            MapFromDataRow( row );
        }

      
        public override void MapFromDataRow( DataRow row )
        {
            if ( row == null ) return;

            Id = row["CategoryId"].ValueOrDefault<int>();
            Description = row["Name"].ValueOrDefault<string>();         
            
          
          
        }
    }
}
