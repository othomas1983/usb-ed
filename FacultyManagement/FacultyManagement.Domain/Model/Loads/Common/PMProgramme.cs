﻿using System.Data;
using FacultyManagement.Domain.Common;
using FacultyManagement.Domain.ExtensionMethods;

namespace FacultyManagement.Domain.Model.Loads.Common
{
    public class PMProgramme : DataRowModel
    {
        #region Properties

        #endregion

        public PMProgramme()
        {
        }

        public PMProgramme( DataRow row )
        {
            MapFromDataRow( row );
        }

        public override void MapFromDataRow( DataRow row )
        {
            if ( row == null ) return;

            Id = row["ProgrammeID"].ValueOrDefault<int>();
            Description = row["Name"].ValueOrDefault<string>();
        }
    }
}
