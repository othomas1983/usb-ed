﻿using System.Data;
using FacultyManagement.Domain.Common;
using FacultyManagement.Domain.ExtensionMethods;

namespace FacultyManagement.Domain.Model.Loads.Common
{
    public class Programme : DataRowModel
    {
        #region Properties

        #endregion

        public Programme()
        {
        }

        public Programme( DataRow row )
        {
            MapFromDataRow( row );
        }

        public override void MapFromDataRow( DataRow row )
        {
            if ( row == null ) return;

            Id = row["ProgrammeId"].ValueOrDefault<int>();
            Description = row["Programme"].ValueOrDefault<string>();
        }
    }
}
