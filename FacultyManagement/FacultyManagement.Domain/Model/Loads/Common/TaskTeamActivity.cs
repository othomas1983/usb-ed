﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Domain.Common;
using FacultyManagement.Domain.ExtensionMethods;

namespace FacultyManagement.Domain.Model.Loads.Common
{
    public class TaskTeamActivity : DataRowModel
    {

        public TaskTeamActivity()
        {
        }

        public TaskTeamActivity( DataRow row )
        {
            MapFromDataRow( row );
        }   

        public override void MapFromDataRow( DataRow row )
        {
            if ( row == null ) return;

            Id = row["TaskTeamActivityId"].ValueOrDefault<int>();
            Description = row["TaskTeamActivityDescription"].ValueOrDefault<string>();
        }
    }
}
