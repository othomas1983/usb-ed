﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Domain.Common;
using FacultyManagement.Domain.ExtensionMethods;

namespace FacultyManagement.Domain.Model.Admin
{
    /// <summary>
    /// Records for Faculty Department : [sp_FacultyDepartmentRead]
    /// </summary>
    public class FacultySufficiency : DataRowModel
    {
       public FacultySufficiency()
        {
        }

        public FacultySufficiency( DataRow row )
        {
            MapFromDataRow( row );
        }

        public override void MapFromDataRow( DataRow row )
        {
            Id = row["SufficiencyID"].ValueOrDefault<int>();
            Description = row["Sufficiency"].ValueOrDefault<string>();
        }
    }
}
