﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Domain.Common;
using FacultyManagement.Domain.ExtensionMethods;

namespace FacultyManagement.Domain.Model.Admin
{
    /// <summary>
    /// Records for Faculty Department : [sp_FacultyDepartmentRead]
    /// </summary>
    public class FacultyClassification : DataRowModel
    {
       public FacultyClassification()
        {
        }

        public FacultyClassification( DataRow row )
        {
            MapFromDataRow( row );
        }

        public override void MapFromDataRow( DataRow row )
        {
            Id = row["ClassificationID"].ValueOrDefault<int>();
            Description = row["Classification"].ValueOrDefault<string>();
        }
    }
}
