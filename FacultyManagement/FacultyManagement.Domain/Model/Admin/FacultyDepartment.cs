﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Domain.Common;
using FacultyManagement.Domain.ExtensionMethods;

namespace FacultyManagement.Domain.Model.Admin
{
    /// <summary>
    /// Records for Faculty Department : [sp_FacultyDepartmentRead]
    /// </summary>
    public class FacultyDepartment : DataRowModel
    {
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public int DepartmentCategoryId { get; set; }
        public string DepartmentCategoryName { get; set; }
        public int CVid { get; set; }

        public FacultyDepartment()
        {
        }

        public FacultyDepartment( DataRow row )
        {
            MapFromDataRow( row );
        }

        public override void MapFromDataRow( DataRow row )
        {
            if ( row == null ) return;

            Id = row["FacultyDepartmentID"].ValueOrDefault<int>();
            CVid = row["CVID"].ValueOrDefault<int>();

            DepartmentId = row["DepartmentId"].ValueOrDefault<int>();
            DepartmentName = row["DepartmentName"].ValueOrDefault<string>();

            DepartmentCategoryId = row["DepartmentCategoryId"].ValueOrDefault<int>();
            DepartmentCategoryName = row["DepartmentCategoryName"].ValueOrDefault<string>();
        }
    }
}
