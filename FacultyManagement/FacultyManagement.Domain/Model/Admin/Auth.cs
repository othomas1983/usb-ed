﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Domain.Common;
using FacultyManagement.Domain.ExtensionMethods;

namespace FacultyManagement.Domain.Model.Admin
{
    /// <summary>
    /// Records for Auth : [sp_AuthRead]
    /// </summary>
    public class Auth : DataRowModel
    {
        #region Properties

        public int CVid { get; private set; }
        public int ManagerCVid { get; private set; }
        public string Action { get; private set; }
        public string Feature { get; private set; }

        #endregion


        public Auth( DataRow row )
        {
            MapFromDataRow( row );
        }

        public Auth()
        {
        }


        public override void MapFromDataRow( DataRow row )
        {
            if ( row == null ) return;

            Id = row["AuthID"].ValueOrDefault<int>();
            CVid = row["UserCVID"].ValueOrDefault<int>();

            ManagerCVid = row["ManagerCVID"].ValueOrDefault<int>();
            Action = row["Action"].ValueOrDefault<string>();
            Feature = row["Feature"].ValueOrDefault<string>();

            IsActive = row["IsActive"].ValueOrDefault<int>();
        }
    }
}
