﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Domain.Common;
using FacultyManagement.Domain.ExtensionMethods;

namespace FacultyManagement.Domain.Model.Admin
{
    /// <summary>
    /// Records for Supervision : [sp_SupervisionRead]
    /// </summary>  
    public class SupervisionLoad : DataRowModel
    {
        #region Properties

       
        public int CVid { get; private set; }     
        public int? Year { get; private set; }
        public string StudentName { get; private set; }
        public int? YearDescription { get; set; }
        public string SupervisionResearch { get; set; }
        public string SupervisionResearchDescription { get; set; }

        public decimal Hours { get; private set; }  

        #endregion

        public SupervisionLoad()
        {
        }

        public SupervisionLoad( DataRow row )
        {
            MapFromDataRow( row );
        }

        public override void MapFromDataRow( DataRow row )
        {
            if ( row == null ) return;

            Id = row["SupervisionID"].ValueOrDefault<int>();
            CVid = row["SupervisorCVID"].ValueOrDefault<int>();           
            Year = row["Year"].ValueOrDefault<int?>();
            YearDescription = row["Year"].ValueOrDefault<int?>();
            StudentName = row["StudentName"].ValueOrDefault<string>();   
            Hours = row["Hours"].ValueOrDefault<decimal>();       
            YearDescription = row["Year"].ValueOrDefault<int>();
            SupervisionResearch = row["SupervisionResearch"].ValueOrDefault<string>();
            SupervisionResearchDescription = row["SupervisionResearch"].ValueOrDefault<string>();

        }
    }
}
