﻿using System;
using System.Data;
using FacultyManagement.Domain.Common;
using FacultyManagement.Domain.ExtensionMethods;

namespace FacultyManagement.Domain.Model.ProfessionalAndPersonalDevelopment
{
    public class Development : DataRowModel
    {
        #region Properties

        public DateTime StartDate { get; private set; }
        public DateTime? EndDate { get; private set; }
        public int DevelopmentCategoryId { get; private set; }
        public string Accomplishment { get; private set; }
        public string Topic { get; private set; }
        public string Institution { get; private set; }
        public string Country { get; private set; }
        public int NoOfDays { get; private set; }
        public int IsCurrent { get; private set; }

        #endregion


        public Development()
        {
        }

        public Development( DataRow row )
        {
            MapFromDataRow( row );
        }
        public override void MapFromDataRow( DataRow row )
        {
            if ( row == null ) return;

            Id = row["DevelopmentId"].ValueOrDefault<int>();
            IsActive = row["IsActive"].ValueOrDefault<int>();
            StartDate = row["StartDate"].ValueOrDefault<DateTime>();
            EndDate = row["EndDate"].ValueOrDefault<DateTime?>();
            Accomplishment = row["Accomplishment"].ValueOrDefault<string>();
            Topic = row["Topic"].ValueOrDefault<string>();
            Institution = row["Institution"].ValueOrDefault<string>();            
            Country = row["Country"].ValueOrDefault<string>();
            DevelopmentCategoryId = row["DevelopmentCategoryId"].ValueOrDefault<int>();
            NoOfDays = row["NoOfDays"].ValueOrDefault<int>();
            
            IsCurrent = row["IsCurrent"].ValueOrDefault<int>();
        }
    }
}
