﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Domain.Common;
using FacultyManagement.Domain.ExtensionMethods;

namespace FacultyManagement.Domain.Model.EducationAndAwards
{
    public class Award : DataRowModel
    {
        #region Properties

        public int Year { get; private set; }
        public string AwardType { get; private set; }
        public string Country { get; private set; }

        #endregion

        public Award()
        {
        }

        public Award( DataRow row )
        {
            MapFromDataRow( row );
        }

        public override void MapFromDataRow( DataRow row )
        {
            if ( row == null ) return;

            Id = row["AwardId"].ValueOrDefault<int>();
            Year = row["Year"].ValueOrDefault<int>();
            AwardType = row["AwardType"].ValueOrDefault<string>();
            Description = row["AwardDescription"].ValueOrDefault<string>();
            Country = row["Country"].ValueOrDefault<string>();
            IsActive = row["IsActive"].ValueOrDefault<int>();
        }
    }
}
