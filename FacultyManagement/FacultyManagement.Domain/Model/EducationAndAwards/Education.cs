﻿using System.Data;
using FacultyManagement.Domain.Common;
using FacultyManagement.Domain.ExtensionMethods;

namespace FacultyManagement.Domain.Model.EducationAndAwards
{
    public class Education : DataRowModel
    {
        #region Properties

        public int Year { get; private set; }
        public string Degree { get; private set; }
        public string FieldOfStudy { get; private set; }
        public string University { get; private set; }
        public string Country { get; private set; }

        #endregion

        public Education()
        {
        }
        public Education( DataRow row )
        {
            MapFromDataRow( row );
        }

        public override void MapFromDataRow( DataRow row )
        {
            if ( row == null ) return;

            Id = row["EducationId"].ValueOrDefault<int>();
            Year = row["Year"].ValueOrDefault<int>();
            Degree = row["Degree"].ValueOrDefault<string>();
            FieldOfStudy = row["FieldOfStudy"].ValueOrDefault<string>();
            University = row["University"].ValueOrDefault<string>();
            Country = row["Country"].ValueOrDefault<string>();
        }
    }
}
