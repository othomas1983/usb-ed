﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Domain.Common;
using FacultyManagement.Domain.ExtensionMethods;

namespace FacultyManagement.Domain.Model.EducationAndAwards
{
    public class Grant : DataRowModel
    {
        #region Properties

        public int Year { get; private set; }
        public string GrantFunding { get; private set; }
        public string Country { get; private set; }

        #endregion

        public Grant()
        {
        }

        public Grant( DataRow row )
        {
            MapFromDataRow( row );
        }

        public override void MapFromDataRow( DataRow row )
        {
            if ( row == null ) return;

            Id = row["GrantId"].ValueOrDefault<int>();
            Year = row["Year"].ValueOrDefault<int>();
            GrantFunding = row["GrantFunding"].ValueOrDefault<string>();
            Description = row["Description"].ValueOrDefault<string>();
            Country = row["Country"].ValueOrDefault<string>();
        }
    }
}
