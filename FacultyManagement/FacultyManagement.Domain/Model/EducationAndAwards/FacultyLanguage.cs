﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FacultyManagement.Domain.Common;
using FacultyManagement.Domain.ExtensionMethods;
using FacultyManagement.Domain.Model.Common;

namespace FacultyManagement.Domain.Model.EducationAndAwards
{
    public class FacultyLanguage : DataRowModel
    {
        #region Properties

        public int? LanguageId { get; private set; }

        #endregion

        public FacultyLanguage()
        {
        }

        public FacultyLanguage( DataRow row )
        {
            MapFromDataRow( row );
        }

        public override void MapFromDataRow( DataRow row )
        {
            if ( row == null ) return;

            Id = row["FacultyLanguageId"].ValueOrDefault<int>();
            Description = row["Language"].ValueOrDefault<string>();
            LanguageId = row["LanguageId"].ValueOrDefault<int>();
        }
    }
}
