﻿using System.Data;
using FacultyManagement.Domain.Common;
using FacultyManagement.Domain.ExtensionMethods;

namespace FacultyManagement.Domain.Model.Common
{
    public class Degree : DataRowModel
    {
        public Degree()
        {
        }

        public Degree( DataRow row )
        {
            MapFromDataRow( row );
        }

        public override void MapFromDataRow( DataRow row )
        {
            Id = row["DegreeId"].ValueOrDefault<int>();
            Description = row["DegreeDescription"].ValueOrDefault<string>();
        }
    }
}
