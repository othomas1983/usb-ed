﻿using System.Data;
using FacultyManagement.Domain.Common;
using FacultyManagement.Domain.ExtensionMethods;

namespace FacultyManagement.Domain.Model.Common
{
    public class MainActivity : DataRowModel
    {

        public MainActivity()
        {
        }
        public MainActivity( DataRow row )
        {
            MapFromDataRow( row );
        }

        public override void MapFromDataRow( DataRow row )
        {
            Id = row["MainActivityId"].ValueOrDefault<int>();
            Description = row["MainActivity"].ValueOrDefault<string>();
        }
    }
}
