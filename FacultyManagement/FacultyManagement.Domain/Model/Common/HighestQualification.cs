﻿using System.Data;
using FacultyManagement.Domain.Common;
using FacultyManagement.Domain.ExtensionMethods;

namespace FacultyManagement.Domain.Model.Common
{
    public class HighestQualification : DataRowModel
    {

        public HighestQualification()
        {
        }

        public HighestQualification( DataRow row )
        {
            MapFromDataRow( row );
        }

        public override void MapFromDataRow( DataRow row )
        {
            Id = row["HighestQualificationId"].ValueOrDefault<int>();
            Description = row["HighestQualification"].ValueOrDefault<string>();
        }
    }
}
