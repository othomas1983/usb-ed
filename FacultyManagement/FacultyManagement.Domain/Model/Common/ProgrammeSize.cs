﻿using System.Data;
using FacultyManagement.Domain.Common;
using FacultyManagement.Domain.ExtensionMethods;

namespace FacultyManagement.Domain.Model.Common
{
    public class ProgrammeSize : DataRowModel
    {
        public ProgrammeSize()
        {
        }

        public ProgrammeSize( DataRow row )
        {
            MapFromDataRow( row );
        }

        public override void MapFromDataRow( DataRow row )
        {
            Id = row["ProgrammeSizeId"].ValueOrDefault<int>();
            Description = row["Name"].ValueOrDefault<string>();
        }
    }
}
