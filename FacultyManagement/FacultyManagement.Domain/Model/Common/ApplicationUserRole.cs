﻿using System.Data;
using FacultyManagement.Domain.Common;
using FacultyManagement.Domain.ExtensionMethods;

namespace FacultyManagement.Domain.Model.Common
{
    public class ApplicationUserRole : DataRowModel
    {
        public ApplicationUserRole()
        {
        }

        public ApplicationUserRole( DataRow row )
        {
            MapFromDataRow( row );
        }

        public override void MapFromDataRow( DataRow row )
        {
            Id = row["ApplicationUserRoleId"].ValueOrDefault<int>();
            Description = row["ApplicationUserRole"].ValueOrDefault<string>();
        }
    }
}
