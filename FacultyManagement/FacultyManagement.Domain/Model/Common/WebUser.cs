﻿using System.Data;
using FacultyManagement.Domain.Common;
using FacultyManagement.Domain.ExtensionMethods;

namespace FacultyManagement.Domain.Model.Common
{
    public class WebUser : DataRowModel
    {
        #region Properties
        public int CVID { get; set; }
        public string Name { get; set; }
        public string Surname { get;  set; }
        public bool WebDisplay { get; set; }
        #endregion

        public WebUser()
        {
        }

        public WebUser( DataRow row )
        {
            MapFromDataRow( row );
        }

        public override void MapFromDataRow( DataRow row )
        {
            if ( row == null ) return;

            CVID = row["CVID"].ValueOrDefault<int>();
            Name = row["Name"].ValueOrDefault<string>(); ;
            Surname = row["Surname"].ValueOrDefault<string>();           
            WebDisplay = row["WebDisplay"].ValueOrDefault<bool>();
        }

        /// <summary>
        /// Gets the full name of the faculty user.
        /// </summary>
        /// <returns></returns>
        public string GetFullName()
        {
            return $"{Surname}, {Name}";
        }
    }
}
