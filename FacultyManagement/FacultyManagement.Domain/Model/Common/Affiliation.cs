﻿using System.Data;
using FacultyManagement.Domain.Common;
using FacultyManagement.Domain.ExtensionMethods;

namespace FacultyManagement.Domain.Model.Common
{
    public class Affiliation : DataRowModel
    {
        public Affiliation()
        {
        }

        public Affiliation( DataRow row )
        {
            MapFromDataRow( row );
        }

        public override void MapFromDataRow( DataRow row )
        {
            Id = row["AffiliationId"].ValueOrDefault<int>();
            Description = row["AffiliationDescription"].ValueOrDefault<string>();
        }
    }
}
