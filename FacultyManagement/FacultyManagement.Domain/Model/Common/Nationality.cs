﻿using System.Data;
using FacultyManagement.Domain.Common;
using FacultyManagement.Domain.ExtensionMethods;

namespace FacultyManagement.Domain.Model.Common
{
    public class Nationality : DataRowModel
    {
        public Nationality()
        {
        }

        public Nationality( DataRow row )
        {
            MapFromDataRow( row );
        }

        public override void MapFromDataRow( DataRow row )
        {
            Id = row["NationalityId"].ValueOrDefault<int>();
            Description = row["Nationality"].ValueOrDefault<string>();            
        }
    }
}
