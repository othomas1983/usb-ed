﻿using System.Data;
using FacultyManagement.Domain.Common;
using FacultyManagement.Domain.ExtensionMethods;

namespace FacultyManagement.Domain.Model.Common
{
    /// <summary>
    /// List of BusinessUnit Types. no SP. 
    /// </summary>    
    public class BusinessUnit : DataRowModel
    {
        public BusinessUnit()
        {
        }

        public BusinessUnit( DataRow row )
        {
            MapFromDataRow( row );
        }

        public override void MapFromDataRow( DataRow row )
        {
            Id = row["BusinessUnitId"].ValueOrDefault<int>();
            Description = row["BusinessUnitName"].ValueOrDefault<string>();
        }
    }
}
