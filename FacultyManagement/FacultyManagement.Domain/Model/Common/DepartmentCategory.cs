﻿using System.Data;
using FacultyManagement.Domain.Common;
using FacultyManagement.Domain.ExtensionMethods;

namespace FacultyManagement.Domain.Model.Common
{
    /// <summary>
    /// Maps department categories to a department.
    /// SP: sp_DepartmentCategoryRead
    /// </summary>
    /// <seealso cref="FacultyManagement.Domain.Common.DataRowModel" />
    public class DepartmentCategory : DataRowModel
    {
        public int DepartmentId { get; set; }

        public DepartmentCategory()
        {
        }

        public DepartmentCategory( DataRow row )
        {
            MapFromDataRow( row );
        }

        public override void MapFromDataRow( DataRow row )
        {
            Id = row["DepartmentCategoryId"].ValueOrDefault<int>();
            Description = row["DepartmentCategoryName"].ValueOrDefault<string>();

            DepartmentId = row["DepartmentId"].ValueOrDefault<int>();
        }
    }
}
