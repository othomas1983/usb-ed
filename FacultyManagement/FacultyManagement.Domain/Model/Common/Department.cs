﻿using System.Data;
using FacultyManagement.Domain.Common;
using FacultyManagement.Domain.ExtensionMethods;

namespace FacultyManagement.Domain.Model.Common
{
    /// <summary>
    /// List of Departments Types. SP: sp_DepartmentRead
    /// </summary>    
    public class Department : DataRowModel
    {
        public Department()
        {
        }

        public Department( DataRow row )
        {
            MapFromDataRow( row );
        }

        public override void MapFromDataRow( DataRow row )
        {
            Id = row["DepartmentId"].ValueOrDefault<int>();
            Description = row["DepartmentName"].ValueOrDefault<string>();
        }
    }
}
