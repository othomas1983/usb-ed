﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FacultyManagement.Domain.Model.Enums
{
    /// <summary>
    /// Enum value must match ExperienceCategoryID in database
    /// </summary>
    public enum ExperienceCategory
    {
        Teaching = 1,
        ExecutiveEducation = 2,
        FullTime = 3,
        PartTime = 5,
        PermanentProfCareer = 6,
        EngagementWithBusinessAndSociety = 7,
        EditorialBoardmembers = 8,
        EngagementWithMedia = 9
    }
}
