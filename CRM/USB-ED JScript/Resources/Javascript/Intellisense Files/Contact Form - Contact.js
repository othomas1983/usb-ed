/// <reference path="Xrm.js" />

var EntityLogicalName = "contact";
var Form_1fed44d1_ae68_4a41_bd2b_f13acac4acfa_Properties = {
    address1_composite: "address1_composite",
    birthdate: "birthdate",
    donotbulkemail: "donotbulkemail",
    donotemail: "donotemail",
    donotfax: "donotfax",
    donotphone: "donotphone",
    donotpostalmail: "donotpostalmail",
    donotsendmm: "donotsendmm",
    emailaddress1: "emailaddress1",
    fullname: "fullname",
    governmentid: "governmentid",
    lastusedincampaign: "lastusedincampaign",
    mobilephone: "mobilephone",
    originatingleadid: "originatingleadid",
    ownerid: "ownerid",
    preferredcontactmethodcode: "preferredcontactmethodcode",
    stb_correspondencelanguagelookup: "stb_correspondencelanguagelookup",
    stb_disabilitylookup: "stb_disabilitylookup",
    stb_doyouuseawheelchair: "stb_doyouuseawheelchair",
    stb_ethnicitylookup: "stb_ethnicitylookup",
    stb_firstyearofregistration: "stb_firstyearofregistration",
    stb_foreignid: "stb_foreignid",
    stb_foreignidexpirydate: "stb_foreignidexpirydate",
    stb_genderlookup: "stb_genderlookup",
    stb_givenname: "stb_givenname",
    stb_homelanguage_languagelookup: "stb_homelanguage_languagelookup",
    stb_idtypelookup: "stb_idtypelookup",
    stb_initials: "stb_initials",
    stb_maidenname: "stb_maidenname",
    stb_maritalstatuslookup: "stb_maritalstatuslookup",
    stb_nationalitylookup: "stb_nationalitylookup",
    stb_otherdietaryrequirements: "stb_otherdietaryrequirements",
    stb_passportexpirydate: "stb_passportexpirydate",
    stb_passportnumber: "stb_passportnumber",
    stb_specialdietaryrequirements: "stb_specialdietaryrequirements",
    stb_titlelookup: "stb_titlelookup",
    stb_usnumber: "stb_usnumber",
    telephone1: "telephone1"
};

var Form_1fed44d1_ae68_4a41_bd2b_f13acac4acfa_Controls = {
    address1_composite: "address1_composite",
    birthdate: "birthdate",
    contactcasessgrid: "contactcasessgrid",
    contactopportunitiesgrid: "contactopportunitiesgrid",
    donotbulkemail: "donotbulkemail",
    donotemail: "donotemail",
    donotfax: "donotfax",
    donotphone: "donotphone",
    donotpostalmail: "donotpostalmail",
    donotsendmm: "donotsendmm",
    emailaddress1: "emailaddress1",
    fullname: "fullname",
    governmentid: "governmentid",
    header_ownerid: "header_ownerid",
    header_stb_usnumber: "header_stb_usnumber",
    lastusedincampaign: "lastusedincampaign",
    mapcontrol: "mapcontrol",
    mobilephone: "mobilephone",
    notescontrol: "notescontrol",
    originatingleadid: "originatingleadid",
    preferredcontactmethodcode: "preferredcontactmethodcode",
    stb_correspondencelanguagelookup: "stb_correspondencelanguagelookup",
    stb_disabilitylookup: "stb_disabilitylookup",
    stb_doyouuseawheelchair: "stb_doyouuseawheelchair",
    stb_ethnicitylookup: "stb_ethnicitylookup",
    stb_firstyearofregistration: "stb_firstyearofregistration",
    stb_foreignid: "stb_foreignid",
    stb_foreignidexpirydate: "stb_foreignidexpirydate",
    stb_genderlookup: "stb_genderlookup",
    stb_givenname: "stb_givenname",
    stb_homelanguage_languagelookup: "stb_homelanguage_languagelookup",
    stb_idtypelookup: "stb_idtypelookup",
    stb_initials: "stb_initials",
    stb_maidenname: "stb_maidenname",
    stb_maritalstatuslookup: "stb_maritalstatuslookup",
    stb_nationalitylookup: "stb_nationalitylookup",
    stb_otherdietaryrequirements: "stb_otherdietaryrequirements",
    stb_passportexpirydate: "stb_passportexpirydate",
    stb_passportnumber: "stb_passportnumber",
    stb_specialdietaryrequirements: "stb_specialdietaryrequirements",
    stb_titlelookup: "stb_titlelookup",
    telephone1: "telephone1"
};

var pageData = {
    "Event": "none",
    "SaveMode": 1,
    "EventSource": null,
    "AuthenticationHeader": "",
    "CurrentTheme": "Default",
    "OrgLcid": 1033,
    "OrgUniqueName": "",
    "QueryStringParameters": {
        "_gridType": "2",
        "etc": "2",
        "id": "",
        "pagemode": "iframe",
        "preloadcache": "1344548892170",
        "rskey": "141637534"
    },
    "ServerUrl": "",
    "UserId": "",
    "UserLcid": 1033,
    "UserRoles": [""],
    "isOutlookClient": false,
    "isOutlookOnline": true,
    "DataXml": "",
    "EntityName": "contact",
    "Id": "",
    "IsDirty": false,
    "CurrentControl": "",
    "CurrentForm": null,
    "Forms": [],
    "FormType": 2,
    "ViewPortHeight": 558,
    "ViewPortWidth": 1231,
    "Attributes": [{
        "Name": "address1_composite",
        "Value": "",
        "Type": "memo",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": false,
            "canCreate": false
        },
        "MaxLength": 1000,
        "Controls": [{
            "Name": "address1_composite"
        }]
    },
    {
        "Name": "birthdate",
        "Value": null,
        "Type": "datetime",
        "Format": "date",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Controls": [{
            "Name": "birthdate"
        }]
    },
    {
        "Name": "donotbulkemail",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "donotbulkemail"
        }]
    },
    {
        "Name": "donotemail",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "donotemail"
        }]
    },
    {
        "Name": "donotfax",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "donotfax"
        }]
    },
    {
        "Name": "donotphone",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "donotphone"
        }]
    },
    {
        "Name": "donotpostalmail",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "donotpostalmail"
        }]
    },
    {
        "Name": "donotsendmm",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "donotsendmm"
        }]
    },
    {
        "Name": "emailaddress1",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "emailaddress1"
        }]
    },
    {
        "Name": "fullname",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": false,
            "canCreate": false
        },
        "MaxLength": 160,
        "Controls": [{
            "Name": "fullname"
        }]
    },
    {
        "Name": "governmentid",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 50,
        "Controls": [{
            "Name": "governmentid"
        }]
    },
    {
        "Name": "lastusedincampaign",
        "Value": null,
        "Type": "datetime",
        "Format": "date",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": false
        },
        "Controls": [{
            "Name": "lastusedincampaign"
        }]
    },
    {
        "Name": "mobilephone",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 50,
        "Controls": [{
            "Name": "mobilephone"
        }]
    },
    {
        "Name": "originatingleadid",
        "Value": [{
            "entityType": "Unknown",
            "id": "{27ACAE0A-5D79-E111-BBD3-2657AEB3167B}",
            "name": "Temp"
        }],
        "Type": "lookup",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": false,
            "canCreate": true
        },
        "Controls": [{
            "Name": "originatingleadid"
        }]
    },
    {
        "Name": "ownerid",
        "Value": [{
            "entityType": "Unknown",
            "id": "{27ACAE0A-5D79-E111-BBD3-2657AEB3167B}",
            "name": "Temp"
        }],
        "Type": "lookup",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": false,
            "canCreate": true
        },
        "Controls": [{
            "Name": "header_ownerid"
        }]
    },
    {
        "Name": "preferredcontactmethodcode",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Any",
            "value": 1
        },
        {
            "text": "Email",
            "value": 2
        },
        {
            "text": "Phone",
            "value": 3
        },
        {
            "text": "Fax",
            "value": 4
        },
        {
            "text": "Mail",
            "value": 5
        }],
        "SelectedOption": {
            "option": "Any",
            "value": 1
        },
        "Text": "Any",
        "Controls": [{
            "Name": "preferredcontactmethodcode"
        }]
    },
    {
        "Name": "stb_correspondencelanguagelookup",
        "Value": [{
            "entityType": "Unknown",
            "id": "{27ACAE0A-5D79-E111-BBD3-2657AEB3167B}",
            "name": "Temp"
        }],
        "Type": "lookup",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Controls": [{
            "Name": "stb_correspondencelanguagelookup"
        }]
    },
    {
        "Name": "stb_disabilitylookup",
        "Value": [{
            "entityType": "Unknown",
            "id": "{27ACAE0A-5D79-E111-BBD3-2657AEB3167B}",
            "name": "Temp"
        }],
        "Type": "lookup",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Controls": [{
            "Name": "stb_disabilitylookup"
        }]
    },
    {
        "Name": "stb_doyouuseawheelchair",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "stb_doyouuseawheelchair"
        }]
    },
    {
        "Name": "stb_ethnicitylookup",
        "Value": [{
            "entityType": "Unknown",
            "id": "{27ACAE0A-5D79-E111-BBD3-2657AEB3167B}",
            "name": "Temp"
        }],
        "Type": "lookup",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Controls": [{
            "Name": "stb_ethnicitylookup"
        }]
    },
    {
        "Name": "stb_firstyearofregistration",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 4,
        "Controls": [{
            "Name": "stb_firstyearofregistration"
        }]
    },
    {
        "Name": "stb_foreignid",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 50,
        "Controls": [{
            "Name": "stb_foreignid"
        }]
    },
    {
        "Name": "stb_foreignidexpirydate",
        "Value": null,
        "Type": "datetime",
        "Format": "date",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Controls": [{
            "Name": "stb_foreignidexpirydate"
        }]
    },
    {
        "Name": "stb_genderlookup",
        "Value": [{
            "entityType": "Unknown",
            "id": "{27ACAE0A-5D79-E111-BBD3-2657AEB3167B}",
            "name": "Temp"
        }],
        "Type": "lookup",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Controls": [{
            "Name": "stb_genderlookup"
        }]
    },
    {
        "Name": "stb_givenname",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "stb_givenname"
        }]
    },
    {
        "Name": "stb_homelanguage_languagelookup",
        "Value": [{
            "entityType": "Unknown",
            "id": "{27ACAE0A-5D79-E111-BBD3-2657AEB3167B}",
            "name": "Temp"
        }],
        "Type": "lookup",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Controls": [{
            "Name": "stb_homelanguage_languagelookup"
        }]
    },
    {
        "Name": "stb_idtypelookup",
        "Value": [{
            "entityType": "Unknown",
            "id": "{27ACAE0A-5D79-E111-BBD3-2657AEB3167B}",
            "name": "Temp"
        }],
        "Type": "lookup",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Controls": [{
            "Name": "stb_idtypelookup"
        }]
    },
    {
        "Name": "stb_initials",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 10,
        "Controls": [{
            "Name": "stb_initials"
        }]
    },
    {
        "Name": "stb_maidenname",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 200,
        "Controls": [{
            "Name": "stb_maidenname"
        }]
    },
    {
        "Name": "stb_maritalstatuslookup",
        "Value": [{
            "entityType": "Unknown",
            "id": "{27ACAE0A-5D79-E111-BBD3-2657AEB3167B}",
            "name": "Temp"
        }],
        "Type": "lookup",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Controls": [{
            "Name": "stb_maritalstatuslookup"
        }]
    },
    {
        "Name": "stb_nationalitylookup",
        "Value": [{
            "entityType": "Unknown",
            "id": "{27ACAE0A-5D79-E111-BBD3-2657AEB3167B}",
            "name": "Temp"
        }],
        "Type": "lookup",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Controls": [{
            "Name": "stb_nationalitylookup"
        }]
    },
    {
        "Name": "stb_otherdietaryrequirements",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 250,
        "Controls": [{
            "Name": "stb_otherdietaryrequirements"
        }]
    },
    {
        "Name": "stb_passportexpirydate",
        "Value": null,
        "Type": "datetime",
        "Format": "date",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Controls": [{
            "Name": "stb_passportexpirydate"
        }]
    },
    {
        "Name": "stb_passportnumber",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 50,
        "Controls": [{
            "Name": "stb_passportnumber"
        }]
    },
    {
        "Name": "stb_specialdietaryrequirements",
        "Value": [{
            "entityType": "Unknown",
            "id": "{27ACAE0A-5D79-E111-BBD3-2657AEB3167B}",
            "name": "Temp"
        }],
        "Type": "lookup",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Controls": [{
            "Name": "stb_specialdietaryrequirements"
        }]
    },
    {
        "Name": "stb_titlelookup",
        "Value": [{
            "entityType": "Unknown",
            "id": "{27ACAE0A-5D79-E111-BBD3-2657AEB3167B}",
            "name": "Temp"
        }],
        "Type": "lookup",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Controls": [{
            "Name": "stb_titlelookup"
        }]
    },
    {
        "Name": "stb_usnumber",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 8,
        "Controls": [{
            "Name": "header_stb_usnumber"
        }]
    },
    {
        "Name": "telephone1",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 50,
        "Controls": [{
            "Name": "telephone1"
        }]
    }],
    "AttributesLength": 38,
    "Controls": [{
        "Name": "address1_composite",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Address",
        "Attribute": "address1_composite"
    },
    {
        "Name": "birthdate",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Birthday",
        "Attribute": "birthdate"
    },
    {
        "Name": "donotbulkemail",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Bulk Email",
        "Attribute": "donotbulkemail"
    },
    {
        "Name": "donotemail",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Email",
        "Attribute": "donotemail"
    },
    {
        "Name": "donotfax",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Fax",
        "Attribute": "donotfax"
    },
    {
        "Name": "donotphone",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Phone",
        "Attribute": "donotphone"
    },
    {
        "Name": "donotpostalmail",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Mail",
        "Attribute": "donotpostalmail"
    },
    {
        "Name": "donotsendmm",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Marketing Materials",
        "Attribute": "donotsendmm"
    },
    {
        "Name": "emailaddress1",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Email",
        "Attribute": "emailaddress1"
    },
    {
        "Name": "fullname",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Full Name",
        "Attribute": "fullname"
    },
    {
        "Name": "governmentid",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Id Number",
        "Attribute": "governmentid"
    },
    {
        "Name": "header_ownerid",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Owner",
        "Attribute": "ownerid"
    },
    {
        "Name": "header_stb_usnumber",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Student Number",
        "Attribute": "stb_usnumber"
    },
    {
        "Name": "lastusedincampaign",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Last Campaign Date",
        "Attribute": "lastusedincampaign"
    },
    {
        "Name": "mobilephone",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Mobile Phone",
        "Attribute": "mobilephone"
    },
    {
        "Name": "originatingleadid",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Originating Lead",
        "Attribute": "originatingleadid"
    },
    {
        "Name": "preferredcontactmethodcode",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Contact Method",
        "Attribute": "preferredcontactmethodcode"
    },
    {
        "Name": "stb_correspondencelanguagelookup",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Correspondence Language",
        "Attribute": "stb_correspondencelanguagelookup"
    },
    {
        "Name": "stb_disabilitylookup",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Disability",
        "Attribute": "stb_disabilitylookup"
    },
    {
        "Name": "stb_doyouuseawheelchair",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Do You Use a Wheelchair",
        "Attribute": "stb_doyouuseawheelchair"
    },
    {
        "Name": "stb_ethnicitylookup",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Ethnicity",
        "Attribute": "stb_ethnicitylookup"
    },
    {
        "Name": "stb_firstyearofregistration",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "First Year Of Registration",
        "Attribute": "stb_firstyearofregistration"
    },
    {
        "Name": "stb_foreignid",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Foreign Id",
        "Attribute": "stb_foreignid"
    },
    {
        "Name": "stb_foreignidexpirydate",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Foreign ID Expiry Date",
        "Attribute": "stb_foreignidexpirydate"
    },
    {
        "Name": "stb_genderlookup",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Gender",
        "Attribute": "stb_genderlookup"
    },
    {
        "Name": "stb_givenname",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Given Name",
        "Attribute": "stb_givenname"
    },
    {
        "Name": "stb_homelanguage_languagelookup",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Home Language",
        "Attribute": "stb_homelanguage_languagelookup"
    },
    {
        "Name": "stb_idtypelookup",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Id Type",
        "Attribute": "stb_idtypelookup"
    },
    {
        "Name": "stb_initials",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Initials",
        "Attribute": "stb_initials"
    },
    {
        "Name": "stb_maidenname",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Maiden Name",
        "Attribute": "stb_maidenname"
    },
    {
        "Name": "stb_maritalstatuslookup",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Marital Status",
        "Attribute": "stb_maritalstatuslookup"
    },
    {
        "Name": "stb_nationalitylookup",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Nationality",
        "Attribute": "stb_nationalitylookup"
    },
    {
        "Name": "stb_otherdietaryrequirements",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Other Dietary Requirements",
        "Attribute": "stb_otherdietaryrequirements"
    },
    {
        "Name": "stb_passportexpirydate",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Passport Expiry Date",
        "Attribute": "stb_passportexpirydate"
    },
    {
        "Name": "stb_passportnumber",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Passport Number",
        "Attribute": "stb_passportnumber"
    },
    {
        "Name": "stb_specialdietaryrequirements",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Special Dietary Requirements",
        "Attribute": "stb_specialdietaryrequirements"
    },
    {
        "Name": "stb_titlelookup",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Title",
        "Attribute": "stb_titlelookup"
    },
    {
        "Name": "telephone1",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Business Phone",
        "Attribute": "telephone1"
    }],
    "ControlsLength": 38,
    "Navigation": [{
        "Id": "navAddresses",
        "Key": "navAddresses",
        "Label": "",
        "Visible": true
    },
    {
        "Id": "navSubConts",
        "Key": "navSubConts",
        "Label": "",
        "Visible": true
    },
    {
        "Id": "navRelationships",
        "Key": "navRelationships",
        "Label": "",
        "Visible": true
    },
    {
        "Id": "navQuotes",
        "Key": "navQuotes",
        "Label": "",
        "Visible": true
    },
    {
        "Id": "navOrders",
        "Key": "navOrders",
        "Label": "",
        "Visible": true
    },
    {
        "Id": "navInvoices",
        "Key": "navInvoices",
        "Label": "",
        "Visible": true
    },
    {
        "Id": "navContracts",
        "Key": "navContracts",
        "Label": "",
        "Visible": true
    },
    {
        "Id": "navCampaignsInSFA",
        "Key": "navCampaignsInSFA",
        "Label": "",
        "Visible": true
    },
    {
        "Id": "navListsInSFA",
        "Key": "navListsInSFA",
        "Label": "",
        "Visible": true
    },
    {
        "Id": "navProcessSessions",
        "Key": "navProcessSessions",
        "Label": "",
        "Visible": true
    },
    {
        "Id": "navAsyncOperations",
        "Key": "navAsyncOperations",
        "Label": "",
        "Visible": true
    },
    {
        "Id": "navOpps",
        "Key": "navOpps",
        "Label": "",
        "Visible": true
    }],
    "Tabs": [{
        "Label": "Summary",
        "Name": "SUMMARY_TAB",
        "DisplayState": "expanded",
        "Visible": true,
        "Sections": [{
            "Label": "CONTACT INFORMATION",
            "Name": "CONTACT_INFORMATION",
            "Visible": true,
            "Controls": [{
                "Name": "fullname"
            },
            {
                "Name": "stb_givenname"
            },
            {
                "Name": "emailaddress1"
            },
            {
                "Name": "telephone1"
            },
            {
                "Name": "mobilephone"
            },
            {
                "Name": "address1_composite"
            }]
        },
        {
            "Label": "",
            "Name": "MapSection",
            "Visible": true,
            "Controls": [{
                "Name": "mapcontrol"
            }]
        },
        {
            "Label": "SOCIAL PANE",
            "Name": "SOCIAL_PANE_TAB",
            "Visible": true,
            "Controls": [{
                "Name": "notescontrol"
            }]
        },
        {
            "Label": "CUSTOMER DETAILS",
            "Name": "CUSTOMER_DETAILS_TAB",
            "Visible": true,
            "Controls": [{
                "Name": "contactcasessgrid"
            },
            {
                "Name": "contactopportunitiesgrid"
            }]
        }]
    },
    {
        "Label": "Details",
        "Name": "DETAILS_TAB",
        "DisplayState": "expanded",
        "Visible": true,
        "Sections": [{
            "Label": "PERSONAL",
            "Name": "PERSONAL INFORMATION",
            "Visible": true,
            "Controls": [{
                "Name": "stb_titlelookup"
            },
            {
                "Name": "stb_initials"
            },
            {
                "Name": "stb_maidenname"
            },
            {
                "Name": "stb_maritalstatuslookup"
            },
            {
                "Name": "stb_homelanguage_languagelookup"
            },
            {
                "Name": "stb_correspondencelanguagelookup"
            },
            {
                "Name": "stb_genderlookup"
            },
            {
                "Name": "stb_ethnicitylookup"
            },
            {
                "Name": "birthdate"
            }]
        },
        {
            "Label": "NATIONALITY",
            "Name": "NATIONALITY",
            "Visible": true,
            "Controls": [{
                "Name": "stb_nationalitylookup"
            },
            {
                "Name": "stb_idtypelookup"
            },
            {
                "Name": "governmentid"
            },
            {
                "Name": "stb_foreignid"
            },
            {
                "Name": "stb_foreignidexpirydate"
            },
            {
                "Name": "stb_passportnumber"
            },
            {
                "Name": "stb_passportexpirydate"
            }]
        },
        {
            "Label": "ADDITIONAL",
            "Name": "ADDITIONAL",
            "Visible": true,
            "Controls": [{
                "Name": "stb_specialdietaryrequirements"
            },
            {
                "Name": "stb_otherdietaryrequirements"
            },
            {
                "Name": "stb_firstyearofregistration"
            },
            {
                "Name": "stb_disabilitylookup"
            },
            {
                "Name": "stb_doyouuseawheelchair"
            }]
        },
        {
            "Label": "MARKETING",
            "Name": "marketing information",
            "Visible": true,
            "Controls": [{
                "Name": "originatingleadid"
            },
            {
                "Name": "lastusedincampaign"
            },
            {
                "Name": "donotsendmm"
            }]
        },
        {
            "Label": "CONTACT PREFERENCES",
            "Name": "CONTACT_PREFERENCES",
            "Visible": true,
            "Controls": [{
                "Name": "preferredcontactmethodcode"
            },
            {
                "Name": "donotemail"
            },
            {
                "Name": "donotbulkemail"
            },
            {
                "Name": "donotphone"
            },
            {
                "Name": "donotfax"
            },
            {
                "Name": "donotpostalmail"
            }]
        }]
    }]
};

var Xrm = new _xrm(pageData);