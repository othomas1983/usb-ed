/// <reference path="Xrm.js" />

var EntityLogicalName = "stb_venue";
var Form_4061baa4_5ccd_4c8f_8894_15f5e611f5cd_Properties = {
    ownerid: "ownerid",
    statecode: "statecode",
    stb_afrigislookup: "stb_afrigislookup",
    stb_buildingnumber: "stb_buildingnumber",
    stb_capacity: "stb_capacity",
    stb_citytown: "stb_citytown",
    stb_conatctemail: "stb_conatctemail",
    stb_contactnumber: "stb_contactnumber",
    stb_contactperson: "stb_contactperson",
    stb_countrylookup: "stb_countrylookup",
    stb_description: "stb_description",
    stb_postalcode: "stb_postalcode",
    stb_rates: "stb_rates",
    stb_resources: "stb_resources",
    stb_roomnumber: "stb_roomnumber",
    stb_street1: "stb_street1",
    stb_street2: "stb_street2",
    stb_street3: "stb_street3",
    stb_suburb: "stb_suburb",
    stb_venuename: "stb_venuename"
};

var Form_4061baa4_5ccd_4c8f_8894_15f5e611f5cd_Controls = {
    footer_statecode: "footer_statecode",
    ownerid: "ownerid",
    stb_afrigislookup: "stb_afrigislookup",
    stb_buildingnumber: "stb_buildingnumber",
    stb_capacity: "stb_capacity",
    stb_citytown: "stb_citytown",
    stb_conatctemail: "stb_conatctemail",
    stb_contactnumber: "stb_contactnumber",
    stb_contactperson: "stb_contactperson",
    stb_countrylookup: "stb_countrylookup",
    stb_description: "stb_description",
    stb_postalcode: "stb_postalcode",
    stb_rates: "stb_rates",
    stb_resources: "stb_resources",
    stb_roomnumber: "stb_roomnumber",
    stb_street1: "stb_street1",
    stb_street2: "stb_street2",
    stb_street3: "stb_street3",
    stb_suburb: "stb_suburb",
    stb_venuename: "stb_venuename"
};

var pageData = {
    "Event": "none",
    "SaveMode": 1,
    "EventSource": null,
    "AuthenticationHeader": "",
    "CurrentTheme": "Default",
    "OrgLcid": 1033,
    "OrgUniqueName": "",
    "QueryStringParameters": {
        "_gridType": "10021",
        "etc": "10021",
        "id": "",
        "pagemode": "iframe",
        "preloadcache": "1344548892170",
        "rskey": "141637534"
    },
    "ServerUrl": "",
    "UserId": "",
    "UserLcid": 1033,
    "UserRoles": [""],
    "isOutlookClient": false,
    "isOutlookOnline": true,
    "DataXml": "",
    "EntityName": "stb_venue",
    "Id": "",
    "IsDirty": false,
    "CurrentControl": "",
    "CurrentForm": null,
    "Forms": [],
    "FormType": 2,
    "ViewPortHeight": 558,
    "ViewPortWidth": 1231,
    "Attributes": [{
        "Name": "ownerid",
        "Value": [{
            "entityType": "Unknown",
            "id": "{27ACAE0A-5D79-E111-BBD3-2657AEB3167B}",
            "name": "Temp"
        }],
        "Type": "lookup",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": false,
            "canCreate": true
        },
        "Controls": [{
            "Name": "ownerid"
        }]
    },
    {
        "Name": "statecode",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": false,
            "canCreate": false
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Active",
            "value": 0
        },
        {
            "text": "Inactive",
            "value": 1
        }],
        "SelectedOption": {
            "option": "Active",
            "value": 0
        },
        "Text": "Active",
        "Controls": [{
            "Name": "footer_statecode"
        }]
    },
    {
        "Name": "stb_afrigislookup",
        "Value": [{
            "entityType": "Unknown",
            "id": "{27ACAE0A-5D79-E111-BBD3-2657AEB3167B}",
            "name": "Temp"
        }],
        "Type": "lookup",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Controls": [{
            "Name": "stb_afrigislookup"
        }]
    },
    {
        "Name": "stb_buildingnumber",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 10,
        "Controls": [{
            "Name": "stb_buildingnumber"
        }]
    },
    {
        "Name": "stb_capacity",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "stb_capacity"
        }]
    },
    {
        "Name": "stb_citytown",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "stb_citytown"
        }]
    },
    {
        "Name": "stb_conatctemail",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 250,
        "Controls": [{
            "Name": "stb_conatctemail"
        }]
    },
    {
        "Name": "stb_contactnumber",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "stb_contactnumber"
        }]
    },
    {
        "Name": "stb_contactperson",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "stb_contactperson"
        }]
    },
    {
        "Name": "stb_countrylookup",
        "Value": [{
            "entityType": "Unknown",
            "id": "{27ACAE0A-5D79-E111-BBD3-2657AEB3167B}",
            "name": "Temp"
        }],
        "Type": "lookup",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Controls": [{
            "Name": "stb_countrylookup"
        }]
    },
    {
        "Name": "stb_description",
        "Value": "",
        "Type": "memo",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 2000,
        "Controls": [{
            "Name": "stb_description"
        }]
    },
    {
        "Name": "stb_postalcode",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "stb_postalcode"
        }]
    },
    {
        "Name": "stb_rates",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "stb_rates"
        }]
    },
    {
        "Name": "stb_resources",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 250,
        "Controls": [{
            "Name": "stb_resources"
        }]
    },
    {
        "Name": "stb_roomnumber",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 10,
        "Controls": [{
            "Name": "stb_roomnumber"
        }]
    },
    {
        "Name": "stb_street1",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "stb_street1"
        }]
    },
    {
        "Name": "stb_street2",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "stb_street2"
        }]
    },
    {
        "Name": "stb_street3",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "stb_street3"
        }]
    },
    {
        "Name": "stb_suburb",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "stb_suburb"
        }]
    },
    {
        "Name": "stb_venuename",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 250,
        "Controls": [{
            "Name": "stb_venuename"
        }]
    }],
    "AttributesLength": 20,
    "Controls": [{
        "Name": "footer_statecode",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Status",
        "Attribute": "statecode"
    },
    {
        "Name": "ownerid",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Owner",
        "Attribute": "ownerid"
    },
    {
        "Name": "stb_afrigislookup",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Postal Code, City or Suburb",
        "Attribute": "stb_afrigislookup"
    },
    {
        "Name": "stb_buildingnumber",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Building Number",
        "Attribute": "stb_buildingnumber"
    },
    {
        "Name": "stb_capacity",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Capacity",
        "Attribute": "stb_capacity"
    },
    {
        "Name": "stb_citytown",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "City / Town",
        "Attribute": "stb_citytown"
    },
    {
        "Name": "stb_conatctemail",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Contact Email",
        "Attribute": "stb_conatctemail"
    },
    {
        "Name": "stb_contactnumber",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Contact Number",
        "Attribute": "stb_contactnumber"
    },
    {
        "Name": "stb_contactperson",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Contact Person",
        "Attribute": "stb_contactperson"
    },
    {
        "Name": "stb_countrylookup",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Country",
        "Attribute": "stb_countrylookup"
    },
    {
        "Name": "stb_description",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Description",
        "Attribute": "stb_description"
    },
    {
        "Name": "stb_postalcode",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Postal Code",
        "Attribute": "stb_postalcode"
    },
    {
        "Name": "stb_rates",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Rates",
        "Attribute": "stb_rates"
    },
    {
        "Name": "stb_resources",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Resources",
        "Attribute": "stb_resources"
    },
    {
        "Name": "stb_roomnumber",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Room Number",
        "Attribute": "stb_roomnumber"
    },
    {
        "Name": "stb_street1",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Street 1",
        "Attribute": "stb_street1"
    },
    {
        "Name": "stb_street2",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Street 2",
        "Attribute": "stb_street2"
    },
    {
        "Name": "stb_street3",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Street 3",
        "Attribute": "stb_street3"
    },
    {
        "Name": "stb_suburb",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Suburb",
        "Attribute": "stb_suburb"
    },
    {
        "Name": "stb_venuename",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Venue Name",
        "Attribute": "stb_venuename"
    }],
    "ControlsLength": 20,
    "Navigation": [{
        "Id": "navConnections",
        "Key": "navConnections",
        "Label": "",
        "Visible": true
    }],
    "Tabs": [{
        "Label": "General",
        "Name": "null",
        "DisplayState": "expanded",
        "Visible": true,
        "Sections": [{
            "Label": "Venue Information",
            "Name": "{989f6201-c032-4d7b-95d7-8dcbdfea9ae9}",
            "Visible": true,
            "Controls": [{
                "Name": "stb_venuename"
            },
            {
                "Name": "stb_contactperson"
            },
            {
                "Name": "stb_buildingnumber"
            },
            {
                "Name": "stb_contactnumber"
            },
            {
                "Name": "stb_roomnumber"
            },
            {
                "Name": "stb_conatctemail"
            },
            {
                "Name": "stb_capacity"
            },
            {
                "Name": "stb_rates"
            },
            {
                "Name": "stb_resources"
            },
            {
                "Name": "stb_description"
            }]
        }]
    },
    {
        "Label": "Postal Address",
        "Name": "Postal Address",
        "DisplayState": "expanded",
        "Visible": true,
        "Sections": [{
            "Label": "Postal Address",
            "Name": "Postal Address_section_3",
            "Visible": true,
            "Controls": [{
                "Name": "stb_countrylookup"
            },
            {
                "Name": "stb_street1"
            },
            {
                "Name": "stb_afrigislookup"
            },
            {
                "Name": "stb_street2"
            },
            {
                "Name": "stb_street3"
            },
            {
                "Name": "stb_suburb"
            },
            {
                "Name": "stb_postalcode"
            },
            {
                "Name": "stb_citytown"
            }]
        },
        {
            "Label": "Administration",
            "Name": "Postal Address_section_2",
            "Visible": true,
            "Controls": [{
                "Name": "ownerid"
            }]
        }]
    }]
};

var Xrm = new _xrm(pageData);