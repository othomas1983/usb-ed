/// <reference path="Xrm.js" />

var EntityLogicalName = "stb_shortcourseoffering";
var Form_f79d3cd6_0b4c_4a95_8d2a_695457ab7216_Properties = {
    ownerid: "ownerid",
    statecode: "statecode",
    statuscode: "statuscode",
    stb_enddate: "stb_enddate",
    stb_maxparticipants: "stb_maxparticipants",
    stb_minparticipants: "stb_minparticipants",
    stb_offeringformatoptionset: "stb_offeringformatoptionset",
    stb_offeringleveltwooptions: "stb_offeringleveltwooptions",
    stb_offeringnameafrikaans: "stb_offeringnameafrikaans",
    stb_offeringnameenglish: "stb_offeringnameenglish",
    stb_offeringoptiontwooptions: "stb_offeringoptiontwooptions",
    stb_offeringurl: "stb_offeringurl",
    stb_salespersonlookup: "stb_salespersonlookup",
    stb_shortcourselookup: "stb_shortcourselookup",
    stb_startdate: "stb_startdate",
    stb_venuelookup: "stb_venuelookup"
};

var Form_f79d3cd6_0b4c_4a95_8d2a_695457ab7216_Controls = {
    CertifiedApplicantsGrid: "CertifiedApplicantsGrid",
    footer_statecode: "footer_statecode",
    notescontrol: "notescontrol",
    ownerid: "ownerid",
    statuscode: "statuscode",
    stb_enddate: "stb_enddate",
    stb_maxparticipants: "stb_maxparticipants",
    stb_minparticipants: "stb_minparticipants",
    stb_offeringformatoptionset: "stb_offeringformatoptionset",
    stb_offeringleveltwooptions: "stb_offeringleveltwooptions",
    stb_offeringnameafrikaans: "stb_offeringnameafrikaans",
    stb_offeringnameenglish: "stb_offeringnameenglish",
    stb_offeringoptiontwooptions: "stb_offeringoptiontwooptions",
    stb_offeringurl: "stb_offeringurl",
    stb_salespersonlookup: "stb_salespersonlookup",
    stb_shortcourselookup: "stb_shortcourselookup",
    stb_startdate: "stb_startdate",
    stb_venuelookup: "stb_venuelookup"
};

var pageData = {
    "Event": "none",
    "SaveMode": 1,
    "EventSource": null,
    "AuthenticationHeader": "",
    "CurrentTheme": "Default",
    "OrgLcid": 1033,
    "OrgUniqueName": "",
    "QueryStringParameters": {
        "_gridType": "10011",
        "etc": "10011",
        "id": "",
        "pagemode": "iframe",
        "preloadcache": "1344548892170",
        "rskey": "141637534"
    },
    "ServerUrl": "",
    "UserId": "",
    "UserLcid": 1033,
    "UserRoles": [""],
    "isOutlookClient": false,
    "isOutlookOnline": true,
    "DataXml": "",
    "EntityName": "stb_shortcourseoffering",
    "Id": "",
    "IsDirty": false,
    "CurrentControl": "",
    "CurrentForm": null,
    "Forms": [],
    "FormType": 2,
    "ViewPortHeight": 558,
    "ViewPortWidth": 1231,
    "Attributes": [{
        "Name": "ownerid",
        "Value": [{
            "entityType": "Unknown",
            "id": "{27ACAE0A-5D79-E111-BBD3-2657AEB3167B}",
            "name": "Temp"
        }],
        "Type": "lookup",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": false,
            "canCreate": true
        },
        "Controls": [{
            "Name": "ownerid"
        }]
    },
    {
        "Name": "statecode",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": false,
            "canCreate": false
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Active",
            "value": 0
        },
        {
            "text": "Inactive",
            "value": 1
        }],
        "SelectedOption": {
            "option": "Active",
            "value": 0
        },
        "Text": "Active",
        "Controls": [{
            "Name": "footer_statecode"
        }]
    },
    {
        "Name": "statuscode",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "New",
            "value": 956390000
        },
        {
            "text": "Open for Application",
            "value": 956390001
        },
        {
            "text": "Application Closed",
            "value": 956390002
        },
        {
            "text": "Complete",
            "value": 956390003
        },
        {
            "text": "Cancelled",
            "value": 2
        }],
        "SelectedOption": {
            "option": "New",
            "value": 956390000
        },
        "Text": "New",
        "Controls": [{
            "Name": "statuscode"
        }]
    },
    {
        "Name": "stb_enddate",
        "Value": null,
        "Type": "datetime",
        "Format": "date",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Controls": [{
            "Name": "stb_enddate"
        }]
    },
    {
        "Name": "stb_maxparticipants",
        "Value": null,
        "Type": "integer",
        "Format": "none",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Max": 2147483647,
        "Min": -2147483648,
        "Precision": 0,
        "Controls": [{
            "Name": "stb_maxparticipants"
        }]
    },
    {
        "Name": "stb_minparticipants",
        "Value": null,
        "Type": "integer",
        "Format": "none",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Max": 2147483647,
        "Min": -2147483648,
        "Precision": 0,
        "Controls": [{
            "Name": "stb_minparticipants"
        }]
    },
    {
        "Name": "stb_offeringformatoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Online",
            "value": 1
        },
        {
            "text": "Distance Learning",
            "value": 2
        },
        {
            "text": "Full-Time",
            "value": 3
        },
        {
            "text": "Part-Time",
            "value": 4
        },
        {
            "text": "Modular",
            "value": 5
        }],
        "SelectedOption": {
            "option": "Online",
            "value": 1
        },
        "Text": "Online",
        "Controls": [{
            "Name": "stb_offeringformatoptionset"
        }]
    },
    {
        "Name": "stb_offeringleveltwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "stb_offeringleveltwooptions"
        }]
    },
    {
        "Name": "stb_offeringnameafrikaans",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 250,
        "Controls": [{
            "Name": "stb_offeringnameafrikaans"
        }]
    },
    {
        "Name": "stb_offeringnameenglish",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 250,
        "Controls": [{
            "Name": "stb_offeringnameenglish"
        }]
    },
    {
        "Name": "stb_offeringoptiontwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "stb_offeringoptiontwooptions"
        }]
    },
    {
        "Name": "stb_offeringurl",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 250,
        "Controls": [{
            "Name": "stb_offeringurl"
        }]
    },
    {
        "Name": "stb_salespersonlookup",
        "Value": [{
            "entityType": "Unknown",
            "id": "{27ACAE0A-5D79-E111-BBD3-2657AEB3167B}",
            "name": "Temp"
        }],
        "Type": "lookup",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Controls": [{
            "Name": "stb_salespersonlookup"
        }]
    },
    {
        "Name": "stb_shortcourselookup",
        "Value": [{
            "entityType": "Unknown",
            "id": "{27ACAE0A-5D79-E111-BBD3-2657AEB3167B}",
            "name": "Temp"
        }],
        "Type": "lookup",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Controls": [{
            "Name": "stb_shortcourselookup"
        }]
    },
    {
        "Name": "stb_startdate",
        "Value": null,
        "Type": "datetime",
        "Format": "date",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Controls": [{
            "Name": "stb_startdate"
        }]
    },
    {
        "Name": "stb_venuelookup",
        "Value": [{
            "entityType": "Unknown",
            "id": "{27ACAE0A-5D79-E111-BBD3-2657AEB3167B}",
            "name": "Temp"
        }],
        "Type": "lookup",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Controls": [{
            "Name": "stb_venuelookup"
        }]
    }],
    "AttributesLength": 16,
    "Controls": [, {
        "Name": "footer_statecode",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Status",
        "Attribute": "statecode"
    },
    {
        "Name": "ownerid",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Owner",
        "Attribute": "ownerid"
    },
    {
        "Name": "statuscode",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Status Reason",
        "Attribute": "statuscode"
    },
    {
        "Name": "stb_enddate",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "End Date",
        "Attribute": "stb_enddate"
    },
    {
        "Name": "stb_maxparticipants",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Max Participants",
        "Attribute": "stb_maxparticipants"
    },
    {
        "Name": "stb_minparticipants",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Min. Participants",
        "Attribute": "stb_minparticipants"
    },
    {
        "Name": "stb_offeringformatoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Offering Format",
        "Attribute": "stb_offeringformatoptionset"
    },
    {
        "Name": "stb_offeringleveltwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Offering Level",
        "Attribute": "stb_offeringleveltwooptions"
    },
    {
        "Name": "stb_offeringnameafrikaans",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Offering Name Afrikaans",
        "Attribute": "stb_offeringnameafrikaans"
    },
    {
        "Name": "stb_offeringnameenglish",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Programme Offering Name",
        "Attribute": "stb_offeringnameenglish"
    },
    {
        "Name": "stb_offeringoptiontwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Offering Option",
        "Attribute": "stb_offeringoptiontwooptions"
    },
    {
        "Name": "stb_offeringurl",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Offering URL",
        "Attribute": "stb_offeringurl"
    },
    {
        "Name": "stb_salespersonlookup",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Sales Person",
        "Attribute": "stb_salespersonlookup"
    },
    {
        "Name": "stb_shortcourselookup",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Short Course",
        "Attribute": "stb_shortcourselookup"
    },
    {
        "Name": "stb_startdate",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Start Date",
        "Attribute": "stb_startdate"
    },
    {
        "Name": "stb_venuelookup",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Venue",
        "Attribute": "stb_venuelookup"
    }],
    "ControlsLength": 16,
    "Navigation": [{
        "Id": "navConnections",
        "Key": "navConnections",
        "Label": "",
        "Visible": true
    }],
    "Tabs": [{
        "Label": "General",
        "Name": "null",
        "DisplayState": "expanded",
        "Visible": true,
        "Sections": [{
            "Label": "General",
            "Name": "{602766f3-460a-4928-b40e-2359baca72cb}",
            "Visible": true,
            "Controls": [{
                "Name": "stb_offeringnameenglish"
            },
            {
                "Name": "stb_offeringnameafrikaans"
            },
            {
                "Name": "stb_shortcourselookup"
            },
            {
                "Name": "stb_offeringurl"
            },
            {
                "Name": "stb_offeringformatoptionset"
            },
            {
                "Name": "stb_offeringleveltwooptions"
            },
            {
                "Name": "stb_offeringoptiontwooptions"
            }]
        },
        {
            "Label": "Details",
            "Name": "{bb903f97-f8e7-4419-9c6f-e59e971dbf33}_section_2",
            "Visible": true,
            "Controls": [{
                "Name": "stb_venuelookup"
            },
            {
                "Name": "stb_minparticipants"
            },
            {
                "Name": "stb_maxparticipants"
            },
            {
                "Name": "stb_salespersonlookup"
            },
            {
                "Name": "stb_startdate"
            },
            {
                "Name": "stb_enddate"
            }]
        },
        {
            "Label": "Administration",
            "Name": "{bb903f97-f8e7-4419-9c6f-e59e971dbf33}_section_3",
            "Visible": true,
            "Controls": [{
                "Name": "ownerid"
            },
            {
                "Name": "statuscode"
            }]
        }]
    },
    {
        "Label": "Notes",
        "Name": "null",
        "DisplayState": "expanded",
        "Visible": true,
        "Sections": [{
            "Label": "Notes",
            "Name": "null",
            "Visible": true,
            "Controls": [{
                "Name": "notescontrol"
            }]
        }]
    },
    {
        "Label": "Certified Applicants",
        "Name": "tab_3",
        "DisplayState": "expanded",
        "Visible": true,
        "Sections": [{
            "Label": "Section",
            "Name": "tab_3_section_1",
            "Visible": true,
            "Controls": [{
                "Name": "CertifiedApplicantsGrid"
            }]
        }]
    }]
};

var Xrm = new _xrm(pageData);