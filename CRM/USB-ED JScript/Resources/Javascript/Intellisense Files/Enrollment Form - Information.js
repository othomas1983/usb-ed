/// <reference path="Xrm.js" />

var EntityLogicalName = "stb_enrollment";
var Form_a5c97de1_395f_4240_a6ed_47812d41a146_Properties = {
    ownerid: "ownerid",
    statuscode: "statuscode",
    stb_academicinstitution: "stb_academicinstitution",
    stb_attendance: "stb_attendance",
    stb_cancelreasonoptionset: "stb_cancelreasonoptionset",
    stb_certificatenumber: "stb_certificatenumber",
    stb_currentemployer: "stb_currentemployer",
    stb_currentlyemployedtwooptions: "stb_currentlyemployedtwooptions",
    stb_debtorcode: "stb_debtorcode",
    stb_employercontactnumber: "stb_employercontactnumber",
    stb_employercontactperson: "stb_employercontactperson",
    stb_fieldofstudylookup: "stb_fieldofstudylookup",
    stb_industrylookup: "stb_industrylookup",
    stb_interdepartmentalrequisitionnumber: "stb_interdepartmentalrequisitionnumber",
    stb_invoicenumber: "stb_invoicenumber",
    stb_jobtitle: "stb_jobtitle",
    stb_leadsource: "stb_leadsource",
    stb_leadsourceother: "stb_leadsourceother",
    stb_marketingreason: "stb_marketingreason",
    stb_marketingreasonother: "stb_marketingreasonother",
    stb_marks: "stb_marks",
    stb_nameoncertificate: "stb_nameoncertificate",
    stb_occupationalcategorylookup: "stb_occupationalcategorylookup",
    stb_payeecontactemailaddress: "stb_payeecontactemailaddress",
    stb_payeecontactnumber: "stb_payeecontactnumber",
    stb_payeecontactperson: "stb_payeecontactperson",
    stb_payeedebtornumber: "stb_payeedebtornumber",
    stb_paymentdate: "stb_paymentdate",
    stb_paymentindicatortwooptions: "stb_paymentindicatortwooptions",
    stb_paymentreceiptnumber: "stb_paymentreceiptnumber",
    stb_paymentreferencenumber: "stb_paymentreferencenumber",
    stb_paymentresponsibilitylookup: "stb_paymentresponsibilitylookup",
    stb_printedtwooptions: "stb_printedtwooptions",
    stb_professionalassociationname: "stb_professionalassociationname",
    stb_professionalassociationregistrationnumber: "stb_professionalassociationregistrationnumber",
    stb_qualificationtypelookup: "stb_qualificationtypelookup",
    stb_qualificationtypeother: "stb_qualificationtypeother",
    stb_rplstudenttwooptions: "stb_rplstudenttwooptions",
    stb_shortcourseofferinglookup: "stb_shortcourseofferinglookup",
    stb_studentcontact_contactlookup: "stb_studentcontact_contactlookup",
    stb_usnumber: "stb_usnumber",
    stb_workarealookup: "stb_workarealookup",
    stb_yearachieved: "stb_yearachieved"
};

var Form_a5c97de1_395f_4240_a6ed_47812d41a146_Controls = {
    header_statuscode: "header_statuscode",
    header_stb_usnumber: "header_stb_usnumber",
    notescontrol: "notescontrol",
    ownerid: "ownerid",
    statuscode: "statuscode",
    stb_academicinstitution: "stb_academicinstitution",
    stb_attendance: "stb_attendance",
    stb_cancelreasonoptionset: "stb_cancelreasonoptionset",
    stb_certificatenumber: "stb_certificatenumber",
    stb_currentemployer: "stb_currentemployer",
    stb_currentlyemployedtwooptions: "stb_currentlyemployedtwooptions",
    stb_debtorcode: "stb_debtorcode",
    stb_employercontactnumber: "stb_employercontactnumber",
    stb_employercontactperson: "stb_employercontactperson",
    stb_fieldofstudylookup: "stb_fieldofstudylookup",
    stb_industrylookup: "stb_industrylookup",
    stb_interdepartmentalrequisitionnumber: "stb_interdepartmentalrequisitionnumber",
    stb_invoicenumber: "stb_invoicenumber",
    stb_jobtitle: "stb_jobtitle",
    stb_leadsource: "stb_leadsource",
    stb_leadsourceother: "stb_leadsourceother",
    stb_marketingreason: "stb_marketingreason",
    stb_marketingreasonother: "stb_marketingreasonother",
    stb_marks: "stb_marks",
    stb_nameoncertificate: "stb_nameoncertificate",
    stb_occupationalcategorylookup: "stb_occupationalcategorylookup",
    stb_payeecontactemailaddress: "stb_payeecontactemailaddress",
    stb_payeecontactnumber: "stb_payeecontactnumber",
    stb_payeecontactperson: "stb_payeecontactperson",
    stb_payeedebtornumber: "stb_payeedebtornumber",
    stb_paymentdate: "stb_paymentdate",
    stb_paymentindicatortwooptions: "stb_paymentindicatortwooptions",
    stb_paymentreceiptnumber: "stb_paymentreceiptnumber",
    stb_paymentreferencenumber: "stb_paymentreferencenumber",
    stb_paymentresponsibilitylookup: "stb_paymentresponsibilitylookup",
    stb_printedtwooptions: "stb_printedtwooptions",
    stb_professionalassociationname: "stb_professionalassociationname",
    stb_professionalassociationregistrationnumber: "stb_professionalassociationregistrationnumber",
    stb_qualificationtypelookup: "stb_qualificationtypelookup",
    stb_qualificationtypeother: "stb_qualificationtypeother",
    stb_rplstudenttwooptions: "stb_rplstudenttwooptions",
    stb_shortcourseofferinglookup: "stb_shortcourseofferinglookup",
    stb_studentcontact_contactlookup: "stb_studentcontact_contactlookup",
    stb_workarealookup: "stb_workarealookup",
    stb_yearachieved: "stb_yearachieved"
};

var pageData = {
    "Event": "none",
    "SaveMode": 1,
    "EventSource": null,
    "AuthenticationHeader": "",
    "CurrentTheme": "Default",
    "OrgLcid": 1033,
    "OrgUniqueName": "",
    "QueryStringParameters": {
        "_gridType": "10084",
        "etc": "10084",
        "id": "",
        "pagemode": "iframe",
        "preloadcache": "1344548892170",
        "rskey": "141637534"
    },
    "ServerUrl": "",
    "UserId": "",
    "UserLcid": 1033,
    "UserRoles": [""],
    "isOutlookClient": false,
    "isOutlookOnline": true,
    "DataXml": "",
    "EntityName": "stb_enrollment",
    "Id": "",
    "IsDirty": false,
    "CurrentControl": "",
    "CurrentForm": null,
    "Forms": [],
    "FormType": 2,
    "ViewPortHeight": 558,
    "ViewPortWidth": 1231,
    "Attributes": [{
        "Name": "ownerid",
        "Value": [{
            "entityType": "Unknown",
            "id": "{27ACAE0A-5D79-E111-BBD3-2657AEB3167B}",
            "name": "Temp"
        }],
        "Type": "lookup",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": false,
            "canCreate": true
        },
        "Controls": [{
            "Name": "ownerid"
        }]
    },
    {
        "Name": "statuscode",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Applied",
            "value": 1
        },
        {
            "text": "Qualified",
            "value": 956390017
        },
        {
            "text": "Approved",
            "value": 956390018
        },
        {
            "text": "Request: Admitted",
            "value": 956390000
        },
        {
            "text": "Admitted",
            "value": 956390001
        },
        {
            "text": "Failed: Admitted",
            "value": 956390002
        },
        {
            "text": "Waitlisted",
            "value": 956390003
        },
        {
            "text": "Not Admitted",
            "value": 956390004
        },
        {
            "text": "Request: Registered",
            "value": 956390005
        },
        {
            "text": "Registered",
            "value": 956390006
        },
        {
            "text": "Failed: Registered",
            "value": 956390007
        },
        {
            "text": "Request: Passed",
            "value": 956390008
        },
        {
            "text": "Passed",
            "value": 956390009
        },
        {
            "text": "Failed: Passed",
            "value": 956390010
        },
        {
            "text": "Request: Failed",
            "value": 956390011
        },
        {
            "text": "Failed",
            "value": 956390012
        },
        {
            "text": "Failed: Failed",
            "value": 956390013
        },
        {
            "text": "Cancelled",
            "value": 956390014
        },
        {
            "text": "Deactivated",
            "value": 2
        }],
        "SelectedOption": {
            "option": "Applied",
            "value": 1
        },
        "Text": "Applied",
        "Controls": [{
            "Name": "header_statuscode"
        },
        {
            "Name": "statuscode"
        }]
    },
    {
        "Name": "stb_academicinstitution",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "stb_academicinstitution"
        }]
    },
    {
        "Name": "stb_attendance",
        "Value": null,
        "Type": "decimal",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Max": 1000000000,
        "Min": 0,
        "Precision": 0,
        "Controls": [{
            "Name": "stb_attendance"
        }]
    },
    {
        "Name": "stb_cancelreasonoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Disqualified",
            "value": 956390000
        },
        {
            "text": "Financial Reasons",
            "value": 956390001
        },
        {
            "text": "Imcomplete/Discontinued",
            "value": 956390002
        },
        {
            "text": "Medical Reasons",
            "value": 956390003
        },
        {
            "text": "Moved to another Programme",
            "value": 956390004
        },
        {
            "text": "No Show",
            "value": 956390005
        },
        {
            "text": "Personal Reasons",
            "value": 956390006
        },
        {
            "text": "Programme Cancelled",
            "value": 956390007
        },
        {
            "text": "Programme Postponed",
            "value": 956390008
        },
        {
            "text": "No Confirmation from Client",
            "value": 956390009
        }],
        "SelectedOption": {
            "option": "Disqualified",
            "value": 956390000
        },
        "Text": "Disqualified",
        "Controls": [{
            "Name": "stb_cancelreasonoptionset"
        }]
    },
    {
        "Name": "stb_certificatenumber",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "stb_certificatenumber"
        }]
    },
    {
        "Name": "stb_currentemployer",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "stb_currentemployer"
        }]
    },
    {
        "Name": "stb_currentlyemployedtwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "stb_currentlyemployedtwooptions"
        }]
    },
    {
        "Name": "stb_debtorcode",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 10,
        "Controls": [{
            "Name": "stb_debtorcode"
        }]
    },
    {
        "Name": "stb_employercontactnumber",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "stb_employercontactnumber"
        }]
    },
    {
        "Name": "stb_employercontactperson",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "stb_employercontactperson"
        }]
    },
    {
        "Name": "stb_fieldofstudylookup",
        "Value": [{
            "entityType": "Unknown",
            "id": "{27ACAE0A-5D79-E111-BBD3-2657AEB3167B}",
            "name": "Temp"
        }],
        "Type": "lookup",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Controls": [{
            "Name": "stb_fieldofstudylookup"
        }]
    },
    {
        "Name": "stb_industrylookup",
        "Value": [{
            "entityType": "Unknown",
            "id": "{27ACAE0A-5D79-E111-BBD3-2657AEB3167B}",
            "name": "Temp"
        }],
        "Type": "lookup",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Controls": [{
            "Name": "stb_industrylookup"
        }]
    },
    {
        "Name": "stb_interdepartmentalrequisitionnumber",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 10,
        "Controls": [{
            "Name": "stb_interdepartmentalrequisitionnumber"
        }]
    },
    {
        "Name": "stb_invoicenumber",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 10,
        "Controls": [{
            "Name": "stb_invoicenumber"
        }]
    },
    {
        "Name": "stb_jobtitle",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "stb_jobtitle"
        }]
    },
    {
        "Name": "stb_leadsource",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Curata",
            "value": 200016
        },
        {
            "text": "Referral Website",
            "value": 200015
        },
        {
            "text": "Advertisement",
            "value": 1
        },
        {
            "text": "Biz Community",
            "value": 200006
        },
        {
            "text": "Business Day website",
            "value": 200007
        },
        {
            "text": "ED Focus email",
            "value": 200013
        },
        {
            "text": "Email Campaign",
            "value": 200002
        },
        {
            "text": "Email Enquiry",
            "value": 200000
        },
        {
            "text": "Email Vibrant",
            "value": 200003
        },
        {
            "text": "Facebook",
            "value": 100000001
        },
        {
            "text": "Twitter",
            "value": 100000002
        },
        {
            "text": "Employer Referral",
            "value": 2
        },
        {
            "text": "External Referral",
            "value": 3
        },
        {
            "text": "Google AdWords",
            "value": 200004
        },
        {
            "text": "Google Search",
            "value": 200011
        },
        {
            "text": "HR Works",
            "value": 200008
        },
        {
            "text": "Human Capital Review",
            "value": 200009
        },
        {
            "text": "Leader.co.za",
            "value": 200001
        },
        {
            "text": "Other",
            "value": 10
        },
        {
            "text": "Partner",
            "value": 4
        },
        {
            "text": "Rocketseed Signature",
            "value": 200014
        },
        {
            "text": "Seminar",
            "value": 6
        },
        {
            "text": "Telephone",
            "value": 200005
        },
        {
            "text": "Trade Show",
            "value": 7
        },
        {
            "text": "US",
            "value": 100000000
        },
        {
            "text": "USB",
            "value": 200012
        },
        {
            "text": "Web - General",
            "value": 8
        },
        {
            "text": "Web - News Section",
            "value": 30
        },
        {
            "text": "Web - Views Section",
            "value": 31
        },
        {
            "text": "Walk-in",
            "value": 200010
        },
        {
            "text": "Word of Mouth",
            "value": 9
        }],
        "SelectedOption": {
            "option": "Curata",
            "value": 200016
        },
        "Text": "Curata",
        "Controls": [{
            "Name": "stb_leadsource"
        }]
    },
    {
        "Name": "stb_leadsourceother",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "stb_leadsourceother"
        }]
    },
    {
        "Name": "stb_marketingreason",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "International Accreditation",
            "value": 1
        },
        {
            "text": "Competitive Fees",
            "value": 2
        },
        {
            "text": "Recommended by Alumni",
            "value": 3
        },
        {
            "text": "You are an Alumnus of the University",
            "value": 4
        },
        {
            "text": "Geographical Location",
            "value": 5
        },
        {
            "text": "Good Reputation",
            "value": 6
        },
        {
            "text": "Recommended by Employer",
            "value": 7
        },
        {
            "text": "High Quality of Students",
            "value": 8
        },
        {
            "text": "Career Progression/Change",
            "value": 9
        },
        {
            "text": "Attended Information Session",
            "value": 10
        },
        {
            "text": "Other",
            "value": 11
        }],
        "SelectedOption": {
            "option": "International Accreditation",
            "value": 1
        },
        "Text": "International Accreditation",
        "Controls": [{
            "Name": "stb_marketingreason"
        }]
    },
    {
        "Name": "stb_marketingreasonother",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "stb_marketingreasonother"
        }]
    },
    {
        "Name": "stb_marks",
        "Value": null,
        "Type": "decimal",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Max": 1000000000,
        "Min": 0,
        "Precision": 0,
        "Controls": [{
            "Name": "stb_marks"
        }]
    },
    {
        "Name": "stb_nameoncertificate",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 250,
        "Controls": [{
            "Name": "stb_nameoncertificate"
        }]
    },
    {
        "Name": "stb_occupationalcategorylookup",
        "Value": [{
            "entityType": "Unknown",
            "id": "{27ACAE0A-5D79-E111-BBD3-2657AEB3167B}",
            "name": "Temp"
        }],
        "Type": "lookup",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Controls": [{
            "Name": "stb_occupationalcategorylookup"
        }]
    },
    {
        "Name": "stb_payeecontactemailaddress",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 250,
        "Controls": [{
            "Name": "stb_payeecontactemailaddress"
        }]
    },
    {
        "Name": "stb_payeecontactnumber",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "stb_payeecontactnumber"
        }]
    },
    {
        "Name": "stb_payeecontactperson",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "stb_payeecontactperson"
        }]
    },
    {
        "Name": "stb_payeedebtornumber",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "stb_payeedebtornumber"
        }]
    },
    {
        "Name": "stb_paymentdate",
        "Value": null,
        "Type": "datetime",
        "Format": "date",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Controls": [{
            "Name": "stb_paymentdate"
        }]
    },
    {
        "Name": "stb_paymentindicatortwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "stb_paymentindicatortwooptions"
        }]
    },
    {
        "Name": "stb_paymentreceiptnumber",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 20,
        "Controls": [{
            "Name": "stb_paymentreceiptnumber"
        }]
    },
    {
        "Name": "stb_paymentreferencenumber",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "stb_paymentreferencenumber"
        }]
    },
    {
        "Name": "stb_paymentresponsibilitylookup",
        "Value": [{
            "entityType": "Unknown",
            "id": "{27ACAE0A-5D79-E111-BBD3-2657AEB3167B}",
            "name": "Temp"
        }],
        "Type": "lookup",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Controls": [{
            "Name": "stb_paymentresponsibilitylookup"
        }]
    },
    {
        "Name": "stb_printedtwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "stb_printedtwooptions"
        }]
    },
    {
        "Name": "stb_professionalassociationname",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 250,
        "Controls": [{
            "Name": "stb_professionalassociationname"
        }]
    },
    {
        "Name": "stb_professionalassociationregistrationnumber",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "stb_professionalassociationregistrationnumber"
        }]
    },
    {
        "Name": "stb_qualificationtypelookup",
        "Value": [{
            "entityType": "Unknown",
            "id": "{27ACAE0A-5D79-E111-BBD3-2657AEB3167B}",
            "name": "Temp"
        }],
        "Type": "lookup",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Controls": [{
            "Name": "stb_qualificationtypelookup"
        }]
    },
    {
        "Name": "stb_qualificationtypeother",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "stb_qualificationtypeother"
        }]
    },
    {
        "Name": "stb_rplstudenttwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "stb_rplstudenttwooptions"
        }]
    },
    {
        "Name": "stb_shortcourseofferinglookup",
        "Value": [{
            "entityType": "Unknown",
            "id": "{27ACAE0A-5D79-E111-BBD3-2657AEB3167B}",
            "name": "Temp"
        }],
        "Type": "lookup",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Controls": [{
            "Name": "stb_shortcourseofferinglookup"
        }]
    },
    {
        "Name": "stb_studentcontact_contactlookup",
        "Value": [{
            "entityType": "Unknown",
            "id": "{27ACAE0A-5D79-E111-BBD3-2657AEB3167B}",
            "name": "Temp"
        }],
        "Type": "lookup",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Controls": [{
            "Name": "stb_studentcontact_contactlookup"
        }]
    },
    {
        "Name": "stb_usnumber",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 8,
        "Controls": [{
            "Name": "header_stb_usnumber"
        }]
    },
    {
        "Name": "stb_workarealookup",
        "Value": [{
            "entityType": "Unknown",
            "id": "{27ACAE0A-5D79-E111-BBD3-2657AEB3167B}",
            "name": "Temp"
        }],
        "Type": "lookup",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Controls": [{
            "Name": "stb_workarealookup"
        }]
    },
    {
        "Name": "stb_yearachieved",
        "Value": null,
        "Type": "datetime",
        "Format": "date",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Controls": [{
            "Name": "stb_yearachieved"
        }]
    }],
    "AttributesLength": 43,
    "Controls": [{
        "Name": "header_statuscode",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Status Reason",
        "Attribute": "statuscode"
    },
    {
        "Name": "header_stb_usnumber",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Student Number",
        "Attribute": "stb_usnumber"
    },
    {
        "Name": "ownerid",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Owner",
        "Attribute": "ownerid"
    },
    {
        "Name": "statuscode",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Status Reason",
        "Attribute": "statuscode"
    },
    {
        "Name": "stb_academicinstitution",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Academic Institution",
        "Attribute": "stb_academicinstitution"
    },
    {
        "Name": "stb_attendance",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Attendance (%)",
        "Attribute": "stb_attendance"
    },
    {
        "Name": "stb_cancelreasonoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Cancel Reason",
        "Attribute": "stb_cancelreasonoptionset"
    },
    {
        "Name": "stb_certificatenumber",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Certificate Number",
        "Attribute": "stb_certificatenumber"
    },
    {
        "Name": "stb_currentemployer",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Current Employer",
        "Attribute": "stb_currentemployer"
    },
    {
        "Name": "stb_currentlyemployedtwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Currently Employed",
        "Attribute": "stb_currentlyemployedtwooptions"
    },
    {
        "Name": "stb_debtorcode",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Debtor Code",
        "Attribute": "stb_debtorcode"
    },
    {
        "Name": "stb_employercontactnumber",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Employer Contact Number",
        "Attribute": "stb_employercontactnumber"
    },
    {
        "Name": "stb_employercontactperson",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Employer Contact Person",
        "Attribute": "stb_employercontactperson"
    },
    {
        "Name": "stb_fieldofstudylookup",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Field of Study",
        "Attribute": "stb_fieldofstudylookup"
    },
    {
        "Name": "stb_industrylookup",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Industry",
        "Attribute": "stb_industrylookup"
    },
    {
        "Name": "stb_interdepartmentalrequisitionnumber",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "InterDepartmental Requisition Number",
        "Attribute": "stb_interdepartmentalrequisitionnumber"
    },
    {
        "Name": "stb_invoicenumber",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Invoice Number",
        "Attribute": "stb_invoicenumber"
    },
    {
        "Name": "stb_jobtitle",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Job Title",
        "Attribute": "stb_jobtitle"
    },
    {
        "Name": "stb_leadsource",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Lead Source",
        "Attribute": "stb_leadsource"
    },
    {
        "Name": "stb_leadsourceother",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Lead Source (Other)",
        "Attribute": "stb_leadsourceother"
    },
    {
        "Name": "stb_marketingreason",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Marketing Reason",
        "Attribute": "stb_marketingreason"
    },
    {
        "Name": "stb_marketingreasonother",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Marketing Reason (Other)",
        "Attribute": "stb_marketingreasonother"
    },
    {
        "Name": "stb_marks",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Marks (%)",
        "Attribute": "stb_marks"
    },
    {
        "Name": "stb_nameoncertificate",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Name on Certificate",
        "Attribute": "stb_nameoncertificate"
    },
    {
        "Name": "stb_occupationalcategorylookup",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Occupational Category",
        "Attribute": "stb_occupationalcategorylookup"
    },
    {
        "Name": "stb_payeecontactemailaddress",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Payee Contact Email Address",
        "Attribute": "stb_payeecontactemailaddress"
    },
    {
        "Name": "stb_payeecontactnumber",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Payee Contact Number",
        "Attribute": "stb_payeecontactnumber"
    },
    {
        "Name": "stb_payeecontactperson",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Payee Contact Person",
        "Attribute": "stb_payeecontactperson"
    },
    {
        "Name": "stb_payeedebtornumber",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Payee Debtor Number",
        "Attribute": "stb_payeedebtornumber"
    },
    {
        "Name": "stb_paymentdate",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Payment Date",
        "Attribute": "stb_paymentdate"
    },
    {
        "Name": "stb_paymentindicatortwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Payment Indicator",
        "Attribute": "stb_paymentindicatortwooptions"
    },
    {
        "Name": "stb_paymentreceiptnumber",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Payment Receipt Number",
        "Attribute": "stb_paymentreceiptnumber"
    },
    {
        "Name": "stb_paymentreferencenumber",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Payment Reference Number",
        "Attribute": "stb_paymentreferencenumber"
    },
    {
        "Name": "stb_paymentresponsibilitylookup",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Payment Responsibility",
        "Attribute": "stb_paymentresponsibilitylookup"
    },
    {
        "Name": "stb_printedtwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Certificate Printed",
        "Attribute": "stb_printedtwooptions"
    },
    {
        "Name": "stb_professionalassociationname",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Professional Association Name",
        "Attribute": "stb_professionalassociationname"
    },
    {
        "Name": "stb_professionalassociationregistrationnumber",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Professional Association Registration Number",
        "Attribute": "stb_professionalassociationregistrationnumber"
    },
    {
        "Name": "stb_qualificationtypelookup",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Qualification Type",
        "Attribute": "stb_qualificationtypelookup"
    },
    {
        "Name": "stb_qualificationtypeother",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Qualification Type (Other)",
        "Attribute": "stb_qualificationtypeother"
    },
    {
        "Name": "stb_rplstudenttwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "RPL Student",
        "Attribute": "stb_rplstudenttwooptions"
    },
    {
        "Name": "stb_shortcourseofferinglookup",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Short Course Offering",
        "Attribute": "stb_shortcourseofferinglookup"
    },
    {
        "Name": "stb_studentcontact_contactlookup",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Student Contact",
        "Attribute": "stb_studentcontact_contactlookup"
    },
    {
        "Name": "stb_workarealookup",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Work Area",
        "Attribute": "stb_workarealookup"
    },
    {
        "Name": "stb_yearachieved",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Year Achieved",
        "Attribute": "stb_yearachieved"
    }],
    "ControlsLength": 44,
    "Navigation": [],
    "Tabs": [{
        "Label": "General",
        "Name": "tab_2",
        "DisplayState": "expanded",
        "Visible": true,
        "Sections": [{
            "Label": "Student Detail",
            "Name": "tab_2_section_1",
            "Visible": true,
            "Controls": [{
                "Name": "stb_nameoncertificate"
            },
            {
                "Name": "stb_studentcontact_contactlookup"
            },
            {
                "Name": "stb_shortcourseofferinglookup"
            },
            {
                "Name": "stb_printedtwooptions"
            },
            {
                "Name": "stb_certificatenumber"
            },
            {
                "Name": "ownerid"
            }]
        },
        {
            "Label": "Student Results",
            "Name": "tab_2_section_3",
            "Visible": true,
            "Controls": [{
                "Name": "stb_attendance"
            },
            {
                "Name": "stb_marks"
            },
            {
                "Name": "stb_cancelreasonoptionset"
            }]
        }]
    },
    {
        "Label": "Finances",
        "Name": "tab_3",
        "DisplayState": "expanded",
        "Visible": true,
        "Sections": [{
            "Label": "Payment Detail",
            "Name": "tab_3_section_1",
            "Visible": true,
            "Controls": [{
                "Name": "stb_paymentresponsibilitylookup"
            },
            {
                "Name": "stb_paymentreferencenumber"
            },
            {
                "Name": "stb_paymentindicatortwooptions"
            },
            {
                "Name": "stb_paymentreceiptnumber"
            },
            {
                "Name": "stb_invoicenumber"
            },
            {
                "Name": "stb_paymentdate"
            },
            {
                "Name": "stb_debtorcode"
            },
            {
                "Name": "stb_interdepartmentalrequisitionnumber"
            }]
        },
        {
            "Label": "Payee Information",
            "Name": "tab_3_section_2",
            "Visible": true,
            "Controls": [{
                "Name": "stb_payeecontactperson"
            },
            {
                "Name": "stb_payeedebtornumber"
            },
            {
                "Name": "stb_payeecontactnumber"
            },
            {
                "Name": "stb_payeecontactemailaddress"
            }]
        }]
    },
    {
        "Label": "Background",
        "Name": "tab_4",
        "DisplayState": "expanded",
        "Visible": true,
        "Sections": [{
            "Label": "Current Highest Qualifications",
            "Name": "tab_4_section_1",
            "Visible": true,
            "Controls": [{
                "Name": "stb_qualificationtypelookup"
            },
            {
                "Name": "stb_fieldofstudylookup"
            },
            {
                "Name": "stb_qualificationtypeother"
            },
            {
                "Name": "stb_leadsource"
            },
            {
                "Name": "stb_academicinstitution"
            },
            {
                "Name": "stb_leadsourceother"
            },
            {
                "Name": "stb_marketingreason"
            },
            {
                "Name": "stb_yearachieved"
            },
            {
                "Name": "stb_marketingreasonother"
            },
            {
                "Name": "stb_rplstudenttwooptions"
            }]
        },
        {
            "Label": "Current Employment",
            "Name": "tab_4_section_2",
            "Visible": true,
            "Controls": [{
                "Name": "stb_currentlyemployedtwooptions"
            },
            {
                "Name": "stb_jobtitle"
            },
            {
                "Name": "stb_currentemployer"
            },
            {
                "Name": "stb_workarealookup"
            },
            {
                "Name": "stb_employercontactnumber"
            },
            {
                "Name": "stb_industrylookup"
            },
            {
                "Name": "stb_employercontactperson"
            }]
        },
        {
            "Label": "Professional Membership",
            "Name": "tab_4_section_3",
            "Visible": true,
            "Controls": [{
                "Name": "stb_professionalassociationname"
            },
            {
                "Name": "stb_occupationalcategorylookup"
            },
            {
                "Name": "stb_professionalassociationregistrationnumber"
            }]
        }]
    },
    {
        "Label": "Notes",
        "Name": "tab_5",
        "DisplayState": "expanded",
        "Visible": true,
        "Sections": [{
            "Label": "Notes",
            "Name": "tab_5_section_3",
            "Visible": true,
            "Controls": [{
                "Name": "notescontrol"
            }]
        }]
    },
    {
        "Label": "Hidden Fields",
        "Name": "Hidden Fields",
        "DisplayState": "expanded",
        "Visible": true,
        "Sections": [{
            "Label": "Section",
            "Name": "tab_5_section_1",
            "Visible": true,
            "Controls": [{
                "Name": "statuscode"
            }]
        },
        {
            "Label": "Section",
            "Name": "tab_5_section_2",
            "Visible": true,
            "Controls": []
        }]
    }]
};

var Xrm = new _xrm(pageData);