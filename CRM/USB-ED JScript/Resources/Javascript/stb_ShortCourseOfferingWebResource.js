﻿ /// <reference path="EOHMC Standard Library.js" />
/// <reference path="Intellisense Files/Short Course Offering Form - Information.js" />

//Ribbon Controlled Functions
//
//
//
//Function to control Inside CRM form token functionality---------------------------------------------------------------------------
function AddEnrollment() {
    debugger;
    if (confirm('Record will be saved and closed to complete this operation')) {
        //Check Short Course is valid
        var lookup = Xrm.Page.getAttribute("stb_shortcourselookup").getValue();
        if (lookup == null && lookup[0] != null) {
            alert("Please select a valid Short Course for the record");
            LibSetFieldFocus("stb_shortcourselookup");
            return;
        }
        //Check if Offering is in status "Open for Application"
        if (Xrm.Page.getAttribute('statuscode').getValue() != 956390001) {
            alert("Cannot add enrollment: offering is not Open for Application");
            return;
        }
        //Get Short Course Offering Guid
        var guidShortCourseOffering = Xrm.Page.data.entity.getId().substring(1, 37);
        var tokenGuid = CreateWebToken(guidShortCourseOffering);
        //Close current window and open external URL
        Xrm.Page.data.entity.save("saveandclose");
        var URL = applicationFormBaseURL + "webTokenId=" + tokenGuid + "&oID=" + guidShortCourseOffering;
        window.open(URL, "_blank", "height=" + screen.height + ",width=" + screen.width + ",menubar=yes,resizable=yes,toolbar=yes,location=yes,scrollbars=yes,status=yes,titlebar=yes");
    }
    else {
        return;
    }
}
//END-------------------------------------------------------------------------------------------------------------------------------

//Function to call dialog from a Button---------------------------------------------------------------------------------------------
function SetOfferingDate() {
    debugger;
    var primaryEntityId = Xrm.Page.data.entity.getId();
    //Close base record to get around background opening
    if (confirm('Record will be saved and closed to complete this operation')) {
        Xrm.Page.data.entity.save("saveandclose");
        URL = baseUrl + "/cs/dialog/rundialog.aspx?DialogId=%7b8C4C32C2-C4B2-4D30-8A16-F534C8797BEC%7d&EntityName=stb_shortcourseoffering&ObjectId=" + primaryEntityId;
        window.open(URL, "_blank", "height=600px,width=800px");
    }
    else {
        return;
    }
}
//END-------------------------------------------------------------------------------------------------------------------------------

//Function to call dialog from a Button---------------------------------------------------------------------------------------------
function RequestCertificate() {
    debugger;
    if (confirm('Record will be saved and closed to complete this operation')) {
        //Get Short Course Offering Guid
        var guidShortCourseOffering = Xrm.Page.data.entity.getId().substring(1, 37);
        var tokenGuid = CreateWebToken(guidShortCourseOffering);
        //Close current window and open external URL
        Xrm.Page.data.entity.save("saveandclose");
        var URL = requestCertificateBaseURL + "webTokenId=" + tokenGuid + "&shortCourseOfferingId=" + guidShortCourseOffering;
        window.open(URL, "_blank", "height=" + screen.height + ",width=" + screen.width + ",menubar=yes,resizable=yes,toolbar=yes,location=yes,scrollbars=yes,status=yes,titlebar=yes");
    }
    else {
        return;
    }
}
//END-------------------------------------------------------------------------------------------------------------------------------
//
//
//
//End Ribbon Controlled Functions

//On Change Functions
//
//
//
//Function to prevent Status Reason change if Date Blank----------------------------------------------------------------------------
function OfferingStatusChange() {
    debugger;
    if (Xrm.Page.getAttribute("stb_startdate").getValue() == null) {
        LibSetFieldDisabled("statuscode");
    }
}
//END-------------------------------------------------------------------------------------------------------------------------------
//
//
//
//End On Change Functions

//On Load Functions
//
//
//
//Function to update URL on Short Course Offering on Load---------------------------------------------------------------------------
function UpdateOfferingURL() {
    debugger;
    //var offeringUrl = applicationFormBaseURL + "oID=" + Xrm.Page.data.entity.getId().toLowerCase().substring(1, 37);
    var offeringUrlold = "http://applications.usb-ed.com/?oID=" + Xrm.Page.data.entity.getId().toLowerCase().substring(1, 37);
    var offeringUrl = "http://application.usb-ed.com/?oID=" + Xrm.Page.data.entity.getId().toLowerCase().substring(1, 37);
    if (Xrm.Page.getAttribute("stb_offeringurl").getValue() == null || Xrm.Page.getAttribute("stb_offeringurl").getValue() != offeringUrlold) {
        LibSubmitToField("stb_offeringurl", offeringUrl);
    }
}
//END-------------------------------------------------------------------------------------------------------------------------------
//
//
//
//End Load Functions

//Ribbon Controlled Functions
//
//
//
//Functions to control availability of buttons in the ribbon
function EnableAddEnrollmentButton() {
    debugger;
    //Always enabled
    return true;
}
//END-------------------------------------------------------------------------------------------------------------------------------

function EnableSetOfferingDateButton() {
    debugger;
    //Always enabled
    return true;
}
//END-------------------------------------------------------------------------------------------------------------------------------

function EnableRequestCertificateButton() {
    debugger;
    //Check if current user is the Owner
    var ownerId = Xrm.Page.getAttribute("ownerid").getValue()[0].id;
    var currentUserId = Xrm.Page.context.getUserId();

    if (ownerId == currentUserId) {
        return true;
    }
}
//END-------------------------------------------------------------------------------------------------------------------------------
//
//
//
//End Ribbon Controlled Functions

//Reusable Functions for Offering Entity
//
//
//Function to create a Web Token in the Context of a Short Course Offering
function CreateWebToken(guidShortCourseOffering) {
    var tokenGuid;
    //Add Webtoken Object for creation of Record
    var WebToken = new Object();
    WebToken.stb_Name = guidShortCourseOffering;
    //Create Web Token
    var ReturnData = LibCreateNewRecord(WebToken, "stb_webtokenSet");
    if (ReturnData && ReturnData.d != null) {
        tokenGuid = ReturnData.d.stb_webtokenId;
    }
    return tokenGuid;
}
//END-------------------------------------------------------------------------------------------------------------------------------
//
//
//
//Function to check Certificate Type on Short Course
/*function setCertificateType(executionContext) {
    debugger;

   var FieldName = executionContext.getEventSource().getName();
   if (FieldName == null) {
       return;
   }

    //Update Address based on field changed
   strAfrigisLookup = Xrm.Page.getAttribute(FieldName).getValue();
   if (strAfrigisLookup == null) {
       return;
   }
   var ODataQuery = oDataPath + "/stb_shortcourseSet?$select=stb_CertificateTypeTwoOptions"

    var ReturnData = LibGetData(ODataQuery);

    if (ReturnData && ReturnData.d != null) {
        if (ReturnData.d.results.length > 0) {
           // var CertificateType = Xrm.Page.getAttribute("stb_CertificateTypeTwoOptions").getValue();
            //Check Certificate Type
          //  if (CertificateType == 1) {
                LibSubmitToField('stb_CertificateTypeTwoOptions', ReturnData.d.results[0].stb_CertificateTypeTwoOptions);
                Xrm.Page.data.entity.save();
            }
        }
    }
//}
*/
//END
//
//End Reusable Functions for Offering Entity