﻿ /// <reference path="stb_EOHMCStandardLibrary.js" />

//Student Lifecycle------------------------------------------------------------------------------------------------------------------

//Status Reasons
var statusApplied = 1 //old 1
var StatusRequestQualified = 956390027
var statusQualified = 956390017 //old 956390017
var statusNotQualified = 956390019
var statusApproved = 956390018 //old 956390018
var statusNotApproved = 956390020
var statusRequestAccepted = 956390028
var statusAccepted = 956390021
var statusNotAccepted = 956390022
var statusPaid = 956390023
var statusNotPaid = 956390024
var statusRequestAdmitted = 956390000 //old 956390000
var statusAdmitted = 956390001 //old 956390001
var statusWaitlisted = 956390003 //old 956390003
var statusNotAdmitted = 956390004 //old 956390004
var statusRequestRegistered = 956390005 //old 956390005
var statusRegistered = 956390006 //old 956390006
var statusRequestPassed = 956390008 //old 956390008
var statusPassed = 956390009 //old 956390009
var statusCompleted = 956390025
var statusRequestFailed = 956390011 //old 956390011
var statusFailed = 956390012 //old 956390012
var statusIncompleted = 956390026
var statusCancelled = 956390014 //old 956390014

//On Load Functions
//
//
//
//On Load Function Group
function OnLoad() {
    ClearCancelReason();
    MarksLoad();
}
//END-------------------------------------------------------------------------------------------------------------------------------

//Function to Clear Cancel Reason if Status Reaosn is Not Cancelled-----------------------------------------------------------------
function ClearCancelReason() {
    debugger;
    //Not Cancelled
    var intStatusValue = Xrm.Page.getAttribute("statuscode").getValue();
    if (intStatusValue != statusCancelled) {
        LibSubmitToField("stb_cancelreasonoptionset", null);
    }
}
//END-------------------------------------------------------------------------------------------------------------------------------
//
//
//
//End On Load Functions

//Ribbon Controlled Functions
//
//
//
//Function to post Qualified Status of an enrollment-------------------------------------------------------------------------------
function Qualified() {
    debugger;
    
    //var guidRecord = Xrm.Page.data.entity.getId().substring(1, 37);

    //Get the Short Course Offering
    var offeringLookup = Xrm.Page.getAttribute("stb_shortcourseofferinglookup").getValue();
    if (offeringLookup != null) {
        offeringGuid = offeringLookup[0].id.substring(1, 37);
    }
       
    //var oDataQuery = oDataPath + "/stb_shortcourseofferingSet?$select=stb_shortcourseofferingId&$filter=stb_shortcourseofferingId '" + offeringGuid + "'stb_countQualified,stb_MaxParticipants";

    //"/stb_shortcourseofferingSet?$select=stb_EndDate,stb_shortcourseoffering_ShortCourseLookup/stb_CertificateTypeTwoOptions&$expand=stb_shortcourseoffering_ShortCourseLookup&$filter=stb_shortcourseofferingId eq guid'" + guidShortCourseOffering + "'"
    //"/stb_shortcourseofferingSet?$select=stb_EndDate,stb_shortcourseoffering_ShortCourseLookup/stb_CertificateTypeTwoOptions&$expand=stb_shortcourseoffering_ShortCourseLookup&$filter=stb_shortcourseofferingId eq guid'7CDE7128-6808-E511-827A-402CF4573DF7'"

    var oDataQuery = oDataPath + "/stb_shortcourseofferingSet?$select=stb_countQualified,stb_MaxParticipants,stb_shortcourseofferingId&$filter=stb_shortcourseofferingId eq guid'" + offeringGuid + "'";

    var ReturnData = LibGetData(oDataQuery);

    if (ReturnData && ReturnData.d != null) {
        if (ReturnData.d.results.length > 0) {

            var maxParticipant = ReturnData.d.results[0].stb_MaxParticipants;
            var countQualified = ReturnData.d.results[0].stb_countQualified;
            
            
            //Check if Max Participant is != Null
            if (maxParticipant == null) {
                alert("Short Course Offering has no Max Participant value specified");
                return;
            }

            //Check if countQualififed == Max Participant
            else if (countQualified >= maxParticipant) {
                alert("Amount of Students exceed the number of Max Participants for this Offering - Please Waitlist the Student");
                return;
            }
            else {                
                //Set Status Reason to Qualified
                var intStatusValue = StatusRequestQualified;
                LibSubmitToField('statuscode', intStatusValue);
                Xrm.Page.data.entity.save();
            }
            //return;
        }
    }   
}
//END-------------------------------------------------------------------------------------------------------------------------------

function NotQualified() {
    debugger;
    //Set Status Reason to Not Qualified
    var intStatusValue = statusNotQualified;
    LibSubmitToField('statuscode', intStatusValue);
    Xrm.Page.data.entity.save();
}
//END-------------------------------------------------------------------------------------------------------------------------------

//Function to post Approved Status of an enrollment-------------------------------------------------------------------------------
function Approved() {
    debugger;
    //Set Status Reason to Approved or Qualified
    var intStatusValue = statusApproved;
    var intStatusValueQ = StatusRequestQualified;
        
    var offeringLookup = Xrm.Page.getAttribute("stb_shortcourseofferinglookup").getValue();
    
    if (offeringLookup != null) {
        offeringGuid = offeringLookup[0].id.substring(1, 37);
    }

    //stb_shortcourseofferingSet?$select=stb_OfferingTypeOptionSet&$filter=stb_shortcourseofferingId eq guid''
    var oDataQuery = oDataPath + "/stb_shortcourseofferingSet?$select=stb_OfferingTypeOptionSet,stb_shortcourseofferingId&$filter=stb_shortcourseofferingId eq guid'" + offeringGuid + "'";
    
    var ReturnData = LibGetData(oDataQuery);

    if (ReturnData && ReturnData.d != null) {
        if (ReturnData.d.results.length > 0) {

            var offeringType = ReturnData.d.results[0].stb_OfferingTypeOptionSet;
        //check Offering type equals General || GAP                       
            if (offeringType.Value == 1 ||
                offeringType.Value == 3) {
                LibSubmitToField('statuscode', intStatusValueQ);
                Xrm.Page.data.entity.save();
            }
            else {
                LibSubmitToField('statuscode', intStatusValue);
                Xrm.Page.data.entity.save();
            }
        }
        else {
            return;
        }
    }
}
//END-------------------------------------------------------------------------------------------------------------------------------

//Function to post Not Approved of an enrollment------------------------------------------------------------------------------------
function NotApproved() {
    debugger;
    //Set Status Reason to Not Approved
    var intStatusValue = statusNotApproved;
    LibSubmitToField('statuscode', intStatusValue);
    Xrm.Page.data.entity.save();
}
//End-------------------------------------------------------------------------------------------------------------------------------

//Function to post Accepted of an enrollment----------------------------------------------------------------------------------------
function Accepted(){
    debugger;
    //Set Status Reason to Accepted
    var intStatusValue = statusRequestAccepted;
    LibSubmitToField('statuscode', intStatusValue);
    Xrm.Page.data.entity.save();
}
//End-------------------------------------------------------------------------------------------------------------------------------

//Function to post Not Accepted of an enrollment----------------------------------------------------------------------------------------
function NotAccepted() {
    debugger;
    //Set Status Reason to Accepted
    var intStatusValue = statusNotAccepted;
    LibSubmitToField('statuscode', intStatusValue);
    Xrm.Page.data.entity.save();
}
//End-------------------------------------------------------------------------------------------------------------------------------

//Function to post Admitted Status of an enrollment---------------------------------------------------------------------------------
function Admitted() {
     debugger;
     //Set Status Reason to Request: Admitted
     var intStatusValue = statusRequestAdmitted;
     LibSubmitToField('statuscode', intStatusValue);
     Xrm.Page.data.entity.save();
}
//END-------------------------------------------------------------------------------------------------------------------------------

//Function to post Waitlisted Status of an enrollment-------------------------------------------------------------------------------
function Waitlisted() {
     debugger;
     //Set Status Reason to Waitlisted
     var intStatusValue = statusWaitlisted;
     LibSubmitToField('statuscode', intStatusValue);
     Xrm.Page.data.entity.save();
}
//END-------------------------------------------------------------------------------------------------------------------------------

//Function to post Not-Admitted Status of an enrollment-----------------------------------------------------------------------------
function NotAdmitted() {
    debugger;
    //Set Status Reason to Not Admitted
    var intStatusValue = statusNotAdmitted;
    LibSubmitToField('statuscode', intStatusValue);
    Xrm.Page.data.entity.save();
}
//END-------------------------------------------------------------------------------------------------------------------------------

//Function to post Registered Status of an enrollment-------------------------------------------------------------------------------
function Registered() {
    //debugger;
    //Set Status Reason to Request: Registered
    var intStatusValue = statusRequestRegistered;
    //Check if Short Course Offering has started
    //Get Date
    var todayDate = new Date();
    //var day7 = startdate.getDate() + 7;

    //offset against UTC timezone
    //var dateSystemDate = LibAddDays(now, -1);
    //var dateCurrentDate = dateSystemDate.getFullYear() + "-" + LibStrPad00(dateSystemDate.getMonth() + 1) + "-" + LibStrPad00(dateSystemDate.getDate()) + "T00:22:00.000Z";

    //Get Short Course Offering id
    var lookup = Xrm.Page.getAttribute("stb_shortcourseofferinglookup").getValue();
    if (lookup != null && lookup[0] != null) {
        var guidShortCourseOffering = lookup[0].id;
    }
    else {
        alert("Please select a valid Short Course offering for the record");
        LibSetFieldFocus("stb_shortcourseofferinglookup");
        return;
    }

    //Query Data
    var something = oDataPath + "/stb_shortcourseofferingSet?$select=stb_StartDate, stb_EndDate&$filter=stb_shortcourseofferingId eq guid'" + guidShortCourseOffering + "'";
    var ReturnSomething = LibGetData(something)
    if (ReturnSomething && ReturnSomething.d != null) {
        if (ReturnSomething.d.results.length > 0) {
            //var SD = Xrm.Page.getAttribute("stb_StartDate").getValue();
            var SD = ReturnSomething.d.results[0].stb_StartDate.toString().replace(/\D/g, "");
            var ED = ReturnSomething.d.results[0].stb_EndDate.toString().replace(/\D/g, "");
            var newSD = new Date(parseInt(SD));
            var newED = new Date(parseInt(ED));
            //var Between = oDataPath + "/stb_shortcourseofferingSet?$select=stb_shortcourseofferingId&$filter=stb_shortcourseofferingId eq guid'" + guidShortCourseOffering + "' and stb_StartDate le datetime'" + startDate.toISOString().slice(0, 10) + "' and  stb_EndDate ge datetime'" + startDate.toISOString().slice(0, 10) + "'";
            //var BeforeStart = oDataPath + "/stb_shortcourseofferingSet?$select=stb_shortcourseofferingId&$filter=stb_shortcourseofferingId eq guid'" + guidShortCourseOffering + "' and (stb_StartDate le datetime'" + (new Date(newSD.setDate(newSD.getDate() + 7))).toISOString().slice(0, 10) + "')";
            //var BeforeStart = oDataPath + "/stb_shortcourseofferingSet?$select=stb_shortcourseofferingId&$filter=stb_shortcourseofferingId eq guid'" + guidShortCourseOffering + "' and (stb_StartDate le datetime'" + (new Date(newSD.setDate(newSD.getDate() - 7))).toISOString().slice(0, 10) + "')";

            var newStart = new Date(newSD);
            


            function removeDays(date, days) {
                var result = new Date(date);
                result.setDate(result.getDate() - days);

                return result;
            }

            newStart = removeDays(newStart, 7);

           //newStart.setDate(newStart - 7);
           // var Between = oDataPath + "/stb_shortcourseofferingSet?$select=stb_shortcourseofferingId&$filter=stb_shortcourseofferingId eq guid'" + guidShortCourseOffering + "' and '" + newStart.toISOString().slice(0, 10) + "' le datetime '" + startDate.toISOString().slice(0, 10) + "' and  stb_EndDate ge datetime '" + startDate.toISOString().slice(0, 10) + "'";
            //var Between = oDataPath + "/stb_shortcourseofferingSet?$select=stb_shortcourseofferingId&$filter=stb_shortcourseofferingId eq guid'" + guidShortCourseOffering + "' and stb_StartDate le datetime'" + startDate.toISOString().slice(0, 10) + "' and  stb_EndDate ge datetime'" + startDate.toISOString().slice(0, 10) + "'";
            debugger;
           // var ReturnDataBetween = LibGetData(Between)
           // var ReturnDataBeforeStart = LibGetData(BeforeStart)
            //if (ReturnDataBetween && ReturnDataBetween.d != null ) {
            // if (ReturnDataBetween && ReturnDataBetween.d != null || ReturnDataBeforeStart && ReturnDataBeforeStart.d != null) {
            // && startDate.setHours(0, 0, 0, 0) >= newED.setHours(0, 0, 0, 0)
            if (todayDate.setHours(0, 0, 0, 0) >= newStart.setHours(0, 0, 0, 0) && todayDate.setHours(0, 0, 0, 0) <= newED.setHours(0, 0, 0, 0)) {
                    var PaidStatus = Xrm.Page.getAttribute("stb_paymentindicatortwooptions").getValue();
                    //Check if Student has Paid
                    if (PaidStatus == 1) {
                        LibSubmitToField('statuscode', intStatusValue);
                        Xrm.Page.data.entity.save();
                    }
                    else {
                        alert("Student has not yet been marked as Paid");
                        LibSetFieldFocus("stb_paymentindicatortwooptions");
                        return;
                    }
                }
                else {
                    alert("Short Course offering date is not valid to be registered for");
                }
            //}
            }
        }
}
//END-------------------------------------------------------------------------------------------------------------------------------

//Function to post Passed Status of an enrollment-----------------------------------------------------------------------------------
function Passed() {
    debugger;
    //Set Status Reason to Request: Passed
    var intStatusValue = statusRequestPassed;

    //Check Certificate type and related fields on Enrollment
    lookup = Xrm.Page.getAttribute('stb_shortcourseofferinglookup').getValue();
    if (lookup != null && lookup[0] != null) {
        var guidShortCourseOffering = lookup[0].id.substring(1, 37);
    }
    else {
        alert("Please select a valid Short Course offering for the record");
        Xrm.Page.ui.controls.get("stb_shortcourseofferinglookup").setFocus();
        return;
    }

    //var ODataQuery = oDataPath + "/stb_shortcourseofferingSet?$select=stb_stb_shortcourse_stb_shortcourseoffering_ShortCourseLookup/stb_CertificateTypeTwoOptions, stb_EndDate&$expand=stb_stb_shortcourse_stb_shortcourseoffering_ShortCourseLookup&$filter=stb_shortcourseofferingId eq guid'" + guidShortCourseOffering + "'"
    //var ODataQuery = oDataPath + "/stb_shortcourseofferingSet?$select=stb_EndDate,stb_shortcourseoffering_ShortCourseLookup/stb_CertificateTypeTwoOptions&$expand=stb_shortcourseoffering_SalesPersonLookup,stb_shortcourseoffering_ShortCourseLookup&$filter=stb_shortcourseofferingId eq guid'" + guidShortCourseOffering + "'"
    var ODataQuery = oDataPath + "/stb_shortcourseofferingSet?$select=stb_EndDate,stb_shortcourseoffering_ShortCourseLookup/stb_CertificateTypeTwoOptions&$expand=stb_shortcourseoffering_ShortCourseLookup&$filter=stb_shortcourseofferingId eq guid'" + guidShortCourseOffering + "'"

    var returnData = LibGetData(ODataQuery);

    if (returnData && returnData.d != null) {
        if (returnData.d.results.length > 0) {
            var today = new Date();
            var endDate = LibJulianToDate(returnData.d.results[0].stb_EndDate);

            //Attendance
            if (returnData.d.results[0].stb_shortcourseoffering_ShortCourseLookup.stb_CertificateTypeTwoOptions == false) {
                //Check if End date is after today and Allow passed = No, display warning and make no changes
                if (LibDates.compare(endDate, today) == 1
                        && (Xrm.Page.getAttribute("stb_allowsetpassedtwooptions").getValue() == false
                            || Xrm.Page.getAttribute("stb_allowsetpassedtwooptions").getValue() == null)) {
                    alert("Attendance certificates may be requested for participants prior to the short course having been fully attended by the participants and if specifically motivated. \n\nPlease contact the Short Courses Division for further assistance.");
                    return;
                }
                    // if (LibDates.compare(endDate, today) == 1) {
                    //   alert("Attendance certificates may be requested for participants prior to the short course having been fully attended by the participants and if specifically motivated. \n\nPlease contact the Short Courses Division for further assistance.");
                    //    return;
                    //}
                    //Check if Attendance % has been captured otherwise display alert and make no change
                else if (Xrm.Page.getAttribute('stb_attendance').getValue() == null) {
                    alert("Please enter Attendance % to Pass Student");
                    Xrm.Page.ui.controls.get("stb_attendance").setFocus();
                    return;
                }
                else {
                    LibSubmitToField("statuscode", intStatusValue);
                    Xrm.Page.data.entity.save();
                }
            }
                //Competence
            else {
                //Check if End date is after today, display alert and make no changes
                if (LibDates.compare(endDate, today) == 1) {
                    alert('You may not issue competence certificates prior to the completion date of the short course. \n\nCompetence certificates may only be issued to candidates once they have completed the short course and formal process of assessment successfully and within the specific dates indicated and approved during the course registration process.');
                    return;
                }
                    //Check if Marks % has been captured otherwise display alert and make no change
                else if (Xrm.Page.getAttribute('stb_marks').getValue() == null) {
                    alert("Please enter Marks % to Pass Student");
                    Xrm.Page.ui.controls.get("stb_marks").setFocus();
                    return;
                }
                else {
                    LibSubmitToField("statuscode", intStatusValue);
                    Xrm.Page.data.entity.save();
                }
            }

        }
    }

    ////Double Prompt
    //if (confirm("Are you sure you want to proceed?\r\n \r\nCertificate Number will be issued")) {
    //    LibSubmitToField('statuscode', intStatusValue);
    //    Xrm.Page.data.entity.save();
    //}
}
//END-------------------------------------------------------------------------------------------------------------------------------

//Function to post Failed Status of an enrollment-----------------------------------------------------------------------------------
function Failed() {
    debugger;
    //Set Status Reason to Request: Failed
    var intStatusValue = statusRequestFailed;
    LibSubmitToField('statuscode', intStatusValue);
    Xrm.Page.data.entity.save();
}
//END-------------------------------------------------------------------------------------------------------------------------------

//Function to post Cancelled Status of an enrollment--------------------------------------------------------------------------------
function Cancelled() {
    debugger;
    //Set Status Reason to Cancelled
    var intStatusValue = statusCancelled;
    //Verify that Cancel Reason is populated
    if (Xrm.Page.getAttribute("stb_cancelreasonoptionset").getValue() == null) {
        alert("You must capture a Cancel Reason in order to Cancel a Enrollment");
        LibSetFieldFocus("stb_cancelreasonoptionset");
        return;
    }
    else {
        LibSubmitToField('statuscode', intStatusValue);
        Xrm.Page.data.entity.save();
    }
}
//END-------------------------------------------------------------------------------------------------------------------------------

//Swop out redirect-----------------------------------------------------------------------------------------------------------------
function SwopOut() {
    debugger;
    /* Not in use at this point
    if (confirm('Record will be saved and closed to complete this operation')) {
        //Check if Valid short course
        var lookup = Xrm.Page.getAttribute("stb_shortcourseofferinglookup").getValue();
        if (lookup == null) {
            alert("Please select a valid Short Course offering for the record");
            LibSetFieldFocus("stb_shortcourseofferinglookup");
            return;
        }
        //Add Webtoken Object for creation of Record
        var WebToken = new Object();
        //Get Guid from current record
        WebToken.stb_Name = Xrm.Page.data.entity.getId().substring(1, 37);
        //Create Web Token
        var ReturnData = LibCreateNewRecord(WebToken, "stb_webtokenSet");
        if (ReturnData && ReturnData.d != null) {
            var tokenGuid = ReturnData.d.stb_webtokenId;
        }
        //Close current window and open external URL
        Xrm.Page.data.entity.save("saveandclose");
        URL = SwopOutBaseURL + tokenGuid;
        window.open(URL, "_blank", "height=" + screen.height + ",width=" + screen.width + ",menubar=yes,resizable=yes,toolbar=yes,location=yes,scrollbars=yes,status=yes,titlebar=yes");
    }
    else {
        return;
    }
    */
}
//END-------------------------------------------------------------------------------------------------------------------------------

// Marks Integration - manual entry
function Marks(executionContext) {
    debugger;
    //Excecution context must be switched on for this to work
    var FieldName = executionContext.getEventSource().getName();
    if (FieldName == null) {
        return;
    }
    strMarks = Xrm.Page.getAttribute(FieldName).getValue();

    var intStatusValue = Xrm.Page.getAttribute("statuscode").getValue();

    if (intStatusValue == statusRegistered && strMarks != null){
        if (strMarks >= 50) {
            Passed();
        } else {
            alert("Please Mark the Student as Failed");
        }
    } else {
        return;
    }
}

//on load auto function
function MarksLoad() {
    debugger;
    var intStatusValue = Xrm.Page.getAttribute("statuscode").getValue();
     
    if (intStatusValue == statusRegistered) {

        var Attendance = Xrm.Page.getAttribute("stb_attendance").getValue();
        var Marks = Xrm.Page.getAttribute("stb_marks").getValue();

        if (Attendance != null || Marks != null) {
            if (Attendance >= 50 || Marks >= 50) {
                 Passed();
            } else {
                alert("Please Mark the Student as Failed");
            }
        } else {
            return;
        }
   } else {
        return;
   }
}

//END------------------------------------------------------------------------------------------------------------------------------

    //Functions to control availability of buttons in the ribbon
function EnableQualifiedButton() {
    debugger;
    var intStatusValue = Xrm.Page.getAttribute("statuscode").getValue();
    //Approved
    if (intStatusValue == statusApproved) {
        return true;
    }
}
    //END-------------------------------------------------------------------------------------------------------------------------------
function EnableNotQualifiedButton() {
    debugger;
    var intStatusValue = Xrm.Page.getAttribute("statuscode").getValue();
    //Approved
    if (intStatusValue == statusApproved) {
        return true;
    }
}
    //END-------------------------------------------------------------------------------------------------------------------------------
function EnableApprovedButton() {
    debugger;
    var intStatusValue = Xrm.Page.getAttribute("statuscode").getValue();
    //Applied
    if (intStatusValue == statusApplied) {
        return true;
    }
}
    //END-------------------------------------------------------------------------------------------------------------------------------
function EnableNotApprovedButton() {
     debugger;
     var intStatusValue = Xrm.Page.getAttribute("statuscode").getValue();
     //Applied
     if (intStatusValue == statusApplied) {
          return true;
     }
}
    //END-------------------------------------------------------------------------------------------------------------------------------
function EnableWaitlistedButton() {
     debugger;
     var intStatusValue = Xrm.Page.getAttribute("statuscode").getValue();
     //Approved
     if (intStatusValue == statusQualified) {
          return true;
     }
}
    //END-------------------------------------------------------------------------------------------------------------------------------
function EnableAcceptedButton() {
     debugger;
     var intStatusValue = Xrm.Page.getAttribute("statuscode").getValue();
        //Qualified or Waitlisted
     if (intStatusValue == statusQualified || intStatusValue == statusWaitlisted) {
          return true;
     }
}
    //END-------------------------------------------------------------------------------------------------------------------------------
function EnableNotAcceptedButton() {
    debugger;
    var intStatusValue = Xrm.Page.getAttribute("statuscode").getValue();
    //Qualiffied or Waitlisted
    if (intStatusValue == statusQualified || intStatusValue == statusWaitlisted) {
        return true;
    }
}
    //END-------------------------------------------------------------------------------------------------------------------------------
function EnableAdmittedButton() {
    debugger;
    var intStatusValue = Xrm.Page.getAttribute("statuscode").getValue();
    //Accepted
    if (intStatusValue == statusAccepted) {
        return true;
    }
}

//END-------------------------------------------------------------------------------------------------------------------------------

function EnableNotAdmittedButton() {
    debugger;
    var intStatusValue = Xrm.Page.getAttribute("statuscode").getValue();
    //Accepted
    if (intStatusValue == statusAccepted) {
        return true;
    }
}
//END-------------------------------------------------------------------------------------------------------------------------------

function EnableRegisteredButton() {
     debugger;
     var intStatusValue = Xrm.Page.getAttribute("statuscode").getValue();
     //Admitted
     if (intStatusValue == statusAdmitted) {
          return true;
     }
}
    //END-------------------------------------------------------------------------------------------------------------------------------
function EnablePassedButton() {
    debugger;
    var intStatusValue = Xrm.Page.getAttribute("statuscode").getValue();
    //Registered
    if (intStatusValue == statusRegistered) {
         return true;
    }
}
    //END-------------------------------------------------------------------------------------------------------------------------------
function EnableFailedButton() {
     debugger;
     var intStatusValue = Xrm.Page.getAttribute("statuscode").getValue();
     //Registered
     if (intStatusValue == statusRegistered) {
          return true;
     }
}
    //END-------------------------------------------------------------------------------------------------------------------------------
function EnableCancelledButton() {
     debugger;
     //Always enabled
     return true;
}
    //END-------------------------------------------------------------------------------------------------------------------------------
function EnableSwopOutButton() {
     debugger;
      /* Not in use at this point
      var intStatusValue = Xrm.Page.getAttribute("statuscode").getValue();
      //Registered
      if (intStatusValue == statusRegistered) {
          return true;
      }
      */
     return false;
}
    //END-------------------------------------------------------------------------------------------------------------------------------
    //
    //
    //
    //End Ribbon Controlled Functions