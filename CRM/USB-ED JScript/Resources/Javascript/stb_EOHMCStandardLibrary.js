//Global Variables
//
//
//
//CRM Server Dynamic URL
var baseUrl = Xrm.Page.context.getClientUrl();
var oDataPath = baseUrl + "/XRMServices/2011/OrganizationData.svc";

//Application form Dynamic URL
var applicationFormBaseURL = "http://" + window.location.hostname + ":84/UsbEdRedirect/ApplicationFormInsideCrm.aspx?";

//Request Certificate Dynamic URL
var requestCertificateBaseURL = "http://" + window.location.hostname + ":84/UsbEdRedirect/CertificatePrinting.aspx?";
//
//
//
//End Global Variables

//Standard Libraries
//
//
//
//Query Record through Odata--------------------------------------------------------------------------------------------------------
function LibGetData(ODataQuery) {
    var ReturnData;
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        url: ODataQuery,
        async: false,
        beforeSend: function (XMLHttpRequest) {
            XMLHttpRequest.setRequestHeader("Accept", "application/json");
        },
        success: function (data, textStatus, XmlHttpRequest) {
            if (data && data.d != null) {
                ReturnData = data;
            }
        },
        error: function (XmlHttpRequest, textStatus, errorThrown) {
            alert("Error :  has occurred during retrieval of Data with error: " + XmlHttpRequest.responseText);
        }
    });
    return ReturnData;
}
//END-------------------------------------------------------------------------------------------------------------------------------

//Create New Record through Odata---------------------------------------------------------------------------------------------------
function LibCreateNewRecord(entityObject, odataSetName) {
    var jsonEntity = window.JSON.stringify(entityObject);
    //Synchronous AJAX function to Update a CRM record using OData
    jQuery.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        data: jsonEntity,
        url: oDataPath + "/" + odataSetName,
        async: false,
        beforeSend: function (XMLHttpRequest) {
            XMLHttpRequest.setRequestHeader("Accept", "application/json");
        },
        success: function (data, textStatus, XmlHttpRequest) {
            if (data && data.d != null) {
                ReturnData = data;
            }
        },
        error: function (XmlHttpRequest, textStatus, errorThrown) {
            if (XmlHttpRequest && XmlHttpRequest.responseText) {
                alert("Error while creating " + odataSetName + " ; Error � " + XmlHttpRequest.responseText);
            }
        }
    });
    return ReturnData;
}
//END-------------------------------------------------------------------------------------------------------------------------------

//Update Record through Odata-------------------------------------------------------------------------------------------------------
function LibUpdateRecord(entityObject, odataSetName) {
    var jsonEntity = window.JSON.stringify(entityObject);
    //Synchronous AJAX function to Update a CRM record using OData
    jQuery.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        data: jsonEntity,
        url: oDataPath + "/" + odataSetName,
        async: false,
        beforeSend: function (XMLHttpRequest) {
            //Need to complete this
        },
        success: function (data, textStatus, XmlHttpRequest) {
            if (data && data.d != null) {
                ReturnData = data;
            }
        },
        error: function (XmlHttpRequest, textStatus, errorThrown) {
            if (XmlHttpRequest && XmlHttpRequest.responseText) {
                alert("Error while updating " + odataSetName + " ; Error � " + XmlHttpRequest.responseText);
            }
        }
    });
    return ReturnData;
}
//END-------------------------------------------------------------------------------------------------------------------------------

//Submit value to Field-------------------------------------------------------------------------------------------------------------
function LibSubmitToField(FieldName, Value) {
    Xrm.Page.getAttribute(FieldName).setSubmitMode("always");
    Xrm.Page.getAttribute(FieldName).setValue(Value);
}
//END-------------------------------------------------------------------------------------------------------------------------------

//Submit value to Lookup------------------------------------------------------------------------------------------------------------
function LibSubmitToLookup(FieldName, Value) {
    Xrm.Page.getAttribute(FieldName).setSubmitMode("always");
    var name = Value[0].name;
    var id = Value[0].id;
    var entityType = Value[0].entityType;
    Xrm.Page.getAttribute(FieldName).setValue([{
        id: id,
        name: name,
        entityType: entityType
    }]);
}
//END-------------------------------------------------------------------------------------------------------------------------------

//Set Field Disabled----------------------------------------------------------------------------------------------------------------
function LibSetFieldDisabled(FieldName) {
    Xrm.Page.ui.controls.get(FieldName).setDisabled(true);
}
//END-------------------------------------------------------------------------------------------------------------------------------

//Set Field Enabled----------------------------------------------------------------------------------------------------------------
function LibSetFieldEnabled(FieldName) {
    Xrm.Page.ui.controls.get(FieldName).setDisabled(false);
}
//END-------------------------------------------------------------------------------------------------------------------------------

//Set Field Focus-------------------------------------------------------------------------------------------------------------------
function LibSetFieldFocus(FieldName) {
    Xrm.Page.ui.controls.get(FieldName).setFocus();
}
//END-------------------------------------------------------------------------------------------------------------------------------

//Pad values as 00------------------------------------------------------------------------------------------------------------------
function LibStrPad00(s) {
    s = s + '';
    if (s.length === 1) s = '0' + s;
    return s;
}
//END-------------------------------------------------------------------------------------------------------------------------------

//Add Days to a date
function LibAddDays(date, offsetdays) {
    var result = new Date(date);
    result.setDate(date.getDate() + offsetdays);
    return result;
}
//END-------------------------------------------------------------------------------------------------------------------------------

// Julian Date Function - Date Validation 
function LibJulianToDate(julianDate) {
    julianDate = julianDate.replace("/Date(", "");
    julianDate = julianDate.replace(")/", "");
    var dateValue = new Date(parseInt(julianDate, 10));
    dateValue.setDate(dateValue.getDate());
    return dateValue;
}

// Source: http://stackoverflow.com/questions/497790
var LibDates = {
    convert: function (d) {
        // Converts the date in d to a date-object. The input can be:
        //   a date object: returned without modification
        //  an array      : Interpreted as [year,month,day]. NOTE: month is 0-11.
        //   a number     : Interpreted as number of milliseconds
        //                  since 1 Jan 1970 (a timestamp) 
        //   a string     : Any format supported by the javascript engine, like
        //                  "YYYY/MM/DD", "MM/DD/YYYY", "Jan 31 2009" etc.
        //  an object     : Interpreted as an object with year, month and date
        //                  attributes.  **NOTE** month is 0-11.
        return (
            d.constructor === Date ? d :
            d.constructor === Array ? new Date(d[0], d[1], d[2]) :
            d.constructor === Number ? new Date(d) :
            d.constructor === String ? new Date(d) :
            typeof d === "object" ? new Date(d.year, d.month, d.date) :
            NaN
        );
    },
    compare: function (a, b) {
        // Compare two dates (could be of any type supported by the convert
        // function above) and returns:
        //  -1 : if a < b
        //   0 : if a = b
        //   1 : if a > b
        // NaN : if a or b is an illegal date
        // NOTE: The code inside isFinite does an assignment (=).
        return (
            isFinite(a = this.convert(a).valueOf()) &&
            isFinite(b = this.convert(b).valueOf()) ?
            (a > b) - (a < b) :
            NaN
        );
    },
    inRange: function (d, start, end) {
        // Checks if date in d is between dates in start and end.
        // Returns a boolean or NaN:
        //    true  : if d is between start and end (inclusive)
        //    false : if d is before start or after end
        //    NaN   : if one or more of the dates is illegal.
        // NOTE: The code inside isFinite does an assignment (=).
        return (
             isFinite(d = this.convert(d).valueOf()) &&
             isFinite(start = this.convert(start).valueOf()) &&
             isFinite(end = this.convert(end).valueOf()) ?
             start <= d && d <= end :
             NaN
         );
    }
}
//
//
//
//End Standard Libraries
