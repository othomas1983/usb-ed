﻿ /// <reference path="EOHMC Standard Library.js" />
/// <reference path="Intellisense Files/Venue Form - Information.js" />

//On Change Functions
//
//
//
//Populate City, Suburb and Postal Code from Afigis Field---------------------------------------------------------------------------
function UpdateAddress() {
    debugger;
    //Update Address based on above return
    strAfrigisLookup = Xrm.Page.getAttribute("stb_afrigislookup").getValue();
    if (strAfrigisLookup == null) {
        return;
    }
    else {
        guidAfrigisGuid = strAfrigisLookup[0].id;
        //Query Information on Afrigis Entity
        var ODataQuery = oDataPath + "/stb_afrigisSet?$select=stb_City,stb_Suburb,stb_PostalCode&$filter=stb_afrigisId eq guid'" + guidAfrigisGuid + "'";
        var ReturnData = LibGetData(ODataQuery);
        if (ReturnData && ReturnData.d != null) {
            if (ReturnData.d.results.length > 0) {
                LibSubmitToField('stb_citytown', ReturnData.d.results[0].stb_City);
                LibSubmitToField('stb_suburb', ReturnData.d.results[0].stb_Suburb);
                LibSubmitToField('stb_postalcode', ReturnData.d.results[0].stb_PostalCode);
            }
        }
    }
}
//END-------------------------------------------------------------------------------------------------------------------------------
//
//
//
//End On Change Functions
