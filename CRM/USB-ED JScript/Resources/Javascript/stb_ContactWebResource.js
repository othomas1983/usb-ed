﻿ /// <reference path="EOHMC Standard Library.js" />
/// <reference path="Intellisense Files/Contact Form - Information.js" />

//On Change Functions
//
//
//
//Populate City, Suburb and Postal Code from Afigis Field (Requires Execution Context)----------------------------------------------
function UpdateAddress(executionContext) {
    debugger;
    //Excecution context must be switched on for this to work
    var FieldName = executionContext.getEventSource().getName();
    if (FieldName == null) {
        return;
    }
    //Update Address based on field changed
    strAfrigisLookup = Xrm.Page.getAttribute(FieldName).getValue();
    if (strAfrigisLookup == null) {
        return;
    }
    else {
        guidAfrigisGuid = strAfrigisLookup[0].id;
        //Query Information on Afrigis Entity
        var ODataQuery = oDataPath + "/stb_afrigisSet?$select=stb_City,stb_Suburb,stb_PostalCode&$filter=stb_afrigisId eq guid'" + guidAfrigisGuid + "'";
        var ReturnData = LibGetData(ODataQuery);
        if (ReturnData && ReturnData.d != null) {
            if (ReturnData.d.results.length > 0) {
                //Address 1
                if (FieldName == "stb_address1_afrigislookup") {
                    LibSubmitToField('stb_address1_city', ReturnData.d.results[0].stb_City);
                    LibSubmitToField('stb_address1_suburb', ReturnData.d.results[0].stb_Suburb);
                    LibSubmitToField('stb_address1_postalcode', ReturnData.d.results[0].stb_PostalCode);
                }
                //Address 2
                if (FieldName == "stb_address2_afrigislookup") {
                    LibSubmitToField('stb_address2_city', ReturnData.d.results[0].stb_City);
                    LibSubmitToField('stb_address2_suburb', ReturnData.d.results[0].stb_Suburb);
                    LibSubmitToField('stb_address2_postalcode', ReturnData.d.results[0].stb_PostalCode);
                }
            }
        }
    }
}
//END-------------------------------------------------------------------------------------------------------------------------------
//
//
//
//End Ribbon Controlled Functions


//Update DOB
/*function UpdateDOB(executionContext) {
    debugger;

    var DOB = executionContext.getEventSource().getName();
    if (DOB == null) {
        return;
    }

    //Update DOB with Birtday
    var strDOB = Xrm.Page.getAttribute(DOB).getValue();
    if (strDOB == null) {
        return;
    }
    else {
          //strDOB = str.replace(/-/g, "");
         //strDOB = Xrm.Page.getAttribute("birtdate").getValue();
        // var newDOB = strDOB.replace(/[^a-zA-Z 0-9]+/g, '');
        strDOB = strDOB.replace("-", "");
        LibSubmitToField('strDOB');
        Xrm.Page.data.entity.save();
    }
}*/
//END-------------------------------------------------------------------------------------------------------------------------------