var EntityLogicalName = "campaign";
var Form_6356ff2a_bbbe_49fb_9da2_160b08865688_Properties = {
    actualend: "actualend",
    actualstart: "actualstart",
    budgetedcost: "budgetedcost",
    createdon: "createdon",
    expectedrevenue: "expectedrevenue",
    istemplate: "istemplate",
    modifiedby: "modifiedby",
    modifiedon: "modifiedon",
    name: "name",
    objective: "objective",
    othercost: "othercost",
    ownerid: "ownerid",
    proposedend: "proposedend",
    proposedstart: "proposedstart",
    statuscode: "statuscode",
    totalactualcost: "totalactualcost",
    totalcampaignactivityactualcost: "totalcampaignactivityactualcost",
    transactioncurrencyid: "transactioncurrencyid",
    typecode: "typecode",
    usb_cateringtwooptions: "usb_cateringtwooptions",
    usb_city: "usb_city",
    usb_cocktailfunctiontwooptions: "usb_cocktailfunctiontwooptions",
    usb_country: "usb_country",
    usb_eventcategoryoptionset: "usb_eventcategoryoptionset",
    usb_eventtypeoptionset: "usb_eventtypeoptionset",
    usb_htmlcode: "usb_htmlcode",
    usb_location: "usb_location",
    usb_maximumeventcapacity: "usb_maximumeventcapacity",
    usb_partnerfunctiontwooptions: "usb_partnerfunctiontwooptions",
    usb_postalcode: "usb_postalcode",
    usb_previewhtmltwooptions: "usb_previewhtmltwooptions",
    usb_registrationcount: "usb_registrationcount",
    usb_stateorprovince: "usb_stateorprovince",
    usb_street1: "usb_street1",
    usb_street2: "usb_street2",
    usb_street3: "usb_street3"
};

var Form_6356ff2a_bbbe_49fb_9da2_160b08865688_Controls = {
    Activities: "Activities",
    actualend: "actualend",
    actualstart: "actualstart",
    budgetedcost: "budgetedcost",
    createdon: "createdon",
    header_expectedrevenue: "header_expectedrevenue",
    header_istemplate: "header_istemplate",
    header_ownerid: "header_ownerid",
    header_statuscode: "header_statuscode",
    IFRAME_HTMLPreview: "IFRAME_HTMLPreview",
    Leads: "Leads",
    Lists: "Lists",
    modifiedby: "modifiedby",
    modifiedon: "modifiedon",
    name: "name",
    notescontrol: "notescontrol",
    objective: "objective",
    othercost: "othercost",
    ownerid: "ownerid",
    proposedend: "proposedend",
    proposedstart: "proposedstart",
    Responses: "Responses",
    totalactualcost: "totalactualcost",
    totalcampaignactivityactualcost: "totalcampaignactivityactualcost",
    transactioncurrencyid: "transactioncurrencyid",
    typecode: "typecode",
    usb_cateringtwooptions: "usb_cateringtwooptions",
    usb_city: "usb_city",
    usb_cocktailfunctiontwooptions: "usb_cocktailfunctiontwooptions",
    usb_country: "usb_country",
    usb_eventcategoryoptionset: "usb_eventcategoryoptionset",
    usb_eventtypeoptionset: "usb_eventtypeoptionset",
    usb_htmlcode: "usb_htmlcode",
    usb_location: "usb_location",
    usb_maximumeventcapacity: "usb_maximumeventcapacity",
    usb_partnerfunctiontwooptions: "usb_partnerfunctiontwooptions",
    usb_postalcode: "usb_postalcode",
    usb_previewhtmltwooptions: "usb_previewhtmltwooptions",
    usb_registrationcount: "usb_registrationcount",
    usb_stateorprovince: "usb_stateorprovince",
    usb_street1: "usb_street1",
    usb_street2: "usb_street2",
    usb_street3: "usb_street3"
};

var pageData = {
    "Event": "none",
    "SaveMode": 1,
    "EventSource": null,
    "AuthenticationHeader": "",
    "CurrentTheme": "Default",
    "OrgLcid": 1033,
    "OrgUniqueName": "",
    "QueryStringParameters": {
        "_gridType": "4400",
        "etc": "4400",
        "id": "",
        "pagemode": "iframe",
        "preloadcache": "1344548892170",
        "rskey": "141637534"
    },
    "ServerUrl": "",
    "UserId": "",
    "UserLcid": 1033,
    "UserRoles": [""],
    "isOutlookClient": false,
    "isOutlookOnline": true,
    "DataXml": "",
    "EntityName": "campaign",
    "Id": "",
    "IsDirty": false,
    "CurrentControl": "",
    "CurrentForm": null,
    "Forms": [],
    "FormType": 2,
    "ViewPortHeight": 558,
    "ViewPortWidth": 1231,
    "Attributes": [{
        "Name": "actualend",
        "Value": null,
        "Type": "datetime",
        "Format": "date",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Controls": [{
            "Name": "actualend"
        }]
    },
    {
        "Name": "actualstart",
        "Value": null,
        "Type": "datetime",
        "Format": "date",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Controls": [{
            "Name": "actualstart"
        }]
    },
    {
        "Name": "budgetedcost",
        "Value": null,
        "Type": "double",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Max": 1000000000000,
        "Min": 0,
        "Precision": 2,
        "Controls": [{
            "Name": "budgetedcost"
        }]
    },
    {
        "Name": "createdon",
        "Value": null,
        "Type": "datetime",
        "Format": "date",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": false,
            "canCreate": false
        },
        "Controls": [{
            "Name": "createdon"
        }]
    },
    {
        "Name": "expectedrevenue",
        "Value": null,
        "Type": "double",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Max": 1000000000000,
        "Min": 0,
        "Precision": 2,
        "Controls": [{
            "Name": "header_expectedrevenue"
        }]
    },
    {
        "Name": "istemplate",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": false,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "header_istemplate"
        }]
    },
    {
        "Name": "modifiedby",
        "Value": [{
            "entityType": "Unknown",
            "id": "{27ACAE0A-5D79-E111-BBD3-2657AEB3167B}",
            "name": "Temp"
        }],
        "Type": "lookup",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": false,
            "canCreate": false
        },
        "Controls": [{
            "Name": "modifiedby"
        }]
    },
    {
        "Name": "modifiedon",
        "Value": null,
        "Type": "datetime",
        "Format": "date",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": false,
            "canCreate": false
        },
        "Controls": [{
            "Name": "modifiedon"
        }]
    },
    {
        "Name": "name",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 128,
        "Controls": [{
            "Name": "name"
        }]
    },
    {
        "Name": "objective",
        "Value": "",
        "Type": "memo",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 2000,
        "Controls": [{
            "Name": "objective"
        }]
    },
    {
        "Name": "othercost",
        "Value": null,
        "Type": "double",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Max": 1000000000000,
        "Min": 0,
        "Precision": 2,
        "Controls": [{
            "Name": "othercost"
        }]
    },
    {
        "Name": "ownerid",
        "Value": [{
            "entityType": "Unknown",
            "id": "{27ACAE0A-5D79-E111-BBD3-2657AEB3167B}",
            "name": "Temp"
        }],
        "Type": "lookup",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": false,
            "canCreate": true
        },
        "Controls": [{
            "Name": "header_ownerid"
        },
        {
            "Name": "ownerid"
        }]
    },
    {
        "Name": "proposedend",
        "Value": null,
        "Type": "datetime",
        "Format": "date",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Controls": [{
            "Name": "proposedend"
        }]
    },
    {
        "Name": "proposedstart",
        "Value": null,
        "Type": "datetime",
        "Format": "date",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Controls": [{
            "Name": "proposedstart"
        }]
    },
    {
        "Name": "statuscode",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Proposed",
            "value": 0
        },
        {
            "text": "Launched",
            "value": 2
        },
        {
            "text": "Completed",
            "value": 3
        },
        {
            "text": "Canceled",
            "value": 4
        },
        {
            "text": "Inactive",
            "value": 6
        }],
        "SelectedOption": {
            "option": "Proposed",
            "value": 0
        },
        "Text": "Proposed",
        "Controls": [{
            "Name": "header_statuscode"
        }]
    },
    {
        "Name": "totalactualcost",
        "Value": null,
        "Type": "double",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": false,
            "canCreate": false
        },
        "Max": 1000000000000,
        "Min": 0,
        "Precision": 2,
        "Controls": [{
            "Name": "totalactualcost"
        }]
    },
    {
        "Name": "totalcampaignactivityactualcost",
        "Value": null,
        "Type": "double",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": false,
            "canCreate": false
        },
        "Max": 1000000000000,
        "Min": 0,
        "Precision": 2,
        "Controls": [{
            "Name": "totalcampaignactivityactualcost"
        }]
    },
    {
        "Name": "transactioncurrencyid",
        "Value": [{
            "entityType": "Unknown",
            "id": "{27ACAE0A-5D79-E111-BBD3-2657AEB3167B}",
            "name": "Temp"
        }],
        "Type": "lookup",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": false,
            "canCreate": true
        },
        "Controls": [{
            "Name": "transactioncurrencyid"
        }]
    },
    {
        "Name": "typecode",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Advertisement",
            "value": 1
        },
        {
            "text": "Direct Marketing",
            "value": 2
        },
        {
            "text": "Event",
            "value": 3
        },
        {
            "text": "Co-branding",
            "value": 4
        },
        {
            "text": "Other",
            "value": 5
        }],
        "SelectedOption": {
            "option": "Advertisement",
            "value": 1
        },
        "Text": "Advertisement",
        "Controls": [{
            "Name": "typecode"
        }]
    },
    {
        "Name": "usb_cateringtwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "usb_cateringtwooptions"
        }]
    },
    {
        "Name": "usb_city",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 80,
        "Controls": [{
            "Name": "usb_city"
        }]
    },
    {
        "Name": "usb_cocktailfunctiontwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "usb_cocktailfunctiontwooptions"
        }]
    },
    {
        "Name": "usb_country",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 80,
        "Controls": [{
            "Name": "usb_country"
        }]
    },
    {
        "Name": "usb_eventcategoryoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Career",
            "value": 1
        },
        {
            "text": "Information Session",
            "value": 2
        },
        {
            "text": "Leader&#39;s Angle",
            "value": 3
        },
        {
            "text": "Masterclass",
            "value": 4
        },
        {
            "text": "USB Alumni",
            "value": 5
        },
        {
            "text": "We Read For You",
            "value": 6
        }],
        "SelectedOption": {
            "option": "Career",
            "value": 1
        },
        "Text": "Career",
        "Controls": [{
            "Name": "usb_eventcategoryoptionset"
        }]
    },
    {
        "Name": "usb_eventtypeoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Conference",
            "value": 1
        },
        {
            "text": "Demonstration",
            "value": 2
        },
        {
            "text": "Discussion",
            "value": 3
        },
        {
            "text": "Networking Event",
            "value": 4
        },
        {
            "text": "Seminar",
            "value": 5
        },
        {
            "text": "Social Gathering",
            "value": 6
        },
        {
            "text": "Webcast",
            "value": 7
        }],
        "SelectedOption": {
            "option": "Conference",
            "value": 1
        },
        "Text": "Conference",
        "Controls": [{
            "Name": "usb_eventtypeoptionset"
        }]
    },
    {
        "Name": "usb_htmlcode",
        "Value": "",
        "Type": "memo",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 10000,
        "Controls": [{
            "Name": "usb_htmlcode"
        }]
    },
    {
        "Name": "usb_location",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 250,
        "Controls": [{
            "Name": "usb_location"
        }]
    },
    {
        "Name": "usb_maximumeventcapacity",
        "Value": null,
        "Type": "integer",
        "Format": "none",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Max": 9999,
        "Min": 0,
        "Precision": 0,
        "Controls": [{
            "Name": "usb_maximumeventcapacity"
        }]
    },
    {
        "Name": "usb_partnerfunctiontwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "usb_partnerfunctiontwooptions"
        }]
    },
    {
        "Name": "usb_postalcode",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 20,
        "Controls": [{
            "Name": "usb_postalcode"
        }]
    },
    {
        "Name": "usb_registrationcount",
        "Value": null,
        "Type": "integer",
        "Format": "none",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Max": 9999,
        "Min": 0,
        "Precision": 0,
        "Controls": [{
            "Name": "usb_registrationcount"
        }]
    },
    {
        "Name": "usb_stateorprovince",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 50,
        "Controls": [{
            "Name": "usb_stateorprovince"
        }]
    },
    {
        "Name": "usb_street1",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 250,
        "Controls": [{
            "Name": "usb_street1"
        }]
    },
    {
        "Name": "usb_street2",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 250,
        "Controls": [{
            "Name": "usb_street2"
        }]
    },
    {
        "Name": "usb_street3",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 250,
        "Controls": [{
            "Name": "usb_street3"
        }]
    }],
    "AttributesLength": 35,
    "Controls": [, {
        "Name": "actualend",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Actual End",
        "Attribute": "actualend"
    },
    {
        "Name": "actualstart",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Actual Start",
        "Attribute": "actualstart"
    },
    {
        "Name": "budgetedcost",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Allocated Budget",
        "Attribute": "budgetedcost"
    },
    {
        "Name": "createdon",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Created On",
        "Attribute": "createdon"
    },
    {
        "Name": "header_expectedrevenue",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Estimated Revenue",
        "Attribute": "expectedrevenue"
    },
    {
        "Name": "header_istemplate",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Template",
        "Attribute": "istemplate"
    },
    {
        "Name": "header_ownerid",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Owner",
        "Attribute": "ownerid"
    },
    {
        "Name": "header_statuscode",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Status Details",
        "Attribute": "statuscode"
    },
    {
        "Name": "modifiedby",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Modified By",
        "Attribute": "modifiedby"
    },
    {
        "Name": "modifiedon",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Modified On",
        "Attribute": "modifiedon"
    },
    {
        "Name": "name",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Name",
        "Attribute": "name"
    },
    {
        "Name": "objective",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Offer",
        "Attribute": "objective"
    },
    {
        "Name": "othercost",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Misc. Cost",
        "Attribute": "othercost"
    },
    {
        "Name": "ownerid",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Owner",
        "Attribute": "ownerid"
    },
    {
        "Name": "proposedend",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Proposed End",
        "Attribute": "proposedend"
    },
    {
        "Name": "proposedstart",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Proposed Start",
        "Attribute": "proposedstart"
    },
    {
        "Name": "totalactualcost",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Total Cost",
        "Attribute": "totalactualcost"
    },
    {
        "Name": "totalcampaignactivityactualcost",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Activity Cost",
        "Attribute": "totalcampaignactivityactualcost"
    },
    {
        "Name": "transactioncurrencyid",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Currency",
        "Attribute": "transactioncurrencyid"
    },
    {
        "Name": "typecode",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Campaign Type",
        "Attribute": "typecode"
    },
    {
        "Name": "usb_cateringtwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Catering",
        "Attribute": "usb_cateringtwooptions"
    },
    {
        "Name": "usb_city",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "City",
        "Attribute": "usb_city"
    },
    {
        "Name": "usb_cocktailfunctiontwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Cocktail Function",
        "Attribute": "usb_cocktailfunctiontwooptions"
    },
    {
        "Name": "usb_country",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Country/Region",
        "Attribute": "usb_country"
    },
    {
        "Name": "usb_eventcategoryoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Event Category",
        "Attribute": "usb_eventcategoryoptionset"
    },
    {
        "Name": "usb_eventtypeoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Event Type",
        "Attribute": "usb_eventtypeoptionset"
    },
    {
        "Name": "usb_htmlcode",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "HTML Code",
        "Attribute": "usb_htmlcode"
    },
    {
        "Name": "usb_location",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Location",
        "Attribute": "usb_location"
    },
    {
        "Name": "usb_maximumeventcapacity",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Maximum Event Capacity",
        "Attribute": "usb_maximumeventcapacity"
    },
    {
        "Name": "usb_partnerfunctiontwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Partner Function",
        "Attribute": "usb_partnerfunctiontwooptions"
    },
    {
        "Name": "usb_postalcode",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Postal Code",
        "Attribute": "usb_postalcode"
    },
    {
        "Name": "usb_registrationcount",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Registration Count",
        "Attribute": "usb_registrationcount"
    },
    {
        "Name": "usb_stateorprovince",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "State/Province",
        "Attribute": "usb_stateorprovince"
    },
    {
        "Name": "usb_street1",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Street 1",
        "Attribute": "usb_street1"
    },
    {
        "Name": "usb_street2",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Street 2",
        "Attribute": "usb_street2"
    },
    {
        "Name": "usb_street3",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Street 3",
        "Attribute": "usb_street3"
    }],
    "ControlsLength": 36,
    "Navigation": [],
    "Tabs": [{
        "Label": "Summary",
        "Name": "SUMMARY",
        "DisplayState": "expanded",
        "Visible": true,
        "Sections": [{
            "Label": "CAMPAIGN",
            "Name": "CAMPAIGN",
            "Visible": true,
            "Controls": [{
                "Name": "typecode"
            },
            {
                "Name": "name"
            }]
        },
        {
            "Label": "EVENT DETAILS",
            "Name": "EventDetails",
            "Visible": true,
            "Controls": [{
                "Name": "usb_eventtypeoptionset"
            },
            {
                "Name": "usb_eventcategoryoptionset"
            },
            {
                "Name": "usb_maximumeventcapacity"
            },
            {
                "Name": "usb_registrationcount"
            },
            {
                "Name": "proposedstart"
            },
            {
                "Name": "proposedend"
            },
            {
                "Name": "budgetedcost"
            },
            {
                "Name": "usb_cateringtwooptions"
            },
            {
                "Name": "usb_cocktailfunctiontwooptions"
            },
            {
                "Name": "usb_partnerfunctiontwooptions"
            }]
        },
        {
            "Label": "OFFER",
            "Name": "OFFER",
            "Visible": true,
            "Controls": [{
                "Name": "objective"
            }]
        },
        {
            "Label": "RELATED PANE",
            "Name": "RELATED PANE",
            "Visible": true,
            "Controls": [{
                "Name": "notescontrol"
            }]
        },
        {
            "Label": "LISTS",
            "Name": "LISTS",
            "Visible": true,
            "Controls": [{
                "Name": "Lists"
            }]
        },
        {
            "Label": "LEADS",
            "Name": "LEADS",
            "Visible": true,
            "Controls": [{
                "Name": "Leads"
            }]
        },
        {
            "Label": "CAMPAIGN ACTIVITIES",
            "Name": "CAMPAIGN ACTIVITIES",
            "Visible": true,
            "Controls": [{
                "Name": "Activities"
            }]
        }]
    },
    {
        "Label": "Details",
        "Name": "DETAILS",
        "DisplayState": "expanded",
        "Visible": true,
        "Sections": [{
            "Label": "ADDRESS DETAILS",
            "Name": "AddressDetails",
            "Visible": true,
            "Controls": [{
                "Name": "usb_location"
            },
            {
                "Name": "usb_stateorprovince"
            },
            {
                "Name": "usb_street1"
            },
            {
                "Name": "usb_postalcode"
            },
            {
                "Name": "usb_street2"
            },
            {
                "Name": "usb_city"
            },
            {
                "Name": "usb_street3"
            },
            {
                "Name": "usb_country"
            }]
        },
        {
            "Label": "RESPONSES",
            "Name": "RESPONSES",
            "Visible": true,
            "Controls": [{
                "Name": "Responses"
            }]
        }]
    },
    {
        "Label": "HTML",
        "Name": "HTML",
        "DisplayState": "expanded",
        "Visible": true,
        "Sections": [{
            "Label": "HTML CODE",
            "Name": "HTMLCode",
            "Visible": true,
            "Controls": [{
                "Name": "usb_previewhtmltwooptions"
            },
            {
                "Name": "usb_htmlcode"
            }]
        },
        {
            "Label": "HTML PREVIEW",
            "Name": "HTMLPreview",
            "Visible": true,
            "Controls": [{
                "Name": "IFRAME_HTMLPreview"
            }]
        }]
    },
    {
        "Label": "ADMINISTRATION",
        "Name": "Administration",
        "DisplayState": "expanded",
        "Visible": true,
        "Sections": [{
            "Label": "ADMINISTRATION",
            "Name": "ADMINISTRATION",
            "Visible": true,
            "Controls": [{
                "Name": "ownerid"
            },
            {
                "Name": "createdon"
            },
            {
                "Name": "modifiedby"
            },
            {
                "Name": "modifiedon"
            },
            {
                "Name": "transactioncurrencyid"
            },
            {
                "Name": "actualstart"
            },
            {
                "Name": "actualend"
            }]
        },
        {
            "Label": "FINANCIALS",
            "Name": "FINANCIALS",
            "Visible": true,
            "Controls": [{
                "Name": "totalcampaignactivityactualcost"
            },
            {
                "Name": "totalactualcost"
            },
            {
                "Name": "othercost"
            }]
        }]
    }]
};