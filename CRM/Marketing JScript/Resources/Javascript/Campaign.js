﻿/// <reference path="Intellisense Files/Campaign Form - Campaign.js" />

function OnLoad() {
    Xrm.Page.getAttribute("usb_previewhtmltwooptions").setValue(false);
}

function ChangeHtml() {
    Xrm.Page.getAttribute("usb_previewhtmltwooptions").setValue(false);
    PreviewHtml();
}

function PreviewHtml() {
    debugger;
    if (Xrm.Page.getAttribute("usb_previewhtmltwooptions").getValue() == true) {
        var htmlSource = Xrm.Page.getAttribute("usb_htmlcode").getValue();
        if (htmlSource != null) {
            
            var frame = parent.document.getElementById('IFRAME_HTMLPreview');
            var doc = frame.contentDocument;

            doc.open();
            doc.write(htmlSource);
            doc.close();
        }
    }
    else {
        debugger;
        var frame = parent.document.getElementById('IFRAME_HTMLPreview');
        var doc = frame.contentDocument;
        doc.open();
        doc.write("");
        doc.close();
    }
}