//--------------------------------------------------------Global Variables---------------------------------------------------------
var serverUrl = "http://" + window.location.host + "/" + Xrm.Page.context.getOrgUniqueName();
var oDataPath = serverUrl + "/XRMServices/2011/OrganizationData.svc";

//----------------------------Function to post Admitted Status of an enrollment-----------------------------------------------------
function Admitted() {
    //Set Status Reason to Request: Admitted
    var intStatusValue = 864480012;

    Xrm.Page.data.entity.save();
    Xrm.Page.getAttribute('statuscode').setSubmitMode("always");
    Xrm.Page.getAttribute('statuscode').setValue(intStatusValue);
    Xrm.Page.data.entity.save("save");
}

//----------------------------Function to control Display of Admitted button-----------------------------------------------------
function DisplayAdmitted() {
    //debugger;
    //Get Status Reason of Record
    var intStatusReason = Xrm.Page.getAttribute('statuscode').getValue();

    //Get Guid of current user
    // var guidCurrentUser = Xrm.Page.context.getUserId();


    //Get Approver Role Guid
    //  var RoleName = "Approver"
    //  var ODataQuery = oDataPath + "/RoleSet?$select=RoleId&$filter=Name eq '" + RoleName + "'";
    //var QueryData = GetData(ODataQuery);
    //  var guidRole = QueryData.d.results[0].RoleId;


    //Check if User exist in the Role
    // var ODataQuery = oDataPath + "/SystemUserRolesSet?$select=SystemUserId&$filter=SystemUserId eq guid'" + guidCurrentUser + "' and RoleId eq guid'" + guidRole + "'";
    //var QueryData = GetData(ODataQuery);
    //  if (QueryData.d.results.length > 0) {
    //Check if Buttons can be Enabled based on Status
    if (intStatusReason == 864480010) {
        return true;
    }
}
//}

//----------------------------Function to post Not-Admitted Status of an enrollment-----------------------------------------------------
function NotAdmitted() {
    //Set Status Reason to Not Admitted
    var intStatusValue = 864480002;

    Xrm.Page.data.entity.save();
    Xrm.Page.getAttribute('statuscode').setSubmitMode("always");
    Xrm.Page.getAttribute('statuscode').setValue(intStatusValue);
    Xrm.Page.data.entity.save("save");
}

//----------------------------Function to control Display of Not Admitted button-----------------------------------------------------
function DisplayNotAdmitted() {
    //debugger;
    //Get Status Reason of Record
    var intStatusReason = Xrm.Page.getAttribute('statuscode').getValue();

    //Get Guid of current user
    // var guidCurrentUser = Xrm.Page.context.getUserId();


    //Get Approver Role Guid
    //  var RoleName = "Approver"
    // var ODataQuery = oDataPath + "/RoleSet?$select=RoleId&$filter=Name eq '" + RoleName + "'";
    //var QueryData = GetData(ODataQuery);
    //  var guidRole = QueryData.d.results[0].RoleId;


    //Check if User exist in the Role
    //  var ODataQuery = oDataPath + "/SystemUserRolesSet?$select=SystemUserId&$filter=SystemUserId eq guid'" + guidCurrentUser + "' and RoleId eq guid'" + guidRole + "'";
    //var QueryData = GetData(ODataQuery);
    //  if (QueryData.d.results.length > 0) {
    //Check if Buttons can be Enabled based on Status
    if (intStatusReason == 864480010) {
        return true;
    }
}
//}

function Accepted() {
    //debugger;
    //set Status Reason to Request:Accepted
    var intStatusValue = 864480017;
    Xrm.Page.data.entity.save();
    Xrm.Page.getAttribute('statuscode').setSubmitMode("always");
    Xrm.Page.getAttribute('statuscode').setValue(intStatusValue);
    Xrm.Page.data.entity.save("save");
}

function DisplayAccepted() {
    //Enable Accepted if status reason = Admitted
    var intStatusReason = Xrm.Page.getAttribute('statuscode').getValue();
    if (intStatusReason == 864480001) {
        return true;
    }
}

function NotAccepted() {
      //debugger;
     //set Status Reason = Request:Not Accepted
     var intStatusValue = 864480020;
     Xrm.Page.data.entity.save();
     Xrm.Page.getAttribute('statuscode').setSubmitMode("always");
     Xrm.Page.getAttribute('statuscode').setValue(intStatusValue);
     Xrm.Page.data.entity.save("save");
}

function DisplayNotAccepted() {
     //Enable Not Accepted if status Reason = Admitted
     var intStatusReason = Xrm.Page.getAttribute('statuscode').getValue();
     if (intStatusReason == 864480001) {
          return true;
     }
}

//----------------------------Function to post Registered Status of an enrollment-----------------------------------------------------
/*
function Registered() {
    //Set Status Reason to Request: Registered
    var intStatusValue = 864480006;

    //Check if Short Course Offering has started

    //Get Date
    var now = new Date();
    var dateCurrentDate = now.getFullYear() + "-" + strpad00(now.getMonth() + 1) + "-" + strpad00(now.getDate() - 1) + "T22:00:00Z";

    //Get Short Course Offering id
    var lookup = Xrm.Page.getAttribute("usb_programmeofferinglookup").getValue();
    if (lookup != null && lookup[0] != null) {
        var guidProgrammeOffering = lookup[0].id;
    }
    else {
        alert("Please select a valid Programme Offering for the record");
        return;
    }

    +    //Query date on associated Short Course Offering Record
    //var ODataQuery = oDataPath + "/usb_programmeofferingSet?$select=usb_StartDate&$filter=usb_programmeofferingId eq guid'" + guidProgrammeOffering + "' and usb_StartDate le datetime'" + dateCurrentDate + "'";
    //alert(ODataQuery);

    //Check for valid record return	

        jQuery.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            url: ODataQuery,
            async: false,
            beforeSend: function (XMLHttpRequest) {
                //Specifying this header ensures that the results will be returned as JSON.
                XMLHttpRequest.setRequestHeader("Accept", "application/json");
            },
            success: function (data, textStatus, XmlHttpRequest) {
                if (data && data.d != null) {
                    if (data.d.results.length > 0) {
                        //alert("Date OK");
                        Xrm.Page.data.entity.save();
                        Xrm.Page.getAttribute('statuscode').setSubmitMode("always");
                        Xrm.Page.getAttribute('statuscode').setValue(intStatusValue);
                        Xrm.Page.data.entity.save("saveandclose")
                    }
                    else {
                        alert("Programme Offering has not started yet");
                    }
                }
            },
            error: function (XmlHttpRequest, textStatus, errorThrown) {
                //alert(textStatus);
                //alert(errorThrown);
                alert("An Error has occured during retrieval of Data");
            }
        });
}
*/
//----------------------------Function to post Passed Status of an enrollment-----------------------------------------------------

function Passed() {
    //Set Status Reason to Passed
    var intStatusValue = 864480007;

    Xrm.Page.data.entity.save();
    Xrm.Page.getAttribute('statuscode').setSubmitMode("always");
    Xrm.Page.getAttribute('statuscode').setValue(intStatusValue);
    Xrm.Page.data.entity.save("save");
}

//----------------------------Function to post Failed Status of an enrollment-----------------------------------------------------
function Failed() {
    //Set Status Reason to Failed
    var intStatusValue = 864480008;

    Xrm.Page.data.entity.save();
    Xrm.Page.getAttribute('statuscode').setSubmitMode("always");
    Xrm.Page.getAttribute('statuscode').setValue(intStatusValue);
    Xrm.Page.data.entity.save("save");
}

//----------------------------Function to post Cancelled Status of an enrollment-----------------------------------------------------
function Cancelled() {
    //Set Status Reason to Cancelled
    var intStatusValue = 864480009;

    Xrm.Page.data.entity.save();
    Xrm.Page.getAttribute('statuscode').setSubmitMode("always");
    Xrm.Page.getAttribute('statuscode').setValue(intStatusValue);
    Xrm.Page.data.entity.save("save");
}


/*
//-------------------------------------Function to control Inside CRM form token functionality---------------------------------------
function CreateToken() {
    var userGuid = Xrm.Page.context.getUserId();
    var guidShortCourseOffering = Xrm.Page.data.entity.getId();
    var nameShortCourseOffering = Xrm.Page.getAttribute("stb_offeringnameenglish").getValue();
    var entityShortCourseOffering = Xrm.Page.data.entity.getEntityName();

     alert(guidShortCourseOffering);
    alert(nameShortCourseOffering);

    //Post Update of allow surveys field on contacts
    var WebToken = new Object();

    // Set Lookup field for Short Course Offering (must exists in the system)
    var lookupShortCourseOffering = new Object();
    lookupShortCourseOffering.ContactId = guidShortCourseOffering;
    lookupShortCourseOffering.FullName = nameShortCourseOffering;

    if (lookupShortCourseOffering != null) {
        WebToken.stb_ShortCourseOfferingLookup = {
            Id: lookupShortCourseOffering.ContactId,
            LogicalName: entityShortCourseOffering,
            Name: lookupShortCourseOffering.FullName
        };
    }

    CreateRecord(WebToken, "stb_webtokenSet");
}

//--------------------------------------------------------General Functions----------------------------------------------------------

//Pad values as 00
function strpad00(s) {
    s = s + '';
    if (s.length === 1) s = '0' + s;
    return s;
}

//Post Data through Odata Method
function CreateRecord(entityObject, odataSetName) {
    var jsonEntity = window.JSON.stringify(entityObject);
    //Synchronous AJAX function to Update a CRM record using OData
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        data: jsonEntity,
        async: false,
        url: oDataPath + "/" + odataSetName,
        beforeSend: function (XMLHttpRequest) {
            //Specifying this header ensures that the results will be returned as JSON.
            XMLHttpRequest.setRequestHeader("Accept", "application/json");
            //Specify the HTTP method MERGE to update just the changes you are submitting.
            //XMLHttpRequest.setRequestHeader("X-HTTP-Method", "MERGE");
        },
        success: function (data, textStatus, XmlHttpRequest) {
            alert("Web Token created successfully");
        },
        error: function (XmlHttpRequest, textStatus, errorThrown) {
            if (XmlHttpRequest && XmlHttpRequest.responseText) {
                alert("Error while updating " + odataSetName + " ; Error – " + XmlHttpRequest.responseText);
            }
        }
    });
}
*/