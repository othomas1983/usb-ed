﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;
using System.ServiceModel;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using HubService;

namespace StellenboschUniversity.UsbEd.Integration.CrmCustomization
{
    public class CreateOrUpdateAccpacDebtor : CodeActivity
    {
        private EndpointAddress serviceAddress = new EndpointAddress("http://localhost:8080/HubService/Debtors");
        
        protected override void Execute(CodeActivityContext executionContext)
        {
            IWorkflowContext context = executionContext.GetExtension<IWorkflowContext>();
            Guid debtorGuid = context.PrimaryEntityId;

            if (debtorGuid != Guid.Empty)
            {
                DebtorServiceClient debtorServiceClient = new DebtorServiceClient(new BasicHttpBinding(), serviceAddress);

                debtorServiceClient.CreateOrUpdateDebtorOnAccpac(debtorGuid);
            }
        }
    }
}
