﻿#region Using Statements
using System;
using System.ServiceModel;
#endregion

namespace Belpark.Service.Contracts
{
    [ServiceContract(Namespace = "http://Belpark.Service")]
    public interface IContactSync
    {
        [OperationContract]
        string UsbEdPostContactSync(Guid contactId);
        [OperationContract]
        string UsbEdPostEnrollmentStatus(Guid enrollmentId);
        [OperationContract]
        string UsbEdPostOfferingSync(Guid shortCourseOfferingId);
        //[OperationContract]
        //string BpcPostContactSync(Guid contactId);
        //[OperationContract]
        //string BpcPostSarStatus(Guid enrollmentId);
        //[OperationContract]
        //string BpcPostOfferingSync(Guid programmeOfferingId);
        [OperationContract]
        string UsbPostContactSync(Guid contactId);
        [OperationContract]
        string UsbPostSarStatus(Guid enrollmentId);
        [OperationContract]
        string UsbPostOfferingSync(Guid programmeOfferingId);
    }
}
