﻿using System;
using Belpark.CommonLibrary;
using Belpark.CRMLibrary;
using Belpark.LoggingLibrary;
using Belpark.MappingLibrary;
using Belpark.Service.Interaction;
using Microsoft.Xrm.Sdk;

namespace Belpark.Service.Implementation
{
    // Implement the service contract in the service class
    public class SynchronisationService : Belpark.Service.Contracts.IContactSync
    {
        #region Class Level Members
        private Entity marketingContactEntity = new Entity(Marketing_Entities.contact); //declare this here to allow global append
        private Entity marketingUsbEdOfferingEntity = new Entity(Marketing_Entities.usb_usbedoffering); //declare this here to allow global append
        private Entity marketingUsbOfferingEntity = new Entity(Marketing_Entities.usb_usboffering); //declare this here to allow global append
        private Entity marketingOfferingStatusEntity = new Entity(Marketing_Entities.usb_offeringstatus); //declare this here to allow global append

        private IOrganizationService usbedService;
        private IOrganizationService usbService;
        private IOrganizationService bpcService;
        private IOrganizationService marketingService;

        private string resultMessage = null;
        #endregion 

        public string UsbEdPostContactSync(Guid contactId)
        {
            /* Processing Logic:
             * Called on Contact Create or Update
             * Check if contact record exists in Marketing CRM using Contact.EmailAddress1
             * Contact: 
             *          Create if Does not Exist and Update if exists
             */
            try
            {
                //Connect to CRM Services
                usbedService = CRM_Service.GetCRMService("EncodedUsbEdCRMConnString");
                marketingService = CRM_Service.GetCRMService("EncodedMarketingCRMConnString");

                //Get UsbEd Contact Information
                Entity usbedContactEntity = CRM_UsbEd_GetData.RetrieveUsbEdContactEntity(usbedService, contactId);
                Logger.WriteLog(string.Format("usbed get contact info, contactid: {0}", contactId));

                //Get Marketing Contact Information
                Guid marketingContactId = CRM_Marketing_GetData.CheckMarketingContactExists(marketingService, usbedContactEntity, CRMOrgs.Usbed);
                Logger.WriteLog(string.Format("marketing get contact info, marketingcontactid: {0}",marketingContactId));

                //Create or Update Marketing Contact 
                marketingContactId = CRM_Marketing_SubmitData.SubmitMarketingContact(marketingService, marketingContactId, marketingContactEntity, usbedContactEntity, CRMOrgs.Usbed);
                Logger.WriteLog(string.Format("create or Update marketing Contact, marketingcontactid: {0}",marketingContactId));

                //return null message so that Plugin does not interperet as a failure
                return resultMessage;
            }
            catch (Exception ex)
            {
                resultMessage = ex.ToString();
                Logger.WriteLog(resultMessage);
                return resultMessage;
            }
        }

        public string UsbEdPostEnrollmentStatus(Guid enrollmentId)
        {
            /* Processing Logic:
             * Called on Enrollment Create and Update
             * Get Enrollment, Contact, Offering & Short Course record Details from UsbEd CRM
             * Check if contact record exists in Marketing CRM using Contact.EmailAddress1
             * Contact: 
             *          Create if Does not Exist and Update if exists
             *          if Status equals "Passed"
             *              Set Usbed Alumni flag
             *              if Short Course Name contains "DP"
             *                  Set Usb Alumni Flag
             * UsbEd Offering:
             *          Create if does not exist
             * Offering Status: 
             *          Create if does not Exist
             *          Associate Offering and Set Status
             *          Associate to Contact record
             */
            try
            {
                //Connect to CRM Services
                usbedService = CRM_Service.GetCRMService("EncodedUsbEdCRMConnString");
                marketingService = CRM_Service.GetCRMService("EncodedMarketingCRMConnString");

                //Get UsbEd Enrollment Information
                Entity usbedEnrollmentEntity = CRM_UsbEd_GetData.RetrieveUsbEdEnrollmentEntity(usbedService, enrollmentId);
                Logger.WriteLog(string.Format("usbed get enrollment info, enrollmentid: {0}", enrollmentId));

                //Get UsbEd Contact Information
                Entity usbedContactEntity = CRM_UsbEd_GetData.RetrieveUsbEdContactEntity(usbedService, usbedEnrollmentEntity);
                Logger.WriteLog(string.Format("usbed get contact info, contactid: {0}", ((EntityReference)usbedEnrollmentEntity.Attributes[UsbEd_Entity_Enrollment.stb_studentcontact_contactlookup]).Id));
                
                //Get Marketing Contact Information
                Guid marketingContactId = CRM_Marketing_GetData.CheckMarketingContactExists(marketingService, usbedContactEntity, CRMOrgs.Usbed);
                Logger.WriteLog(string.Format("marketing get contact info, marketingcontactid: {0}", marketingContactId));

                //Get UsbEd Short Course Offering Information
                Guid shortCourseOfferingId = usbedEnrollmentEntity.GetAttributeValue<EntityReference>(UsbEd_Entity_Enrollment.stb_shortcourseofferinglookup).Id;
                Entity usbedShortCourseOfferingEntity = CRM_UsbEd_GetData.RetrieveUsbEdShortCourseOfferingEntity(usbedService, shortCourseOfferingId);
                Logger.WriteLog(string.Format("usbed get offering info, offeringid: {0}", usbedEnrollmentEntity.GetAttributeValue<EntityReference>(UsbEd_Entity_Enrollment.stb_shortcourseofferinglookup).Id));

                //Add Alumni Details if applicable
                marketingContactEntity = CRM_Marketing_PrepareData.PrepareMarketingContactEntity_UsbEdAlumni(usbedEnrollmentEntity, marketingContactEntity, usbedShortCourseOfferingEntity);
                Logger.WriteLog("Add Alumni Data");

                //Create or Update Marketing Contact 
                marketingContactId = CRM_Marketing_SubmitData.SubmitMarketingContact(marketingService, marketingContactId, marketingContactEntity, usbedContactEntity, CRMOrgs.Usbed);
                Logger.WriteLog(string.Format("create or Update marketing Contact, marketingcontactid: {0}", marketingContactId));

                //Get Marketing UsbEd Offering Information
                Guid usbedShortCourseOfferingId = usbedEnrollmentEntity.GetAttributeValue<EntityReference>(UsbEd_Entity_Enrollment.stb_shortcourseofferinglookup).Id;
                Guid marketingUsbEdOfferingId = CRM_Marketing_GetData.CheckMarketingUsbEdOfferingExists(marketingService, usbedShortCourseOfferingId);
                Logger.WriteLog(string.Format("marketing get usbedoffering info, marketingUsbEdOfferingId: {0}", marketingUsbEdOfferingId));

                //Create or Update Marketing UsbEb Offering
                marketingUsbEdOfferingId = CRM_Marketing_SubmitData.SubmitMarketingUsbEdOffering(marketingService, marketingUsbEdOfferingId
                    , marketingUsbEdOfferingEntity, usbedShortCourseOfferingEntity);
                Logger.WriteLog(string.Format("create or Update marketing usbedoffering, marketingUsbEdOfferingId: {0}", marketingUsbEdOfferingId));
                
                //Get Marketing Offering Status Information
                Guid marketingOfferingStatusId = CRM_Marketing_GetData.CheckMarketingOfferingStatusExists(marketingService, marketingContactId, marketingUsbEdOfferingId);
                Logger.WriteLog(string.Format("marketing get offeringstatus info, marketingOfferingStatusId: {0}", marketingOfferingStatusId));

                //Add Status Reason Details
                marketingOfferingStatusEntity = CRM_Marketing_PrepareData.PrepareMarketingOfferingStatusEntity_UsbEdStatusReason(marketingOfferingStatusEntity, usbedEnrollmentEntity);
                Logger.WriteLog("Add Status Reason Data");

                //Create or Update Offering Status
                marketingOfferingStatusId = CRM_Marketing_SubmitData.SubmitMarketingOfferingStatus(marketingService, marketingOfferingStatusId, marketingUsbEdOfferingId, marketingContactId
                    , marketingOfferingStatusEntity, CRMOrgs.Usbed);
                Logger.WriteLog(string.Format("create or Update marketing offeringstatus, marketingOfferingStatusId: {0}", marketingOfferingStatusId));

                //return null message so that Plugin does not interperet as a failure
                return resultMessage;
            }
            catch (Exception ex)
            {
                resultMessage = ex.ToString();
                Logger.WriteLog(resultMessage);
                return resultMessage;
            }
        }

        public string UsbEdPostOfferingSync(Guid shortCourseOfferingId)
        {
            try
            {
                //Connect to CRM Services
                usbedService = CRM_Service.GetCRMService("EncodedUsbEdCRMConnString");
                marketingService = CRM_Service.GetCRMService("EncodedMarketingCRMConnString");

                //Get UsbEd Offering Information
                Entity usbedShortCourseOfferingEntity = CRM_UsbEd_GetData.RetrieveUsbEdShortCourseOfferingEntity(usbedService, shortCourseOfferingId);
                Logger.WriteLog(string.Format("usbed get offering info, OfferingId: {0}", shortCourseOfferingId));

                //Get Marketing Offering Information
                Guid marketingUsbEdOfferingId = CRM_Marketing_GetData.CheckMarketingUsbEdOfferingExists(marketingService, shortCourseOfferingId);
                Logger.WriteLog(string.Format("marketing get UsbEd offering info, OfferingId: {0}", shortCourseOfferingId));

                //Create or Update Marketing Offering 
                marketingUsbEdOfferingId = CRM_Marketing_SubmitData.SubmitMarketingUsbEdOffering(marketingService, marketingUsbEdOfferingId, marketingUsbEdOfferingEntity, usbedShortCourseOfferingEntity);
                Logger.WriteLog(string.Format("create or Update marketing UsbEd Offering, marketingUsbEdOfferingId: {0}", marketingUsbEdOfferingId));

                //return null message so that Plugin does not interperet as a failure
                return resultMessage;
            }
            catch (Exception ex)
            {
                resultMessage = ex.ToString();
                Logger.WriteLog(resultMessage);
                return resultMessage;
            }
        }

        //public string BpcPostContactSync(Guid contactId)
        //{
        //    /* Processing Logic:
        //     * Called on Contact Create or Update
        //     * Check if contact record exists in Marketing CRM using Contact.EmailAddress1
        //     * Contact: 
        //     *          Create if Does not Exist and Update if exists
        //     */
        //    try
        //    {
        //        //Connect to CRM Services
        //        bpcService = CRM_Service.GetCRMService("EncodedBpcCRMConnString");
        //        marketingService = CRM_Service.GetCRMService("EncodedMarketingCRMConnString");

        //        //Get Bpc Contact Information
        //        Entity bpcContactEntity = CRM_Bpc_GetData.RetrieveBpcContactEntity(bpcService, contactId);
        //        Logger.WriteLog(string.Format("bpc get contact info, contactid: {0}", contactId));

        //        //Get Marketing Contact Information
        //        Guid marketingContactId = CRM_Marketing_GetData.CheckMarketingContactExists(marketingService, bpcContactEntity, CRMOrgs.Bpc);
        //        Logger.WriteLog(string.Format("marketing get contact info, marketingcontactid: {0}", marketingContactId));

        //        //Create or Update Marketing Contact 
        //        marketingContactId = CRM_Marketing_SubmitData.SubmitMarketingContact(marketingService, marketingContactId, marketingContactEntity, bpcContactEntity, CRMOrgs.Bpc);
        //        Logger.WriteLog(string.Format("create or Update marketing Contact, marketingcontactid: {0}", marketingContactId));

        //        //return null message so that Plugin does not interperet as a failure
        //        return resultMessage;
        //    }
        //    catch (Exception ex)
        //    {
        //        resultMessage = ex.ToString();
        //        Logger.WriteLog(resultMessage);
        //        return resultMessage;
        //    }
        //}

        //public string BpcPostSarStatus(Guid studentAcademicRecordId)
        //{
        //    /* Processing Logic:
        //     * Called on SAR Create and Update
        //     * Get SAR, Contact, Offering & Programme record Details from Bpc CRM
        //     * Check if contact record exists in Marketing CRM using Contact.EmailAddress1
        //     * Contact: 
        //     *          Create if Does not Exist and Update if exists
        //     *          if Status equals "Completed Successfully" or Completion Dates Contain Data
        //     *              Set Usb Alumni flag
        //     * Usb Offering:
        //     *          Create if does not exist
        //     * Offering Status: 
        //     *          Create if does not Exist
        //     *          Associate Offering and Set Status
        //     *          Associate to Contact record
        //     */
        //    try
        //    {
        //        //Connect to CRM Services
        //        bpcService = CRM_Service.GetCRMService("EncodedBpcCRMConnString");
        //        marketingService = CRM_Service.GetCRMService("EncodedMarketingCRMConnString");

        //        //Get Bpc Enrollment Information
        //        Entity bpcSarEntity = CRM_Bpc_GetData.RetrieveBpcSarEntity(bpcService, studentAcademicRecordId);
        //        Logger.WriteLog(string.Format("bpc get enrollment info, studentAcademicRecordid: {0}", studentAcademicRecordId));

        //        //Get Bpc Contact Information
        //        Entity bpcContactEntity = CRM_Bpc_GetData.RetrieveBpcContactEntity(bpcService, bpcSarEntity);
        //        Logger.WriteLog(string.Format("bpc get contact info, contactid: {0}"
        //            , ((EntityReference)bpcSarEntity.Attributes[Bpc_Entity_SAR.lt_studentid]).Id));

        //        //Get Marketing Contact Information
        //        Guid marketingContactId = CRM_Marketing_GetData.CheckMarketingContactExists(marketingService, bpcContactEntity, CRMOrgs.Bpc);
        //        Logger.WriteLog(string.Format("marketing get contact info, marketingcontactid: {0}", marketingContactId));

        //        //Get bpc Programme Offering Information
        //        Guid programmeOfferingId = bpcSarEntity.GetAttributeValue<EntityReference>(Bpc_Entity_SAR.lt_academicprogrammeofferingid).Id;
        //        Entity bpcProgrammeOfferingEntity = CRM_Bpc_GetData.RetrieveBpcProgrammeOfferingEntity(bpcService, programmeOfferingId);
        //        Logger.WriteLog(string.Format("bpc get offering info, offeringid: {0}"
        //            , bpcSarEntity.GetAttributeValue<EntityReference>(Bpc_Entity_SAR.lt_academicprogrammeofferingid).Id));

        //        //Add Alumni Details if applicable
        //        marketingContactEntity = CRM_Marketing_PrepareData.PrepareMarketingContactEntity_BpcAlumni(bpcSarEntity
        //            , marketingContactEntity);
        //        Logger.WriteLog("Add Alumni Data");

        //        //Create or Update Marketing Contact 
        //        marketingContactId = CRM_Marketing_SubmitData.SubmitMarketingContact(marketingService, marketingContactId, marketingContactEntity
        //            , bpcContactEntity, CRMOrgs.Bpc);
        //        Logger.WriteLog(string.Format("create or Update marketing Contact, marketingcontactid: {0}", marketingContactId));

        //        //Get Marketing Usb Offering Information (use Usb in anticipation of New Usb org)
        //        Guid marketingUsbOfferingId = CRM_Marketing_GetData.CheckMarketingUsbOfferingExists(marketingService, programmeOfferingId);
        //        Logger.WriteLog(string.Format("marketing get usboffering info, marketingUsbOfferingId: {0}", marketingUsbOfferingId));

        //        //Create or Update Marketing Usb Offering (use Usb in anticipation of New Usb org)
        //        marketingUsbOfferingId = CRM_Marketing_SubmitData.SubmitMarketingUsbOffering(marketingService, marketingUsbOfferingId
        //            , marketingUsbOfferingEntity, bpcProgrammeOfferingEntity, CRMOrgs.Bpc);
        //        Logger.WriteLog(string.Format("create or Update marketing usboffering, marketingUsbOfferingId: {0}", marketingUsbOfferingId));

        //        //Get Marketing Offering Status Information
        //        Guid marketingOfferingStatusId = CRM_Marketing_GetData.CheckMarketingOfferingStatusExists(marketingService, marketingContactId, marketingUsbOfferingId);
        //        Logger.WriteLog(string.Format("marketing get offeringstatus info, marketingOfferingStatusId: {0}", marketingOfferingStatusId));

        //        //Add Status Reason Details
        //        marketingOfferingStatusEntity = CRM_Marketing_PrepareData.PrepareMarketingOfferingStatusEntity_BpcStatusReasonAndGraduationDates(marketingOfferingStatusEntity, bpcSarEntity);
        //        Logger.WriteLog("Add Status Reason Data");

        //        //Create or Update Offering Status
        //        marketingOfferingStatusId = CRM_Marketing_SubmitData.SubmitMarketingOfferingStatus(marketingService, marketingOfferingStatusId, marketingUsbOfferingId, marketingContactId
        //            , marketingOfferingStatusEntity, CRMOrgs.Bpc);
        //        Logger.WriteLog(string.Format("create or Update marketing offeringstatus, marketingOfferingStatusId: {0}", marketingOfferingStatusId));

        //        //return null message so that Plugin does not interperet as a failure
        //        return resultMessage;
        //    }
        //    catch (Exception ex)
        //    {
        //        resultMessage = ex.ToString();
        //        Logger.WriteLog(resultMessage);
        //        return resultMessage;
        //    }
        //}

        //public string BpcPostOfferingSync(Guid programmeOfferingId)
        //{
        //    try
        //    {
        //        //Connect to CRM Services
        //        bpcService = CRM_Service.GetCRMService("EncodedBpcCRMConnString");
        //        marketingService = CRM_Service.GetCRMService("EncodedMarketingCRMConnString");

        //        //Get BPC Offering Information
        //        Entity bpcProgrammeOfferingEntity = CRM_Bpc_GetData.RetrieveBpcProgrammeOfferingEntity(bpcService, programmeOfferingId);
        //        Logger.WriteLog(string.Format("bpc get offering info, OfferingId: {0}", programmeOfferingId));

        //        //Get Marketing Offering Information
        //        Guid marketingUsbOfferingId = CRM_Marketing_GetData.CheckMarketingUsbOfferingExists(marketingService, programmeOfferingId);
        //        Logger.WriteLog(string.Format("marketing get Usb offering info, OfferingId: {0}", programmeOfferingId));

        //        //Create or Update Marketing Offering 
        //        marketingUsbOfferingId = CRM_Marketing_SubmitData.SubmitMarketingUsbOffering(marketingService, marketingUsbOfferingId, marketingUsbOfferingEntity, bpcProgrammeOfferingEntity, CRMOrgs.Bpc);
        //        Logger.WriteLog(string.Format("create or Update marketing Usb Offering, marketingUsbOfferingId: {0}", marketingUsbOfferingId));

        //        //return null message so that Plugin does not interperet as a failure
        //        return resultMessage;
        //    }
        //    catch (Exception ex)
        //    {
        //        resultMessage = ex.ToString();
        //        Logger.WriteLog(resultMessage);
        //        return resultMessage;
        //    }
        //}

        public string UsbPostContactSync(Guid contactId)
        {
            /* Processing Logic:
             * Called on Contact Create or Update
             * Check if contact record exists in Marketing CRM using Contact.EmailAddress1
             * Contact: 
             *          Create if Does not Exist and Update if exists
             */
            try
            {
                //Connect to CRM Services
                usbService = CRM_Service.GetCRMService("EncodedUsbCRMConnString");
                marketingService = CRM_Service.GetCRMService("EncodedMarketingCRMConnString");

                //Get Usb Contact Information
                Entity usbContactEntity = CRM_Usb_GetData.RetrieveUsbContactEntity(usbService, contactId);
                Logger.WriteLog(string.Format("usb get contact info, contactid: {0}", contactId));

                //Assign Additional Attributes to Contact Entity
                //Get Usb Nationality Information
                Guid usbNationalityId = Common.FindEntityReference(usbContactEntity, Usb_Entity_Contact.usb_nationalitylookup).Id;
                Entity usbNationalityEntity = CRM_Usb_GetData.RetrieveUsbNationalityEntity(usbService, usbNationalityId);
                string nationalitySisCode = Common.FindString(usbNationalityEntity, Usb_Entity_Nationality.usb_siscode);
                Guid marketingNationalityId = CRM_Marketing_GetData.GetNationalityIdBySisCode(marketingService, nationalitySisCode);
                //Map Nationality to Contact Entity
                marketingContactEntity = Mapper.UsbContactNationality_MarketingContactNationality(marketingContactEntity, marketingNationalityId);
                
                //Get Marketing Contact Information
                Guid marketingContactId = CRM_Marketing_GetData.CheckMarketingContactExists(marketingService, usbContactEntity, CRMOrgs.Usb);
                Logger.WriteLog(string.Format("marketing get contact info, marketingcontactid: {0}", marketingContactId));

                //Create or Update Marketing Contact 
                marketingContactId = CRM_Marketing_SubmitData.SubmitMarketingContact(marketingService, marketingContactId, marketingContactEntity, usbContactEntity, CRMOrgs.Usb);
                Logger.WriteLog(string.Format("create or Update marketing Contact, marketingcontactid: {0}", marketingContactId));

                //return null message so that Plugin does not interperet as a failure
                return resultMessage;
            }
            catch (Exception ex)
            {
                resultMessage = ex.ToString();
                Logger.WriteLog(resultMessage);
                return resultMessage;
            }
        }

        public string UsbPostSarStatus(Guid studentAcademicRecordId)
        {
            /* Processing Logic:
             * Called on SAR Create and Update
             * Get SAR, Contact, Offering & Programme record Details from Usb CRM
             * Check if contact record exists in Marketing CRM using Contact.EmailAddress1
             * Contact: 
             *          Create if Does not Exist and Update if exists
             *          if Status equals "Completed Successfully" or Completion Dates Contain Data
             *              Set Usb Alumni flag
             * Usb Offering:
             *          Create if does not exist
             * Offering Status: 
             *          Create if does not Exist
             *          Associate Offering and Set Status
             *          Associate to Contact record
             */
            try
            {
                //Connect to CRM Services
                usbService = CRM_Service.GetCRMService("EncodedUsbCRMConnString");
                marketingService = CRM_Service.GetCRMService("EncodedMarketingCRMConnString");

                //Get Usb Enrollment Information
                Entity usbSarEntity = CRM_Usb_GetData.RetrieveUsbSarEntity(usbService, studentAcademicRecordId);
                Logger.WriteLog(string.Format("usb get enrollment info, studentAcademicRecordid: {0}", studentAcademicRecordId));

                //Get Usb Contact Information
                Entity usbContactEntity = CRM_Usb_GetData.RetrieveUsbContactEntity(usbService, usbSarEntity);
                Logger.WriteLog(string.Format("usb get contact info, contactid: {0}"
                    , ((EntityReference)usbSarEntity.Attributes[Usb_Entity_SAR.usb_contactlookup]).Id));

                //Assign Additional Attributes to Contact Entity
                //Get Usb Nationality Information
                Guid usbNationalityId = Common.FindEntityReference(usbContactEntity, Usb_Entity_Contact.usb_nationalitylookup).Id;
                Entity usbNationalityEntity = CRM_Usb_GetData.RetrieveUsbNationalityEntity(usbService, usbNationalityId);
                string nationalitySisCode = Common.FindString(usbNationalityEntity, Usb_Entity_Nationality.usb_siscode);
                Guid marketingNationalityId = CRM_Marketing_GetData.GetNationalityIdBySisCode(marketingService, nationalitySisCode);
                //Map Nationality to Contact Entity
                marketingContactEntity = Mapper.UsbContactNationality_MarketingContactNationality(marketingContactEntity, marketingNationalityId);

                //Get Marketing Contact Information
                Guid marketingContactId = CRM_Marketing_GetData.CheckMarketingContactExists(marketingService, usbContactEntity, CRMOrgs.Usb);
                Logger.WriteLog(string.Format("marketing get contact info, marketingcontactid: {0}", marketingContactId));

                //Get usb Programme Offering Information
                Guid programmeOfferingId = usbSarEntity.GetAttributeValue<EntityReference>(Usb_Entity_SAR.usb_programmeofferinglookup).Id;
                Entity usbProgrammeOfferingEntity = CRM_Usb_GetData.RetrieveUsbProgrammeOfferingEntity(usbService, programmeOfferingId);
                Logger.WriteLog(string.Format("usb get offering info, offeringid: {0}"
                    , usbSarEntity.GetAttributeValue<EntityReference>(Usb_Entity_SAR.usb_programmeofferinglookup).Id));

                //Add Alumni Details if applicable
                marketingContactEntity = CRM_Marketing_PrepareData.PrepareMarketingContactEntity_UsbAlumni(usbSarEntity, marketingContactEntity);
                Logger.WriteLog("Add Alumni Data");

                //Create or Update Marketing Contact 
                marketingContactId = CRM_Marketing_SubmitData.SubmitMarketingContact(marketingService, marketingContactId, marketingContactEntity
                    , usbContactEntity, CRMOrgs.Usb);
                Logger.WriteLog(string.Format("create or Update marketing Contact, marketingcontactid: {0}", marketingContactId));

                //Get Marketing Usb Offering Information (use Usb in anticipation of New Usb org)
                Guid marketingUsbOfferingId = CRM_Marketing_GetData.CheckMarketingUsbOfferingExists(marketingService, programmeOfferingId);
                Logger.WriteLog(string.Format("marketing get usboffering info, marketingUsbOfferingId: {0}", marketingUsbOfferingId));

                //Create or Update Marketing Usb Offering (use Usb in anticipation of New Usb org)
                marketingUsbOfferingId = CRM_Marketing_SubmitData.SubmitMarketingUsbOffering(marketingService, marketingUsbOfferingId
                    , marketingUsbOfferingEntity, usbProgrammeOfferingEntity, CRMOrgs.Usb);
                Logger.WriteLog(string.Format("create or Update marketing usboffering, marketingUsbOfferingId: {0}", marketingUsbOfferingId));

                //Get Marketing Offering Status Information
                Guid marketingOfferingStatusId = CRM_Marketing_GetData.CheckMarketingOfferingStatusExists(marketingService, marketingContactId, marketingUsbOfferingId);
                Logger.WriteLog(string.Format("marketing get offeringstatus info, marketingOfferingStatusId: {0}", marketingOfferingStatusId));

                //Add Status Reason Details
                marketingOfferingStatusEntity = CRM_Marketing_PrepareData.PrepareMarketingOfferingStatusEntity_UsbStatusReasonAndGraduationDates(marketingOfferingStatusEntity, usbSarEntity);
                Logger.WriteLog("Add Status Reason Data");

                //Create or Update Offering Status
                marketingOfferingStatusId = CRM_Marketing_SubmitData.SubmitMarketingOfferingStatus(marketingService, marketingOfferingStatusId, marketingUsbOfferingId, marketingContactId
                    , marketingOfferingStatusEntity, CRMOrgs.Usb);
                Logger.WriteLog(string.Format("create or Update marketing offeringstatus, marketingOfferingStatusId: {0}", marketingOfferingStatusId));

                //return null message so that Plugin does not interperet as a failure
                return resultMessage;
            }
            catch (Exception ex)
            {
                resultMessage = ex.ToString();
                Logger.WriteLog(resultMessage);
                return resultMessage;
            }
        }

        public string UsbPostOfferingSync(Guid programmeOfferingId)
        {
            try
            {
                //Connect to CRM Services
                usbService = CRM_Service.GetCRMService("EncodedUsbCRMConnString");
                marketingService = CRM_Service.GetCRMService("EncodedMarketingCRMConnString");

                //Get BPC Offering Information
                Entity usbProgrammeOfferingEntity = CRM_Usb_GetData.RetrieveUsbProgrammeOfferingEntity(usbService, programmeOfferingId);
                Logger.WriteLog(string.Format("usb get offering info, OfferingId: {0}", programmeOfferingId));

                //Get Marketing Offering Information
                Guid marketingUsbOfferingId = CRM_Marketing_GetData.CheckMarketingUsbOfferingExists(marketingService, programmeOfferingId);
                Logger.WriteLog(string.Format("marketing get Usb offering info, OfferingId: {0}", programmeOfferingId));

                //Create or Update Marketing Offering 
                marketingUsbOfferingId = CRM_Marketing_SubmitData.SubmitMarketingUsbOffering(marketingService, marketingUsbOfferingId, marketingUsbOfferingEntity, usbProgrammeOfferingEntity, CRMOrgs.Usb);
                Logger.WriteLog(string.Format("create or Update marketing Usb Offering, marketingUsbOfferingId: {0}", marketingUsbOfferingId));

                //return null message so that Plugin does not interperet as a failure
                return resultMessage;
            }
            catch (Exception ex)
            {
                resultMessage = ex.ToString();
                Logger.WriteLog(resultMessage);
                return resultMessage;
            }
        }
    }
}
