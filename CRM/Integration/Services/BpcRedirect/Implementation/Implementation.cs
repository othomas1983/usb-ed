﻿using System;
using Belpark.LoggingLibrary;
using BpcRedirect.IntegrationService;

namespace Belpark.Service.Implementation
{
    // Implement the service contract in the service class
    public class RedirectService : Belpark.Service.Contracts.IRedirect
    {
        #region Class Level Members
        private string resultMessage = null;
        #endregion

        public string BpcPostContactSync(Guid contactId)
        {
            try
            {

                Logger.WriteLog(string.Format("Call remote integration Service - Contact {0}", contactId));
                //Call Remote integration Service
                ContactSyncClient myclient = new ContactSyncClient();
                resultMessage = myclient.BpcPostContactSync(contactId);

                //return null message so that Plugin does not interperet as a failure
                return resultMessage;
            }
            catch (Exception ex)
            {
                resultMessage = ex.ToString();
                Logger.WriteLog(resultMessage);
                return resultMessage;
            }
        }

        public string BpcPostSarStatus(Guid studentAcademicRecordid)
        {
            try
            {
                Logger.WriteLog(string.Format("Call remote integration Service - SAR {0}", studentAcademicRecordid));
                //Call Remote integration Service
                ContactSyncClient myclient = new ContactSyncClient();
                resultMessage = myclient.BpcPostSarStatus(studentAcademicRecordid);
                
                //return null message so that Plugin does not interperet as a failure
                return resultMessage;
            }
            catch (Exception ex)
            {
                resultMessage = ex.ToString();
                Logger.WriteLog(resultMessage);
                return resultMessage;
            }
        }

        public string BpcPostOfferingSync(Guid programmeOfferingId)
        {
            try
            {

                Logger.WriteLog(string.Format("Call remote integration Service - Offering {0}", programmeOfferingId));
                //Call Remote integration Service
                ContactSyncClient myclient = new ContactSyncClient();
                resultMessage = myclient.BpcPostOfferingSync(programmeOfferingId);

                //return null message so that Plugin does not interperet as a failure
                return resultMessage;
            }
            catch (Exception ex)
            {
                resultMessage = ex.ToString();
                Logger.WriteLog(resultMessage);
                return resultMessage;
            }
        }
    }
}
