﻿#region Using Statements
using System;
using System.ComponentModel;
using System.ServiceModel;
using System.ServiceProcess;
using System.Configuration.Install;
using Belpark.LoggingLibrary;
using Belpark.Service.Implementation;
#endregion

namespace Belpark.Service
{
    public class RedirectWindowsService : ServiceBase
    {
        public ServiceHost serviceHost = null;
        public RedirectWindowsService()
        {
            ServiceName = "Belpark Redirect Services";
        }

        public static void Main()
        {
            ServiceBase.Run(new RedirectWindowsService());
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                if (serviceHost != null)
                {
                    serviceHost.Close();
                }
                serviceHost = new ServiceHost(typeof(RedirectService));
                serviceHost.Open();
                Logger.WriteLog("Service Started");
            }
            catch (Exception ex)
            {
                Logger.WriteLog(ex.ToString());
            }
        }

        protected override void OnStop()
        {
            if (serviceHost != null)
            {
                serviceHost.Close();
                serviceHost = null;
                Logger.WriteLog("Service Stopped");
            }
        }
    }

    [RunInstaller(true)]
    public class ProjectInstaller : Installer
    {
        private ServiceProcessInstaller process;
        private ServiceInstaller service;

        public ProjectInstaller()
        {
            process = new ServiceProcessInstaller();
            process.Account = ServiceAccount.LocalSystem;
            service = new ServiceInstaller();
            service.ServiceName = "Belpark Redirect Services";
            Installers.Add(process);
            Installers.Add(service);
        }

        protected override void OnCommitted(System.Collections.IDictionary savedState)
        {
            ServiceController sc = new ServiceController("Belpark Redirect Services");
            sc.Start();
        }
    }
}
