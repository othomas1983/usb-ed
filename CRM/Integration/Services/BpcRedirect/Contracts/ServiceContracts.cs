﻿#region Using Statements
using System;
using System.ServiceModel;
#endregion

namespace Belpark.Service.Contracts
{
    [ServiceContract(Namespace = "http://Belpark.Service")]
    public interface IRedirect
    {
        [OperationContract]
        string BpcPostContactSync(Guid contactid);
        [OperationContract]
        string BpcPostSarStatus(Guid enrollmentid);
        [OperationContract]
        string BpcPostOfferingSync(Guid programmeOfferingId);
    }
}