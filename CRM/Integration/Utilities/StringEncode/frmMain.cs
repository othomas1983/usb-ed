﻿using System;
using System.Windows.Forms;
using Belpark.CommonLibrary;

namespace Belpark.StringEncode
{
    public partial class FrmMain : Form
    {
        public FrmMain()
        {
            InitializeComponent();
        }

        private void btnEncode_Click(object sender, EventArgs e)
        {
            try
            {
                string encoded = Common.EncodeTo64(txtInput.Text);

                MessageBox.Show(this, encoded, @"Encode Result", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, @"Encode Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDecode_Click(object sender, EventArgs e)
        {
            try
            {
                string encoded = Common.DecodeFrom64(txtInput.Text);

                MessageBox.Show(this, encoded, @"Decode Result", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, @"Decode Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
