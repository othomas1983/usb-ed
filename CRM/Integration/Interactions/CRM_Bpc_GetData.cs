﻿using System;
using Belpark.CRMLibrary;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace Belpark.Service.Interaction
{
    public class CRM_Bpc_GetData
    {
        //public static Entity RetrieveBpcContactEntity(IOrganizationService bpcService, Entity bpcSarEntity)
        //{
        //    Guid bpcContactId = ((EntityReference)bpcSarEntity.Attributes[Bpc_Entity_SAR.lt_studentid]).Id;
        //    return RetrieveBpcContactEntity(bpcService, bpcContactId);
        //}

        //public static Entity RetrieveBpcContactEntity(IOrganizationService bpcService, Guid bpcContactId)
        //{
        //    ColumnSet bpcContactColumns = new ColumnSet
        //        (Bpc_Entity_Contact.emailaddress2
        //        , Bpc_Entity_Contact.firstname
        //        , Bpc_Entity_Contact.lastname
        //        , Bpc_Entity_Contact.mobilephone
        //        , Bpc_Entity_Contact.new_studentnumber
        //        , Bpc_Entity_Contact.telephone1
        //        , Bpc_Entity_Contact.new_titlelookupid
        //        , Bpc_Entity_Contact.new_initials
        //        , Bpc_Entity_Contact.governmentid
        //        , Bpc_Entity_Contact.lt_correspondencelanguageid
        //        , Bpc_Entity_Contact.address1_line1
        //        , Bpc_Entity_Contact.address1_line2
        //        , Bpc_Entity_Contact.address1_postalcode
        //        , Bpc_Entity_Contact.address1_city
        //        , Bpc_Entity_Contact.address1_stateorprovince
        //        , Bpc_Entity_Contact.address1_country
        //        , Bpc_Entity_Contact.telephone2
        //        , Bpc_Entity_Contact.lt_nationalityid
        //        , Bpc_Entity_Contact.parentcustomerid
        //        , Bpc_Entity_Contact.jobtitle
        //        , Bpc_Entity_Contact.lt_industry_os
        //        , Bpc_Entity_Contact.lt_workarea_os
        //        );

        //    Entity bpcContactEntity = bpcService.Retrieve(Bpc_Entities.contact, bpcContactId, bpcContactColumns);

        //    return bpcContactEntity;
        //}

        //public static Entity RetrieveBpcSarEntity(IOrganizationService bpcService, Guid studentAcademicRecordid)
        //{
        //    ColumnSet bpcSarColumns = new ColumnSet
        //        (Bpc_Entity_SAR.statuscode
        //        , Bpc_Entity_SAR.lt_studentid
        //        , Bpc_Entity_SAR.lt_academicprogrammeofferingid
        //        , Bpc_Entity_SAR.lt_stud_mastersyear
        //        , Bpc_Entity_SAR.lt_stud_honoursyear
        //        , Bpc_Entity_SAR.lt_stud_phdyear
        //        , Bpc_Entity_SAR.lt_stud_postgraduateyear);

        //    Entity bpcSarEntity = bpcService.Retrieve(Bpc_Entities.lt_studentacademicrecord, studentAcademicRecordid, bpcSarColumns);

        //    return bpcSarEntity;
        //}

        //public static Entity RetrieveBpcProgrammeOfferingEntity(IOrganizationService bpcService, Guid programmeOfferingId)
        //{
        //    ColumnSet bpcProgrammeOfferingColumns = new ColumnSet
        //        (Bpc_Entity_ProgrammeOffering.lt_programmeid
        //        , Bpc_Entity_ProgrammeOffering.lt_name
        //        , Bpc_Entity_ProgrammeOffering.lt_offeringstartdate);

        //    Entity bpcProgrammeOfferingEntity = bpcService.Retrieve
        //        (Bpc_Entities.lt_programmeoffering, programmeOfferingId, bpcProgrammeOfferingColumns);

        //    return bpcProgrammeOfferingEntity;
        //}
    }
}
