﻿using System;
using Belpark.CRMLibrary;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace Belpark.Service.Interaction
{
    public class CRM_UsbEd_GetData
    {
        public static Entity RetrieveUsbEdEnrollmentEntity(IOrganizationService usbedService, Guid enrollmentid)
        {
            ColumnSet usbedEnrollmentColumns = new ColumnSet
                (UsbEd_Entity_Enrollment.statuscode
                , UsbEd_Entity_Enrollment.stb_studentcontact_contactlookup
                , UsbEd_Entity_Enrollment.stb_shortcourseofferinglookup);

            Entity usbedEnrollmentEntity = usbedService.Retrieve(UsbEd_Entities.stb_enrollment, enrollmentid, usbedEnrollmentColumns);

            return usbedEnrollmentEntity;
        }

        public static Entity RetrieveUsbEdContactEntity(IOrganizationService usbedService, Entity usbedEnrollmentEntity)
        {
            Guid usbedContactId = ((EntityReference)usbedEnrollmentEntity.Attributes[UsbEd_Entity_Enrollment.stb_studentcontact_contactlookup]).Id;
            return RetrieveUsbEdContactEntity(usbedService, usbedContactId);
        }

        public static Entity RetrieveUsbEdContactEntity(IOrganizationService usbedService, Guid usbedContactId)
        {
            ColumnSet usbedContactColumns = new ColumnSet
                (UsbEd_Entity_Contact.emailaddress1
                , UsbEd_Entity_Contact.firstname
                , UsbEd_Entity_Contact.lastname
                , UsbEd_Entity_Contact.mobilephone
                , UsbEd_Entity_Contact.stb_usnumber
                , UsbEd_Entity_Contact.telephone1);

            Entity usbedContactEntity = usbedService.Retrieve(UsbEd_Entities.contact, usbedContactId, usbedContactColumns);

            return usbedContactEntity;
        }

        public static Entity RetrieveUsbEdShortCourseOfferingEntity(IOrganizationService usbedService, Guid shortCourseOfferingId)
        {
            ColumnSet usbedShortCourseOfferingColumns = new ColumnSet
                (UsbEd_Entity_ShortCourseOffering.stb_shortcourselookup
                , UsbEd_Entity_ShortCourseOffering.stb_offeringnameenglish
                , UsbEd_Entity_ShortCourseOffering.stb_startdate);
            Entity usbedShortCourseOfferingEntity = usbedService.Retrieve
                (UsbEd_Entities.stb_shortcourseoffering, shortCourseOfferingId, usbedShortCourseOfferingColumns);

            return usbedShortCourseOfferingEntity;
        }
    }
}
