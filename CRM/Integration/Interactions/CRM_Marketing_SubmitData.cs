﻿using System;
using Belpark.CommonLibrary;
using Belpark.CRMLibrary;
using Belpark.MappingLibrary;
using Microsoft.Xrm.Sdk;


namespace Belpark.Service.Interaction
{
    public class CRM_Marketing_SubmitData
    {
        public static Guid SubmitMarketingContact(IOrganizationService marketingService, Guid marketingContactId, Entity marketingContactEntity, Entity orgContactEntity, String CRMOrg)
        {
            switch (CRMOrg)
            {
                case CRMOrgs.Usbed:
                    marketingContactEntity[Marketing_Entity_Contact.firstname] = Common.FindString(orgContactEntity, UsbEd_Entity_Contact.firstname);
                    marketingContactEntity[Marketing_Entity_Contact.lastname] = Common.FindString(orgContactEntity, UsbEd_Entity_Contact.lastname);
                    marketingContactEntity[Marketing_Entity_Contact.mobilephone] = Common.FindString(orgContactEntity, UsbEd_Entity_Contact.mobilephone);
                    marketingContactEntity[Marketing_Entity_Contact.telephone1] = Common.FindString(orgContactEntity, UsbEd_Entity_Contact.telephone1);
                    marketingContactEntity[Marketing_Entity_Contact.emailaddress1] = Common.FindString(orgContactEntity, UsbEd_Entity_Contact.emailaddress1);
                    marketingContactEntity[Marketing_Entity_Contact.usb_usnumber] = Common.FindString(orgContactEntity, UsbEd_Entity_Contact.stb_usnumber);
                    break;

                case CRMOrgs.Usb:
                    marketingContactEntity[Marketing_Entity_Contact.firstname] = Common.FindString(orgContactEntity, Usb_Entity_Contact.firstname);
                    marketingContactEntity[Marketing_Entity_Contact.lastname] = Common.FindString(orgContactEntity, Usb_Entity_Contact.lastname);
                    marketingContactEntity[Marketing_Entity_Contact.mobilephone] = Common.FindString(orgContactEntity, Usb_Entity_Contact.mobilephone);
                    marketingContactEntity[Marketing_Entity_Contact.telephone1] = Common.FindString(orgContactEntity, Usb_Entity_Contact.telephone1);
                    marketingContactEntity[Marketing_Entity_Contact.emailaddress1] = Common.FindString(orgContactEntity, Usb_Entity_Contact.emailaddress2);
                    marketingContactEntity[Marketing_Entity_Contact.usb_usnumber] = Common.FindString(orgContactEntity, Usb_Entity_Contact.usb_usnumber);

                    marketingContactEntity[Marketing_Entity_Contact.salutation] = Common.FindEntityReferenceName(orgContactEntity,Usb_Entity_Contact.usb_titlelookup);
                    marketingContactEntity[Marketing_Entity_Contact.usb_initials] = Common.FindString(orgContactEntity, Usb_Entity_Contact.usb_initials);
                    marketingContactEntity[Marketing_Entity_Contact.governmentid] = Common.FindString(orgContactEntity, Usb_Entity_Contact.governmentid);
                    marketingContactEntity[Marketing_Entity_Contact.address1_line1] = Common.FindString(orgContactEntity, Usb_Entity_Contact.address1_line1);
                    marketingContactEntity[Marketing_Entity_Contact.address1_line2] = Common.FindString(orgContactEntity, Usb_Entity_Contact.address1_line2);
                    marketingContactEntity[Marketing_Entity_Contact.address1_line3] = Common.FindString(orgContactEntity, Usb_Entity_Contact.address1_line3);
                    marketingContactEntity[Marketing_Entity_Contact.address1_postalcode] = Common.FindString(orgContactEntity, Usb_Entity_Contact.address1_postalcode);
                    marketingContactEntity[Marketing_Entity_Contact.address1_city] = Common.FindString(orgContactEntity, Usb_Entity_Contact.address1_city);
                    marketingContactEntity[Marketing_Entity_Contact.address1_stateorprovince] = Common.FindString(orgContactEntity, Usb_Entity_Contact.address1_stateorprovince);
                    marketingContactEntity[Marketing_Entity_Contact.address1_country] = Common.FindEntityReferenceName(orgContactEntity, Usb_Entity_Contact.usb_address1country_lookup);
                    marketingContactEntity[Marketing_Entity_Contact.telephone2] = Common.FindString(orgContactEntity, Usb_Entity_Contact.telephone2);
                    marketingContactEntity[Marketing_Entity_Contact.usb_languageoptionset] = Mapper.UsbContactLanguage_MarketingContactLanguage(orgContactEntity);
                    //Naitonality - Already included in the Marketing Contact object

                    break;

                default:
                    throw new InvalidPluginExecutionException("No Organisation Specified");
            }
            
            if (marketingContactId != Guid.Empty)
            {
                marketingContactEntity[Marketing_Entity_Contact.contactid] = marketingContactId;
                marketingService.Update(marketingContactEntity);
            }
            else
            {
                marketingContactId = marketingService.Create(marketingContactEntity);
            }

            return marketingContactId;
        }

        public static Guid SubmitMarketingUsbEdOffering(IOrganizationService marketingService, Guid marketingUsbEdOfferingId, Entity marketingUsbEdOfferingEntity
            , Entity usbedShortCourseOfferingEntity)
        {
            marketingUsbEdOfferingEntity[Marketing_Entity_UsbEdOffering.usb_offeringname] = usbedShortCourseOfferingEntity[UsbEd_Entity_ShortCourseOffering.stb_offeringnameenglish];
            marketingUsbEdOfferingEntity[Marketing_Entity_UsbEdOffering.usb_sourceguid] = usbedShortCourseOfferingEntity.Id.ToString();
            marketingUsbEdOfferingEntity[Marketing_Entity_UsbEdOffering.usb_startdate] = usbedShortCourseOfferingEntity[UsbEd_Entity_ShortCourseOffering.stb_startdate];

            if (marketingUsbEdOfferingId != Guid.Empty)
            {
                marketingUsbEdOfferingEntity[Marketing_Entity_UsbEdOffering.usb_usbedofferingid] = marketingUsbEdOfferingId;
                marketingService.Update(marketingUsbEdOfferingEntity);
            }
            else
            {
                marketingUsbEdOfferingId = marketingService.Create(marketingUsbEdOfferingEntity);
            }

            return marketingUsbEdOfferingId;
        }

        public static Guid SubmitMarketingUsbOffering(IOrganizationService marketingService, Guid marketingUsbOfferingId, Entity marketingUsbOfferingEntity
            , Entity orgProgrammeOfferingEntity, string CRMOrg)
        {
            switch (CRMOrg)
            {
                case CRMOrgs.Usb:
                    marketingUsbOfferingEntity[Marketing_Entity_UsbOffering.usb_offeringname] = Common.FindString(orgProgrammeOfferingEntity, Usb_Entity_ProgrammeOffering.usb_name);
                    marketingUsbOfferingEntity[Marketing_Entity_UsbOffering.usb_sourceguid] = orgProgrammeOfferingEntity.Id.ToString();
                    marketingUsbOfferingEntity[Marketing_Entity_UsbOffering.usb_startdate] = Common.FindDateTime(orgProgrammeOfferingEntity, Usb_Entity_ProgrammeOffering.usb_offeringstartdate);
                    break;
                default:
                    throw new InvalidPluginExecutionException("No Organisation Specified");
            }

            if (marketingUsbOfferingId != Guid.Empty)
            {
                marketingUsbOfferingEntity[Marketing_Entity_UsbOffering.usb_usbofferingid] = marketingUsbOfferingId;
                marketingService.Update(marketingUsbOfferingEntity);
            }
            else
            {
                marketingUsbOfferingId = marketingService.Create(marketingUsbOfferingEntity);
            }

            return marketingUsbOfferingId;
        }

        public static Guid SubmitMarketingOfferingStatus(IOrganizationService marketingService, Guid marketingOfferingStatusId, Guid marketingOrgOfferingId, Guid marketingContactId
            , Entity marketingOfferingStatusEntity, string CRMOrg)
        {
            switch (CRMOrg)
            {
                case CRMOrgs.Usbed:
                    marketingOfferingStatusEntity[Marketing_Entity_OfferingStatus.usb_usbedofferinglookup] = new EntityReference(Marketing_Entities.usb_usbedoffering, marketingOrgOfferingId);
                    break;
                case CRMOrgs.Usb:
                    marketingOfferingStatusEntity[Marketing_Entity_OfferingStatus.usb_usbofferinglookup] = new EntityReference(Marketing_Entities.usb_usboffering, marketingOrgOfferingId);
                    break;
                //case CRMOrgs.Bpc:
                //    marketingOfferingStatusEntity[Marketing_Entity_OfferingStatus.usb_usbofferinglookup] = new EntityReference(Marketing_Entities.usb_usboffering, marketingOrgOfferingId);
                //    break;
                default:
                    throw new InvalidPluginExecutionException("No Organisation Specified");
            }
            marketingOfferingStatusEntity[Marketing_Entity_OfferingStatus.usb_name] = string.Concat(marketingContactId.ToString(), "-", marketingOrgOfferingId.ToString());
            marketingOfferingStatusEntity[Marketing_Entity_OfferingStatus.usb_contactlookup] = new EntityReference(Marketing_Entities.contact, marketingContactId);

            if (marketingOfferingStatusId != Guid.Empty)
            {
                marketingOfferingStatusEntity[Marketing_Entity_OfferingStatus.usb_offeringstatusid] = marketingOfferingStatusId;
                marketingService.Update(marketingOfferingStatusEntity);
            }
            else
            {
                marketingOfferingStatusId = marketingService.Create(marketingOfferingStatusEntity);
            }

            return marketingOfferingStatusId;
        }
    }
}
