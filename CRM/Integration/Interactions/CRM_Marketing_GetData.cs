﻿using System;
using Belpark.CommonLibrary;
using Belpark.CRMLibrary;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace Belpark.Service.Interaction
{
    public class CRM_Marketing_GetData
    {
        public static Guid CheckMarketingContactExists(IOrganizationService marketingService, Entity orgContactEntity, String CRMOrg)
        {
            //First check for Us Number match
            //Second check for Email address match
            //Third check for ID No/Passport/foreign Id Match
            //if nothing found then return a Guid.Empty

            //Define the lookup field values per org
            Guid marketingContactId = Guid.Empty;
            string emailAddress;
            string studentNumber;

            switch (CRMOrg)
            {
                case CRMOrgs.Usbed:
                    studentNumber = Common.FindString(orgContactEntity, UsbEd_Entity_Contact.stb_usnumber);
                    emailAddress = Common.FindString(orgContactEntity, UsbEd_Entity_Contact.emailaddress1);
                    break;
                case CRMOrgs.Usb:
                    studentNumber = Common.FindString(orgContactEntity, Usb_Entity_Contact.usb_usnumber);
                    emailAddress = Common.FindString(orgContactEntity, Usb_Entity_Contact.emailaddress2);
                    break;

                default:
                    throw new InvalidPluginExecutionException("No Organisation Specified");
            }

            //Check for Student number match
            if (studentNumber != null || studentNumber != string.Empty)
            {
                QueryExpression query = new QueryExpression(Marketing_Entities.contact);
                query.ColumnSet.AddColumn(Marketing_Entity_Contact.usb_usnumber);
                query.Criteria.AddCondition(Marketing_Entity_Contact.usb_usnumber
                    , ConditionOperator.Equal, studentNumber);
                query.PageInfo.PageNumber = 1;
                query.PageInfo.Count = 1;
                query.PageInfo.PagingCookie = null;

                EntityCollection results = marketingService.RetrieveMultiple(query);
                foreach (Entity a in results.Entities)
                {
                    marketingContactId = (Guid)a.Attributes[Marketing_Entity_Contact.contactid];
                }
            }

            //Check for Email Address match if Student Number does not return a result
            if (emailAddress != null || emailAddress != string.Empty || marketingContactId == Guid.Empty)
            {
                QueryExpression query = new QueryExpression(Marketing_Entities.contact);
                query.ColumnSet.AddColumn(Marketing_Entity_Contact.emailaddress1);
                query.Criteria.AddCondition(Marketing_Entity_Contact.emailaddress1
                    , ConditionOperator.Equal, emailAddress);
                query.PageInfo.PageNumber = 1;
                query.PageInfo.Count = 1;
                query.PageInfo.PagingCookie = null;

                EntityCollection results = marketingService.RetrieveMultiple(query);
                foreach (Entity a in results.Entities)
                {
                    marketingContactId = (Guid)a.Attributes[Marketing_Entity_Contact.contactid];
                }
            }

            //Check for ID/Passport/ForeignId match if Email Address does not return a result

            /*Not yet implemented
            if (emailAddress != null || emailAddress != string.Empty || marketingContactId == Guid.Empty)
            {
                QueryExpression query = new QueryExpression(Marketing_Entities.contact);
                query.ColumnSet.AddColumn(Marketing_Entity_Contact.emailaddress1);
                query.Criteria.AddCondition(Marketing_Entity_Contact.emailaddress1
                    , ConditionOperator.Equal, emailAddress);
                query.PageInfo.PageNumber = 1;
                query.PageInfo.Count = 1;
                query.PageInfo.PagingCookie = null;

                EntityCollection results = marketingService.RetrieveMultiple(query);
                foreach (Entity a in results.Entities)
                {
                    marketingContactId = (Guid)a.Attributes[Marketing_Entity_Contact.contactid];
                }
            }
            */

            return marketingContactId;
        }

        public static Guid CheckMarketingUsbEdOfferingExists(IOrganizationService marketingService, Guid usbedShortCourseOfferingId)
        {
            Guid marketingUsbEdOfferingId = Guid.Empty;
            
            QueryExpression query = new QueryExpression(Marketing_Entities.usb_usbedoffering);
            query.ColumnSet.AddColumn(Marketing_Entity_UsbEdOffering.usb_sourceguid);
            query.Criteria.AddCondition(Marketing_Entity_UsbEdOffering.usb_sourceguid, ConditionOperator.Equal, usbedShortCourseOfferingId.ToString());
            query.PageInfo.PageNumber = 1;
            query.PageInfo.Count = 1;
            query.PageInfo.PagingCookie = null;

            EntityCollection results = marketingService.RetrieveMultiple(query);
            foreach (Entity a in results.Entities)
            {
                marketingUsbEdOfferingId = (Guid)a.Attributes[Marketing_Entity_UsbEdOffering.usb_usbedofferingid];
            }

            return marketingUsbEdOfferingId;
        }

        public static Guid CheckMarketingUsbOfferingExists(IOrganizationService marketingService, Guid offeringId)
        {
            Guid marketingUsbOfferingId = Guid.Empty;
            Guid orgProgrammeOfferingId = Guid.Empty;
            
            QueryExpression query = new QueryExpression(Marketing_Entities.usb_usboffering);
            query.ColumnSet.AddColumn(Marketing_Entity_UsbOffering.usb_sourceguid);
            query.Criteria.AddCondition(Marketing_Entity_UsbOffering.usb_sourceguid, ConditionOperator.Equal, offeringId.ToString());
            query.PageInfo.PageNumber = 1;
            query.PageInfo.Count = 1;
            query.PageInfo.PagingCookie = null;

            EntityCollection results = marketingService.RetrieveMultiple(query);
            foreach (Entity a in results.Entities)
            {
                marketingUsbOfferingId = (Guid)a.Attributes[Marketing_Entity_UsbOffering.usb_usbofferingid];
            }

            return marketingUsbOfferingId;
        }

        public static Guid CheckMarketingOfferingStatusExists(IOrganizationService marketingService, Guid marketingContactId, Guid marketingOrgOfferingId)
        {
            Guid marketingOfferingStatusId = Guid.Empty;
            string marketingOfferingStatusName = string.Concat(marketingContactId.ToString(), "-", marketingOrgOfferingId.ToString());

            QueryExpression query = new QueryExpression(Marketing_Entities.usb_offeringstatus);
            query.ColumnSet.AddColumn(Marketing_Entity_OfferingStatus.usb_name);
            query.Criteria.AddCondition(Marketing_Entity_OfferingStatus.usb_name, ConditionOperator.Equal, marketingOfferingStatusName);
            query.PageInfo.PageNumber = 1;
            query.PageInfo.Count = 1;
            query.PageInfo.PagingCookie = null;

            EntityCollection results = marketingService.RetrieveMultiple(query);
            foreach (Entity a in results.Entities)
            {
                marketingOfferingStatusId = (Guid)a.Attributes[Marketing_Entity_OfferingStatus.usb_offeringstatusid];
            }

            return marketingOfferingStatusId;
        }

        public static Guid GetNationalityIdBySisCode(IOrganizationService marketingService, string sisCode)
        {
            Guid marketingNationalityId = Guid.Empty;

            QueryExpression query = new QueryExpression(Marketing_Entities.usb_nationality);
            query.ColumnSet.AddColumn(Marketing_Entity_Nationality.usb_nameenglish);
            query.Criteria.AddCondition(Marketing_Entity_Nationality.usb_siscode, ConditionOperator.Equal, sisCode);
            query.PageInfo.PageNumber = 1;
            query.PageInfo.Count = 1;
            query.PageInfo.PagingCookie = null;

            EntityCollection results = marketingService.RetrieveMultiple(query);
            foreach (Entity a in results.Entities)
            {
                marketingNationalityId = a.Id;
            }

            return marketingNationalityId;
        }
    }
}
