﻿using System;
using Belpark.CRMLibrary;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace Belpark.Service.Interaction
{
    public class CRM_Usb_GetData
    {
        public static Entity RetrieveUsbContactEntity(IOrganizationService usbService, Entity usbSarEntity)
        {
            Guid usbContactId = ((EntityReference)usbSarEntity.Attributes[Usb_Entity_SAR.usb_contactlookup]).Id;
            return RetrieveUsbContactEntity(usbService, usbContactId);
        }

        public static Entity RetrieveUsbContactEntity(IOrganizationService usbService, Guid usbContactId)
        {
            ColumnSet usbContactColumns = new ColumnSet
                (Usb_Entity_Contact.emailaddress2
                , Usb_Entity_Contact.firstname
                , Usb_Entity_Contact.lastname
                , Usb_Entity_Contact.mobilephone
                , Usb_Entity_Contact.usb_usnumber
                , Usb_Entity_Contact.telephone1
                , Usb_Entity_Contact.usb_titlelookup //Title
                , Usb_Entity_Contact.usb_initials //Initials
                , Usb_Entity_Contact.governmentid //Id Number
                , Usb_Entity_Contact.usb_correspondencelanguagelookup //Language
                , Usb_Entity_Contact.address1_line1 //Street 1
                , Usb_Entity_Contact.address1_line2 //Street2
                , Usb_Entity_Contact.address1_line3 //Street3
                , Usb_Entity_Contact.address1_postalcode //Zip/Postal Code
                , Usb_Entity_Contact.address1_city //City
                , Usb_Entity_Contact.address1_stateorprovince //State/Province
                , Usb_Entity_Contact.usb_address1country_lookup //Country/Region
                , Usb_Entity_Contact.telephone2
                , Usb_Entity_Contact.usb_nationalitylookup //Nationality
                );

            Entity usbContactEntity = usbService.Retrieve(Usb_Entities.contact, usbContactId, usbContactColumns);

            return usbContactEntity;
        }

        public static Entity RetrieveUsbSarEntity(IOrganizationService usbService, Guid studentAcademicRecordid)
        {
            ColumnSet usbSarColumns = new ColumnSet
                (Usb_Entity_SAR.statuscode
                , Usb_Entity_SAR.usb_contactlookup
                , Usb_Entity_SAR.usb_programmeofferinglookup
                , Usb_Entity_SAR.usb_mastersyear
                , Usb_Entity_SAR.usb_honoursyear
                , Usb_Entity_SAR.usb_phdyear
                , Usb_Entity_SAR.usb_postgraduateyear);

            Entity usbSarEntity = usbService.Retrieve(Usb_Entities.usb_studentacademicrecord, studentAcademicRecordid, usbSarColumns);

            return usbSarEntity;
        }

        public static Entity RetrieveUsbProgrammeOfferingEntity(IOrganizationService usbService, Guid programmeOfferingId)
        {
            ColumnSet usbProgrammeOfferingColumns = new ColumnSet
                (Usb_Entity_ProgrammeOffering.usb_programmeofferingid
                , Usb_Entity_ProgrammeOffering.usb_name
                , Usb_Entity_ProgrammeOffering.usb_offeringstartdate);

            Entity usbProgrammeOfferingEntity = usbService.Retrieve
                (Usb_Entities.usb_programmeoffering, programmeOfferingId, usbProgrammeOfferingColumns);

            return usbProgrammeOfferingEntity;
        }

        public static Entity RetrieveUsbNationalityEntity(IOrganizationService usbService, Guid nationalityId)
        {
            ColumnSet usbNationalityColumns = new ColumnSet
                (Usb_Entity_Nationality.usb_name
                , Usb_Entity_Nationality.usb_siscode);

            Entity usbProgrammeOfferingEntity = usbService.Retrieve
                (Usb_Entities.usb_nationality, nationalityId, usbNationalityColumns);

            return usbProgrammeOfferingEntity;
        }
    }
}
