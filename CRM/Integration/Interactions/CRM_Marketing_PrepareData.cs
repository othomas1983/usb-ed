﻿using System;
using Belpark.CommonLibrary;
using Belpark.CRMLibrary;
using Belpark.MappingLibrary;
using Microsoft.Xrm.Sdk;

namespace Belpark.Service.Interaction
{
    public class CRM_Marketing_PrepareData
    {
        public static Entity PrepareMarketingContactEntity_UsbEdAlumni(Entity usbedEnrollmentEntity, Entity marketingContactEntity, Entity usbedShortCourseOfferingEntity)
        {
            if (usbedEnrollmentEntity.GetAttributeValue<OptionSetValue>(UsbEd_Entity_Enrollment.statuscode).Value == UsbEd_OptionSet_Enrollment_StatusReason.Passed)
            {
                //Check if Short Course is Comprehensive - Currently only way in CRM is to check if Short Course Name contains "DP"
                string usbedShortCourseName =
                    usbedShortCourseOfferingEntity.GetAttributeValue<EntityReference>(UsbEd_Entity_ShortCourseOffering.stb_shortcourselookup).Name;

                marketingContactEntity[Marketing_Entity_Contact.usb_usbedalumnitwooptions] = true;
                //Check if Comprehensive
                if (usbedShortCourseName.Contains("DP"))
                {
                    marketingContactEntity[Marketing_Entity_Contact.usb_usbalumnitwooptions] = true;
                }
            }

            return marketingContactEntity;
        }

        public static Entity PrepareMarketingContactEntity_UsbAlumni(Entity usbSarEntity, Entity marketingContactEntity)
        {
            if (usbSarEntity.GetAttributeValue<OptionSetValue>(Usb_Entity_SAR.statuscode).Value == Usb_OptionSet_SAR_StatusReason.Graduated
                || (usbSarEntity.Contains(Usb_Entity_SAR.usb_mastersyear)
                    || usbSarEntity.Contains(Usb_Entity_SAR.usb_honoursyear)
                    || usbSarEntity.Contains(Usb_Entity_SAR.usb_phdyear)
                    || usbSarEntity.Contains(Usb_Entity_SAR.usb_postgraduateyear)))
            {
                marketingContactEntity[Marketing_Entity_Contact.usb_usbalumnitwooptions] = true;
            }
            return marketingContactEntity;
        }
        
        //public static Entity PrepareMarketingContactEntity_BpcAlumni(Entity bpcSarEntity, Entity marketingContactEntity)
        //{
        //    if (bpcSarEntity.GetAttributeValue<OptionSetValue>(Bpc_Entity_SAR.statuscode).Value == Bp.srcStatusCompletedSuccessfully
        //        || (bpcSarEntity.Contains(Bpc_Entity_SAR.lt_stud_mastersyear)
        //            || bpcSarEntity.Contains(Bpc_Entity_SAR.lt_stud_honoursyear)
        //            || bpcSarEntity.Contains(Bpc_Entity_SAR.lt_stud_phdyear)
        //            || bpcSarEntity.Contains(Bpc_Entity_SAR.lt_stud_postgraduateyear)))
        //    {
        //        marketingContactEntity[Marketing_Entity_Contact.usb_usbalumnitwooptions] = true;
        //    }

        //    return marketingContactEntity;
        //}

        //TODO: Look at this

        public static Entity PrepareMarketingOfferingStatusEntity_UsbEdStatusReason(Entity marketingOfferingStatusEntity, Entity usbedEnrollmentEntity)
        {
            marketingOfferingStatusEntity[Marketing_Entity_OfferingStatus.usb_usbedstudentstatusoptionset] 
                = Mapper.UsbEdEnrollmentStatus_MarketingOfferingStatus(usbedEnrollmentEntity);

            return marketingOfferingStatusEntity;
        }

        public static Entity PrepareMarketingOfferingStatusEntity_UsbStatusReasonAndGraduationDates(Entity marketingOfferingStatusEntity, Entity usbSarEntity)
        {
            DateTime? honoursYear = Common.FindDateTime(usbSarEntity, Usb_Entity_SAR.usb_honoursyear);
            DateTime? mastersYear = Common.FindDateTime(usbSarEntity, Usb_Entity_SAR.usb_mastersyear);
            DateTime? phdYear = Common.FindDateTime(usbSarEntity, Usb_Entity_SAR.usb_phdyear);
            DateTime? postgraduateYear = Common.FindDateTime(usbSarEntity, Usb_Entity_SAR.usb_postgraduateyear);

            //Graduation Dates
            marketingOfferingStatusEntity[Marketing_Entity_OfferingStatus.usb_honoursyear] = honoursYear;
            marketingOfferingStatusEntity[Marketing_Entity_OfferingStatus.usb_mastersyear] = mastersYear;
            marketingOfferingStatusEntity[Marketing_Entity_OfferingStatus.usb_phdyear] = phdYear;
            marketingOfferingStatusEntity[Marketing_Entity_OfferingStatus.usb_postgraduateyear] = postgraduateYear;

            //Status Code - If One of the Dates above are populated with Data then Status = Graduated

            if (honoursYear != null || mastersYear != null || phdYear != null || postgraduateYear != null)
            {
                marketingOfferingStatusEntity[Marketing_Entity_OfferingStatus.usb_usbstudentstatusoptionset]
                    = new OptionSetValue(Marketing_OptionSet_OfferingStatus_UsbStudentStatus.Graduated);
            }
            else
            {
                marketingOfferingStatusEntity[Marketing_Entity_OfferingStatus.usb_usbstudentstatusoptionset]
                = Mapper.UsbSARStatus_MarketingOfferingStatus(usbSarEntity);
            }
            return marketingOfferingStatusEntity;
        }

        //public static Entity PrepareMarketingOfferingStatusEntity_BpcStatusReasonAndGraduationDates(Entity marketingOfferingStatusEntity, Entity bpcSarEntity)
        //{
        //    DateTime? honoursYear = Common.FindDateTime(bpcSarEntity, Bpc_Entity_SAR.lt_stud_honoursyear);
        //    DateTime? mastersYear = Common.FindDateTime(bpcSarEntity, Bpc_Entity_SAR.lt_stud_mastersyear);
        //    DateTime? phdYear = Common.FindDateTime(bpcSarEntity, Bpc_Entity_SAR.lt_stud_phdyear);
        //    DateTime? postgraduateYear = Common.FindDateTime(bpcSarEntity, Bpc_Entity_SAR.lt_stud_postgraduateyear);

        //    //Graduation Dates
        //    marketingOfferingStatusEntity[Marketing_Entity_OfferingStatus.usb_honoursyear] = honoursYear;
        //    marketingOfferingStatusEntity[Marketing_Entity_OfferingStatus.usb_mastersyear] = mastersYear;
        //    marketingOfferingStatusEntity[Marketing_Entity_OfferingStatus.usb_phdyear] = phdYear;
        //    marketingOfferingStatusEntity[Marketing_Entity_OfferingStatus.usb_postgraduateyear] = postgraduateYear;

        //    //Status Code - If One of the Dates above are populated with Data then Status = Graduated

        //    if (honoursYear != null || mastersYear != null || phdYear != null || postgraduateYear != null)
        //    {
        //        marketingOfferingStatusEntity[Marketing_Entity_OfferingStatus.usb_usbstudentstatusoptionset] 
        //            = new OptionSetValue(Mapper_BpcSarStatus_MarketingContactStatus.destStatusGraduated);
        //    }
        //    else
        //    {
        //        int bpcSarStatus = bpcSarEntity.GetAttributeValue<OptionSetValue>(Bpc_Entity_SAR.statuscode).Value;
        //        int marketingOfferingStatus = Mapper.map_BpcSarStatus_MarketingContactStatus(bpcSarStatus);
        //        if (marketingOfferingStatus != 0)
        //        {
        //            marketingOfferingStatusEntity[Marketing_Entity_OfferingStatus.usb_usbstudentstatusoptionset] = new OptionSetValue(marketingOfferingStatus);
        //        } 
        //    }

        //    return marketingOfferingStatusEntity;
        //}
    }
}
