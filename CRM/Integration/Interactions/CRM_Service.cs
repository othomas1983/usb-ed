﻿using System.Configuration;
using Belpark.CommonLibrary;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk;

namespace Belpark.Service.Interaction
{
    public class CRM_Service
    {
        public static IOrganizationService GetCRMService(string ConnStringName)
        {
            string decodedConnString = Common.DecodeFrom64(ConfigurationManager.ConnectionStrings[ConnStringName].ConnectionString);
            ConnectionStringSettings connStringSettings = new ConnectionStringSettings(ConnStringName, decodedConnString);
            CrmConnection crmConn = new CrmConnection(connStringSettings);
            IOrganizationService service = new OrganizationService(crmConn);
            return (service);
        }
    }
}
