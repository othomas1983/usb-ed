﻿#region Using Statements
using System.ServiceModel;
using Belpark.SyncRef;
using Microsoft.Xrm.Sdk; 
#endregion

namespace Belpark.Service
{
    public class ServiceDefinition
    {
        public static RedirectClient GetInterfaceClient()
        {
            BasicHttpBinding wsBinding = new BasicHttpBinding();
            EndpointAddress endPointAddress = new EndpointAddress("http://localhost/BelparkService/Redirect");
            RedirectClient myClient = new RedirectClient(wsBinding, endPointAddress);
            return myClient;
        }
    }
}
