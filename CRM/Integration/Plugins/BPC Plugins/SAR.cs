﻿namespace Belpark.BpcPlugins
{
    using System;
    using Belpark.Service;
    using Belpark.SyncRef;
    using Microsoft.Xrm.Sdk;

    public class BpcSarStatusSync : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            /* EOHMC - PROCESSING LOGIC
             * On Create or Update
             *      Update or Create Contact
             *      Update or Create Offering and Status
             *      Set Alumni status on Marketing Org if Status Reason = Completed Successfully 
            */
            try
            {
                IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider
                    .GetService(typeof(IPluginExecutionContext));

                RedirectClient myClient = ServiceDefinition.GetInterfaceClient();

                if (context.MessageName == "Create" || context.MessageName == "Update")
                {
                    string resultMessage = myClient.BpcPostSarStatus(context.PrimaryEntityId);
                    if (resultMessage != null)
                    {
                        throw new InvalidPluginExecutionException(resultMessage);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}