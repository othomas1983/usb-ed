﻿namespace Belpark.BpcPlugins
{
    using System;
    using Belpark.Service;
    using Belpark.SyncRef;
    using Microsoft.Xrm.Sdk;

    public class UsbEdEnrollmentStatusSync : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            /* EOHMC - PROCESSING LOGIC
             * On Create or Update
             *      Update or Create Contact
             *      Update or Create Offering and Status
             *      Set Alumni status on Marketing Org if Status Reason = Passed 
            */
            try
            {
                IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider
                    .GetService(typeof(IPluginExecutionContext));

                ContactSyncClient myClient = ServiceDefinition.GetInterfaceClient();

                if (context.MessageName == "Create" || context.MessageName == "Update")
                {
                    string resultMessage = myClient.UsbEdPostEnrollmentStatus(context.PrimaryEntityId);
                    if (resultMessage != null)
                    {
                        throw new InvalidPluginExecutionException(resultMessage);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}