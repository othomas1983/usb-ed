﻿namespace Belpark.BpcPlugins
{
    using System;
    using Belpark.Service;
    using Belpark.SyncRef;
    using Microsoft.Xrm.Sdk;

    public class UsbEdContactSync : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            /* EOHMC - PROCESSING LOGIC
             * On Create - Sync new record to Marketing Org
             * On Update - Sync updated record to Marketing Org
            */
            try
            {
                IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider
                    .GetService(typeof(IPluginExecutionContext));

                ContactSyncClient myClient = ServiceDefinition.GetInterfaceClient();

                if (context.MessageName == "Create" || context.MessageName == "Update")
                {
                    string resultMessage = myClient.UsbEdPostContactSync(context.PrimaryEntityId);
                    if (resultMessage != null)
                    {
                        throw new InvalidPluginExecutionException(resultMessage);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}