﻿#region Using Statements
using System.ServiceModel;
using Belpark.SyncRef; 
#endregion

namespace Belpark.Service
{
    public class ServiceDefinition
    {
        public static ContactSyncClient GetInterfaceClient()
        {
            EndpointAddress endPointAddress = new EndpointAddress("http://localhost/BelparkService/service");
            BasicHttpBinding wsBinding = new BasicHttpBinding();
            ContactSyncClient myClient = new ContactSyncClient(wsBinding, endPointAddress);
            return myClient;
        }
    }
}
