﻿using System;
using Belpark.CommonLibrary;
using Belpark.CRMLibrary;
using Microsoft.Xrm.Sdk;

namespace Belpark.MappingLibrary
{
    public class Mapper
    {
        public static OptionSetValue UsbEdEnrollmentStatus_MarketingOfferingStatus(Entity usbedEnrollmentEntity)
        {
            OptionSetValue marketingUsbedStudentStatusOptionSet = new OptionSetValue();
            int usbedStatusReasonValue = Common.FindOptionSet(usbedEnrollmentEntity, UsbEd_Entity_Enrollment.statuscode).Value;

            switch (usbedStatusReasonValue)
            {
                case UsbEd_OptionSet_Enrollment_StatusReason.Applied:
                    marketingUsbedStudentStatusOptionSet.Value = Marketing_OptionSet_OfferingStatus_UsbEdStudentStatus.Applied;
                    break;

                case UsbEd_OptionSet_Enrollment_StatusReason.Qualified:
                    marketingUsbedStudentStatusOptionSet.Value = Marketing_OptionSet_OfferingStatus_UsbEdStudentStatus.Qualified;
                    break;

                case UsbEd_OptionSet_Enrollment_StatusReason.Approved:
                    marketingUsbedStudentStatusOptionSet.Value = Marketing_OptionSet_OfferingStatus_UsbEdStudentStatus.Approved;
                    break;

                case UsbEd_OptionSet_Enrollment_StatusReason.Admitted:
                    marketingUsbedStudentStatusOptionSet.Value = Marketing_OptionSet_OfferingStatus_UsbEdStudentStatus.Admitted;
                    break;

                case UsbEd_OptionSet_Enrollment_StatusReason.Waitlisted:
                    marketingUsbedStudentStatusOptionSet.Value = Marketing_OptionSet_OfferingStatus_UsbEdStudentStatus.Waitlisted;
                    break;

                case UsbEd_OptionSet_Enrollment_StatusReason.NotAdmitted:
                    marketingUsbedStudentStatusOptionSet.Value = Marketing_OptionSet_OfferingStatus_UsbEdStudentStatus.NotAdmitted;
                    break;

                case UsbEd_OptionSet_Enrollment_StatusReason.Registered:
                    marketingUsbedStudentStatusOptionSet.Value = Marketing_OptionSet_OfferingStatus_UsbEdStudentStatus.Registered;
                    break;

                case UsbEd_OptionSet_Enrollment_StatusReason.Passed:
                    marketingUsbedStudentStatusOptionSet.Value = Marketing_OptionSet_OfferingStatus_UsbEdStudentStatus.Passed;
                    break;

                case UsbEd_OptionSet_Enrollment_StatusReason.Failed:
                    marketingUsbedStudentStatusOptionSet.Value = Marketing_OptionSet_OfferingStatus_UsbEdStudentStatus.Failed;
                    break;

                case UsbEd_OptionSet_Enrollment_StatusReason.Cancelled:
                    marketingUsbedStudentStatusOptionSet.Value = Marketing_OptionSet_OfferingStatus_UsbEdStudentStatus.Cancelled;
                    break;

                case UsbEd_OptionSet_Enrollment_StatusReason.Deactivated:
                    marketingUsbedStudentStatusOptionSet.Value = Marketing_OptionSet_OfferingStatus_UsbEdStudentStatus.Deactivated;
                    break;
                                        
                default:
                    marketingUsbedStudentStatusOptionSet.Value = Marketing_OptionSet_OfferingStatus_UsbEdStudentStatus.Undefined;
                    break;
            }
            return marketingUsbedStudentStatusOptionSet;
        }

        public static OptionSetValue UsbSARStatus_MarketingOfferingStatus(Entity usbSAREntity)
        {
            OptionSetValue marketingUsbStudentStatusOptionSet = new OptionSetValue();
            int usbStatusReasonValue = Common.FindOptionSet(usbSAREntity, Usb_Entity_SAR.statuscode).Value;

            switch (usbStatusReasonValue)
            {
                case Usb_OptionSet_SAR_StatusReason.Applicant:
                    marketingUsbStudentStatusOptionSet.Value = Marketing_OptionSet_OfferingStatus_UsbStudentStatus.Applicant;
                    break;

                case Usb_OptionSet_SAR_StatusReason.ApplicationCompleted:
                    marketingUsbStudentStatusOptionSet.Value = Marketing_OptionSet_OfferingStatus_UsbStudentStatus.ApplicationCompleted;
                    break;

                case Usb_OptionSet_SAR_StatusReason.InterviewRound1:
                    marketingUsbStudentStatusOptionSet.Value = Marketing_OptionSet_OfferingStatus_UsbStudentStatus.InterviewRound1;
                    break;

                case Usb_OptionSet_SAR_StatusReason.SelectionCommittee:
                    marketingUsbStudentStatusOptionSet.Value = Marketing_OptionSet_OfferingStatus_UsbStudentStatus.SelectionCommittee;
                    break;

                case Usb_OptionSet_SAR_StatusReason.InterviewRound2:
                    marketingUsbStudentStatusOptionSet.Value = Marketing_OptionSet_OfferingStatus_UsbStudentStatus.InterviewRound2;
                    break;

                case Usb_OptionSet_SAR_StatusReason.Cancelled:
                    marketingUsbStudentStatusOptionSet.Value = Marketing_OptionSet_OfferingStatus_UsbStudentStatus.Cancelled;
                    break;

                case Usb_OptionSet_SAR_StatusReason.NotAdmitted:
                    marketingUsbStudentStatusOptionSet.Value = Marketing_OptionSet_OfferingStatus_UsbStudentStatus.NotAdmitted;
                    break;

                case Usb_OptionSet_SAR_StatusReason.WaitingList:
                    marketingUsbStudentStatusOptionSet.Value = Marketing_OptionSet_OfferingStatus_UsbStudentStatus.Waitlisted;
                    break;

                case Usb_OptionSet_SAR_StatusReason.RequestAdmitted:
                    marketingUsbStudentStatusOptionSet.Value = Marketing_OptionSet_OfferingStatus_UsbStudentStatus.Undefined;
                    break;

                case Usb_OptionSet_SAR_StatusReason.Admitted:
                    marketingUsbStudentStatusOptionSet.Value = Marketing_OptionSet_OfferingStatus_UsbStudentStatus.Admitted;
                    break;

                case Usb_OptionSet_SAR_StatusReason.FailedAdmitted:
                    marketingUsbStudentStatusOptionSet.Value = Marketing_OptionSet_OfferingStatus_UsbStudentStatus.Undefined;
                    break;

                case Usb_OptionSet_SAR_StatusReason.Postponed:
                    marketingUsbStudentStatusOptionSet.Value = Marketing_OptionSet_OfferingStatus_UsbStudentStatus.Postponed;
                    break;

                case Usb_OptionSet_SAR_StatusReason.RequestNotAccepted:
                    marketingUsbStudentStatusOptionSet.Value = Marketing_OptionSet_OfferingStatus_UsbStudentStatus.Undefined;
                    break;

                case Usb_OptionSet_SAR_StatusReason.NotAccepted:
                    marketingUsbStudentStatusOptionSet.Value = Marketing_OptionSet_OfferingStatus_UsbStudentStatus.NotAccepted;
                    break;

                case Usb_OptionSet_SAR_StatusReason.FailedNotAccepted:
                    marketingUsbStudentStatusOptionSet.Value = Marketing_OptionSet_OfferingStatus_UsbStudentStatus.Undefined;
                    break;

                case Usb_OptionSet_SAR_StatusReason.Accepted:
                    marketingUsbStudentStatusOptionSet.Value = Marketing_OptionSet_OfferingStatus_UsbStudentStatus.Accepted;
                    break;

                case Usb_OptionSet_SAR_StatusReason.Registered:
                    marketingUsbStudentStatusOptionSet.Value = Marketing_OptionSet_OfferingStatus_UsbStudentStatus.Registered;
                    break;

                case Usb_OptionSet_SAR_StatusReason.Unsuccessful:
                    marketingUsbStudentStatusOptionSet.Value = Marketing_OptionSet_OfferingStatus_UsbStudentStatus.Unsuccessfull;
                    break;

                case Usb_OptionSet_SAR_StatusReason.Discontinued:
                    marketingUsbStudentStatusOptionSet.Value = Marketing_OptionSet_OfferingStatus_UsbStudentStatus.Discontinued;
                    break;

                case Usb_OptionSet_SAR_StatusReason.Graduated:
                    marketingUsbStudentStatusOptionSet.Value = Marketing_OptionSet_OfferingStatus_UsbStudentStatus.Graduated;
                    break;

                case Usb_OptionSet_SAR_StatusReason.Deactivated:
                    marketingUsbStudentStatusOptionSet.Value = Marketing_OptionSet_OfferingStatus_UsbStudentStatus.Deactivated;
                    break;

                default:
                    marketingUsbStudentStatusOptionSet.Value = Marketing_OptionSet_OfferingStatus_UsbStudentStatus.Undefined;
                    break;
            }
            return marketingUsbStudentStatusOptionSet;
        }

        //public static int map_BpcSarStatus_MarketingContactStatus(int BpcSarStatus)
        //{
        //    Dictionary<int, int> mappedStatusReason = new Dictionary<int, int>();
        //    mappedStatusReason.Add
        //        (Mapper_BpcSarStatus_MarketingContactStatus.srcStatusApplicant
        //        , Mapper_BpcSarStatus_MarketingContactStatus.destStatusApplicant);
        //    mappedStatusReason.Add
        //        (Mapper_BpcSarStatus_MarketingContactStatus.srcStatusInterviewRequired
        //        , Mapper_BpcSarStatus_MarketingContactStatus.destStatusInterviewRound1);
        //    mappedStatusReason.Add
        //        (Mapper_BpcSarStatus_MarketingContactStatus.srcStatusSelectionProcess
        //        , Mapper_BpcSarStatus_MarketingContactStatus.destStatusSelectionCommittee);
        //    mappedStatusReason.Add
        //        (Mapper_BpcSarStatus_MarketingContactStatus.srcStatusApprovalRequired
        //        , Mapper_BpcSarStatus_MarketingContactStatus.destStatusInterviewRound2);
        //    mappedStatusReason.Add
        //        (Mapper_BpcSarStatus_MarketingContactStatus.srcStatusAdmitted
        //        , Mapper_BpcSarStatus_MarketingContactStatus.destStatusAdmitted);
        //    mappedStatusReason.Add
        //        (Mapper_BpcSarStatus_MarketingContactStatus.srcStatusNotAdmitted
        //        , Mapper_BpcSarStatus_MarketingContactStatus.destStatusNotAdmitted);
        //    mappedStatusReason.Add
        //        (Mapper_BpcSarStatus_MarketingContactStatus.srcStatusWaitingList
        //        , Mapper_BpcSarStatus_MarketingContactStatus.destStatusWaitlisted);
        //    mappedStatusReason.Add
        //        (Mapper_BpcSarStatus_MarketingContactStatus.srcStatusPossibleApplicant
        //        , Mapper_BpcSarStatus_MarketingContactStatus.destStatusWaitlisted);
        //    mappedStatusReason.Add
        //        (Mapper_BpcSarStatus_MarketingContactStatus.srcStatusUncertain
        //        , Mapper_BpcSarStatus_MarketingContactStatus.destStatusWaitlisted);
        //    mappedStatusReason.Add
        //        (Mapper_BpcSarStatus_MarketingContactStatus.srcStatusMovedToAnotherProgramme
        //        , Mapper_BpcSarStatus_MarketingContactStatus.destStatusPostponed);
        //    mappedStatusReason.Add
        //        (Mapper_BpcSarStatus_MarketingContactStatus.srcStatusAccepted
        //        , Mapper_BpcSarStatus_MarketingContactStatus.destStatusAccepted);
        //    mappedStatusReason.Add
        //        (Mapper_BpcSarStatus_MarketingContactStatus.srcStatusNotAccepted
        //        , Mapper_BpcSarStatus_MarketingContactStatus.destStatusNotAccepted);
        //    mappedStatusReason.Add
        //        (Mapper_BpcSarStatus_MarketingContactStatus.srcStatusRegistered
        //        , Mapper_BpcSarStatus_MarketingContactStatus.destStatusRegistered);
        //    mappedStatusReason.Add
        //        (Mapper_BpcSarStatus_MarketingContactStatus.srcStatusCompletedSuccessfully
        //        , Mapper_BpcSarStatus_MarketingContactStatus.destStatusGraduated);
        //    mappedStatusReason.Add
        //        (Mapper_BpcSarStatus_MarketingContactStatus.srcStatusCompletedUnsuccessfully
        //        , Mapper_BpcSarStatus_MarketingContactStatus.destStatusUnsuccessfull);
        //    mappedStatusReason.Add
        //        (Mapper_BpcSarStatus_MarketingContactStatus.srcStatusDiscontinued
        //        , Mapper_BpcSarStatus_MarketingContactStatus.destStatusDiscontinued);
        //    mappedStatusReason.Add
        //        (Mapper_BpcSarStatus_MarketingContactStatus.srcStatusFinancial
        //        , Mapper_BpcSarStatus_MarketingContactStatus.destStatusDiscontinued);
        //    mappedStatusReason.Add
        //        (Mapper_BpcSarStatus_MarketingContactStatus.srcStatusMedical
        //        , Mapper_BpcSarStatus_MarketingContactStatus.destStatusDiscontinued);

        //    int MarketingContactStatus;
        //    mappedStatusReason.TryGetValue(BpcSarStatus, out MarketingContactStatus);
        //    return MarketingContactStatus;
        //}

        public static OptionSetValue UsbContactLanguage_MarketingContactLanguage(Entity usbContactEntity)
        {
            OptionSetValue marketingContactLanguageOptionSet = new OptionSetValue();
            string usbCorrespondenceLanguageValue = Common.FindEntityReferenceName(usbContactEntity, Usb_Entity_Contact.usb_correspondencelanguagelookup);

            switch (usbCorrespondenceLanguageValue)
            {
                case "English":
                    marketingContactLanguageOptionSet.Value = Marketing_OptionSet_Contact_Language.English;
                    break;

                case "Afrikaans":
                    marketingContactLanguageOptionSet.Value = Marketing_OptionSet_Contact_Language.Afrikaans;
                    break;

                default:
                    marketingContactLanguageOptionSet.Value = Marketing_OptionSet_Contact_Language.English;
                    break;
            }
            return marketingContactLanguageOptionSet;
        }

        public static Entity UsbContactNationality_MarketingContactNationality(Entity marketingContactEntity, Guid marketingNationalityId)
        {
            EntityReference marketingContactNationalityReference = new EntityReference(Marketing_Entities.usb_nationality, marketingNationalityId);

            marketingContactEntity[Marketing_Entity_Contact.usb_nationalitylookup] = marketingContactNationalityReference;

            return marketingContactEntity;
        }

        //public static OptionSetValue UsbEmploymentHistoryIndustry_MarketingContactIndustry(Entity usbEmploymentHistoryEntity)
        //{
        //    OptionSetValue marketingContactIndustryOptionSet = new OptionSetValue();
        //    int usbIndustryValue = Common.FindOptionSet(usbEmploymentHistoryEntity, Usb_Entity_EmploymentHistory.usb_industryoptionset).Value;

        //    switch (usbIndustryValue)
        //    {
        //        case Usb_OptionSet_EmploymentHistory_Industry.AgricultureFoodAndBeverages:
        //            marketingContactIndustryOptionSet.Value = Marketing_OptionSet_Contact_Industry.AgricultureFoodAndBeverages;
        //            break;

        //        case Usb_OptionSet_EmploymentHistory_Industry.DefenceAndSecurityServices:
        //            marketingContactIndustryOptionSet.Value = Marketing_OptionSet_Contact_Industry.DefenceAndSecurityServices;
        //            break;

        //        case Usb_OptionSet_EmploymentHistory_Industry.EducationAndDevelopment:
        //            marketingContactIndustryOptionSet.Value = Marketing_OptionSet_Contact_Industry.EducationAndDevelopment;
        //            break;

        //        case Usb_OptionSet_EmploymentHistory_Industry.ConsultancyServices:
        //            marketingContactIndustryOptionSet.Value = Marketing_OptionSet_Contact_Industry.ConsultancyServices;
        //            break;

        //        case Usb_OptionSet_EmploymentHistory_Industry.HealthAndWellness:
        //            marketingContactIndustryOptionSet.Value = Marketing_OptionSet_Contact_Industry.HealthAndWellness;
        //            break;

        //        case Usb_OptionSet_EmploymentHistory_Industry.FinancialServicesAndInsurance:
        //            marketingContactIndustryOptionSet.Value = Marketing_OptionSet_Contact_Industry.FinancialServicesAndInsurance;
        //            break;

        //        case Usb_OptionSet_EmploymentHistory_Industry.RetailAndTrade:
        //            marketingContactIndustryOptionSet.Value = Marketing_OptionSet_Contact_Industry.RetailAndTrade;
        //            break;

        //        case Usb_OptionSet_EmploymentHistory_Industry.TransportAndLogistics:
        //            marketingContactIndustryOptionSet.Value = Marketing_OptionSet_Contact_Industry.TransportAndLogistics;
        //            break;

        //        case Usb_OptionSet_EmploymentHistory_Industry.HospitalitySportAndRecreation:
        //            marketingContactIndustryOptionSet.Value = Marketing_OptionSet_Contact_Industry.HospitalitySportAndRecreation;
        //            break;

        //        case Usb_OptionSet_EmploymentHistory_Industry.EngineeringAndConstruction:
        //            marketingContactIndustryOptionSet.Value = Marketing_OptionSet_Contact_Industry.EngineeringAndConstruction;
        //            break;

        //        case Usb_OptionSet_EmploymentHistory_Industry.Property:
        //            marketingContactIndustryOptionSet.Value = Marketing_OptionSet_Contact_Industry.Property;
        //            break;

        //        case Usb_OptionSet_EmploymentHistory_Industry.MiningAndEnergy:
        //            marketingContactIndustryOptionSet.Value = Marketing_OptionSet_Contact_Industry.MiningAndEnergy;
        //            break;

        //        case Usb_OptionSet_EmploymentHistory_Industry.ElectronicsAndICT:
        //            marketingContactIndustryOptionSet.Value = Marketing_OptionSet_Contact_Industry.ElectronicsAndICT;
        //            break;

        //        case Usb_OptionSet_EmploymentHistory_Industry.CommunicationAndMarketing:
        //            marketingContactIndustryOptionSet.Value = Marketing_OptionSet_Contact_Industry.CommunicationAndMarketing;
        //            break;

        //        case Usb_OptionSet_EmploymentHistory_Industry.PublicSectorAndNGO:
        //            marketingContactIndustryOptionSet.Value = Marketing_OptionSet_Contact_Industry.PublicSectorAndNGO;
        //            break;

        //        case Usb_OptionSet_EmploymentHistory_Industry.Other:
        //            marketingContactIndustryOptionSet.Value = Marketing_OptionSet_Contact_Industry.Other;
        //            break;

        //        case Usb_OptionSet_EmploymentHistory_Industry.BusinessManagementAndEntrepreneurship:
        //            marketingContactIndustryOptionSet.Value = Marketing_OptionSet_Contact_Industry.BusinessManagementAndEntrepreneurship;
        //            break;

        //        case Usb_OptionSet_EmploymentHistory_Industry.ReligiousSocialAndPoliticalStudies:
        //            marketingContactIndustryOptionSet.Value = Marketing_OptionSet_Contact_Industry.ReligiousSocialAndPoliticalStudies;
        //            break;

        //        case Usb_OptionSet_EmploymentHistory_Industry.VeterinaryAndAnimalScience:
        //            marketingContactIndustryOptionSet.Value = Marketing_OptionSet_Contact_Industry.VeterinaryAndAnimalScience;
        //            break;

        //        case Usb_OptionSet_EmploymentHistory_Industry.LawAndHumanRights:
        //            marketingContactIndustryOptionSet.Value = Marketing_OptionSet_Contact_Industry.LawAndHumanRights;
        //            break;

        //        default:
        //            marketingContactIndustryOptionSet.Value = Marketing_OptionSet_Contact_Industry.Other;
        //            break;
        //    }
        //    return marketingContactIndustryOptionSet;
        //}

        //public static OptionSetValue UsbEmploymentHistoryWorkArea_MarketingContactWorkArea(Entity usbEmploymentHistoryEntity)
        //{
        //    OptionSetValue marketingContactWorkAreaOptionSet = new OptionSetValue();
        //    int usbWorkAreaValue = Common.FindOptionSet(usbEmploymentHistoryEntity, Usb_Entity_EmploymentHistory.usb_workareaoptionset).Value;

        //    switch (usbWorkAreaValue)
        //    {
        //        case Usb_OptionSet_EmploymentHistory_WorkArea.Finance:
        //            marketingContactWorkAreaOptionSet.Value = Marketing_OptionSet_Contact_WorkArea.Finance;
        //            break;

        //        case Usb_OptionSet_EmploymentHistory_WorkArea.Management:
        //            marketingContactWorkAreaOptionSet.Value = Marketing_OptionSet_Contact_WorkArea.Management;
        //            break;

        //        case Usb_OptionSet_EmploymentHistory_WorkArea.HumanResource:
        //            marketingContactWorkAreaOptionSet.Value = Marketing_OptionSet_Contact_WorkArea.HumanResource;
        //            break;

        //        case Usb_OptionSet_EmploymentHistory_WorkArea.IT:
        //            marketingContactWorkAreaOptionSet.Value = Marketing_OptionSet_Contact_WorkArea.IT;
        //            break;

        //        case Usb_OptionSet_EmploymentHistory_WorkArea.Marketing:
        //            marketingContactWorkAreaOptionSet.Value = Marketing_OptionSet_Contact_WorkArea.Marketing;
        //            break;

        //        case Usb_OptionSet_EmploymentHistory_WorkArea.Production:
        //            marketingContactWorkAreaOptionSet.Value = Marketing_OptionSet_Contact_WorkArea.Production;
        //            break;

        //        case Usb_OptionSet_EmploymentHistory_WorkArea.Sales:
        //            marketingContactWorkAreaOptionSet.Value = Marketing_OptionSet_Contact_WorkArea.Sales;
        //            break;

        //        case Usb_OptionSet_EmploymentHistory_WorkArea.Administration:
        //            marketingContactWorkAreaOptionSet.Value = Marketing_OptionSet_Contact_WorkArea.Administration;
        //            break;

        //        case Usb_OptionSet_EmploymentHistory_WorkArea.StrategyAndPlanning:
        //            marketingContactWorkAreaOptionSet.Value = Marketing_OptionSet_Contact_WorkArea.StrategyAndPlanning;
        //            break;

        //        case Usb_OptionSet_EmploymentHistory_WorkArea.Engineering:
        //            marketingContactWorkAreaOptionSet.Value = Marketing_OptionSet_Contact_WorkArea.Engineering;
        //            break;

        //        case Usb_OptionSet_EmploymentHistory_WorkArea.Law:
        //            marketingContactWorkAreaOptionSet.Value = Marketing_OptionSet_Contact_WorkArea.Law;
        //            break;

        //        case Usb_OptionSet_EmploymentHistory_WorkArea.Other:
        //            marketingContactWorkAreaOptionSet.Value = Marketing_OptionSet_Contact_WorkArea.Other;
        //            break;

        //        default:
        //            marketingContactWorkAreaOptionSet.Value = Marketing_OptionSet_Contact_WorkArea.Other;
        //            break;
        //    }
        //    return marketingContactWorkAreaOptionSet;
        //}

        //public static OptionSetValue PrepareMarketingContactEntity_BpcLanguageMapping(Entity bpcContactEntity)
        //{
        //    OptionSetValue bpcLanguageOptionSet = new OptionSetValue();

        //    //Assign mapping here

        //    return bpcLanguageOptionSet;
        //}

        //public static EntityReference PrepareMarketingContactEntity_BpcNationalityMapping(Entity bpcContactEntity)
        //{
        //    EntityReference bpcNationalityReference = new EntityReference();

        //    //Assign mapping here

        //    return bpcNationalityReference;
        //}

        //public static OptionSetValue PrepareMarketingContactEntity_BpcIndustryMapping(Entity bpcContactEntity)
        //{
        //    OptionSetValue bpcIndustryOptionSet = new OptionSetValue();
        //    int bpcIndustryValue = Common.FindOptionSet(bpcContactEntity, Bpc_Entity_Contact.lt_industry_os).Value;

        //    switch (bpcIndustryValue)
        //    {
        //        case Bpc_OptionSet_Contact_Industry.FinancialServices:
        //            bpcIndustryOptionSet.Value = Marketing_OptionSet_Contact_Industry.FinancialServicesAndInsurance;
        //            break;

        //        case Bpc_OptionSet_Contact_Industry.EngineeringAndConstruction:
        //            bpcIndustryOptionSet.Value = Marketing_OptionSet_Contact_Industry.EngineeringAndConstruction;
        //            break;

        //        case Bpc_OptionSet_Contact_Industry.PublicSectorAndGovernment:
        //            bpcIndustryOptionSet.Value = Marketing_OptionSet_Contact_Industry.PublicSectorAndNGO;
        //            break;

        //        case Bpc_OptionSet_Contact_Industry.MineralAndEnergyAffairs:
        //            bpcIndustryOptionSet.Value = Marketing_OptionSet_Contact_Industry.MiningAndEnergy;
        //            break;

        //        case Bpc_OptionSet_Contact_Industry.ElectronicsAndIT:
        //            bpcIndustryOptionSet.Value = Marketing_OptionSet_Contact_Industry.ElectronicsAndICT;
        //            break;

        //        case Bpc_OptionSet_Contact_Industry.Transport:
        //            bpcIndustryOptionSet.Value = Marketing_OptionSet_Contact_Industry.TransportAndLogistics;
        //            break;

        //        case Bpc_OptionSet_Contact_Industry.AgricultureFoodAndBeverages:
        //            bpcIndustryOptionSet.Value = Marketing_OptionSet_Contact_Industry.AgricultureFoodAndBeverages;
        //            break;

        //        case Bpc_OptionSet_Contact_Industry.EducationAndDevelopment:
        //            bpcIndustryOptionSet.Value = Marketing_OptionSet_Contact_Industry.EducationAndDevelopment;
        //            break;

        //        case Bpc_OptionSet_Contact_Industry.TradeAndIndustrial:
        //            bpcIndustryOptionSet.Value = Marketing_OptionSet_Contact_Industry.RetailAndTrade;
        //            break;

        //        case Bpc_OptionSet_Contact_Industry.CommunicationAndMarketing:
        //            bpcIndustryOptionSet.Value = Marketing_OptionSet_Contact_Industry.CommunicationAndMarketing;
        //            break;

        //        case Bpc_OptionSet_Contact_Industry.HealthAndMedical:
        //            bpcIndustryOptionSet.Value = Marketing_OptionSet_Contact_Industry.HealthAndWellness;
        //            break;

        //        case Bpc_OptionSet_Contact_Industry.ManagementAndConsultation:
        //            bpcIndustryOptionSet.Value = Marketing_OptionSet_Contact_Industry.ConsultancyServices;
        //            break;

        //        case Bpc_OptionSet_Contact_Industry.Other:
        //            bpcIndustryOptionSet.Value = Marketing_OptionSet_Contact_Industry.Other;
        //            break;

        //        case Bpc_OptionSet_Contact_Industry.DefenceAndSecurity:
        //            bpcIndustryOptionSet.Value = Marketing_OptionSet_Contact_Industry.DefenceAndSecurityServices;
        //            break;

        //        case Bpc_OptionSet_Contact_Industry.Property:
        //            bpcIndustryOptionSet.Value = Marketing_OptionSet_Contact_Industry.Property;
        //            break;

        //        case Bpc_OptionSet_Contact_Industry.HRAndSocialSciences:
        //            bpcIndustryOptionSet.Value = Marketing_OptionSet_Contact_Industry.ReligiousSocialAndPoliticalStudies;
        //            break;

        //        case Bpc_OptionSet_Contact_Industry.HospitalityServices:
        //            bpcIndustryOptionSet.Value = Marketing_OptionSet_Contact_Industry.HospitalitySportAndRecreation;
        //            break;

        //        case Bpc_OptionSet_Contact_Industry.ManagingConsultant:
        //            bpcIndustryOptionSet.Value = Marketing_OptionSet_Contact_Industry.ConsultancyServices;
        //            break;

        //        case Bpc_OptionSet_Contact_Industry.Law:
        //            bpcIndustryOptionSet.Value = Marketing_OptionSet_Contact_Industry.LawAndHumanRights;
        //            break;

        //        case Bpc_OptionSet_Contact_Industry.ContinuousImprovementAndSupplyChainConsultant:
        //            bpcIndustryOptionSet.Value = Marketing_OptionSet_Contact_Industry.TransportAndLogistics;
        //            break;

        //        case Bpc_OptionSet_Contact_Industry.Quay62Media:
        //            bpcIndustryOptionSet.Value = Marketing_OptionSet_Contact_Industry.CommunicationAndMarketing;
        //            break;

        //        case Bpc_OptionSet_Contact_Industry.CommunicationDirector:
        //            bpcIndustryOptionSet.Value = Marketing_OptionSet_Contact_Industry.CommunicationAndMarketing;
        //            break;

        //        case Bpc_OptionSet_Contact_Industry.DataAnalyst:
        //            bpcIndustryOptionSet.Value = Marketing_OptionSet_Contact_Industry.ConsultancyServices;
        //            break;

        //        case Bpc_OptionSet_Contact_Industry.CEODirectorMngDirector:
        //            bpcIndustryOptionSet.Value = Marketing_OptionSet_Contact_Industry.AgricultureFoodAndBeverages; //?
        //            break;

        //        case Bpc_OptionSet_Contact_Industry.ChiefCommercialOfficer:
        //            bpcIndustryOptionSet.Value = Marketing_OptionSet_Contact_Industry.RetailAndTrade;
        //            break;

        //        case Bpc_OptionSet_Contact_Industry.GrapeBreederAndEvaluator:
        //            bpcIndustryOptionSet.Value = Marketing_OptionSet_Contact_Industry.AgricultureFoodAndBeverages;
        //            break;

        //        case Bpc_OptionSet_Contact_Industry.ManagingDirector:
        //            bpcIndustryOptionSet.Value = Marketing_OptionSet_Contact_Industry.AgricultureFoodAndBeverages; //?
        //            break;

        //        default:
        //            bpcIndustryOptionSet.Value = Marketing_OptionSet_Contact_Industry.PublicSectorAndNGO; //?
        //            break;
        //    }
        //    return bpcIndustryOptionSet;
        //}

        //public static OptionSetValue PrepareMarketingContactEntity_BpcWorkAreaMapping(Entity bpcContactEntity)
        //{
        //    OptionSetValue bpcWorkAreaOptionSet = new OptionSetValue();
        //    int bpcWorkAreaValue = Common.FindOptionSet(bpcContactEntity, Bpc_Entity_Contact.lt_workarea_os).Value;

        //    switch (bpcWorkAreaValue)
        //    {
        //        case Bpc_OptionSet_Contact_WorkArea.Date20071231:
        //            bpcWorkAreaOptionSet.Value = Marketing_OptionSet_Contact_WorkArea.Other;
        //            break;

        //        case Bpc_OptionSet_Contact_WorkArea.Administration:
        //            bpcWorkAreaOptionSet.Value = Marketing_OptionSet_Contact_WorkArea.Administration;
        //            break;

        //        case Bpc_OptionSet_Contact_WorkArea.AgricultureFoodAndBeverages:
        //            bpcWorkAreaOptionSet.Value = Marketing_OptionSet_Contact_WorkArea.Management;
        //            break;

        //        case Bpc_OptionSet_Contact_WorkArea.Director:
        //            bpcWorkAreaOptionSet.Value = Marketing_OptionSet_Contact_WorkArea.Management;
        //            break;

        //        case Bpc_OptionSet_Contact_WorkArea.ElectronicsAndIT:
        //            bpcWorkAreaOptionSet.Value = Marketing_OptionSet_Contact_WorkArea.IT;
        //            break;

        //        case Bpc_OptionSet_Contact_WorkArea.Engineering:
        //            bpcWorkAreaOptionSet.Value = Marketing_OptionSet_Contact_WorkArea.Engineering;
        //            break;

        //        case Bpc_OptionSet_Contact_WorkArea.EngineeringAndConstruction:
        //            bpcWorkAreaOptionSet.Value = Marketing_OptionSet_Contact_WorkArea.Engineering;
        //            break;

        //        case Bpc_OptionSet_Contact_WorkArea.Finance:
        //            bpcWorkAreaOptionSet.Value = Marketing_OptionSet_Contact_WorkArea.Finance;
        //            break;

        //        case Bpc_OptionSet_Contact_WorkArea.FinancialServices:
        //            bpcWorkAreaOptionSet.Value = Marketing_OptionSet_Contact_WorkArea.Finance;
        //            break;

        //        case Bpc_OptionSet_Contact_WorkArea.HumanResources:
        //            bpcWorkAreaOptionSet.Value = Marketing_OptionSet_Contact_WorkArea.HumanResource;
        //            break;

        //        case Bpc_OptionSet_Contact_WorkArea.IT:
        //            bpcWorkAreaOptionSet.Value = Marketing_OptionSet_Contact_WorkArea.IT;
        //            break;

        //        case Bpc_OptionSet_Contact_WorkArea.Law:
        //            bpcWorkAreaOptionSet.Value = Marketing_OptionSet_Contact_WorkArea.Law;
        //            break;

        //        case Bpc_OptionSet_Contact_WorkArea.Management:
        //            bpcWorkAreaOptionSet.Value = Marketing_OptionSet_Contact_WorkArea.Management;
        //            break;

        //        case Bpc_OptionSet_Contact_WorkArea.Marketing:
        //            bpcWorkAreaOptionSet.Value = Marketing_OptionSet_Contact_WorkArea.Marketing;
        //            break;

        //        case Bpc_OptionSet_Contact_WorkArea.Other:
        //            bpcWorkAreaOptionSet.Value = Marketing_OptionSet_Contact_WorkArea.Other;
        //            break;

        //        case Bpc_OptionSet_Contact_WorkArea.Production:
        //            bpcWorkAreaOptionSet.Value = Marketing_OptionSet_Contact_WorkArea.Production;
        //            break;

        //        case Bpc_OptionSet_Contact_WorkArea.Sales:
        //            bpcWorkAreaOptionSet.Value = Marketing_OptionSet_Contact_WorkArea.Sales;
        //            break;

        //        case Bpc_OptionSet_Contact_WorkArea.StrategyAndPlanning:
        //            bpcWorkAreaOptionSet.Value = Marketing_OptionSet_Contact_WorkArea.StrategyAndPlanning;
        //            break;

        //        case Bpc_OptionSet_Contact_WorkArea.TradeAndIndustrial:
        //            bpcWorkAreaOptionSet.Value = Marketing_OptionSet_Contact_WorkArea.Finance;
        //            break;

        //        default:
        //            bpcWorkAreaOptionSet.Value = Marketing_OptionSet_Contact_WorkArea.Other;
        //            break;
        //    }
        //    return bpcWorkAreaOptionSet;
        //}
    }
}
