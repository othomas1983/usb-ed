﻿using System;
using Microsoft.Xrm.Sdk;

namespace Belpark.CommonLibrary
{
    public class Common
    {        
        public static string DecodeFrom64(string encodedData)
        {
            byte[] encodedDataAsBytes = Convert.FromBase64String(encodedData);
            string returnValue = System.Text.Encoding.ASCII.GetString(encodedDataAsBytes);

            return returnValue;
        }

        public static string EncodeTo64(string toEncode)
        {
            byte[] toEncodeAsBytes = System.Text.Encoding.ASCII.GetBytes(toEncode);
            string returnValue = Convert.ToBase64String(toEncodeAsBytes);

            return returnValue;
        }

        public static string FindString(Entity entity, string attributeToFind)
        {
            return entity.Attributes.Contains(attributeToFind)
                       ? entity.Attributes[attributeToFind].ToString()
                       : string.Empty;
        }

        public static string FindEntityReferenceName(Entity entity, string attributeToFind)
        {
            return entity.Attributes.Contains(attributeToFind)
                ? ((EntityReference)entity.Attributes[attributeToFind]).Name
                : string.Empty;
        }

        public static EntityReference FindEntityReference(Entity entity, string attributeToFind)
        {
            return entity.Attributes.Contains(attributeToFind)
                ? ((EntityReference)entity.Attributes[attributeToFind])
                : null;
        }

        public static Money FindMoney(Entity entity, string attributeToFind)
        {
            return entity.Attributes.Contains(attributeToFind)
                       ? (Money)entity.Attributes[attributeToFind]
                       : null;
        }

        public static bool FindTwoOptionsValue(Entity entity, string attributeToFind)
        {
            return entity.Attributes.Contains(attributeToFind)
                ? ((bool)entity.Attributes[attributeToFind])
                : false;
        }

        public static OptionSetValue FindOptionSet(Entity entity, string attributeToFind)
        {
            return entity.Attributes.Contains(attributeToFind)
                ? (OptionSetValue)entity.Attributes[attributeToFind]
                : null;
        }

        public static DateTime? FindDateTime(Entity entity, string attributeToFind)
        {
            return entity.Attributes.Contains(attributeToFind)
                ? (DateTime?)entity.Attributes[attributeToFind]
                : null;
        }
    }
}
