﻿using System.Collections.Generic; 

namespace Belpark.CRMLibrary
{
    /// <summary>
    /// Organisations within the Belpark Domain
    /// </summary>
    public class CRMOrgs
    {
        //public const string Bpc = "Bpc";
        //public const string International = "International";
        public const string Marketing = "Marketing";
        public const string Usb = "Usb";
        public const string Usbed = "Usbed";
    }
    


    /// <summary>
    /// Usb Organisation Entities and Attributes
    /// </summary>
    public class Usb_Entities
    {
        public const string contact = "contact";
        public const string usb_studentacademicrecord = "usb_studentacademicrecord";
        public const string usb_nationality = "usb_nationality";
        public const string usb_programme = "usb_programme";
        public const string usb_programmeoffering = "usb_programmeoffering";
    }

    public class Usb_Entity_Contact
    {
        public const string contactid = "contactid";
        public const string firstname = "firstname"; //First Name
        public const string lastname = "lastname"; //Last Name
        public const string mobilephone = "mobilephone"; //Mobile Phone
        public const string telephone1 = "telephone1"; //Business Phone
        public const string emailaddress2 = "emailaddress2"; //Email
        public const string usb_usnumber = "usb_usnumber"; //US Number
        public const string usb_titlelookup = "usb_titlelookup"; //Title
        public const string usb_initials = "usb_initials"; //Initials
        public const string governmentid = "governmentid"; //ID Number
        public const string usb_correspondencelanguagelookup = "usb_correspondencelanguagelookup"; //Language
        public const string address1_line1 = "address1_line1"; //Street 1
        public const string address1_line2 = "address1_line2"; //Street 2
        public const string address1_line3 = "address1_line3"; //Street 3
        public const string address1_postalcode = "address1_postalcode"; //ZIP/Postal Code
        public const string address1_city = "address1_city"; //City
        public const string address1_stateorprovince = "address1_stateorprovince"; //State/Province
        public const string usb_address1country_lookup = "usb_address1country_lookup"; //Country/Region
        public const string telephone2 = "telephone2"; //Home Phone
        public const string usb_nationalitylookup = "usb_nationalitylookup"; //Nationality       
    }
    
    public class Usb_Entity_Nationality
    {
        public const string usb_nationalityid = "usb_nationalityid";
        public const string usb_name = "usb_name";
        public const string usb_siscode = "usb_siscode";
    }

    public class Usb_Entity_SAR
    {
        public const string usb_studentacademicrecordid = "usb_studentacademicrecordid";
        public const string statuscode = "statuscode"; //Student Status
        public const string usb_contactlookup = "usb_contactlookup";
        public const string usb_programmeofferinglookup = "usb_programmeofferinglookup";
        public const string usb_mastersyear = "usb_mastersyear"; //Masters Year
        public const string usb_honoursyear = "usb_honoursyear"; //Honours Year
        public const string usb_phdyear = "usb_phdyear"; //PhD Year
        public const string usb_postgraduateyear = "usb_postgraduateyear"; //PostGrad Year
    }

    public class Usb_Entity_ProgrammeOffering
    {
        public const string usb_programmeofferingid = "usb_programmeofferingid";
        public const string usb_programmelookup = "usb_programmelookup";
        public const string usb_name = "usb_name"; //Offering Name
        public const string usb_offeringstartdate = "usb_offeringstartdate"; //Start Date

    }

    public class Usb_Entity_Programme
    {
        public const string usb_programmeid = "usb_programmeid";
        public const string usb_name = "usb_name";
    }
    
    public class Usb_OptionSet_SAR_StatusReason
    {
        //Active States
        public const int Applicant              = 1;
        public const int ApplicationCompleted   = 863480000;
        public const int InterviewRound1        = 864480001;
        public const int SelectionCommittee     = 864480002;
        public const int InterviewRound2        = 864480003;
        public const int Cancelled              = 864480004;
        public const int NotAdmitted            = 864480005;
        public const int WaitingList            = 864480006;
        public const int RequestAdmitted        = 864480015;
        public const int Admitted               = 864480007;
        public const int FailedAdmitted         = 864480016;
        public const int Postponed              = 864480008;
        public const int RequestNotAccepted     = 864480017;
        public const int NotAccepted            = 864480009;
        public const int FailedNotAccepted      = 864480018;
        public const int RequestAccepted        = 864480019;
        public const int Accepted               = 864480010;
        public const int FailedAccepted         = 864480020;
        public const int Registered             = 864480011;
        public const int Unsuccessful           = 864480012;
        public const int Discontinued           = 864480013;
        public const int Graduated              = 864480014;
        //Inactive States
        public const int Deactivated            = 2;

    }


    
    /// <summary>
    /// Usb-Ed Organisation Entities and Attributes
    /// </summary>
    public class UsbEd_Entities
    {
        public const string contact = "contact";
        public const string stb_enrollment = "stb_enrollment";
        public const string stb_shortcourse = "stb_shortcourse";
        public const string stb_shortcourseoffering = "stb_shortcourseoffering";
    }

    public class UsbEd_Entity_Contact
    {
        public const string contactid = "contactid";
        public const string firstname = "firstname"; //First Name
        public const string lastname = "lastname"; //Last Name
        public const string mobilephone = "mobilephone"; //Mobile Phone
        public const string telephone1 = "telephone1"; //Business Phone
        public const string emailaddress1 = "emailaddress1"; //Email
        public const string stb_usnumber = "stb_usnumber"; //Us Number
    }

    public class UsbEd_Entity_Enrollment
    {
        public const string stb_enrollmentid = "stb_enrollmentid";
        public const string statuscode = "statuscode"; //Student Status
        public const string stb_studentcontact_contactlookup = "stb_studentcontact_contactlookup";
        public const string stb_shortcourseofferinglookup = "stb_shortcourseofferinglookup";
    }

    public class UsbEd_Entity_ShortCourseOffering
    {
        public const string stb_shortcourseofferingid = "stb_shortcourseofferingid";
        public const string stb_shortcourselookup = "stb_shortcourselookup";
        public const string stb_offeringnameenglish = "stb_offeringnameenglish"; //Offering Name
        public const string stb_startdate = "stb_startdate"; //Start Date
    }

    public class UsbEd_Entity_ShortCourse
    {
        public const string stb_shortcourseid = "stb_shortcourseid";
        public const string stb_shortcoursename = "stb_shortcoursename";
    }

    //TODO: Check the names of these status Reasons
    public class UsbEd_OptionSet_Enrollment_StatusReason
    {
        //Active States
        public const int Applied        = 1;
        public const int Qualified      = 956390017;
        public const int Approved       = 956390018;
        public const int Admitted       = 956390001;
        public const int Waitlisted     = 956390003;
        public const int NotAdmitted    = 956390004;
        public const int Registered     = 956390006;
        public const int Passed         = 956390009;
        public const int Failed         = 956390012;
        public const int Cancelled      = 956390014;
        //Inactive States
        public const int Deactivated    = 2;
    }


    
    /// <summary>
    /// Marketing Organisation Entities and Attributes
    /// </summary>
    public class Marketing_Entities
    {
        public const string contact = "contact";
        public const string usb_nationality = "usb_nationality";
        public const string usb_offeringstatus = "usb_offeringstatus";
        public const string usb_usboffering = "usb_usboffering";
        public const string usb_usbedoffering = "usb_usbedoffering";
    }

    public class Marketing_Entity_Contact
    {
        public const string contactid = "contactid";
        public const string firstname = "firstname"; //First Name
        public const string lastname = "lastname"; //Last Name
        public const string mobilephone = "mobilephone"; //Mobile Phone
        public const string telephone1 = "telephone1"; //Business Phone
        public const string emailaddress1 = "emailaddress1"; //Email
        public const string usb_usnumber = "usb_usnumber"; //Us Number
        public const string usb_usbedalumnitwooptions = "usb_usbedalumnitwooptions"; //Usb Ed Alumni
        public const string usb_usbalumnitwooptions = "usb_usbalumnitwooptions"; //Usb Alumni
        public const string salutation = "salutation"; //Title
        public const string usb_initials = "usb_initials"; //Initials
        public const string governmentid = "governmentid"; //ID Number
        public const string usb_languageoptionset = "usb_languageoptionset"; //Language
        public const string address1_line1 = "address1_line1"; //Street 1
        public const string address1_line2 = "address1_line2"; //Street 2
        public const string address1_line3 = "address1_line3"; //Street 3
        public const string address1_postalcode = "address1_postalcode"; //ZIP/Postal Code
        public const string address1_city = "address1_city"; //City
        public const string address1_stateorprovince = "address1_stateorprovince"; //State/Province
        public const string address1_country = "address1_country"; //Country/Region
        public const string telephone2 = "telephone2"; //Home Phone
        public const string usb_nationalitylookup = "usb_nationalitylookup"; //Nationality
        public const string usb_employer = "usb_employer"; //Employer
        public const string department = "department"; //Department
        public const string jobtitle = "jobtitle"; //Job Title
        public const string usb_industryoptionset = "usb_industryoptionset"; //Industry
        public const string usb_workareaoptionset = "usb_workareaoptionset"; //Work Area
    }

    public class Marketing_Entity_Nationality
    {
        public const string usb_nationalityid = "usb_nationalityid";
        public const string usb_nameenglish = "usb_nameenglish";
        public const string usb_siscode = "usb_siscode";
    }

    public class Marketing_Entity_OfferingStatus
    {
        public const string usb_offeringstatusid = "usb_offeringstatusid";
        public const string usb_name = "usb_name";
        public const string usb_contactlookup = "usb_contactlookup";
        public const string usb_honoursyear = "usb_honoursyear"; //Honours Year
        public const string usb_mastersyear = "usb_mastersyear"; //Masters Year
        public const string usb_phdyear = "usb_phdyear"; //PhD Year
        public const string usb_postgraduateyear = "usb_postgraduateyear"; //Post Grad Year
        public const string usb_usbedofferinglookup = "usb_usbedofferinglookup";
        public const string usb_usbofferinglookup = "usb_usbofferinglookup";
        public const string usb_usbedstudentstatusoptionset = "usb_usbedstudentstatusoptionset"; //Usb Ed Student Status
        public const string usb_usbstudentstatusoptionset = "usb_usbstudentstatusoptionset"; //Usb Student Status
        public const string statuscode = "statuscode";
        public const string statecode = "statecode";
    }

    public class Marketing_Entity_UsbOffering
    {
        public const string usb_usbofferingid = "usb_usbofferingid";
        public const string usb_sourceguid = "usb_sourceguid";
        public const string usb_offeringname = "usb_offeringname"; //Offering Name
        public const string usb_startdate = "usb_startdate"; //Start Date
        public const string statuscode = "statuscode";
        public const string statecode = "statecode";
    }

    public class Marketing_Entity_UsbEdOffering
    {
        public const string usb_usbedofferingid = "usb_usbedofferingid";
        public const string usb_sourceguid = "usb_sourceguid";
        public const string usb_offeringname = "usb_offeringname"; //Offering Name
        public const string usb_startdate = "usb_startdate"; //Start Date
        public const string statuscode = "statuscode";
        public const string statecode = "statecode";
    }
    
    public class Marketing_OptionSet_Contact_Industry
    {
        public const int AgricultureFoodAndBeverages            = 1;
        public const int DefenceAndSecurityServices             = 2;
        public const int EducationAndDevelopment                = 3;
        public const int ConsultancyServices                    = 4;
        public const int HealthAndWellness                      = 5;
        public const int FinancialServicesAndInsurance          = 6;
        public const int RetailAndTrade                         = 7;
        public const int TransportAndLogistics                  = 8;
        public const int HospitalitySportAndRecreation          = 9;
        public const int EngineeringAndConstruction             = 10;
        public const int Property                               = 11;
        public const int MiningAndEnergy                        = 12;
        public const int ElectronicsAndICT                      = 13;
        public const int CommunicationAndMarketing              = 14;
        public const int PublicSectorAndNGO                     = 15;
        public const int Other                                  = 16;
        public const int BusinessManagementAndEntrepreneurship  = 17;
        public const int ReligiousSocialAndPoliticalStudies     = 18;
        public const int VeterinaryAndAnimalScience             = 19;
        public const int LawAndHumanRights                      = 20;
    }

    public class Marketing_OptionSet_Contact_Language
    {
        public const int English    = 1;
        public const int Afrikaans  = 2;
    }

    public class Marketing_OptionSet_Contact_WorkArea
    {
        public const int Finance                = 1;
        public const int Management             = 2;
        public const int HumanResource          = 3;
        public const int IT                     = 4;
        public const int Marketing              = 5;
        public const int Production             = 6;
        public const int Sales                  = 7;
        public const int Administration         = 8;
        public const int StrategyAndPlanning    = 9;
        public const int Engineering            = 10;
        public const int Law                    = 11;
        public const int Other                  = 12;
    }

    public class Marketing_OptionSet_OfferingStatus_UsbEdStudentStatus
    {
        public const int Applied        = 1;
        public const int Qualified      = 956390017;
        public const int Approved       = 956390018;
        public const int Admitted       = 956390001;
        public const int Waitlisted     = 956390003;
        public const int NotAdmitted    = 956390004;
        public const int Registered     = 956390006;
        public const int Passed         = 956390009;
        public const int Failed         = 956390012;
        public const int Cancelled      = 956390014;
        public const int Deactivated    = 2;
        public const int Undefined      = 99;
    }

    public class Marketing_OptionSet_OfferingStatus_UsbStudentStatus
    {
        public const int Applicant              = 1;
        public const int Deactivated            = 2;  
        public const int ApplicationCompleted   = 3;
        public const int InterviewRound1        = 4;
        public const int SelectionCommittee     = 5;
        public const int InterviewRound2        = 6;
        public const int Admitted               = 7;
        public const int NotAdmitted            = 8;
        public const int Waitlisted             = 9;
        public const int Postponed              = 10;
        public const int Accepted               = 11;
        public const int NotAccepted            = 12;
        public const int Registered             = 13;
        public const int Graduated              = 14;
        public const int Unsuccessfull          = 15;
        public const int Discontinued           = 16;
        public const int Cancelled              = 17;
        public const int Undefined              = 99;
    }
}
