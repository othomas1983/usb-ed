﻿#region *** Using Directives ***
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using CrmWsdl;
#endregion

public partial class NewsAndEvents_EventItem : System.Web.UI.Page
{
    #region *** Page_Load ***
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack && !Page.IsCallback)
        {
            StartCRMService();
        }
    }
    #endregion
    
    #region *** StartCRMService ***
    public void StartCRMService()
    {

        //Set no-cache
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache);

        //Get QueryString value
        string strEventItem = Request.QueryString["EventItem"];

        //Write iID to hidden field
        //strTheValue.te = strTheValue;
        iIDLnkBtn.CommandArgument = strEventItem;
        
        
        CrmWsdl.CrmService _car = new CrmWsdl.CrmService();
        CrmAuthenticationToken token = new CrmAuthenticationToken();
        token.AuthenticationType = 0;
        token.OrganizationName = "US-BPC";

        _car.CrmAuthenticationTokenValue = token;
        _car.PreAuthenticate = true;
        _car.Credentials = System.Net.CredentialCache.DefaultCredentials;

        //Retrieve all active (launched) campaigns
        QueryByAttribute nxtquery = new QueryByAttribute();
        nxtquery.ColumnSet = new AllColumns();
        nxtquery.EntityName = EntityName.campaign.ToString();
        nxtquery.Attributes = new string[] { "campaignid" };
        nxtquery.Values = new String[] { strEventItem };

        BusinessEntityCollection retrieved = _car.RetrieveMultiple(nxtquery);

        campaign EventDetails = (campaign)retrieved.BusinessEntities[0];

        if (EventDetails.name != null)
        {
            lblEventName.Text = EventDetails.name;
        }

        if (EventDetails.msa_eventdetails != null)
        {
            tblDetails.Visible = true;
            lblDetailsDisplay.Text = EventDetails.msa_eventdetails.ToString();
        }

        if (EventDetails.msa_startdatetime != null)
        {
            tblDate.Visible = true;

            DateTime dt = Convert.ToDateTime(EventDetails.msa_startdatetime.Value.ToString());

            lblDateDisplay.Text = dt.ToString("dddd, dd MMMM yyyy");
        }

        if (EventDetails.msa_location != null)
        {
            tblVenue.Visible = true;
            lblVenueDisplay.Text = EventDetails.msa_location.ToString();
        }

        if (EventDetails.msa_startdatetime != null)
        {
            tblTime.Visible = true;

            DateTime tm = Convert.ToDateTime(EventDetails.msa_startdatetime.Value.ToString());
            lblTimesDisplay.Text = tm.ToString("HH:mm");
        }

        if (EventDetails.msa_costofadmission != null)
        {
            tblCost.Visible = true;

            if (EventDetails.lt_paymentdetails.Value != false)
            {
                lblCostDisplay.Text = "R " + EventDetails.msa_costofadmission.ToString();
            }
            else
            {
                lblCostDisplay.Text = EventDetails.msa_costofadmission.ToString();
            }
        }

        if (EventDetails.msa_eventcontact != null)
        {
            tblContactPerson.Visible = true;
            lblContactPersonDisplay.Text = EventDetails.msa_eventcontact.ToString();
        }

        if (EventDetails.lt_phonenumber != null)
        {
            tblPhoneNo.Visible = false;
            lblPhoneNoDisplay.Text = EventDetails.lt_phonenumber.ToString();
        }

        if (EventDetails.lt_emailaddress != null)
        {
            tblEmailAddress.Visible = true;

            string ContactEmail = EventDetails.lt_emailaddress.ToString();

            lblEmailAddressDisplay.Text = "<a href='mailto:" + ContactEmail + "'>" + ContactEmail + "</a>";
        }
        

        //Set visibility of Online Booking Form
        if (EventDetails.lt_onlinebookingform != null)
        {
            if (EventDetails.lt_onlinebookingform.Value == true)
            {
                tblOnlineBookingForm.Visible = true;
            }
            else
            {
                tblOnlineBookingForm.Visible = false;
            }
        }
        else
        {
            tblOnlineBookingForm.Visible = false;
        }    

    }
    #endregion

    #region *** LinkBtniID_Command ***
    public void LinkBtniID_Command(Object sender, CommandEventArgs e)
    {
        string strTheValue = e.CommandArgument.ToString();
        string strURL = "BookingForm.aspx?iID=" + strTheValue;
        Server.Transfer(strURL, true);
    }
    #endregion   

    #region *** ReturnToListLnkBt_Command ***
    public void ReturnToListLnkBt_Command(Object sender, CommandEventArgs e)
    {        
        string strURL = "EventsCalendar.aspx";
        Server.Transfer(strURL, true);
    }
    #endregion 
  
    #region *** RSSFeedLinkBtn_Click ***
    protected void RSSFeedLinkBtn_Click(object sender, EventArgs e)
    {
        string strURL;
        strURL = "USBEventsFeed.aspx";

        Response.Redirect(strURL);
    }
    #endregion

}
    
      


    

