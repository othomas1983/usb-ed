﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TestBookingForm.aspx.cs" Inherits="NewsAndEvents_TestBookingForm" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1"%>

<!--
<%@ Register TagPrefix="IncludeFile" TagName="Header" Src="~/Common/UserControls/HeaderUserControl.ascx" %>
<%@ Register TagPrefix="IncludeFile" TagName="Footer" Src="~/Common/UserControls/FooterUserControl.ascx"  %>
-->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>USB | News And Events | Booking Form</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href="//usbevents.belpark.sun.ac.za/Common/StyleSheets/sub-style.css" rel="stylesheet" type="text/css" media="screen" />

    <script type="text/javascript" src="//usbevents.belpark.sun.ac.za/Common/Scripts/jquery-1.4.4.min.js"></script>
	
	<script language="javascript" type="text/javascript">
	
	    function checkTab(theTab) {
	        f = document.forms['frmBookingForm'];

	        if (f.BulkRegistrationCheckBox.checked == true) {
	            document.getElementById("attendees_table").style.display = "";
	        } else {
	            document.getElementById("attendees_table").style.display = "none";
	        }
	    }
	
	    function RequiredDetails(source, args) {

	        var chk = document.getElementById('BulkRegistrationCheckBox');
	        var Name = document.getElementById('NumberOfAttendessInput');

	        if (chk.checked) {

	            if (Name.value.length <= 0) {
	                args.IsValid = false;
	            }
	            else {
	                args.IsValid = true;
	            }
	        }
	        return;
	    }

    </script>        

    <script language="javascript" type="text/javascript">

        $(document).ready(function () {

            $("input[name$='rblPartnerDetails']").click(function () {

                var radio_value = $(this).val();

                if (radio_value == '1') {
                    $("#FoodTablePartner").show();
                }
                else if (radio_value == '0') {
                    $("#FoodTablePartner").hide();
                }
            });

            $("#FoodTablePartner").hide();

        });

        
        $(document).ready(function () {

            $("#ddlNumberOfPeopleDetails").change(function () {

                var selectedValue = $(this).val();

                if (selectedValue == '2') {
                    $("#FoodTablePartner").show();
                }
                else if (selectedValue !== '2') {
                    $("#FoodTablePartner").hide();
                }
            });

            $("#FoodTablePartner").hide();

        });

    </script>

    <%--<script language="javascript" type="text/javascript">

        function openNewWin(url) { 
            var x = window.open(url, 'mynewwin', 'width=600,height=600,toolbar=1');
            x.focus();
        }

    </script>--%>
</head>

<script type="text/javascript" src="//usbevents.belpark.sun.ac.za/Common/Scripts/milonic_src.js"></script> 
<script type="text/javascript" src="//usbevents.belpark.sun.ac.za/Common/Scripts/mmenudom.js"></script>

<body>

<%--<div id="outer">

<div id="container">
	<div id="header">		
        <!-- HeaderUserControl goes here -->
	    <IncludeFile:Header ID="Header1" runat="server" />	
	</div>
	<!--End Header-->
	
	<div id="topbar-newsandevents">
	    &nbsp;
	</div>
	<!--End Topbar-->

	<div id="wrapper">	
		<div id="leftnav">		
			<script type="text/javascript" src="../Common/Scripts/usb_left_menu_newsandevents.js"></script>
		</div>
		<!--End Leftnav-->
		
		<div id="right">			
			<div class="rightbox">				
				<img src="../Common/Images/AdministeredImages/NoneCrossLinked/6_1_NewsAndEvents.jpg" width="180" height="287" onerror="this.onerror=null;this.src='../Common/Images/AdministeredImages/NoneCrossLinked/6_1_NewsAndEvents.jpg'"  alt="" />
			</div>		     
			<!--End Rightbox-->
		</div>
		<!--End Right-->

		<div id="content">--%>
		    <form id="frmBookingForm" runat="server">
		      <asp:ScriptManager ID="MyScriptManager" runat="server" EnablePartialRendering="true"></asp:ScriptManager>			        
                  
                <asp:PlaceHolder ID="plhBookingForm" runat="server" Visible="False">
                  <asp:Label ID="leadGUID" runat="server" Visible="False"></asp:Label>
                  
                  <asp:Table ID="BookingTable" runat="server" Width="100%">
                    <asp:TableRow>
                            <asp:TableCell ColumnSpan="2">
                                &nbsp;
                            </asp:TableCell>                    
                        </asp:TableRow>
                    <asp:TableRow ID="TableRow1" runat="server">
                        <asp:TableCell ColumnSpan="2"><asp:Label ID="lblBookingForm" CssClass="mainheading" runat="server" Text="BOOKING FORM: "></asp:Label><asp:Label ID="EventName" CssClass="mainheading" runat="server"></asp:Label></asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell ColumnSpan="2">&nbsp;</asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell ColumnSpan="2">
                        
                            <asp:Table ID="ContactTable" Visible="false" runat="server">
                                <asp:TableRow>
                                    <asp:TableCell ColumnSpan="2"><asp:Label ID="ContactDetails" CssClass="subheading" runat="server" Text="CONTACT DETAILS"></asp:Label></asp:TableCell>               
                                </asp:TableRow>                                
                                <asp:TableRow>
                                    <asp:TableCell Width="50%">
                                        <asp:Label ID="Surname" CssClass="bodycopy" runat="server" Text="Surname:"></asp:Label> 
                                        <asp:Label ID="lblSurnameRequired" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell Width="50%">
                                        <asp:TextBox ID="SurnameInput" Columns="35" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvSurname" runat="server" ControlToValidate="SurnameInput" ErrorMessage="- Please insert your surname" Display="None">*</asp:RequiredFieldValidator>                                
                                    </asp:TableCell>                            
                                </asp:TableRow>                               
                                <asp:TableRow>
                                    <asp:TableCell Width="50%">
                                        <asp:Label ID="Name" CssClass="bodycopy" runat="server" Text="Name:"></asp:Label>
                                        <asp:Label ID="lblNameRequired" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell Width="50%">
                                        <asp:TextBox ID="NameInput" Columns="35" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="NameInput" ErrorMessage="- Please insert your name" Display="None">*</asp:RequiredFieldValidator>                                
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Width="50%">
                                        <asp:Label ID="lblAssociation" runat="server" CssClass="bodycopy" Text="Association with USB:"></asp:Label>
                                        <asp:Label ID="lblAssociationRequired" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell Width="50%"> 
                                        <asp:DropDownList ID="ddlAssociationDetails" runat="server" CssClass="bodycopy">
                                            <asp:ListItem Value="" Text="- Make a selection -"></asp:ListItem> 
                                            <asp:ListItem Value="1" Text="Alumnus"></asp:ListItem> 
                                            <asp:ListItem Value="2" Text="Student"></asp:ListItem>                                             
                                            <asp:ListItem Value="3" Text="Other"></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rfvAssociation" runat="server" ControlToValidate="ddlAssociationDetails" ErrorMessage="- Please select your association" Display="None">*</asp:RequiredFieldValidator>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Width="50%"><asp:Label ID="Company" CssClass="bodycopy" runat="server" Text="Employer/Organisation:"></asp:Label></asp:TableCell>
                                    <asp:TableCell Width="50%"><asp:TextBox ID="CompanyInput" Columns="35" runat="server"></asp:TextBox></asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Width="50%"><asp:Label ID="JobTitle" CssClass="bodycopy" runat="server" Text="Job Title:"></asp:Label></asp:TableCell>
                                    <asp:TableCell Width="50%"><asp:TextBox ID="JobTitleInput" Columns="35" runat="server"></asp:TextBox></asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Width="50%"><asp:Label ID="BusinessPhone" CssClass="bodycopy" runat="server" Text="Business Phone:"></asp:Label></asp:TableCell>
                                    <asp:TableCell Width="50%"><asp:TextBox ID="BusinessPhoneInput" Columns="35" runat="server"></asp:TextBox></asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Width="50%">
                                        <asp:Label ID="MobileNo" CssClass="bodycopy" runat="server" Text="Mobile Phone:"></asp:Label>
                                        <asp:Label ID="lblMobileNoRequired" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell Width="50%">
                                        <asp:TextBox ID="MobileNoInput" Columns="35" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvMobileNo" runat="server" ControlToValidate="MobileNoInput" ErrorMessage="- Please insert your mobile phone number" Display="None">*</asp:RequiredFieldValidator>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Width="50%">
                                        <asp:Label ID="Email" CssClass="bodycopy" runat="server" Text="E-mail:"></asp:Label>
                                        <asp:Label ID="lblEmailRequired" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell Width="50%">
                                        <asp:TextBox ID="EmailInput" Columns="35" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvUserEmail" runat="server" ControlToValidate="EmailInput" ErrorMessage="- Please insert your e-mail address" Display="None">*</asp:RequiredFieldValidator>                                        
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Width="50%"><asp:Label ID="TestIDNo" CssClass="bodycopy" runat="server" Text="ID No:"></asp:Label></asp:TableCell>
                                    <asp:TableCell Width="50%"><asp:TextBox ID="TestIDNoOutput" Columns="35" runat="server"></asp:TextBox>
                                        <%--<asp:RegularExpressionValidator ID="regExValTestIDNoOutput" runat="server" ControlToValidate="TestIDNoOutput"
                                        ErrorMessage="- ID Number must be 13 (numeric) characters" ValidationExpression="\d{13}">*</asp:RegularExpressionValidator>--%>
                                    </asp:TableCell>                                        
                                </asp:TableRow>                                               
                                <asp:TableRow>
                                    <asp:TableCell Width="50%"><asp:Label ID="lblForeignIdNo" CssClass="bodycopy" runat="server" Text="Foreign ID No:"></asp:Label></asp:TableCell>
                                    <asp:TableCell Width="50%"><asp:TextBox ID="tbForeignIdNoInput" Columns="35" runat="server"></asp:TextBox></asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Width="50%"><asp:Label ID="PassportNo" CssClass="bodycopy" runat="server" Text="Passport No:"></asp:Label></asp:TableCell>
                                    <asp:TableCell Width="50%"><asp:TextBox ID="PassportNoInput" Columns="35" runat="server"></asp:TextBox></asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell ColumnSpan="2">&nbsp;</asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            
                            <asp:Table ID="PartnerFunctionTable" Visible="false" runat="server" Width="100%">
                                <asp:TableRow>
                                    <asp:TableCell Width="50%"><asp:Label ID="PartnerDetails" runat="server" CssClass="bodycopy" Text="Attending with partner:"></asp:Label> </asp:TableCell>
                                    <asp:TableCell Width="50%"> 
                                        <asp:RadioButtonList ID="rblPartnerDetails" RepeatDirection="Horizontal" runat="server">
                                            <asp:ListItem Value="0" Text="No" Selected="True"></asp:ListItem>
                                            <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </asp:TableCell>
                                </asp:TableRow>                            
                                <%--<asp:TableRow>
                                    <asp:TableCell Width="50%"><asp:Label ID="ClassGroupDetails" runat="server" CssClass="bodycopy" Text="Group:"></asp:Label> </asp:TableCell>
                                    <asp:TableCell Width="50%">                                         
                                        <asp:DropDownList ID="ddlClassGroupDetails" runat="server" CssClass="bodycopy">
                                            <asp:ListItem Value="" Text="- Make a selection -"></asp:ListItem> 
                                            <asp:ListItem Value="1" Text="Full-Time"></asp:ListItem> 
                                            <asp:ListItem Value="2" Text="Part-Time"></asp:ListItem>                                             
                                            <asp:ListItem Value="3" Text="Management"></asp:ListItem>
                                            <asp:ListItem Value="4" Text="Academic"></asp:ListItem>
                                            <asp:ListItem Value="5" Text="Administrative"></asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                </asp:TableRow> --%>
                                <asp:TableRow>
                                    <asp:TableCell ColumnSpan="2">&nbsp;</asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            
                            <asp:Table ID="PeopleAttendingTable" Visible="false" runat="server" Width="100%">
                                <asp:TableRow>
                                    <asp:TableCell ColumnSpan="2"><asp:Label ID="lblTableDetails" CssClass="subheading" runat="server" Text="TABLE DETAILS"></asp:Label></asp:TableCell>               
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Width="50%"><asp:Label ID="NumberOfPeopleDetails" runat="server" CssClass="bodycopy" Text="Number of seats:"></asp:Label> </asp:TableCell>
                                    <asp:TableCell Width="50%"> 
                                        <asp:DropDownList ID="ddlNumberOfPeopleDetails" runat="server">
                                            <asp:ListItem Value="" Text="- Make a selection -"></asp:ListItem>
                                        </asp:DropDownList>
                                    </asp:TableCell>
                                </asp:TableRow>                                                            
                                <asp:TableRow>
                                    <asp:TableCell ColumnSpan="2">&nbsp;</asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            
                            <asp:Table ID="AddressTable" Visible="false" runat="server">             
                                <asp:TableRow>
                                    <asp:TableCell ColumnSpan="2"><asp:Label ID="AddressDetails" CssClass="subheading" runat="server" Text="ADDRESS DETAILS"></asp:Label></asp:TableCell>                
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Width="50%"><asp:Label ID="HomeAddress" CssClass="bodycopy" runat="server" Text="Home Address:"></asp:Label></asp:TableCell>
                                    <asp:TableCell Width="50%"><asp:TextBox ID="HomeAdressStreet1Input" Columns="35" runat="server"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;</asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Width="50%">&nbsp;</asp:TableCell>
                                    <asp:TableCell Width="50%"><asp:TextBox ID="HomeAddressSteet2Input" Columns="35" runat="server"></asp:TextBox></asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Width="50%">&nbsp;</asp:TableCell>
                                    <asp:TableCell Width="50%"><asp:TextBox ID="HomeAddressStreet3Input" Columns="35" runat="server"></asp:TextBox></asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Width="50%"><asp:Label ID="City" CssClass="bodycopy" runat="server" Text="City:"></asp:Label></asp:TableCell>
                                    <asp:TableCell Width="50%"><asp:TextBox ID="HomeAddressCityInput" Columns="35" runat="server"></asp:TextBox></asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Width="50%"><asp:Label ID="PostalCode" CssClass="bodycopy" runat="server" Text="Postal Code:"></asp:Label></asp:TableCell>
                                    <asp:TableCell Width="50%"><asp:TextBox ID="HomeAddressPostalCodeInput" Columns="35" runat="server"></asp:TextBox></asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Width="50%"><asp:Label ID="Country" CssClass="bodycopy" runat="server" Text="Country:"></asp:Label></asp:TableCell>
                                    <asp:TableCell Width="50%"><asp:TextBox ID="HomeAddressCountryInput" Columns="35" runat="server"></asp:TextBox></asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell ColumnSpan="2">&nbsp;</asp:TableCell>                
                                </asp:TableRow>
                            </asp:Table>

                            <asp:Table ID="BookOrderTable" Visible="false" runat="server">                        
                                <asp:TableRow>
                                    <asp:TableCell ColumnSpan="2"><asp:Label ID="Label1" CssClass="subheading" runat="server" Text="ORDER DETAILS"></asp:Label></asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Width="50%"><asp:Label ID="NumberOfCopies" runat="server" CssClass="bodycopy" Text="Number of copies:"></asp:Label></asp:TableCell>
                                    <asp:TableCell Width="50%">                                        
                                        <asp:DropDownList ID="ddlNumberOfCopies" runat="server">
                                            <asp:ListItem Value="" Text="- Make a selection -"></asp:ListItem>                                            
                                            <asp:ListItem Value="1" Text="1"></asp:ListItem> 
                                            <asp:ListItem Value="2" Text="2"></asp:ListItem> 
                                            <asp:ListItem Value="3" Text="3"></asp:ListItem> 
                                            <asp:ListItem Value="4" Text="4"></asp:ListItem> 
                                            <asp:ListItem Value="5" Text="5"></asp:ListItem>
                                            <asp:ListItem Value="6" Text="6"></asp:ListItem>
                                            <asp:ListItem Value="7" Text="7"></asp:ListItem>
                                            <asp:ListItem Value="8" Text="8"></asp:ListItem>
                                            <asp:ListItem Value="9" Text="9"></asp:ListItem>
                                            <asp:ListItem Value="10" Text="10"></asp:ListItem>
                                        </asp:DropDownList>                                    
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Width="50%">&nbsp;</asp:TableCell>
                                    <asp:TableCell Width="50%"> 
                                        <asp:RadioButtonList ID="rblPostageOrCollect" RepeatDirection="Vertical" runat="server">
                                            <asp:ListItem Value="1" Text="Packaging and postage: R30 (if we must send the book)" Selected="True"></asp:ListItem>
                                            <asp:ListItem Value="2" Text="Collect book from USB (no packaging and postage)"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell ColumnSpan="2">&nbsp;</asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                                                
                            <asp:Table ID="BulkTable" runat="server">                      
                                <asp:TableRow>
                                    <asp:TableCell ColumnSpan="2"><asp:Label ID="BulkRegistration" CssClass="subheading" runat="server" Text="GROUP/BULK REGISTRATION"></asp:Label></asp:TableCell>                
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell><asp:Label ID="BulkRegistrationQuestion" CssClass="bodycopy" runat="server" Text="Please indicate if this is a group/bulk registration"></asp:Label></asp:TableCell>
                                    <asp:TableCell><asp:CheckBox ID="BulkRegistrationCheckBox" runat="server" /></asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell ColumnSpan="2">
                                        <asp:Table ID="attendees_table" runat="server">
                                            <asp:TableRow>
                                                <asp:TableCell Width="50%"><asp:Label ID="NumberOfAttendees" CssClass="bodycopy" runat="server" Text="Number of Attendees:"></asp:Label></asp:TableCell>
                                                <asp:TableCell Width="50%">
                                                    <asp:TextBox ID="NumberOfAttendessInput" Columns="35" runat="server"></asp:TextBox>
                                                    <asp:CustomValidator ID="CustomValidator1" runat="server" 
                                                        ErrorMessage="- Enter number of attendees" ControlToValidate="NumberOfAttendessInput" ClientValidationFunction="RequiredDetails" ValidateEmptyText="true">*</asp:CustomValidator>
                                                    <asp:CompareValidator ID="CompValAttendees" runat="server" ControlToValidate="NumberOfAttendessInput" 
                                                        ErrorMessage="- Enter numeric value" Operator="DataTypeCheck" Type="Integer" Display="Dynamic">*</asp:CompareValidator>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell Width="50%"><asp:Label ID="BulkCompanyName" CssClass="bodycopy" runat="server" Text="Company name:"></asp:Label></asp:TableCell>
                                                <asp:TableCell Width="50%"><asp:TextBox ID="BulkCompanyNameInput" Columns="35" runat="server"></asp:TextBox></asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </asp:TableCell>                
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell ColumnSpan="2">&nbsp;</asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <asp:Table ID="FoodTable" Visible="false" runat="server" Width="100%">                        
                                        <asp:TableRow>
                                            <asp:TableCell ColumnSpan="2"><asp:Label ID="lblFoodDetails" CssClass="subheading" runat="server" Text="FOOD DETAILS"></asp:Label></asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell><asp:Label ID="FoodDetails" CssClass="bodycopy" runat="server" Text="Please indicate special dietary requirements:"></asp:Label></asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Right">
                                                <asp:DropDownList ID="ddlFoodDetails" runat="server" CssClass="bodycopy">
                                                    <asp:ListItem Value="" Text="- Make a selection -"></asp:ListItem> 
                                                    <asp:ListItem Value="6" Text="None"></asp:ListItem> 
                                                    <asp:ListItem Value="1" Text="Diabetic"></asp:ListItem> 
                                                    <asp:ListItem Value="2" Text="Halaal"></asp:ListItem> 
                                                    <asp:ListItem Value="3" Text="Kosher"></asp:ListItem> 
                                                    <asp:ListItem Value="4" Text="Vegan"></asp:ListItem> 
                                                    <asp:ListItem Value="5" Text="Vegetarian"></asp:ListItem>
                                                    <asp:ListItem Value="7" Text="Other"></asp:ListItem>
                                                </asp:DropDownList>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell ColumnSpan="2">
                                                <asp:TableCell ColumnSpan="2">&nbsp;</asp:TableCell>
                                            </asp:TableCell>
                                        </asp:TableRow>                                                                 
                                    </asp:Table>                    
                                    
                                    <asp:Table ID="FoodTablePartner" runat="server" Width="100%">
                                        <asp:TableRow>
                                            <asp:TableCell><asp:Label ID="FoodDetailsPartner" CssClass="bodycopy" runat="server" Text="Please indicate special dietary requirements - partner:"></asp:Label></asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Right">
                                                <asp:DropDownList ID="ddlFoodDetailsPartner" runat="server" CssClass="bodycopy">
                                                    <asp:ListItem Value="" Text="- Make a selection -"></asp:ListItem> 
                                                    <asp:ListItem Value="6" Text="None"></asp:ListItem> 
                                                    <asp:ListItem Value="1" Text="Diabetic"></asp:ListItem> 
                                                    <asp:ListItem Value="2" Text="Halaal"></asp:ListItem> 
                                                    <asp:ListItem Value="3" Text="Kosher"></asp:ListItem> 
                                                    <asp:ListItem Value="4" Text="Vegan"></asp:ListItem> 
                                                    <asp:ListItem Value="5" Text="Vegetarian"></asp:ListItem>
                                                    <asp:ListItem Value="7" Text="Other"></asp:ListItem>
                                                </asp:DropDownList>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell ColumnSpan="2">&nbsp;</asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>

                                    <asp:Table ID="FoodTableAdditionalInfo" Visible="false" runat="server" Width="100%">
                                        <asp:TableFooterRow>
                                            <asp:TableCell VerticalAlign="Top"><asp:Label ID="FoodDetailsAdditional" CssClass="bodycopy" runat="server" Text="Additional dietary information:"></asp:Label></asp:TableCell>
                                            <asp:TableCell HorizontalAlign="Right">                                                
                                                <asp:TextBox ID="tbFoodDetailsAdditional" runat="server" TextMode="Multiline" Wrap="False" Columns="26" Rows="5" style="resize: none;"></asp:TextBox>
                                            </asp:TableCell>
                                        </asp:TableFooterRow>                                        
                                    </asp:Table>
                                </ContentTemplate>
                                <Triggers>                                    
                                    <asp:AsyncPostBackTrigger ControlID="rblPartnerDetails" />
                                </Triggers>
                            </asp:UpdatePanel>     
                            
                            <%--Cocktail function--%>
                            <asp:Table ID="tblCocktailFunction" runat="server" Width="100%" Visible="false">
                                <asp:TableRow>
                                    <asp:TableCell><asp:Label ID="lblCocktail" runat="server" CssClass="bodycopy" Text="Please tick the box if you intend to attend the Cocktail Function:"></asp:Label> </asp:TableCell>
                                    <asp:TableCell><asp:CheckBox ID="CocktailCheckBox" runat="server" /></asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell ColumnSpan="2">&nbsp;</asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        
                            
                            <asp:Table ID="InfoSessionSHLTable" Visible="false" runat="server">                        
                                <asp:TableRow>
                                    <asp:TableCell ColumnSpan="2"><asp:Label ID="lblInfoSessionSHL" CssClass="subheading" runat="server" Text="INFORMATION SESSION / SHL"></asp:Label></asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell><asp:Label ID="lblInfoSession" runat="server" CssClass="bodycopy" Text="Attending Information Session:"></asp:Label> </asp:TableCell>
                                    <asp:TableCell><asp:CheckBox ID="InfoSessionCheckBox" runat="server" /></asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell><asp:Label ID="lblSHL" runat="server" CssClass="bodycopy" Text="Attending SHL:"></asp:Label> </asp:TableCell>
                                    <asp:TableCell><asp:CheckBox ID="SHLCheckBox" runat="server" /></asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell ColumnSpan="2">&nbsp;</asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                                                        
                             <asp:Table ID="BankingDetailsTable" Visible="false" runat="server">                        
                                <asp:TableRow>
                                    <asp:TableCell ColumnSpan="2"><asp:Label ID="lblBankingDetails" CssClass="subheading" runat="server" Text="BANKING DETAILS"></asp:Label></asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>&nbsp;</asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Label ID="lblName" runat="server" Text="University of Stellenbosch"></asp:Label><br />                                           
                                        <asp:Label ID="lblBank" runat="server" Text="Standard Bank: 073003069"></asp:Label><br />
                                        <asp:Label ID="lblCode" runat="server" Text="Branch code: 050610"></asp:Label>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell ColumnSpan="2">&nbsp;</asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            
                            <asp:Table ID="tblInvoiceRequired" Visible="false" runat="server">
                                <asp:TableRow>
                                    <asp:TableCell><asp:Label ID="lblInvoiceRequired" CssClass="subheading" runat="server" Text="INVOICE REQUIRED"></asp:Label></asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell><p><b>If you require an invoice</b> prior to payment, please download, complete this <a href="../Common/Other/Debtors_Code_Application.xls">form</a> along with a copy of your company letterhead, and return it to <%--<asp:Label ID="lblContactEmail" runat="server"></asp:Label>--%><a href="mailto:usbalumni@usb.ac.za">usbalumni@usb.ac.za</a>
                                    or fax it to 021 918 4468<%--<asp:Label ID="lblContactFax" runat="server"></asp:Label>--%>. An invoice will then be sent to you.</p></asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            
                            <asp:Table ID="InvoiceTable" Visible="false" runat="server">
                                <asp:TableRow>
                                    <asp:TableCell ColumnSpan="2"><asp:Label ID="lblInvoiceDetails" CssClass="subheading" runat="server" Text="INVOICE DETAILS"></asp:Label></asp:TableCell> 
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell ColumnSpan="2"><asp:Label ID="InvoicingDetailsStatement" CssClass="bodycopy" runat="server" Text="If we need to invoice your company please complete this section."></asp:Label></asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Width="50%"><asp:Label ID="InvoiceCompanyName" CssClass="bodycopy" runat="server" Text="Company name:"></asp:Label></asp:TableCell>
                                    <asp:TableCell Width="50%"><asp:TextBox ID="InvoiceCompanyNameInput" Columns="35" runat="server"></asp:TextBox></asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Width="50%"><asp:Label ID="InvoiceCompanyRegistrationNumber" CssClass="bodycopy" runat="server" Text="Company Registration Number:"></asp:Label></asp:TableCell>
                                    <asp:TableCell Width="50%"><asp:TextBox ID="InvoiceCompanyRegistrationNumberInput" Columns="35" runat="server"></asp:TextBox></asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Width="50%"><asp:Label ID="InvoiceCompanyVatNumber" CssClass="bodycopy" runat="server" Text="VAT Number:"></asp:Label></asp:TableCell>
                                    <asp:TableCell Width="50%"><asp:TextBox ID="InvoiceCompanyVatNumberInput" Columns="35" runat="server"></asp:TextBox></asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Width="50%"><asp:Label ID="CompanyAddress" CssClass="bodycopy" runat="server" Text="Company Address:"></asp:Label></asp:TableCell>
                                    <asp:TableCell Width="50%"><asp:TextBox ID="CompanyAddressStreet1Input" Columns="35" runat="server"></asp:TextBox></asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Width="50%">&nbsp;</asp:TableCell>
                                    <asp:TableCell Width="50%"><asp:TextBox ID="CompanyAddressStreet2Input" Columns="35" runat="server"></asp:TextBox></asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Width="50%">&nbsp;</asp:TableCell>
                                    <asp:TableCell Width="50%"><asp:TextBox ID="CompanyAddressStreet3Input" Columns="35" runat="server"></asp:TextBox></asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Width="50%"><asp:Label ID="CompanyAddressCity" CssClass="bodycopy" runat="server" Text="City:"></asp:Label></asp:TableCell>
                                    <asp:TableCell Width="50%"><asp:TextBox ID="CompanyAddressCityInput" Columns="35" runat="server"></asp:TextBox></asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Width="50%"><asp:Label ID="CompanyAddressPostalCode" CssClass="bodycopy" runat="server" Text="Postal Code:"></asp:Label></asp:TableCell>
                                    <asp:TableCell Width="50%"><asp:TextBox ID="CompanyAddressPostalCodeInput" Columns="35" runat="server"></asp:TextBox></asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell ColumnSpan="2">&nbsp;</asp:TableCell>            
                                </asp:TableRow>                                
                                <asp:TableRow>
                                    <asp:TableCell ColumnSpan="2">
                                        <asp:Label ID="RegistrationStatement" CssClass="bodycopy" runat="server" ForeColor="Red" Font-Bold="true" Text="* Please note:"></asp:Label><br />
                                        <asp:Label ID="lblRegistrationStatementCont" CssClass="bodycopy" runat="server" ForeColor="Red" Font-Bold="true" Text="&nbsp;&nbsp;&nbsp;Registration will only be confirmed on receipt of proof of payment."></asp:Label>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell ColumnSpan="2">&nbsp;</asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>                
                    </asp:TableRow>                    
                    <asp:TableRow>
                        <asp:TableCell ColumnSpan="2">&nbsp;</asp:TableCell>            
                    </asp:TableRow>
                    <%--<asp:TableRow>
                        <asp:TableCell ColumnSpan="2" HorizontalAlign="Center" BackColor="#e7d0d7"> <asp:CheckBox ID="cbCalendar" runat="server" /> <b>Send the Date Reminder to my Microsoft Outlook Calendar</b> </asp:TableCell>            
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell ColumnSpan="2">&nbsp;</asp:TableCell>            
                    </asp:TableRow>--%>
                    
                    <asp:TableRow>
                       <asp:TableCell><b>Verification code:</b></asp:TableCell>                       
                       <asp:TableCell><img src="//usbevents.belpark.sun.ac.za/Common/UserControls/Captcha.aspx" alt="Verification Code" /> </asp:TableCell>                        

                    </asp:TableRow>                      
                    <asp:TableRow>
                       <asp:TableCell><b>Enter the verification code:</b></asp:TableCell>
                       <asp:TableCell><asp:TextBox ID="VerifiedCaptchaCodeTB" runat="server" CssClass="input"></asp:TextBox> </asp:TableCell>                        
                    </asp:TableRow>                        
                    <asp:TableRow>
                        <asp:TableCell ColumnSpan="2"><asp:Label ID="CaptchaImageLbl" runat="server" Font-Names="Verdana" ForeColor="#891536" Font-Bold="true"></asp:Label></asp:TableCell>                      
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell ColumnSpan="2">&nbsp;</asp:TableCell>                      
                    </asp:TableRow>
                    
                    <asp:TableRow ID="TableRow2" runat="server">
                        <asp:TableCell ID="TableCell1" ColumnSpan="2" runat="server" HorizontalAlign="Center">
                            <asp:LinkButton runat="server" ID="ValidationHiddenBtn" style="display: none" />
                            <asp:ValidationSummary runat="server" ID="ValidationSummary1" ShowMessageBox="true" ValidationGroup="Group1" HeaderText="The Form could not be submitted for the following reasons:" />                                                           
                            <asp:Button ID="SubmitButtonUpdate" CssClass="button" runat="server" OnClick="SubmitButtonUpdate_Click" Text="Submit Form (U)" />    
							<asp:Label ID="text" CssClass="bodycopy" runat="server" Text="<br><br>Please note: Because of your association with the University of Stellenbosch Business School, you will receive communication from us."></asp:Label> 							
                        </asp:TableCell>                
                    </asp:TableRow>
					
                    <asp:TableRow>
                        <asp:TableCell ColumnSpan="2">&nbsp;</asp:TableCell>                    
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell ColumnSpan="2"><asp:Label ID="lblDisplay" runat="server"></asp:Label></asp:TableCell>                    
                    </asp:TableRow>
                    <asp:TableRow>
                            <asp:TableCell ColumnSpan="2"><asp:ValidationSummary ID="ValSum" CssClass="errorheading" runat="server" DisplayMode="List" /></asp:TableCell>                    
                        </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell ColumnSpan="2">&nbsp;</asp:TableCell>                    
                    </asp:TableRow>
                </asp:Table>
                
                <asp:Label ID="lblTest" runat="server" Visible="false"></asp:Label>
                <asp:Label ID="lbliID" runat="server" Visible="false"></asp:Label>
                
                </asp:PlaceHolder>
                
                <asp:PlaceHolder ID="plhPaymentForm" runat="server" Visible="false">
                    <asp:Table ID="tblPaymentTable" runat="server" Width="100%">                      
                        <asp:TableRow>
                            <asp:TableCell ColumnSpan="2">&nbsp;</asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell ColumnSpan="2"><asp:Label ID="lblPaymentDetails" CssClass="subheading" runat="server" Text="PAYMENT DETAILS:"></asp:Label></asp:TableCell>                
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell ColumnSpan="2">&nbsp;</asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>                            
                            <asp:TableCell ColumnSpan="2"><asp:Label ID="lblEventNamePayOutput" CssClass="bodycopy" Font-Bold="true" runat="server"></asp:Label> </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell ColumnSpan="2">&nbsp;</asp:TableCell>
                        </asp:TableRow>                
                        <asp:TableRow>
                            <asp:TableCell Width="30%"><asp:Label ID="lblEventCost" CssClass="bodycopy" runat="server" Text="Cost of Event:"></asp:Label></asp:TableCell>
                            <asp:TableCell Width="70%"><asp:Label ID="lblEventCostOutput" CssClass="subheading" runat="server"></asp:Label> </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell ColumnSpan="2">&nbsp;</asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell ColumnSpan="2" HorizontalAlign="Center"><asp:Button ID="btnCreditCardPayment" CssClass="button" runat="server" OnClick="btnCreditCardPayment_Click" OnClientClick="frmBookingForm.target ='_blank';" Text="Pay via Credit Card" />&nbsp;&nbsp;&nbsp;<asp:Button ID="btnPayLater" CssClass="button" runat="server" OnClick="btnPayLater_Click" Text="Pay via EFT / Direct Deposit" /></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell ColumnSpan="2">&nbsp;</asp:TableCell>
                        </asp:TableRow>                       
                        <asp:TableRow>
                            <asp:TableCell ColumnSpan="2">&nbsp;</asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:PlaceHolder>               
                                
                <asp:PlaceHolder ID="plhBookingConfirm" runat="server" Visible="False">
                    <asp:Table ID="tblBookingConfirm" runat="server">
                        <asp:TableRow>
                            <asp:TableCell>&nbsp;</asp:TableCell>                    
                        </asp:TableRow>
                       <%-- <asp:TableRow>
                            <asp:TableCell><asp:Label ID="lblSuccess" CssClass="subheading" runat="server" Text="Booking / Registration successful." ForeColor="red"></asp:Label></asp:TableCell>                    
                        </asp:TableRow>--%>
                       <%-- <asp:TableRow>
                            <asp:TableCell>&nbsp;</asp:TableCell>                    
                        </asp:TableRow>--%>
                       <%-- <asp:TableRow>
                            <asp:TableCell><asp:Label ID="lblMessage" CssClass="bodycopy" runat="server" Text="Thank you."></asp:Label></asp:TableCell>                    
                        </asp:TableRow>--%>
                      <%--  <asp:TableRow>
                            <asp:TableCell>&nbsp;</asp:TableCell>                    
                        </asp:TableRow>--%>
                        <asp:TableRow>
                            <asp:TableCell>&nbsp;<asp:LinkButton ID="LnkBtnEventsList" runat="server" PostBackUrl="EventsCalendar.aspx" Text="Return to Events List"><img src="../Common/Images/BookingSuccesful.jpg" alt="Success" /></asp:LinkButton></asp:TableCell>                    
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>&nbsp;</asp:TableCell>                    
                        </asp:TableRow>
                    </asp:Table>
                </asp:PlaceHolder>        
                
                <asp:PlaceHolder ID="plhPayLaterRef" runat="server" Visible="false">
                    <asp:Table ID="tblPayLaterRef" runat="server">
                        <asp:TableRow>
                            <asp:TableCell ColumnSpan="2">&nbsp;</asp:TableCell>                    
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell ColumnSpan="2"><asp:Label ID="lblPayLaterRefHeading" CssClass="mainheading" runat="server" Text="Booking / Registration:"></asp:Label></asp:TableCell>                    
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell ColumnSpan="2">&nbsp;</asp:TableCell>                    
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell ColumnSpan="2"><asp:Label ID="lblEventNameOutput" CssClass="subheading_bold" runat="server"></asp:Label></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell ColumnSpan="2">&nbsp;</asp:TableCell>                    
                        </asp:TableRow>
                        <asp:TableHeaderRow>
                            <asp:TableCell><asp:Label ID="lblUserName" runat="server" Text="Name:"></asp:Label></asp:TableCell>
                            <asp:TableCell><asp:Label ID="lblUserNameOutput" runat="server"></asp:Label>&nbsp;<asp:Label ID="lblUserSurnameOutput" runat="server"></asp:Label></asp:TableCell>
                        </asp:TableHeaderRow>
                        <asp:TableHeaderRow>
                            <asp:TableCell><asp:Label ID="lblUserCompany" runat="server" Text="Company:"></asp:Label></asp:TableCell>
                            <asp:TableCell><asp:Label ID="lblUserCompanyOutput" runat="server"></asp:Label></asp:TableCell>
                        </asp:TableHeaderRow>
                        <asp:TableHeaderRow>
                            <asp:TableCell><asp:Label ID="lblUserContactNum" runat="server" Text="Contact Number:"></asp:Label></asp:TableCell>
                            <asp:TableCell><asp:Label ID="lblUserContactNumOutput" runat="server"></asp:Label></asp:TableCell>
                        </asp:TableHeaderRow>
                        <asp:TableHeaderRow>
                            <asp:TableCell><asp:Label ID="lblUserEmail" runat="server" Text="E-mail:"></asp:Label></asp:TableCell>
                            <asp:TableCell><asp:Label ID="lblUserEmailOutput" runat="server"></asp:Label></asp:TableCell>
                        </asp:TableHeaderRow>
                        <asp:TableRow>
                            <asp:TableCell ColumnSpan="2">&nbsp;</asp:TableCell>                    
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell><asp:Label ID="lblBankName" runat="server" Text="Bank:"></asp:Label></asp:TableCell>                    
                            <asp:TableCell><asp:Label ID="lblBankNameOutput" runat="server" Text="Standard Bank"></asp:Label></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell><asp:Label ID="lblBankBranchName" runat="server" Text="Branch:"></asp:Label></asp:TableCell>                    
                            <asp:TableCell><asp:Label ID="lblBankBranchNameOutput" runat="server" Text="Stellenbosch"></asp:Label></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell><asp:Label ID="lblBankBranchCode" runat="server" Text="Branch Code:"></asp:Label></asp:TableCell>                    
                            <asp:TableCell><asp:Label ID="lblBankBranchCodeOutput" runat="server" Text="050610"></asp:Label></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell><asp:Label ID="lblAccountType" runat="server" Text="Type of Account:"></asp:Label></asp:TableCell>                    
                            <asp:TableCell><asp:Label ID="lblAccountTypeOutput" runat="server" Text="Cheque"></asp:Label></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell><asp:Label ID="lblAccountName" runat="server" Text="Name of Account:"></asp:Label></asp:TableCell>                    
                            <asp:TableCell><asp:Label ID="lblAccountNameOutput" runat="server" Text="University of Stellenbosch"></asp:Label></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell><asp:Label ID="lblAccountNum" runat="server" Text="Account Number:"></asp:Label></asp:TableCell>                    
                            <asp:TableCell><asp:Label ID="lblAccountNumOutput" runat="server" Text="073003069" Font-Bold="true" ForeColor="Red"></asp:Label></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell><asp:Label ID="lblPayLaterRef" runat="server" Text="Payment Reference:"></asp:Label></asp:TableCell>                    
                            <asp:TableCell RowSpan="2" VerticalAlign="Top"><asp:Label ID="lblPayLaterRefOutput" runat="server" Font-Bold="true" ForeColor="Red"></asp:Label></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>&nbsp;</asp:TableCell>                    
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell ColumnSpan="2"><asp:Label ID="lblProofOfPaymentMsg" runat="server" Font-Bold="true" Text="Please send proof of payment through to the USB and include your reference number."></asp:Label></asp:TableCell>                    
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>&nbsp;</asp:TableCell>                    
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell><asp:Label ID="lblUSBEmail" runat="server" Text="USB E-mail:"></asp:Label></asp:TableCell>                    
                            <asp:TableCell><asp:Label ID="lblUSBEmailOutput" runat="server" Text="usbcom@usb.ac.za"></asp:Label></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell><asp:Label ID="lblUSBFaxNum" runat="server" Text="USB Fax Number:"></asp:Label></asp:TableCell>                    
                            <asp:TableCell><asp:Label ID="lblUSBFaxNumOutput" runat="server" Text="+27 21 918 4468"></asp:Label></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell ColumnSpan="2">&nbsp;</asp:TableCell>                    
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell ColumnSpan="2"><asp:Label ID="lblPaymentNotification" runat="server" Font-Bold="true" ForeColor="Red" Text="* Please note: Registration will only be confirmed on receipt of proof of payment."></asp:Label></asp:TableCell>                    
                        </asp:TableRow>
                        <%--<asp:TableRow>
                            <asp:TableCell ColumnSpan="2">&nbsp;</asp:TableCell>                    
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell ColumnSpan="2" HorizontalAlign="Center"><asp:LinkButton ID="LinkButton1" runat="server" PostBackUrl="EventsCalendar.aspx" Text="Return to Events List"></asp:LinkButton></asp:TableCell>                    
                        </asp:TableRow>--%>
                        <asp:TableRow>
                            <asp:TableCell ColumnSpan="2">&nbsp;</asp:TableCell>                    
                        </asp:TableRow>
                    </asp:Table>
                </asp:PlaceHolder>
                                
                <asp:PlaceHolder ID="plhFooter" runat="server">
                    <asp:Table ID="tblFooter" runat="server">
                        <asp:TableRow><asp:TableCell CssClass="tblfooter">&nbsp;</asp:TableCell></asp:TableRow>
                    </asp:Table>
                </asp:PlaceHolder>		     
                
                <p class="bottom no-margin">&nbsp;</p>
                    	
		    </form>	
		<%--</div>
		<!--End Content-->		
		
	</div>
	<!--End Wrapper-->			
		
	<div id="footer">
	    <!-- FooterUserControl goes here -->
    	<IncludeFile:Footer ID="Footer1" runat="server" />					
	</div>
	<!--End Footer-->
	
	<div id="copyright">
        Copyright 2009 University of Stellenbosch Business School. All rights reserved.
    </div>
    <!--End Copyright-->

</div><!--End Container-->
</div><!--End Outer-->--%>

</body>
</html>

