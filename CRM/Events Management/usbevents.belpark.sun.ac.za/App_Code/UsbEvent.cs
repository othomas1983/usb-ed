﻿#region *** Using Directives ***
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.ObjectModel;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using CrmWsdl;
#endregion

/// <summary>
/// Summary description for UsbEvent
/// </summary>
public class UsbEvent
{
    #region *** Private Variables ***
        private int _iEventID = -1;
        private string _sEventTitle = String.Empty;
        private string _sEventVenue = String.Empty;
        private string _sEventStartDate = String.Empty;
        private string _sintEventStartDate = string.Empty;
        private string _sEventStartMonth = String.Empty;
        private string _sEventDetails = string.Empty;
        private string _sEventTime = string.Empty;
        private string _sEventDressCode = string.Empty;
        private string _sEventCost = string.Empty;
        private string _sEventContactPerson = string.Empty;
        private string _sEventContactNumber = string.Empty;
        private string _sEventContactEmail = string.Empty;
        private int _iEventBookingFormID = -1;
        private string _sCurrentYearLbl = string.Empty;
        private string _sCurrentMonthLbl = string.Empty;
        private string _sEventID = string.Empty;
    #endregion
	
    #region *** Constructors ***
        #region *** Default Constructor ***
            /// <summary>
            /// Initializes a new instance of the <see cref="UsbEvent"/> class.
            /// </summary>            
	        public UsbEvent()
	        {
		        //
		        // TODO: Add constructor logic here
		        //
	        }
        #endregion 

        #region *** Variable Constructor ***
            /// <summary>
            /// Initializes a new instance of the <see cref="PublicationArticle"/> class.
            /// </summary>
            /// <param name="sPdfLink">The sEventTitle.</param>
            /// <param name="sExtract">The sEventVenue.</param>
            /// <param name="sArticleMonth">The sEventStartDate.</param>
            /// 
            public UsbEvent(string sEventTitle, string sEventVenue, string sEventStartDate, string sintEventStartDate, string sEventStartMonth, string sEventDetails, string sEventTime, string sEventDressCode, string sEventCost, string sEventContactPerson, string sEventContactNumber, string sEventContactEmail, int iEventBookingFormID, string sCurrentYearLbl, string sCurrentMonthLbl, string sEventID)
            {
                _sEventTitle = sEventTitle;
                _sEventVenue = sEventVenue;
                _sEventStartDate = sEventStartDate;
                _sintEventStartDate = sintEventStartDate;
                _sEventStartMonth = sEventStartMonth;
                _sEventDetails = sEventDetails;
                _sEventTime = sEventTime;
                _sEventDressCode = sEventDressCode;
                _sEventCost = sEventCost;
                _sEventContactPerson = sEventContactPerson;
                _sEventContactNumber = sEventContactNumber;
                _sEventContactEmail = sEventContactEmail;
                _iEventBookingFormID = iEventBookingFormID;
                _sCurrentYearLbl = sCurrentYearLbl;
                _sCurrentMonthLbl = sCurrentMonthLbl;
                _sEventID = sEventID;
            }
        #endregion    
    #endregion

    #region *** Public Variables ***
            #region *** iEventID ***
            /// <summary>
            /// Gets or sets the iEventID.
            /// </summary>
            /// <value>The iEventID.</value>
            public int iEventID
            {
                get { return _iEventID; }
                set { _iEventID = value; }
            }
            #endregion

            #region *** sEventTitle ***
            /// <summary>
            /// Gets or sets the sEventTitle.
            /// </summary>
            /// <value>The sEventTitle.</value>
            public string sEventTitle
            {
                get { return _sEventTitle; }
                set
                {
                    if (_sEventTitle != value)
                    {
                        _sEventTitle = value;
                    }
                }
            }
            #endregion

            #region *** sEventVenue ***
            /// <summary>
            /// Gets or sets the sEventVenue.
            /// </summary>
            /// <value>The sEventVenue.</value>
            public string sEventVenue
            {
                get { return _sEventVenue; }
                set
                {
                    if (_sEventVenue != value)
                    {
                        _sEventVenue = value;
                    }
                }
            }
            #endregion

            #region *** sEventStartDate ***
            /// <summary>
            /// Gets or sets the sEventStartDate.
            /// </summary>
            /// <value>The sEventStartDate.</value>
            public string sEventStartDate
            {
                get { return _sEventStartDate; }
                set
                {
                    if (_sEventStartDate != value)
                    {
                        _sEventStartDate = value;
                    }
                }
            }
            #endregion

            #region *** sintEventStartDate ***
            /// <summary>
            /// Gets or sets the sintEventStartDate.
            /// </summary>
            /// <value>The sintEventStartDate.</value>
            public string sintEventStartDate
            {
                get { return _sintEventStartDate; }
                set
                {
                    if (_sintEventStartDate != value)
                    {
                        _sintEventStartDate = value;
                    }
                }
            }
            #endregion    

            #region *** sEventStartMonth ***
            /// <summary>
            /// Gets or sets the sEventStartMonth.
            /// </summary>
            /// <value>The sEventStartMonth.</value>
            public string sEventStartMonth
            {
                get { return _sEventStartMonth; }
                set
                {
                    if (_sEventStartMonth != value)
                    {
                        _sEventStartMonth = value;
                    }
                }
            }
            #endregion

            #region *** sEventDetails ***
            /// <summary>
            /// Gets or sets the sEventDetails.
            /// </summary>
            /// <value>The sEventDetails.</value>
            public string sEventDetails
            {
                get { return _sEventDetails; }
                set
                {
                    if (_sEventDetails != value)
                    {
                        _sEventDetails = value;
                    }
                }
            }
            #endregion

            #region *** sEventTime ***
            /// <summary>
            /// Gets or sets the sEventTime.
            /// </summary>
            /// <value>The sEventTime.</value>
            public string sEventTime
            {
                get { return _sEventTime; }
                set
                {
                    if (_sEventTime != value)
                    {
                        _sEventTime = value;
                    }
                }
            }
            #endregion

            #region *** sEventDressCode ***
            /// <summary>
            /// Gets or sets the sEventDressCode.
            /// </summary>
            /// <value>The sEventDressCode.</value>
            public string sEventDressCode
            {
                get { return _sEventDressCode; }
                set
                {
                    if (_sEventDressCode != value)
                    {
                        _sEventDressCode = value;
                    }
                }
            }
            #endregion

            #region *** sEventCost ***
            /// <summary>
            /// Gets or sets the sEventCost.
            /// </summary>
            /// <value>The sEventCost.</value>
            public string sEventCost
            {
                get { return _sEventCost; }
                set
                {
                    if (_sEventCost != value)
                    {
                        _sEventCost = value;
                    }
                }
            }
            #endregion        

            #region *** sEventContactPerson ***
            /// <summary>
            /// Gets or sets the sEventContactPerson.
            /// </summary>
            /// <value>The sEventContactPerson.</value>
            public string sEventContactPerson
            {
                get { return _sEventContactPerson; }
                set
                {
                    if (_sEventContactPerson != value)
                    {
                        _sEventContactPerson = value;
                    }
                }
            }
            #endregion

            #region *** sEventContactNumber ***
            /// <summary>
            /// Gets or sets the sEventContactNumber.
            /// </summary>
            /// <value>The sEventContactNumber.</value>
            public string sEventContactNumber
            {
                get { return _sEventContactNumber; }
                set
                {
                    if (_sEventContactNumber != value)
                    {
                        _sEventContactNumber = value;
                    }
                }
            }
            #endregion        
            
            #region *** sEventContactEmail ***
            /// <summary>
            /// Gets or sets the sEventContactEmail.
            /// </summary>
            /// <value>The sEventContactEmail.</value>
            public string sEventContactEmail
            {
                get { return _sEventContactEmail; }
                set
                {
                    if (_sEventContactEmail != value)
                    {
                        _sEventContactEmail = value;
                    }
                }
            }
            #endregion

            #region *** iEventBookingFormID ***
            /// <summary>
            /// Gets or sets the iEventBookingFormID.
            /// </summary>
            /// <value>The iEventBookingFormID.</value>
            public int iEventBookingFormID
            {
                get { return _iEventBookingFormID; }
                set { _iEventBookingFormID = value; }
            }
            #endregion    

            #region *** sCurrentYearLbl ***
            /// <summary>
            /// Gets or sets the sCurrentYearLbl.
            /// </summary>
            /// <value>The sCurrentYearLbl.</value>
            public string sCurrentYearLbl
            {
                get { return _sCurrentYearLbl; }
                set
                {
                    if (_sCurrentYearLbl != value)
                    {
                        _sCurrentYearLbl = value;
                    }
                }
            }
            #endregion

            #region *** sCurrentMonthLbl ***
            /// <summary>
            /// Gets or sets the sCurrentMonthLbl.
            /// </summary>
            /// <value>The sCurrentMonthLbl.</value>
            public string sCurrentMonthLbl
            {
                get { return _sCurrentMonthLbl; }
                set
                {
                    if (_sCurrentMonthLbl != value)
                    {
                        _sCurrentMonthLbl = value;
                    }
                }
            }
            #endregion

            #region *** sEventID ***
            /// <summary>
            /// Gets or sets the sEventID.
            /// </summary>
            /// <value>The sEventID.</value>
            public string sEventID
            {
                get { return _sEventID; }
                set
                {
                    if (_sEventID != value)
                    {
                        _sEventID = value;
                    }
                }
            }
            #endregion
            
    #endregion

    #region *** Methods ***
         #region *** List Events ***
            #region ***  List<UsbEvent> GetNextThreeEvents() ***
            public List<UsbEvent> GetNextThreeEvents()
            {
                List<UsbEvent> lUsbEvent = null;
                UsbEvent usbEvent;
                SqlDataReader rdr;
                SqlConnection conn = null;
                DateTime dt;

                try
                {
                    conn = new SqlConnection(ConfigurationManager.ConnectionStrings["USBWebDB"].ConnectionString);
                    conn.Open();

                    SqlCommand cmd = new SqlCommand("sp_GetNextThreeEvents", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    // execute the command
                    rdr = cmd.ExecuteReader();

                    if (rdr.HasRows)
                    {
                        lUsbEvent = new List<UsbEvent>();

                        while (rdr.Read())
                        {
                            usbEvent = new UsbEvent();
                            usbEvent.iEventID = Convert.ToInt32(rdr["EventID"].ToString());
                            
                            if (rdr["EventTitle"].ToString() != "")
                            {
                                if (rdr["EventTitle"].ToString().Length > 26)
                                {
                                    usbEvent.sEventTitle = DoStringManipulation(26,rdr["EventTitle"].ToString());
                                }
                                else
                                {
                                    usbEvent.sEventTitle = rdr["EventTitle"].ToString().Trim(); 
                                }                                
                            }

                            if (rdr["EventVenue"].ToString() != "")
                            {
                                if (rdr["EventVenue"].ToString().Length > 40)
                                {
                                    usbEvent.sEventVenue = DoStringManipulation(40, rdr["EventVenue"].ToString());
                                }
                                else
                                {
                                    usbEvent.sEventVenue = rdr["EventVenue"].ToString().Trim();
                                }                              
                            }

                            if (rdr["EventStart"].ToString() != "")
                            {
                                dt = Convert.ToDateTime(rdr["EventStart"].ToString());
                                usbEvent.sEventStartDate = dt.ToString("dd MMM");
                                usbEvent.sintEventStartDate = dt.ToString("dd");
                                usbEvent.sEventStartMonth = dt.ToString("MMM");
                            }
                            lUsbEvent.Add(usbEvent);                            
                        }
                    }
                    cmd.Dispose();
                }
                finally
                {
                    conn.Dispose();
                }

                return lUsbEvent;
            }

            #endregion

            #region ***  List<UsbEvent> GetFutureEvents() ***
            public List<UsbEvent> GetFutureEvents()
            {
                List<UsbEvent> lUsbEvent = null;
                UsbEvent usbEvent;
                SqlDataReader rdr;
                SqlConnection conn = null;
                DateTime dt;

                try
                {
                    conn = new SqlConnection(ConfigurationManager.ConnectionStrings["USBWebDB"].ConnectionString);
                    conn.Open();

                    SqlCommand cmd = new SqlCommand("sp_GetFutureEvents", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    // execute the command
                    rdr = cmd.ExecuteReader();

                    if (rdr.HasRows)
                    {
                        lUsbEvent = new List<UsbEvent>();

                        while (rdr.Read())
                        {
                            usbEvent = new UsbEvent();
                            usbEvent.iEventID = Convert.ToInt32(rdr["EventID"].ToString());

                            if (rdr["EventTitle"].ToString() != "")
                            {
                                usbEvent.sEventTitle = rdr["EventTitle"].ToString().Trim();
                            }

                            if (rdr["EventVenue"].ToString() != "")
                            {
                                usbEvent.sEventVenue = rdr["EventVenue"].ToString().Trim();
                            }

                            if (rdr["EventStart"].ToString() != "")
                            {
                                dt = Convert.ToDateTime(rdr["EventStart"].ToString());
                                usbEvent.sEventStartDate = dt.ToString("dd MMM");
                                usbEvent.sintEventStartDate = dt.ToString("dd");
                                usbEvent.sEventStartMonth = dt.ToString("MMM");
                            }
                            lUsbEvent.Add(usbEvent);
                        }
                    }
                    cmd.Dispose();
                }
                finally
                {
                    conn.Dispose();
                }

                return lUsbEvent;
            }

            #endregion

            #region ***  List<UsbEvent> GetAllEvents() ***
            public List<UsbEvent> GetAllEvents()
            {
                List<UsbEvent> lUsbEvent = null;
                UsbEvent usbEvent;
                SqlDataReader rdr;
                SqlConnection conn = null;
                DateTime dt;

                try
                {
                    conn = new SqlConnection(ConfigurationManager.ConnectionStrings["USBWebDB"].ConnectionString);
                    conn.Open();

                    SqlCommand cmd = new SqlCommand("sp_GetAllEvents", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    // execute the command
                    rdr = cmd.ExecuteReader();

                    if (rdr.HasRows)
                    {
                        lUsbEvent = new List<UsbEvent>();

                        while (rdr.Read())
                        {
                            usbEvent = new UsbEvent();
                            usbEvent.iEventID = Convert.ToInt32(rdr["EventID"].ToString());

                            if (rdr["EventTitle"].ToString() != "")
                            {
                                usbEvent.sEventTitle = rdr["EventTitle"].ToString().Trim();
                            }

                            if (rdr["EventVenue"].ToString() != "")
                            {
                                usbEvent.sEventVenue = rdr["EventVenue"].ToString().Trim();
                            }

                            if (rdr["EventStart"].ToString() != "")
                            {
                                dt = Convert.ToDateTime(rdr["EventStart"].ToString());
                                usbEvent.sEventStartDate = dt.ToString("dd MMM");
                                usbEvent.sintEventStartDate = dt.ToString("dd");
                                usbEvent.sEventStartMonth = dt.ToString("MMM");
                            }

                            lUsbEvent.Add(usbEvent);
                        }
                    }
                    cmd.Dispose();
                }
                finally
                {
                    conn.Dispose();
                }

                return lUsbEvent;
            }

            #endregion                

            #region ***  List<UsbEvent> GetFutureEventsList() ***
            public List<UsbEvent> GetFutureEventsList()
            {
                List<UsbEvent> lUsbEvent = null;
                UsbEvent usbEvent;
                SqlDataReader rdr;
                SqlConnection conn = null;
                DateTime dt;
                
                string sCurrentYear = "";
                string sPreviousYear = "";
                string sCurrentMonth = "";
                string sPreviousMonth = "";

                try
                {
                    conn = new SqlConnection(ConfigurationManager.ConnectionStrings["USBWebDB"].ConnectionString);
                    conn.Open();

                    SqlCommand cmd = new SqlCommand("sp_GetAllFutureEvents", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    // execute the command
                    rdr = cmd.ExecuteReader();

                    if (rdr.HasRows)
                    {
                        lUsbEvent = new List<UsbEvent>();

                        while (rdr.Read())
                        {
                            usbEvent = new UsbEvent();
                            usbEvent.iEventID = Convert.ToInt32(rdr["iID"].ToString());

                            if (rdr["dtStartDate"].ToString() != "")
                            {
                                dt = Convert.ToDateTime(rdr["dtStartDate"].ToString());
                                sCurrentYear = dt.ToString("yyyy");
                                sCurrentMonth = dt.ToString("MMMM");

                                usbEvent.sEventStartDate = dt.ToString("dd MMM yyyy");
                                //usbEvent.sEventStartDate = dt.ToString("M/dd/yyyy");
                                usbEvent.sintEventStartDate = dt.ToString("dd");
                                usbEvent.sEventStartMonth = dt.ToString("MMM");
                            }

                            if (sCurrentYear != sPreviousYear && sCurrentMonth != sPreviousMonth)
                            {
                                usbEvent.sCurrentYearLbl = "<tr><td style='border-bottom: 1px #333333 solid;' valign='top' colspan='2'><b>" + sCurrentYear.ToString() + "</b></td></tr>";
                                usbEvent.sCurrentMonthLbl = "<tr><td><span class='subheading_bold'><b>" + sCurrentMonth.ToString() + "</b></span></td></tr>";
                            }

                            if (sCurrentYear != sPreviousYear && sCurrentMonth == sPreviousMonth)
                            {
                                usbEvent.sCurrentYearLbl = "<tr><td style='border-bottom: 1px #333333 solid;' valign='top'><span class='subheading_bold'><b>" + sCurrentYear.ToString() + "</b></span></td></tr>";
                            }

                            if (sCurrentYear == sPreviousYear && sCurrentMonth != sPreviousMonth)
                            {
                                usbEvent.sCurrentMonthLbl = "<tr><td valign='top'><span class='subheading_bold'><b>" + sCurrentMonth.ToString() + "</b></span></td></tr>";
                            }     

                            if (rdr["sTitle"].ToString() != "")
                            {
                                usbEvent.sEventTitle = rdr["sTitle"].ToString().Trim();
                            }

                            if (rdr["sVenue"].ToString() != "")
                            {
                                usbEvent.sEventVenue = rdr["sVenue"].ToString().Trim();
                            }
                            
                            lUsbEvent.Add(usbEvent);
                            sPreviousYear = sCurrentYear;
                            sPreviousMonth = sCurrentMonth;                            
                        }
                    }
                    cmd.Dispose();
                }
                finally
                {
                    conn.Dispose();
                }

                return lUsbEvent;
            }

            #endregion                            

            #region ***  List<UsbEvent> GetFutureEventsListByRegionID(int intRegionID) ***
            public List<UsbEvent> GetFutureEventsListByRegionID(int intRegionID)
            {
                List<UsbEvent> lUsbEvent = null;
                UsbEvent usbEvent;
                SqlDataReader rdr;
                SqlConnection conn = null;
                DateTime dt;

                string sCurrentYear = "";
                string sPreviousYear = "";
                string sCurrentMonth = "";
                string sPreviousMonth = "";

                try
                {
                    conn = new SqlConnection(ConfigurationManager.ConnectionStrings["USBWebDB"].ConnectionString);
                    conn.Open();

                    SqlCommand cmd = new SqlCommand("sp_GetFutureEventsByRegionID", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@RegionID", intRegionID);

                    // execute the command
                    rdr = cmd.ExecuteReader();

                    if (rdr.HasRows)
                    {
                        lUsbEvent = new List<UsbEvent>();

                        while (rdr.Read())
                        {
                            usbEvent = new UsbEvent();
                            usbEvent.iEventID = Convert.ToInt32(rdr["EventID"].ToString());

                            if (rdr["EventStart"].ToString() != "")
                            {
                                dt = Convert.ToDateTime(rdr["EventStart"].ToString());
                                sCurrentYear = dt.ToString("yyyy");
                                sCurrentMonth = dt.ToString("MMMM");

                                usbEvent.sEventStartDate = dt.ToString("dd MMM yyyy");
                                usbEvent.sintEventStartDate = dt.ToString("dd");
                                usbEvent.sEventStartMonth = dt.ToString("MMM");
                            }

                            if (sCurrentYear != sPreviousYear && sCurrentMonth != sPreviousMonth)
                            {
                                usbEvent.sCurrentYearLbl = "<tr><td style='border-bottom: 1px #333333 solid;' valign='top' colspan='2'><b>" + sCurrentYear.ToString() + "</b></td></tr>";
                                usbEvent.sCurrentMonthLbl = "<tr><td><span class='subheading_bold'><b>" + sCurrentMonth.ToString() + "</b></span></td></tr>";
                            }

                            if (sCurrentYear != sPreviousYear && sCurrentMonth == sPreviousMonth)
                            {
                                usbEvent.sCurrentYearLbl = "<tr><td style='border-bottom: 1px #333333 solid;' valign='top'><span class='subheading_bold'><b>" + sCurrentYear.ToString() + "</b></span></td></tr>";
                            }

                            if (sCurrentYear == sPreviousYear && sCurrentMonth != sPreviousMonth)
                            {
                                usbEvent.sCurrentMonthLbl = "<tr><td valign='top'><span class='subheading_bold'><b>" + sCurrentMonth.ToString() + "</b></span></td></tr>";
                            }                         
                            
                            if (rdr["EventTitle"].ToString() != "")
                            {
                                usbEvent.sEventTitle = rdr["EventTitle"].ToString().Trim();
                            }

                            if (rdr["EventVenue"].ToString() != "")
                            {
                                usbEvent.sEventVenue = rdr["EventVenue"].ToString().Trim();
                            }

                            lUsbEvent.Add(usbEvent);
                            sPreviousYear = sCurrentYear;
                            sPreviousMonth = sCurrentMonth;
                        }

                    }
                    else
                    {
                        lUsbEvent = new List<UsbEvent>();
                        usbEvent = new UsbEvent();
                        usbEvent.sCurrentYearLbl = "<tr><td style='border-bottom: 1px #333333 solid;' valign='top' colspan='2'><b>No future events found for this region.</b></td></tr>";
                        lUsbEvent.Add(usbEvent);                        
                    }
                    cmd.Dispose();
                }
                finally
                {
                    conn.Dispose();
                }

                return lUsbEvent;
            }

            #endregion                            

            #region ***  List<UsbEvent> GetPastEventsListByRegionID(int intRegionID) ***
            public List<UsbEvent> GetPastEventsListByRegionID(int intRegionID)
            {
                List<UsbEvent> lUsbEvent = null;
                UsbEvent usbEvent;
                SqlDataReader rdr;
                SqlConnection conn = null;
                DateTime dt;
                
                string sCurrentYear = "";
                string sPreviousYear = "";
                string sCurrentMonth = "";
                string sPreviousMonth = "";

                try
                {
                    conn = new SqlConnection(ConfigurationManager.ConnectionStrings["USBWebDB"].ConnectionString);
                    conn.Open();

                    SqlCommand cmd = new SqlCommand("sp_GetPastEventsByRegionID", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@RegionID", intRegionID);

                    // execute the command
                    rdr = cmd.ExecuteReader();

                    if (rdr.HasRows)
                    {
                        lUsbEvent = new List<UsbEvent>();

                        while (rdr.Read())
                        {
                            usbEvent = new UsbEvent();
                            usbEvent.iEventID = Convert.ToInt32(rdr["EventID"].ToString());

                            if (rdr["EventStart"].ToString() != "")
                            {
                                dt = Convert.ToDateTime(rdr["EventStart"].ToString());
                                sCurrentYear = dt.ToString("yyyy");
                                sCurrentMonth = dt.ToString("MMMM");

                                usbEvent.sEventStartDate = dt.ToString("dd MMM yyyy");
                                usbEvent.sintEventStartDate = dt.ToString("dd");
                                usbEvent.sEventStartMonth = dt.ToString("MMM");
                            }

                            if (sCurrentYear != sPreviousYear && sCurrentMonth != sPreviousMonth)
                            {
                                usbEvent.sCurrentYearLbl = "<tr><td style='border-bottom: 1px #333333 solid;' valign='top' colspan='2'><b>" + sCurrentYear.ToString() + "</b></td></tr>";
                                usbEvent.sCurrentMonthLbl = "<tr><td><span class='subheading_bold'><b>" + sCurrentMonth.ToString() + "</b></span></td></tr>";
                            }

                            if (sCurrentYear != sPreviousYear && sCurrentMonth == sPreviousMonth)
                            {
                                usbEvent.sCurrentYearLbl = "<tr><td style='border-bottom: 1px #333333 solid;' valign='top' colspan='2'><b>" + sCurrentYear.ToString() + "</b></td></tr>";
                                usbEvent.sCurrentMonthLbl = "<tr><td><span class='subheading_bold'><b>" + sCurrentMonth.ToString() + "</b></span></td></tr>";
                            }

                            if (sCurrentYear == sPreviousYear && sCurrentMonth != sPreviousMonth)
                            {
                                usbEvent.sCurrentMonthLbl = "<tr><td valign='top'><span class='subheading_bold'><b>" + sCurrentMonth.ToString() + "</b></span></td></tr>";
                            } 

                            if (rdr["EventTitle"].ToString() != "")
                            {
                                usbEvent.sEventTitle = rdr["EventTitle"].ToString().Trim();
                            }

                            if (rdr["EventVenue"].ToString() != "")
                            {
                                usbEvent.sEventVenue = rdr["EventVenue"].ToString().Trim();
                            }

                            lUsbEvent.Add(usbEvent);
                            sPreviousYear = sCurrentYear;
                            sPreviousMonth = sCurrentMonth;
                        }
                    }
                    else
                    {
                        lUsbEvent = new List<UsbEvent>();
                        usbEvent = new UsbEvent();
                        usbEvent.sCurrentYearLbl = "<tr><td style='border-bottom: 1px #333333 solid;' valign='top' colspan='2'><b>No past events found for this region.</b></td></tr>";
                        lUsbEvent.Add(usbEvent);
                    }
                    cmd.Dispose();                    
                }
                finally
                {
                    conn.Dispose();
                }

                return lUsbEvent;
            }

            #endregion                            

            #region ***  List<UsbEvent> GetCRMFutureEventsList() ***
            public List<UsbEvent> GetCRMFutureEventsList()
            {
                List<UsbEvent> lUsbEvent = null;
                UsbEvent usbEvent;                
                DateTime dt;

                string sCurrentYear = "";
                string sPreviousYear = "";
                string sCurrentMonth = "";
                string sPreviousMonth = "";
                int iRecordsFound = 0;
                
                try
                {
                    initialiseCRMService();
                                        
                    lUsbEvent = new List<UsbEvent>();

                    foreach (campaign activeCampaigns in CRMRetrieveMultipleOnOrAfterQuery().BusinessEntities)
                    {
                        if (activeCampaigns.msa_startdatetime != null)
                        {
                            usbEvent = new UsbEvent();
                            usbEvent.sEventID = activeCampaigns.campaignid.Value.ToString();

                            if (activeCampaigns.msa_startdatetime.Value.ToString() != "")
                            {
                                dt = Convert.ToDateTime(activeCampaigns.msa_startdatetime.Value.ToString());
                                sCurrentYear = dt.ToString("yyyy");
                                sCurrentMonth = dt.ToString("MMMM");

                                usbEvent.sEventStartDate = dt.ToString("dd MMM yyyy");
                                usbEvent.sintEventStartDate = dt.ToString("dd");
                                usbEvent.sEventStartMonth = dt.ToString("MMM");
                            }

                            if (sCurrentYear != sPreviousYear && sCurrentMonth != sPreviousMonth)
                            {
                                usbEvent.sCurrentYearLbl = "<tr><td style='border-bottom: 1px #333333 solid;' valign='top' colspan='2'><b>" + sCurrentYear.ToString() + "</b></td></tr>";
                                usbEvent.sCurrentMonthLbl = "<tr><td><span class='subheading_bold'><b>" + sCurrentMonth.ToString() + "</b></span></td></tr>";
                            }

                            if (sCurrentYear != sPreviousYear && sCurrentMonth == sPreviousMonth)
                            {
                                usbEvent.sCurrentYearLbl = "<tr><td style='border-bottom: 1px #333333 solid;' valign='top'><span class='subheading_bold'><b>" + sCurrentYear.ToString() + "</b></span></td></tr>";
                            }

                            if (sCurrentYear == sPreviousYear && sCurrentMonth != sPreviousMonth)
                            {
                                usbEvent.sCurrentMonthLbl = "<tr><td valign='top'><span class='subheading_bold'><b>" + sCurrentMonth.ToString() + "</b></span></td></tr>";
                            }

                            if (activeCampaigns.msa_location != null)
                            {
                                activeCampaigns.msa_location.ToString();
                            }

                            if (activeCampaigns.name.ToString() != "")
                            {
                                usbEvent.sEventTitle = activeCampaigns.name.ToString().Trim();
                            }

                            if (activeCampaigns.msa_location != null)
                            {
                                usbEvent.sEventVenue = activeCampaigns.msa_location.ToString().Trim();
                            }

                            lUsbEvent.Add(usbEvent);
                            sPreviousYear = sCurrentYear;
                            sPreviousMonth = sCurrentMonth;
                            iRecordsFound = iRecordsFound + 1;                                                            
                        }                        
                    }
                    
                    if (iRecordsFound == 0)
                    {
                        usbEvent = new UsbEvent();
                        usbEvent.sCurrentYearLbl = "<tr><td valign='top' colspan='2'><b>* No events found.</b></td></tr>";
                        lUsbEvent.Add(usbEvent);
                    }
                    _srv.Dispose();                    
                }

                catch (Exception ex)
                {
                    lUsbEvent = new List<UsbEvent>();

                    usbEvent = new UsbEvent();
                    usbEvent.sCurrentYearLbl = "<tr><td valign='top' colspan='2'><b>* " + ex.Message + "</b></td></tr>";                    
                    lUsbEvent.Add(usbEvent);                           
                }

                finally
                {                    
                    _srv.Dispose();
                }
                return lUsbEvent;
            }
            #endregion                            

            #region ***  List<UsbEvent> GetCRMFutureEventsListByRegion(string strRegionName) ***
            public List<UsbEvent> GetCRMFutureEventsListByRegion(string strRegionName)
            {
                List<UsbEvent> lUsbEvent = null;
                UsbEvent usbEvent;
                DateTime dt;

                string sCurrentYear = "";
                string sPreviousYear = "";
                string sCurrentMonth = "";
                string sPreviousMonth = "";
                int iRecordsFound = 0;
                int iRecordsNotFound = 0;
                
                try
                {
                    initialiseCRMService();
                    
                    lUsbEvent = new List<UsbEvent>();

                    foreach (campaign activeCampaigns in this.CRMGetOnOrAfterRegionEvents().BusinessEntities)
                    {                        
                        if ((activeCampaigns.msa_startdatetime.Value.ToString() != "" && activeCampaigns.lt_regions_picklist_value != null) && (activeCampaigns.lt_eventcategory != null))
                        {
                            if (activeCampaigns.lt_regions_picklist_value.ToString().Trim().ToLower() == strRegionName || activeCampaigns.lt_regions_picklist_value.ToString().Trim().ToLower() == "national||")
                            {                                
                                usbEvent = new UsbEvent();
                                usbEvent.sEventID = activeCampaigns.campaignid.Value.ToString();

                                if (activeCampaigns.msa_startdatetime.Value.ToString() != "")
                                {
                                    dt = Convert.ToDateTime(activeCampaigns.msa_startdatetime.Value.ToString());
                                    sCurrentYear = dt.ToString("yyyy");
                                    sCurrentMonth = dt.ToString("MMMM");

                                    usbEvent.sEventStartDate = dt.ToString("dd MMM yyyy");
                                    usbEvent.sintEventStartDate = dt.ToString("dd");
                                    usbEvent.sEventStartMonth = dt.ToString("MMM");
                                }

                                if (sCurrentYear != sPreviousYear && sCurrentMonth != sPreviousMonth)
                                {
                                    usbEvent.sCurrentYearLbl = "<tr><td style='border-bottom: 1px #333333 solid;' valign='top' colspan='2'><b>" + sCurrentYear.ToString() + "</b></td></tr>";
                                    usbEvent.sCurrentMonthLbl = "<tr><td><span class='subheading_bold'><b>" + sCurrentMonth.ToString() + "</b></span></td></tr>";
                                }

                                if (sCurrentYear != sPreviousYear && sCurrentMonth == sPreviousMonth)
                                {
                                    usbEvent.sCurrentYearLbl = "<tr><td style='border-bottom: 1px #333333 solid;' valign='top'><span class='subheading_bold'><b>" + sCurrentYear.ToString() + "</b></span></td></tr>";
                                }

                                if (sCurrentYear == sPreviousYear && sCurrentMonth != sPreviousMonth)
                                {
                                    usbEvent.sCurrentMonthLbl = "<tr><td valign='top'><span class='subheading_bold'><b>" + sCurrentMonth.ToString() + "</b></span></td></tr>";
                                }

                                if (activeCampaigns.msa_location != null)
                                {
                                    activeCampaigns.msa_location.ToString();
                                }

                                if (activeCampaigns.name.ToString() != "")
                                {
                                    usbEvent.sEventTitle = activeCampaigns.name.ToString().Trim();
                                }

                                if (activeCampaigns.msa_location != null)
                                {
                                    usbEvent.sEventVenue = activeCampaigns.msa_location.ToString().Trim();
                                }                                        

                                lUsbEvent.Add(usbEvent);
                                sPreviousYear = sCurrentYear;
                                sPreviousMonth = sCurrentMonth;
                                iRecordsFound = iRecordsFound + 1;                                
                            }
                            else
                            {
                                iRecordsNotFound = iRecordsNotFound + 1;
                            }
                        }                                                                            
                    }
                    if (iRecordsFound == 0 && iRecordsNotFound != 0)
                    {
                        usbEvent = new UsbEvent();
                        usbEvent.sCurrentYearLbl = "<tr><td style='border-bottom: 1px #333333 solid;' valign='top' colspan='2'><b>No future events found for this region.</b></td></tr>";
                        lUsbEvent.Add(usbEvent);                       
                    }
                    _srv.Dispose();
                }
                finally
                {
                    _srv.Dispose();
                }
                return lUsbEvent;
            }

            #endregion                            

            #region ***  List<UsbEvent> GetCRMPastEventsListByRegion(string strRegionName) ***
            public List<UsbEvent> GetCRMPastEventsListByRegion(string strRegionName)
            {
                List<UsbEvent> lUsbEvent = null;
                UsbEvent usbEvent;
                DateTime dt;

                string sCurrentYear = "";
                string sPreviousYear = "";
                string sCurrentMonth = "";
                string sPreviousMonth = "";
                int iRecordsFound = 0;
                int iRecordsNotFound = 0;                

                try
                {
                    initialiseCRMService();                    

                    lUsbEvent = new List<UsbEvent>();

                    foreach (campaign activeCampaigns in CRMRetrieveMultipleNoDateQuery().BusinessEntities)
                    {                        
                        //if ((activeCampaigns.msa_startdatetime.Value.ToString() != "" && activeCampaigns.lt_regions_picklist_value != null) && (activeCampaigns.lt_eventcategory != null))
                        if (activeCampaigns.msa_startdatetime != null && activeCampaigns.lt_regions_picklist_value != null && activeCampaigns.lt_eventcategory != null)
                        {
                            if (activeCampaigns.lt_regions_picklist_value.ToString().Trim().ToLower() == strRegionName || activeCampaigns.lt_regions_picklist_value.ToString().Trim().ToLower() == "national||")
                            {
                                DateTime Today = DateTime.Parse(DateTime.Today.ToString("s"));
                                DateTime Tomorrow = Today.AddDays(1);

                                if (Tomorrow > DateTime.Parse(activeCampaigns.msa_startdatetime.Value))
                                {
                                    usbEvent = new UsbEvent();
                                    usbEvent.sEventID = activeCampaigns.campaignid.Value.ToString();

                                    if (activeCampaigns.msa_startdatetime.Value.ToString() != "")
                                    {
                                        dt = Convert.ToDateTime(activeCampaigns.msa_startdatetime.Value.ToString());
                                        sCurrentYear = dt.ToString("yyyy");
                                        sCurrentMonth = dt.ToString("MMMM");

                                        usbEvent.sEventStartDate = dt.ToString("dd MMM yyyy");
                                        usbEvent.sintEventStartDate = dt.ToString("dd");
                                        usbEvent.sEventStartMonth = dt.ToString("MMM");
                                    }

                                    if (sCurrentYear != sPreviousYear && sCurrentMonth != sPreviousMonth)
                                    {
                                        usbEvent.sCurrentYearLbl = "<tr><td style='border-bottom: 1px #333333 solid;' valign='top' colspan='2'><b>" + sCurrentYear.ToString() + "</b></td></tr>";
                                        usbEvent.sCurrentMonthLbl = "<tr><td><span class='subheading_bold'><b>" + sCurrentMonth.ToString() + "</b></span></td></tr>";
                                    }

                                    if (sCurrentYear != sPreviousYear && sCurrentMonth == sPreviousMonth)
                                    {
                                        usbEvent.sCurrentYearLbl = "<tr><td style='border-bottom: 1px #333333 solid;' valign='top'><span class='subheading_bold'><b>" + sCurrentYear.ToString() + "</b></span></td></tr>";
                                    }

                                    if (sCurrentYear == sPreviousYear && sCurrentMonth != sPreviousMonth)
                                    {
                                        usbEvent.sCurrentMonthLbl = "<tr><td valign='top'><span class='subheading_bold'><b>" + sCurrentMonth.ToString() + "</b></span></td></tr>";
                                    }

                                    if (activeCampaigns.msa_location != null)
                                    {
                                        activeCampaigns.msa_location.ToString();
                                    }

                                    if (activeCampaigns.name.ToString() != "")
                                    {
                                        usbEvent.sEventTitle = activeCampaigns.name.ToString().Trim();
                                    }

                                    if (activeCampaigns.msa_location != null)
                                    {
                                        usbEvent.sEventVenue = activeCampaigns.msa_location.ToString().Trim();
                                    }

                                    lUsbEvent.Add(usbEvent);
                                    sPreviousYear = sCurrentYear;
                                    sPreviousMonth = sCurrentMonth;
                                    iRecordsFound = iRecordsFound + 1;
                                }
                            }
                            else
                            {
                                iRecordsNotFound = iRecordsNotFound + 1;
                            }
                        }                        
                    }
                    if (iRecordsFound == 0 && iRecordsNotFound != 0)
                    {
                        usbEvent = new UsbEvent();
                        usbEvent.sCurrentYearLbl = "<tr><td style='border-bottom: 1px #333333 solid;' valign='top' colspan='2'><b>No past events found for this region.</b></td></tr>";
                        lUsbEvent.Add(usbEvent);
                    }
                    _srv.Dispose();
                }
                finally
                {
                    _srv.Dispose();
                }
                return lUsbEvent;
            }

            #endregion                            

        #endregion

            #region *** CRMGetOnOrAfterRegionEvents ***
            public BusinessEntityCollection CRMGetOnOrAfterRegionEvents()
            {
                //Retrieve all active (launched) campaigns on or after today's date
                QueryExpression query = new QueryExpression();
                query.ColumnSet = new AllColumns();
                query.EntityName = EntityName.campaign.ToString();

                ConditionExpression fistCondition = new ConditionExpression();
                fistCondition.AttributeName = "statuscode";
                fistCondition.Operator = ConditionOperator.Equal;
                fistCondition.Values = new String[] { "2" };

                ConditionExpression secondCondition = new ConditionExpression();
                secondCondition.AttributeName = "msa_startdatetime";
                secondCondition.Operator = ConditionOperator.OnOrAfter;
                secondCondition.Values = new string[] { DateTime.Now.ToString("s") };

                //Retrieve all USB M&C campaigns (USB M&C Business Unit GUID # Small Caps c1d531a7-232b-de11-98d9-00155d610f1a)
                ConditionExpression thirdCondition = new ConditionExpression();
                thirdCondition.AttributeName = "owningbusinessunit";
                thirdCondition.Operator = ConditionOperator.Equal;
                thirdCondition.Values = new string[] { "c1d531a7-232b-de11-98d9-00155d610f1a" };

                FilterExpression childFilter = new FilterExpression();
                childFilter.FilterOperator = LogicalOperator.And;
                childFilter.Conditions = new ConditionExpression[] { fistCondition, secondCondition, thirdCondition };
                //CRMGetOnOrAfterRegionEventchildFilter.Conditions = new ConditionExpression[] { fistCondition, secondCondition };

                //Set the order of returned campaigns
                OrderExpression oe = new OrderExpression();
                oe.AttributeName = "msa_startdatetime";
                oe.OrderType = OrderType.Ascending;

                query.Orders = new OrderExpression[] { oe };
                query.Criteria = childFilter;

                BusinessEntityCollection retrieved = _srv.RetrieveMultiple(query);

                return retrieved;
            }
            #endregion

         #region *** Object Events ***
            #region ***  UsbEvent GetEventDetails(int USBEventID) ***
                public UsbEvent GetEventDetails(int USBEventID)
                {
                    UsbEvent usbEvent = null;
                    SqlDataReader rdr;
                    SqlConnection conn = null;
                    DateTime dt;

                    try
                    {
                        conn = new SqlConnection(ConfigurationManager.ConnectionStrings["USBWebDB"].ConnectionString);
                        conn.Open();

                        SqlCommand cmd = new SqlCommand("sp_UILoadEvent", conn);
                        cmd.CommandType = CommandType.StoredProcedure;

                        // 3. add parameters to command, which
                        //    will be passed to the stored procedure
                        cmd.Parameters.AddWithValue("@iID", USBEventID);

                        // execute the command
                        rdr = cmd.ExecuteReader();

                        if (rdr.HasRows)
                        {
                            while (rdr.Read())
                            {
                                usbEvent = new UsbEvent();
                                //usbEvent.iEventID = Convert.ToInt32(rdr["EventID"].ToString());

                                if (rdr["sTitle"].ToString() != "")
                                {
                                    usbEvent.sEventTitle = rdr["sTitle"].ToString().Trim();
                                }

                                if (rdr["sDetails"].ToString() != "")
                                {
                                    usbEvent.sEventDetails = rdr["sDetails"].ToString().Trim();
                                }

                                if (rdr["dtStartDate"].ToString() != "")
                                {
                                    dt = Convert.ToDateTime(rdr["dtStartDate"].ToString());
                                    usbEvent.sEventStartDate = dt.ToString("dd MMM yyyy");
                                    usbEvent.sintEventStartDate = dt.ToString("dd");
                                    usbEvent.sEventStartMonth = dt.ToString("MMM");
                                }

                                if (rdr["sVenue"].ToString() != "")
                                {
                                    usbEvent.sEventVenue = rdr["sVenue"].ToString().Trim();
                                }

                                if (rdr["sCost"].ToString() != "")
                                {
                                    usbEvent.sEventCost = rdr["sCost"].ToString().Trim();
                                }

                                if (rdr["sContactPerson"].ToString() != "")
                                {
                                    usbEvent.sEventContactPerson = rdr["sContactPerson"].ToString().Trim();
                                }

                                if (rdr["sContactNumber"].ToString() != "")
                                {
                                    usbEvent.sEventContactNumber = rdr["sContactNumber"].ToString().Trim();
                                }

                                if (rdr["sContactEmail"].ToString() != "")
                                {
                                    usbEvent.sEventContactEmail = rdr["sContactEmail"].ToString().Trim();
                                }

                                if (rdr["iBookingFormID"].ToString() != "")
                                {
                                    usbEvent.iEventBookingFormID = Convert.ToInt32(rdr["iBookingFormID"].ToString());
                                }
                            }
                        }
                        cmd.Dispose();
                    }
                    finally
                    {
                        conn.Dispose();
                    }
                    return usbEvent;
                }
            #endregion        
         #endregion

        #region *** initialiseCRMService ***
            CrmWsdl.CrmService _srv = new CrmWsdl.CrmService();
            public void initialiseCRMService()
            {
                CrmAuthenticationToken token = new CrmAuthenticationToken();
                token.AuthenticationType = 0;
                token.OrganizationName = "US-BPC";

                _srv.CrmAuthenticationTokenValue = token;
                _srv.PreAuthenticate = true;
                _srv.Credentials = System.Net.CredentialCache.DefaultCredentials;
            }
        #endregion

        #region *** CRMRetrieveMultipleOnOrAfterQuery ***
            public BusinessEntityCollection CRMRetrieveMultipleOnOrAfterQuery()
            {
                //Retrieve all active (launched) campaigns on or after today's date
                QueryExpression query = new QueryExpression();
                query.ColumnSet = new AllColumns();
                query.EntityName = EntityName.campaign.ToString();

                ConditionExpression fistCondition = new ConditionExpression();
                fistCondition.AttributeName = "statuscode";
                fistCondition.Operator = ConditionOperator.Equal;
                fistCondition.Values = new String[] { "2" };

                ConditionExpression secondCondition = new ConditionExpression();
                secondCondition.AttributeName = "msa_startdatetime";
                secondCondition.Operator = ConditionOperator.OnOrAfter;
                secondCondition.Values = new string[] { DateTime.Now.ToString("s") };

                //Retrieve all USB M&C campaigns (USB M&C Business Unit GUID # Small Caps c1d531a7-232b-de11-98d9-00155d610f1a)
                ConditionExpression thirdCondition = new ConditionExpression();
                thirdCondition.AttributeName = "owningbusinessunit";
                thirdCondition.Operator = ConditionOperator.Equal;
                thirdCondition.Values = new string[] { "c1d531a7-232b-de11-98d9-00155d610f1a" };

                FilterExpression childFilter = new FilterExpression();
                childFilter.FilterOperator = LogicalOperator.And;
                childFilter.Conditions = new ConditionExpression[] { fistCondition, secondCondition, thirdCondition };

                //Set the order of returned campaigns
                OrderExpression oe = new OrderExpression();
                oe.AttributeName = "msa_startdatetime";
                oe.OrderType = OrderType.Ascending;

                query.Orders = new OrderExpression[] { oe };
                query.Criteria = childFilter;

                BusinessEntityCollection retrieved = _srv.RetrieveMultiple(query);

                return retrieved;
            }
        #endregion

        #region *** CRMRetrieveMultipleNoDateQuery ***
            public BusinessEntityCollection CRMRetrieveMultipleNoDateQuery()
            {
                //Retrieve all active (launched) campaigns on or after today's date
                QueryExpression query = new QueryExpression();
                query.ColumnSet = new AllColumns();
                query.EntityName = EntityName.campaign.ToString();

                ConditionExpression fistCondition = new ConditionExpression();
                fistCondition.AttributeName = "statuscode";
                fistCondition.Operator = ConditionOperator.Equal;
                fistCondition.Values = new String[] { "2" };
                
                //Retrieve all USB M&C campaigns (USB M&C Business Unit GUID # Small Caps c1d531a7-232b-de11-98d9-00155d610f1a)
                ConditionExpression secondCondition = new ConditionExpression();
                secondCondition.AttributeName = "owningbusinessunit";
                secondCondition.Operator = ConditionOperator.Equal;
                secondCondition.Values = new string[] { "c1d531a7-232b-de11-98d9-00155d610f1a" };

                FilterExpression childFilter = new FilterExpression();
                childFilter.FilterOperator = LogicalOperator.And;
                childFilter.Conditions = new ConditionExpression[] { fistCondition, secondCondition };

                //Set the order of returned campaigns
                OrderExpression oe = new OrderExpression();
                oe.AttributeName = "msa_startdatetime";
                oe.OrderType = OrderType.Ascending;

                query.Orders = new OrderExpression[] { oe };
                query.Criteria = childFilter;

                BusinessEntityCollection retrieved = _srv.RetrieveMultiple(query);

                return retrieved;
            }
        #endregion

        #region ***  List<UsbEvent> GetCRMFutureCareerEventsList() ***
        public List<UsbEvent> GetCareerServicesCRMFutureEventsList()
        {
            List<UsbEvent> lUsbEvent = null;
            UsbEvent usbEvent;
            DateTime dt;

            string sCurrentYear = "";
            string sPreviousYear = "";
            string sCurrentMonth = "";
            string sPreviousMonth = "";
            int iRecordsFound = 0;

            try
            {
                initialiseCRMService();

                //Retrieve all active (launched) campaigns on or after today's date
                QueryExpression query = new QueryExpression();
                query.ColumnSet = new AllColumns();
                query.EntityName = EntityName.campaign.ToString();

                ConditionExpression fistCondition = new ConditionExpression();
                fistCondition.AttributeName = "statuscode";
                fistCondition.Operator = ConditionOperator.Equal;
                fistCondition.Values = new String[] { "2" };

                ConditionExpression secondCondition = new ConditionExpression();
                secondCondition.AttributeName = "msa_startdatetime";
                secondCondition.Operator = ConditionOperator.OnOrAfter;
                secondCondition.Values = new string[] { DateTime.Now.ToString("s") };

                //Retrieve all USB M&C campaigns (USB M&C Business Unit GUID # Small Caps c1d531a7-232b-de11-98d9-00155d610f1a)
                ConditionExpression thirdCondition = new ConditionExpression();
                thirdCondition.AttributeName = "owningbusinessunit";
                thirdCondition.Operator = ConditionOperator.Equal;
                thirdCondition.Values = new string[] { "c1d531a7-232b-de11-98d9-00155d610f1a" };

                FilterExpression childFilter = new FilterExpression();
                childFilter.FilterOperator = LogicalOperator.And;
                childFilter.Conditions = new ConditionExpression[] { fistCondition, secondCondition, thirdCondition };

                //Set the order of returned campaigns
                OrderExpression oe = new OrderExpression();
                oe.AttributeName = "msa_startdatetime";
                oe.OrderType = OrderType.Ascending;

                query.Orders = new OrderExpression[] { oe };
                query.Criteria = childFilter;

                BusinessEntityCollection retrieved = _srv.RetrieveMultiple(query);

                lUsbEvent = new List<UsbEvent>();

                foreach (campaign activeCampaigns in retrieved.BusinessEntities)
                {
                    if (activeCampaigns.msa_startdatetime != null && activeCampaigns.lt_eventcategory != null)
                    {
                        if (activeCampaigns.lt_eventcategory.Value == 3)
                        {
                            usbEvent = new UsbEvent();
                            usbEvent.sEventID = activeCampaigns.campaignid.Value.ToString();

                            if (activeCampaigns.msa_startdatetime.Value.ToString() != "")
                            {
                                dt = Convert.ToDateTime(activeCampaigns.msa_startdatetime.Value.ToString());
                                sCurrentYear = dt.ToString("yyyy");
                                sCurrentMonth = dt.ToString("MMMM");

                                usbEvent.sEventStartDate = dt.ToString("dd MMM yyyy");
                                usbEvent.sintEventStartDate = dt.ToString("dd");
                                usbEvent.sEventStartMonth = dt.ToString("MMM");
                            }

                            if (sCurrentYear != sPreviousYear && sCurrentMonth != sPreviousMonth)
                            {
                                usbEvent.sCurrentYearLbl = "<tr><td style='border-bottom: 1px #333333 solid;' valign='top' colspan='2'><b>" + sCurrentYear.ToString() + "</b></td></tr>";
                                usbEvent.sCurrentMonthLbl = "<tr><td><span class='subheading_bold'><b>" + sCurrentMonth.ToString() + "</b></span></td></tr>";
                            }

                            if (sCurrentYear != sPreviousYear && sCurrentMonth == sPreviousMonth)
                            {
                                usbEvent.sCurrentYearLbl = "<tr><td style='border-bottom: 1px #333333 solid;' valign='top'><span class='subheading_bold'><b>" + sCurrentYear.ToString() + "</b></span></td></tr>";
                            }

                            if (sCurrentYear == sPreviousYear && sCurrentMonth != sPreviousMonth)
                            {
                                usbEvent.sCurrentMonthLbl = "<tr><td valign='top'><span class='subheading_bold'><b>" + sCurrentMonth.ToString() + "</b></span></td></tr>";
                            }

                            if (activeCampaigns.msa_location != null)
                            {
                                activeCampaigns.msa_location.ToString();
                            }

                            if (activeCampaigns.name.ToString() != "")
                            {
                                usbEvent.sEventTitle = activeCampaigns.name.ToString().Trim();
                            }

                            if (activeCampaigns.msa_location != null)
                            {
                                usbEvent.sEventVenue = activeCampaigns.msa_location.ToString().Trim();
                            }

                            lUsbEvent.Add(usbEvent);
                            sPreviousYear = sCurrentYear;
                            sPreviousMonth = sCurrentMonth;
                            iRecordsFound = iRecordsFound + 1;
                        }
                    }
                }

                if (iRecordsFound == 0)
                {
                    usbEvent = new UsbEvent();
                    usbEvent.sCurrentYearLbl = "<tr><td valign='top' colspan='2'><b>* No events found.</b></td></tr>";
                    lUsbEvent.Add(usbEvent);
                }
                _srv.Dispose();
            }

            catch (Exception ex)
            {
                lUsbEvent = new List<UsbEvent>();

                usbEvent = new UsbEvent();
                usbEvent.sCurrentYearLbl = "<tr><td valign='top' colspan='2'><b>* " + ex.Message + "</b></td></tr>";
                lUsbEvent.Add(usbEvent);
            }

            finally
            {
                _srv.Dispose();
            }
            return lUsbEvent;
        }
        #endregion                            

        #region *** DoStringManipulation ***
            private string DoStringManipulation(int StringSizeLimit, string StringToBeManipulated)
            {
                string strMyString = "";
                strMyString = StringToBeManipulated;
                string strSubedStringPlusOne = "";
                string strSubedString = "";
                string strMyAlteredString = "";
                string strLastCharInSubedStringPlusOne = "";

                strSubedStringPlusOne = strMyString.Substring(0, StringSizeLimit + 1);
                strLastCharInSubedStringPlusOne = strSubedStringPlusOne.Substring(strSubedStringPlusOne.Length - 1, 1);
                strSubedString = strMyString.Substring(0, StringSizeLimit);

                if (strLastCharInSubedStringPlusOne != " ")
                {
                    while (!strSubedString.EndsWith(" "))
                    {
                        strMyAlteredString = strSubedString.Substring(0, strSubedString.Length - 1);
                        strSubedString = strMyAlteredString;
                    }
                }
                strMyString = strSubedString.Trim();
                return strMyString;
            }
        #endregion                
    #endregion
}
