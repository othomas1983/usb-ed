﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EventsCalendar.aspx.cs" Inherits="NewsAndEvents_EventsCalendar" %>
<%@ Register TagPrefix="IncludeFile" TagName="Header" Src="Common/UserControls/HeaderUserControl.ascx" %>
<%@ Register TagPrefix="IncludeFile" TagName="Footer" Src="Common/UserControls/FooterUserControl.ascx"  %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>USB | News And Events | Events calendar</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href="<%=ConfigurationManager.AppSettings["UserImageURL"]%>Common/StyleSheets/sub-style.css" rel="stylesheet" type="text/css" media="screen" />
</head>

<script type="text/javascript" src="<%=ConfigurationManager.AppSettings["UserImageURL"]%>Common/Scripts/milonic_src.js"></script> 
<script type="text/javascript" src="<%=ConfigurationManager.AppSettings["UserImageURL"]%>Common/Scripts/mmenudom.js"></script>

<body>

<div id="outer">

<div id="container">
	<div id="header">		
        <!-- HeaderUserControl goes here -->
	    <IncludeFile:Header ID="Header1" runat="server" />	
	</div>
	<!--End Header-->
	
	<div id="topbar-newsandevents">
	    &nbsp;
	</div>
	<!--End Topbar-->

	<div id="wrapper">	
		<div id="leftnav">		
			<script type="text/javascript" src="<%=ConfigurationManager.AppSettings["UserImageURL"]%>Common/Scripts/usb_left_menu_newsandevents.js"></script>
		</div>
		<!--End Leftnav-->
		
		<div id="right">			
			<div class="rightbox">				
				<img src="Common/Images/AdministeredImages/NoneCrossLinked/6_1_NewsAndEvents.jpg" width="180" height="287" onerror="this.onerror=null;this.src='Common/Images/AdministeredImages/NoneCrossLinked/6_1_NewsAndEvents.jpg'"  alt="" />
			</div>		     
			<!--End Rightbox-->
		</div>
		<!--End Right-->

		<div id="content">
		    <form id="form1" runat="server">
		        <p><span class="mainheading"><b>Events calendar</b></span></p>

                <p>&nbsp;</p>
                
		        <p><span class="subheading_bold">Upcoming events</span></p>	
		            <asp:Repeater ID="RepeaterEvents" runat="server">
			            <HeaderTemplate> <table cellpadding="4" cellspacing="0" width="100%"> </HeaderTemplate>
			            <ItemTemplate>
			                <%#DataBinder.Eval(Container.DataItem, "sCurrentYearLbl")%>
			                <%#DataBinder.Eval(Container.DataItem, "sCurrentMonthLbl")%>
			                <tr>
			                    <td valign="top"><%#DataBinder.Eval(Container.DataItem, "sEventStartDate")%></td>
			                    <td valign="top"><asp:LinkButton ID="LinkButton1" runat="server" CommandArgument='<%#DataBinder.Eval(Container.DataItem, "sEventID")%>' OnCommand="LinkBtnEventItem_Command"><b><%#DataBinder.Eval(Container.DataItem, "sEventTitle")%></b></asp:LinkButton><br /><%#DataBinder.Eval(Container.DataItem, "sEventVenue")%></td>			            
			                </tr> 			                      	        
			            </ItemTemplate>
			            <FooterTemplate> </table> </FooterTemplate>
			        </asp:Repeater>	

                    <%--<p class="subheading_bold">We are performing scheduled maintenance. We should be back shortly.</p>--%>
                    
		    </form>	
		    
		    <p class="bottom no-margin">&nbsp;</p>
		    
		</div>
		<!--End Content-->		
		
	</div>
	<!--End Wrapper-->			
		
	<div id="footer">
	    <!-- FooterUserControl goes here -->
    	<IncludeFile:Footer ID="Footer1" runat="server" />					
	</div>
	<!--End Footer-->
	
	<div id="copyright">
        Copyright 2009 University of Stellenbosch Business School. All rights reserved.
    </div>
    <!--End Copyright-->

</div><!--End Container-->
</div><!--End Outer-->

</body>
</html>
