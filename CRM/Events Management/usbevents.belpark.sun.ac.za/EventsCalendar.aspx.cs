﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class NewsAndEvents_EventsCalendar : System.Web.UI.Page
{
    #region *** Page_Load ***
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack && !Page.IsCallback)
        {
            GetAllFutureEvents();
        }
    }
    #endregion

    #region *** GetAllFutureEvents ***
    private void GetAllFutureEvents()
    {
        UsbEvent oUsbEvent = new UsbEvent();
        List<UsbEvent> oListUsbEvent = oUsbEvent.GetCRMFutureEventsList();
        RepeaterEvents.DataSource = oListUsbEvent;
        RepeaterEvents.DataBind();
    }
    #endregion

    #region *** LinkBtnEventItem_Command ***
    public void LinkBtnEventItem_Command(Object sender, CommandEventArgs e)
    {
        string strTheValue = e.CommandArgument.ToString();
        string strURL = "EventItem.aspx?EventItem=" + strTheValue + "&RefFunctionality=events";
        Response.Redirect(strURL, true);
    }
    #endregion    
}
