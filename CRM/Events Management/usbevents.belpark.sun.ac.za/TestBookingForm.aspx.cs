﻿using System;
using System.IO;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using System.Net.Mail;
using System.Windows.Forms;
using CrmWsdl;

public partial class NewsAndEvents_TestBookingForm : System.Web.UI.Page
{
    private CrmWsdl.CrmService _svc = new CrmWsdl.CrmService();
            
    public static string AppCodeOut;

    private static string errorMsg = "An error has occurred while processing your booking.\\n\\nPlease try again later.";

    //public static bool BookOrder = false;
                
    public void initialiseCRMService()
    {
        CrmAuthenticationToken token = new CrmAuthenticationToken();
        token.AuthenticationType = 0;
        token.OrganizationName = "US-BPC";

        _svc.CrmAuthenticationTokenValue = token;
        _svc.PreAuthenticate = true;
        _svc.Credentials = System.Net.CredentialCache.DefaultCredentials;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack && !Page.IsCallback)
        {
            //Set no-cache
            Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache);            
        }
                                                 
        //Set form object properties       
        BulkRegistrationCheckBox.Attributes.Add("onClick", "return checkTab(this);");        
        ContactTable.Attributes.Add("style", "width: 100%;");
        AddressTable.Attributes.Add("style", "width: 100%;");
        attendees_table.Attributes.Add("style", "display: none;");
        BulkTable.Attributes.Add("style", "width: 100%;");                        
        tblFooter.Attributes.Add("style", "width: 100%;");
                            
        plhBookingForm.Visible = true;
        plhPaymentForm.Visible = false;
        plhBookingConfirm.Visible = false;
        
        SubmitButtonUpdate.Text = "Submit Form";
       
        LoadForm();        
    }
    
    private void LoadForm()
    {
        initialiseCRMService();

        //Set no-cache
        Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache);

        //Get QueryString value(s)
        string iID = Request.QueryString["iID"];
        string Group = Request.QueryString["GRP"];

        //Write iID to hidden label
        lbliID.Text = iID;

        //Check Campaign GUID before doing any processing
        if (lbliID.Text != null)
        {
            //Retrieve all active (launched) campaigns
            QueryByAttribute nxtquery = new QueryByAttribute();
            nxtquery.ColumnSet = new AllColumns();
            nxtquery.EntityName = EntityName.campaign.ToString();
            nxtquery.Attributes = new string[] { "campaignid" };
            nxtquery.Values = new String[] { iID };

            BusinessEntityCollection retrieved = _svc.RetrieveMultiple(nxtquery);

            campaign GetCampaign = (campaign)retrieved.BusinessEntities[0];

            EventName.Text = GetCampaign.name;


            //Set visibility of ContactTable
            if (GetCampaign.lt_contactdetails != null)
            {
                if (GetCampaign.lt_contactdetails.Value == true)
                {
                    ContactTable.Visible = true;
                }
                else
                {
                    ContactTable.Visible = false;
                }
            }
            else
            {
                ContactTable.Visible = false;
            }


            //Set visibility of AddressTable
            if (GetCampaign.lt_addressdetails != null)
            {
                if (GetCampaign.lt_addressdetails.Value == true)
                {
                    AddressTable.Visible = true;
                }
                else
                {
                    AddressTable.Visible = false;
                }
            }
            else
            {
                AddressTable.Visible = false;
            }


            //Set visibility of BulkTable
            if (GetCampaign.lt_bulkbookingdetails != null)
            {
                if (GetCampaign.lt_bulkbookingdetails.Value == true)
                {
                    BulkTable.Visible = true;
                }
                else
                {
                    BulkTable.Visible = false;
                }
            }
            else
            {
                BulkTable.Visible = false;
            }

            //Set visibility of Cocktail function table.
            if(GetCampaign.lt_attendcocktailfunction != null)
            {
                if (GetCampaign.lt_attendcocktailfunction.Value == true)
                {
                    tblCocktailFunction.Visible = true; 
                }
            }

            //Set visibility of FoodTable
            if (GetCampaign.lt_fooddetails != null)
            {
                if (GetCampaign.lt_fooddetails.Value == true)
                {
                    FoodTable.Visible = true;
                    FoodTableAdditionalInfo.Visible = true;
                }
                else
                {
                    FoodTable.Visible = false;
                    FoodTableAdditionalInfo.Visible = false;
                }
            }
            else
            {
                FoodTable.Visible = false;
                FoodTableAdditionalInfo.Visible = false;
            }


            //Set visibility of InfoSessionSHLTable
            if (GetCampaign.lt_infosessionshl != null)
            {
                if (GetCampaign.lt_infosessionshl.Value == true)
                {
                    InfoSessionSHLTable.Visible = true;
                }
                else
                {
                    InfoSessionSHLTable.Visible = false;
                }
            }
            else
            {
                InfoSessionSHLTable.Visible = false;
            }


            //Set visibility of BankingDetailsTable
            if (GetCampaign.lt_bankingdetails != null)
            {
                if (GetCampaign.lt_bankingdetails.Value == true)
                {
                    BankingDetailsTable.Visible = true;
                }
                else
                {
                    BankingDetailsTable.Visible = false;
                }
            }
            else
            {
                BankingDetailsTable.Visible = false;
            }


            //Set visibility of InvoiceRequiredTable
            if (GetCampaign.lt_invoicerequired != null)
            {
                if (GetCampaign.lt_invoicerequired.Value == true)
                {
                    tblInvoiceRequired.Visible = true;

                    if (GetCampaign.lt_emailaddress != null)
                    {
                        //string ContactEmail = GetCampaign.lt_emailaddress.ToString();

                        //lblContactEmail.Text = "<a href='mailto:" + ContactEmail + "'>" + ContactEmail + "</a>";
                    }
                }
                else
                {
                    tblInvoiceRequired.Visible = false;
                }
            }
            else
            {
                tblInvoiceRequired.Visible = false;
            }


            //Set visibility of InvoiceTable
            if (GetCampaign.lt_invoicedetails != null)
            {
                if (GetCampaign.lt_invoicedetails.Value == true)
                {
                    InvoiceTable.Visible = true;
                }
                else
                {
                    InvoiceTable.Visible = false;
                }
            }
            else
            {
                InvoiceTable.Visible = false;
            }


            //Set visibility of PartnerFunctionTable
            if (GetCampaign.lt_partnerfunction != null)
            {
                if (GetCampaign.lt_partnerfunction.Value == true)
                {
                    PartnerFunctionTable.Visible = true;                    
                }
                else
                {
                    PartnerFunctionTable.Visible = false;
                }
            }
            else
            {
                PartnerFunctionTable.Visible = false;
            }


            //Set visibility of PeopleAttendingTable
            if (GetCampaign.lt_peopleattending != null)
            {
                if (GetCampaign.lt_peopleattending.Value == true)
                {
                    PeopleAttendingTable.Visible = true;

                    if (Group != null && Group != "")
                    {
                        //Set Number of People Attending Dropdown list values
                        if (Group.ToLower() == "corp")
                        {
                            //ddl.Items.Add(new ListItem("text", "value"));
                            ddlNumberOfPeopleDetails.Items.Add(new ListItem("4", "4"));
                            ddlNumberOfPeopleDetails.Items.Add(new ListItem("8", "8"));
                        }

                        if (Group.ToLower() == "pers")
                        {
                            //ddl.Items.Add(new ListItem("text", "value"));                
                            ddlNumberOfPeopleDetails.Items.Add(new ListItem("4", "4"));
                            ddlNumberOfPeopleDetails.Items.Add(new ListItem("8", "8"));
                        }

                        if (Group.ToLower() == "ind")
                        {
                            //ddl.Items.Add(new ListItem("text", "value"));
                            ddlNumberOfPeopleDetails.Items.Add(new ListItem("1", "1"));
                            ddlNumberOfPeopleDetails.Items.Add(new ListItem("2", "2"));
                        }
                    }
                }
                else
                {
                    PeopleAttendingTable.Visible = false;
                }
            }
            else
            {
                PeopleAttendingTable.Visible = false;
            }


            //Set visibility of BookOrderTable
            if (GetCampaign.lt_bookorderdetails != null)
            {
                if (GetCampaign.lt_bookorderdetails.Value == true)
                {
                    BookOrderTable.Visible = true;
                    lblBookingForm.Text = "BOOK ORDER FORM: ";
                }
                else
                {
                    BookOrderTable.Visible = false;
                }
            }
            else
            {
                BookOrderTable.Visible = false;
            }


            ////Set visibility of PaymentTable
            //if (GetCampaign.lt_paymentdetails != null)
            //{
            //    if (GetCampaign.lt_paymentdetails.Value == true)
            //    {
            //        tblPaymentTable.Visible = true;                    
            //    }
            //    else
            //    {
            //        tblPaymentTable.Visible = false;
            //    }
            //}
            //else
            //{
            //    tblPaymentTable.Visible = false;
            //}
           
        }
    }


    ////Building userRefCRM string
    //private string BuildUserRef()
    //{        
    //    string FirstNameInitial = NameInput.Text.Substring(0, 1);
        
    //    Regex r = new Regex(@"\s");
    //    string TrimSurname = r.Replace(SurnameInput.Text, "");

    //    string userRefCRM = FirstNameInitial + TrimSurname + "_" + GetTime;

    //    return userRefCRM;
    //}


    //Building userRefCRM string
    protected void BuildUserRef()
    {
        string FirstNameInitial = NameInput.Text.Substring(0, 1);

        Regex r = new Regex(@"\s");
        string TrimSurname = r.Replace(SurnameInput.Text, "");

        string userRefCRM = FirstNameInitial + TrimSurname + "_" + DateTime.Now.ToString("ddMMyyyy_HHmmss");

        HttpContext.Current.Session.Add("DateTime", userRefCRM);
    }

        
    //Log file        
    public void LogFile(string sExceptionName, string sEventName)
    {
        StreamWriter log;
        
        string logName = DateTime.Now.ToString("yyyy-MM-dd") + ".log";

        if (!File.Exists(logName))
        {
            log = new StreamWriter(logName);
        }
        else
        {
            log = File.AppendText(logName);
        }

        //Write to the file
        log.WriteLine("Data Time: " + DateTime.Now);
        log.WriteLine("Exception Name: " + sExceptionName);
        log.WriteLine("Event Name: " + sEventName);

        //Close the stream
        log.Close();
    }

       
    protected void SubmitButtonUpdate_Click(object sender, EventArgs e)
    {
        string user_code = VerifiedCaptchaCodeTB.Text;
        string captcha_code = HttpContext.Current.Session["CAPTCHA_CODE"].ToString();                              


        if (user_code != captcha_code)
        {
            VerifiedCaptchaCodeTB.Focus();
            CaptchaImageLbl.Text = "* Verification failed. Please retype the above verification code carefully.";
        }
        else
        {
            plhBookingForm.Visible = false;
            plhPaymentForm.Visible = false;
            plhBookingConfirm.Visible = false;

            BuildUserRef();

            initialiseCRMService();

            //Retrieve all campaign related information
            AllColumns col = new AllColumns();

            //campaignGuid is the GUID of the record being retrieved
            Guid campaignGuid = new Guid(lbliID.Text);

            campaign GetCampaign = (campaign)_svc.Retrieve(EntityName.campaign.ToString(), campaignGuid, col);

            //Populate event cost; AppCode & TransactCode
            if (GetCampaign.msa_costofadmission != null && GetCampaign.lt_paymentdetails != null)
            {
                if (GetCampaign.lt_paymentdetails.Value == true)
                {
                    if (BulkRegistrationCheckBox.Checked && NumberOfAttendessInput.Text != "")
                    {
                        int CostPerHead_Bulk = Convert.ToInt32(GetCampaign.msa_costofadmission);

                        decimal CostPerAttendees_Bulk = Convert.ToDecimal(NumberOfAttendessInput.Text) * CostPerHead_Bulk;

                        lblEventCostOutput.Text = "R " + CostPerAttendees_Bulk;
                    }
                    else if (GetCampaign.lt_peopleattending != null && GetCampaign.lt_peopleattending.Value == true)
                    {
                        if (ddlNumberOfPeopleDetails.SelectedValue.ToString() != "")
                        {
                            int CostPerHead_PplNo = Convert.ToInt32(GetCampaign.msa_costofadmission);

                            decimal CostPerAttendees_PplNo = Convert.ToDecimal(ddlNumberOfPeopleDetails.SelectedValue) * CostPerHead_PplNo;

                            lblEventCostOutput.Text = "R " + CostPerAttendees_PplNo;
                        }
                        else
                        {
                            lblEventCostOutput.Text = "R " + GetCampaign.msa_costofadmission;
                        }
                    }
                    else if (GetCampaign.lt_bookorderdetails != null)
                    {
                        if (GetCampaign.lt_bookorderdetails.Value == true && ddlNumberOfCopies.SelectedValue.ToString() != "")
                        {
                            //BookOrder = true;

                            int CostPerBook_Base = Convert.ToInt32(GetCampaign.msa_costofadmission);

                            decimal PostAndPackFee = Convert.ToDecimal(30);

                            //Postage & packaging
                            if (rblPostageOrCollect.SelectedValue == "1")
                            {
                                decimal CostPerBook_PostAndPack = Convert.ToDecimal(ddlNumberOfCopies.SelectedValue) * CostPerBook_Base + PostAndPackFee;

                                lblEventCostOutput.Text = "R " + CostPerBook_PostAndPack;
                            }

                            //Collect
                            if (rblPostageOrCollect.SelectedValue == "2")
                            {
                                decimal CostPerBook_Collect = Convert.ToDecimal(ddlNumberOfCopies.SelectedValue) * CostPerBook_Base;

                                lblEventCostOutput.Text = "R " + CostPerBook_Collect;
                            }
                        }
                        else
                        {
                            lblEventCostOutput.Text = "R " + GetCampaign.msa_costofadmission;
                        }
                    }
                    else
                    {
                        lblEventCostOutput.Text = "R " + GetCampaign.msa_costofadmission;
                    }

                    if (GetCampaign.lt_appcode != null)
                    {
                        AppCodeOut = GetCampaign.lt_appcode.ToString();
                    }

                    plhPaymentForm.Visible = true;

                    lblEventNamePayOutput.Text = EventName.Text;
                }
                else
                {
                    plhBookingConfirm.Visible = true;
                }
            }
            else
            {
                plhBookingConfirm.Visible = true;               
            }            


            //Create a lead for the attendee
            lead newLead = new lead();
                        
            //Create the activity party object
            activityparty attendee = new activityparty();

            //Create the campaign response object
            campaignresponse rsvp = new campaignresponse();
                       

            //Contact Details
            newLead.lastname = SurnameInput.Text;
            newLead.firstname = NameInput.Text;
            newLead.lt_governmentid = TestIDNoOutput.Text;
            newLead.lt_passportnumber = PassportNoInput.Text;
            newLead.lt_foreignidnumber = tbForeignIdNoInput.Text;            
            newLead.companyname = CompanyInput.Text;
            newLead.jobtitle = JobTitleInput.Text;
            newLead.telephone1 = BusinessPhoneInput.Text;
            newLead.mobilephone = MobileNoInput.Text;
            newLead.emailaddress1 = EmailInput.Text;

            if (ddlAssociationDetails.SelectedValue != "")
            { 
                newLead.lt_associationwithusb = new Picklist();
                newLead.lt_associationwithusb.Value = Convert.ToInt32(ddlAssociationDetails.SelectedValue);
            }

            //Address Details
            if (GetCampaign.lt_addressdetails != null)
            {
                newLead.address1_line1 = HomeAdressStreet1Input.Text;
                newLead.address1_line2 = HomeAddressSteet2Input.Text;
                newLead.address1_line3 = HomeAddressStreet3Input.Text;
                newLead.address1_city = HomeAddressCityInput.Text;
                newLead.address1_postalcode = HomeAddressPostalCodeInput.Text;
                newLead.address1_country = HomeAddressCountryInput.Text;
            }

            //Food Details
            if (ddlFoodDetails.SelectedValue != "")
            {
                newLead.lt_dietaryrequirements = new Picklist();
                newLead.lt_dietaryrequirements.Value = Convert.ToInt32(ddlFoodDetails.SelectedValue);
            }

            //Food Details Partner
            if (ddlFoodDetailsPartner.SelectedValue != "")
            {
                newLead.lt_partnerdietaryrequirements = new Picklist();
                newLead.lt_partnerdietaryrequirements.Value = Convert.ToInt32(ddlFoodDetailsPartner.SelectedValue);
            }

            //Food Details - Additional Information
            if (tbFoodDetailsAdditional.Text != "")
            {
                newLead.lt_dietaryrequirementsother = tbFoodDetailsAdditional.Text;
            }
            
            
            Guid newLeadGuid = _svc.Create(newLead);
                        
            attendee.partyid = new Lookup();
            attendee.partyid.Value = newLeadGuid;
            attendee.partyid.type = EntityName.lead.ToString();

            //Create campaign response
            rsvp.customer = new activityparty[] { attendee };

            rsvp.regardingobjectid = new Lookup();
            rsvp.regardingobjectid.Value = (new Guid(lbliID.Text));
            rsvp.regardingobjectid.type = EntityName.campaign.ToString();
            rsvp.subject = "Online booking";

            rsvp.receivedon = new CrmDateTime();
            rsvp.receivedon.Value = DateTime.Today.ToString("yyyy-MM-ddTHH:mm:ss");

            //Update Partner Function Details
            if (GetCampaign.lt_partnerfunction != null)
            {
                //With partner
                if (rblPartnerDetails.SelectedValue == "1")
                {
                    rsvp.lt_attendingwithpartner = new CrmBoolean();
                    rsvp.lt_attendingwithpartner.Value = true;

                    rsvp.lt_numberofattendees = new CrmNumber();
                    rsvp.lt_numberofattendees.Value = 2;
                }

                //No partner
                if (rblPartnerDetails.SelectedValue == "0")
                {
                    rsvp.lt_numberofattendees = new CrmNumber();
                    rsvp.lt_numberofattendees.Value = 1;
                }
            }

            //Update Book Order Details
            if (GetCampaign.lt_bookorderdetails != null)
            {
                if (GetCampaign.lt_bookorderdetails.Value == true && ddlNumberOfCopies.SelectedValue != "")
                {
                    //Number of books ordered
                    rsvp.lt_numberofbooksordered = new CrmNumber();
                    rsvp.lt_numberofbooksordered.Value = Convert.ToInt32(ddlNumberOfCopies.SelectedValue);
                }

                if (GetCampaign.lt_bookorderdetails.Value == true && rblPostageOrCollect.SelectedValue != "")
                {
                    //Postage & packaging or collect
                    rsvp.lt_postageorcollection = new Picklist();
                    rsvp.lt_postageorcollection.Value = Convert.ToInt32(rblPostageOrCollect.SelectedValue);
                }
            }
            
            //Update bulk registration details in case bulk registration selected
            if (BulkRegistrationCheckBox.Checked && NumberOfAttendessInput.Text != "")
            {
                rsvp.lt_groupbulkregistration = new CrmBoolean();
                rsvp.lt_groupbulkregistration.Value = true;

                rsvp.lt_numberofattendees = new CrmNumber();
                rsvp.lt_numberofattendees.Value = Convert.ToInt32(NumberOfAttendessInput.Text);
            }            

            //Update number of attendees in case people attending selected
            if (GetCampaign.lt_peopleattending != null)
            {
                if (GetCampaign.lt_peopleattending.Value == true && ddlNumberOfPeopleDetails.SelectedValue != "")
                {
                    rsvp.lt_numberofattendees = new CrmNumber();
                    rsvp.lt_numberofattendees.Value = Convert.ToInt32(ddlNumberOfPeopleDetails.SelectedValue);                                        
                }
            }

            //Update Cocktail attendance for Alumni Refresher event.
            if(GetCampaign.lt_attendcocktailfunction != null)
            {
                if (CocktailCheckBox.Checked)
                {
                    rsvp.lt_attendingcocktailfunction = new CrmBoolean();
                    rsvp.lt_attendingcocktailfunction.Value = true;
                }
            }

            //Update InfoSessionSHL
            if (GetCampaign.lt_infosessionshl != null)
            {
                //Update Info Session details in case InfoSession is selected
                if (InfoSessionCheckBox.Checked)
                {
                    rsvp.lt_infosession = new CrmBoolean();
                    rsvp.lt_infosession.Value = true;
                }

                //Update SHL details in case SHL is selected
                if (SHLCheckBox.Checked)
                {
                    rsvp.lt_shl = new CrmBoolean();
                    rsvp.lt_shl.Value = true;
                }
            }

            //Write Lead GUID to campaign response lt_leadid
            rsvp.lt_leadid = new Lookup();
            rsvp.lt_leadid.Value = newLeadGuid;
            

            //Update payment received on crm to yes
            //rsvp.lt_paymentreceived = new CrmBoolean();
            //rsvp.lt_paymentreceived.Value = true;
                        
            if (GetCampaign.msa_costofadmission != null && GetCampaign.lt_paymentdetails != null)
            {
                if (GetCampaign.lt_paymentdetails.Value == true)
                {
                    rsvp.lt_epayreference = HttpContext.Current.Session["DateTime"].ToString();
                }
            }

            try
            {
                //Create the campaign response in CRM
                Guid compaignResponseGuid = _svc.Create(rsvp);
            }

            catch (System.Web.Services.Protocols.SoapException ex)
            {
                //Log error                
                LogFile(ex.Message, e.ToString());

                ClientScriptManager script = Page.ClientScript;
                script.RegisterClientScriptBlock(this.GetType(), "An error has occurred", "<script type=text/javascript>alert('" + errorMsg + "')</script>");
            }

            catch (Exception ex)
            {
                //Log error
                LogFile(ex.Message, e.ToString());

                ClientScriptManager script = Page.ClientScript;
                script.RegisterClientScriptBlock(this.GetType(), "An error has occurred", "<script type=text/javascript>alert('" + errorMsg + "')</script>");
            }
        }
    }
        
    protected void btnCreditCardPayment_Click(object sender, EventArgs e)
    {
        initialiseCRMService();

        plhBookingForm.Visible = false;
        plhPaymentForm.Visible = false;
        plhBookingConfirm.Visible = false;
                      
        //Set out chars to strip from the start of string       
        string TrimmedEventCostOutput = lblEventCostOutput.Text.Remove(0, 2);
        
        //Redirect to payment portal
        //Test Link
        //Response.Redirect("http://v240-02.sun.ac.za:7784/ePay/processPayment.jsp?APP_CODE=" + AppCodeOut + "&APP_AMOUNT=" + TrimmedEventCostOutput + "&REF_NUMBER=" + HttpContext.Current.Session["DateTime"].ToString() + "&EMAIL=" + EmailInput.Text + "&EVENT_DESCR=" + EventName.Text);
        
        ////Leader's Angle Link
        //Response.Redirect("http://t2000-05.sun.ac.za/ePay/processPayment.jsp?APP_CODE=USBEVENTS&APP_AMOUNT=" + TrimmedEventCostOutput + "&REF_NUMBER=" + HttpContext.Current.Session["DateTime"].ToString() + "&EMAIL=" + EmailInput.Text + "&EVENT_DESCR=" + EventName.Text);        
        
        ////Production Link
        Response.Redirect("http://t2000-05.sun.ac.za/ePay/processPayment.jsp?APP_CODE=" + AppCodeOut + "&APP_AMOUNT=" + TrimmedEventCostOutput + "&REF_NUMBER=" + HttpContext.Current.Session["DateTime"].ToString() + "&EMAIL=" + EmailInput.Text + "&EVENT_DESCR=" + EventName.Text);
          
        //Test Dev links
        //Response.Redirect("http://v240-02.sun.ac.za:7784/ePay/processPayment.jsp?APP_CODE=" + AppCodeOut + "&APP_AMOUNT=" + TrimmedEventCostOutput + "&REF_NUMBER=" + HttpContext.Current.Session["DateTime"].ToString() + "&EMAIL=" + EmailInput.Text + "&EVENT_DESCR=" + EventName.Text);        
        //Response.Redirect("http://v240-02.sun.ac.za:7785/ePay/processPayment.jsp?APP_CODE=" + AppCodeOut + "&APP_AMOUNT=" + TrimmedEventCostOutput + "&REF_NUMBER=" + HttpContext.Current.Session["DateTime"].ToString() + "&EMAIL=" + EmailInput.Text + "&EVENT_DESCR=" + EventName.Text);        

        //if (BookOrder == true)
        //{
        //    //open new window
        //    string popup = "http://www.usb.ac.za/AboutUs/usb50prospectus-thankyou.aspx";

        //    ClientScript.RegisterStartupScript(this.GetType(), "OpenWin", "<script>openNewWin('" + popup + "')</script>");
        //}
    }

    protected void btnPayLater_Click(object sender, EventArgs e)
    {
        plhBookingForm.Visible = false;
        plhPayLaterRef.Visible = true;

        lblEventNameOutput.Text = EventName.Text;
        lblUserNameOutput.Text = NameInput.Text;
        lblUserSurnameOutput.Text = SurnameInput.Text;
        lblUserCompanyOutput.Text = CompanyInput.Text;
        lblUserContactNumOutput.Text = MobileNoInput.Text;
        lblUserEmailOutput.Text = EmailInput.Text;
        lblPayLaterRefOutput.Text = HttpContext.Current.Session["DateTime"].ToString();
        
        
        //if (BookOrder == true)
        //{
        //    lblPayLaterRefHeading.Text = "Book order:";

        //    //open new window
        //    string popup = "http://www.usb.ac.za/AboutUs/usb50prospectus-thankyou.aspx";

        //    ClientScript.RegisterStartupScript(this.GetType(), "OpenWin", "<script>openNewWin('" + popup + "')</script>");
        //}
        
            //string url = Request.QueryString["iID"];

            //if (url == "1579bbb7-3b74-e211-8b85-005056b82153" || url == "550a9828-3b74-e211-8b85-005056b82153" || url == "54302937-2c74-e211-8b85-005056b82153")
            //{
            //    //Generate SBA TAXI Fare Ref-no
            //    lblPayLaterRefOutput.Text = "SBA12575-5373";
            //}
            //else
            //{
            //    //Generate Random Ref-no
                
            //}
    }

}
