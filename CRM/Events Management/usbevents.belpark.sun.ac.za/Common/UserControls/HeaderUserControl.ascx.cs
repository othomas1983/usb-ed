﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;

public partial class Common_UserControls_HeaderUserControl : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
              
    }

    public void getActiveDomain()
    {
        string strCurrentImageURL = "";
        string strDomainName = "";
        strDomainName = System.Environment.MachineName.ToString().ToLower();
        if (!strDomainName.Contains("://"))
            strDomainName = "http://" + strDomainName + "/";

        strCurrentImageURL = ConfigurationSettings.AppSettings["UserImageURL"].ToString();

        if (strDomainName != strCurrentImageURL)
        {
            strCurrentImageURL = strDomainName;
        }
    }

   
}
