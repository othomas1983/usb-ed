﻿#region *** Using Directives ***
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.ServiceModel.Syndication;
using System.Xml;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Security;
using CrmWsdl;
#endregion

public partial class Common_UserControls_USBEventsFeed : System.Web.UI.UserControl
{
    #region *** Page_Load ***
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack || !Page.IsCallback)
        {
            SetupUI();
        }
    }
    #endregion

    #region *** SetupUI() ***
    public void SetupUI()
    {

        Response.Clear();
        Response.ContentType = "text/xml";
        XmlTextWriter objX = new XmlTextWriter(Response.OutputStream, Encoding.UTF8);

        objX.WriteStartDocument();
        objX.WriteStartElement("rss");
        objX.WriteAttributeString("version", "2.0");

        objX.WriteStartElement("channel");
        objX.WriteElementString("title", "RSS Feed for the USB Events Calendar");
        objX.WriteElementString("link", ConfigurationManager.AppSettings["UserImageURL"].ToString() + "Common/UserControls/USBEventsFeed.aspx");
        objX.WriteElementString("description", "USB upcoming events RSS Feed.");
        objX.WriteElementString("copyright", "(c) 2009 Copyright information");
        objX.WriteElementString("ttl", "720");

        DateTime dt;
        string sCurrentYear = "";
        string sCurrentMonth = "";

        CrmWsdl.CrmService _srv = new CrmWsdl.CrmService();

        CrmAuthenticationToken token = new CrmAuthenticationToken();
        token.AuthenticationType = 0;
        token.OrganizationName = "US-BPC";

        _srv.CrmAuthenticationTokenValue = token;
        _srv.PreAuthenticate = true;
        _srv.Credentials = System.Net.CredentialCache.DefaultCredentials;

        try
        {

            //Retrieve all active (launched) campaigns
            QueryByAttribute query = new QueryByAttribute();
            query.ColumnSet = new AllColumns();
            query.EntityName = EntityName.campaign.ToString();

            //Set the order of returned campaigns
            OrderExpression oe = new OrderExpression();
            oe.AttributeName = "actualstart";
            oe.OrderType = OrderType.Ascending;

            query.Attributes = new string[] { "statuscode" };
            query.Values = new String[] { "2" };
            query.Orders = new OrderExpression[] { oe };

            BusinessEntityCollection retrieved = _srv.RetrieveMultiple(query);

            foreach (campaign activeCampaigns in retrieved.BusinessEntities)
            {
                if (activeCampaigns.owningbusinessunit.Value.ToString() == "c1d531a7-232b-de11-98d9-00155d610f1a")
                {
                    if (activeCampaigns.msa_startdatetime.Value.ToString() != "")
                    {
                        if (DateTime.Parse(activeCampaigns.msa_startdatetime.Value) > DateTime.Parse(DateTime.Now.ToString("s")))
                        {
                            objX.WriteStartElement("item");


                            if (activeCampaigns.msa_startdatetime.Value.ToString() != "")
                            {

                                dt = Convert.ToDateTime(activeCampaigns.msa_startdatetime.Value.ToString());
                                string sDay = dt.ToString("dd");
                                sCurrentYear = dt.ToString("yyyy");
                                sCurrentMonth = dt.ToString("MMMM");

                                objX.WriteStartElement("EventStartDate");
                                objX.WriteCData(sDay + " " + sCurrentMonth + " " + sCurrentYear);
                                objX.WriteEndElement();

                            }


                            if (activeCampaigns.name.ToString() != "")
                            {

                                objX.WriteStartElement("title");
                                objX.WriteCData(activeCampaigns.name.ToString().Trim());
                                objX.WriteEndElement();

                            }


                            if (activeCampaigns.msa_location != null)
                            {
                                objX.WriteStartElement("description");
                                objX.WriteCData(activeCampaigns.msa_location.ToString().Trim());
                                objX.WriteEndElement();
                            }
                            else
                            {
                                objX.WriteStartElement("description");
                                objX.WriteCData("Venue not confirmed yet.");
                                objX.WriteEndElement();
                            }


                            objX.WriteStartElement("link");
                            objX.WriteCData(ConfigurationManager.AppSettings["UserImageURL"].ToString() + "NewsAndEvents/EventItem.aspx?EventItem=" + activeCampaigns.campaignid.Value.ToString() + "&RefFunctionality=events");
                            objX.WriteEndElement();

                            objX.WriteElementString("pubDate", DateTime.Now.ToString("s"));
                            objX.WriteEndElement();
                        }
                    }
                }
            }
            _srv.Dispose();
        }
        finally
        {
            _srv.Dispose();
            objX.WriteEndElement();
            objX.WriteEndElement();
            objX.WriteEndDocument();
            objX.Flush();
            objX.Close();
            Response.End();
        }
    }
    #endregion
}
