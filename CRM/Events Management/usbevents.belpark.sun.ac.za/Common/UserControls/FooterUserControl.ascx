﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FooterUserControl.ascx.cs" Inherits="Common_UserControls_FooterUserControl" %>

<img src="/Common/Images/bottombarNew.jpg" alt="USB Sponsors" usemap="#SponsorsMap" style="border: none;"/>
<map id="SponsorsMap" name="SponsorsMap">
    <area shape="rect" coords="6, 49, 106, 95" href="http://www.absa.co.za/" target="_blank" 
        alt="Absa" />
    <area shape="rect" coords="117, 12, 306, 87" href="http://www.usb-ed.com/" target="_blank" 
        alt="USB-ED" />
    <area shape="rect" coords="344, 11, 526, 87" href="http://www.sun.ac.za/" target="_blank" 
        alt="University of Stellenbosch" />
    <area shape="rect" coords="557, 10, 671, 89" 
        href="http://www.beyondgreypinstripes.org/" target="_blank" 
        alt="Beyond Grey Pin Stripes" />
    <area shape="rect" coords="703, 16, 820, 88" href="http://www.eduniversal.com/" target="_blank" 
        alt="Eduniversal" />
    <area shape="rect" coords="837, 16, 921, 96" href="http://www.aacsb.edu/" target="_blank" 
        alt="AACSB" />
    <area shape="rect" coords="931, 22, 1019, 95" href="http://www.efmd.org/" target="_blank" 
        alt="EFMD" />
    <area shape="rect" coords="8, 105, 108, 146" href="http://www.distell.co.za/" target="_blank" 
        alt="Distell" />
    <area shape="rect" coords="118, 93, 232, 167" href="http://www.aabschools.com/" target="_blank" 
        alt="Association of African Business Schools" />
    <area shape="rect" coords="236, 94, 342, 167" href="http://www.ceeman.org/" target="_blank" 
        alt="Ceeman" />
    <area shape="rect" coords="346, 95, 440, 168" href="http://www.gbsnonline.org/" target="_blank" 
        alt="GBSN" />
    <area shape="rect" coords="445, 95, 588, 167" href="http://www.grli.org/" target="_blank" 
        alt="Globally Responsible Leadership Initiative" />
    <area shape="rect" coords="598, 109, 710, 164" href="http://www.unprme.org/" target="_blank" 
        alt="Prime" />
    <area shape="rect" coords="716, 107, 820, 159" href="http://www.sabsa.co.za/" target="_blank" 
        alt="SABSA" />
    <area shape="rect" coords="846, 103, 1007, 165" href="http://www.mbaworld.com/" target="_blank" 
        alt="MBA World" />
</map>

<!-- Start phpmyvisites -->
<a href="http://www.phpmyvisites.us/" title="Free web analytics, website statistics" 
onclick="window.open(this.href);return(false);" style="color: #231F20;"><script type="text/javascript">
<!--
    var a_vars = Array();
    var pagename = '';

    var phpmyvisitesSite = 47;
    var phpmyvisitesURL = "http://webstats.sun.ac.za/phpmyvisites.php";
//-->
</script>
<script language="javascript" src="http://webstats.sun.ac.za/phpmyvisites.js" type="text/javascript"></script>
<object><noscript><p>Free web analytics, website statistics
<img src="http://webstats.sun.ac.za/phpmyvisites.php" alt="Statistics" style="border:0" />
</p></noscript></object></a>
<!-- End phpmyvisites --> 
