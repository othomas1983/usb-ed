﻿#region *** Using Directives ***
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.ServiceModel.Syndication;
using System.Xml;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Security;
#endregion

public partial class Common_UserControls_USBNewsroomFeed : System.Web.UI.UserControl
{
    #region *** Page_Load ***
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack || !Page.IsCallback)
        {
            SetupUI();
        }
    }
    #endregion

    #region *** SetupUI() ***
    public void SetupUI()
    {

        Response.Clear();
        Response.ContentType = "text/xml";
        XmlTextWriter objX = new XmlTextWriter(Response.OutputStream, Encoding.UTF8);

        objX.WriteStartDocument();
        objX.WriteStartElement("rss");
        objX.WriteAttributeString("version", "2.0");

        objX.WriteStartElement("channel");
        objX.WriteElementString("title", "RSS Feed for the USB Newsroom Articles");
        objX.WriteElementString("link", ConfigurationManager.AppSettings["UserImageURL"].ToString() + "Common/UserControls/USBNewsroomFeed.aspx");
        objX.WriteElementString("description", "USB Newsroom.");
        objX.WriteElementString("copyright", "(c) 2009 Copyright information");
        objX.WriteElementString("ttl", "720");

        SqlDataReader rdr;
        SqlConnection conn = null;
        DateTime dt;        

        try
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["USBWebDB"].ConnectionString);
            conn.Open();

            // 1.  create a command object identifying
            //     the stored procedure                    
            SqlCommand cmd = new SqlCommand("sp_GetActiveNewLetterNewsItemsEngAndAfrNew", conn);

            // 2. set the command object so it knows
            //    to execute a stored procedure
            cmd.CommandType = CommandType.StoredProcedure;

            // execute the command
            rdr = cmd.ExecuteReader();            

            if (rdr.HasRows)
            {
                while (rdr.Read())
                {
                    objX.WriteStartElement("item");

                    if (rdr["sArticleTitle"].ToString() != "")
                    {
                        string sHTMLTitle, sTextTitle;
                        
                        sHTMLTitle = rdr["sArticleTitle"].ToString().Trim();
                        sTextTitle = System.Text.RegularExpressions.Regex.Replace(sHTMLTitle, @"<(.|\n)*?>", string.Empty);
      
                        objX.WriteStartElement("title"); 
                        objX.WriteCData(sTextTitle);
                        objX.WriteEndElement();
                    }

                    if (rdr["iArticleID"].ToString() != "")
                    {
                        objX.WriteStartElement("link");
                        objX.WriteCData("http://www.usb.ac.za/NewsAndEvents/FullNewsItem.aspx?NewsItem=" + rdr["iArticleID"].ToString().Trim() + "&RefFunctionality=inthenews");
                        objX.WriteEndElement();
                    }

                    if (rdr["sArticleAbstract"].ToString() != "")
                    {
                        objX.WriteStartElement("description"); 
                        objX.WriteCData(rdr["sArticleAbstract"].ToString().Trim());
                        objX.WriteEndElement();
                    }

                    if (rdr["dtDateSent"].ToString() != "")
                    {
                        dt = Convert.ToDateTime(rdr["dtDateSent"].ToString());
                        objX.WriteStartElement("pubDate");
                        objX.WriteCData(dt.ToString("r"));
                        objX.WriteEndElement();
                    }

                    objX.WriteEndElement();
                }
            }
            
            cmd.Dispose();
        }

        finally
        {        
            objX.WriteEndElement();
            objX.WriteEndDocument();
            objX.Flush();
            objX.Close();
            Response.End();
        }
    }
    #endregion
}
