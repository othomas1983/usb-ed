﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="HeaderUserControl.ascx.cs" Inherits="Common_UserControls_HeaderUserControl" %>

<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-2859690-1']);
    _gaq.push(['_trackPageview']);

    (function () {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>

<div id="logo">
    <table id="Table_01" width="1024" height="125" border="0" cellpadding="0" cellspacing="0">
	    <tr>
		    <td rowspan="3" valign="top"><a href="<%=ConfigurationSettings.AppSettings["UserImageURL"]%>Default.aspx" onclick="_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'Homepage - TopNav']);"><img src="<%=ConfigurationSettings.AppSettings["UserImageURL"]%>Common/Images/header_01.jpg" width="596" height="124" alt="" /></a></td>
		    <td valign="top"><a href="<%=ConfigurationSettings.AppSettings["UserImageURL"]%>Default.aspx" onclick="_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'Homepage - TopNav2']);"><img src="<%=ConfigurationSettings.AppSettings["UserImageURL"]%>Common/Images/header_02.jpg" width="72" height="32" alt="" /></a></td>
		    <td colspan="2" valign="top"><a href="../../AboutUs/Search.aspx" onclick="_gaq.push(['_trackEvent', 'External Links', 'Click', 'Search - TopNav']);"><img src="<%=ConfigurationSettings.AppSettings["UserImageURL"]%>Common/Images/header_03.jpg" width="78" height="32" alt="" /></a></td>
		    <td valign="top"><a href="<%=ConfigurationSettings.AppSettings["UserImageURL"]%>AboutUs/ContactUs.aspx" onclick="_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'Contact Us - TopNav']);"><img src="<%=ConfigurationSettings.AppSettings["UserImageURL"]%>Common/Images/header_04.jpg" width="94" height="32" alt="" /></a></td>
		    <td valign="top"><a href="<%=ConfigurationSettings.AppSettings["UserImageURL"]%>AboutUs/DirectionsToCampus.aspx" onclick="_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'DirectionsToCampus - TopNav']);"><img src="<%=ConfigurationSettings.AppSettings["UserImageURL"]%>Common/Images/header_05.jpg" width="140" height="32" alt="" /></a></td>
		    <td valign="top"><img src="<%=ConfigurationSettings.AppSettings["UserImageURL"]%>Common/Images/header_06.jpg" width="44" height="32" alt="" /></td>
	    </tr>
	    <tr>
		    <td colspan="6" height="40" valign="top"></td>
	    </tr>
	    <tr>
		    <td colspan="4" height="52" valign="top"><p class="header_font">USB Communities: <br /><a href="http://my.usb.ac.za/" target="_blank" class="header" onclick="_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'Students - TopNav']);">Students</a> | <a href="<%=ConfigurationSettings.AppSettings["UserImageURL"]%>AboutUs/alumni.aspx" target="_blank" class="header" onclick="_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'Alumni - TopNav']);">Alumni</a> | <a href="https://bpcmoss02.belpark.sun.ac.za/" target="_blank" class="header" onclick="_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'Staff - TopNav']);">Staff</a> </p></td>	
             <td rowspan="2" height="80" valign="top"><a href="AboutUs/History.aspx"><img src="<%=ConfigurationSettings.AppSettings["UserImageURL"]%>Common/Images/50yrweb.jpg" width="80" height="80" align="right" alt="" style="margin-top:-37px;"/></a></td>	
	    </tr>
	    <tr>
		    <td valign="top"><img src="<%=ConfigurationSettings.AppSettings["UserImageURL"]%>Common/Images/header-spacer.gif" width="596" height="1" alt="" /></td>
		    <td valign="top"><img src="<%=ConfigurationSettings.AppSettings["UserImageURL"]%>Common/Images/header-spacer.gif" width="72" height="1" alt="" /></td>
		    <td valign="top"><img src="<%=ConfigurationSettings.AppSettings["UserImageURL"]%>Common/Images/header-spacer.gif" width="39" height="1" alt="" /></td>
		    <td valign="top"><img src="<%=ConfigurationSettings.AppSettings["UserImageURL"]%>Common/Images/header-spacer.gif" width="39" height="1" alt="" /></td>
		    <td valign="top"><img src="<%=ConfigurationSettings.AppSettings["UserImageURL"]%>Common/Images/header-spacer.gif" width="94" height="1" alt="" /></td>
		    <td valign="top"><img src="<%=ConfigurationSettings.AppSettings["UserImageURL"]%>Common/Images/header-spacer.gif" width="140" height="1" alt="" /></td>
		    <td valign="top"><img src="<%=ConfigurationSettings.AppSettings["UserImageURL"]%>Common/Images/header-spacer.gif" width="44" height="1" alt="" /></td>
	    </tr>
    </table>
</div>
<div id="menu">
	<script type="text/javascript" src="<%=ConfigurationSettings.AppSettings["UserImageURL"]%>Common/Scripts/usb_top_menu.js"></script>
</div>