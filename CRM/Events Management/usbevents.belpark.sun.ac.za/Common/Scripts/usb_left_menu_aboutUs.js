﻿//fixMozillaZIndex = true; //Fixes Z-Index problem with Mozilla browsers but causes odd scrolling problem, toggle to see if it helps
_menuCloseDelay = 800;
_menuOpenDelay = 100;
_subOffsetTop = 0;
_subOffsetLeft = 0;
var UserImageURL = "http://" + top.location.host + "/";

with (styMainMenu = new mm_style()) {
    styleid = 1;
    bordercolor = "#5E5A5B";
    borderstyle = "solid";
    borderwidth = 1;
    fontfamily = "Verdana";
    fontsize = "10pt";
    fontstyle = "normal";
    fontweight = "normal";
    headercolor = "#000000";
    offbgcolor = "#E7D0D7";
    offcolor = "#333333";
    onborder = "1px solid #FFFFFF";
    onbgcolor = "#891536";
    oncolor = "#FFFFFF";
    onsubimage = UserImageURL + "Common/Images/menu-white-arrow.gif";
    pagebgcolor = "#95A3A3";
    pagecolor = "#FFFFFF";
    outfilter = "fade(duration=0.5)";
    padding = 5;
    separatorcolor = "#FFFFFF";
    separatorsize = 1;
    subimage = UserImageURL + "Common/Images/menu-grey-arrow.gif";
    subimagepadding = 4;
}

with (stySubMainMenu = new mm_style()) {
    styleid = 1;
    bordercolor = "#5E5A5B";
    borderstyle = "solid";
    borderwidth = 1;
    fontfamily = "Verdana";
    fontsize = "8pt";
    fontstyle = "normal";
    fontweight = "normal";
    headercolor = "#000000";
    offbgcolor = "#E7D0D7";
    offcolor = "#333333";
    onborder = "1px solid #FFFFFF";
    onbgcolor = "#891536";
    oncolor = "#FFFFFF";
    onsubimage = UserImageURL + "Common/Images/menu-white-arrow.gif";
    pagebgcolor = "#95A3A3";
    pagecolor = "#FFFFFF";
    outfilter = "fade(duration=0.5)";
    padding = 5;
    separatorcolor = "#FFFFFF";
    separatorsize = 1;
    subimage = UserImageURL + "Common/Images/menu-grey-arrow.gif";
    subimagepadding = 4;
}

with (mnuDiscoverSubmenu = new menuname("DiscoverSubmenu")) {
    style = styMainMenu;
    alwaysvisible = 1;
    position = "relative";
    align = "bottom";
    itemwidth = "100%";
    menuwidth = "100%";

    aI("text=Celebrating 50 years;url=" + UserImageURL + "AboutUs/History.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'Celebrating - SideNav-AboutUs']);");
    aI("text=Discover USB;showmenu=DiscoverSubmenuAbout;url=" + UserImageURL + "AboutUs/DiscoverUSB.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'DiscoverUSB - SideNav-AboutUs']);");
    aI("text=Accreditations and rankings;url=" + UserImageURL + "AboutUs/AccreditationsAndRatings.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'Accreditation - SideNav-AboutUs']);");
    aI("text=Academic offering;url=" + UserImageURL + "AboutUs/AcademicOffering.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'AcademicOffering - SideNav-AboutUs']);");
    aI("text=USB Alumni;showmenu=usbalumniSubmenuAbout;url=" + UserImageURL + "AboutUs/alumni.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'DiscoverUSB - SideNav-AboutUs']);");

    aI("text=Social engagement;showmenu=DiscoverSubmenuInvestment;url=" + UserImageURL + "AboutUs/SocialInvestment.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'SocialInvestment - SideNav-AboutUs']);");
    aI("text=Small Business Academy;url=" + UserImageURL + "AboutUs/SmallBusinessAcademy.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'SmallBusinessAcademy - SideNav-AboutUs']);");
    aI("text=Career Services;showmenu=DiscoverSubmenuCareers;url=" + UserImageURL + "AboutUs/CareerServices/Default.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'CareerServices - SideNav-AboutUs']);");    
    aI("text=Campus and facilities;url=" + UserImageURL + "AboutUs/CampusAndFacilities.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'CampusAndFacilities - SideNav-AboutUs']);");
    aI("text=Directions to campus;url=" + UserImageURL + "AboutUs/DirectionsToCampus.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'DirectionsToCampus - SideNav-AboutUs']);");
    aI("text=Elephant campaign;url=" + UserImageURL + "AboutUs/ElephantCampaign.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'ElephantCampaign - SideNav-AboutUs']);");
    aI("text=Contact us;url=" + UserImageURL + "AboutUs/ContactUs.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'ContactUs - SideNav-AboutUs']);");
}

with (mnuDiscoverSubmenu = new menuname("usbalumniSubmenuAbout")) {
    style = stySubMainMenu;
    overflow = "scroll";
    itemwidth = "120%";
    menuwidth = "175";

    aI("text=Take part in regional activities;url=" + UserImageURL + "AboutUs/alumni_governingbody.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'VisionAndMission - SideNav-AboutUs']);");
    //aI("text=Director's message;url=" + UserImageURL + "AboutUs/DirectorsMessage.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'DirectorsMessage - SideNav-AboutUs']);");
    aI("text=Events and USB ThoughtPrint;url=" + UserImageURL + "AboutUs/alumni_events.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'History - SideNav-AboutUs']);");
    //aI("text=Access to jobs;url=" + UserImageURL + "Degrees/MBADegree/alumni_events.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'History - SideNav-AboutUs']);");
    aI("text=Alumni news;url=" + UserImageURL + "AboutUs/alumni_news.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'History - SideNav-AboutUs']);");
    aI("text=Giving back to USB;url=" + UserImageURL + "AboutUs/alumni_givingback.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'History - SideNav-AboutUs']);");
    aI("text=Talk to us / Update your details;url=" + UserImageURL + "AboutUs/alumni.aspx#talk; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'History - SideNav-AboutUs']);");
}

with (mnuDiscoverSubmenu = new menuname("DiscoverSubmenuAbout")) {
    style = stySubMainMenu;
    overflow = "scroll";
    itemwidth = "100%";
    menuwidth = "175";

    aI("text=Mission & Vision;url=" + UserImageURL + "AboutUs/VisionAndMission.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'VisionAndMission - SideNav-AboutUs']);");
    //aI("text=Director's message;url=" + UserImageURL + "AboutUs/DirectorsMessage.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'DirectorsMessage - SideNav-AboutUs']);");
    aI("text=History;url=" + UserImageURL + "AboutUs/History.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'History - SideNav-AboutUs']);");
}

with (mnuDiscoverSubmenu = new menuname("DiscoverSubmenuInvestment")) {
    style = stySubMainMenu;
    overflow = "scroll";
    itemwidth = "100%";
    menuwidth = "175";

    
    aI("text=Programme for NPOs;url=" + UserImageURL + "AboutUs/NpoInitiative.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'NpoInitiative - SideNav-AboutUs']);");
    aI("text=Gala Dinner;url=" + UserImageURL + "AboutUs/GalaDinner.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'GalaDinner - SideNav-AboutUs']);");    
    aI("text=Busary Fund;url=" + UserImageURL + "AboutUs/FutureFund.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'FutureFund - SideNav-AboutUs']);");    
}

with (mnuDiscoverSubmenu = new menuname("DiscoverSubmenuCareers")) {
    style = stySubMainMenu;
    overflow = "scroll";
    itemwidth = "100%";
    menuwidth = "175";

    aI("text=Launch your career;url=" + UserImageURL + "AboutUs/CareerServices/LaunchYourCareer.aspx;");
    aI("text=Career strategy session;url=" + UserImageURL + "AboutUs/CareerServices/CareerStrategySessions.aspx;");
    aI("text=Preparation sessions for job interviews;url=" + UserImageURL + "AboutUs/CareerServices/PreparationSessionsJobInterviews.aspx;");
    aI("text=Job portal ;url=" + UserImageURL + "AboutUs/CareerServices/JobPortal/Default.aspx;");
    aI("text=Resources;url=" + UserImageURL + "AboutUs/CareerServices/Resources.aspx;");
    aI("text=Coaching, mentoring and internships;url=" + UserImageURL + "AboutUs/CareerServices/CoachingMentoringAndInternships.aspx;");
    aI("text=Contact us;url=" + UserImageURL + "AboutUs/CareerServices/ContactCareerServices.aspx;");
}


drawMenus();