﻿//fixMozillaZIndex = true; //Fixes Z-Index problem with Mozilla browsers but causes odd scrolling problem, toggle to see if it helps
_menuCloseDelay = 800;
_menuOpenDelay = 100;
_subOffsetTop = 0;
_subOffsetLeft = 0;
var UserImageURL = "http://" + top.location.host + "/";

with (styMainMenu = new mm_style()) {
    styleid = 1;
    bordercolor = "#5E5A5B";
    borderstyle = "solid";
    borderwidth = 1;
    fontfamily = "Verdana";
    fontsize = "10pt";
    fontstyle = "normal";
    fontweight = "normal";
    headercolor = "#000000";
    offbgcolor = "#E7D0D7";
    offcolor = "#333333";
    onborder = "1px solid #FFFFFF";
    onbgcolor = "#891536";
    oncolor = "#FFFFFF";
    onsubimage = UserImageURL + "Common/Images/menu-white-arrow.gif";
    pagebgcolor = "#95A3A3";
    pagecolor = "#FFFFFF";
    outfilter = "fade(duration=0.5)";
    padding = 5;
    separatorcolor = "#FFFFFF";
    separatorsize = 1;
    subimage = UserImageURL + "Common/Images/menu-grey-arrow.gif";
    subimagepadding = 4;
}

with (stySubMainMenu = new mm_style()) {
    styleid = 1;
    bordercolor = "#5E5A5B";
    borderstyle = "solid";
    borderwidth = 1;
    fontfamily = "Verdana";
    fontsize = "8pt";
    fontstyle = "normal";
    fontweight = "normal";
    headercolor = "#000000";
    offbgcolor = "#E7D0D7";
    offcolor = "#333333";
    onborder = "1px solid #FFFFFF";
    onbgcolor = "#891536";
    oncolor = "#FFFFFF";
    onsubimage = UserImageURL + "Common/Images/menu-white-arrow.gif";
    pagebgcolor = "#95A3A3";
    pagecolor = "#FFFFFF";
    outfilter = "fade(duration=0.5)";
    padding = 5;
    separatorcolor = "#FFFFFF";
    separatorsize = 1;
    subimage = UserImageURL + "Common/Images/menu-grey-arrow.gif";
    subimagepadding = 4;
}

with (mnuPublicationsSubmenu = new menuname("PublicationsSubmenu")) {
    style = styMainMenu;
    alwaysvisible = 1;
    position = "relative";
    align = "bottom";
    itemwidth = "100%";
    menuwidth = "100%";

    aI("text=Overview;url=" + UserImageURL + "Publications/PublicationsOverview.aspx;");
    aI("text=USB Agenda magazine;showmenu=PublicationsSubmenuAgenda;url=http://thoughtprint.usb.ac.za/Pages/Agenda.aspx;");
    aI("text=USB Leaders' Lab journal;showmenu=PublicationsSubmenuLeadersLab;url=" + UserImageURL + "Publications/UsbLeadersLabJournal/LeadersLabJournal.aspx;");    
    aI("text=New thinking @ USB;url=" + UserImageURL + "Publications/NewThinking@Usb.aspx;");
    aI("text=Multimedia;url=" + UserImageURL + "Publications/Multimedia/Default.aspx;");    
}

with (mnuPublicationsSubmenu = new menuname("PublicationsSubmenuAgenda")) {
    style = stySubMainMenu;
    overflow = "scroll";
    itemwidth = "100%";
    menuwidth = "175";

    aI("text=Back issues;url=" + UserImageURL + "Publications/UsbAgendaMagazine/BackIssues.aspx;");
    aI("text=Subscribe;url=" + UserImageURL + "Publications/UsbAgendaMagazine/Subscribe.aspx;");

}

with (mnuPublicationsSubmenu = new menuname("PublicationsSubmenuLeadersLab")) {
    style = stySubMainMenu;
    overflow = "scroll";
    itemwidth = "100%";
    menuwidth = "175";

    aI("text=Back issues;url=" + UserImageURL + "Publications/UsbLeadersLabJournal/BackIssues.aspx;");
    aI("text=Subscribe;url=" + UserImageURL + "Publications/UsbLeadersLabJournal/Subscribe.aspx;");
    //aI("text=Advertise in USB Leaders' Lab;url=" + UserImageURL + "Publications/UsbLeadersLabJournal/Advertise.aspx;");
}

drawMenus();