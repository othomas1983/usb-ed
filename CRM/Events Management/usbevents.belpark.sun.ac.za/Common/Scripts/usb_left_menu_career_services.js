﻿//fixMozillaZIndex = true; //Fixes Z-Index problem with Mozilla browsers but causes odd scrolling problem, toggle to see if it helps
_menuCloseDelay = 800;
_menuOpenDelay = 100;
_subOffsetTop = 0;
_subOffsetLeft = 0;
var UserImageURL = "http://" + top.location.host + "/";

with (styMainMenu = new mm_style()) {
    styleid = 1;
    bordercolor = "#5E5A5B";
    borderstyle = "solid";
    borderwidth = 1;
    fontfamily = "Verdana";
    fontsize = "10pt";
    fontstyle = "normal";
    fontweight = "normal";
    headercolor = "#000000";
    offbgcolor = "#E7D0D7";
    offcolor = "#333333";
    onborder = "1px solid #FFFFFF";
    onbgcolor = "#891536";
    oncolor = "#FFFFFF";
    onsubimage = UserImageURL + "Common/Images/menu-white-arrow.gif";
    pagebgcolor = "#95A3A3";
    pagecolor = "#FFFFFF";
    outfilter = "fade(duration=0.5)";
    padding = 5;
    separatorcolor = "#FFFFFF";
    separatorsize = 1;
    subimage = UserImageURL + "Common/Images/menu-grey-arrow.gif";
    subimagepadding = 4;
}

with (stySubMainMenu = new mm_style()) {
    styleid = 1;
    bordercolor = "#5E5A5B";
    borderstyle = "solid";
    borderwidth = 1;
    fontfamily = "Verdana";
    fontsize = "8pt";
    fontstyle = "normal";
    fontweight = "normal";
    headercolor = "#000000";
    offbgcolor = "#E7D0D7";
    offcolor = "#333333";
    onborder = "1px solid #FFFFFF";
    onbgcolor = "#891536";
    oncolor = "#FFFFFF";
    onsubimage = UserImageURL + "Common/Images/menu-white-arrow.gif";
    pagebgcolor = "#95A3A3";
    pagecolor = "#FFFFFF";
    outfilter = "fade(duration=0.5)";
    padding = 5;
    separatorcolor = "#FFFFFF";
    separatorsize = 1;
    subimage = UserImageURL + "Common/Images/menu-grey-arrow.gif";
    subimagepadding = 4;
}

with (mnuDiscoverSubmenu = new menuname("DiscoverSubmenu")) {
    style = styMainMenu;
    alwaysvisible = 1;
    position = "relative";
    align = "bottom";
    itemwidth = "100%";
    menuwidth = "100%";

    aI("text=Career Services;url=" + UserImageURL + "AboutUs/career-services/usb-career-services.aspx;");
    aI("text=Launch your career;url=" + UserImageURL + "AboutUs/career-services/launch-your-career.aspx;");
    aI("text=Career strategy session;url=" + UserImageURL + "AboutUs/career-services/career-strategy-sessions.aspx;");
    aI("text=Preparation sessions for job interviews;url=" + UserImageURL + "AboutUs/career-services/preparation-sessions-for-job-interviews.aspx;");
    aI("text=Job portal ;url=" + UserImageURL + "AboutUs/career-services/job-portal/career-services-job-portal.aspx;");
    aI("text=Resources;url=" + UserImageURL + "AboutUs/career-services/career-resources.aspx;;");
    aI("text=Coaching, mentoring and internships;url=" + UserImageURL + "AboutUs/career-services/coaching-mentoring-and-internships.aspx;");
    aI("text=Contact us;url=" + UserImageURL + "AboutUs/career-services/contact-career-services.aspx;");
}

drawMenus();