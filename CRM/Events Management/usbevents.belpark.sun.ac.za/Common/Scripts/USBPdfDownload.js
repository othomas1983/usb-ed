// JavaScript source code

    function JavaScriptFunction() {
        var JavaScriptVar = "AFR";
        document.getElementById('<%= hide1.ClientID %>').value = JavaScriptVar;
    }


    function ScriptIndiaBrochure() {
        var JavaScriptVar = "India";
        document.getElementById('<%= hide1.ClientID %>').value = JavaScriptVar;
    }

    function ScriptAfricaBrochure() {
        var JavaScriptVar = "Africa";
        document.getElementById('<%= hide1.ClientID %>').value = JavaScriptVar;
    }

    $(function () {
        $('[id*=ddCountry]').on('change', function () {
            if ($('[id*=ddCountry]').val() != "ZA") {
                $('[id*=ddProvince]').attr('disabled', true);
            }
            else {
                $('[id*=ddProvince]').attr('disabled', false);
            }
        });
    });

    function clear() {
        $('#txtName').val("");
        $('#txtSurname').val("");
        $('#txtEmail').val("");
        $('#ErrorlistSummary').empty();

    }


    $(function () {
        $("[id*=btnModalPopup]").live("click", function () {
            $("#modal_dialog").dialog({
                title: "Enter your details to download English brochure"

                ,
                modal: true, height: 400, width: 400, closeOnEscape: false, draggable: true, resizable: false, close: clear,

                open: function (event, ui) { $(this).parent().children().children(".ui-dialog-titlebar-close").show(); }


            });
            $("#modal_dialog").parent().appendTo($("form:first"));


            return false;
        });
        $("[id*=btnAFRDownload]").live("click", function ShowPopup() {
            $("#modal_dialog").dialog({
                title: "Enter your details to download Afrikaans brochure"

                ,
                modal: true, height: 400, width: 400, closeOnEscape: true, draggable: true, resizable: false, close: clear,

                open: function (event, ui) { $(this).parent().children().children(".ui-dialog-titlebar-close").show(); }


            });
            $("#modal_dialog").parent().appendTo($("form:first"));


            return false;
        });
        $("[id*=btnIndianBrochureDownload]").live("click", function ShowPopup() {
            $("#modal_dialog").dialog({
                title: "Enter your details to download India brochure"

                ,
                modal: true, height: 400, width: 400, closeOnEscape: true, draggable: true, resizable: false, close: clear,

                open: function (event, ui) { $(this).parent().children().children(".ui-dialog-titlebar-close").show(); }


            });
            $("#modal_dialog").parent().appendTo($("form:first"));



            return false;
        });
        $("[id*=btnAfricaBrochureDownload]").live("click", function ShowPopup() {
            $("#modal_dialog").dialog({
                title: "Enter your details to download Africa brochure"

                ,
                modal: true, height: 400, width: 400, closeOnEscape: true, draggable: true, resizable: false, close: clear,

                open: function (event, ui) { $(this).parent().children().children(".ui-dialog-titlebar-close").show(); }


            });
            $("#modal_dialog").parent().appendTo($("form:first"));


            return false;
        });
    });
 

