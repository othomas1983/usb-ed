﻿//fixMozillaZIndex = true; //Fixes Z-Index problem with Mozilla browsers but causes odd scrolling problem, toggle to see if it helps
_menuCloseDelay = 800;
_menuOpenDelay = 100;
_subOffsetTop = 0;
_subOffsetLeft = 0;
var UserImageURL = "http://" + top.location.host + "/";

with (styMainMenu = new mm_style()) {
    styleid = 1;
    bordercolor = "#5E5A5B";
    borderstyle = "solid";
    borderwidth = 1;
    fontfamily = "Verdana";
    fontsize = "10pt";
    fontstyle = "normal";
    fontweight = "normal";
    headercolor = "#000000";
    offbgcolor = "#E7D0D7";
    offcolor = "#891536";
    onborder = "1px solid #FFFFFF";
    onbgcolor = "#891536";
    oncolor = "#FFFFFF";
    onsubimage = UserImageURL + "Common/Images/menu-white-arrow.gif";
    pagebgcolor = "#95A3A3";
    pagecolor = "#FFFFFF";
    outfilter = "fade(duration=0.5)";
    padding = 5;
    separatorcolor = "#FFFFFF";
    separatorsize = 1;
    subimage = UserImageURL + "Common/Images/menu-grey-arrow.gif";
    subimagepadding = 4;
}

with (stySubMainMenu = new mm_style()) {
    styleid = 1;
    bordercolor = "#5E5A5B";
    borderstyle = "solid";
    borderwidth = 1;
    fontfamily = "Verdana";
    fontsize = "8pt";
    fontstyle = "normal";
    fontweight = "normal";
    headercolor = "#000000";
    offbgcolor = "#E7D0D7";
    offcolor = "#891536";
    onborder = "1px solid #FFFFFF";
    onbgcolor = "#891536";
    oncolor = "#FFFFFF";
    onsubimage = UserImageURL + "Common/Images/menu-white-arrow.gif";
    pagebgcolor = "#95A3A3";
    pagecolor = "#FFFFFF";
    outfilter = "fade(duration=0.5)";
    padding = 5;
    separatorcolor = "#FFFFFF";
    separatorsize = 1;
    subimage = UserImageURL + "Common/Images/menu-grey-arrow.gif";
    subimagepadding = 4;
}

with (mnuDiscoverSubmenu = new menuname("PeopleSubmenu")) {
    style = styMainMenu;
    alwaysvisible = 1;
    position = "relative";
    align = "bottom";
    itemwidth = "100%";
    menuwidth = "100%";

    aI("text=Advisory Board;url=" + UserImageURL + "People/AdvisoryBoard.aspx;");
    aI("text=USB Management;url=" + UserImageURL + "People/UsbManagement.aspx;");
    aI("text=Academic staff;showmenu=PeopleSubmenuAcademicStaff;url=" + UserImageURL + "People/AcademicStaff.aspx;");
    aI("text=Support staff;showmenu=PeopleSubmenuSupportStaff;url=" + UserImageURL + "People/SupportStaff.aspx;");    
}

with (mnuDiscoverSubmenu = new menuname("PeopleSubmenuAcademicStaff")) {
    style = stySubMainMenu;
    overflow = "scroll";
    itemwidth = "100%";
    menuwidth = "175";

    aI("text=Core Faculty;url=" + UserImageURL + "People/CoreFaculty.aspx;");
    aI("text=Professor Extraordinaire;url=" + UserImageURL + "People/FacultyExtraordinaire.aspx;");
    aI("text=Associate Professor Extraordinaire;url=" + UserImageURL + "People/AssociateProfessorExtraordinaire.aspx;");
    aI("text=Senior Lecturers Extraordinaire;url=" + UserImageURL + "People/SeniorLecturersExtraordinaire.aspx;");
    aI("text=Visiting International Faculty;url=" + UserImageURL + "People/VisitingInternationalFaculty.aspx;");
    aI("text=Visiting Local Faculty;url=" + UserImageURL + "People/PartTimeAcademicStaff.aspx;");    
}

with (mnuDiscoverSubmenu = new menuname("PeopleSubmenuSupportStaff")) {
    style = stySubMainMenu;
    overflow = "scroll";
    itemwidth = "100%";
    menuwidth = "175";

    aI("text=Academic administration;url=" + UserImageURL + "People/AcademicAdministration.aspx;");
    aI("text=Accommodation and conference venues;url=" + UserImageURL + "People/AccommodationAndConferenceVenues.aspx;");    
    //aI("text=Deputy Registrar;url=" + UserImageURL + "People/DeputyRegistrar.aspx;");
    aI("text=Finance;url=" + UserImageURL + "People/FinanceAndAdministration.aspx;");
    aI("text=Information Technology;url=" + UserImageURL + "People/InformationTechnology.aspx;");
    aI("text=International Affairs;url=" + UserImageURL + "People/InternationalAffairs.aspx;");
    aI("text=Library services;url=" + UserImageURL + "People/Library.aspx;");
    aI("text=Marketing & Communication, Admissions and Career Services;url=" + UserImageURL + "People/MarketingAndCommunication.aspx;");
    aI("text=Secretarial;url=" + UserImageURL + "People/Secretaries.aspx;");
    aI("text=Services;url=" + UserImageURL + "People/Services.aspx;");
}

drawMenus();