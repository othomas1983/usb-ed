﻿//fixMozillaZIndex = true; //Fixes Z-Index problem with Mozilla browsers but causes odd scrolling problem, toggle to see if it helps
_menuCloseDelay = 800;
_menuOpenDelay = 100;
_subOffsetTop = 0;
_subOffsetLeft = 0;
var UserImageURL = "http://" + top.location.host + "/";

with (styMainMenu = new mm_style()) {
    styleid = 1;
    bordercolor = "#5E5A5B";
    borderstyle = "solid";
    borderwidth = 1;
    fontfamily = "Verdana";
    fontsize = "10pt";
    fontstyle = "normal";
    fontweight = "normal";
    headercolor = "#000000";
    offbgcolor = "#E7D0D7";
    offcolor = "#333333";
    onborder = "1px solid #FFFFFF";
    onbgcolor = "#891536";
    oncolor = "#FFFFFF";
    onsubimage = UserImageURL + "Common/Images/menu-white-arrow.gif";
    pagebgcolor = "#95A3A3";
    pagecolor = "#FFFFFF";
    outfilter = "fade(duration=0.5)";
    padding = 5;
    separatorcolor = "#FFFFFF";
    separatorsize = 1;
    subimage = UserImageURL + "Common/Images/menu-grey-arrow.gif";
    subimagepadding = 4;
}

with (stySubMainMenu = new mm_style()) {
    styleid = 1;
    bordercolor = "#5E5A5B";
    borderstyle = "solid";
    borderwidth = 1;
    fontfamily = "Verdana";
    fontsize = "8pt";
    fontstyle = "normal";
    fontweight = "normal";
    headercolor = "#000000";
    offbgcolor = "#E7D0D7";
    offcolor = "#333333";
    onborder = "1px solid #FFFFFF";
    onbgcolor = "#891536";
    oncolor = "#FFFFFF";
    onsubimage = UserImageURL + "Common/Images/menu-white-arrow.gif";
    pagebgcolor = "#95A3A3";
    pagecolor = "#FFFFFF";
    outfilter = "fade(duration=0.5)";
    padding = 5;
    separatorcolor = "#FFFFFF";
    separatorsize = 1;
    subimage = UserImageURL + "Common/Images/menu-grey-arrow.gif";
    subimagepadding = 4;
}

with (mnuDegreesSubmenu = new menuname("DegreesSubmenu")) {
    style = styMainMenu;
    alwaysvisible = 1;
    position = "relative";
    align = "bottom";
    itemwidth = "100%";
    menuwidth = "100%";

    aI("text=Introduction;url=" + UserImageURL + "Alumni/Introduction.aspx;");
    aI("text=USB Alumni Association;showmenu=AlumniAssociationSubmenu;url=" + UserImageURL + "Alumni/UsbAlumniAssociation.aspx;");
    aI("text=USB Alumni Office;url=" + UserImageURL + "Alumni/UsbAlumniOffice.aspx;");
    aI("text=Update your details;showmenu=UpdateDetailsSubmenu;url=" + UserImageURL + "Alumni/UpdateDetails.aspx;");    
}

with (mnuDegreesSubmenu = new menuname("AlumniAssociationSubmenu")) {
    style = stySubMainMenu;
    overflow = "scroll";
    itemwidth = "100%";
    menuwidth = "175";

    aI("text=Overview;url=" + UserImageURL + "Alumni/UsbAlumniAssociation/Overview.aspx;");
    aI("text=National level;url=" + UserImageURL + "Alumni/UsbAlumniAssociation/NationalLevel.aspx");
    aI("text=Regions;showmenu=AlumniAssociationRegions;url=" + UserImageURL + "Alumni/UsbAlumniAssociation/Regions.aspx;");
    aI("text=Continued learning;url=" + UserImageURL + "Alumni/UsbAlumniAssociation/ContinueLearning.aspx;");
}

with (mnuDegreesSubmenu = new menuname("UpdateDetailsSubmenu")) {
    style = stySubMainMenu;
    overflow = "scroll";
    itemwidth = "100%";
    menuwidth = "175";

    aI("text=Personal information;url=" + UserImageURL + "Alumni/UpdateDetails.aspx?tab=0;");
    aI("text=Address/Contact information;url=" + UserImageURL + "Alumni/UpdateDetails.aspx?tab=1;");  
}

with (mnuDegreesSubmenu = new menuname("AlumniAssociationRegions")) {
    style = stySubMainMenu;
    overflow = "scroll";
    itemwidth = "100%";
    menuwidth = "175";

    aI("text=Western Cape;url=" + UserImageURL + "Alumni/UsbAlumniAssociation/Regions/WesternCape.aspx;");
    aI("text=Eastern Cape;url=" + UserImageURL + "Alumni/UsbAlumniAssociation/Regions/EasternCape.aspx;");
    aI("text=Gauteng;url=" + UserImageURL + "Alumni/UsbAlumniAssociation/Regions/Gauteng.aspx;");
    aI("text=KwaZulu-Natal;url=" + UserImageURL + "Alumni/UsbAlumniAssociation/Regions/KwazuluNatal.aspx;");
    aI("text=Mpumalanga;url=" + UserImageURL + "Alumni/UsbAlumniAssociation/Regions/Mpumalanga.aspx;");
    aI("text=Namibia;url=" + UserImageURL + "Alumni/UsbAlumniAssociation/Regions/Namibia.aspx;");
    aI("text=East Africa;url=" + UserImageURL + "Alumni/UsbAlumniAssociation/Regions/EastAfrica.aspx;");
    aI("text=West Africa;url=" + UserImageURL + "Alumni/UsbAlumniAssociation/Regions/WestAfrica.aspx;");
    aI("text=Germany;url=" + UserImageURL + "Alumni/UsbAlumniAssociation/Regions/Germany.aspx;");
    aI("text=United Kingdom;url=" + UserImageURL + "Alumni/UsbAlumniAssociation/Regions/UnitedKingdom.aspx;");    
}

drawMenus();