﻿//fixMozillaZIndex = true; //Fixes Z-Index problem with Mozilla browsers but causes odd scrolling problem, toggle to see if it helps
_menuCloseDelay = 800;
_menuOpenDelay = 100;
_subOffsetTop = 0;
_subOffsetLeft = 0;
var UserImageURL = "http://" + top.location.host + "/";

with (styMainMenu = new mm_style()) {
    styleid = 1;
    bordercolor = "#5E5A5B";
    borderstyle = "solid";
    borderwidth = 1;
    fontfamily = "Verdana";
    fontsize = "10pt";
    fontstyle = "normal";
    fontweight = "normal";
    headercolor = "#000000";
    offbgcolor = "#E7D0D7";
    offcolor = "#333333";
    onborder = "1px solid #FFFFFF";
    onbgcolor = "#891536";
    oncolor = "#FFFFFF";
    onsubimage = UserImageURL + "Common/Images/menu-white-arrow.gif";
    pagebgcolor = "#95A3A3";
    pagecolor = "#FFFFFF";
    outfilter = "fade(duration=0.5)";
    padding = 5;
    separatorcolor = "#FFFFFF";
    separatorsize = 1;
    subimage = UserImageURL + "Common/Images/menu-grey-arrow.gif";
    subimagepadding = 4;
}

with (stySubMainMenu = new mm_style()) {
    styleid = 1;
    bordercolor = "#5E5A5B";
    borderstyle = "solid";
    borderwidth = 1;
    fontfamily = "Verdana";
    fontsize = "8pt";
    fontstyle = "normal";
    fontweight = "normal";
    headercolor = "#000000";
    offbgcolor = "#E7D0D7";
    offcolor = "#333333";
    onborder = "1px solid #FFFFFF";
    onbgcolor = "#891536";
    oncolor = "#FFFFFF";
    onsubimage = UserImageURL + "Common/Images/menu-white-arrow.gif";
    pagebgcolor = "#95A3A3";
    pagecolor = "#FFFFFF";
    outfilter = "fade(duration=0.5)";
    padding = 5;
    separatorcolor = "#FFFFFF";
    separatorsize = 1;
    subimage = UserImageURL + "Common/Images/menu-grey-arrow.gif";
    subimagepadding = 4;
}

with (mnuDegreesSubmenu = new menuname("DegreesSubmenu")) {
    style = styMainMenu;
    alwaysvisible = 1;
    position = "relative";
    align = "bottom";
    itemwidth = "100%";
    menuwidth = "100%";

    aI("text=MBA;showmenu=DegreesSubmenuMBA;url="+UserImageURL+"Degrees/MBADegree/Default.aspx;");
    aI("text=MPhil in Development Finance;showmenu=DegreesSubmenuMDevF;url=" + UserImageURL + "Degrees/MPhilInDevelopmentFinance/Default.aspx;");
    aI("text=MPhil in Management Coaching;showmenu=DegreesSubmenuCoaching;url=" + UserImageURL + "Degrees/MPhilInCoaching/Default.aspx;");
    aI("text=PhD in Business Management and Administration;showmenu=DegreesSubmenuPhD;url=" + UserImageURL + "Degrees/Phd/Default.aspx;");
    aI("text=PhD in Development Finance;showmenu=DegreesSubmenuPhDDevF;url=" + UserImageURL + "Degrees/PhdInDevelopmentFinance/Default.aspx;");
	aI("text=Post Graduate Diploma in Business Management and Administration;showmenu=DegreesSubmenuPGDBM;url=" + UserImageURL + "Degrees/Diplomabusinessmanagement/Default.aspx;")
    aI("text=Post Graduate Diploma in Development Finance;showmenu=DegreesSubmenuPGDDevF;url=" + UserImageURL + "Degrees/DiplomaDevelopmentFinance/Default.aspx;")
    //aI("text=Post Graduate Diploma in Dispute Settlement;url=" + UserImageURL + "disputesettlement/diploma_dispute_settlement_overview.html;target=_blank;")
    aI("text=Post Graduate Diploma in Leadership Development;showmenu=DegreesSubmenuLeadership;url=" + UserImageURL + "Degrees/DiplomaLeadership/Default.aspx;")
    aI("text=Post Graduate Diploma in Project Management;showmenu=DegreesSubmenuProjectManagement;url=" + UserImageURL + "Degrees/DiplomaProjectManagement/Default.aspx;")
    aI("text=Post Graduate Diploma in Futures Studies;url=" + UserImageURL + "Degrees/DiplomaFuturesStudies/Default.aspx;");
    aI("text=Doctoral Research Training;url=" + UserImageURL + "Degrees/Phd/DoctoralResearchTrainingProgramme.aspx;");
}

with (mnuDegreesSubmenu = new menuname("DegreesSubmenuMBA")) {
    style = stySubMainMenu;
    overflow = "scroll";
    itemwidth = "100%";
    menuwidth = "175";

    aI("text=Our learning philosophy;url=" + UserImageURL + "Degrees/MBADegree/OurLearningPhilosophy.aspx;");
    aI("text=Structure and content;showmenu=DegreesSubmenuMBASubStructure;url=" + UserImageURL + "Degrees/MBADegree/StructureAndContent.aspx;");
    aI("text=Full-time English;url=" + UserImageURL + "Degrees/MBADegree/FulltimeEnglish.aspx;");
    aI("text=Part-time English;url=" + UserImageURL + "Degrees/MBADegree/PartimeEnglish.aspx;");
    aI("text=Modular English;url=" + UserImageURL + "Degrees/MBADegree/ModularEnglish.aspx;");
    aI("text=Modulêr Afrikaans;url=" + UserImageURL + "Degrees/MBADegree/ModulerAfrikaansEnEngels.aspx;");
    aI("text=Fees and financing;url=" + UserImageURL + "Degrees/MBADegree/FeesAndFinancing.aspx;");
    aI("text=Career Services;url=" + UserImageURL + "Degrees/MBADegree/CareerServices.aspx;");
    aI("text=Information Sessions;url=" + UserImageURL + "Degrees/MBADegree/AttendAnOpenDay.aspx;");
    aI("text=FAQs;url=" + UserImageURL + "Degrees/MBADegree/Faqs.aspx;");
    aI("text=Apply;showmenu=DegreesSubmenuMBASubApply;url=" + UserImageURL + "Degrees/ApplyForAnMBA/Default.aspx;");
}

with (mnuDegreesSubmenu = new menuname("DegreesSubmenuMDevF")) {
    style = stySubMainMenu;
    overflow = "scroll";
    itemwidth = "100%";
    menuwidth = "175";

    aI("text=Our learning philosophy;url=" + UserImageURL + "Degrees/MPhilInDevelopmentFinance/OurLearningPhilosophy.aspx;");
    aI("text=Structure and content;url=" + UserImageURL + "Degrees/MPhilInDevelopmentFinance/StructureAndContent.aspx;");
    aI("text=Fees;url=" + UserImageURL + "Degrees/MPhilInDevelopmentFinance/Fees.aspx;");
    aI("text=Apply;showmenu=DegreesSubmenuMDevFSubApply;url=" + UserImageURL + "Degrees/MPhilInDevelopmentFinance/Apply.aspx;");
}

with (mnuDegreesSubmenu = new menuname("DegreesSubmenuCoaching")) {
    style = stySubMainMenu;
    overflow = "scroll";
    itemwidth = "100%";
    menuwidth = "175";

    aI("text=Our learning philosophy;url=" + UserImageURL + "Degrees/MPhilInCoaching/OurLearningPhilosophy.aspx;");
    aI("text=Structure and content;url=" + UserImageURL + "Degrees/MPhilInCoaching/StructureAndContent.aspx;");
    aI("text=Fees;url=" + UserImageURL + "Degrees/MPhilInCoaching/Fees.aspx;");
    aI("text=Apply;showmenu=DegreesSubmenuCoachingSubApply;url=" + UserImageURL + "Degrees/MPhilInCoaching/Apply.aspx;");
}

with (mnuDegreesSubmenu = new menuname("DegreesSubmenuPhD")) {
    style = stySubMainMenu;
    overflow = "scroll";
    itemwidth = "100%";
    menuwidth = "175";

    aI("text=Our learning philosophy;url=" + UserImageURL + "Degrees/Phd/LearningPhilosophy.aspx;");
    aI("text=Admission requirements;url=" + UserImageURL + "Degrees/Phd/AdmissionRequirements.aspx;");
    aI("text=Research proposal;url=" + UserImageURL + "Degrees/Phd/ResearchProposal.aspx;");
    aI("text=Fees and funding;url=" + UserImageURL + "Degrees/Phd/FeesAndFunding.aspx;");
    aI("text=Application procedure;url=" + UserImageURL + "Degrees/Phd/ApplicationProcedure.aspx;");
    aI("text=Online application form;url=http://phd.applications.usb.ac.za/;");
    //aI("text=Application form;url=" + UserImageURL + "Common/Pdfs/PhD_Application.pdf;target=_blank;");
    aI("text=EDAMBA;url=" + UserImageURL + "Degrees/Phd/Edamba.aspx;");
    aI("text=Doctoral Research Training Programme;url=" + UserImageURL + "Degrees/Phd/DoctoralResearchTrainingProgramme.aspx;");
}

with (mnuDegreesSubmenu = new menuname("DegreesSubmenuPhDDevF")) {
    style = stySubMainMenu;
    overflow = "scroll";
    itemwidth = "100%";
    menuwidth = "175";

    aI("text=Programme overview;url=" + UserImageURL + "Degrees/PhdInDevelopmentFinance/ProgrammeOverview.aspx;");
    aI("text=Admission requirements;url=" + UserImageURL + "Degrees/PhdInDevelopmentFinance/AdmissionRequirements.aspx;");
    aI("text=Fees;url=" + UserImageURL + "Degrees/PhdInDevelopmentFinance/Fees.aspx;");
    aI("text=Apply;url=" + UserImageURL + "Degrees/PhdInDevelopmentFinance/Apply.aspx;");
    //aI("text=EDAMBA;url=" + UserImageURL + "Degrees/Phd/Edamba.aspx;");
    //aI("text=Doctoral Research Training Programme;url=" + UserImageURL + "Degrees/Phd/DoctoralResearchTrainingProgramme.aspx;");
}

with (mnuDegreesSubmenu = new menuname("DegreesSubmenuPGDBM")) {
    style = stySubMainMenu;
    overflow = "scroll";
    itemwidth = "100%";
    menuwidth = "175";

    aI("text=Programme overview and outcomes;showmenu=DegreesSubmenubusinessmanagement;url=" + UserImageURL + "Degrees/Diplomabusinessmanagement/programmeoverview.aspx;");
    aI("text=Structure and content;showmenu=DegreesSubmenubusinessmanagement2;url=" + UserImageURL + "Degrees/Diplomabusinessmanagement/structure.aspx;");
    aI("text=Fees;url=" + UserImageURL + "Degrees/Diplomabusinessmanagement/fees.aspx;");
	aI("text=Important dates;url=" + UserImageURL + "Degrees/Diplomabusinessmanagement/Importantdates.aspx;");
    aI("text=Apply;showmenu=DegreesSubmenubusinessmanagement3;url=" + UserImageURL + "Degrees/Diplomabusinessmanagement/Apply.aspx;");

}

with (mnuDegreesSubmenu = new menuname("DegreesSubmenuPGDDevF")) {
    style = stySubMainMenu;
    overflow = "scroll";
    itemwidth = "100%";
    menuwidth = "175";

    aI("text=Programme overview;url=" + UserImageURL + "Degrees/DiplomaDevelopmentFinance/ProgrammeOverview.aspx;");
    aI("text=Structure and content;url=" + UserImageURL + "Degrees/DiplomaDevelopmentFinance/StructureAndContent.aspx;");
    aI("text=Fees;url=" + UserImageURL + "Degrees/DiplomaDevelopmentFinance/Fees.aspx;");
    aI("text=Apply;url=" + UserImageURL + "Degrees/DiplomaDevelopmentFinance/HowToApply.aspx;");

}

with (mnuDegreesSubmenu = new menuname("DegreesSubmenuLeadership")) {
    style = stySubMainMenu;
    overflow = "scroll";
    itemwidth = "100%";
    menuwidth = "175";

    aI("text=Learning philosophy;url=" + UserImageURL + "Degrees/DiplomaLeadership/LearningPhilosophy.aspx;");
    aI("text=Structure and content;url=" + UserImageURL + "Degrees/DiplomaLeadership/StructureAndContent.aspx;");
    aI("text=Fees;url=" + UserImageURL + "Degrees/DiplomaLeadership/Fees.aspx;");
    aI("text=Important dates;url=" + UserImageURL + "Degrees/DiplomaLeadership/ImportantDates.aspx;");
    aI("text=Apply;showmenu=DegreesSubmenuLeadershipSubApply;url=" + UserImageURL + "Degrees/DiplomaLeadership/Apply.aspx;");
}

with (mnuDegreesSubmenu = new menuname("DegreesSubmenuProjectManagement")) {
    style = stySubMainMenu;
    overflow = "scroll";
    itemwidth = "100%";
    menuwidth = "175";

    aI("text=Why do this programme?;url=" + UserImageURL + "Degrees/DiplomaProjectManagement/WhyThisPogramme.aspx;");
    aI("text=Structure and content;url=" + UserImageURL + "Degrees/DiplomaProjectManagement/StructureAndContent.aspx;");
    aI("text=Fees;url=" + UserImageURL + "Degrees/DiplomaProjectManagement/Fees.aspx;");    
    aI("text=Apply;showmenu=DegreesSubmenuProjectManagementSubApply;url=" + UserImageURL + "Degrees/DiplomaProjectManagement/Apply.aspx;");
}

with (mnuDegreesSubmenu = new menuname("DegreesSubmenuMBASubStructure")) {
    style = stySubMainMenu;
    overflow = "scroll";
    itemwidth = "100%";
    menuwidth = "175";

    aI("text=Management fundamentals;url=" + UserImageURL + "Degrees/MBADegree/ManagementFundamentals.aspx;");
    aI("text=Management in context;url=" + UserImageURL + "Degrees/MBADegree/ManagementInContext.aspx;");
    aI("text=Management enhancement;url=" + UserImageURL + "Degrees/MBADegree/ManagementEnhancement.aspx;");
    aI("text=International study tour;url=" + UserImageURL + "Degrees/MBADegree/InternationalStudyTour.aspx;");
}

with (mnuDegreesSubmenu = new menuname("DegreesSubmenuMBASubApply")) {
    style = stySubMainMenu;
    overflow = "scroll";
    itemwidth = "100%";
    menuwidth = "175";

    aI("text=Admission requirements;url=" + UserImageURL + "Degrees/ApplyForAnMBA/AdmissionRequirements.aspx;");
    aI("text=FAQs;url=" + UserImageURL + "Degrees/ApplyForAnMBA/Faqs.aspx;");
    aI("text=SHL or GMAT?;url=" + UserImageURL + "Degrees/ApplyForAnMBA/ShlOrGmatSelectionTest.aspx;");
    aI("text=Motivational essays;url=" + UserImageURL + "Degrees/ApplyForAnMBA/MotivationalEssayAndCv.aspx;");
    aI("text=Application fee;url=" + UserImageURL + "Degrees/ApplyForAnMBA/ApplicationFee.aspx;");    
    aI("text=Online application form;url=http://applications.usb.ac.za/Programmes/MBA;target=_blank;");
}

with (mnuDegreesSubmenu = new menuname("DegreesSubmenuMDevFSubApply")) {
    style = stySubMainMenu;
    overflow = "scroll";
    itemwidth = "100%";
    menuwidth = "175";

    aI("text=Admission requirements;url=" + UserImageURL + "Degrees/MPhilInDevelopmentFinance/AdmissionRequirements.aspx;");
    aI("text=USB Admissions Office;url=" + UserImageURL + "Degrees/MPhilInDevelopmentFinance/USBAdmissionsOffice.aspx;");
    aI("text=Agent for East Africa;url=" + UserImageURL + "Degrees/MPhilInDevelopmentFinance/EastAfricanAdmissionsOffice.aspx;");
    aI("text=Agent for West Africa;url=" + UserImageURL + "Degrees/MPhilInDevelopmentFinance/WestAfricanAdmissionsOffice.aspx;");    
}

with (mnuDegreesSubmenu = new menuname("DegreesSubmenuCoachingSubApply")) {
    style = stySubMainMenu;
    overflow = "scroll";
    itemwidth = "100%";
    menuwidth = "175";

    aI("text=Admission requirements;url=" + UserImageURL + "Degrees/MPhilInCoaching/AdmissionRequirements.aspx;");
    //aI("text=Application fee;url=" + UserImageURL + "Degrees/MPhilInCoaching/ApplicationFee.aspx;");
    aI("text=Online application form;url=http://applications.usb.ac.za/Programmes/MPHIL;target=_blank;");
    //aI("text=We are no longer accepting applications for the MPhil in Management Coaching for 2012.;");
}

with (mnuDegreesSubmenu = new menuname("DegreesSubmenubusinessmanagement")) {
    style = stySubMainMenu;
    overflow = "scroll";
    itemwidth = "100%";
    menuwidth = "175";

    aI("text=Gain access to an MBA;url=/Degrees/Diplomabusinessmanagement/gainaccess.aspx;");    
}

with (mnuDegreesSubmenu = new menuname("DegreesSubmenubusinessmanagement2")) {
    style = stySubMainMenu;
    overflow = "scroll";
    itemwidth = "100%";
    menuwidth = "175";

    aI("text=Core modules and electives form;url=/Degrees/Diplomabusinessmanagement/coremodules.aspx;");    
}

with (mnuDegreesSubmenu = new menuname("DegreesSubmenubusinessmanagement3")) {
    style = stySubMainMenu;
    overflow = "scroll";
    itemwidth = "100%";
    menuwidth = "175";

    aI("text=Admission requirements;url=/Degrees/Diplomabusinessmanagement/admissions.aspx;");    
}

with (mnuDegreesSubmenu = new menuname("DegreesSubmenuLeadershipSubApply")) {
    style = stySubMainMenu;
    overflow = "scroll";
    itemwidth = "100%";
    menuwidth = "175";

    aI("text=Online application form;url=http://applications.usb.ac.za/programmes/NG%20DIP;target=_blank;");    
}

with (mnuDegreesSubmenu = new menuname("DegreesSubmenuProjectManagementSubApply")) {
    style = stySubMainMenu;
    overflow = "scroll";
    itemwidth = "100%";
    menuwidth = "175";

    aI("text=Admission requirements;url=" + UserImageURL + "Degrees/DiplomaProjectManagement/AdmissionRequirements.aspx;");
    aI("text=Online application form;url=http://applications.usb.ac.za/programmes/NG%20DIP;target=_blank;");
}

drawMenus();