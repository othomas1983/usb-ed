﻿//fixMozillaZIndex = true; //Fixes Z-Index problem with Mozilla browsers but causes odd scrolling problem, toggle to see if it helps
_menuCloseDelay = 800;
_menuOpenDelay = 100;
_subOffsetTop = 0;
_subOffsetLeft = -0;
var UserImageURL = "http://" + top.location.host + "/";

with (styMainMenu = new mm_style()) {
    fontfamily = "Verdana";
    fontsize = "10pt";
    fontstyle = "normal";
    fontweight = "normal";
    headerbgcolor = "#FFFFFF";
    headercolor = "#000000";
    offcolor = "#FFFFFF";
    oncolor = "#FFFFFF";
    outfilter = "fade(duration=0.5)";
    padding = 5;
    separatorcolor = "#FFFFFF";
    separatorpadding = 1;
    subimagepadding = 2;
    pointer = "default";
}

with (submenuStyle = new mm_style()) {
    styleid = 1;
    bordercolor = "#5E5A5B";
    borderstyle = "solid";
    borderwidth = 1;
    fontfamily = "Verdana";
    fontsize = "8pt";
    fontstyle = "normal";
    fontweight = "normal";
    headercolor = "#000000";
    offbgcolor = "#891536";
    offcolor = "#FFFFFF";
    onborder = "1px solid #FFFFFF";
    oncolor = "#FFFFFF";
    outfilter = "fade(duration=0.5)";
    padding = 5;
    separatorsize = 1;
    subimagepadding = 8;
}

with (mnuUSBMainMenu = new menuname("USBMainMenu")) {
    style = styMainMenu;
    alwaysvisible = 1;
    orientation = "horizontal";
    position = "relative";

    aI("image=" + UserImageURL + "Common/Images/grey_degrees.jpg;overimage="+UserImageURL+"Common/Images/red_degrees.jpg;showmenu=USBMainMenuDegrees;");
    aI("image=" + UserImageURL + "Common/Images/grey_spacer.jpg;");
    aI("image=" + UserImageURL + "Common/Images/grey_aboutus.jpg;overimage=" + UserImageURL + "Common/Images/red_aboutus.jpg;showmenu=USBMainMenuAboutUs;");
    aI("image=" + UserImageURL + "Common/Images/grey_spacer.jpg;");
    aI("image=" + UserImageURL + "Common/Images/grey_international.jpg;overimage=" + UserImageURL + "Common/Images/red_international.jpg;showmenu=USBMainMenuInternational;");
    aI("image=" + UserImageURL + "Common/Images/grey_spacer.jpg;");
    aI("image=" + UserImageURL + "Common/Images/grey_news&events.jpg;overimage=" + UserImageURL + "Common/Images/red_news&events.jpg;showmenu=USBMainMenuNews&Events;");
    aI("image=" + UserImageURL + "Common/Images/grey_spacer.jpg;");
    aI("image=" + UserImageURL + "Common/Images/grey_publications.jpg;overimage=" + UserImageURL + "Common/Images/red_publications.jpg;showmenu=USBMainMenuPublications;");
    aI("image=" + UserImageURL + "Common/Images/grey_spacer.jpg;");
    aI("image=" + UserImageURL + "Common/Images/grey_research.jpg;overimage=" + UserImageURL + "Common/Images/red_research.jpg;showmenu=USBMainMenuResearch;");
    aI("image=" + UserImageURL + "Common/Images/grey_spacer.jpg;");
    aI("image=" + UserImageURL + "Common/Images/grey_people.jpg;overimage=" + UserImageURL + "Common/Images/red_people.jpg;showmenu=USBMainMenuPeople;");
}

with (mnuUSBMainMenu = new menuname("USBMainMenuDegrees")) {
    style = submenuStyle;
    overflow = "scroll";
    itemwidth = "100%";
    menuwidth = "132";

    aI("text=MBA;url=" + UserImageURL + "Degrees/MBADegree/Default.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'MBA - MainNav']);")
    aI("text=MPhil in Development Finance;url=" + UserImageURL + "Degrees/MPhilInDevelopmentFinance/Default.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'MDF - MainNav']);")
    aI("text=MPhil in Management Coaching;url=" + UserImageURL + "Degrees/MPhilInCoaching/Default.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'MPhilCoaching - MainNav']);")
    aI("text=PhD in Business Management and Administration;url=" + UserImageURL + "Degrees/Phd/Default.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'PhD - MainNav']);")
    aI("text=PhD in Development Finance;url=" + UserImageURL + "Degrees/PhdInDevelopmentFinance/Default.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'PhD DevF - MainNav']);")
    aI("text=Postgraduate Diploma in Business Management and Administration;url=" + UserImageURL + "Degrees/Diplomabusinessmanagement/Default.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'MBA - MainNav']);")
	aI("text=Post Graduate Diploma in Development Finance;url=" + UserImageURL + "Degrees/DiplomaDevelopmentFinance/Default.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'DiplomaDevelopmentFinance - MainNav']);")
    //aI("text=Post Graduate Diploma in Dispute Settlement;url=" + UserImageURL + "disputesettlement/diploma_dispute_settlement_overview.html; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'Dispute Settlement - MainNav']);")
    aI("text=Post Graduate Diploma in Leadership Development;url=" + UserImageURL + "Degrees/DiplomaLeadership/Default.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'DiplomaLeadership - MainNav']);")
    aI("text=Post Graduate Diploma in Project Management;url=" + UserImageURL + "Degrees/DiplomaProjectManagement/Default.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'DiplomaProjectManagement - MainNav']);")
    aI("text=Post Graduate Diploma in Future Studies;url=" + UserImageURL + "Degrees/DiplomaFuturesStudies/Default.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'MPhilFutureStudies - MainNav']);")
    aI("text=Doctoral Research Training;url=" + UserImageURL + "Degrees/Phd/DoctoralResearchTrainingProgramme.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'DoctoralResearchTrainingProgramme - MainNav']);")

}

with (mnuUSBMainMenu = new menuname("USBMainMenuAboutUs")) {
    style = submenuStyle;
    overflow = "scroll";
    itemwidth = "100%";
    menuwidth = "132";

    aI("text=Celebrating 50 Years;url=" + UserImageURL + "AboutUs/History.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'DiscoverUSB - MainNav']);")
    aI("text=Discover USB;url=" + UserImageURL + "AboutUs/DiscoverUSB.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'DiscoverUSB - MainNav']);")
    aI("text=Accreditations and rankings;url=" + UserImageURL + "AboutUs/AccreditationsAndRatings.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'Accreditations - MainNav']);")
    aI("text=Academic offering;url=" + UserImageURL + "AboutUs/AcademicOffering.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'AcademicOffering - MainNav']);")
    aI("text=USB Alumni;url=" + UserImageURL + "AboutUs/alumni.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'DiscoverUSB - MainNav']);")
    aI("text=Social engagement;url=" + UserImageURL + "AboutUs/SocialInvestment.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'SocialInvestment - MainNav']);")
    aI("text=Small Business Academy;url=" + UserImageURL + "AboutUs/SmallBusinessAcademy.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'SmallBusinessAcademy - MainNav']);");
    aI("text=Career Services;url=" + UserImageURL + "AboutUs/CareerServices/Default.aspx;")
    aI("text=Campus and facilities;url=" + UserImageURL + "AboutUs/CampusAndFacilities.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'CampusAndFacilities - MainNav']);")
    aI("text=Directions to campus;url=" + UserImageURL + "AboutUs/DirectionsToCampus.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'DirectionsToCampus - MainNav']);")
    aI("text=Elephant campaign;url=" + UserImageURL + "AboutUs/ElephantCampaign.aspx;")
    aI("text=Contact us;url=" + UserImageURL + "AboutUs/ContactUs.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'ContactUs - MainNav']);")
}

with (mnuUSBMainMenu = new menuname("USBMainMenuInternational")) {
    style = submenuStyle;
    overflow = "scroll";
    itemwidth = "100%";
    menuwidth = "132";

    aI("text=Overview;url=" + UserImageURL + "International/Overview.aspx;")
    aI("text=Partner business schools;url=" + UserImageURL + "International/PartnerBusinessSchools.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'Partners - MainNav']);")
    aI("text=Exchange study options;url=" + UserImageURL + "International/ExchangeOptions.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'ExchangePrograms - MainNav']);")
    aI("text=Help for international students;url=" + UserImageURL + "International/PracticalInformationForInternationalStudents.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'InternationalStudents - MainNav']);")
    aI("text=Visiting groups;url=" + UserImageURL + "International/VisitingGroups.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'VisitingGroups - MainNav']);")
    aI("text=International affairs office;url=" + UserImageURL + "International/InternationalAffairsOffice.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'InternationalOffice - MainNav']);")
}

with (mnuUSBMainMenu = new menuname("USBMainMenuNews&Events")) {
    style = submenuStyle;
    overflow = "scroll";
    itemwidth = "100%";
    menuwidth = "132";

    aI("text=Events calendar;url=" + UserImageURL + "NewsAndEvents/EventsCalendar.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'EventsCalendar - MainNav']);")
    aI("text=Leader's Angle;url=" + UserImageURL + "NewsAndEvents/LeadersAngle.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'LeadersAngle - MainNav']);")
    aI("text=In the News;url=" + UserImageURL + "NewsAndEvents/InTheNews.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'InTheNews - MainNav']);")
    aI("text=USB Newsroom;url=" + UserImageURL + "NewsAndEvents/NewsItemsList.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'NewsItemsList - MainNav']);")
    aI("text=Newsletters;url=" + UserImageURL + "NewsAndEvents/Newsletters/Default.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'NewsLetters - MainNav']);")
    //aI("text=Event Streaming;url=" + UserImageURL + "NewsAndEvents/Streaming/Default.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'Streaming - MainNav']);")
}

with (mnuUSBMainMenu = new menuname("USBMainMenuPublications")) {
    style = submenuStyle;
    overflow = "scroll";
    itemwidth = "100%";
    menuwidth = "132";

    //aI("text=Overview;url=" + UserImageURL + "Publications/PublicationsOverview.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'Overview - MainNav']);")
    //aI("text=USB Agenda magazine;url=http://thoughtprint.usb.ac.za/Pages/Agenda.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'Agenda - MainNav']);")
    //aI("text=USB Leaders' Lab journal;url=" + UserImageURL + "Publications/UsbLeadersLabJournal/LeadersLabJournal.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'LeadersLab - MainNav']);")    
    //aI("text=New thinking @ USB;url=" + UserImageURL + "Publications/NewThinking@Usb.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'NewThinking - MainNav']);")
    //aI("text=Multimedia;url=" + UserImageURL + "Publications/Multimedia/Default.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'Multimedia - MainNav']);")

    aI("text=Search;url=http://thoughtprint.usb.ac.za/Pages/TP-Home.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'Overview - MainNav']);")
    aI("text=USB Agenda;url=http://thoughtprint.usb.ac.za/Pages/Agenda.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'Agenda - MainNav']);")
    aI("text=USB Leader’s Lab;url=http://thoughtprint.usb.ac.za/Pages/USB-Leaders-Lab.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'LeadersLab - MainNav']);")    
    aI("text=Leader’s Angle;url=http://thoughtprint.usb.ac.za/Pages/Leaders-Angle.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'NewThinking - MainNav']);")
    aI("text=New thinking;url=http://thoughtprint.usb.ac.za/Pages/new-thinking.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'NewThinking - MainNav']);")
    aI("text=Research;url=" + UserImageURL + "Research/Research.aspx;")
    aI("text=Past Events;url=http://thoughtprint.usb.ac.za/Pages/USB-Events.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'NewThinking - MainNav']);")
    aI("text=USB General;url=http://thoughtprint.usb.ac.za/Pages/USB-MultiMedia.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'NewThinking - MainNav']);")
    //aI("text=Multimedia;url=" + UserImageURL + "Publications/Multimedia/Default.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'Multimedia - MainNav']);")    
}


with (mnuUSBMainMenu = new menuname("USBMainMenuResearch")) {
    style = submenuStyle;
    overflow = "scroll";
    itemwidth = "100%";
    menuwidth = "132";

    aI("text=Research;url=" + UserImageURL + "Research/Research.aspx;")
    aI("text=Research centres;url=" + UserImageURL + "Research/ResearchCentres.aspx;")
    aI("text=Visiting Research Fellowships;url=" + UserImageURL + "Research/VisitingResearchFellowships.aspx;")
    //aI("text=Journals managed by USB;url=" + UserImageURL + "Research/ResearchJournals.aspx;")
    //aI("text=Call for papers;url=" + UserImageURL + "Research/CallForPapers.aspx;")        
}

with (mnuUSBMainMenu = new menuname("USBMainMenuPeople")) {
    style = submenuStyle;
    overflow = "scroll";
    itemwidth = "100%";
    menuwidth = "132";

    aI("text=Advisory Board;url=" + UserImageURL + "People/AdvisoryBoard.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'AdvisoryBoard - MainNav']);")
    aI("text=USB ExCo;url=" + UserImageURL + "People/UsbManagement.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'Management - MainNav']);")
    aI("text=Academic Staff;url=" + UserImageURL + "People/AcademicStaff.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'AcademicStaff - MainNav']);")
    aI("text=Support Staff;url=" + UserImageURL + "People/SupportStaff.aspx; clickfunction=_gaq.push(['_trackEvent', 'Internal Links', 'Click', 'SupportStaff - MainNav']);")
}

drawMenus();