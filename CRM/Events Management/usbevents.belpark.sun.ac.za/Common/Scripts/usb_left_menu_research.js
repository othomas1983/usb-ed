﻿//fixMozillaZIndex = true; //Fixes Z-Index problem with Mozilla browsers but causes odd scrolling problem, toggle to see if it helps
_menuCloseDelay = 800;
_menuOpenDelay = 100;
_subOffsetTop = 0;
_subOffsetLeft = 0;
var UserImageURL = "http://" + top.location.host + "/";

with (styMainMenu = new mm_style()) {
    styleid = 1;
    bordercolor = "#5E5A5B";
    borderstyle = "solid";
    borderwidth = 1;
    fontfamily = "Verdana";
    fontsize = "10pt";
    fontstyle = "normal";
    fontweight = "normal";
    headercolor = "#000000";
    offbgcolor = "#E7D0D7";
    offcolor = "#891536";
    onborder = "1px solid #FFFFFF";
    onbgcolor = "#891536";
    oncolor = "#FFFFFF";
    onsubimage = UserImageURL + "Common/Images/menu-white-arrow.gif";
    pagebgcolor = "#95A3A3";
    pagecolor = "#FFFFFF";
    outfilter = "fade(duration=0.5)";
    padding = 5;
    separatorcolor = "#FFFFFF";
    separatorsize = 1;
    subimage = UserImageURL + "Common/Images/menu-grey-arrow.gif";
    subimagepadding = 4;
}

with (stySubMainMenu = new mm_style()) {
    styleid = 1;
    bordercolor = "#5E5A5B";
    borderstyle = "solid";
    borderwidth = 1;
    fontfamily = "Verdana";
    fontsize = "8pt";
    fontstyle = "normal";
    fontweight = "normal";
    headercolor = "#000000";
    offbgcolor = "#E7D0D7";
    offcolor = "#891536";
    onborder = "1px solid #FFFFFF";
    onbgcolor = "#891536";
    oncolor = "#FFFFFF";
    onsubimage = UserImageURL + "Common/Images/menu-white-arrow.gif";
    pagebgcolor = "#95A3A3";
    pagecolor = "#FFFFFF";
    outfilter = "fade(duration=0.5)";
    padding = 5;
    separatorcolor = "#FFFFFF";
    separatorsize = 1;
    subimage = UserImageURL + "Common/Images/menu-grey-arrow.gif";
    subimagepadding = 4;
}

with (mnuDiscoverSubmenu = new menuname("ResearchSubmenu")) {
    style = styMainMenu;
    alwaysvisible = 1;
    position = "relative";
    align = "bottom";
    itemwidth = "100%";
    menuwidth = "100%";

    aI("text=Research;showmenu=ResearchSubmenuAcademic;url=" + UserImageURL + "Research/Research.aspx;");
    aI("text=Research centres;showmenu=ResearchSubmenuCentres;url=" + UserImageURL + "Research/ResearchCentres.aspx;");
    aI("text=Visiting Research Fellowships;url=" + UserImageURL + "Research/VisitingResearchFellowships.aspx;");
  //  aI("text=Journals managed by USB;url=" + UserImageURL + "Research/ResearchJournals.aspx;");
 //   aI("text=Call for papers;url=" + UserImageURL + "Research/CallForPapers.aspx;")   
}

with (mnuDiscoverSubmenu = new menuname("ResearchSubmenuAcademic")) {
    style = stySubMainMenu;
    overflow = "scroll";
    itemwidth = "100%";
    menuwidth = "175";

    aI("text=Articles in accredited journals;url=" + UserImageURL + "Research/AcademicResearch2.aspx");
    aI("text=Articles in non-accredited journals;url=" + UserImageURL + "Research/AcademicResearch3.aspx");
    aI("text=Proceedings: International;url=" + UserImageURL + "Research/AcademicResearch4.aspx");
    aI("text=Proceedings: National;url=" + UserImageURL + "Research/AcademicResearch5.aspx");
    //aI("text=Papers: International;url=" + UserImageURL + "Research/AcademicResearch6.aspx");
    //aI("text=Papers: National;url=" + UserImageURL + "Research/AcademicResearch.aspx?sResearchCodes=PNSC;");
    aI("text=Books;url=" + UserImageURL + "Research/AcademicResearch6.aspx");
    aI("text=Chapters in books;url=" + UserImageURL + "Research/AcademicResearch.aspx?sResearchCodes=CBKS;");
    aI("text=Research reports;url=" + UserImageURL + "Research/AcademicResearch.aspx?sResearchCodes=RR;");
    aI("text=Creative work;url=" + UserImageURL + "Research/AcademicResearch.aspx?sResearchCodes=CW;");
    aI("text=Doctoral research in progress;url=" + UserImageURL + "Research/AcademicResearch.aspx?sResearchCodes=DPC;");
    aI("text=Doctoral research completed;url=" + UserImageURL + "Research/AcademicResearch.aspx?sResearchCodes=DSC;");
    aI("text=MBA research projects in progress;url=" + UserImageURL + "Research/AcademicResearch.aspx?sResearchCodes=MPC;");
    aI("text=MBA research projects completed;url=" + UserImageURL + "Research/AcademicResearch.aspx?sResearchCodes=MTC;");
}

with (mnuDiscoverSubmenu = new menuname("ResearchSubmenuCentres")) {
    style = stySubMainMenu;
    overflow = "scroll";
    itemwidth = "100%";
    menuwidth = "175";

    aI("text=Centre for Leadership Studies;url=" + UserImageURL + "Research/ResearchCentres.aspx;");
    aI("text=Centre for Corporate Governance in Africa;url=http://www.governance.usb.ac.za/;target=_blank;");
    aI("text=Africa Centre for Dispute Settlement;url=http://www.usb.ac.za/disputesettlement;target=_blank;");
    aI("text=The Base of the Pyramid Learning Lab;url=http://www.bop.org.za/;target=_blank;");
    aI("text=Institute for Futures Research;url=http://www.ifr.sun.ac.za/;target=_blank;");    
}

drawMenus();