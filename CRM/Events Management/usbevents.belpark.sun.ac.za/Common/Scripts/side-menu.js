﻿function URL(url, name, targetBlank, id) {
    this.url = url;
    this.name = name;
    this.children = new Array();
    this.id = id;
    this.targetBlank = targetBlank;
    return true;
}

//var ROOTADDRESS_OPEN = "http://bpcweb08";                   // Dev
var ROOTADDRESS_OPEN = "http://www.usb.ac.za";            // Live

//var ROOTADDRESS_CLOSED = "http://bpcweb08";                 // Dev
var ROOTADDRESS_CLOSED = "http://alumni.usb.ac.za";     // Live

var myURLs = new Array();

/* Level 1: Open */                                                                                                                                     // Index
myURLs.push(new URL(ROOTADDRESS_OPEN.toString() + '/alumni/default.aspx', 'Alumni Home', false));                                                      // 0
myURLs.push(new URL(ROOTADDRESS_OPEN.toString() + '/alumni/usbalumniassociation.aspx', 'USB Alumni Association', false));                               // 1
myURLs.push(new URL(ROOTADDRESS_OPEN.toString() + '/alumni/usbalumniassociation/continuelearning.aspx', 'Continued Learning', false));                  // 2
myURLs.push(new URL(ROOTADDRESS_OPEN.toString() + '/newsandevents/eventscalendar.aspx', 'Events', true));                                               // 2
myURLs.push(new URL(ROOTADDRESS_OPEN.toString() + '/alumni/investmentinsociety.aspx', 'Investment in Society', false));                                 // 3
myURLs.push(new URL(null, 'Publications', false));                                                                                                      //4
myURLs.push(new URL(ROOTADDRESS_OPEN.toString() + '/alumni/news.aspx', 'Alumni News', false));                                                          //5
myURLs.push(new URL(ROOTADDRESS_OPEN.toString() + '/publications/publicationsoverview.aspx', 'Thought Leadership', true));                              //6                                                                        // 4
myURLs.push(new URL(ROOTADDRESS_OPEN.toString() + '/alumni/USBInShort.aspx', 'USB in Short', false));                                                   // 7
myURLs.push(new URL(ROOTADDRESS_OPEN.toString() + '/publications/multimedia/Default.aspx', 'Videos', true));                                            // 8
myURLs.push(new URL(ROOTADDRESS_OPEN.toString() + '/aboutus/academicoffering.aspx', 'USB Programmes', true));                                           // 9
myURLs.push(new URL('http://www.usb-ed.com/Courses', 'USB-ED Programmes', true));                                                                       // 10
myURLs.push(new URL(ROOTADDRESS_OPEN.toString() + '/alumni/givingtousb.aspx', 'Giving to USB', false));                                                 // 11
myURLs.push(new URL(ROOTADDRESS_OPEN.toString() + '/alumni/usbalumnioffice.aspx', 'Contact Alumni Office', false));                                      // 12

/* Level 1: Closed */
myURLs.push(new URL('no-url', 'SIGN IN FOR:', false));                                                                                                  // 11
//myURLs.push(new URL(ROOTADDRESS_CLOSED.toString() + '/alumni/myinformation.html', 'My Information', false));
myURLs.push(new URL(ROOTADDRESS_OPEN.toString() + '/alumni/closed/UnderConstruction.aspx', 'Update personal details', false));
myURLs.push(new URL(ROOTADDRESS_OPEN.toString() + '/alumni/closed/UnderConstruction.aspx', 'Update contact details', false));

//myURLs.push(new URL('http://web-apps.sun.ac.za/PIUpdate/PIUPersoonlik.jsp?pLang=english&PIIType=A', 'Update personal details', true));           // 17
//myURLs.push(new URL('http://web-apps.sun.ac.za/PIUpdate/PIUAdres.jsp?pLang=english&PIAType=A', 'Update contact details', true));                                                                                               // 12

myURLs.push(new URL(ROOTADDRESS_OPEN.toString() + '/alumni/closed/UnderConstruction.aspx', 'Alumni Social Networks', false));                          // 13
myURLs.push(new URL(ROOTADDRESS_OPEN.toString() + '/alumni/closed/UnderConstruction.aspx', 'Career Development', false));                                    // 14
myURLs.push(new URL(ROOTADDRESS_OPEN.toString() + '/alumni/closed/UnderConstruction.aspx', 'Exclusive Resources', false));                                  // 15
//myURLs.push(new URL('http://web-apps.sun.ac.za/PIUpdate/PIUPersoonlik.jsp?pLang=english&PIIType=A', 'Update personal details', true));
myURLs.push(new URL(ROOTADDRESS_OPEN.toString() + '/alumni/closed/UnderConstruction.aspx', 'Alumni Business Services', false));                         // 16
/*
myURLs.push(new URL('http://sun025.sun.ac.za/portal/page/portal/alumni_page_group/USB_ALUMNI_Password_Reset', 'Manage your password', true));           // 17
myURLs.push(new URL('http://sun025.sun.ac.za/portal/page/portal/alumni_page_group/USB_ALUMNI_Password_Questions', 'Password questions', true));         // 18

myURLs.push(new URL(ROOTADDRESS_OPEN.toString() + '/alumni/closed/ManagePassword.aspx', 'Manage your password', false));                                     // 17
myURLs.push(new URL(ROOTADDRESS_OPEN.toString() + '/alumni/closed/PasswordQuestions.aspx', 'Password questions', false));                                    // 18
*/

var lvl2_index1 = new Array();
lvl2_index1.push(new URL(ROOTADDRESS_OPEN.toString() + '/alumni/usbalumniassociation/overview.aspx', 'Overview', false));
lvl2_index1.push(new URL(ROOTADDRESS_OPEN.toString() + '/alumni/usbalumniassociation/nationallevel.aspx', 'National level', false));
lvl2_index1.push(new URL(ROOTADDRESS_OPEN.toString() + '/alumni/usbalumniassociation/regions.aspx', 'Regions', false));
/*lvl2_index1.push(new URL(ROOTADDRESS_OPEN.toString() + '/alumni/usbalumniassociation/continuelearning.aspx', 'Continued Learning', false));*/

var lvl3_index2 = new Array();
lvl3_index2.push(new URL(ROOTADDRESS_OPEN.toString() + '/alumni/usbalumniassociation/regions/centralregion.aspx', 'Central Region', false));
lvl3_index2.push(new URL(ROOTADDRESS_OPEN.toString() + '/alumni/usbalumniassociation/regions/westerncape.aspx', 'Western Cape', false));
lvl3_index2.push(new URL(ROOTADDRESS_OPEN.toString() + '/alumni/usbalumniassociation/regions/easterncape.aspx', 'Eastern Cape', false));
lvl3_index2.push(new URL(ROOTADDRESS_OPEN.toString() + '/alumni/usbalumniassociation/regions/gauteng.aspx', 'Gauteng', false));
lvl3_index2.push(new URL(ROOTADDRESS_OPEN.toString() + '/alumni/usbalumniassociation/regions/kwazulunatal.aspx', 'Kwazulu Natal', false));
lvl3_index2.push(new URL(ROOTADDRESS_OPEN.toString() + '/alumni/usbalumniassociation/regions/mpumalanga.aspx', 'Mpumalanga', false));
lvl3_index2.push(new URL(ROOTADDRESS_OPEN.toString() + '/alumni/usbalumniassociation/regions/namibia.aspx', 'Namibia', false));
lvl3_index2.push(new URL(ROOTADDRESS_OPEN.toString() + '/alumni/usbalumniassociation/regions/eastafrica.aspx', 'East Africa', false));
lvl3_index2.push(new URL(ROOTADDRESS_OPEN.toString() + '/alumni/usbalumniassociation/regions/westafrica.aspx', 'West Africa', false));
lvl3_index2.push(new URL(ROOTADDRESS_OPEN.toString() + '/alumni/usbalumniassociation/regions/europe.aspx', 'Europe', false));
lvl3_index2.push(new URL(ROOTADDRESS_OPEN.toString() + '/alumni/usbalumniassociation/regions/unitedkingdom.aspx', 'United Kingdom', false));

var lvl2_index4 = new Array();
//lvl2_index4.push(new URL(ROOTADDRESS_OPEN.toString() + '/publications/usbagendamagazine/default.aspx', 'Agenda', true));
lvl2_index4.push(new URL('http://thoughtprint.usb.ac.za/Pages/Agenda.aspx', 'Agenda', true));
lvl2_index4.push(new URL(ROOTADDRESS_OPEN.toString() + '/publications/usbleaderslabjournal/default.aspx', 'Leader\'s Lab', true));
//lvl2_index4.push(new URL(ROOTADDRESS_OPEN.toString() + '/newsandevents/newsitemslist.aspx', 'E-news', true));
//lvl2_index4.push(new URL((ROOTADDRESS_OPEN.toString() + '/publications/publicationsoverview.aspx'), 'Thought Leadership', true));

//var lvl2_index10 = new Array();
//lvl2_index10.push(new URL('/alumni/usbalumnioffice.aspx', 'USB Alumni Office', true));

/*
var lvl2_index12 = new Array();
lvl2_index12.push(new URL('http://web-apps.sun.ac.za/PIUpdate/PIUPersoonlik.jsp?pLang=english&PIIType=A', 'Update Personal info', true));
lvl2_index12.push(new URL('http://web-apps.sun.ac.za/PIUpdate/PIUAdres.jsp?pLang=english&PIAType=A', 'Address\/Contact info', true));
*/

//myURLs[2].children[0] = lvl2Array;
myURLs[1].children[0] = lvl2_index1;
myURLs[5].children[0] = lvl2_index4;
//myURLs[10].children[0] = lvl2_index10;
//myURLs[12].children[0] = lvl2_index12;

lvl2_index1[2].children[0] = lvl3_index2;



    function UnorderedList() {
        this.UL = function (_urlArray) {
            this.ulList = document.createElement("ul");      // The unordered list node
            this.ulList.setAttribute("class", "flyout-lvl2");
            this.liItem = new Array();                       // The array containing all the list item nodes

            for (i = 0; i < _urlArray.length; i++) {

                /* Begin: build the list item */
                this.liItem[i] = document.createElement("li");
                this.url = document.createElement("a");
                this.url.setAttribute("href", _urlArray[i].url);
                this.url.appendChild(document.createTextNode(_urlArray[i].name));

                if (_urlArray[i].targetBlank != false) {
                    this.url.setAttribute("target","blank");
                }

                this.liItem[i].appendChild(this.url);
                /* End: build the list item */

                /* level 3 */
                if (_urlArray[i].children[0] != null && _urlArray[i].children[0].length > 0) {

                    /* Give list item flyout image background */
                    this.liItem[i].setAttribute("class", "flyout");
                    var linkListChild = _urlArray[i].children[0];           // Get the array of children
                    var ulLevel3 = new UnorderedList2();
                    this.liItem[i].appendChild(ulLevel3.UL(linkListChild));
                }

            }
            i = 0;

            for (j = 0; j < this.liItem.length; j++) {
                this.ulList.appendChild(this.liItem[j]);
            }
            j = 0;
            return this.ulList;
        };

    return true;
}


function UnorderedList2() {
    this.UL = function (_urlArray) {
        this.ulList = document.createElement("ul");      // The unordered list node
        this.ulList.setAttribute("class", "flyout-lvl3");
        this.liItem = new Array();                       // The array containing all the list item nodes

        for (m = 0; m < _urlArray.length; m++) {

            /* Begin: build the list item */
            this.liItem[m] = document.createElement("li");
            this.url = document.createElement("a");
            this.url.setAttribute("href", _urlArray[m].url);
            this.url.appendChild(document.createTextNode(_urlArray[m].name));
            this.liItem[m].appendChild(this.url);

            if (_urlArray[m].targetBlank != false) {
                this.url.setAttribute("target", "blank");
            }
            /* End: build the list item */

        }
        m = 0;

        for (n = 0; n < this.liItem.length; n++) {
            this.ulList.appendChild(this.liItem[n]);
        }
        n = 0;
        return this.ulList;
    };

    return true;
}

function linkList(parentNode, _myUrls) {
    /*try {*/

        var pNode = parentNode;
        var urlArray = _myUrls;
        var ulList = document.createElement("ul");

        var listItemArray = new Array();
        //var liItem = new Array();


        for (k = 0; k < urlArray.length; k++) {

            /* Only add list-items with URLs that are NOT blank */
            if (urlArray[k].url != ' ') {
                /* Begin: build the list item */
                var liItem = document.createElement("li");
                var liItemurl = document.createElement("a");

                if (urlArray[k].url != 'no-url' && urlArray[k].url != null) {
                    liItemurl.setAttribute("href", urlArray[k].url);

                    if (urlArray[k].targetBlank != false) {
                        liItemurl.setAttribute("target", "blank");
                    }

                }
                else if(urlArray[k].url != ' ') {
                    liItem.setAttribute("class", "static");
                    liItemurl.setAttribute("style", "cursor:default;"); 
                }
                else
                { liItemurl.setAttribute("style", "cursor:default;"); }

                liItemurl.appendChild(document.createTextNode(urlArray[k].name));
                liItem.appendChild(liItemurl);
                /* End: build the list item */

                /* level 2 */
                if (urlArray[k].children[0] != null && urlArray[k].children[0].length > 0) {

                    /* Give list item flyout image background */
                    liItem.setAttribute("class", "flyout");
                    var linkListChild = urlArray[k].children[0];           // Get the array of children
                    var ulLevel2 = new UnorderedList();
                    liItem.appendChild(ulLevel2.UL(linkListChild));
                }

                listItemArray.push(liItem);

            }          
            
        }

        for (z = 0; z < listItemArray.length; z++) {
            ulList.appendChild(listItemArray[z]);
        }

            pNode.appendChild(ulList);
    /*}
    catch (e) { alert(e.get_Description); }*/
}

var lvl0 = new UnorderedList();
var root = document.getElementById("col2");

linkList(root, myURLs);

