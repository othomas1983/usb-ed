﻿//fixMozillaZIndex = true; //Fixes Z-Index problem with Mozilla browsers but causes odd scrolling problem, toggle to see if it helps
_menuCloseDelay = 800;
_menuOpenDelay = 100;
_subOffsetTop = 0;
_subOffsetLeft = 0;
var UserImageURL = "http://" + top.location.host + "/";


with (styMainMenu = new mm_style()) {
    styleid = 1;
    bordercolor = "#5E5A5B";
    borderstyle = "solid";
    borderwidth = 1;
    fontfamily = "Verdana";
    fontsize = "10pt";
    fontstyle = "normal";
    fontweight = "normal";
    headercolor = "#000000";
    offbgcolor = "#E7D0D7";
    offcolor = "#333333";
    onborder = "1px solid #FFFFFF";
    onbgcolor = "#891536";
    oncolor = "#FFFFFF";
    onsubimage = UserImageURL + "Common/Images/menu-white-arrow.gif";
    pagebgcolor = "#95A3A3";
    pagecolor = "#FFFFFF";
    outfilter = "fade(duration=0.5)";
    padding = 5;
    separatorcolor = "#FFFFFF";
    separatorsize = 1;
    subimage = UserImageURL + "Common/Images/menu-grey-arrow.gif";
    subimagepadding = 4;
}

with (stySubMainMenu = new mm_style()) {
    styleid = 1;
    bordercolor = "#5E5A5B";
    borderstyle = "solid";
    borderwidth = 1;
    fontfamily = "Verdana";
    fontsize = "8pt";
    fontstyle = "normal";
    fontweight = "normal";
    headercolor = "#000000";
    offbgcolor = "#E7D0D7";
    offcolor = "#333333";
    onborder = "1px solid #FFFFFF";
    onbgcolor = "#891536";
    oncolor = "#FFFFFF";
    onsubimage = UserImageURL + "Common/Images/menu-white-arrow.gif";
    pagebgcolor = "#95A3A3";
    pagecolor = "#FFFFFF";
    outfilter = "fade(duration=0.5)";
    padding = 5;
    separatorcolor = "#FFFFFF";
    separatorsize = 1;
    subimage = UserImageURL + "Common/Images/menu-grey-arrow.gif";
    subimagepadding = 4;
}

with (mnuDiscoverSubmenu = new menuname("NewsAndEventsSubmenu")) {
    style = styMainMenu;
    alwaysvisible = 1;
    position = "relative";
    align = "bottom";
    itemwidth = "100%";
    menuwidth = "100%";

    aI("text=Events calendar;url=" + UserImageURL + "NewsAndEvents/EventsCalendar.aspx;");
    aI("text=Leader's Angle;url=" + UserImageURL + "NewsAndEvents/LeadersAngle.aspx;");
    aI("text=In the News;url=" + UserImageURL + "NewsAndEvents/InTheNews.aspx;");
    aI("text=USB Newsroom;showmenu=NewsAndEventsSubmenuNewsroom;url=" + UserImageURL + "NewsAndEvents/NewsItemsList.aspx;");
    aI("text=Newsletters;showmenu=NewsAndEventsSubmenuNewsletters;url=" + UserImageURL + "NewsAndEvents/Newsletters/Default.aspx;");
    //aI("text=Event Streaming;url=" + UserImageURL + "NewsAndEvents/Streaming/Default.aspx;");
}

with (mnuDiscoverSubmenu = new menuname("NewsAndEventsSubmenuNewsroom")) {
    style = stySubMainMenu;
    overflow = "scroll";
    itemwidth = "100%";
    menuwidth = "175";

    aI("text=Alumni Newsroom;url=" + UserImageURL + "NewsAndEvents/AlumniNewsItemsList.aspx;");
    aI("text=Staff Newsroom;url=" + UserImageURL + "NewsAndEvents/StaffNewsItemsList.aspx;");
    aI("text=Student Newsroom;url=" + UserImageURL + "NewsAndEvents/StudentNewsItemsList.aspx;");
    aI("text=USB Newsroom Archive;url=" + UserImageURL + "NewsAndEvents/NewsItemsListArchive.aspx;");
}

with (mnuDiscoverSubmenu = new menuname("NewsAndEventsSubmenuNewsletters")) {
    style = stySubMainMenu;
    overflow = "scroll";
    itemwidth = "100%";
    menuwidth = "175";

    aI("text=Subscribe / unsubscribe;url=" + UserImageURL + "NewsAndEvents/Newsletters/SubscribeOrUnsubscribe.aspx;");
    aI("text=Advertise in the newsletter;url=" + UserImageURL + "NewsAndEvents/Newsletters/Advertise.aspx;");
}

drawMenus();