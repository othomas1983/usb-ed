﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EventItem.aspx.cs" Inherits="NewsAndEvents_EventItem" %>
<%@ Register TagPrefix="IncludeFile" TagName="Header" Src="Common/UserControls/HeaderUserControl.ascx" %>
<%@ Register TagPrefix="IncludeFile" TagName="Footer" Src="Common/UserControls/FooterUserControl.ascx"  %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>USB | News And Events | Event Item</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href="//usbevents.belpark.sun.ac.za/Common/StyleSheets/sub-style.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="//usbevents.belpark.sun.ac.za/Common/StyleSheets/block-style.css" rel="stylesheet" type="text/css" media="screen" />
</head>
    <script type="text/javascript" src="//usbevents.belpark.sun.ac.za/Common/Scripts/milonic_src.js"></script> 
    <script type="text/javascript" src="//usbevents.belpark.sun.ac.za/Common/Scripts/mmenudom.js"></script>
<body>

<%--<div id="outer">

<div id="container">
	<div id="header">		
        <!-- HeaderUserControl goes here -->
	    <IncludeFile:Header ID="Header1" runat="server" />	
	</div>
	<!--End Header-->
	
	<div id="topbar-newsandevents">
	    &nbsp;
	</div>--%>
	<!--End Topbar-->
    <%--<div id="wrapper">	
		<div id="leftnav">		
			<script type="text/javascript" src="<%=ConfigurationManager.AppSettings["UserImageURL"]%>Common/Scripts/usb_left_menu_newsandevents.js"></script>
		</div>
		<!--End Leftnav-->
		
		<div id="right">
			
			<div class="rightbox">				
				<img src="Common/Images/AdministeredImages/NoneCrossLinked/6_1_1_NewsAndEvents.jpg" width="180" height="152" onerror="this.onerror=null;this.src='Common/Images/AdministeredImages/NoneCrossLinked/6_1_NewsAndEvents.jpg'"  alt="" />
			</div>		     
			<!--End Rightbox-->

		</div>
		<!--End Right-->

		<div id="content">--%>
		<form id="form1" runat="server">
		    <p><asp:ImageButton ID="RSSFeedLinkBtn" runat="server" ImageUrl="/Common/StyleSheets/images/RSS.png" OnClick="RSSFeedLinkBtn_Click" OnClientClick="form1.target ='_blank';" ToolTip="Click here to subscribe to Events RSS Feed!" /></p>
		
		    <asp:HiddenField ID="strTheValue" runat="server" />
        
            <asp:Table ID="Table1" runat="server" Width="100%">
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2" Width="100%"> <span style="font-weight: bold;"><asp:Label ID="lblEventName" runat="server"></asp:Label></span> </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="tblDetails" runat="server" Visible="false" Width="100%">
                            <asp:TableRow>
                                <asp:TableCell ColumnSpan="2" Width="100%"> &nbsp; </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell VerticalAlign="Top" Width="15%"> <span style="font-weight: bold;"><asp:Label ID="lblDetails" runat="server" Text="Details:"></asp:Label></span> </asp:TableCell>
                                <asp:TableCell VerticalAlign="Top" Width="85%"> <asp:Label ID="lblDetailsDisplay" runat="server"></asp:Label> </asp:TableCell>
                            </asp:TableRow>                
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2" Width="100%">  
                        <asp:Table ID="tblOnlineBookingForm" runat="server" Visible="false" Width="100%">
                             <asp:TableRow>
                                <asp:TableCell ColumnSpan="2" Width="100%"> &nbsp; </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell><span style="text-align: center; display: block;"><asp:Button ID="iIDLnkBtn" OnCommand="LinkBtniID_Command" runat="server" Text="BOOK HERE" BackColor="#891536" ForeColor="White" CssClass="button" style="color:White;background-color: transparent;font-size: 12pt; background-image: url('/Common/StyleSheets/images/BookNow.png');width: 209px;height: 48px;border: none;background-repeat: no-repeat;"/></span></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="tblDate" runat="server" Visible="false" Width="100%">
                            <asp:TableRow>
                                <asp:TableCell ColumnSpan="2" Width="100%"> &nbsp; </asp:TableCell>
                            </asp:TableRow>
							
                            <asp:TableRow>                    
                                <asp:TableCell Width="15%"> <span style="text-align: center; font-weight: bold;"><asp:Label ID="lblDate" runat="server" Text="Date:"></asp:Label></span> </asp:TableCell>
                                <asp:TableCell Width="85%"> <asp:Label ID="lblDateDisplay" runat="server"></asp:Label> </asp:TableCell>
                            </asp:TableRow>                    
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>         
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="tblVenue" runat="server" Visible="false" Width="100%">
                            <asp:TableRow>
                                <asp:TableCell ColumnSpan="2" Width="100%"> &nbsp; </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Width="15%"> <span style="text-align: center; font-weight: bold;"><asp:Label ID="lblVenue" runat="server" Text="Venue:"></asp:Label></span> </asp:TableCell>
                                <asp:TableCell Width="85%"> <asp:Label ID="lblVenueDisplay" runat="server"></asp:Label> </asp:TableCell>
                            </asp:TableRow>         
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="tblTime" runat="server" Visible="false" Width="100%">
                            <asp:TableRow>
                                <asp:TableCell ColumnSpan="2" Width="100%"> &nbsp; </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Width="15%"> <span style="text-align: center; font-weight: bold;"><asp:Label ID="lblTimes" runat="server" Text="Times:"></asp:Label></span> </asp:TableCell>
                                <asp:TableCell Width="85%"> <asp:Label ID="lblTimesDisplay" runat="server"></asp:Label> </asp:TableCell>
                            </asp:TableRow>         
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="tblCost" runat="server" Visible="false" Width="100%">
                            <asp:TableRow>
                                <asp:TableCell ColumnSpan="2" Width="100%"> &nbsp; </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Width="15%"> <span style="text-align: center; font-weight: bold;"><asp:Label ID="lblCost" runat="server" Text="Cost:"></asp:Label></span> </asp:TableCell>
                                <asp:TableCell Width="85%"> <asp:Label ID="lblCostDisplay" runat="server"></asp:Label> </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="tblContactPerson" runat="server" Visible="false" Width="100%">
                            <asp:TableRow>
                                <asp:TableCell ColumnSpan="2" Width="100%"> &nbsp; </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Width="15%"> <span style="text-align: center; font-weight: bold;"><asp:Label ID="lblContactPerson" runat="server" Text="Contact Person:"></asp:Label></span> </asp:TableCell>
                                <asp:TableCell Width="85%"> <asp:Label ID="lblContactPersonDisplay" runat="server"></asp:Label> </asp:TableCell>
                            </asp:TableRow>         
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="tblPhoneNo" runat="server" Visible="false" Width="100%">
                            <asp:TableRow>
                                <asp:TableCell ColumnSpan="2" Width="100%"> &nbsp; </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Width="15%"> <span style="text-align: center; font-weight: bold;"><asp:Label ID="lblPhoneNo" runat="server" Text="Phone Number:"></asp:Label></span> </asp:TableCell>
                                <asp:TableCell Width="85%"> <asp:Label ID="lblPhoneNoDisplay" runat="server"></asp:Label> </asp:TableCell>
                            </asp:TableRow>         
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>                 
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="tblEmailAddress" runat="server" Visible="false" Width="100%">
                            <asp:TableRow>
                                <asp:TableCell ColumnSpan="2" Width="100%"> &nbsp; </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Width="15%"> <span style="text-align: center; font-weight: bold;"><asp:Label ID="lblEmailAddress" runat="server" Text="E-mail Address:"></asp:Label></span> </asp:TableCell>
                                <asp:TableCell Width="85%"> <asp:Label ID="lblEmailAddressDisplay" runat="server"></asp:Label> </asp:TableCell>
                            </asp:TableRow>         
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2" Width="100%"> &nbsp; </asp:TableCell>
                </asp:TableRow>                                
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2" Width="100%">
                        <!-- AddThis Button BEGIN -->
                        <div class="addthis_toolbox addthis_default_style">
                            <a href="http://www.addthis.com/bookmark.php?v=250&amp;username=xa-4b7e96a83b9241bb" class="addthis_button_compact">Share</a>
                            <span class="addthis_separator">|</span>
                            <a class="addthis_button_facebook"></a>
                            <a class="addthis_button_myspace"></a>
                            <a class="addthis_button_google"></a>
                            <a class="addthis_button_twitter"></a>
                        </div>
                        <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#username=xa-4b7e96a83b9241bb"></script>
                        <!-- AddThis Button END -->
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ColumnSpan="2" Width="100%"> &nbsp; </asp:TableCell>
                </asp:TableRow>
                <%--<asp:TableRow>
                    <asp:TableCell ColumnSpan="2" Width="100%"> <span style="text-align: center; display: block;"><asp:LinkButton ID="LnkBtnEventsList" runat="server" PostBackUrl="EventsCalendar.aspx" Text="View all upcoming USB events"></asp:LinkButton></span> </asp:TableCell>                                                                                                                        
                </asp:TableRow>--%>
            </asp:Table>	    
				           	
		</form>	
		<%--</div>
     
		<!--End Content-->		
		
	</div>
	<!--End Wrapper-->			
		
	<div id="footer">
	    <!-- FooterUserControl goes here -->
    	<IncludeFile:Footer ID="Footer1" runat="server" />					
	</div>
	<!--End Footer-->
	
	<div id="copyright">
        Copyright 2009 University of Stellenbosch Business School. All rights reserved.
    </div>--%>
    <!--End Copyright-->

<%--</div><!--End Container-->
</div><!--End Outer-->--%>
    <!-- AddThisEvent -->
<script type="text/javascript" src="http://js.addthisevent.com/atemay.js"></script>
    <!-- AddThisEvent Settings -->
<script type="text/javascript">
    addthisevent.settings({
        license: "aao8iuet5zp9iqw5sm9z",
        mouse: false,
        css: true,
        outlook: { show: true, text: "Outlook Calendar" },
        google: { show: true, text: "Google Calendar" },
        yahoo: { show: true, text: "Yahoo Calendar" },
        hotmail: { show: true, text: "Hotmail Calendar" },
        ical: { show: true, text: "iCal Calendar" },
        facebook: { show: true, text: "Facebook Event" },
        dropdown: { order: "outlook,google,ical" },
        callback: ""
    });
</script>
</body>
</html>
