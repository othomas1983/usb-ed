﻿#region Using Directive
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using System.Text.RegularExpressions;

using USB_ED_CRM_4.CrmWsdl;
#endregion

namespace USB_ED_CRM_4
{
    public partial class BookingForm : System.Web.UI.Page
    {
        private CrmWsdl.CrmService _svc = new CrmWsdl.CrmService();

        private static string errorMsg = "An error has occurred while processing your booking.\\n\\nPlease try again later.";

        public void initialiseCRMService()
        {
            CrmAuthenticationToken token = new CrmAuthenticationToken();
            token.AuthenticationType = 0;
            token.OrganizationName = "US-BPC";

            _svc.CrmAuthenticationTokenValue = token;
            _svc.PreAuthenticate = true;
            _svc.Credentials = System.Net.CredentialCache.DefaultCredentials;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack && !Page.IsCallback)
            {
                //Set no-cache
                Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache);
            }

            //Set form object properties            
            ContactTable.Attributes.Add("style", "width: 100%;");
            tblFooter.Attributes.Add("style", "width: 100%;");

            plhBookingForm.Visible = true;
            plhBookingConfirm.Visible = false;

            SubmitButtonUpdate.Text = "Submit Form";            

            LoadForm();
        }


        private void LoadForm()
        {
            initialiseCRMService();            
                        
            //Set no-cache
            Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache);

            //Get QueryString value(s)
            string iID = Request.QueryString["iID"];

            //Write iID to hidden label
            lbliID.Text = iID;

            //Check Campaign GUID before doing any processing
            if (iID != null || iID != "")
            {
                //if (lbliID.Text != null)
                //{
                //Retrieve all active (launched) campaigns
                QueryByAttribute nxtquery = new QueryByAttribute();
                nxtquery.ColumnSet = new AllColumns();
                nxtquery.EntityName = EntityName.campaign.ToString();
                nxtquery.Attributes = new string[] { "campaignid" };
                nxtquery.Values = new String[] { iID };

                BusinessEntityCollection retrieved = _svc.RetrieveMultiple(nxtquery);

                campaign GetCampaign = (campaign)retrieved.BusinessEntities[0];


                lblEventName.Text = GetCampaign.name;

                if (GetCampaign.msa_startdatetime != null && GetCampaign.msa_startdatetime.Value != "")
                {
                    DateTime dt = Convert.ToDateTime(GetCampaign.msa_startdatetime.Value.ToString());

                    lblEventDateTime.Text = dt.ToString("dd MMMM yyyy") + " | " + dt.ToString("HH:mm tt");
                }

                if (GetCampaign.statuscode.name != "Sold Out")
                {
                    //Set visibility of ContactTable
                    if (GetCampaign.lt_contactdetails != null)
                    {
                        if (GetCampaign.lt_contactdetails.Value == true)
                        {
                            ContactTable.Visible = true;
                        }
                        else
                        {
                            ContactTable.Visible = false;
                        }
                    }
                    else
                    {
                        ContactTable.Visible = false;
                    }

                    //Set visibility of PartnerFunctionTable
                    if (GetCampaign.lt_partnerfunction != null)
                    {
                        if (GetCampaign.lt_partnerfunction.Value == true)
                        {
                            PartnerFunctionTable.Visible = true;
                        }
                        else
                        {
                            PartnerFunctionTable.Visible = false;
                        }
                    }
                    else
                    {
                        PartnerFunctionTable.Visible = false;
                    }
                }
                else
                {
                    plhBookingForm.Visible = false;
                    plhEventClosed.Visible = true;
                }
            }
        }


        //Log file        
        public void LogFile(string sExceptionName, string sEventName)
        {
            StreamWriter log;

            string logName = DateTime.Now.ToString("yyyy-MM-dd") + ".log";

            if (!File.Exists(logName))
            {
                log = new StreamWriter(logName);
            }
            else
            {
                log = File.AppendText(logName);
            }

            //Write to the file
            log.WriteLine("Data Time: " + DateTime.Now);
            log.WriteLine("Exception Name: " + sExceptionName);
            log.WriteLine("Event Name: " + sEventName);

            //Close the stream
            log.Close();
        }


        protected void SubmitButtonUpdate_Click(object sender, EventArgs e)
        {
            string user_code = VerifiedCaptchaCodeTB.Text;
            string captcha_code = Session["CAPTCHA_CODE"].ToString();

            if (user_code != captcha_code)
            {
                VerifiedCaptchaCodeTB.Focus();
                CaptchaImageLbl.Text = "* Verification failed. Please retype the above verification code carefully.";
            }
            else
            {

                plhBookingForm.Visible = false;
                plhBookingConfirm.Visible = false;

                initialiseCRMService();

                //Retrieve all campaign related information
                AllColumns col = new AllColumns();

                //campaignGuid is the GUID of the record being retrieved
                Guid campaignGuid = new Guid(lbliID.Text);

                campaign GetCampaign = (campaign)_svc.Retrieve(EntityName.campaign.ToString(), campaignGuid, col);


                plhBookingConfirm.Visible = true;


                //Create a lead for the attendee
                lead newLead = new lead();

                //Create the activity party object
                activityparty attendee = new activityparty();

                //Create the campaign response object
                campaignresponse rsvp = new campaignresponse();


                //Contact Details
                newLead.lastname = SurnameInput.Text;
                newLead.firstname = NameInput.Text;                
                newLead.telephone1 = BusinessPhoneInput.Text;
                newLead.mobilephone = MobileNoInput.Text;
                newLead.emailaddress1 = EmailInput.Text;


                Guid newLeadGuid = _svc.Create(newLead);

                attendee.partyid = new Lookup();
                attendee.partyid.Value = newLeadGuid;
                attendee.partyid.type = EntityName.lead.ToString();

                //Create campaign response
                rsvp.customer = new activityparty[] { attendee };

                rsvp.regardingobjectid = new Lookup();
                rsvp.regardingobjectid.Value = (new Guid(lbliID.Text));
                rsvp.regardingobjectid.type = EntityName.campaign.ToString();
                rsvp.subject = "Online booking";

                rsvp.receivedon = new CrmDateTime();
                rsvp.receivedon.Value = DateTime.Today.ToString("yyyy-MM-ddTHH:mm:ss");

                //RSVP: Attending or Not attending
                rsvp.responsecode = new Picklist();
                rsvp.responsecode.Value = Convert.ToInt32(rblRsvpDetails.SelectedValue);                    

                //Update Partner Function Details
                if (GetCampaign.lt_partnerfunction != null && rblRsvpDetails.SelectedValue != "2")
                {
                    //With partner
                    if (rblPartnerDetails.SelectedValue == "1")
                    {
                        rsvp.lt_attendingwithpartner = new CrmBoolean();
                        rsvp.lt_attendingwithpartner.Value = true;

                        rsvp.lt_numberofattendees = new CrmNumber();
                        rsvp.lt_numberofattendees.Value = 2;
                    }

                    //No partner
                    if (rblPartnerDetails.SelectedValue == "0")
                    {
                        rsvp.lt_numberofattendees = new CrmNumber();
                        rsvp.lt_numberofattendees.Value = 1;
                    }
                }

                //Write Lead GUID to campaign response lt_leadid
                rsvp.lt_leadid = new Lookup();
                rsvp.lt_leadid.Value = newLeadGuid;


                try
                {
                    //Create the campaign response in CRM
                    Guid compaignResponseGuid = _svc.Create(rsvp);
                }

                catch (System.Web.Services.Protocols.SoapException ex)
                {
                    //Log error                
                    LogFile(ex.Message, e.ToString());
                                        
                    ClientScriptManager script = Page.ClientScript;
                    script.RegisterClientScriptBlock(this.GetType(), "An error has occurred", "<script type=text/javascript>alert('" + errorMsg + "')</script>");
                }

                catch (Exception ex)
                {
                    //Log error
                    LogFile(ex.Message, e.ToString());
                                        
                    ClientScriptManager script = Page.ClientScript;
                    script.RegisterClientScriptBlock(this.GetType(), "An error has occurred", "<script type=text/javascript>alert('" + errorMsg + "')</script>");                    
                }
            }
        }
    }
}