﻿#region Using Directive
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using System.Text.RegularExpressions;

using USB_ED_CRM_4.CrmWsdl;
#endregion

namespace USB_ED_CRM_4
{
    public partial class Registration : System.Web.UI.Page
    {
        private CrmWsdl.CrmService _svc = new CrmWsdl.CrmService();

        //private static string GetTime = DateTime.Now.ToString("ddMMyyyy_HHmmss");

        //public static string AppCodeOut;


        public void initialiseCRMService()
        {
            CrmAuthenticationToken token = new CrmAuthenticationToken();
            token.AuthenticationType = 0;
            token.OrganizationName = "US-BPC";

            _svc.CrmAuthenticationTokenValue = token;
            _svc.PreAuthenticate = true;
            _svc.Credentials = System.Net.CredentialCache.DefaultCredentials;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack && !Page.IsCallback)
            {
                //Set no-cache
                Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache);
            }

            //Set form object properties            
            ContactTable.Attributes.Add("style", "width: 100%;");
            tblFooter.Attributes.Add("style", "width: 100%;");

            plhBookingForm.Visible = true;
            plhBookingConfirm.Visible = false;

            SubmitButtonUpdate.Text = "Submit Form";

            LoadForm();
        }


        private void LoadForm()
        {
            initialiseCRMService();

            //Set no-cache
            Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache);

            //Get QueryString value(s)
            string iID = Request.QueryString["iID"];

            //Write iID to hidden label
            lbliID.Text = iID;

            //Check Campaign GUID before doing any processing
            if (iID != null || iID != "")
            {
                //if (lbliID.Text != null)
                //{
                //Retrieve all active (launched) campaigns
                QueryByAttribute nxtquery = new QueryByAttribute();
                nxtquery.ColumnSet = new AllColumns();
                nxtquery.EntityName = EntityName.campaign.ToString();
                nxtquery.Attributes = new string[] { "campaignid" };
                nxtquery.Values = new String[] { iID };

                BusinessEntityCollection retrieved = _svc.RetrieveMultiple(nxtquery);

                campaign GetCampaign = (campaign)retrieved.BusinessEntities[0];


                lblEventName.Text = GetCampaign.name;

                if (GetCampaign.msa_startdatetime != null && GetCampaign.msa_startdatetime.Value != "")
                {
                    DateTime dt = Convert.ToDateTime(GetCampaign.msa_startdatetime.Value.ToString());

                    lblEventDateTime.Text = dt.ToString("dd MMMM yyyy") + " | " + dt.ToString("HH:mm tt");
                }


                //Set visibility of ContactTable
                if (GetCampaign.lt_contactdetails != null)
                {
                    if (GetCampaign.lt_contactdetails.Value == true)
                    {
                        ContactTable.Visible = true;
                    }
                    else
                    {
                        ContactTable.Visible = false;
                    }
                }
                else
                {
                    ContactTable.Visible = false;
                }

            }
        }

        ////Building userRefCRM string
        //private string BuildUserRef()
        //{
        //    string FirstNameInitial = NameInput.Text.Substring(0, 1);

        //    Regex r = new Regex(@"\s");
        //    string TrimSurname = r.Replace(SurnameInput.Text, "");

        //    string userRefCRM = FirstNameInitial + TrimSurname + "_" + GetTime;

        //    return userRefCRM;
        //}

        protected void SubmitButtonUpdate_Click(object sender, EventArgs e)
        {
            string user_code = VerifiedCaptchaCodeTB.Text;
            string captcha_code = Session["CAPTCHA_CODE"].ToString();

            if (user_code != captcha_code)
            {
                VerifiedCaptchaCodeTB.Focus();
                CaptchaImageLbl.Text = "* Verification failed. Please retype the above verification code carefully.";
            }
            else
            {

                plhBookingForm.Visible = false;
                plhBookingConfirm.Visible = false;

                initialiseCRMService();

                //Retrieve all campaign related information
                AllColumns col = new AllColumns();

                //campaignGuid is the GUID of the record being retrieved
                Guid campaignGuid = new Guid(lbliID.Text);

                campaign GetCampaign = (campaign)_svc.Retrieve(EntityName.campaign.ToString(), campaignGuid, col);


                plhBookingConfirm.Visible = true;


                //Create a lead for the attendee
                lead newLead = new lead();

                //Create the activity party object
                activityparty attendee = new activityparty();

                //Create the campaign response object
                campaignresponse rsvp = new campaignresponse();


                //Contact Details
                newLead.lastname = SurnameInput.Text;
                newLead.firstname = NameInput.Text;
                newLead.companyname = CompanyInput.Text;
                newLead.jobtitle = JobTitleInput.Text;
                newLead.telephone1 = BusinessPhoneInput.Text;
                newLead.mobilephone = MobileNoInput.Text;
                newLead.emailaddress1 = EmailInput.Text;


                Guid newLeadGuid = _svc.Create(newLead);

                attendee.partyid = new Lookup();
                attendee.partyid.Value = newLeadGuid;
                attendee.partyid.type = EntityName.lead.ToString();

                //Create campaign response
                rsvp.customer = new activityparty[] { attendee };

                rsvp.regardingobjectid = new Lookup();
                rsvp.regardingobjectid.Value = (new Guid(lbliID.Text));
                rsvp.regardingobjectid.type = EntityName.campaign.ToString();
                rsvp.subject = "Online booking";

                rsvp.receivedon = new CrmDateTime();
                rsvp.receivedon.Value = DateTime.Today.ToString("yyyy-MM-ddTHH:mm:ss");


                //Write Lead GUID to campaign response lt_leadid
                rsvp.lt_leadid = new Lookup();
                rsvp.lt_leadid.Value = newLeadGuid;


                try
                {
                    //Create the campaign response in CRM
                    Guid compaignResponseGuid = _svc.Create(rsvp);
                }

                catch (System.Web.Services.Protocols.SoapException ex)
                {
                    System.Windows.Forms.MessageBox.Show(ex.Message + "." + ex.Detail.InnerText);
                }
            }
        }
    }
}