﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Registration.aspx.cs" Inherits="USB_ED_CRM_4.Registration" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>USB-ED Events</title>
    <meta property="og:title" content="Register now" />
    <meta property="og:description" content="USB-ED Event Booking Form" />
    <meta property="og:image" content="http://www.usb-ed.com/files/FBicon2013.gif" />

    <link rel="stylesheet" type="text/css" href="Common/Styles/Style.css" />

    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-9075615-1']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>
    
    <script type="text/javascript">
        var switchTo5x = true;
    </script>
    
    <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
    
    <script type="text/javascript">
        stLight.options({ publisher: '2bc46e2b-1a73-43c9-9f40-6999daa1203f' });
    </script>

</head>

<body style="background-color:#bfbfbf">

<div id="wrap">
    <form id="frmMain" runat="server">
    	<div id="grey">
    		<div id="logo">
                <a href="http://www.usb-ed.com" target="_blank">
                <img src="Common/Images/logo.jpg" style="margin:0 5px 0 36px" alt="USB-ED" border="none"/></a>
                <img src="Common/Images/events.gif" alt="Events"/>
            </div>
            <div id="heading">
                <h1>Booking Form</h1>

                <h2><asp:Label ID="lblEventName" runat="server"></asp:Label></h2> 
                <h3><asp:Label ID="lblEventDateTime" runat="server"></asp:Label></h3>                
            </div>
        </div>
            
 		<div id="input-fields">        
             
            <!-- Booking Form -->          
            <asp:PlaceHolder ID="plhBookingForm" runat="server" Visible="False">
                <asp:Label ID="leadGUID" runat="server" Visible="False"></asp:Label>    
                          
                <p>&nbsp;</p>                      
                                                      
                <asp:Panel ID="panelValidation" runat="server" CssClass="" style="width: 400px; margin: auto;">
                    <asp:ValidationSummary ID="ValSum" CssClass="" runat="server" DisplayMode="List" HeaderText="The Form could not be submitted for the following reasons:" style="padding: 10px; text-align:left;" BorderColor="Red" ForeColor="Red" BorderStyle="Solid" Font-Bold="False" BorderWidth="1px" />                                
                </asp:Panel>
              
                <asp:Table ID="BookingTable" runat="server" Width="100%">             
                    <asp:TableRow>
                        <asp:TableCell ColumnSpan="2">&nbsp;</asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell ColumnSpan="2">
                    
                            <!-- Contact Details -->
                            <asp:Table ID="ContactTable" Visible="false" runat="server">                            
                                <asp:TableRow>
                                    <asp:TableCell Width="50%">
                                        <asp:Label ID="Surname" CssClass="body" runat="server" Text="Surname:"></asp:Label> 
                                        <asp:Label ID="lblSurnameRequired" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell Width="50%">
                                        <asp:TextBox ID="SurnameInput" Columns="35" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvSurname" runat="server" ControlToValidate="SurnameInput" ErrorMessage="- Please insert your surname" Display="None">*</asp:RequiredFieldValidator>                                
                                    </asp:TableCell>                            
                                </asp:TableRow>                               
                                <asp:TableRow>
                                    <asp:TableCell Width="50%">
                                        <asp:Label ID="Name" CssClass="body" runat="server" Text="Name:"></asp:Label>
                                        <asp:Label ID="lblNameRequired" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell Width="50%">
                                        <asp:TextBox ID="NameInput" Columns="35" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="NameInput" ErrorMessage="- Please insert your name" Display="None">*</asp:RequiredFieldValidator>                                
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Width="50%"><asp:Label ID="Company" CssClass="body" runat="server" Text="Employer/Organisation:"></asp:Label></asp:TableCell>
                                    <asp:TableCell Width="50%"><asp:TextBox ID="CompanyInput" Columns="35" runat="server"></asp:TextBox></asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Width="50%"><asp:Label ID="JobTitle" CssClass="body" runat="server" Text="Job Title:"></asp:Label></asp:TableCell>
                                    <asp:TableCell Width="50%"><asp:TextBox ID="JobTitleInput" Columns="35" runat="server"></asp:TextBox></asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Width="50%"><asp:Label ID="BusinessPhone" CssClass="body" runat="server" Text="Business Phone:"></asp:Label></asp:TableCell>
                                    <asp:TableCell Width="50%"><asp:TextBox ID="BusinessPhoneInput" Columns="35" runat="server"></asp:TextBox></asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Width="50%">
                                        <asp:Label ID="MobileNo" CssClass="body" runat="server" Text="Mobile Phone:"></asp:Label>
                                        <asp:Label ID="lblMobileNoRequired" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell Width="50%">
                                        <asp:TextBox ID="MobileNoInput" Columns="35" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvMobileNo" runat="server" ControlToValidate="MobileNoInput" ErrorMessage="- Please insert your mobile phone number" Display="None">*</asp:RequiredFieldValidator>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Width="50%">
                                        <asp:Label ID="Email" CssClass="body" runat="server" Text="E-mail:"></asp:Label>
                                        <asp:Label ID="lblEmailRequired" runat="server" ForeColor="Red" Text="*"></asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell Width="50%">
                                        <asp:TextBox ID="EmailInput" Columns="35" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvUserEmail" runat="server" ControlToValidate="EmailInput" ErrorMessage="- Please insert your e-mail address" Display="None">*</asp:RequiredFieldValidator>                                        
                                    </asp:TableCell>
                                </asp:TableRow>                                                                       
                                <asp:TableRow>
                                    <asp:TableCell ColumnSpan="2">&nbsp;</asp:TableCell>            
                                </asp:TableRow>            
                                <asp:TableRow>
                                    <asp:TableCell ColumnSpan="2">&nbsp;</asp:TableCell>            
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Width="50%"><b>Verification code:</b></asp:TableCell>
                                    <asp:TableCell Width="50%"><img src="Common/Captcha/Captcha.aspx" alt="Verification Code" /> </asp:TableCell>                        
                                </asp:TableRow>                      
                                <asp:TableRow>
                                    <asp:TableCell Width="50%"><b>Enter the verification code:</b></asp:TableCell>
                                    <asp:TableCell Width="50%"><asp:TextBox ID="VerifiedCaptchaCodeTB" runat="server" CssClass="body"></asp:TextBox> </asp:TableCell>                        
                                </asp:TableRow>                        
                                <asp:TableRow>
                                    <asp:TableCell ColumnSpan="2"><asp:Label ID="CaptchaImageLbl" runat="server" Font-Names="Verdana" ForeColor="#891536" Font-Bold="true"></asp:Label></asp:TableCell>                      
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell ColumnSpan="2">&nbsp;</asp:TableCell>                      
                                </asp:TableRow>
                                <asp:TableRow ID="TableRow2" runat="server">
                                    <asp:TableCell>&nbsp;</asp:TableCell>
                                    <asp:TableCell ID="TableCell1" ColumnSpan="2" runat="server" HorizontalAlign="left">
                                        <asp:LinkButton runat="server" ID="ValidationHiddenBtn" style="display: none" />                                                                                               
                                        <asp:Button ID="SubmitButtonUpdate" name="SubmitButtonUpdate" CssClass="button" runat="server" OnClick="SubmitButtonUpdate_Click" Text="Submit Form (U)" />                                                
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell ColumnSpan="2">&nbsp;</asp:TableCell></asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell ColumnSpan="2"><asp:Label ID="lblDisplay" runat="server"></asp:Label></asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell ColumnSpan="2">&nbsp;</asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                                                
                        </asp:TableCell>                
                    </asp:TableRow>                    
                
                </asp:Table>
                        
                <asp:Label ID="lblTest" runat="server" Visible="false"></asp:Label>
                <asp:Label ID="lbliID" runat="server" Visible="false"></asp:Label>
            </asp:PlaceHolder>
                    
            <asp:PlaceHolder ID="plhBookingConfirm" runat="server" Visible="False">
                <asp:Table ID="tblBookingConfirm" runat="server">
                    <asp:TableRow>
                        <asp:TableCell>&nbsp;</asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>&nbsp;</asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>&nbsp;</asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell><asp:Label ID="lblSuccess" CssClass="body" runat="server" Text="Booking / Registration successful." ForeColor="red"></asp:Label></asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>&nbsp;</asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell><asp:Label ID="lblMessage" CssClass="body" runat="server" Text="Thank you."></asp:Label></asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>&nbsp;</asp:TableCell>
                    </asp:TableRow>
                    <%--<asp:TableRow>
                        <asp:TableCell>&nbsp;<asp:Button ID="btnReturn" CssClass="button" runat="server" Text="Return to We Read For You" PostBackUrl="http://www.usb-ed.com/wrfy" /></asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                            <asp:TableCell>&nbsp;</asp:TableCell>
                    </asp:TableRow>--%>
                </asp:Table>
            </asp:PlaceHolder>
                    
            <asp:PlaceHolder ID="plhFooter" runat="server">
                <asp:Table ID="tblFooter" runat="server">
                    <asp:TableRow>
                        <asp:TableCell CssClass="">&nbsp;</asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:PlaceHolder>

        </div>
        <div id="rightImg">
            <img src="Common/Images/circle.png" alt="Your journey starts here"/>
        </div> 
        <div id="footer">           
            <table width="899" cellpadding="0" cellspacing="2" bgcolor="#e9e7d0" style="font-family:Arial, Helvetica, sans-serif;font-size:10px;color:#443f35;">
                <tr>
					<td colspan="15">
                        <table width="891">
                            <tr>
                                <td width="10"></td>
                                <td valign="top">
                                    <img width="120px" src="Common/Images/Africa.png" alt="Africa"/>
                                </td>
                                <td width="10"></td>
                            	<td style="font-family:Arial, Helvetica, sans-serif;font-size:16px;text-align:left;color:#54534a;" colspan="15"><br />
                                    <span style="font-family:Tahoma, Geneva, sans-serif;font-size:24px;color:#8a8728;"><strong>CONTACT US</strong></span><br />
                                    Interventions may be presented in-house, anywhere in Africa<br />
                                    <strong><a href="http://www.usb-ed.com" target="_blank"><u><span style="color:#8a8728;font-size:12px;line-height:20px;">Click here to view our website</span></u></a></strong><br /><br /><br />
    						    </td>
                            </tr>
                        </table>
                    </td>                            
                </tr>
                <tr>
                    <td width="20"></td>
                    <td style="text-align:left;" valign="top">
    					<span style="font-size:12px"><strong>Western Cape,<br />
                            South Africa</strong></span><br />
							Tel: +27 (0)21 918 4488<br />
   			 				Email: <a href="mailto:info@usb-ed.com?subject=ED Focus enquiry" target="_blank">info@usb-ed.com</a>
                    </td>
                    <td width="10"></td>
                    <td bgcolor="#c1c1c1" align="left" valign="top" width="1" ></td>
                    <td width="10"></td>
                    <td  valign="top" style="text-align:left;">
    					<span style="font-size:12px"><strong>Gauteng,<br />
                            South Africa</strong></span><br />
							Tel: +27 (0)11 460 6980<br />
    						Email: <a href="mailto:info@usb-ed.com?subject=ED Focus enquiry" target="_blank">info@usb-ed.com</a>
    				</td>
                    <td width="10"></td>
                    <td bgcolor="#c1c1c1" align="left" valign="top" width="1" ></td>
                    <td width="10"></td>
                    <td valign="top" style="text-align:left;">
    						<span style="font-size:12px"><strong>Kwa-Zulu Natal,<br />
                                South Africa </strong></span><br />
								Tel: +27 (0)31 535 7117<br />
    							Email: <a href="mailto:info@usb-ed.com?subject=ED Focus enquiry" target="_blank">info@usb-ed.com</a>
    						</td>
                    <td width="10"></td>
                    <td width="10"></td>
                    <td bgcolor="#c1c1c1" align="left" valign="top" width="1" ></td>
                    <td width="10"></td>
     				<td valign="top" style="text-align:left;">
    					<span style="font-size:12px"><strong>Africa</strong></span><br />
    						<table cellpadding="0" cellspacing="0" style="font-family:Arial, Helvetica, sans-serif;font-size:10px;color:#443f35">
                                <tr>
                                    <td>Tel:</td>
                                    <td>+27 (0)82 415 8484</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>+27(0)11 460 6988</td>
    							</tr>
                            </table>
    								
                            Email: <a href="mailto:africa@usb-ed.com" target="_blank">africa@usb-ed.com</a><br /><br />
                    </td>
                </tr>
                <tr>
                    <td valign="top"  colspan="15" align="center">
                        <br /><br />
                                                                     
                        <table align="centre"><tr><td >
                            <a href="http://www.facebook.com/USBED" target="_blank">
                            <img src="Common/Images/f.gif" border="0"/></a>&nbsp; </td>
                            <td valign="top">
                            <a href="http://twitter.com/USB_ED" target="_blank">
                            <img src="Common/Images/tw.gif" border="0"/></a>&nbsp; </td>
                            <td valign="top">
                            <a href="https://www.linkedin.com/company/usb-executive-development-ltd?trk=biz-companies-cym" target="_blank">
                            <img src="Common/Images/li.gif" border="0"/></a>&nbsp; </td>
                            </tr>
                        </table>
 					</td>
                </tr>
            </table>
		        
        </div>
    </form>
</div>
</body>
</html>