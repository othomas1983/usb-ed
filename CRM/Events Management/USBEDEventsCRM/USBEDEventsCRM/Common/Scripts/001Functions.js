﻿// Returns the Jquery object of the element using JQuery to find ASP.NET control ID
getElementObject = function (elementId) {
    // Returns all elements that end with the elementId
    return $("[id$='" + elementId + "']");
}

// Returns the Jquery object of the element using JQuery to find ASP.NET control ID
getElementObjectWithContext = function (elementId, context) {
    // Returns all elements that end with the elementId
    return $("[id$='" + elementId + "']", context);
}

// Returns the element using JQuery to find ASP.NET control ID  - includes context for speed
getElementWithContext = function (elementId, context) {
    // Returns all elements that end with the elementId
    return $("[id$='" + elementId + "']", context);
}

// Returns the value of an element using JQuery to find ASP.NET control ID
getElementValue = function (elementId) {
    // Returns all elements that end with the elementId
    return $(getElement(elementId)).val();
}

// Returns the value of an element using JQuery to find ASP.NET control ID - includes context for speed
getElementValueWithContext = function (elementId, context) {
    // Returns all elements that end with the elementId
    return $(getElementWithContext(elementId, context)).val();
}


// Returns the value of an element using JQuery to find ASP.NET control ID
getElementHTML = function (elementId) {
    // Returns all elements that end with the elementId
    return $(getElement(elementId)).html();
}

// Returns the value of an element using JQuery to find ASP.NET control ID - includes context for speed
getElementHTMLWithContext = function (elementId, context) {
    // Returns all elements that end with the elementId
    return $(getElementWithContext(elementId, context)).html();
}

// Replaces the illegal URL characters for folder name
getFolderName = function (folderName)
{
    folderName = folderName.replace("#", "_");
    folderName = folderName.replace("%", "_");
    folderName = folderName.replace("&", "_");
    folderName = folderName.replace("*", "_");
    folderName = folderName.replace("{", "_");
    folderName = folderName.replace("}", "_");
    folderName = folderName.replace("\\", "_");
    folderName = folderName.replace(":", "_");
    folderName = folderName.replace("<", "_");
    folderName = folderName.replace(">", "_");
    folderName = folderName.replace("?", "_");
    folderName = folderName.replace("/", "_");
    folderName = folderName.replace("+", "_");

    return folderName;
}

function getParameterByName(name)
{
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.href);
    if (results == null)
        return "";
    else
        return decodeURIComponent(results[1].replace(/\+/g, " "));
}

// Displays and then fades out the Error Message
function ErrorMessage(fadeSpeed, timeOut, errorMsg)
{
    errorMsg = errorMsg.replace(/&lt;/g, '<');
    errorMsg = errorMsg.replace(/&gt;/g, '>');

    $("#lblError").html(errorMsg);
    $("#pnlContainer").attr("style", "visibility: visible;");
    $(".PageError").fadeIn(function ()
    {
        if (timeOut > 0)
        {
            setTimeout(function ()
            {
                $(".PageError").fadeOut(fadeSpeed);
                $("#pnlContainer").attr("style", "visibility: hidden;");
            }, timeOut);
        }
    });
}

// Displays and then fades out the Success Message
function SuccessMessage(fadeSpeed, timeOut, successMsg)
{
    successMsg = successMsg.replace(/&lt;/g, '<');
    successMsg = successMsg.replace(/&gt;/g, '>');

    $("#lblMessage").html(successMsg);
    $("#pnlContainer").attr("style", "visibility: visible;");
    $(".PageMessage").fadeIn(function ()
    {
        if (timeOut > 0)
        {
            setTimeout(function ()
            {
                $(".PageMessage").fadeOut(fadeSpeed);
                $("#pnlContainer").attr("style", "visibility: hidden;");
            }, timeOut);
        }
    });
}

// Preload Images Function
(function ($)
{
    var cache = [];
    // Arguments are image paths relative to the current page.
    $.preLoadImages = function ()
    {
        var args_len = arguments.length;
        for (var i = args_len; i--; )
        {
            var cacheImage = document.createElement('img');
            cacheImage.src = arguments[i];
            cache.push(cacheImage);
        }
    }
})(jQuery)