/// <reference path="Xrm.js" />

var EntityLogicalName = "usb_studentacademicrecord";
var Form_f06046d0_e5ff_47d3_acf3_611f276f3d35_Properties = {
    ownerid: "ownerid",
    statuscode: "statuscode",
    usb_academicsupportoptionset: "usb_academicsupportoptionset",
    usb_acceptanceformtwooptions: "usb_acceptanceformtwooptions",
    usb_acquirebusinessknowledgeoptionset: "usb_acquirebusinessknowledgeoptionset",
    usb_acquiredevfinskillsoptionset: "usb_acquiredevfinskillsoptionset",
    usb_acquiremanagementskillsoptionset: "usb_acquiremanagementskillsoptionset",
    usb_adminsupportoptionset: "usb_adminsupportoptionset",
    usb_advertisementtwooptions: "usb_advertisementtwooptions",
    usb_africaemergingmarketoptionset: "usb_africaemergingmarketoptionset",
    usb_africamultinationalenvironmentoptionset: "usb_africamultinationalenvironmentoptionset",
    usb_africastrategicdecisionoptionset: "usb_africastrategicdecisionoptionset",
    usb_africasustainabledevelopmentoptionset: "usb_africasustainabledevelopmentoptionset",
    usb_afrikaansleesoptionset: "usb_afrikaansleesoptionset",
    usb_afrikaanspraatoptionset: "usb_afrikaanspraatoptionset",
    usb_afrikaansskryfoptionset: "usb_afrikaansskryfoptionset",
    usb_afrikaansverstaanoptionset: "usb_afrikaansverstaanoptionset",
    usb_alignforfuturistoptionset: "usb_alignforfuturistoptionset",
    usb_analyticalwriting: "usb_analyticalwriting",
    usb_applicationfeerefno: "usb_applicationfeerefno",
    usb_applicationfeetwooptions: "usb_applicationfeetwooptions",
    usb_applicationformurl: "usb_applicationformurl",
    usb_aquirescareskillsoptionset: "usb_aquirescareskillsoptionset",
    usb_aquirethinkingskillsoptionset: "usb_aquirethinkingskillsoptionset",
    usb_attendedinfosessiontwooptions: "usb_attendedinfosessiontwooptions",
    usb_authenticleadershipoptioset: "usb_authenticleadershipoptioset",
    usb_availablecareeroptionset: "usb_availablecareeroptionset",
    usb_benefitfromuniquetypeoptionset: "usb_benefitfromuniquetypeoptionset",
    usb_blendedlearningoptionset: "usb_blendedlearningoptionset",
    usb_brochuretwooptions: "usb_brochuretwooptions",
    usb_bursaryapplicanttwooptions: "usb_bursaryapplicanttwooptions",
    usb_bursaryawardeetwootions: "usb_bursaryawardeetwootions",
    usb_businessleaderoptionset: "usb_businessleaderoptionset",
    usb_campussecurityoptionset: "usb_campussecurityoptionset",
    usb_careerchangeoptionset: "usb_careerchangeoptionset",
    usb_cateringservicesoptionset: "usb_cateringservicesoptionset",
    usb_classroombasedoptionset: "usb_classroombasedoptionset",
    usb_closetohometwooptions: "usb_closetohometwooptions",
    usb_complexorgenvironmentoptionset: "usb_complexorgenvironmentoptionset",
    usb_conferenceexpotwooptions: "usb_conferenceexpotwooptions",
    usb_contactlookup: "usb_contactlookup",
    usb_deliverymodetwooptions: "usb_deliverymodetwooptions",
    usb_depositrefno: "usb_depositrefno",
    usb_developasaleaderoptionset: "usb_developasaleaderoptionset",
    usb_developefuturestudiesoptionset: "usb_developefuturestudiesoptionset",
    usb_developmentofothersoptionset: "usb_developmentofothersoptionset",
    usb_digitalskillsoptionset: "usb_digitalskillsoptionset",
    usb_diverseclassgroupoptionset: "usb_diverseclassgroupoptionset",
    usb_diversefaultycompositeoptionset: "usb_diversefaultycompositeoptionset",
    usb_electronicstudymaterialoptionset: "usb_electronicstudymaterialoptionset",
    usb_employerassistfinanciallyoptionset: "usb_employerassistfinanciallyoptionset",
    usb_employeremail: "usb_employeremail",
    usb_employername: "usb_employername",
    usb_employertelephone: "usb_employertelephone",
    usb_englishreadoptionset: "usb_englishreadoptionset",
    usb_englishspeakoptionset: "usb_englishspeakoptionset",
    usb_englishunderstandoptionset: "usb_englishunderstandoptionset",
    usb_englishwriteoptionset: "usb_englishwriteoptionset",
    usb_enhancecoachingskillsoptionset: "usb_enhancecoachingskillsoptionset",
    usb_entrepreneurialknowledgeoptionset: "usb_entrepreneurialknowledgeoptionset",
    usb_eqitwooptions: "usb_eqitwooptions",
    usb_excellentreputationtwooptions: "usb_excellentreputationtwooptions",
    usb_firstname: "usb_firstname",
    usb_gainknowledgeoptionset: "usb_gainknowledgeoptionset",
    usb_gmatresult: "usb_gmatresult",
    usb_highersalaryoptionset: "usb_highersalaryoptionset",
    usb_highestlevelmathematicscompetedoptionset: "usb_highestlevelmathematicscompetedoptionset",
    usb_highqualitystudentstwooptions: "usb_highqualitystudentstwooptions",
    usb_honoursyear: "usb_honoursyear",
    usb_howtobealeaderoptionset: "usb_howtobealeaderoptionset",
    usb_iamamatietwooptions: "usb_iamamatietwooptions",
    usb_ictsupportoptionset: "usb_ictsupportoptionset",
    usb_inductivereasoning: "usb_inductivereasoning",
    usb_infosessionoptionset: "usb_infosessionoptionset",
    usb_innovativeoptionset: "usb_innovativeoptionset",
    usb_insightpoliciesprogrammesoptionset: "usb_insightpoliciesprogrammesoptionset",
    usb_internationallecturersinclasstwooptions: "usb_internationallecturersinclasstwooptions",
    usb_internationalstudentstwooptions: "usb_internationalstudentstwooptions",
    usb_interviewedby: "usb_interviewedby",
    usb_lastname: "usb_lastname",
    usb_lecturerqaulityoptionset: "usb_lecturerqaulityoptionset",
    usb_libraryservicesoptionset: "usb_libraryservicesoptionset",
    usb_makeadifferenceoptionset: "usb_makeadifferenceoptionset",
    usb_manageprojectsoptionset: "usb_manageprojectsoptionset",
    usb_mastersyear: "usb_mastersyear",
    usb_mentorsoptionset: "usb_mentorsoptionset",
    usb_name: "usb_name",
    usb_newsarticletwooptions: "usb_newsarticletwooptions",
    usb_offcampussupportoptionset: "usb_offcampussupportoptionset",
    usb_oncampuseventtwooptions: "usb_oncampuseventtwooptions",
    usb_owncoachingpracticeoptionset: "usb_owncoachingpracticeoptionset",
    usb_ownframeworkmodeloptionset: "usb_ownframeworkmodeloptionset",
    usb_phdyear: "usb_phdyear",
    usb_pmskillsoptionset: "usb_pmskillsoptionset",
    usb_postgraduateyear: "usb_postgraduateyear",
    usb_professionalcoachescommunityoptionset: "usb_professionalcoachescommunityoptionset",
    usb_professionalfuturistoptionset: "usb_professionalfuturistoptionset",
    usb_programmeformattwooptions: "usb_programmeformattwooptions",
    usb_programmeofferinglookup: "usb_programmeofferinglookup",
    usb_progresscareeroptionset: "usb_progresscareeroptionset",
    usb_progresstombaoptionset: "usb_progresstombaoptionset",
    usb_promotionalemailstwooptions: "usb_promotionalemailstwooptions",
    usb_reasonforstoppingoptionset: "usb_reasonforstoppingoptionset",
    usb_reasonforstoppingother: "usb_reasonforstoppingother",
    usb_recommendedbyalumnitwooptions: "usb_recommendedbyalumnitwooptions",
    usb_recommendedbyemployertwooptions: "usb_recommendedbyemployertwooptions",
    usb_referee1_cellphone: "usb_referee1_cellphone",
    usb_referee1_email: "usb_referee1_email",
    usb_referee1_name: "usb_referee1_name",
    usb_referee1_position: "usb_referee1_position",
    usb_referee1_telephone: "usb_referee1_telephone",
    usb_referee2_cellphone: "usb_referee2_cellphone",
    usb_referee2_email: "usb_referee2_email",
    usb_referee2_name: "usb_referee2_name",
    usb_referee2_position: "usb_referee2_position",
    usb_referee2_telephone: "usb_referee2_telephone",
    usb_referralbyalumnustwooptions: "usb_referralbyalumnustwooptions",
    usb_referralbycurrentstudenttwooptions: "usb_referralbycurrentstudenttwooptions",
    usb_referralbyemployertwooptions: "usb_referralbyemployertwooptions",
    usb_referralbyfamilyfriendtwooptions: "usb_referralbyfamilyfriendtwooptions",
    usb_researchsupervisionoptionset: "usb_researchsupervisionoptionset",
    usb_responsibleleaderoptionset: "usb_responsibleleaderoptionset",
    usb_round1interviewcompletedtwooptions: "usb_round1interviewcompletedtwooptions",
    usb_round1interviewerlookup: "usb_round1interviewerlookup",
    usb_round1interviewreqtwooptions: "usb_round1interviewreqtwooptions",
    usb_round1interviewtypeoptionset: "usb_round1interviewtypeoptionset",
    usb_round2interviewcompletedtwooptions: "usb_round2interviewcompletedtwooptions",
    usb_round2interviewedby: "usb_round2interviewedby",
    usb_round2interviewerlookup: "usb_round2interviewerlookup",
    usb_round2interviewreqtwooptions: "usb_round2interviewreqtwooptions",
    usb_round2interviewtypeoptionset: "usb_round2interviewtypeoptionset",
    usb_rplcandidatetwooptions: "usb_rplcandidatetwooptions",
    usb_sagrowthcontributionoptionset: "usb_sagrowthcontributionoptionset",
    usb_salaryoptionset: "usb_salaryoptionset",
    usb_scenariotoolsoptionset: "usb_scenariotoolsoptionset",
    usb_searchenginetwooptions: "usb_searchenginetwooptions",
    usb_selfdevelopmentoptionset: "usb_selfdevelopmentoptionset",
    usb_shlfeerefno: "usb_shlfeerefno",
    usb_shlnmg35: "usb_shlnmg35",
    usb_shlnumeric: "usb_shlnumeric",
    usb_shloverall: "usb_shloverall",
    usb_shltestrequiredtwooptions: "usb_shltestrequiredtwooptions",
    usb_shlvmg48: "usb_shlvmg48",
    usb_socialmediatwooptions: "usb_socialmediatwooptions",
    usb_stellenboschuniversitytwooptions: "usb_stellenboschuniversitytwooptions",
    usb_stennmg: "usb_stennmg",
    usb_stenvng: "usb_stenvng",
    usb_studymaterialoptionset: "usb_studymaterialoptionset",
    usb_sustainablefutureoptionset: "usb_sustainablefutureoptionset",
    usb_sustainabletransoptionset: "usb_sustainabletransoptionset",
    usb_symbolachieved: "usb_symbolachieved",
    usb_theoreticalcoachingoptionset: "usb_theoreticalcoachingoptionset",
    usb_theoryoffututestudiesoptionset: "usb_theoryoffututestudiesoptionset",
    usb_totalyearsworkingexperience: "usb_totalyearsworkingexperience",
    usb_tripleaccreditationtwooptions: "usb_tripleaccreditationtwooptions",
    usb_usb_developsaeconomyoptionset: "usb_usb_developsaeconomyoptionset",
    usb_usb_usbedtwooptions: "usb_usb_usbedtwooptions",
    usb_usbafricafocustwooptions: "usb_usbafricafocustwooptions",
    usb_usbcollaborativelearningtwooptions: "usb_usbcollaborativelearningtwooptions",
    usb_usbgeneralleadershipfocustwooptions: "usb_usbgeneralleadershipfocustwooptions",
    usb_usbinternationaltwooptions: "usb_usbinternationaltwooptions",
    usb_usbwebsitetwooptions: "usb_usbwebsitetwooptions",
    usb_usnumber: "usb_usnumber",
    usb_valueformoneytwooptions: "usb_valueformoneytwooptions",
    usb_verbal: "usb_verbal",
    usb_whatdidyoudolastyearoptionset: "usb_whatdidyoudolastyearoptionset"
};

var Form_f06046d0_e5ff_47d3_acf3_611f276f3d35_Controls = {
    EmploymentHistory: "EmploymentHistory",
    header_ownerid: "header_ownerid",
    header_statuscode: "header_statuscode",
    header_usb_usnumber: "header_usb_usnumber",
    notescontrol: "notescontrol",
    Qualifications: "Qualifications",
    usb_academicsupportoptionset: "usb_academicsupportoptionset",
    usb_acceptanceformtwooptions: "usb_acceptanceformtwooptions",
    usb_acquirebusinessknowledgeoptionset: "usb_acquirebusinessknowledgeoptionset",
    usb_acquiredevfinskillsoptionset: "usb_acquiredevfinskillsoptionset",
    usb_acquiremanagementskillsoptionset: "usb_acquiremanagementskillsoptionset",
    usb_adminsupportoptionset: "usb_adminsupportoptionset",
    usb_advertisementtwooptions: "usb_advertisementtwooptions",
    usb_africaemergingmarketoptionset: "usb_africaemergingmarketoptionset",
    usb_africamultinationalenvironmentoptionset: "usb_africamultinationalenvironmentoptionset",
    usb_africastrategicdecisionoptionset: "usb_africastrategicdecisionoptionset",
    usb_africasustainabledevelopmentoptionset: "usb_africasustainabledevelopmentoptionset",
    usb_afrikaansleesoptionset: "usb_afrikaansleesoptionset",
    usb_afrikaanspraatoptionset: "usb_afrikaanspraatoptionset",
    usb_afrikaansskryfoptionset: "usb_afrikaansskryfoptionset",
    usb_afrikaansverstaanoptionset: "usb_afrikaansverstaanoptionset",
    usb_alignforfuturistoptionset: "usb_alignforfuturistoptionset",
    usb_analyticalwriting: "usb_analyticalwriting",
    usb_applicationfeerefno: "usb_applicationfeerefno",
    usb_applicationfeetwooptions: "usb_applicationfeetwooptions",
    usb_applicationformurl: "usb_applicationformurl",
    usb_aquirescareskillsoptionset: "usb_aquirescareskillsoptionset",
    usb_aquirethinkingskillsoptionset: "usb_aquirethinkingskillsoptionset",
    usb_attendedinfosessiontwooptions: "usb_attendedinfosessiontwooptions",
    usb_authenticleadershipoptioset: "usb_authenticleadershipoptioset",
    usb_availablecareeroptionset: "usb_availablecareeroptionset",
    usb_benefitfromuniquetypeoptionset: "usb_benefitfromuniquetypeoptionset",
    usb_blendedlearningoptionset: "usb_blendedlearningoptionset",
    usb_brochuretwooptions: "usb_brochuretwooptions",
    usb_bursaryapplicanttwooptions: "usb_bursaryapplicanttwooptions",
    usb_bursaryawardeetwootions: "usb_bursaryawardeetwootions",
    usb_businessleaderoptionset: "usb_businessleaderoptionset",
    usb_campussecurityoptionset: "usb_campussecurityoptionset",
    usb_careerchangeoptionset: "usb_careerchangeoptionset",
    usb_cateringservicesoptionset: "usb_cateringservicesoptionset",
    usb_classroombasedoptionset: "usb_classroombasedoptionset",
    usb_closetohometwooptions: "usb_closetohometwooptions",
    usb_complexorgenvironmentoptionset: "usb_complexorgenvironmentoptionset",
    usb_conferenceexpotwooptions: "usb_conferenceexpotwooptions",
    usb_contactlookup: "usb_contactlookup",
    usb_deliverymodetwooptions: "usb_deliverymodetwooptions",
    usb_depositrefno: "usb_depositrefno",
    usb_developasaleaderoptionset: "usb_developasaleaderoptionset",
    usb_developefuturestudiesoptionset: "usb_developefuturestudiesoptionset",
    usb_developmentofothersoptionset: "usb_developmentofothersoptionset",
    usb_digitalskillsoptionset: "usb_digitalskillsoptionset",
    usb_diverseclassgroupoptionset: "usb_diverseclassgroupoptionset",
    usb_diversefaultycompositeoptionset: "usb_diversefaultycompositeoptionset",
    usb_electronicstudymaterialoptionset: "usb_electronicstudymaterialoptionset",
    usb_employerassistfinanciallyoptionset: "usb_employerassistfinanciallyoptionset",
    usb_employeremail: "usb_employeremail",
    usb_employername: "usb_employername",
    usb_employertelephone: "usb_employertelephone",
    usb_englishreadoptionset: "usb_englishreadoptionset",
    usb_englishspeakoptionset: "usb_englishspeakoptionset",
    usb_englishunderstandoptionset: "usb_englishunderstandoptionset",
    usb_englishwriteoptionset: "usb_englishwriteoptionset",
    usb_enhancecoachingskillsoptionset: "usb_enhancecoachingskillsoptionset",
    usb_entrepreneurialknowledgeoptionset: "usb_entrepreneurialknowledgeoptionset",
    usb_eqitwooptions: "usb_eqitwooptions",
    usb_excellentreputationtwooptions: "usb_excellentreputationtwooptions",
    usb_firstname: "usb_firstname",
    usb_gainknowledgeoptionset: "usb_gainknowledgeoptionset",
    usb_gmatresult: "usb_gmatresult",
    usb_highersalaryoptionset: "usb_highersalaryoptionset",
    usb_highestlevelmathematicscompetedoptionset: "usb_highestlevelmathematicscompetedoptionset",
    usb_highqualitystudentstwooptions: "usb_highqualitystudentstwooptions",
    usb_honoursyear: "usb_honoursyear",
    usb_howtobealeaderoptionset: "usb_howtobealeaderoptionset",
    usb_iamamatietwooptions: "usb_iamamatietwooptions",
    usb_ictsupportoptionset: "usb_ictsupportoptionset",
    usb_inductivereasoning: "usb_inductivereasoning",
    usb_infosessionoptionset: "usb_infosessionoptionset",
    usb_innovativeoptionset: "usb_innovativeoptionset",
    usb_insightpoliciesprogrammesoptionset: "usb_insightpoliciesprogrammesoptionset",
    usb_internationallecturersinclasstwooptions: "usb_internationallecturersinclasstwooptions",
    usb_internationalstudentstwooptions: "usb_internationalstudentstwooptions",
    usb_interviewedby: "usb_interviewedby",
    usb_lastname: "usb_lastname",
    usb_lecturerqaulityoptionset: "usb_lecturerqaulityoptionset",
    usb_libraryservicesoptionset: "usb_libraryservicesoptionset",
    usb_makeadifferenceoptionset: "usb_makeadifferenceoptionset",
    usb_manageprojectsoptionset: "usb_manageprojectsoptionset",
    usb_mastersyear: "usb_mastersyear",
    usb_mentorsoptionset: "usb_mentorsoptionset",
    usb_name: "usb_name",
    usb_newsarticletwooptions: "usb_newsarticletwooptions",
    usb_offcampussupportoptionset: "usb_offcampussupportoptionset",
    usb_oncampuseventtwooptions: "usb_oncampuseventtwooptions",
    usb_owncoachingpracticeoptionset: "usb_owncoachingpracticeoptionset",
    usb_ownframeworkmodeloptionset: "usb_ownframeworkmodeloptionset",
    usb_phdyear: "usb_phdyear",
    usb_pmskillsoptionset: "usb_pmskillsoptionset",
    usb_postgraduateyear: "usb_postgraduateyear",
    usb_professionalcoachescommunityoptionset: "usb_professionalcoachescommunityoptionset",
    usb_professionalfuturistoptionset: "usb_professionalfuturistoptionset",
    usb_programmeformattwooptions: "usb_programmeformattwooptions",
    usb_programmeofferinglookup: "usb_programmeofferinglookup",
    usb_progresscareeroptionset: "usb_progresscareeroptionset",
    usb_progresstombaoptionset: "usb_progresstombaoptionset",
    usb_promotionalemailstwooptions: "usb_promotionalemailstwooptions",
    usb_reasonforstoppingoptionset: "usb_reasonforstoppingoptionset",
    usb_reasonforstoppingother: "usb_reasonforstoppingother",
    usb_recommendedbyalumnitwooptions: "usb_recommendedbyalumnitwooptions",
    usb_recommendedbyemployertwooptions: "usb_recommendedbyemployertwooptions",
    usb_referee1_cellphone: "usb_referee1_cellphone",
    usb_referee1_email: "usb_referee1_email",
    usb_referee1_name: "usb_referee1_name",
    usb_referee1_position: "usb_referee1_position",
    usb_referee1_telephone: "usb_referee1_telephone",
    usb_referee2_cellphone: "usb_referee2_cellphone",
    usb_referee2_email: "usb_referee2_email",
    usb_referee2_name: "usb_referee2_name",
    usb_referee2_position: "usb_referee2_position",
    usb_referee2_telephone: "usb_referee2_telephone",
    usb_referralbyalumnustwooptions: "usb_referralbyalumnustwooptions",
    usb_referralbycurrentstudenttwooptions: "usb_referralbycurrentstudenttwooptions",
    usb_referralbyemployertwooptions: "usb_referralbyemployertwooptions",
    usb_referralbyfamilyfriendtwooptions: "usb_referralbyfamilyfriendtwooptions",
    usb_researchsupervisionoptionset: "usb_researchsupervisionoptionset",
    usb_responsibleleaderoptionset: "usb_responsibleleaderoptionset",
    usb_round1interviewcompletedtwooptions: "usb_round1interviewcompletedtwooptions",
    usb_round1interviewerlookup: "usb_round1interviewerlookup",
    usb_round1interviewreqtwooptions: "usb_round1interviewreqtwooptions",
    usb_round1interviewtypeoptionset: "usb_round1interviewtypeoptionset",
    usb_round2interviewcompletedtwooptions: "usb_round2interviewcompletedtwooptions",
    usb_round2interviewedby: "usb_round2interviewedby",
    usb_round2interviewerlookup: "usb_round2interviewerlookup",
    usb_round2interviewreqtwooptions: "usb_round2interviewreqtwooptions",
    usb_round2interviewtypeoptionset: "usb_round2interviewtypeoptionset",
    usb_rplcandidatetwooptions: "usb_rplcandidatetwooptions",
    usb_sagrowthcontributionoptionset: "usb_sagrowthcontributionoptionset",
    usb_salaryoptionset: "usb_salaryoptionset",
    usb_scenariotoolsoptionset: "usb_scenariotoolsoptionset",
    usb_searchenginetwooptions: "usb_searchenginetwooptions",
    usb_selfdevelopmentoptionset: "usb_selfdevelopmentoptionset",
    usb_shlfeerefno: "usb_shlfeerefno",
    usb_shlnmg35: "usb_shlnmg35",
    usb_shlnumeric: "usb_shlnumeric",
    usb_shloverall: "usb_shloverall",
    usb_shltestrequiredtwooptions: "usb_shltestrequiredtwooptions",
    usb_shlvmg48: "usb_shlvmg48",
    usb_socialmediatwooptions: "usb_socialmediatwooptions",
    usb_stellenboschuniversitytwooptions: "usb_stellenboschuniversitytwooptions",
    usb_stennmg: "usb_stennmg",
    usb_stenvng: "usb_stenvng",
    usb_studymaterialoptionset: "usb_studymaterialoptionset",
    usb_sustainablefutureoptionset: "usb_sustainablefutureoptionset",
    usb_sustainabletransoptionset: "usb_sustainabletransoptionset",
    usb_symbolachieved: "usb_symbolachieved",
    usb_theoreticalcoachingoptionset: "usb_theoreticalcoachingoptionset",
    usb_theoryoffututestudiesoptionset: "usb_theoryoffututestudiesoptionset",
    usb_totalyearsworkingexperience: "usb_totalyearsworkingexperience",
    usb_tripleaccreditationtwooptions: "usb_tripleaccreditationtwooptions",
    usb_usb_developsaeconomyoptionset: "usb_usb_developsaeconomyoptionset",
    usb_usb_usbedtwooptions: "usb_usb_usbedtwooptions",
    usb_usbafricafocustwooptions: "usb_usbafricafocustwooptions",
    usb_usbcollaborativelearningtwooptions: "usb_usbcollaborativelearningtwooptions",
    usb_usbgeneralleadershipfocustwooptions: "usb_usbgeneralleadershipfocustwooptions",
    usb_usbinternationaltwooptions: "usb_usbinternationaltwooptions",
    usb_usbwebsitetwooptions: "usb_usbwebsitetwooptions",
    usb_usnumber: "usb_usnumber",
    usb_valueformoneytwooptions: "usb_valueformoneytwooptions",
    usb_verbal: "usb_verbal",
    usb_whatdidyoudolastyearoptionset: "usb_whatdidyoudolastyearoptionset"
};

var pageData = {
    "Event": "none",
    "SaveMode": 1,
    "EventSource": null,
    "AuthenticationHeader": "",
    "CurrentTheme": "Default",
    "OrgLcid": 1033,
    "OrgUniqueName": "",
    "QueryStringParameters": {
        "_gridType": "10026",
        "etc": "10026",
        "id": "",
        "pagemode": "iframe",
        "preloadcache": "1344548892170",
        "rskey": "141637534"
    },
    "ServerUrl": "",
    "UserId": "",
    "UserLcid": 1033,
    "UserRoles": [""],
    "isOutlookClient": false,
    "isOutlookOnline": true,
    "DataXml": "",
    "EntityName": "usb_studentacademicrecord",
    "Id": "",
    "IsDirty": false,
    "CurrentControl": "",
    "CurrentForm": null,
    "Forms": [],
    "FormType": 2,
    "ViewPortHeight": 558,
    "ViewPortWidth": 1231,
    "Attributes": [{
        "Name": "ownerid",
        "Value": [{
            "entityType": "Unknown",
            "id": "{27ACAE0A-5D79-E111-BBD3-2657AEB3167B}",
            "name": "Temp"
        }],
        "Type": "lookup",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": false,
            "canCreate": true
        },
        "Controls": [{
            "Name": "header_ownerid"
        }]
    },
    {
        "Name": "statuscode",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Applicant",
            "value": 1
        },
        {
            "text": "Application Completed",
            "value": 864480000
        },
        {
            "text": "Interview Round 1",
            "value": 864480001
        },
        {
            "text": "Selection Committee",
            "value": 864480002
        },
        {
            "text": "Interview Round 2",
            "value": 864480003
        },
        {
            "text": "Cancelled",
            "value": 864480004
        },
        {
            "text": "Not Admitted",
            "value": 864480005
        },
        {
            "text": "Waiting List",
            "value": 864480006
        },
        {
            "text": "Request: Admitted",
            "value": 864480015
        },
        {
            "text": "Admitted",
            "value": 864480007
        },
        {
            "text": "Failed: Admitted",
            "value": 864480016
        },
        {
            "text": "Postponed",
            "value": 864480008
        },
        {
            "text": "Request: Not Accepted",
            "value": 864480017
        },
        {
            "text": "Not Accepted",
            "value": 864480009
        },
        {
            "text": "Failed: Not Accepted",
            "value": 864480018
        },
        {
            "text": "Accepted",
            "value": 864480010
        },
        {
            "text": "Registered",
            "value": 864480011
        },
        {
            "text": "Unsuccessful",
            "value": 864480012
        },
        {
            "text": "Discontinued",
            "value": 864480013
        },
        {
            "text": "Graduated",
            "value": 864480014
        },
        {
            "text": "Deactivated",
            "value": 2
        },
        {
            "text": "Request: Accepted",
            "value": 864480019
        },
        {
            "text": "Failed: Accepted",
            "value": 864480020
        }],
        "SelectedOption": {
            "option": "Applicant",
            "value": 1
        },
        "Text": "Applicant",
        "Controls": [{
            "Name": "header_statuscode"
        }]
    },
    {
        "Name": "usb_academicsupportoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_academicsupportoptionset"
        }]
    },
    {
        "Name": "usb_acceptanceformtwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "usb_acceptanceformtwooptions"
        }]
    },
    {
        "Name": "usb_acquirebusinessknowledgeoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_acquirebusinessknowledgeoptionset"
        }]
    },
    {
        "Name": "usb_acquiredevfinskillsoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_acquiredevfinskillsoptionset"
        }]
    },
    {
        "Name": "usb_acquiremanagementskillsoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_acquiremanagementskillsoptionset"
        }]
    },
    {
        "Name": "usb_adminsupportoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_adminsupportoptionset"
        }]
    },
    {
        "Name": "usb_advertisementtwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "usb_advertisementtwooptions"
        }]
    },
    {
        "Name": "usb_africaemergingmarketoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_africaemergingmarketoptionset"
        }]
    },
    {
        "Name": "usb_africamultinationalenvironmentoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_africamultinationalenvironmentoptionset"
        }]
    },
    {
        "Name": "usb_africastrategicdecisionoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_africastrategicdecisionoptionset"
        }]
    },
    {
        "Name": "usb_africasustainabledevelopmentoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_africasustainabledevelopmentoptionset"
        }]
    },
    {
        "Name": "usb_afrikaansleesoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Goed",
            "value": 1
        },
        {
            "text": "Gemiddeld",
            "value": 2
        },
        {
            "text": "Swak",
            "value": 3
        }],
        "SelectedOption": {
            "option": "Goed",
            "value": 1
        },
        "Text": "Goed",
        "Controls": [{
            "Name": "usb_afrikaansleesoptionset"
        }]
    },
    {
        "Name": "usb_afrikaanspraatoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Goed",
            "value": 1
        },
        {
            "text": "Gemiddeld",
            "value": 2
        },
        {
            "text": "Swak",
            "value": 3
        }],
        "SelectedOption": {
            "option": "Goed",
            "value": 1
        },
        "Text": "Goed",
        "Controls": [{
            "Name": "usb_afrikaanspraatoptionset"
        }]
    },
    {
        "Name": "usb_afrikaansskryfoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Goed",
            "value": 1
        },
        {
            "text": "Gemiddeld",
            "value": 2
        },
        {
            "text": "Swak",
            "value": 3
        }],
        "SelectedOption": {
            "option": "Goed",
            "value": 1
        },
        "Text": "Goed",
        "Controls": [{
            "Name": "usb_afrikaansskryfoptionset"
        }]
    },
    {
        "Name": "usb_afrikaansverstaanoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Goed",
            "value": 1
        },
        {
            "text": "Gemiddeld",
            "value": 2
        },
        {
            "text": "Swak",
            "value": 3
        }],
        "SelectedOption": {
            "option": "Goed",
            "value": 1
        },
        "Text": "Goed",
        "Controls": [{
            "Name": "usb_afrikaansverstaanoptionset"
        }]
    },
    {
        "Name": "usb_alignforfuturistoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_alignforfuturistoptionset"
        }]
    },
    {
        "Name": "usb_analyticalwriting",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "usb_analyticalwriting"
        }]
    },
    {
        "Name": "usb_applicationfeerefno",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "usb_applicationfeerefno"
        }]
    },
    {
        "Name": "usb_applicationfeetwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "usb_applicationfeetwooptions"
        }]
    },
    {
        "Name": "usb_applicationformurl",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 500,
        "Controls": [{
            "Name": "usb_applicationformurl"
        }]
    },
    {
        "Name": "usb_aquirescareskillsoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_aquirescareskillsoptionset"
        }]
    },
    {
        "Name": "usb_aquirethinkingskillsoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_aquirethinkingskillsoptionset"
        }]
    },
    {
        "Name": "usb_attendedinfosessiontwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "usb_attendedinfosessiontwooptions"
        }]
    },
    {
        "Name": "usb_authenticleadershipoptioset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_authenticleadershipoptioset"
        }]
    },
    {
        "Name": "usb_availablecareeroptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_availablecareeroptionset"
        }]
    },
    {
        "Name": "usb_benefitfromuniquetypeoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_benefitfromuniquetypeoptionset"
        }]
    },
    {
        "Name": "usb_blendedlearningoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_blendedlearningoptionset"
        }]
    },
    {
        "Name": "usb_brochuretwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "usb_brochuretwooptions"
        }]
    },
    {
        "Name": "usb_bursaryapplicanttwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "usb_bursaryapplicanttwooptions"
        }]
    },
    {
        "Name": "usb_bursaryawardeetwootions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "usb_bursaryawardeetwootions"
        }]
    },
    {
        "Name": "usb_businessleaderoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_businessleaderoptionset"
        }]
    },
    {
        "Name": "usb_campussecurityoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_campussecurityoptionset"
        }]
    },
    {
        "Name": "usb_careerchangeoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very Important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very Important",
            "value": 100000
        },
        "Text": "Very Important",
        "Controls": [{
            "Name": "usb_careerchangeoptionset"
        }]
    },
    {
        "Name": "usb_cateringservicesoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_cateringservicesoptionset"
        }]
    },
    {
        "Name": "usb_classroombasedoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_classroombasedoptionset"
        }]
    },
    {
        "Name": "usb_closetohometwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "usb_closetohometwooptions"
        }]
    },
    {
        "Name": "usb_complexorgenvironmentoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_complexorgenvironmentoptionset"
        }]
    },
    {
        "Name": "usb_conferenceexpotwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "usb_conferenceexpotwooptions"
        }]
    },
    {
        "Name": "usb_contactlookup",
        "Value": [{
            "entityType": "Unknown",
            "id": "{27ACAE0A-5D79-E111-BBD3-2657AEB3167B}",
            "name": "Temp"
        }],
        "Type": "lookup",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Controls": [{
            "Name": "usb_contactlookup"
        }]
    },
    {
        "Name": "usb_deliverymodetwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "usb_deliverymodetwooptions"
        }]
    },
    {
        "Name": "usb_depositrefno",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "usb_depositrefno"
        }]
    },
    {
        "Name": "usb_developasaleaderoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_developasaleaderoptionset"
        }]
    },
    {
        "Name": "usb_developefuturestudiesoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_developefuturestudiesoptionset"
        }]
    },
    {
        "Name": "usb_developmentofothersoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_developmentofothersoptionset"
        }]
    },
    {
        "Name": "usb_digitalskillsoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_digitalskillsoptionset"
        }]
    },
    {
        "Name": "usb_diverseclassgroupoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_diverseclassgroupoptionset"
        }]
    },
    {
        "Name": "usb_diversefaultycompositeoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_diversefaultycompositeoptionset"
        }]
    },
    {
        "Name": "usb_electronicstudymaterialoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_electronicstudymaterialoptionset"
        }]
    },
    {
        "Name": "usb_employerassistfinanciallyoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "No",
            "value": 1
        },
        {
            "text": "Partly",
            "value": 2
        },
        {
            "text": "Fully",
            "value": 3
        },
        {
            "text": "Not Sure",
            "value": 4
        }],
        "SelectedOption": {
            "option": "No",
            "value": 1
        },
        "Text": "No",
        "Controls": [{
            "Name": "usb_employerassistfinanciallyoptionset"
        }]
    },
    {
        "Name": "usb_employeremail",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "usb_employeremail"
        }]
    },
    {
        "Name": "usb_employername",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "usb_employername"
        }]
    },
    {
        "Name": "usb_employertelephone",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "usb_employertelephone"
        }]
    },
    {
        "Name": "usb_englishreadoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Good",
            "value": 1
        },
        {
            "text": "Average",
            "value": 2
        },
        {
            "text": "Poor",
            "value": 3
        }],
        "SelectedOption": {
            "option": "Good",
            "value": 1
        },
        "Text": "Good",
        "Controls": [{
            "Name": "usb_englishreadoptionset"
        }]
    },
    {
        "Name": "usb_englishspeakoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Good",
            "value": 1
        },
        {
            "text": "Average",
            "value": 2
        },
        {
            "text": "Poor",
            "value": 3
        }],
        "SelectedOption": {
            "option": "Good",
            "value": 1
        },
        "Text": "Good",
        "Controls": [{
            "Name": "usb_englishspeakoptionset"
        }]
    },
    {
        "Name": "usb_englishunderstandoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Good",
            "value": 1
        },
        {
            "text": "Average",
            "value": 2
        },
        {
            "text": "Poor",
            "value": 3
        }],
        "SelectedOption": {
            "option": "Good",
            "value": 1
        },
        "Text": "Good",
        "Controls": [{
            "Name": "usb_englishunderstandoptionset"
        }]
    },
    {
        "Name": "usb_englishwriteoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Good",
            "value": 1
        },
        {
            "text": "Average",
            "value": 2
        },
        {
            "text": "Poor",
            "value": 3
        }],
        "SelectedOption": {
            "option": "Good",
            "value": 1
        },
        "Text": "Good",
        "Controls": [{
            "Name": "usb_englishwriteoptionset"
        }]
    },
    {
        "Name": "usb_enhancecoachingskillsoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_enhancecoachingskillsoptionset"
        }]
    },
    {
        "Name": "usb_entrepreneurialknowledgeoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_entrepreneurialknowledgeoptionset"
        }]
    },
    {
        "Name": "usb_eqitwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "usb_eqitwooptions"
        }]
    },
    {
        "Name": "usb_excellentreputationtwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "usb_excellentreputationtwooptions"
        }]
    },
    {
        "Name": "usb_firstname",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "usb_firstname"
        }]
    },
    {
        "Name": "usb_gainknowledgeoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_gainknowledgeoptionset"
        }]
    },
    {
        "Name": "usb_gmatresult",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "usb_gmatresult"
        }]
    },
    {
        "Name": "usb_highersalaryoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_highersalaryoptionset"
        }]
    },
    {
        "Name": "usb_highestlevelmathematicscompetedoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "No Secondary Schooling Mathematics",
            "value": 1
        },
        {
            "text": "South African Grade 9",
            "value": 2
        },
        {
            "text": "South African Grade 12 SG",
            "value": 3
        },
        {
            "text": "South African Grade 12 HG",
            "value": 4
        },
        {
            "text": "GCSE",
            "value": 5
        },
        {
            "text": "A Levels",
            "value": 6
        },
        {
            "text": "Tertiary Education",
            "value": 7
        },
        {
            "text": "Other",
            "value": 8
        },
        {
            "text": "Chartered Accountant",
            "value": 9
        },
        {
            "text": "IEB Grade 12 HG",
            "value": 10
        },
        {
            "text": "Engineering Mathematics, Applied Mathematics",
            "value": 11
        },
        {
            "text": "German Abitur (Higher Secondary Education)",
            "value": 12
        },
        {
            "text": "Technical University Diploma in Germany",
            "value": 13
        },
        {
            "text": "First Semester Calcs and Stats, National Diploma",
            "value": 14
        },
        {
            "text": "Statistics Honours",
            "value": 15
        },
        {
            "text": "German A-Level, Witten Examination; Several Math courses at University level",
            "value": 16
        },
        {
            "text": "German Abitur and all Math courses, Chemistry master with all advanced math courses",
            "value": 17
        }],
        "SelectedOption": {
            "option": "No Secondary Schooling Mathematics",
            "value": 1
        },
        "Text": "No Secondary Schooling Mathematics",
        "Controls": [{
            "Name": "usb_highestlevelmathematicscompetedoptionset"
        }]
    },
    {
        "Name": "usb_highqualitystudentstwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "usb_highqualitystudentstwooptions"
        }]
    },
    {
        "Name": "usb_honoursyear",
        "Value": null,
        "Type": "datetime",
        "Format": "date",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Controls": [{
            "Name": "usb_honoursyear"
        }]
    },
    {
        "Name": "usb_howtobealeaderoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_howtobealeaderoptionset"
        }]
    },
    {
        "Name": "usb_iamamatietwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "usb_iamamatietwooptions"
        }]
    },
    {
        "Name": "usb_ictsupportoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_ictsupportoptionset"
        }]
    },
    {
        "Name": "usb_inductivereasoning",
        "Value": null,
        "Type": "integer",
        "Format": "none",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Max": 100,
        "Min": 0,
        "Precision": 0,
        "Controls": [{
            "Name": "usb_inductivereasoning"
        }]
    },
    {
        "Name": "usb_infosessionoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "USB campus",
            "value": 1
        },
        {
            "text": "Abuja",
            "value": 2
        },
        {
            "text": "Accra",
            "value": 3
        },
        {
            "text": "Addis Ababa",
            "value": 4
        },
        {
            "text": "Bloemfontein",
            "value": 5
        },
        {
            "text": "Dar-es-Salaam",
            "value": 6
        },
        {
            "text": "Douala",
            "value": 7
        },
        {
            "text": "Durban",
            "value": 8
        },
        {
            "text": "Gaborone",
            "value": 9
        },
        {
            "text": "Harare",
            "value": 10
        },
        {
            "text": "Johannesburg",
            "value": 11
        },
        {
            "text": "Kampala",
            "value": 12
        },
        {
            "text": "Kimberley",
            "value": 13
        },
        {
            "text": "Kumasi",
            "value": 14
        },
        {
            "text": "Lagos Island",
            "value": 15
        },
        {
            "text": "Lagos Main Land",
            "value": 16
        },
        {
            "text": "Lusaka",
            "value": 17
        },
        {
            "text": "Manzini",
            "value": 18
        },
        {
            "text": "Maputo",
            "value": 19
        },
        {
            "text": "Maseru",
            "value": 20
        },
        {
            "text": "Mbabane",
            "value": 21
        },
        {
            "text": "Mombasa",
            "value": 22
        },
        {
            "text": "Nairobi",
            "value": 23
        },
        {
            "text": "Nelspruit",
            "value": 24
        },
        {
            "text": "Port Elizabeth",
            "value": 25
        },
        {
            "text": "Port Harcourt",
            "value": 26
        },
        {
            "text": "Pretoria",
            "value": 27
        },
        {
            "text": "Richard’s Bay",
            "value": 28
        },
        {
            "text": "Windhoek",
            "value": 29
        },
        {
            "text": "USB webinar",
            "value": 30
        }],
        "SelectedOption": {
            "option": "USB campus",
            "value": 1
        },
        "Text": "USB campus",
        "Controls": [{
            "Name": "usb_infosessionoptionset"
        }]
    },
    {
        "Name": "usb_innovativeoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_innovativeoptionset"
        }]
    },
    {
        "Name": "usb_insightpoliciesprogrammesoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_insightpoliciesprogrammesoptionset"
        }]
    },
    {
        "Name": "usb_internationallecturersinclasstwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "usb_internationallecturersinclasstwooptions"
        }]
    },
    {
        "Name": "usb_internationalstudentstwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "usb_internationalstudentstwooptions"
        }]
    },
    {
        "Name": "usb_interviewedby",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 250,
        "Controls": [{
            "Name": "usb_interviewedby"
        }]
    },
    {
        "Name": "usb_lastname",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "usb_lastname"
        }]
    },
    {
        "Name": "usb_lecturerqaulityoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_lecturerqaulityoptionset"
        }]
    },
    {
        "Name": "usb_libraryservicesoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_libraryservicesoptionset"
        }]
    },
    {
        "Name": "usb_makeadifferenceoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_makeadifferenceoptionset"
        }]
    },
    {
        "Name": "usb_manageprojectsoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_manageprojectsoptionset"
        }]
    },
    {
        "Name": "usb_mastersyear",
        "Value": null,
        "Type": "datetime",
        "Format": "date",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Controls": [{
            "Name": "usb_mastersyear"
        }]
    },
    {
        "Name": "usb_mentorsoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_mentorsoptionset"
        }]
    },
    {
        "Name": "usb_name",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "usb_name"
        }]
    },
    {
        "Name": "usb_newsarticletwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "usb_newsarticletwooptions"
        }]
    },
    {
        "Name": "usb_offcampussupportoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_offcampussupportoptionset"
        }]
    },
    {
        "Name": "usb_oncampuseventtwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "usb_oncampuseventtwooptions"
        }]
    },
    {
        "Name": "usb_owncoachingpracticeoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_owncoachingpracticeoptionset"
        }]
    },
    {
        "Name": "usb_ownframeworkmodeloptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_ownframeworkmodeloptionset"
        }]
    },
    {
        "Name": "usb_phdyear",
        "Value": null,
        "Type": "datetime",
        "Format": "date",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Controls": [{
            "Name": "usb_phdyear"
        }]
    },
    {
        "Name": "usb_pmskillsoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_pmskillsoptionset"
        }]
    },
    {
        "Name": "usb_postgraduateyear",
        "Value": null,
        "Type": "datetime",
        "Format": "date",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Controls": [{
            "Name": "usb_postgraduateyear"
        }]
    },
    {
        "Name": "usb_professionalcoachescommunityoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_professionalcoachescommunityoptionset"
        }]
    },
    {
        "Name": "usb_professionalfuturistoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_professionalfuturistoptionset"
        }]
    },
    {
        "Name": "usb_programmeformattwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "usb_programmeformattwooptions"
        }]
    },
    {
        "Name": "usb_programmeofferinglookup",
        "Value": [{
            "entityType": "Unknown",
            "id": "{27ACAE0A-5D79-E111-BBD3-2657AEB3167B}",
            "name": "Temp"
        }],
        "Type": "lookup",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Controls": [{
            "Name": "usb_programmeofferinglookup"
        }]
    },
    {
        "Name": "usb_progresscareeroptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_progresscareeroptionset"
        }]
    },
    {
        "Name": "usb_progresstombaoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_progresstombaoptionset"
        }]
    },
    {
        "Name": "usb_promotionalemailstwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "usb_promotionalemailstwooptions"
        }]
    },
    {
        "Name": "usb_reasonforstoppingoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Financial",
            "value": 1
        },
        {
            "text": "Medical",
            "value": 2
        },
        {
            "text": "Moved to another Programme",
            "value": 3
        },
        {
            "text": "Other",
            "value": 4
        }],
        "SelectedOption": {
            "option": "Financial",
            "value": 1
        },
        "Text": "Financial",
        "Controls": [{
            "Name": "usb_reasonforstoppingoptionset"
        }]
    },
    {
        "Name": "usb_reasonforstoppingother",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 250,
        "Controls": [{
            "Name": "usb_reasonforstoppingother"
        }]
    },
    {
        "Name": "usb_recommendedbyalumnitwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "usb_recommendedbyalumnitwooptions"
        }]
    },
    {
        "Name": "usb_recommendedbyemployertwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "usb_recommendedbyemployertwooptions"
        }]
    },
    {
        "Name": "usb_referee1_cellphone",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "usb_referee1_cellphone"
        }]
    },
    {
        "Name": "usb_referee1_email",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "usb_referee1_email"
        }]
    },
    {
        "Name": "usb_referee1_name",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "usb_referee1_name"
        }]
    },
    {
        "Name": "usb_referee1_position",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "usb_referee1_position"
        }]
    },
    {
        "Name": "usb_referee1_telephone",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "usb_referee1_telephone"
        }]
    },
    {
        "Name": "usb_referee2_cellphone",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "usb_referee2_cellphone"
        }]
    },
    {
        "Name": "usb_referee2_email",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "usb_referee2_email"
        }]
    },
    {
        "Name": "usb_referee2_name",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "usb_referee2_name"
        }]
    },
    {
        "Name": "usb_referee2_position",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "usb_referee2_position"
        }]
    },
    {
        "Name": "usb_referee2_telephone",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "usb_referee2_telephone"
        }]
    },
    {
        "Name": "usb_referralbyalumnustwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "usb_referralbyalumnustwooptions"
        }]
    },
    {
        "Name": "usb_referralbycurrentstudenttwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "usb_referralbycurrentstudenttwooptions"
        }]
    },
    {
        "Name": "usb_referralbyemployertwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "usb_referralbyemployertwooptions"
        }]
    },
    {
        "Name": "usb_referralbyfamilyfriendtwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "usb_referralbyfamilyfriendtwooptions"
        }]
    },
    {
        "Name": "usb_researchsupervisionoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_researchsupervisionoptionset"
        }]
    },
    {
        "Name": "usb_responsibleleaderoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_responsibleleaderoptionset"
        }]
    },
    {
        "Name": "usb_round1interviewcompletedtwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "usb_round1interviewcompletedtwooptions"
        }]
    },
    {
        "Name": "usb_round1interviewerlookup",
        "Value": [{
            "entityType": "Unknown",
            "id": "{27ACAE0A-5D79-E111-BBD3-2657AEB3167B}",
            "name": "Temp"
        }],
        "Type": "lookup",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Controls": [{
            "Name": "usb_round1interviewerlookup"
        }]
    },
    {
        "Name": "usb_round1interviewreqtwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "usb_round1interviewreqtwooptions"
        }]
    },
    {
        "Name": "usb_round1interviewtypeoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "RPL",
            "value": 1
        },
        {
            "text": "Alumnus",
            "value": 2
        },
        {
            "text": "Academic Discussion",
            "value": 3
        },
        {
            "text": "Academic Interview",
            "value": 4
        }],
        "SelectedOption": {
            "option": "RPL",
            "value": 1
        },
        "Text": "RPL",
        "Controls": [{
            "Name": "usb_round1interviewtypeoptionset"
        }]
    },
    {
        "Name": "usb_round2interviewcompletedtwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "usb_round2interviewcompletedtwooptions"
        }]
    },
    {
        "Name": "usb_round2interviewedby",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 250,
        "Controls": [{
            "Name": "usb_round2interviewedby"
        }]
    },
    {
        "Name": "usb_round2interviewerlookup",
        "Value": [{
            "entityType": "Unknown",
            "id": "{27ACAE0A-5D79-E111-BBD3-2657AEB3167B}",
            "name": "Temp"
        }],
        "Type": "lookup",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Controls": [{
            "Name": "usb_round2interviewerlookup"
        }]
    },
    {
        "Name": "usb_round2interviewreqtwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "usb_round2interviewreqtwooptions"
        }]
    },
    {
        "Name": "usb_round2interviewtypeoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Academic",
            "value": 1
        },
        {
            "text": "Faculty Board",
            "value": 2
        },
        {
            "text": "Senate",
            "value": 3
        },
        {
            "text": "Alumnus",
            "value": 4
        }],
        "SelectedOption": {
            "option": "Academic",
            "value": 1
        },
        "Text": "Academic",
        "Controls": [{
            "Name": "usb_round2interviewtypeoptionset"
        }]
    },
    {
        "Name": "usb_rplcandidatetwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "usb_rplcandidatetwooptions"
        }]
    },
    {
        "Name": "usb_sagrowthcontributionoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_sagrowthcontributionoptionset"
        }]
    },
    {
        "Name": "usb_salaryoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "&lt;R150 000",
            "value": 1
        },
        {
            "text": "R150 000 - R300 000",
            "value": 2
        },
        {
            "text": "R300 000 - R450 000",
            "value": 3
        },
        {
            "text": "R450 000 - R600 000",
            "value": 4
        },
        {
            "text": "R600 000 - R750 000",
            "value": 5
        },
        {
            "text": "&gt;R750 000",
            "value": 6
        }],
        "SelectedOption": {
            "option": "&lt;R150 000",
            "value": 1
        },
        "Text": "&lt;R150 000",
        "Controls": [{
            "Name": "usb_salaryoptionset"
        }]
    },
    {
        "Name": "usb_scenariotoolsoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_scenariotoolsoptionset"
        }]
    },
    {
        "Name": "usb_searchenginetwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "usb_searchenginetwooptions"
        }]
    },
    {
        "Name": "usb_selfdevelopmentoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_selfdevelopmentoptionset"
        }]
    },
    {
        "Name": "usb_shlfeerefno",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "usb_shlfeerefno"
        }]
    },
    {
        "Name": "usb_shlnmg35",
        "Value": null,
        "Type": "integer",
        "Format": "none",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Max": 100,
        "Min": 0,
        "Precision": 0,
        "Controls": [{
            "Name": "usb_shlnmg35"
        }]
    },
    {
        "Name": "usb_shlnumeric",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "usb_shlnumeric"
        }]
    },
    {
        "Name": "usb_shloverall",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "usb_shloverall"
        }]
    },
    {
        "Name": "usb_shltestrequiredtwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "usb_shltestrequiredtwooptions"
        }]
    },
    {
        "Name": "usb_shlvmg48",
        "Value": null,
        "Type": "integer",
        "Format": "none",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Max": 100,
        "Min": 0,
        "Precision": 0,
        "Controls": [{
            "Name": "usb_shlvmg48"
        }]
    },
    {
        "Name": "usb_socialmediatwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "usb_socialmediatwooptions"
        }]
    },
    {
        "Name": "usb_stellenboschuniversitytwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "usb_stellenboschuniversitytwooptions"
        }]
    },
    {
        "Name": "usb_stennmg",
        "Value": null,
        "Type": "integer",
        "Format": "none",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Max": 100,
        "Min": 0,
        "Precision": 0,
        "Controls": [{
            "Name": "usb_stennmg"
        }]
    },
    {
        "Name": "usb_stenvng",
        "Value": null,
        "Type": "integer",
        "Format": "none",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Max": 100,
        "Min": 0,
        "Precision": 0,
        "Controls": [{
            "Name": "usb_stenvng"
        }]
    },
    {
        "Name": "usb_studymaterialoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_studymaterialoptionset"
        }]
    },
    {
        "Name": "usb_sustainablefutureoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_sustainablefutureoptionset"
        }]
    },
    {
        "Name": "usb_sustainabletransoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_sustainabletransoptionset"
        }]
    },
    {
        "Name": "usb_symbolachieved",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "usb_symbolachieved"
        }]
    },
    {
        "Name": "usb_theoreticalcoachingoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Of little importance",
            "value": 864480001
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_theoreticalcoachingoptionset"
        }]
    },
    {
        "Name": "usb_theoryoffututestudiesoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_theoryoffututestudiesoptionset"
        }]
    },
    {
        "Name": "usb_totalyearsworkingexperience",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "usb_totalyearsworkingexperience"
        }]
    },
    {
        "Name": "usb_tripleaccreditationtwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "usb_tripleaccreditationtwooptions"
        }]
    },
    {
        "Name": "usb_usb_developsaeconomyoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Very important",
            "value": 100000
        },
        {
            "text": "Important",
            "value": 100001
        },
        {
            "text": "Not important or unimportant (neutral)",
            "value": 100002
        },
        {
            "text": "Of little importance",
            "value": 100003
        },
        {
            "text": "Totally unimportant",
            "value": 100004
        }],
        "SelectedOption": {
            "option": "Very important",
            "value": 100000
        },
        "Text": "Very important",
        "Controls": [{
            "Name": "usb_usb_developsaeconomyoptionset"
        }]
    },
    {
        "Name": "usb_usb_usbedtwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "usb_usb_usbedtwooptions"
        }]
    },
    {
        "Name": "usb_usbafricafocustwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "usb_usbafricafocustwooptions"
        }]
    },
    {
        "Name": "usb_usbcollaborativelearningtwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "usb_usbcollaborativelearningtwooptions"
        }]
    },
    {
        "Name": "usb_usbgeneralleadershipfocustwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "usb_usbgeneralleadershipfocustwooptions"
        }]
    },
    {
        "Name": "usb_usbinternationaltwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "usb_usbinternationaltwooptions"
        }]
    },
    {
        "Name": "usb_usbwebsitetwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "usb_usbwebsitetwooptions"
        }]
    },
    {
        "Name": "usb_usnumber",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 8,
        "Controls": [{
            "Name": "header_usb_usnumber"
        },
        {
            "Name": "usb_usnumber"
        }]
    },
    {
        "Name": "usb_valueformoneytwooptions",
        "Value": null,
        "Type": "boolean",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": null,
        "Controls": [{
            "Name": "usb_valueformoneytwooptions"
        }]
    },
    {
        "Name": "usb_verbal",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "usb_verbal"
        }]
    },
    {
        "Name": "usb_whatdidyoudolastyearoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "College Of Education",
            "value": 6
        },
        {
            "text": "Employed",
            "value": 3
        },
        {
            "text": "Grade 12 / School",
            "value": 1
        },
        {
            "text": "Military Service",
            "value": 2
        },
        {
            "text": "Nursing College",
            "value": 8
        },
        {
            "text": "Technical Institute",
            "value": 7
        },
        {
            "text": "Technikon",
            "value": 5
        },
        {
            "text": "University",
            "value": 4
        },
        {
            "text": "Former SU Student",
            "value": 9
        }],
        "SelectedOption": {
            "option": "College Of Education",
            "value": 6
        },
        "Text": "College Of Education",
        "Controls": [{
            "Name": "usb_whatdidyoudolastyearoptionset"
        }]
    }],
    "AttributesLength": 166,
    "Controls": [, {
        "Name": "header_ownerid",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Owner",
        "Attribute": "ownerid"
    },
    {
        "Name": "header_statuscode",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Status Reason",
        "Attribute": "statuscode"
    },
    {
        "Name": "header_usb_usnumber",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Student Number",
        "Attribute": "usb_usnumber"
    },
    {
        "Name": "usb_academicsupportoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Academic support",
        "Attribute": "usb_academicsupportoptionset"
    },
    {
        "Name": "usb_acceptanceformtwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Acceptance Form: Reviewed",
        "Attribute": "usb_acceptanceformtwooptions"
    },
    {
        "Name": "usb_acquirebusinessknowledgeoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "I want to acquire business knowledge",
        "Attribute": "usb_acquirebusinessknowledgeoptionset"
    },
    {
        "Name": "usb_acquiredevfinskillsoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "I want to acquire specialist skills in development finance",
        "Attribute": "usb_acquiredevfinskillsoptionset"
    },
    {
        "Name": "usb_acquiremanagementskillsoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "I want to acquire management skills",
        "Attribute": "usb_acquiremanagementskillsoptionset"
    },
    {
        "Name": "usb_adminsupportoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Administrative support",
        "Attribute": "usb_adminsupportoptionset"
    },
    {
        "Name": "usb_advertisementtwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Advertisement",
        "Attribute": "usb_advertisementtwooptions"
    },
    {
        "Name": "usb_africaemergingmarketoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "I want to gain insight in Africa as an emerging market",
        "Attribute": "usb_africaemergingmarketoptionset"
    },
    {
        "Name": "usb_africamultinationalenvironmentoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "I want to learn how to operate in the multi-nation business environment of Africa",
        "Attribute": "usb_africamultinationalenvironmentoptionset"
    },
    {
        "Name": "usb_africastrategicdecisionoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "I want to be able to make strategic decisions for Africa's development",
        "Attribute": "usb_africastrategicdecisionoptionset"
    },
    {
        "Name": "usb_africasustainabledevelopmentoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "I want to contribute towards Africa’s sustainable development",
        "Attribute": "usb_africasustainabledevelopmentoptionset"
    },
    {
        "Name": "usb_afrikaansleesoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Afrikaans Lees",
        "Attribute": "usb_afrikaansleesoptionset"
    },
    {
        "Name": "usb_afrikaanspraatoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Afrikaans Praat",
        "Attribute": "usb_afrikaanspraatoptionset"
    },
    {
        "Name": "usb_afrikaansskryfoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Afrikaans Skryf",
        "Attribute": "usb_afrikaansskryfoptionset"
    },
    {
        "Name": "usb_afrikaansverstaanoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Afrikaans Verstaan",
        "Attribute": "usb_afrikaansverstaanoptionset"
    },
    {
        "Name": "usb_alignforfuturistoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "I want to align myself with skills required for futurists",
        "Attribute": "usb_alignforfuturistoptionset"
    },
    {
        "Name": "usb_analyticalwriting",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Analytical Writing",
        "Attribute": "usb_analyticalwriting"
    },
    {
        "Name": "usb_applicationfeerefno",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Application Fee Ref No:",
        "Attribute": "usb_applicationfeerefno"
    },
    {
        "Name": "usb_applicationfeetwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Application Fee: Reviewed",
        "Attribute": "usb_applicationfeetwooptions"
    },
    {
        "Name": "usb_applicationformurl",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Application Form URL",
        "Attribute": "usb_applicationformurl"
    },
    {
        "Name": "usb_aquirescareskillsoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "I want to acquire this scarce skill",
        "Attribute": "usb_aquirescareskillsoptionset"
    },
    {
        "Name": "usb_aquirethinkingskillsoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "I want to acquire critical thinking skills",
        "Attribute": "usb_aquirethinkingskillsoptionset"
    },
    {
        "Name": "usb_attendedinfosessiontwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Have you attended an Info Session?",
        "Attribute" : "usb_attendedinfosessiontwooptions"
    },
    {
        "Name": "usb_authenticleadershipoptioset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "I want to develop my own authentic leadership styl",
        "Attribute": "usb_authenticleadershipoptioset"
    },
    {
        "Name": "usb_availablecareeroptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Availability of career services",
        "Attribute": "usb_availablecareeroptionset"
    },
    {
        "Name": "usb_benefitfromuniquetypeoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "I want to benefit from this unique type of program",
        "Attribute": "usb_benefitfromuniquetypeoptionset"
    },
    {
        "Name": "usb_blendedlearningoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Blended learning delivery mode",
        "Attribute": "usb_blendedlearningoptionset"
    },
    {
        "Name": "usb_brochuretwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Brochure",
        "Attribute": "usb_brochuretwooptions"
    },
    {
        "Name": "usb_bursaryapplicanttwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Bursary Applicant",
        "Attribute": "usb_bursaryapplicanttwooptions"
    },
    {
        "Name": "usb_bursaryawardeetwootions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Bursary Awardee",
        "Attribute": "usb_bursaryawardeetwootions"
    },
    {
        "Name": "usb_businessleaderoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "I want to develop as a business leader",
        "Attribute": "usb_businessleaderoptionset"
    },
    {
        "Name": "usb_campussecurityoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Campus security",
        "Attribute": "usb_campussecurityoptionset"
    },
    {
        "Name": "usb_careerchangeoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "I want to change careers",
        "Attribute": "usb_careerchangeoptionset"
    },
    {
        "Name": "usb_cateringservicesoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Catering services",
        "Attribute": "usb_cateringservicesoptionset"
    },
    {
        "Name": "usb_classroombasedoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Classroom based delivery mode",
        "Attribute": "usb_classroombasedoptionset"
    },
    {
        "Name": "usb_closetohometwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Geographic location (close to home)",
        "Attribute": "usb_closetohometwooptions"
    },
    {
        "Name": "usb_complexorgenvironmentoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "I want to gain insight in the complexity of the organisational environment",
        "Attribute": "usb_complexorgenvironmentoptionset"
    },
    {
        "Name": "usb_conferenceexpotwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Conference or expo",
        "Attribute": "usb_conferenceexpotwooptions"
    },
    {
        "Name": "usb_contactlookup",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Contact",
        "Attribute": "usb_contactlookup"
    },
    {
        "Name": "usb_deliverymodetwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Delivery mode (blended learning, face-to-face, etc",
        "Attribute": "usb_deliverymodetwooptions"
    },
    {
        "Name": "usb_depositrefno",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Deposit Ref No:",
        "Attribute": "usb_depositrefno"
    },
    {
        "Name": "usb_developasaleaderoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "I want to develop as a leader",
        "Attribute": "usb_developasaleaderoptionset"
    },
    {
        "Name": "usb_developefuturestudiesoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "I want to develop my own futures studies model",
        "Attribute": "usb_developefuturestudiesoptionset"
    },
    {
        "Name": "usb_developmentofothersoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "I want to contribute to the development of others",
        "Attribute": "usb_developmentofothersoptionset"
    },
    {
        "Name": "usb_digitalskillsoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "I want to acquire digital skills for business",
        "Attribute": "usb_digitalskillsoptionset"
    },
    {
        "Name": "usb_diverseclassgroupoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Diverse class group",
        "Attribute": "usb_diverseclassgroupoptionset"
    },
    {
        "Name": "usb_diversefaultycompositeoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Diverse faculty composition",
        "Attribute": "usb_diversefaultycompositeoptionset"
    },
    {
        "Name": "usb_electronicstudymaterialoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Electronic study material",
        "Attribute": "usb_electronicstudymaterialoptionset"
    },
    {
        "Name": "usb_employerassistfinanciallyoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Will your employer be assisting you financially?",
        "Attribute" : "usb_employerassistfinanciallyoptionset"
    },
    {
        "Name": "usb_employeremail",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Employer Contact Email",
        "Attribute": "usb_employeremail"
    },
    {
        "Name": "usb_employername",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Employer Name",
        "Attribute": "usb_employername"
    },
    {
        "Name": "usb_employertelephone",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Employer Contact Telephone",
        "Attribute": "usb_employertelephone"
    },
    {
        "Name": "usb_englishreadoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "English Read",
        "Attribute": "usb_englishreadoptionset"
    },
    {
        "Name": "usb_englishspeakoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "English Speak",
        "Attribute": "usb_englishspeakoptionset"
    },
    {
        "Name": "usb_englishunderstandoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "English Understand",
        "Attribute": "usb_englishunderstandoptionset"
    },
    {
        "Name": "usb_englishwriteoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "English Write",
        "Attribute": "usb_englishwriteoptionset"
    },
    {
        "Name": "usb_enhancecoachingskillsoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "I want to enhance my coaching skills",
        "Attribute": "usb_enhancecoachingskillsoptionset"
    },
    {
        "Name": "usb_entrepreneurialknowledgeoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "I want to acquire entrepreneurial knowledge",
        "Attribute": "usb_entrepreneurialknowledgeoptionset"
    },
    {
        "Name": "usb_eqitwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "EQI",
        "Attribute": "usb_eqitwooptions"
    },
    {
        "Name": "usb_excellentreputationtwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Excellent reputation",
        "Attribute": "usb_excellentreputationtwooptions"
    },
    {
        "Name": "usb_firstname",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "First Name",
        "Attribute": "usb_firstname"
    },
    {
        "Name": "usb_gainknowledgeoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "I want to gain knowledge of business strategy",
        "Attribute": "usb_gainknowledgeoptionset"
    },
    {
        "Name": "usb_gmatresult",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "GMAT Result/Verbal",
        "Attribute": "usb_gmatresult"
    },
    {
        "Name": "usb_highersalaryoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "I want to earn a high salary when I graduate",
        "Attribute": "usb_highersalaryoptionset"
    },
    {
        "Name": "usb_highestlevelmathematicscompetedoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Highest mathematics level successfully completed?",
        "Attribute" : "usb_highestlevelmathematicscompetedoptionset"
    },
    {
        "Name": "usb_highqualitystudentstwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "High quality students",
        "Attribute": "usb_highqualitystudentstwooptions"
    },
    {
        "Name": "usb_honoursyear",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Honours Year",
        "Attribute": "usb_honoursyear"
    },
    {
        "Name": "usb_howtobealeaderoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "I want to understand how to be a leader in the South African context",
        "Attribute": "usb_howtobealeaderoptionset"
    },
    {
        "Name": "usb_iamamatietwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "I'm a Matie and want to study at Stellenbosch agai",
        "Attribute": "usb_iamamatietwooptions"
    },
    {
        "Name": "usb_ictsupportoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "ICT support",
        "Attribute": "usb_ictsupportoptionset"
    },
    {
        "Name": "usb_inductivereasoning",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Inductive Reasoning",
        "Attribute": "usb_inductivereasoning"
    },
    {
        "Name": "usb_infosessionoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Which Info Session did you attend?",
        "Attribute" : "usb_infosessionoptionset"
    },
    {
        "Name": "usb_innovativeoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "I want to learn how to be innovative",
        "Attribute": "usb_innovativeoptionset"
    },
    {
        "Name": "usb_insightpoliciesprogrammesoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "I want to acquire insight to shape policies and programmes",
        "Attribute": "usb_insightpoliciesprogrammesoptionset"
    },
    {
        "Name": "usb_internationallecturersinclasstwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "International lecturers in class (electives)",
        "Attribute": "usb_internationallecturersinclasstwooptions"
    },
    {
        "Name": "usb_internationalstudentstwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "International students on campus",
        "Attribute": "usb_internationalstudentstwooptions"
    },
    {
        "Name": "usb_interviewedby",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Round 1: Interview by (If more than 1)",
        "Attribute": "usb_interviewedby"
    },
    {
        "Name": "usb_lastname",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Last Name",
        "Attribute": "usb_lastname"
    },
    {
        "Name": "usb_lecturerqaulityoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Lecturer quality",
        "Attribute": "usb_lecturerqaulityoptionset"
    },
    {
        "Name": "usb_libraryservicesoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Library services",
        "Attribute": "usb_libraryservicesoptionset"
    },
    {
        "Name": "usb_makeadifferenceoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "I want to make a difference",
        "Attribute": "usb_makeadifferenceoptionset"
    },
    {
        "Name": "usb_manageprojectsoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "I want to learn to confidently manage projects",
        "Attribute": "usb_manageprojectsoptionset"
    },
    {
        "Name": "usb_mastersyear",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Masters Year",
        "Attribute": "usb_mastersyear"
    },
    {
        "Name": "usb_mentorsoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Availability of mentors (alumni)",
        "Attribute": "usb_mentorsoptionset"
    },
    {
        "Name": "usb_name",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Name",
        "Attribute": "usb_name"
    },
    {
        "Name": "usb_newsarticletwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "News article",
        "Attribute": "usb_newsarticletwooptions"
    },
    {
        "Name": "usb_offcampussupportoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Off-campus support",
        "Attribute": "usb_offcampussupportoptionset"
    },
    {
        "Name": "usb_oncampuseventtwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "On-campus event (Leader’s angle, etc.)",
        "Attribute": "usb_oncampuseventtwooptions"
    },
    {
        "Name": "usb_owncoachingpracticeoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "I want to start my own coaching practice",
        "Attribute": "usb_owncoachingpracticeoptionset"
    },
    {
        "Name": "usb_ownframeworkmodeloptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "I want to develop my own personalised coaching framework and model",
        "Attribute": "usb_ownframeworkmodeloptionset"
    },
    {
        "Name": "usb_phdyear",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Phd Year",
        "Attribute": "usb_phdyear"
    },
    {
        "Name": "usb_pmskillsoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "I want to acquire project management skills",
        "Attribute": "usb_pmskillsoptionset"
    },
    {
        "Name": "usb_postgraduateyear",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Postgraduate Year",
        "Attribute": "usb_postgraduateyear"
    },
    {
        "Name": "usb_professionalcoachescommunityoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "I want to join the community of professional coaches",
        "Attribute": "usb_professionalcoachescommunityoptionset"
    },
    {
        "Name": "usb_professionalfuturistoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "I want to become a professional futurist",
        "Attribute": "usb_professionalfuturistoptionset"
    },
    {
        "Name": "usb_programmeformattwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Programme format (modular, etc.)",
        "Attribute": "usb_programmeformattwooptions"
    },
    {
        "Name": "usb_programmeofferinglookup",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Programme Offering",
        "Attribute": "usb_programmeofferinglookup"
    },
    {
        "Name": "usb_progresscareeroptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "I want to progress in my career",
        "Attribute": "usb_progresscareeroptionset"
    },
    {
        "Name": "usb_progresstombaoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "I want to progress to the MBA",
        "Attribute": "usb_progresstombaoptionset"
    },
    {
        "Name": "usb_promotionalemailstwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "USB promotional emails",
        "Attribute": "usb_promotionalemailstwooptions"
    },
    {
        "Name": "usb_reasonforstoppingoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Reason for Stopping",
        "Attribute": "usb_reasonforstoppingoptionset"
    },
    {
        "Name": "usb_reasonforstoppingother",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Reason For Stopping (Other)",
        "Attribute": "usb_reasonforstoppingother"
    },
    {
        "Name": "usb_recommendedbyalumnitwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Recommended by alumni of USB",
        "Attribute": "usb_recommendedbyalumnitwooptions"
    },
    {
        "Name": "usb_recommendedbyemployertwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Recommended by employer",
        "Attribute": "usb_recommendedbyemployertwooptions"
    },
    {
        "Name": "usb_referee1_cellphone",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Cellphone",
        "Attribute": "usb_referee1_cellphone"
    },
    {
        "Name": "usb_referee1_email",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Email",
        "Attribute": "usb_referee1_email"
    },
    {
        "Name": "usb_referee1_name",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Name",
        "Attribute": "usb_referee1_name"
    },
    {
        "Name": "usb_referee1_position",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Position",
        "Attribute": "usb_referee1_position"
    },
    {
        "Name": "usb_referee1_telephone",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Telephone",
        "Attribute": "usb_referee1_telephone"
    },
    {
        "Name": "usb_referee2_cellphone",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Cellphone",
        "Attribute": "usb_referee2_cellphone"
    },
    {
        "Name": "usb_referee2_email",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Email",
        "Attribute": "usb_referee2_email"
    },
    {
        "Name": "usb_referee2_name",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Name",
        "Attribute": "usb_referee2_name"
    },
    {
        "Name": "usb_referee2_position",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Position",
        "Attribute": "usb_referee2_position"
    },
    {
        "Name": "usb_referee2_telephone",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Telephone",
        "Attribute": "usb_referee2_telephone"
    },
    {
        "Name": "usb_referralbyalumnustwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Referral by an alumnus",
        "Attribute": "usb_referralbyalumnustwooptions"
    },
    {
        "Name": "usb_referralbycurrentstudenttwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Referral by a current student",
        "Attribute": "usb_referralbycurrentstudenttwooptions"
    },
    {
        "Name": "usb_referralbyemployertwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Referral by my employer",
        "Attribute": "usb_referralbyemployertwooptions"
    },
    {
        "Name": "usb_referralbyfamilyfriendtwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Referral by a family member or friend",
        "Attribute": "usb_referralbyfamilyfriendtwooptions"
    },
    {
        "Name": "usb_researchsupervisionoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Research supervision",
        "Attribute": "usb_researchsupervisionoptionset"
    },
    {
        "Name": "usb_responsibleleaderoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "I want to become a responsible and ethical leader",
        "Attribute": "usb_responsibleleaderoptionset"
    },
    {
        "Name": "usb_round1interviewcompletedtwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Round 1: Interview Completed",
        "Attribute": "usb_round1interviewcompletedtwooptions"
    },
    {
        "Name": "usb_round1interviewerlookup",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Round 1: Interviewer",
        "Attribute": "usb_round1interviewerlookup"
    },
    {
        "Name": "usb_round1interviewreqtwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Round 1: Interview Required",
        "Attribute": "usb_round1interviewreqtwooptions"
    },
    {
        "Name": "usb_round1interviewtypeoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Round 1: Interview Type",
        "Attribute": "usb_round1interviewtypeoptionset"
    },
    {
        "Name": "usb_round2interviewcompletedtwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Round 2: Interview Completed",
        "Attribute": "usb_round2interviewcompletedtwooptions"
    },
    {
        "Name": "usb_round2interviewedby",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Round 2: Interview by (if more than 1)",
        "Attribute": "usb_round2interviewedby"
    },
    {
        "Name": "usb_round2interviewerlookup",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Round 2: Interviewer",
        "Attribute": "usb_round2interviewerlookup"
    },
    {
        "Name": "usb_round2interviewreqtwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Round 2: Interview Required",
        "Attribute": "usb_round2interviewreqtwooptions"
    },
    {
        "Name": "usb_round2interviewtypeoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Round 2: Interview Type",
        "Attribute": "usb_round2interviewtypeoptionset"
    },
    {
        "Name": "usb_rplcandidatetwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "RPL Candidate",
        "Attribute": "usb_rplcandidatetwooptions"
    },
    {
        "Name": "usb_sagrowthcontributionoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "I want to make a contribution to growth in South Africa or the African continent",
        "Attribute": "usb_sagrowthcontributionoptionset"
    },
    {
        "Name": "usb_salaryoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Salary",
        "Attribute": "usb_salaryoptionset"
    },
    {
        "Name": "usb_scenariotoolsoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "I want to acquire the tools for scenario planning",
        "Attribute": "usb_scenariotoolsoptionset"
    },
    {
        "Name": "usb_searchenginetwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Did you use a Search Engine?",
        "Attribute" : "usb_searchenginetwooptions"
    },
    {
        "Name": "usb_selfdevelopmentoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "I want to develop myself",
        "Attribute": "usb_selfdevelopmentoptionset"
    },
    {
        "Name": "usb_shlfeerefno",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "SHL Fee Ref No:",
        "Attribute": "usb_shlfeerefno"
    },
    {
        "Name": "usb_shlnmg35",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "SHL NMG/35",
        "Attribute": "usb_shlnmg35"
    },
    {
        "Name": "usb_shlnumeric",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Numeric",
        "Attribute": "usb_shlnumeric"
    },
    {
        "Name": "usb_shloverall",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Overall",
        "Attribute": "usb_shloverall"
    },
    {
        "Name": "usb_shltestrequiredtwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "SHL (Test Required)",
        "Attribute": "usb_shltestrequiredtwooptions"
    },
    {
        "Name": "usb_shlvmg48",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "SHL VMG/48",
        "Attribute": "usb_shlvmg48"
    },
    {
        "Name": "usb_socialmediatwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Social media",
        "Attribute": "usb_socialmediatwooptions"
    },
    {
        "Name": "usb_stellenboschuniversitytwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Stellenbosch University",
        "Attribute": "usb_stellenboschuniversitytwooptions"
    },
    {
        "Name": "usb_stennmg",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "STEN NMG",
        "Attribute": "usb_stennmg"
    },
    {
        "Name": "usb_stenvng",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "STEN VNG",
        "Attribute": "usb_stenvng"
    },
    {
        "Name": "usb_studymaterialoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Printed study material",
        "Attribute": "usb_studymaterialoptionset"
    },
    {
        "Name": "usb_sustainablefutureoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "I want to be involved in creating a desired and sustainable future",
        "Attribute": "usb_sustainablefutureoptionset"
    },
    {
        "Name": "usb_sustainabletransoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "I want to enable sustainable transformation in my organisation",
        "Attribute": "usb_sustainabletransoptionset"
    },
    {
        "Name": "usb_symbolachieved",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Symbol Achieved",
        "Attribute": "usb_symbolachieved"
    },
    {
        "Name": "usb_theoreticalcoachingoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "I want to gain a theoretical understanding of coaching",
        "Attribute": "usb_theoreticalcoachingoptionset"
    },
    {
        "Name": "usb_theoryoffututestudiesoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "I want to understand the theory of futures studies",
        "Attribute": "usb_theoryoffututestudiesoptionset"
    },
    {
        "Name": "usb_totalyearsworkingexperience",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Total Years Working Experience",
        "Attribute": "usb_totalyearsworkingexperience"
    },
    {
        "Name": "usb_tripleaccreditationtwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Triple International Accreditation",
        "Attribute": "usb_tripleaccreditationtwooptions"
    },
    {
        "Name": "usb_usb_developsaeconomyoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "I want to gain knowledge on how to develop the SA economy",
        "Attribute": "usb_usb_developsaeconomyoptionset"
    },
    {
        "Name": "usb_usb_usbedtwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "USB-ED",
        "Attribute": "usb_usb_usbedtwooptions"
    },
    {
        "Name": "usb_usbafricafocustwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "USB’s focus on Africa",
        "Attribute": "usb_usbafricafocustwooptions"
    },
    {
        "Name": "usb_usbcollaborativelearningtwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "USB’s collaborative learning approach",
        "Attribute": "usb_usbcollaborativelearningtwooptions"
    },
    {
        "Name": "usb_usbgeneralleadershipfocustwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "USB’s general focus on responsible leadership",
        "Attribute": "usb_usbgeneralleadershipfocustwooptions"
    },
    {
        "Name": "usb_usbinternationaltwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "USB’s international networks",
        "Attribute": "usb_usbinternationaltwooptions"
    },
    {
        "Name": "usb_usbwebsitetwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "USB website",
        "Attribute": "usb_usbwebsitetwooptions"
    },
    {
        "Name": "usb_usnumber",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Student Number",
        "Attribute": "usb_usnumber"
    },
    {
        "Name": "usb_valueformoneytwooptions",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Value for money",
        "Attribute": "usb_valueformoneytwooptions"
    },
    {
        "Name": "usb_verbal",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Verbal",
        "Attribute": "usb_verbal"
    },
    {
        "Name": "usb_whatdidyoudolastyearoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "What did you do last Year?",
        "Attribute" : "usb_whatdidyoudolastyearoptionset"
    }],
    "ControlsLength": 167,
    "Navigation": [{
        "Id": "navConnections",
        "Key": "navConnections",
        "Label": "",
        "Visible": true
    }],
    "Tabs": [{
        "Label": "General",
        "Name": "{d052633c-aece-4c4a-af3c-677658498b5c}",
        "DisplayState": "expanded",
        "Visible": true,
        "Sections": [{
            "Label": "General",
            "Name": "{06f18b54-7589-43f4-856e-8b3b2657bb03}",
            "Visible": true,
            "Controls": [{
                "Name": "usb_usnumber"
            },
            {
                "Name": "usb_contactlookup"
            },
            {
                "Name": "usb_firstname"
            },
            {
                "Name": "usb_lastname"
            },
            {
                "Name": "usb_applicationformurl"
            },
            {
                "Name": "usb_programmeofferinglookup"
            },
            {
                "Name": "usb_salaryoptionset"
            },
            {
                "Name": "usb_reasonforstoppingoptionset"
            },
            {
                "Name": "usb_totalyearsworkingexperience"
            },
            {
                "Name": "usb_reasonforstoppingother"
            },
            {
                "Name": "usb_name"
            },
            {
                "Name": "usb_whatdidyoudolastyearoptionset"
            },
            {
                "Name": "usb_postgraduateyear"
            },
            {
                "Name": "usb_honoursyear"
            },
            {
                "Name": "usb_mastersyear"
            },
            {
                "Name": "usb_phdyear"
            }]
        },
        {
            "Label": "Notes",
            "Name": "{10f7f326-e19c-48bb-a904-d9bc11215a57}",
            "Visible": true,
            "Controls": [{
                "Name": "notescontrol"
            }]
        }]
    },
    {
        "Label": "Academic",
        "Name": "tab_4",
        "DisplayState": "expanded",
        "Visible": true,
        "Sections": [{
            "Label": "Section",
            "Name": "tab_4_section_1",
            "Visible": true,
            "Controls": [{
                "Name": "usb_afrikaansleesoptionset"
            },
            {
                "Name": "usb_englishreadoptionset"
            },
            {
                "Name": "usb_afrikaanspraatoptionset"
            },
            {
                "Name": "usb_englishspeakoptionset"
            },
            {
                "Name": "usb_afrikaansskryfoptionset"
            },
            {
                "Name": "usb_englishwriteoptionset"
            },
            {
                "Name": "usb_afrikaansverstaanoptionset"
            },
            {
                "Name": "usb_englishunderstandoptionset"
            },
            {
                "Name": "usb_highestlevelmathematicscompetedoptionset"
            },
            {
                "Name": "usb_symbolachieved"
            }]
        }]
    },
    {
        "Label": "Financial Assistance",
        "Name": "tab_5",
        "DisplayState": "expanded",
        "Visible": true,
        "Sections": [{
            "Label": "Section",
            "Name": "tab_5_section_1",
            "Visible": true,
            "Controls": [{
                "Name": "usb_employerassistfinanciallyoptionset"
            },
            {
                "Name": "usb_employeremail"
            },
            {
                "Name": "usb_employername"
            },
            {
                "Name": "usb_employertelephone"
            }]
        }]
    },
    {
        "Label": "Assessment Details",
        "Name": "tab_8",
        "DisplayState": "expanded",
        "Visible": true,
        "Sections": [{
            "Label": "SHL",
            "Name": "tab_8_section_1",
            "Visible": true,
            "Controls": [{
                "Name": "usb_shltestrequiredtwooptions"
            },
            {
                "Name": "usb_shlnmg35"
            },
            {
                "Name": "usb_shlvmg48"
            },
            {
                "Name": "usb_stennmg"
            },
            {
                "Name": "usb_stenvng"
            },
            {
                "Name": "usb_verbal"
            },
            {
                "Name": "usb_inductivereasoning"
            }]
        },
        {
            "Label": "GMAT",
            "Name": "tab_8_section_2",
            "Visible": true,
            "Controls": [{
                "Name": "usb_analyticalwriting"
            },
            {
                "Name": "usb_shlnumeric"
            },
            {
                "Name": "usb_gmatresult"
            },
            {
                "Name": "usb_shloverall"
            },
            {
                "Name": "usb_eqitwooptions"
            }]
        }]
    },
    {
        "Label": "Payment Details",
        "Name": "tab_9",
        "DisplayState": "expanded",
        "Visible": true,
        "Sections": [{
            "Label": "Section",
            "Name": "tab_9_section_1",
            "Visible": true,
            "Controls": [{
                "Name": "usb_applicationfeetwooptions"
            },
            {
                "Name": "usb_acceptanceformtwooptions"
            },
            {
                "Name": "usb_applicationfeerefno"
            },
            {
                "Name": "usb_depositrefno"
            },
            {
                "Name": "usb_shlfeerefno"
            },
            {
                "Name": "usb_bursaryapplicanttwooptions"
            },
            {
                "Name": "usb_bursaryawardeetwootions"
            }]
        }]
    },
    {
        "Label": "Interview",
        "Name": "tab_7",
        "DisplayState": "expanded",
        "Visible": true,
        "Sections": [{
            "Label": "Section",
            "Name": "tab_7_section_1",
            "Visible": true,
            "Controls": [{
                "Name": "usb_rplcandidatetwooptions"
            }]
        },
        {
            "Label": "Interview Round 1",
            "Name": "tab_7_section_2",
            "Visible": true,
            "Controls": [{
                "Name": "usb_round1interviewreqtwooptions"
            },
            {
                "Name": "usb_round1interviewerlookup"
            },
            {
                "Name": "usb_round1interviewtypeoptionset"
            },
            {
                "Name": "usb_interviewedby"
            },
            {
                "Name": "usb_round1interviewcompletedtwooptions"
            }]
        },
        {
            "Label": "Interview Round 2",
            "Name": "tab_7_section_3",
            "Visible": true,
            "Controls": [{
                "Name": "usb_round2interviewreqtwooptions"
            },
            {
                "Name": "usb_round2interviewerlookup"
            },
            {
                "Name": "usb_round2interviewtypeoptionset"
            },
            {
                "Name": "usb_round2interviewedby"
            },
            {
                "Name": "usb_round2interviewcompletedtwooptions"
            }]
        }]
    },
    {
        "Label": "Referee",
        "Name": "tab_3",
        "DisplayState": "expanded",
        "Visible": true,
        "Sections": [{
            "Label": "Referee 1",
            "Name": "tab_3_section_1",
            "Visible": true,
            "Controls": [{
                "Name": "usb_referee1_name"
            },
            {
                "Name": "usb_referee1_telephone"
            },
            {
                "Name": "usb_referee1_email"
            },
            {
                "Name": "usb_referee1_cellphone"
            },
            {
                "Name": "usb_referee1_position"
            }]
        },
        {
            "Label": "Referee 2",
            "Name": "tab_3_section_2",
            "Visible": true,
            "Controls": [{
                "Name": "usb_referee2_name"
            },
            {
                "Name": "usb_referee2_telephone"
            },
            {
                "Name": "usb_referee2_email"
            },
            {
                "Name": "usb_referee2_cellphone"
            },
            {
                "Name": "usb_referee2_position"
            }]
        }]
    },
    {
        "Label": "Tab",
        "Name": "tab_10",
        "DisplayState": "expanded",
        "Visible": true,
        "Sections": [{
            "Label": "Section",
            "Name": "tab_10_section_1",
            "Visible": true,
            "Controls": [{
                "Name": "EmploymentHistory"
            }]
        },
        {
            "Label": "Section",
            "Name": "tab_10_section_3",
            "Visible": true,
            "Controls": [{
                "Name": "Qualifications"
            }]
        }]
    },
    {
        "Label": "Marketing",
        "Name": "tab_2",
        "DisplayState": "expanded",
        "Visible": true,
        "Sections": [{
            "Label": "Where have you heard about USB",
            "Name": "tab_2_section_1",
            "Visible": true,
            "Controls": [{
                "Name": "usb_advertisementtwooptions"
            },
            {
                "Name": "usb_newsarticletwooptions"
            },
            {
                "Name": "usb_promotionalemailstwooptions"
            },
            {
                "Name": "usb_conferenceexpotwooptions"
            },
            {
                "Name": "usb_referralbyemployertwooptions"
            },
            {
                "Name": "usb_stellenboschuniversitytwooptions"
            },
            {
                "Name": "usb_referralbyalumnustwooptions"
            },
            {
                "Name": "usb_usbwebsitetwooptions"
            },
            {
                "Name": "usb_referralbycurrentstudenttwooptions"
            },
            {
                "Name": "usb_usb_usbedtwooptions"
            },
            {
                "Name": "usb_referralbyfamilyfriendtwooptions"
            },
            {
                "Name": "usb_brochuretwooptions"
            },
            {
                "Name": "usb_socialmediatwooptions"
            },
            {
                "Name": "usb_oncampuseventtwooptions"
            }]
        },
        {
            "Label": "Section",
            "Name": "tab_2_section_2",
            "Visible": true,
            "Controls": [{
                "Name": "usb_attendedinfosessiontwooptions"
            },
            {
                "Name": "usb_infosessionoptionset"
            },
            {
                "Name": "usb_searchenginetwooptions"
            }]
        },
        {
            "Label": "How Important were the following when you decided to do your MBA?",
            "Name" : "tab_2_section_3",
            "Visible": true,
            "Controls": [{
                "Name": "usb_careerchangeoptionset"
            },
            {
                "Name": "usb_progresscareeroptionset"
            },
            {
                "Name": "usb_developasaleaderoptionset"
            },
            {
                "Name": "usb_aquirethinkingskillsoptionset"
            },
            {
                "Name": "usb_acquiremanagementskillsoptionset"
            },
            {
                "Name": "usb_highersalaryoptionset"
            },
            {
                "Name": "usb_responsibleleaderoptionset"
            },
            {
                "Name": "usb_acquirebusinessknowledgeoptionset"
            },
            {
                "Name": "usb_gainknowledgeoptionset"
            }]
        },
        {
            "Label": "How important were the following when you decided to do your MPhil/PGDip in Development Finance?",
            "Name" : "tab_2_section_6",
            "Visible": true,
            "Controls": [{
                "Name": "usb_acquiredevfinskillsoptionset"
            },
            {
                "Name": "usb_africasustainabledevelopmentoptionset"
            },
            {
                "Name": "usb_africamultinationalenvironmentoptionset"
            },
            {
                "Name": "usb_usb_developsaeconomyoptionset"
            },
            {
                "Name": "usb_africaemergingmarketoptionset"
            },
            {
                "Name": "usb_africastrategicdecisionoptionset"
            },
            {
                "Name": "usb_insightpoliciesprogrammesoptionset"
            },
            {
                "Name": "usb_businessleaderoptionset"
            }]
        },
        {
            "Label": "How important were the following when you decided to do your MPhil/PGDip Futures Studies?",
            "Name" : "tab_2_section_7",
            "Visible": true,
            "Controls": [{
                "Name": "usb_professionalfuturistoptionset"
            },
            {
                "Name": "usb_benefitfromuniquetypeoptionset"
            },
            {
                "Name": "usb_complexorgenvironmentoptionset"
            },
            {
                "Name": "usb_sagrowthcontributionoptionset"
            },
            {
                "Name": "usb_theoryoffututestudiesoptionset"
            },
            {
                "Name": "usb_developefuturestudiesoptionset"
            },
            {
                "Name": "usb_alignforfuturistoptionset"
            },
            {
                "Name": "usb_sustainablefutureoptionset"
            },
            {
                "Name": "usb_scenariotoolsoptionset"
            }]
        },
        {
            "Label": "How important were the following when you decided to do your MPhil in Management Coaching?",
            "Name" : "tab_2_section_8",
            "Visible": true,
            "Controls": [{
                "Name": "usb_selfdevelopmentoptionset"
            },
            {
                "Name": "usb_enhancecoachingskillsoptionset"
            },
            {
                "Name": "usb_theoreticalcoachingoptionset"
            },
            {
                "Name": "usb_owncoachingpracticeoptionset"
            },
            {
                "Name": "usb_ownframeworkmodeloptionset"
            },
            {
                "Name": "usb_developmentofothersoptionset"
            },
            {
                "Name": "usb_professionalcoachescommunityoptionset"
            }]
        },
        {
            "Label": "How important were the following when you decided to do your PGD Business Management and Administration?",
            "Name" : "tab_2_section_9",
            "Visible": true,
            "Controls": [{
                "Name": "usb_entrepreneurialknowledgeoptionset"
            },
            {
                "Name": "usb_innovativeoptionset"
            },
            {
                "Name": "usb_digitalskillsoptionset"
            },
            {
                "Name": "usb_progresstombaoptionset"
            }]
        },
        {
            "Label": "How important were the following when you decided to do your PGD in Leadership?",
            "Name" : "tab_2_section_10",
            "Visible": true,
            "Controls": [{
                "Name": "usb_authenticleadershipoptioset"
            },
            {
                "Name": "usb_makeadifferenceoptionset"
            },
            {
                "Name": "usb_howtobealeaderoptionset"
            },
            {
                "Name": "usb_sustainabletransoptionset"
            }]
        },
        {
            "Label": "How important were the following when you decided to do your PGD in Project Management?",
            "Name" : "tab_2_section_11",
            "Visible": true,
            "Controls": [{
                "Name": "usb_aquirescareskillsoptionset"
            },
            {
                "Name": "usb_manageprojectsoptionset"
            },
            {
                "Name": "usb_pmskillsoptionset"
            }]
        },
        {
            "Label": "How important are the following to you?",
            "Name" : "tab_2_section_4",
            "Visible": true,
            "Controls": [{
                "Name": "usb_lecturerqaulityoptionset"
            },
            {
                "Name": "usb_electronicstudymaterialoptionset"
            },
            {
                "Name": "usb_studymaterialoptionset"
            },
            {
                "Name": "usb_researchsupervisionoptionset"
            },
            {
                "Name": "usb_ictsupportoptionset"
            },
            {
                "Name": "usb_availablecareeroptionset"
            },
            {
                "Name": "usb_libraryservicesoptionset"
            },
            {
                "Name": "usb_academicsupportoptionset"
            },
            {
                "Name": "usb_mentorsoptionset"
            },
            {
                "Name": "usb_campussecurityoptionset"
            },
            {
                "Name": "usb_cateringservicesoptionset"
            },
            {
                "Name": "usb_adminsupportoptionset"
            },
            {
                "Name": "usb_offcampussupportoptionset"
            },
            {
                "Name": "usb_diverseclassgroupoptionset"
            },
            {
                "Name": "usb_diversefaultycompositeoptionset"
            },
            {
                "Name": "usb_blendedlearningoptionset"
            },
            {
                "Name": "usb_classroombasedoptionset"
            }]
        },
        {
            "Label": "What made you decide to apply at the USB?",
            "Name" : "tab_2_section_5",
            "Visible": true,
            "Controls": [{
                "Name": "usb_tripleaccreditationtwooptions"
            },
            {
                "Name": "usb_valueformoneytwooptions"
            },
            {
                "Name": "usb_recommendedbyalumnitwooptions"
            },
            {
                "Name": "usb_closetohometwooptions"
            },
            {
                "Name": "usb_excellentreputationtwooptions"
            },
            {
                "Name": "usb_recommendedbyemployertwooptions"
            },
            {
                "Name": "usb_highqualitystudentstwooptions"
            },
            {
                "Name": "usb_programmeformattwooptions"
            },
            {
                "Name": "usb_usbinternationaltwooptions"
            },
            {
                "Name": "usb_usbafricafocustwooptions"
            },
            {
                "Name": "usb_usbgeneralleadershipfocustwooptions"
            },
            {
                "Name": "usb_usbcollaborativelearningtwooptions"
            },
            {
                "Name": "usb_iamamatietwooptions"
            },
            {
                "Name": "usb_internationallecturersinclasstwooptions"
            },
            {
                "Name": "usb_internationalstudentstwooptions"
            },
            {
                "Name": "usb_deliverymodetwooptions"
            }]
        }]
    }]
};

var Xrm = new _xrm(pageData);