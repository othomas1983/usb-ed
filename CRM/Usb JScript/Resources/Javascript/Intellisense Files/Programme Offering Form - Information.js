/// <reference path="Xrm.js" />

var EntityLogicalName = "usb_programmeoffering";
var Form_924ab9c7_aa2c_4103_826a_30b411e10471_Properties = {
    ownerid: "ownerid",
    statuscode: "statuscode",
    usb_name: "usb_name",
    usb_offeringenddate: "usb_offeringenddate",
    usb_offeringstartdate: "usb_offeringstartdate",
    usb_offeringtypeoptionset: "usb_offeringtypeoptionset",
    usb_programmeformatlookup: "usb_programmeformatlookup",
    usb_programmelookup: "usb_programmelookup",
    usb_programmeofferingcode: "usb_programmeofferingcode",
    usb_yearoffered: "usb_yearoffered"
};

var Form_924ab9c7_aa2c_4103_826a_30b411e10471_Controls = {
    header_statuscode: "header_statuscode",
    ownerid: "ownerid",
    SAR: "SAR",
    usb_name: "usb_name",
    usb_offeringenddate: "usb_offeringenddate",
    usb_offeringstartdate: "usb_offeringstartdate",
    usb_offeringtypeoptionset: "usb_offeringtypeoptionset",
    usb_programmeformatlookup: "usb_programmeformatlookup",
    usb_programmelookup: "usb_programmelookup",
    usb_programmeofferingcode: "usb_programmeofferingcode",
    usb_yearoffered: "usb_yearoffered"
};

var pageData = {
    "Event": "none",
    "SaveMode": 1,
    "EventSource": null,
    "AuthenticationHeader": "",
    "CurrentTheme": "Default",
    "OrgLcid": 1033,
    "OrgUniqueName": "",
    "QueryStringParameters": {
        "_gridType": "10022",
        "etc": "10022",
        "id": "",
        "pagemode": "iframe",
        "preloadcache": "1344548892170",
        "rskey": "141637534"
    },
    "ServerUrl": "",
    "UserId": "",
    "UserLcid": 1033,
    "UserRoles": [""],
    "isOutlookClient": false,
    "isOutlookOnline": true,
    "DataXml": "",
    "EntityName": "usb_programmeoffering",
    "Id": "",
    "IsDirty": false,
    "CurrentControl": "",
    "CurrentForm": null,
    "Forms": [],
    "FormType": 2,
    "ViewPortHeight": 558,
    "ViewPortWidth": 1231,
    "Attributes": [{
        "Name": "ownerid",
        "Value": [{
            "entityType": "Unknown",
            "id": "{27ACAE0A-5D79-E111-BBD3-2657AEB3167B}",
            "name": "Temp"
        }],
        "Type": "lookup",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": false,
            "canCreate": true
        },
        "Controls": [{
            "Name": "ownerid"
        }]
    },
    {
        "Name": "statuscode",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Offering Proposed",
            "value": 1
        },
        {
            "text": "Offering Launched",
            "value": 864480000
        },
        {
            "text": "Offering Closed",
            "value": 864480003
        },
        {
            "text": "Inactive",
            "value": 2
        }],
        "SelectedOption": {
            "option": "Offering Proposed",
            "value": 1
        },
        "Text": "Offering Proposed",
        "Controls": [{
            "Name": "header_statuscode"
        }]
    },
    {
        "Name": "usb_name",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "usb_name"
        }]
    },
    {
        "Name": "usb_offeringenddate",
        "Value": null,
        "Type": "datetime",
        "Format": "date",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Controls": [{
            "Name": "usb_offeringenddate"
        }]
    },
    {
        "Name": "usb_offeringstartdate",
        "Value": null,
        "Type": "datetime",
        "Format": "date",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Controls": [{
            "Name": "usb_offeringstartdate"
        }]
    },
    {
        "Name": "usb_offeringtypeoptionset",
        "Value": null,
        "Type": "optionset",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "InitialValue": 930650000,
        "Options": [{
            "text": "Attendance",
            "value": 0
        },
        {
            "text": "Blended",
            "value": 1
        },
        {
            "text": "Online",
            "value": 2
        }],
        "SelectedOption": {
            "option": "Attendance",
            "value": 0
        },
        "Text": "Attendance",
        "Controls": [{
            "Name": "usb_offeringtypeoptionset"
        }]
    },
    {
        "Name": "usb_programmeformatlookup",
        "Value": [{
            "entityType": "Unknown",
            "id": "{27ACAE0A-5D79-E111-BBD3-2657AEB3167B}",
            "name": "Temp"
        }],
        "Type": "lookup",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Controls": [{
            "Name": "usb_programmeformatlookup"
        }]
    },
    {
        "Name": "usb_programmelookup",
        "Value": [{
            "entityType": "Unknown",
            "id": "{27ACAE0A-5D79-E111-BBD3-2657AEB3167B}",
            "name": "Temp"
        }],
        "Type": "lookup",
        "Format": null,
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "Controls": [{
            "Name": "usb_programmelookup"
        }]
    },
    {
        "Name": "usb_programmeofferingcode",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 100,
        "Controls": [{
            "Name": "usb_programmeofferingcode"
        }]
    },
    {
        "Name": "usb_yearoffered",
        "Value": "",
        "Type": "string",
        "Format": "text",
        "IsDirty": false,
        "RequiredLevel": "none",
        "SubmitMode": "dirty",
        "UserPrivilege": {
            "canRead": true,
            "canUpdate": true,
            "canCreate": true
        },
        "MaxLength": 10,
        "Controls": [{
            "Name": "usb_yearoffered"
        }]
    }],
    "AttributesLength": 10,
    "Controls": [{
        "Name": "header_statuscode",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Status Reason",
        "Attribute": "statuscode"
    },
    {
        "Name": "ownerid",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Owner",
        "Attribute": "ownerid"
    },
    {
        "Name": "usb_name",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Descriptive Name",
        "Attribute": "usb_name"
    },
    {
        "Name": "usb_offeringenddate",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Offering End Date",
        "Attribute": "usb_offeringenddate"
    },
    {
        "Name": "usb_offeringstartdate",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Offering Start Date",
        "Attribute": "usb_offeringstartdate"
    },
    {
        "Name": "usb_offeringtypeoptionset",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Offering Type",
        "Attribute": "usb_offeringtypeoptionset"
    },
    {
        "Name": "usb_programmeformatlookup",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Programme Format",
        "Attribute": "usb_programmeformatlookup"
    },
    {
        "Name": "usb_programmelookup",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Programme",
        "Attribute": "usb_programmelookup"
    },
    {
        "Name": "usb_programmeofferingcode",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Programme Offering Code",
        "Attribute": "usb_programmeofferingcode"
    },
    {
        "Name": "usb_yearoffered",
        "Type": "standard",
        "Disabled": false,
        "Visible": true,
        "Label": "Year Offered",
        "Attribute": "usb_yearoffered"
    }],
    "ControlsLength": 10,
    "Navigation": [{
        "Id": "navConnections",
        "Key": "navConnections",
        "Label": "",
        "Visible": true
    }],
    "Tabs": [{
        "Label": "General",
        "Name": "{18f6044a-5039-4567-97de-f7dab03920e5}",
        "DisplayState": "expanded",
        "Visible": true,
        "Sections": [{
            "Label": "General",
            "Name": "{5cc440f2-66bd-4382-92ed-a69bbc55f553}",
            "Visible": true,
            "Controls": [{
                "Name": "usb_name"
            },
            {
                "Name": "ownerid"
            },
            {
                "Name": "usb_programmelookup"
            },
            {
                "Name": "usb_programmeformatlookup"
            },
            {
                "Name": "usb_offeringstartdate"
            },
            {
                "Name": "usb_offeringenddate"
            },
            {
                "Name": "usb_offeringtypeoptionset"
            },
            {
                "Name": "usb_programmeofferingcode"
            },
            {
                "Name": "usb_yearoffered"
            }]
        },
        {
            "Label": "Student Academic Record",
            "Name": "{18f6044a-5039-4567-97de-f7dab03920e5}_section_2",
            "Visible": true,
            "Controls": [{
                "Name": "SAR"
            }]
        }]
    }]
};

var Xrm = new _xrm(pageData);