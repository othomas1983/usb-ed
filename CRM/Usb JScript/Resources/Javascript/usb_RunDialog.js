﻿function callDialog() {

    var dialogId = 'D7C5A75B-D482-4C69-8070-71B7F6491355';
    var entityName = Xrm.Page.data.entity.getEntityName();
    var entityId = Xrm.Page.data.entity.getId();

    Process.callDialog(
        dialogId,
        entityName,
        entityId,
        
        function () {
            //refresh form to execute the OnLoad Function
            refreshForm();            
        });
}