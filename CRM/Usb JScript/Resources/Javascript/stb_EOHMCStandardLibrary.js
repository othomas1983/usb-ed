//Global Variables
//
//
//
//CRM Server Dynamic URL
var baseUrl = Xrm.Page.context.getClientUrl();
var oDataPath = baseUrl + "/XRMServices/2011/OrganizationData.svc";

//USB-ED Specific Url's-------------------------------------------------------------------------------------------------------------
//Application form Dynamic URL
var applicationFormBaseURL = "http://" + window.location.hostname + ":84/UsbEdRedirect/ApplicationFormInsideCrm.aspx?";

//Request Certificate Dynamic URL
var requestCertificateBaseURL = "http://" + window.location.hostname + ":84/UsbEdRedirect/CertificatePrinting.aspx?";
//End USB-ED Specific Url's---------------------------------------------------------------------------------------------------------

//USB Specific Url's----------------------------------------------------------------------------------------------------------------
//Application form Dynamic URL
var usbApplicationFormBaseURL = "http://" + window.location.hostname + ":85/UsbRedirect/ApplicationForm.aspx/";
//End USB Specific Url's------------------------------------------------------------------------------------------------------------
//
//
//
//End Global Variables

//Standard Libraries
//
//
//
//Query Record through Odata--------------------------------------------------------------------------------------------------------
function LibGetData(oDataQuery) {
    var ReturnData;
    jQuery.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        url: oDataQuery,
        async: false,
        beforeSend: function (XMLHttpRequest) {
            XMLHttpRequest.setRequestHeader("Accept", "application/json");
        },
        success: function (data, textStatus, XmlHttpRequest) {
            if (data && data.d != null) {
                ReturnData = data;
            }
        },
        error: function (XmlHttpRequest, textStatus, errorThrown) {
            alert("Error :  has occurred during retrieval of Data with error: " + XmlHttpRequest.responseText);
        }
    });
    return ReturnData;
}
//END-------------------------------------------------------------------------------------------------------------------------------

//Create New Record through Odata---------------------------------------------------------------------------------------------------
function LibCreateNewRecord(entityObject, odataSetName) {
    var jsonEntity = window.JSON.stringify(entityObject);
    //Synchronous AJAX function to Update a CRM record using OData
    jQuery.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        data: jsonEntity,
        url: oDataPath + "/" + odataSetName,
        async: false,
        beforeSend: function (XMLHttpRequest) {
            XMLHttpRequest.setRequestHeader("Accept", "application/json");
        },
        success: function (data, textStatus, XmlHttpRequest) {
            if (data && data.d != null) {
                ReturnData = data;
            }
        },
        error: function (XmlHttpRequest, textStatus, errorThrown) {
            if (XmlHttpRequest && XmlHttpRequest.responseText) {
                alert("Error while creating " + odataSetName + " ; Error � " + XmlHttpRequest.responseText);
            }
        }
    });
    return ReturnData;
}
//END-------------------------------------------------------------------------------------------------------------------------------

//Update Record through Odata-------------------------------------------------------------------------------------------------------
function LibUpdateRecord(entityObject, odataSetName) {
    var jsonEntity = window.JSON.stringify(entityObject);
    //Synchronous AJAX function to Update a CRM record using OData
    jQuery.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        data: jsonEntity,
        url: oDataPath + "/" + odataSetName,
        async: false,
        beforeSend: function (XMLHttpRequest) {
            //Need to complete this
        },
        success: function (data, textStatus, XmlHttpRequest) {
            if (data && data.d != null) {
                ReturnData = data;
            }
        },
        error: function (XmlHttpRequest, textStatus, errorThrown) {
            if (XmlHttpRequest && XmlHttpRequest.responseText) {
                alert("Error while updating " + odataSetName + " ; Error � " + XmlHttpRequest.responseText);
            }
        }
    });
    return ReturnData;
}
//END-------------------------------------------------------------------------------------------------------------------------------

//Submit value to Field-------------------------------------------------------------------------------------------------------------
function LibSubmitToField(fieldName, Value) {
    Xrm.Page.getAttribute(fieldName).setSubmitMode("always");
    Xrm.Page.getAttribute(fieldName).setValue(Value);
}
//END-------------------------------------------------------------------------------------------------------------------------------

//Submit value to Lookup------------------------------------------------------------------------------------------------------------
function LibSubmitToLookup(fieldName, Value) {
    Xrm.Page.getAttribute(fieldName).setSubmitMode("always");
    var name = Value[0].name;
    var id = Value[0].id;
    var entityType = Value[0].entityType;
    Xrm.Page.getAttribute(fieldName).setValue([{
        id: id,
        name: name,
        entityType: entityType
    }]);
}
//END-------------------------------------------------------------------------------------------------------------------------------

//Set Field Disabled----------------------------------------------------------------------------------------------------------------
function LibSetFieldDisabled(fieldName) {
    Xrm.Page.ui.controls.get(fieldName).setDisabled(true);
}
//END-------------------------------------------------------------------------------------------------------------------------------

//Set Field Enabled----------------------------------------------------------------------------------------------------------------
function LibSetFieldEnabled(fieldName) {
    Xrm.Page.ui.controls.get(fieldName).setDisabled(false);
}
//END-------------------------------------------------------------------------------------------------------------------------------

//Set Field Focus-------------------------------------------------------------------------------------------------------------------
function LibSetFieldFocus(fieldName) {
    Xrm.Page.ui.controls.get(fieldName).setFocus();
}
//END-------------------------------------------------------------------------------------------------------------------------------

//Set Field Mandatory Requirement---------------------------------------------------------------------------------------------------
function LibSetFieldRequirementLevel(fieldName, requirementLevel) {
    //acceptable values none, required, recommended
    Xrm.Page.getAttribute(fieldName).setRequiredLevel(requirementLevel);
}
//END-------------------------------------------------------------------------------------------------------------------------------

//Pad values as 00------------------------------------------------------------------------------------------------------------------
function LibStrPad00(stringToPad) {
    stringToPad = stringToPad + '';
    if (stringToPad.length === 1) stringToPad = '0' + stringToPad;
    return stringToPad;
}
//END-------------------------------------------------------------------------------------------------------------------------------

//Add Days to a date
function LibAddDays(date, offsetDays) {
    var result = new Date(date);
    result.setDate(date.getDate() + offsetDays);
    return result;
}
//END-------------------------------------------------------------------------------------------------------------------------------

//Function to create a Web Token in Usb CRM
function LibCreateWebToken(guidRecord) {
    var tokenGuid;
    //Add Webtoken Object for creation of Record
    var WebToken = new Object();
    WebToken.usb_name = guidRecord;
    //Create Web Token
    var ReturnData = LibCreateNewRecord(WebToken, "usb_webtokenSet");
    if (ReturnData && ReturnData.d != null) {
        tokenGuid = ReturnData.d.usb_webtokenId;
    }
    return tokenGuid;
}
//END-------------------------------------------------------------------------------------------------------------------------------

//Function to create a Web Token in Usb CRM
function LibCreateUsbWebToken(guidRecord) {
    var tokenGuid;
    //Add Webtoken Object for creation of Record
    var WebToken = new Object();
    WebToken.usb_name = guidRecord;
    //Create Web Token
    var ReturnData = LibCreateNewRecord(WebToken, "usb_webtokenSet");
    if (ReturnData && ReturnData.d != null) {
        tokenGuid = ReturnData.d.usb_webtokenId;
    }
    return tokenGuid;
}
//END-------------------------------------------------------------------------------------------------------------------------------

//Function to refresh Record
function refreshForm() {
    Xrm.Utility.openEntityForm(Xrm.Page.data.entity.getEntityName(),
    Xrm.Page.data.entity.getId());
}
//END------------------------------------------------------------------------------------------------------------------------------- 
//
//
//
//End Standard Libraries
