﻿/// <reference path="stb_EOHMCStandardLibrary.js" />


//On Change Functions
//
//
//
//Populate City, Suburb and Postal Code from Afigis Field (Requires Execution Context)----------------------------------------------
function UpdateAddress(executionContext) {
    debugger;
    //Excecution context must be switched on for this to work
    var FieldName = executionContext.getEventSource().getName();
    if (FieldName == null) {
        return;
    }
    //Update Address based on field changed
    strAfrigisLookup = Xrm.Page.getAttribute(FieldName).getValue();
    if (strAfrigisLookup == null) {
        return;
    }
    else {
        guidAfrigisGuid = strAfrigisLookup[0].id;
        //Query Information on Afrigis Entity
        var ODataQuery = oDataPath + "/usb_afrigisSet?$select=usb_CityTown,usb_Suburb,usb_PostalCode&$filter=usb_afrigisId eq guid'" + guidAfrigisGuid + "'";
        var ReturnData = LibGetData(ODataQuery);
        if (ReturnData && ReturnData.d != null) {
            if (ReturnData.d.results.length > 0) {
                //Physical Address
                if (FieldName == "usb_afrigislookup") {
                    LibSubmitToField('address1_city', ReturnData.d.results[0].usb_CityTown);
                    LibSubmitToField('usb_address1_suburb', ReturnData.d.results[0].usb_Suburb);
                    LibSubmitToField('address1_postalcode', ReturnData.d.results[0].usb_PostalCode);
                }
                //Postal Address
                if (FieldName == "usb_afrigispostallookup") {
                    LibSubmitToField('address2_city', ReturnData.d.results[0].usb_CityTown);
                    LibSubmitToField('usb_address2_suburb', ReturnData.d.results[0].usb_Suburb);
                    LibSubmitToField('address2_postalcode', ReturnData.d.results[0].usb_PostalCode);
                }
                //Billing Address 
                if (FieldName == "usb_afrigisbilltolookup") {
                    LibSubmitToField('usb_citybillto', ReturnData.d.results[0].usb_CityTown);
                    LibSubmitToField('usb_suburbbillto', ReturnData.d.results[0].usb_Suburb);
                    LibSubmitToField('usb_postalcodebillto', ReturnData.d.results[0].usb_PostalCode);
                }
                //Work Address 
                if (FieldName == "usb_afrigisworklookup") {
                    LibSubmitToField('usb_citywork', ReturnData.d.results[0].usb_CityTown);
                    LibSubmitToField('usb_suburbwork', ReturnData.d.results[0].usb_Suburb);
                    LibSubmitToField('usb_postalcodework', ReturnData.d.results[0].usb_PostalCode);
                }
            }
        }
    }
}
//END-------------------------------------------------------------------------------------------------------------------------------
//
//
//
//End On Change Functions
