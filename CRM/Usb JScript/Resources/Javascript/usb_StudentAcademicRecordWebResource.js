﻿/// <reference path="stb_EOHMCStandardLibrary.js" />
/// <reference path="Intellisense Files/Student Academic Record Form - Information.js" />

//Student Lifecycle------------------------------------------------------------------------------------------------------------------

//Status Reasons
var statusApplicant = 1;
var statusApplicationCompleted = 864480000;
var statusInterviewRound1 = 864480001;
var statusSelectionCommittee = 864480002;
var statusInterviewRound2 = 864480003;
var statusCancelled = 864480004;
var statusNotAdmitted = 864480005;
var statusWaitingList = 864480006;
var statusRequestAdmitted = 864480015;
var statusAdmitted = 864480007;
var statusFailedAdmitted = 864480016;
var statusPostponed = 864480008;
var statusRequestNotAccepted = 864480017;
var statusNotAccepted = 864480009;
var statusFailedNotAccepted = 864480018;
var statusRequestAccepted = 864480019;
var statusAccepted = 864480010;
var statusFailedAccepted = 864480020;
var statusRegistered = 864480011;
var statusUnsuccessful = 864480012;
var statusDiscontinued = 864480013;
var statusGraduated = 864480014;
var statusDeactivated = 2;

//On Load Functions
//
//
//
//On Load Function Group
function OnLoad() {
    debugger;

    var guidRecord = Xrm.Page.data.entity.getId().substring(1, 37);

    //get the status reason + boolean value of change 
    var intStatusValue = Xrm.Page.getAttribute("statuscode").getValue();
    var boolChangeURL = Xrm.Page.getAttribute("usb_updateurltwooptions").getInitialValue();

    //only run if Applicant && UpdateURL true or Not Null
    if (intStatusValue == statusApplicant && boolChangeURL == 1) {
        //Get SAR Guid

        var guidRecord = Xrm.Page.data.entity.getId().substring(1, 37);

        //Get the Degree Name
        var offeringLookup = Xrm.Page.getAttribute("usb_programmeofferinglookup").getValue();
        if (offeringLookup != null) {
            offeringGuid = offeringLookup[0].id.substring(1, 37);
        }
        var oDataQuery = oDataPath + "/usb_programmeofferingSet(guid'" + offeringGuid + "')?$select=usb_programmeoffering_ProgrammeLookup/usb_DegreeLookup&$expand=usb_programmeoffering_ProgrammeLookup";
        var returnData = LibGetData(oDataQuery);
        if (returnData && returnData.d != null) {
            var degreeName = returnData.d.usb_programmeoffering_ProgrammeLookup.usb_DegreeLookup.Name;

            // eg.http://applications.usb.ac.za/MBA/?id=1caaa5fd-025b-e611-80d8-005056b8008e
            var URL = "http://applications.usb.ac.za/" + degreeName + "/?id=" + guidRecord;
            LibSubmitToField('usb_applicationformurl', URL);
            LibSubmitToField('usb_updateurltwooptions', false);
            Xrm.Page.data.entity.save();
        }
        else {
            return;
        }
    }
    else {
        
        //Get Contact GUID
        var contactLookup = Xrm.Page.getAttribute("usb_contactlookup").getValue();
        if (contactLookup != null) {
            contactGUID = contactLookup[0].id.substring(1, 37);
        }

        //var oDataQuery = oDataPath + "/ContactSet(guid'" + contactGUID + "')?$select=usb_studentacademicrecord_ContactLookup/usb_GenderLookup,usb_studentacademicrecord_ContactLookup/usb_MaritalStatusLookup&$expand=usb_studentacademicrecord_ContactLookup";
        var oDataQuery = oDataPath + "/ContactSet?$select=usb_GenderLookup,usb_MaritalStatusLookup,usb_studentacademicrecord_ContactLookup/usb_ContactLookup&$expand=usb_studentacademicrecord_ContactLookup&$filter=ContactId eq guid'" + contactGUID + "'";

        var returnData = LibGetData(oDataQuery);
        
        //check if married and female      
        var Female = returnData.d.results[0].usb_GenderLookup.Name;
        var Married = returnData.d.results[0].usb_MaritalStatusLookup.Name;
       

        if (Female == 'Female' && Married == 'Married') {
            Xrm.Page.getAttribute('usb_marriagecertificatetwooptions').controls.get(0).setVisible(true);
            Xrm.Page.data.entity.save();
        }
        else {
            return;
        }
    }
}

//END-------------------------------------------------------------------------------------------------------------------------------
//
//
//
//End On Load Functions

//Ribbon Controlled Functions
//
//
//

//Function to open the Inside CRM Application Form to Maintain a SAR's Application--------------------------------------------------
function MaintainSAR() {
    debugger;
    if (confirm('Record will be saved and closed to complete this operation')) {
        //Get Record Guid
        var guidRecord = Xrm.Page.data.entity.getId().substring(1, 37);

        //Get the Degree Name for the Application Form redirect
        var offeringLookup = Xrm.Page.getAttribute("usb_programmeofferinglookup").getValue();
        if (offeringLookup != null) {
            offeringGuid = offeringLookup[0].id.substring(1, 37);
        }
        var oDataQuery = oDataPath + "/usb_programmeofferingSet(guid'" + offeringGuid + "')?$select=usb_programmeoffering_ProgrammeLookup/usb_DegreeLookup&$expand=usb_programmeoffering_ProgrammeLookup";
        var returnData = LibGetData(oDataQuery);
        if (returnData && returnData.d != null) {
            var degreeName = returnData.d.usb_programmeoffering_ProgrammeLookup.usb_DegreeLookup.Name;
            var tokenGuid = LibCreateUsbWebToken(guidRecord);
            //Close current window and open external URL
            Xrm.Page.data.entity.save("saveandclose");
            var URL = usbApplicationFormBaseURL + degreeName + "?id=" + guidRecord + "&webTokenId=" + tokenGuid; //Add the dynamic variable for MBA part
            window.open(URL, "_blank", "height=" + screen.height + ",width=" + screen.width + ",menubar=yes,resizable=yes,toolbar=yes,location=yes,scrollbars=yes,status=yes,titlebar=yes");
        }
    }
    else {
        return;
    }
}
//END-------------------------------------------------------------------------------------------------------------------------------

//Function to post Interview Round 1 Status of an enrollment------------------------------------------------------------------------
function InterviewRound1() {
    debugger;
    //Set Status Reason to Interview Round 1
    var intStatusValue = statusInterviewRound1;
    LibSubmitToField('statuscode', intStatusValue);
    Mscrm.Utilities.reloadPage();
}
//END-------------------------------------------------------------------------------------------------------------------------------

//Function to post Selection Committee Status of an enrollment----------------------------------------------------------------------
function SelectionCommittee() {
    debugger;
    //If SR = Interview Round 1 then Interview Round 1 Completed must = "Yes"
    //If SR = Interview Round 2 then Interview Round 2 Completed must = "Yes"
    var intStatusValue = Xrm.Page.getAttribute("statuscode").getValue();
    //Application Completed
    if (intStatusValue == statusInterviewRound1) {
        var boolInterviewRound1Completed = Xrm.Page.getAttribute("usb_round1interviewcompletedtwooptions").getValue();
        if (boolInterviewRound1Completed == 1) {
            //Set Status Reason to Selection Committe
            var intStatusValue = statusSelectionCommittee;
            LibSubmitToField('statuscode', intStatusValue);
            Mscrm.Utilities.reloadPage();
        }
        else {
            alert("The Interview must be marked as Complete to Continue");
        }
    }
    else if (intStatusValue == statusInterviewRound2) {
        var boolInterviewRound2Completed = Xrm.Page.getAttribute("usb_round2interviewcompletedtwooptions").getValue();
        if (boolInterviewRound2Completed == 1) {
            //Set Status Reason to Selection Committe
            var intStatusValue = statusSelectionCommittee;
            LibSubmitToField('statuscode', intStatusValue);
            Mscrm.Utilities.reloadPage();
        }
        else {
            alert("The Interview must be marked as Complete to Continue");
        }
    }
    else {
        //Set Status Reason to Selection Committe
        var intStatusValue = statusSelectionCommittee;
        LibSubmitToField('statuscode', intStatusValue);
        Mscrm.Utilities.reloadPage();
    }
}
//END-------------------------------------------------------------------------------------------------------------------------------

//Function to post Interview Round 2 Status of an enrollment------------------------------------------------------------------------
function InterviewRound2() {
    debugger;
    //Set Status Reason to Interview Round 2
    var intStatusValue = statusInterviewRound2;
    LibSubmitToField('statuscode', intStatusValue);
    Mscrm.Utilities.reloadPage();
}
//END-------------------------------------------------------------------------------------------------------------------------------

//Function to post Cancelled Status of an enrollment--------------------------------------------------------------------------------
function Cancelled() {
    debugger;
    //Set Status Reason to Cancelled
    var intStatusValue = statusCancelled;
    CheckReasonForStoppingValid(intStatusValue);
}
//END-------------------------------------------------------------------------------------------------------------------------------

//Function to post Admitted Status of an enrollment--------------------------------------------------------------------------------
function Admitted() {
    debugger;
    //Set Status Reason to Request  :Admitted
    var intStatusValue = statusRequestAdmitted;
    LibSubmitToField('statuscode', intStatusValue);
    Mscrm.Utilities.reloadPage();
}
//END-------------------------------------------------------------------------------------------------------------------------------

//Function to post Not Admitted Status of an enrollment-----------------------------------------------------------------------------
function NotAdmitted() {
    debugger;
    //Set Status Reason to Not Admitted
    var intStatusValue = statusNotAdmitted;
    LibSubmitToField('statuscode', intStatusValue);
    Mscrm.Utilities.reloadPage();
}
//END-------------------------------------------------------------------------------------------------------------------------------

//Function to post Waiting List Status of an enrollment-----------------------------------------------------------------------------
function WaitingList() {
    debugger;
    //Set Status Reason to Waiting List
    var intStatusValue = statusWaitingList;
    LibSubmitToField('statuscode', intStatusValue);
    Mscrm.Utilities.reloadPage();
}
//END-------------------------------------------------------------------------------------------------------------------------------

//Function to post Not Accepted Status of an enrollment-----------------------------------------------------------------------------
function NotAccepted() {
    debugger;
    //Set Status Reason to Request: Not Accepted
    var intStatusValue = statusRequestNotAccepted;
    LibSubmitToField('statuscode', intStatusValue);
    Mscrm.Utilities.reloadPage();
}
//END-------------------------------------------------------------------------------------------------------------------------------

//Function to post Accepted Status of an enrollment---------------------------------------------------------------------------------
function Accepted() {
    debugger;
    //Acceptance Form must = "Yes" and Deposit Ref must not = null
    var intAcceptanceForm = Xrm.Page.getAttribute("usb_acceptanceformtwooptions").getValue();
    var intDepositRef = Xrm.Page.getAttribute("usb_depositrefno").getValue();
    if (intAcceptanceForm == 1 && intDepositRef != null) {
        //Set Status Reason to Request: Accepted
        var intStatusValue = statusRequestAccepted;
        LibSubmitToField('statuscode', intStatusValue);
        Mscrm.Utilities.reloadPage();
    }
    else {
        alert("Please mark Acceptance Form as Received and Input a Deposit Ref No to Continue");
    }
}
//END-------------------------------------------------------------------------------------------------------------------------------

//Function to post Postponed Status of an enrollment--------------------------------------------------------------------------------
function Postponed() {
    debugger;
    //Set Status Reason to Postponed
    var intStatusValue = statusPostponed;
    CheckReasonForStoppingValid(intStatusValue);
}
//END-------------------------------------------------------------------------------------------------------------------------------

//Function for the callback
function SwopOut() {
    debugger;
}

//Functions to control availability of buttons in the ribbon
function EnableMaintainSARButton() {
    debugger;
    //Always enabled
    return true;
}
//END-------------------------------------------------------------------------------------------------------------------------------

function EnableInterviewRound1Button() {
    debugger;
    var intStatusValue = Xrm.Page.getAttribute("statuscode").getValue();
    //Application Completed
    if ( intStatusValue == statusSelectionCommittee) {

        //Application must be submitted and Interview Round 1 Complete must not = Yes
        var boolApplicationSubmitted = Xrm.Page.getAttribute("usb_applicationsubmittedtwooptions").getValue();
        var boolInterviewRound1Complete = Xrm.Page.getAttribute("usb_round1interviewcompletedtwooptions").getValue();
        if (boolApplicationSubmitted == 1 && boolInterviewRound1Complete != 1) {
            return true;
        }
    }
}
//END-------------------------------------------------------------------------------------------------------------------------------

function EnableSelectionCommitteeButton() {
    debugger;
    var intStatusValue = Xrm.Page.getAttribute("statuscode").getValue();
    //Application Completed or Interview Round 1 or Interview Round 1
    if (intStatusValue == statusApplicationCompleted  || intStatusValue == statusInterviewRound1 || intStatusValue == statusInterviewRound2  ) {
        return true;
    }
}
//END-------------------------------------------------------------------------------------------------------------------------------

function EnableInterviewRound2Button() {
    debugger;
    var intStatusValue = Xrm.Page.getAttribute("statuscode").getValue();
    //Selection Committee
    if (intStatusValue == statusSelectionCommittee) {
        //Interview Round  Complete must = Yes
        var boolInterviewRound1Complete = Xrm.Page.getAttribute("usb_round1interviewcompletedtwooptions").getValue();
        var boolInterviewRound2Complete = Xrm.Page.getAttribute("usb_round2interviewcompletedtwooptions").getValue();
        if (boolInterviewRound1Complete == 1 && boolInterviewRound2Complete != 1 ) {
            return true;
        }
    }
}
//END-------------------------------------------------------------------------------------------------------------------------------

function EnableCancelledButton() {
    debugger;
    var intStatusValue = Xrm.Page.getAttribute("statuscode").getValue();
    //Applicant or Application Completed or Interview Round 1 or Selection Committe or Interview Round 2
    if (intStatusValue == statusApplicant || intStatusValue == statusApplicationCompleted ||
        intStatusValue == statusInterviewRound1 || intStatusValue == statusSelectionCommittee || intStatusValue == statusInterviewRound2) {
        return true;
    }
}
//END-------------------------------------------------------------------------------------------------------------------------------

function EnableAdmittedButton() {
    debugger;
    var intStatusValue = Xrm.Page.getAttribute("statuscode").getValue();
    //Selection Committee or Waiting List
    if (intStatusValue == statusSelectionCommittee || intStatusValue == statusWaitingList) {
        return true;
    }
}
//END-------------------------------------------------------------------------------------------------------------------------------

function EnableNotAdmittedButton() {
    debugger;
    var intStatusValue = Xrm.Page.getAttribute("statuscode").getValue();
    //Selection Committee
    if (intStatusValue == statusSelectionCommittee) {
        return true;
    }
}
//END-------------------------------------------------------------------------------------------------------------------------------

function EnableWaitingListButton() {
    debugger;
    var intStatusValue = Xrm.Page.getAttribute("statuscode").getValue();
    //Selection Committee
    if (intStatusValue == statusSelectionCommittee) {
        return true;
    }
}
//END-------------------------------------------------------------------------------------------------------------------------------

function EnableNotAcceptedButton() {
    debugger;
    var intStatusValue = Xrm.Page.getAttribute("statuscode").getValue();
    //Waiting List or Admitted
    if (intStatusValue == statusWaitingList || intStatusValue == statusAdmitted) {
        return true;
    }
}
//END-------------------------------------------------------------------------------------------------------------------------------

function EnableAcceptedButton() {
    debugger;
    var intStatusValue = Xrm.Page.getAttribute("statuscode").getValue();
    //Admitted
    if (intStatusValue == statusAdmitted) {
        return true;
    }
}
//END-------------------------------------------------------------------------------------------------------------------------------

function EnablePostponedButton() {
    debugger;
    var intStatusValue = Xrm.Page.getAttribute("statuscode").getValue();
    //Waiting List
    if (intStatusValue == statusWaitingList) {
        return true;
    }
}
//END-------------------------------------------------------------------------------------------------------------------------------

function EnableSwopOutButton() {
    debugger;

    var inStatusValue = Xrm.Page.getAttribute("statuscode").getValue();
    //Postponed
    if (inStatusValue == statusPostponed) {
        return true;
    }

}
//END-------------------------------------------------------------------------------------------------------------------------------
//
//
//
//End Ribbon Controlled Functions



//Common Functions
//
//
//
//Function to check if the Reason for Stopping is populated for specific Status Reasons
//If it has a valid value it will commit the Status Reason Change
function CheckReasonForStoppingValid(intStatusValue) {
    if (Xrm.Page.getAttribute("usb_reasonforstoppingoptionset").getValue() != null) {
        LibSubmitToField('statuscode', intStatusValue);
        Mscrm.Utilities.reloadPage();
    }
    else {
        alert("Please select a Reason for Stopping before you continue");
        LibSetFieldRequirementLevel("usb_reasonforstoppingoptionset", "required");
        LibSetFieldFocus("usb_reasonforstoppingoptionset");
    }
}
//END-------------------------------------------------------------------------------------------------------------------------------
//
//
//End Common Functions