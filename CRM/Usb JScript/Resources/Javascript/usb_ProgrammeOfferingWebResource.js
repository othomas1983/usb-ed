﻿/// <reference path="stb_EOHMCStandardLibrary.js" />
/// <reference path="Intellisense Files/Programme Offering Form - Information.js" />

//Ribbon Controlled Functions
//
//
//
//Function to call Offering Date dialog from a Button-------------------------------------------------------------------------------
function SetOfferingDate() {
    debugger;
    var primaryEntityId = Xrm.Page.data.entity.getId();
    //Close base record to get around background opening
    if (confirm('Record will be saved and closed to complete this operation')) {
        Xrm.Page.data.entity.save("saveandclose");
        //--Need Guid for the Change Offering date Dialog
        URL = baseUrl + "/cs/dialog/rundialog.aspx?DialogId=%7b4CCA459C-C567-4C45-9A28-4C97923F65BF%7d&EntityName=usb_programmeoffering&ObjectId=" + primaryEntityId;
        window.open(URL, "_blank", "height=600px,width=800px");
    }
    else {
        return;
    }
}
//END-------------------------------------------------------------------------------------------------------------------------------
//
//
//
//End Ribbon Controlled Functions


//Ribbon Controlled Functions
//
//
//
//Functions to control availability of buttons in the ribbon
function EnableSetOfferingDateButton() {
    debugger;
    //Never Enabled
    return false;
}
//END-------------------------------------------------------------------------------------------------------------------------------
//
//
//
//End Ribbon Controlled Functions
