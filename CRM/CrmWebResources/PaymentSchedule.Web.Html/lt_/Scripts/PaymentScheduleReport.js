﻿function registerOnloadEvents() {
    
    $('.datepicker').datepicker({
        dateFormat: 'yy/mm/dd'
    });

    $('#ok').click(function () {
        getPaymentScheduleReport();
    });

}

function setDefaultCriteria() {

    var currentDate = new Date();

    var startDate = new Date();
    startDate.setDate(1);
    startDate.setMonth(currentDate.getMonth());
    startDate.setFullYear(currentDate.getFullYear());
    $('#startDate').val($.datepicker.formatDate('yy/mm/dd', startDate));

    var endDate = new Date(currentDate.getFullYear(), currentDate.getMonth() + 1, 0, 23, 59, 59);
    $('#endDate').val($.datepicker.formatDate('yy/mm/dd', endDate));

}

function getServerUrl() {

//    var url = window.parent.Xrm.Page.context.getServerUrl();
//    var firstvalue = '://';
//    var startPosition = url.indexOf(firstvalue);
//    url = url.substring(startPosition + firstvalue.length);
//    var endPosition = url.indexOf(':');
//    url = url.substring(0, endPosition);
    var url = 'bpccrm03'

    return url;

}

function getPaymentScheduleReport() {

    var paymentScheduleReportMessage = {};
    paymentScheduleReportMessage.StartDate = $('#startDate').val();
    paymentScheduleReportMessage.EndDate = $('#endDate').val();

    var messageDto = JSON.stringify(paymentScheduleReportMessage);

    var server = getServerUrl();
    var serviceUrl = 'http://' + server + ':8091/HubService/InvoiceService/restJson/GetPaymentScheduleReport?message=' + messageDto;

    $('#paymentScheduleReportTable').dataTable({

        'bJQueryUI': true,
        'bProcessing': true,
        'sAjaxSource': serviceUrl,
        'sPaginationType': 'full_numbers',
        'bDestroy': true,
        'fnServerData': function (sUrl, aoData, fnCallback, oSettings) {
            oSettings.jqXHR = $.ajax({
                'url': sUrl,
                'data': aoData,
                'success': fnCallback,
                'dataType': 'jsonp',
                'cache': false
            });

        }
    });

}