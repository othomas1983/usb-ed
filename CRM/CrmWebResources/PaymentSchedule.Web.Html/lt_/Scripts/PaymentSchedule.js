﻿function registerOnloadEvents() {

    $('.currency').live('blur', function () {
        $('.currency').toNumber().formatCurrency({
            symbol: 'R '
        });
    });


    $('.required').live('blur', function () {
        validateDialog();
    });

    $('#add').button({
        icons: {
            primary: 'ui-icon-plus'
        }
    });

}
function getServerUrl() {

    var url = window.parent.Xrm.Page.context.getServerUrl();
    var firstvalue = '://';
    var startPosition = url.indexOf(firstvalue);
    url = url.substring(startPosition + firstvalue.length);
    var endPosition = url.indexOf(':');
    url = url.substring(0, endPosition);

    return url;

}
function validateDialog() {
    var valid = true;
    $('.required').each(function () {
        var value = $(this).val();
        if (value == '') {
            valid = false;
        }
    });

    $(".ui-dialog-buttonpane button:contains('Add')").button(valid ? "enable" : "disable");
    $(".ui-dialog-buttonpane button:contains('Update')").button(valid ? "enable" : "disable");
}
function addDialog() {


    $('#add').click(function () {

        $('#deleteForm').hide();
        $('#addEditForm').show();

        $('#date').val('');
        $('#amount').val('');

        $('#dialog').dialog({
            buttons: {
                'Cancel': function () {

                    $('#dialog').dialog('close');
                },
                'Add': function () {
                    createPaymentScheduleItem();

                    $('#dialog').dialog('close');
                }
            },
            modal: true,
            resizable: false,
            title: 'Add Payment Schedule Item',
            width: 380
        });

        $('.datepicker').datepicker({
            minDate: 1,
            dateFormat: 'yy/mm/dd',
            onSelect: function () {
                validateDialog();
            }
        });

    });

}
function updateDialog(paymentScheduleItem) {

    $('#deleteForm').hide();
    $('#addEditForm').show();

    $('#date').val(paymentScheduleItem.Date.replace(/##/g, '/'));
    $('#amount').val(paymentScheduleItem.Amount);

    $('.datepicker').datepicker({
        minDate: 1,
        dateFormat: 'yy/mm/dd',
        onSelect: function () {
            validateDialog();
        }
    });

    var me = this;

    $('#dialog').dialog({
        modal: true,
        title: 'Update Payment Schedule Item',
        resizable: false,
        buttons: {
            'Cancel': function () {
                $('#dialog').dialog('close');
            },
            'Update': function () {
                me.updatePaymentScheduleItem(paymentScheduleItem.Id);

                $('#dialog').dialog('close');
            }
        },
        width: 380
    });
}

function deleteDialog(id) {

    $('#deleteForm').show();
    $('#addEditForm').hide();
    var me = this;

    $('#dialog').dialog({
        modal: true,
        title: 'Delete Payment Schedule Item',
        resizable: false,
        buttons: {
            'Cancel': function () {
                $('#dialog').dialog('close');
            },
            'Delete': function () {
                me.deletePaymentScheduleItem(id);

                $('#dialog').dialog('close');
            }
        },
        width: 380
    });
}

function createPaymentScheduleItem() {

    var paymentSchedule = {};
    var id = window.parent.Xrm.Page.data.entity.getId();
    paymentSchedule.ProgrammeOfferingId = id.substring(1, id.length - 1);
    paymentSchedule.Description = 1;
    paymentSchedule.Date = $('#date').val();
    paymentSchedule.Amount = $('#amount').asNumber();

    var messageDto = JSON.stringify(paymentSchedule);

    var server = getServerUrl();
    var serviceUrl = 'http://' + server + ':8091/HubService/InvoiceService/restJson/CreatePaymentSchedule?message=' + messageDto;

    jQuery.support.cors = true;

    $.ajax({
        url: serviceUrl,
        type: "POST",
        success: function (response, textStatus, jqXHR) {
                        retrievePaymentScheduleTotal();
                        retrievePaymentScheduleItems();
        },
        error: function (jqXHR, textStatus, errorThrown) {
                        retrievePaymentScheduleTotal();
                        retrievePaymentScheduleItems();
        }
    });

}
function retrievePaymentScheduleItems() {

    var id = window.parent.Xrm.Page.data.entity.getId();

    var server = getServerUrl();
    var serviceUrl = 'http://' + server + ':8091/HubService/InvoiceService/restJson/GetPaymentSchedule?message=' + id;

    $('#paymentScheduleTable').dataTable({

        'bJQueryUI': true,
        'bProcessing': true,
        'sAjaxSource': serviceUrl,
        'sPaginationType': 'full_numbers',
        'bDestroy': true,
        'fnServerData': function (sUrl, aoData, fnCallback, oSettings) {
            oSettings.jqXHR = $.ajax({
                'url': sUrl,
                'data': aoData,
                'success': fnCallback,
                'dataType': 'jsonp',
                'cache': false
            });

        },
        'fnDrawCallback': function () {

            $('.edit').bind('click', function () {
                var id = $(this).attr('id');
                id = id.replace('edit', 'hiddenEdit');
                var value = $('#' + id).val();
                var json = jQuery.parseJSON(value);
                updateDialog(json);
            });

            $('.delete').bind('click', function () {
                var id = $(this).attr('id');
                id = id.replace('delete', 'hiddenDelete');
                var value = $('#' + id).val();
                deleteDialog(value);

            });

            $('.edit').button({
                icons: {
                    primary: 'ui-icon-pencil'
                }
            });

            $('.delete').button({
                icons: {
                    primary: 'ui-icon-trash'
                }
            });

        }
    });

}
function retrievePaymentScheduleTotal() {

    var id = window.parent.Xrm.Page.data.entity.getId();

    var server = getServerUrl();
    var serviceUrl = 'http://' + server + ':8091/HubService/InvoiceService/restJson/GetPaymentScheduleTotal?message=' + id;

    $.ajax({
        url: serviceUrl,
        type: 'GET',
        dataType: 'jsonp',
        success: function (result) {
            $('#total').text(result);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert(errorThrown);
        }
    });
}
function updatePaymentScheduleItem(id) {

    var paymentSchedule = {};
    paymentSchedule.Id = id;
    var guid = window.parent.Xrm.Page.data.entity.getId();
    paymentSchedule.ProgrammeOfferingId = guid.substring(1, guid.length - 1);
    paymentSchedule.Description = 1;
    paymentSchedule.Date = $('#date').val();
    paymentSchedule.Amount = $('#amount').asNumber();

    var messageDto = JSON.stringify(paymentSchedule);

    var server = getServerUrl();
    var serviceUrl = 'http://' + server + ':8091/HubService/InvoiceService/restJson/UpdatePaymentSchedule?message=' + messageDto;

    jQuery.support.cors = true;

    $.ajax({
        url: serviceUrl,
        type: "POST",
        success: function (response, textStatus, jqXHR) {
            retrievePaymentScheduleTotal();
            retrievePaymentScheduleItems();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            retrievePaymentScheduleTotal();
            retrievePaymentScheduleItems();
        }
    });
}
function deletePaymentScheduleItem(id) {

    var server = getServerUrl();
    var serviceUrl = 'http://' + server + ':8091/HubService/InvoiceService/restJson/DeletePaymentSchedule?message=' + id;

    jQuery.support.cors = true;

    $.ajax({
        url: serviceUrl,
        type: "POST",
        success: function (response, textStatus, jqXHR) {
            retrievePaymentScheduleTotal();
            retrievePaymentScheduleItems();

        },
        error: function (jqXHR, textStatus, errorThrown) {
            retrievePaymentScheduleTotal();
            retrievePaymentScheduleItems();
        }
    });
}
