﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Interop;

namespace PaymentSchedule
{
    public partial class MainPage : UserControl
    {
        public MainPage()
        {
            InitializeComponent();
            SetProgrammeOfferingId();
            BindPaymentSchedule();
        }

        private void SetProgrammeOfferingId()
        {
            TextBlock txtword = new TextBlock
            {
                Text = "PO Id: " + App.Current.Host.InitParams["id"],
                Foreground = new SolidColorBrush(Colors.Black),
                Height = 18,
                Width = 350,
                FontSize = 12,
                HorizontalAlignment = System.Windows.HorizontalAlignment.Left,
                VerticalAlignment = System.Windows.VerticalAlignment.Top,
                Margin = new Thickness(26, 12, 0, 0),
                FontFamily = new FontFamily("Arial"),
            };
            LayoutRoot.Children.Add(txtword);
        }

        private void BindPaymentSchedule()
        {
            var source = new List<PaymentSchedule>();

            GetMockData(source);

            dataGridPaymentSchedule.ItemsSource = source;
        }

        private static void GetMockData(List<PaymentSchedule> source)
        {
            var item = new PaymentSchedule();
            item.Description = 0.5;
            item.Amount = 2200;
            item.Date = DateTime.Now;
            source.Add(item);

            item = new PaymentSchedule();
            item.Description = 0.25;
            item.Amount = 1100;
            item.Date = DateTime.Now;
            source.Add(item);

            item = new PaymentSchedule();
            item.Description = 0.25;
            item.Amount = 1100;
            item.Date = DateTime.Now;
            source.Add(item);
        }
    }


}
