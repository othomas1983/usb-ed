﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace PaymentSchedule
{
    public class PaymentSchedule
    {
        public double Description { get; set; }

        public double Amount { get; set; }

        public DateTime Date { get; set; }
    }
}
